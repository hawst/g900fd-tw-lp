.class public interface abstract Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;
.super Ljava/lang/Object;
.source "SGWidgetTransformationListener.java"


# virtual methods
.method public abstract onLocalTransformChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
.end method

.method public abstract onOpacityChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidget;F)V
.end method

.method public abstract onPositionChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public abstract onRotationChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public abstract onScaleChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public abstract onSizeChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method
