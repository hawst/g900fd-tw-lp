.class Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;
.super Ljava/lang/Object;
.source "SGEditTextSelectionPointerController.java"

# interfaces
.implements Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;
.implements Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicy;


# static fields
.field private static final AUTOSCROLL_TRESHOLD:I = 0xa

.field static final CURSOR_POINTER_INDEX:I = 0x2

.field public static final EPSILON_FLOAT:F = 1.0E-4f

.field private static final INVALID_POINTER:I = -0x1

.field static final LEFT_SELECTION_INDEX:I = 0x0

.field private static final LOG_TAG:Ljava/lang/String; = "EditTextBox"

.field static final RIGHT_SELECTION_INDEX:I = 0x1

.field static mPointerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCursorPosition:I

.field private mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

.field private mHadSelection:Z

.field mPointerDownTextPosition:I

.field private mPointerInversionEnabled:Z

.field private mSelectionEnabled:Z

.field private mSelectionEnd:I

.field private mSelectionPointerContainer:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

.field private mSelectionPointerController:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;

.field mSelectionPointerIndexChange:I

.field private mSelectionStart:I

.field private mTouchDownCoordinate:Lcom/samsung/android/sdk/sgi/base/SGVector2f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerController:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;

    .line 97
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    .line 98
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerContainer:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 104
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerIndexChange:I

    .line 107
    iput v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mCursorPosition:I

    .line 395
    iput v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerDownTextPosition:I

    .line 114
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mContext:Landroid/content/Context;

    .line 117
    iput-boolean v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionEnabled:Z

    .line 118
    iput-boolean v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerInversionEnabled:Z

    .line 120
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerController:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;

    .line 121
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->initSelectionPointers()V

    .line 122
    return-void
.end method

.method private initSelectionPointers()V
    .locals 3

    .prologue
    const/high16 v1, 0x42a00000    # 80.0f

    .line 125
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    .line 126
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, v1, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    .line 127
    new-instance v1, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0, v2, p0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/content/Context;Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;)V

    .line 128
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setIndex(I)V

    .line 129
    sget-object v2, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    new-instance v1, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0, v2, p0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/content/Context;Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;)V

    .line 132
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setIndex(I)V

    .line 133
    sget-object v2, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    new-instance v1, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0, v2, p0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/content/Context;Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointerListener;)V

    .line 136
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setIndex(I)V

    .line 137
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    return-void
.end method

.method private performScrollingInAdvance(I)V
    .locals 3

    .prologue
    .line 289
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 291
    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerDownTextPosition:I

    if-le v1, p1, :cond_1

    .line 292
    if-lez p1, :cond_0

    add-int/lit8 p1, p1, -0x1

    .line 297
    :cond_0
    :goto_0
    invoke-virtual {v0, p1}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getTotalPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 300
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->performHorizontalScrollIfNecessary(F)V

    .line 301
    return-void

    .line 293
    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerDownTextPosition:I

    if-ge v1, p1, :cond_0

    .line 294
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    .line 295
    add-int/lit8 v2, p1, 0x1

    if-ge v2, v1, :cond_0

    add-int/lit8 p1, p1, 0x1

    goto :goto_0
.end method

.method private scaleContainerShiftToLocal(FFLcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;)Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 5

    .prologue
    const v4, 0x38d1b717    # 1.0E-4f

    const/4 v1, 0x0

    .line 370
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getRelativeTransform(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    move-result-object v0

    .line 371
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->getScale(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    move-result-object v2

    .line 372
    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getX()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_0

    move v0, v1

    .line 373
    :goto_0
    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getY()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v4

    if-gez v3, :cond_1

    move v2, v1

    .line 374
    :goto_1
    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    invoke-direct {v3, v0, v2, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(FFF)V

    return-object v3

    .line 372
    :cond_0
    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getX()F

    move-result v0

    div-float v0, p1, v0

    goto :goto_0

    .line 373
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getY()F

    move-result v2

    div-float v2, p2, v2

    goto :goto_1
.end method

.method private updateSelectionByNewPointerPosition(II)V
    .locals 3

    .prologue
    .line 304
    .line 307
    if-nez p2, :cond_0

    .line 308
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    .line 309
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->positionPointer(II)V

    move v2, v0

    move v0, p1

    move p1, v2

    .line 315
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Landroid/widget/EditText;->setSelection(II)V

    .line 316
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->invalidate()V

    .line 317
    return-void

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    .line 313
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->positionPointer(II)V

    goto :goto_0
.end method


# virtual methods
.method attachDetachWidgetToContainer(ZLcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;)V
    .locals 2

    .prologue
    .line 556
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->INVISIBLE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    invoke-virtual {p2, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setVisibility(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;)V

    .line 557
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerContainer:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-nez v0, :cond_3

    .line 558
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    .line 559
    if-nez v0, :cond_0

    .line 560
    if-eqz p1, :cond_1

    .line 561
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onPointerShow: null surface!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 567
    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 568
    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->removeWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 569
    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    .line 584
    :goto_0
    if-eqz p1, :cond_5

    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->VISIBLE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    .line 585
    :goto_1
    invoke-virtual {p2, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setVisibility(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;)V

    .line 586
    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->bringToFront()V

    .line 587
    :cond_1
    return-void

    .line 572
    :cond_2
    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->removeWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    goto :goto_0

    .line 576
    :cond_3
    if-eqz p1, :cond_4

    .line 577
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerContainer:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 578
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerContainer:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    goto :goto_0

    .line 581
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerContainer:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    goto :goto_0

    .line 584
    :cond_5
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->INVISIBLE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    goto :goto_1
.end method

.method public getEditTextDelegate()Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    return-object v0
.end method

.method getLetterLeftOffset(I)F
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 195
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    if-nez v0, :cond_1

    .line 196
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SGEditTextSelectionPolicy: no EdtiText delegate available, use setEditTextDelegate"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 199
    if-nez v2, :cond_2

    .line 200
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SGEditTextSelectionPolicy: null layout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_2
    invoke-virtual {v2, v4}, Landroid/text/Layout;->getLineStart(I)I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v1, v0

    .line 203
    if-gtz p1, :cond_3

    move v0, v1

    .line 213
    :goto_0
    return v0

    .line 206
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 207
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v0

    .line 208
    if-le p1, v0, :cond_4

    move p1, v0

    .line 211
    :cond_4
    invoke-virtual {v2}, Landroid/text/Layout;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 212
    invoke-static {v3, v4, p1, v0}, Landroid/text/Layout;->getDesiredWidth(Ljava/lang/CharSequence;IILandroid/text/TextPaint;)F

    move-result v0

    add-float/2addr v0, v1

    .line 213
    goto :goto_0
.end method

.method getLineHeight()F
    .locals 3

    .prologue
    .line 217
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    if-nez v0, :cond_1

    .line 218
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SGEditTextSelectionPolicy: no EdtiText delegate available, use setEditTextDelegate"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 221
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getLineCount()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getLineTop(I)I

    move-result v1

    invoke-virtual {v0}, Landroid/text/Layout;->getBottomPadding()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/text/Layout;->getTopPadding()I

    move-result v0

    sub-int v0, v1, v0

    int-to-float v0, v0

    return v0
.end method

.method getScale(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 514
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>()V

    .line 515
    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-direct {v1, v5, v4, v4, v4}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(FFFF)V

    .line 516
    new-instance v2, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-direct {v2, v4, v5, v4, v4}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(FFFF)V

    .line 517
    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-direct {v3, v4, v4, v5, v4}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(FFFF)V

    .line 519
    invoke-virtual {p1, v1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->transformVector(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    move-result-object v1

    .line 520
    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->transformVector(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    move-result-object v2

    .line 521
    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->transformVector(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    move-result-object v3

    .line 523
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getLength()F

    move-result v1

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getLength()F

    move-result v2

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getLength()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->set(FFF)V

    .line 524
    return-object v0
.end method

.method public getSelectionPointerContainer()Lcom/samsung/android/sdk/sgi/ui/SGWidget;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerContainer:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    return-object v0
.end method

.method inversePointerSelection()V
    .locals 4

    .prologue
    .line 414
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 450
    :cond_0
    :goto_0
    return-void

    .line 417
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    .line 418
    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    .line 420
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getLocalCoordinates()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getLocalCoordinates()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 421
    const-string v2, "EditTextBox"

    const-string v3, "inversePointerSelection: need to apply"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    new-instance v2, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getLocalCoordinates()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 424
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getLocalCoordinates()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setLocalCoordinates(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 425
    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setLocalCoordinates(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 427
    new-instance v2, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 428
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 429
    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 441
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getTextPosition()I

    move-result v2

    .line 442
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getTextPosition()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setTextPosition(I)V

    .line 443
    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setTextPosition(I)V

    .line 445
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getTextPosition()I

    move-result v0

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getTextPosition()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setSelection(II)V

    goto :goto_0

    .line 448
    :cond_2
    const-string v0, "EditTextBox"

    const-string v1, "inversePointerSelection: NO need to apply"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isSelectionEnabled()Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionEnabled:Z

    return v0
.end method

.method public isSelectionPointersEmptySelection()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 664
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    if-nez v0, :cond_1

    .line 665
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SGEditTextSelectionPolicy::onPointerShow: no EdtiText delegate available, use setEditTextDelegate"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 668
    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    .line 669
    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    .line 671
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getVisibility()Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->VISIBLE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    if-eq v4, v5, :cond_2

    move v0, v2

    .line 685
    :goto_0
    return v0

    .line 674
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getVisibility()Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->VISIBLE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    if-eq v4, v5, :cond_3

    move v0, v2

    .line 675
    goto :goto_0

    .line 677
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getCursorPosition()I

    move-result v4

    .line 678
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getTextPosition()I

    move-result v0

    .line 679
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getTextPosition()I

    move-result v1

    .line 680
    if-ne v0, v4, :cond_4

    if-ne v0, v1, :cond_4

    move v0, v3

    .line 683
    goto :goto_0

    :cond_4
    move v0, v2

    .line 685
    goto :goto_0
.end method

.method public onPointerDown(FFI)V
    .locals 4

    .prologue
    .line 378
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerIndexChange:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 379
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerIndexChange:I

    if-eq v0, p3, :cond_0

    .line 380
    const-string v0, "EditTextBox"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPointerDown: dismiss "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    :goto_0
    return-void

    .line 384
    :cond_0
    iput p3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerIndexChange:I

    .line 386
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    .line 388
    const-string v1, "EditTextBox"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPointerDown: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " at position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getTextPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getLocalCoordinates()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mTouchDownCoordinate:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 392
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getTextPosition()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerDownTextPosition:I

    goto :goto_0
.end method

.method public onPointerRequestPosition(FFFI)V
    .locals 7

    .prologue
    .line 229
    const-string v0, "EditTextBox"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPointerRequestPosition idx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " finalX "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerIndexChange:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 231
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerIndexChange:I

    if-eq v0, p4, :cond_0

    .line 284
    :goto_0
    return-void

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    if-nez v0, :cond_2

    .line 236
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SGEditTextSelectionPolicy::onPointerRequestPosition: no EdtiText delegate available, use setEditTextDelegate"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241
    :cond_2
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-interface {v0, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    .line 243
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mTouchDownCoordinate:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    if-nez v1, :cond_3

    .line 244
    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getLocalCoordinates()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mTouchDownCoordinate:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 250
    :cond_3
    invoke-direct {p0, p2, p3, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->scaleContainerShiftToLocal(FFLcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;)Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    move-result-object v1

    .line 251
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getX()F

    move-result v1

    .line 253
    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerDownTextPosition:I

    .line 254
    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v3

    .line 255
    invoke-virtual {v3, v2}, Landroid/text/Layout;->getPrimaryHorizontal(I)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getTotalPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getScrollX()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    .line 258
    add-float/2addr v1, v2

    .line 260
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getScrollX()I

    move-result v2

    .line 261
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->performHorizontalScrollIfNecessary(F)V

    .line 263
    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Landroid/widget/EditText;->getOffsetForPosition(FF)I

    move-result v1

    .line 266
    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->performScrollingInAdvance(I)V

    .line 268
    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getScrollX()I

    move-result v3

    .line 269
    if-eq v3, v2, :cond_4

    .line 270
    const-string v4, "EditTextBox"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SCROLL:: fScrollNewX = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " fScrollOldX "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getOnTouchStartPosition()F

    move-result v4

    int-to-float v3, v3

    sub-float v3, v4, v3

    int-to-float v2, v2

    add-float/2addr v2, v3

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setOnTouchStartPosition(F)V

    .line 273
    :cond_4
    const-string v0, "EditTextBox"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "New pointer position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " start "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " end "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    invoke-virtual {p0, v1, p4}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->positionPointer(II)V

    .line 278
    const/4 v0, 0x2

    if-ne p4, v0, :cond_5

    .line 279
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setCursorPosition(I)V

    goto/16 :goto_0

    .line 282
    :cond_5
    invoke-direct {p0, v1, p4}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->updateSelectionByNewPointerPosition(II)V

    goto/16 :goto_0
.end method

.method public onPointerShow(ZI)V
    .locals 3

    .prologue
    .line 454
    const-string v1, "EditTextBox"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPointerShow: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_1

    const-string v0, " SHOW"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerIndexChange:I

    .line 458
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    if-nez v0, :cond_2

    .line 459
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SGEditTextSelectionPolicy::onPointerShow: no EdtiText delegate available, use setEditTextDelegate"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 454
    :cond_1
    const-string v0, " HIDE"

    goto :goto_0

    .line 462
    :cond_2
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    .line 465
    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->attachDetachWidgetToContainer(ZLcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;)V

    .line 467
    if-nez p1, :cond_3

    .line 479
    :goto_1
    return-void

    .line 472
    :cond_3
    if-nez p2, :cond_4

    .line 473
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    .line 478
    :goto_2
    invoke-virtual {p0, v0, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->positionPointer(II)V

    goto :goto_1

    .line 476
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    goto :goto_2
.end method

.method public onPointerUp(FFI)V
    .locals 3

    .prologue
    .line 398
    const-string v0, "EditTextBox"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPointerUp: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerIndexChange:I

    .line 400
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mTouchDownCoordinate:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 401
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerDownTextPosition:I

    .line 402
    const/4 v0, 0x2

    if-ne p3, v0, :cond_1

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 405
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerInversionEnabled:Z

    if-eqz v0, :cond_0

    .line 406
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->inversePointerSelection()V

    goto :goto_0
.end method

.method performHorizontalScrollIfNecessary(F)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 324
    const-string v1, "EditTextBox"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "performHorizontalScrollIfNecessary : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getGravity()I

    move-result v1

    and-int/lit8 v1, v1, 0x7

    const/4 v2, 0x3

    if-eq v1, v2, :cond_1

    .line 326
    const-string v0, "EditTextBox"

    const-string v1, "performHorizontalScrollIfNecessary - no scroll"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    :cond_0
    :goto_0
    return-void

    .line 329
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getPaddingRight()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    .line 330
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->computeScroll()V

    .line 331
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getScrollX()I

    move-result v2

    int-to-float v2, v2

    .line 332
    cmpl-float v3, p1, v1

    if-ltz v3, :cond_3

    .line 333
    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Landroid/widget/EditText;->getOffsetForPosition(FF)I

    move-result v0

    .line 334
    sub-float v3, p1, v1

    add-float/2addr v2, v3

    .line 335
    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v3

    .line 336
    const-string v4, "EditTextBox"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "performHorizontalScrollIfNecessary  border "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " real "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3, v7}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    invoke-virtual {v3, v7}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v4

    if-eq v0, v4, :cond_2

    .line 342
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    float-to-int v1, v2

    invoke-virtual {v0, v1, v7}, Landroid/widget/EditText;->scrollTo(II)V

    goto :goto_0

    .line 345
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->isTextFitsControlHorizontally()Z

    move-result v0

    if-nez v0, :cond_0

    .line 346
    invoke-virtual {v3, v7}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v0

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    .line 347
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    float-to-int v0, v0

    invoke-virtual {v1, v0, v7}, Landroid/widget/EditText;->scrollTo(II)V

    goto/16 :goto_0

    .line 351
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_0

    .line 352
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    sub-float/2addr v1, p1

    .line 353
    cmpl-float v3, v2, v1

    if-ltz v3, :cond_4

    .line 354
    sub-float v0, v2, v1

    .line 359
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->canScrollHorizontally(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 360
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    float-to-int v0, v0

    invoke-virtual {v1, v0, v7}, Landroid/widget/EditText;->scrollTo(II)V

    goto/16 :goto_0
.end method

.method positionPointer(II)V
    .locals 5

    .prologue
    .line 529
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    .line 531
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->getLetterLeftOffset(I)F

    move-result v2

    .line 532
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 533
    if-nez v1, :cond_1

    const/4 v1, 0x0

    .line 535
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getScrollX()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    add-float/2addr v1, v3

    add-float/2addr v1, v2

    .line 536
    const-string v2, "EditTextBox"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "positionPointer ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: scroll = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getScrollX()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->getLineHeight()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EditText;->getTotalPaddingTop()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v2, v3

    .line 539
    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setPosition(FF)V

    .line 540
    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v3, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setLocalCoordinates(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 542
    const-string v2, "EditTextBox"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "positionPointer ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]: fx = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " size "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " pointer "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getPaddingRight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    cmpl-float v2, v1, v2

    if-gtz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 544
    :cond_0
    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->INVISIBLE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setVisibility(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;)V

    .line 550
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerContainer:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {p0, v1, v2, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->showSelectionPointers(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 551
    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setTextPosition(I)V

    .line 552
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->invalidate()V

    .line 553
    return-void

    .line 533
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/text/Layout;->getLineLeft(I)F

    move-result v1

    goto/16 :goto_0

    .line 547
    :cond_2
    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->VISIBLE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setVisibility(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;)V

    goto :goto_1
.end method

.method public restoreSelectionState()V
    .locals 3

    .prologue
    .line 652
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mHadSelection:Z

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mCursorPosition:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setCursorPosition(I)V

    .line 654
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionStart:I

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionEnd:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->setSelection(II)V

    .line 655
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionStart:I

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionEnd:I

    if-ne v0, v1, :cond_0

    .line 656
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->showSelectionPointersForced()V

    .line 659
    :cond_0
    return-void
.end method

.method public saveSelectionState()V
    .locals 1

    .prologue
    .line 641
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mHadSelection:Z

    .line 642
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->hasSelection()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->isSelectionPointersEmptySelection()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 643
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mHadSelection:Z

    .line 644
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getCursorPosition()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mCursorPosition:I

    .line 645
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionStart:I

    .line 646
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->getInternalEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionEnd:I

    .line 648
    :cond_1
    return-void
.end method

.method public setCursorPointerDrawable(I)V
    .locals 1

    .prologue
    .line 621
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->setPointerResourceID(II)V

    .line 622
    return-void
.end method

.method public setCursorPointerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 626
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->setPointerDrawable(ILandroid/graphics/drawable/Drawable;)V

    .line 627
    return-void
.end method

.method public setEditTextDelegate(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mEditTextDelegate:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    .line 187
    return-void
.end method

.method public setLeftSelectionPointerDrawable(I)V
    .locals 1

    .prologue
    .line 601
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->setPointerResourceID(II)V

    .line 602
    return-void
.end method

.method public setLeftSelectionPointerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 611
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->setPointerDrawable(ILandroid/graphics/drawable/Drawable;)V

    .line 612
    return-void
.end method

.method setPointerDrawable(ILandroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 595
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    .line 596
    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 597
    return-void
.end method

.method setPointerResourceID(II)V
    .locals 1

    .prologue
    .line 590
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;

    .line 591
    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;->setDrawableID(I)V

    .line 592
    return-void
.end method

.method public setPointersContainer(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerContainer:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 175
    return-void
.end method

.method public setPointersInversionEnabled(Z)V
    .locals 0

    .prologue
    .line 636
    iput-boolean p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerInversionEnabled:Z

    .line 637
    return-void
.end method

.method public setPointersSize(FF)V
    .locals 2

    .prologue
    .line 631
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerController:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;

    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-virtual {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;->setPointersSize(FFLjava/util/List;)V

    .line 632
    return-void
.end method

.method public setRightSelectionPointerDrawable(I)V
    .locals 1

    .prologue
    .line 606
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->setPointerResourceID(II)V

    .line 607
    return-void
.end method

.method public setRightSelectionPointerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 616
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->setPointerDrawable(ILandroid/graphics/drawable/Drawable;)V

    .line 617
    return-void
.end method

.method public setSelectionEnabled(Z)V
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionEnabled:Z

    if-nez v0, :cond_0

    .line 168
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->showSelectionPointers(Z)V

    .line 170
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionEnabled:Z

    .line 171
    return-void
.end method

.method public setSelectionPointerController(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerController:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;

    .line 142
    return-void
.end method

.method public setSelectionPointerList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/sgi/widgets/SGDefaultSelectionPointer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 145
    sput-object p1, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    .line 146
    return-void
.end method

.method public showCursorGlyph(Z)V
    .locals 2

    .prologue
    .line 690
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerController:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;

    if-eqz v0, :cond_0

    .line 691
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerController:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;

    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;->showCursorGlyph(ZLjava/util/List;)V

    .line 693
    :cond_0
    return-void
.end method

.method showSelectionPointers(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 8

    .prologue
    const v5, 0x38d1b717    # 1.0E-4f

    const/4 v7, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 482
    if-nez p2, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getRelativeTransform(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    move-result-object v0

    .line 483
    :goto_0
    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-direct {v3, v0}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 484
    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    invoke-direct {v1, v7, v7, v7}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(FFF)V

    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->translate(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 486
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->getScale(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    move-result-object v4

    .line 487
    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getX()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v5

    if-gez v1, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->setX(F)V

    .line 488
    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getY()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v5

    if-gez v1, :cond_2

    move v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->setY(F)V

    .line 489
    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getZ()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v5

    if-gez v1, :cond_3

    move v1, v2

    :goto_3
    invoke-virtual {v4, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->setZ(F)V

    .line 490
    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getX()F

    move-result v5

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getY()F

    move-result v6

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getZ()F

    move-result v4

    invoke-direct {v1, v5, v6, v4}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(FFF)V

    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->scale(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 493
    invoke-virtual {p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v1

    .line 495
    new-instance v4, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-direct {v4}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>()V

    .line 496
    const/4 v5, 0x0

    invoke-virtual {v4, v5, v2}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->setElement(IF)V

    .line 497
    const/4 v5, 0x5

    invoke-virtual {v4, v5, v2}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->setElement(IF)V

    .line 498
    const/16 v5, 0xa

    invoke-virtual {v4, v5, v2}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->setElement(IF)V

    .line 499
    const/16 v5, 0xf

    invoke-virtual {v4, v5, v2}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->setElement(IF)V

    .line 500
    new-instance v5, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getQuaternion()Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    move-result-object v3

    invoke-direct {v5, v3}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 502
    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->rotate(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 503
    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    neg-float v1, v1

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v1, v6

    invoke-direct {v3, v1, v7, v7, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(FFFF)V

    invoke-virtual {v4, v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->transformVector(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    move-result-object v1

    .line 504
    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getX()F

    move-result v4

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getY()F

    move-result v1

    invoke-direct {v3, v4, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    .line 505
    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-virtual {p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v4

    invoke-virtual {p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v6

    invoke-direct {v1, v4, v6, v7, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(FFFF)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->transformVector(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    move-result-object v0

    .line 506
    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getX()F

    move-result v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getY()F

    move-result v0

    invoke-direct {v1, v2, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    .line 508
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    add-float/2addr v0, v2

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    add-float/2addr v2, v3

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->set(FF)V

    .line 509
    invoke-virtual {p3, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 510
    invoke-virtual {p3, v5}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setRotation(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 511
    return-void

    .line 482
    :cond_0
    invoke-virtual {p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getRelativeTransform(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    move-result-object v0

    goto/16 :goto_0

    .line 487
    :cond_1
    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getX()F

    move-result v1

    div-float v1, v2, v1

    goto/16 :goto_1

    .line 488
    :cond_2
    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getY()F

    move-result v1

    div-float v1, v2, v1

    goto/16 :goto_2

    .line 489
    :cond_3
    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getZ()F

    move-result v1

    div-float v1, v2, v1

    goto/16 :goto_3
.end method

.method public showSelectionPointers(Z)V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerController:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;

    if-nez v0, :cond_0

    .line 150
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SGEditTextSelectionPolicy: no EdtiText delegate available, use setEditTextDelegate"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_0
    if-eqz p1, :cond_2

    .line 153
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->isSelectionEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 154
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerController:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;

    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;->showSelectionPointers(ZLjava/util/List;)V

    .line 160
    :cond_1
    :goto_0
    return-void

    .line 158
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mSelectionPointerController:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;

    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPolicyImpl;->mPointerList:Ljava/util/List;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextSelectionPointerController;->showSelectionPointers(ZLjava/util/List;)V

    goto :goto_0
.end method
