.class public Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;
.super Lcom/samsung/android/sdk/sgi/ui/SGWidget;
.source "SGScrollIndicator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$1;,
        Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollableIndicatorListener;,
        Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;
    }
.end annotation


# instance fields
.field mContentAreaSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

.field mEndOffset:F

.field mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

.field mLayerSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

.field mMinScrollSize:F

.field mOrientation:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

.field mPadding:F

.field mProgress:F

.field mScrollPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

.field mScrollableIndicatorListener:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollableIndicatorListener;

.field mSizeInd:F

.field mSizeParent:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

.field mStartOffset:F


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x41000000    # 8.0f

    const/high16 v1, 0x40800000    # 4.0f

    .line 75
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, v2, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 43
    iput v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mProgress:F

    .line 44
    iput v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeInd:F

    .line 45
    const/high16 v0, 0x41800000    # 16.0f

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mMinScrollSize:F

    .line 46
    iput v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mPadding:F

    .line 49
    iput v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mStartOffset:F

    .line 50
    iput v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mEndOffset:F

    .line 56
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->VERTICAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mOrientation:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    .line 57
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollableIndicatorListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollableIndicatorListener;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$1;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mScrollableIndicatorListener:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollableIndicatorListener;

    .line 76
    iput-object p3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeParent:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 77
    iput-object p2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mContentAreaSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 78
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mScrollPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 80
    if-nez p1, :cond_0

    .line 81
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 82
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mScrollableIndicatorListener:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollableIndicatorListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->setRedrawListener(Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListener;)V

    .line 91
    :goto_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeInd:F

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeInd:F

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayerSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 92
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayerSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 93
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mProgress:F

    invoke-direct {v1, v3, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 94
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->setContentAreaSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 96
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 97
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 98
    return-void

    .line 84
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v0

    array-length v0, v0

    if-eqz v0, :cond_1

    .line 85
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 88
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 87
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    goto :goto_1
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;-><init>(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 66
    return-void
.end method


# virtual methods
.method public getContentAreaSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mContentAreaSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    return-object v0
.end method

.method public getEndOffset()F
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mEndOffset:F

    return v0
.end method

.method public getIndicatorSize()F
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeInd:F

    return v0
.end method

.method public getMinScrollSize()F
    .locals 1

    .prologue
    .line 248
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mMinScrollSize:F

    return v0
.end method

.method public getOrientation()Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mOrientation:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    return-object v0
.end method

.method public getPadding()F
    .locals 1

    .prologue
    .line 291
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mPadding:F

    return v0
.end method

.method public getScrollAt()F
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mProgress:F

    return v0
.end method

.method public getStartOffset()F
    .locals 1

    .prologue
    .line 263
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mStartOffset:F

    return v0
.end method

.method public setContentAreaSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 188
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    cmpg-float v1, v1, v0

    if-gtz v1, :cond_0

    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->HORIZONTAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mOrientation:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    if-eq v1, v2, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    cmpg-float v1, v1, v0

    if-gtz v1, :cond_2

    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->VERTICAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mOrientation:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    if-ne v1, v2, :cond_2

    .line 211
    :cond_1
    :goto_0
    return-void

    .line 191
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mContentAreaSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 196
    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->VERTICAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mOrientation:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    if-ne v1, v2, :cond_4

    .line 197
    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeInd:F

    .line 198
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeParent:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeParent:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mStartOffset:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mEndOffset:F

    sub-float/2addr v2, v3

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mContentAreaSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    div-float/2addr v0, v2

    .line 199
    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mMinScrollSize:F

    cmpg-float v2, v0, v2

    if-gez v2, :cond_3

    .line 200
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mMinScrollSize:F

    .line 208
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayerSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2, v1, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->set(FF)V

    .line 209
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayerSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 210
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mProgress:F

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->setScrollTo(F)V

    goto :goto_0

    .line 201
    :cond_4
    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->HORIZONTAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mOrientation:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    if-ne v1, v2, :cond_5

    .line 202
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeInd:F

    .line 203
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeParent:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeParent:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mStartOffset:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mEndOffset:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mContentAreaSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    div-float/2addr v1, v2

    .line 204
    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mMinScrollSize:F

    cmpg-float v2, v1, v2

    if-gez v2, :cond_3

    .line 205
    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mMinScrollSize:F

    goto :goto_1

    :cond_5
    move v1, v0

    goto :goto_1
.end method

.method public setEndOffset(F)V
    .locals 0

    .prologue
    .line 270
    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mEndOffset:F

    .line 271
    return-void
.end method

.method public setIndicatorSize(F)V
    .locals 2

    .prologue
    .line 124
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    .line 125
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Size of slider must be greater than zero"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeInd:F

    .line 128
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayerSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0, p1, p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->set(FF)V

    .line 129
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayerSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 130
    return-void
.end method

.method public setMinScrollSize(F)F
    .locals 2

    .prologue
    .line 225
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    .line 226
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Length of slider must be greater than zero"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mMinScrollSize:F

    .line 230
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mOrientation:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->VERTICAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    if-ne v0, v1, :cond_2

    .line 231
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeParent:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayerSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mStartOffset:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mEndOffset:F

    sub-float/2addr v0, v1

    .line 233
    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mMinScrollSize:F

    cmpl-float v1, v1, v0

    if-ltz v1, :cond_1

    .line 234
    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mMinScrollSize:F

    .line 241
    :cond_1
    :goto_0
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mMinScrollSize:F

    return v0

    .line 235
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mOrientation:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->HORIZONTAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    if-ne v0, v1, :cond_1

    .line 236
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeParent:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayerSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mStartOffset:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mEndOffset:F

    sub-float/2addr v0, v1

    .line 238
    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mMinScrollSize:F

    cmpl-float v1, v1, v0

    if-ltz v1, :cond_1

    .line 239
    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mMinScrollSize:F

    goto :goto_0
.end method

.method public setOrientation(Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;)V
    .locals 1

    .prologue
    .line 104
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mOrientation:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    .line 105
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mContentAreaSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->setContentAreaSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 106
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mProgress:F

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->setScrollTo(F)V

    .line 107
    return-void
.end method

.method public setPadding(F)V
    .locals 0

    .prologue
    .line 284
    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mPadding:F

    .line 285
    return-void
.end method

.method setScrollAreaSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 0

    .prologue
    .line 305
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeParent:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 306
    return-void
.end method

.method public setScrollTo(F)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 152
    cmpg-float v1, p1, v0

    if-ltz v1, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, p1, v1

    if-lez v1, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mProgress:F

    .line 160
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayerSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    .line 161
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mLayerSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    .line 163
    sget-object v2, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->VERTICAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mOrientation:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    if-ne v2, v4, :cond_3

    .line 164
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeParent:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mPadding:F

    sub-float v2, v0, v1

    .line 165
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeParent:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    sub-float/2addr v0, v3

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mStartOffset:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mEndOffset:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mProgress:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mStartOffset:F

    add-float/2addr v1, v0

    .line 167
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeParent:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    sub-float/2addr v0, v3

    iget v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mEndOffset:F

    sub-float/2addr v0, v3

    .line 169
    cmpl-float v3, v1, v0

    if-ltz v3, :cond_5

    move v1, v2

    .line 180
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mScrollPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2, v1, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->set(FF)V

    .line 181
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mScrollPosition:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    goto :goto_0

    .line 171
    :cond_3
    sget-object v2, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->HORIZONTAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mOrientation:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    if-ne v2, v4, :cond_4

    .line 172
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeParent:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    sub-float/2addr v0, v1

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mStartOffset:F

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mEndOffset:F

    sub-float/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mProgress:F

    mul-float/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mStartOffset:F

    add-float/2addr v2, v0

    .line 173
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeParent:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iget v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mPadding:F

    sub-float/2addr v0, v4

    sub-float/2addr v0, v3

    .line 175
    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mSizeParent:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v3

    sub-float v1, v3, v1

    iget v3, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mEndOffset:F

    sub-float/2addr v1, v3

    .line 177
    cmpl-float v3, v2, v1

    if-gez v3, :cond_2

    move v1, v2

    goto :goto_1

    :cond_4
    move v1, v0

    goto :goto_1

    :cond_5
    move v0, v1

    move v1, v2

    goto :goto_1
.end method

.method public setStartOffset(F)V
    .locals 0

    .prologue
    .line 256
    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;->mStartOffset:F

    .line 257
    return-void
.end method
