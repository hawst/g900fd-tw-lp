.class public final Lcom/samsung/android/sdk/sgi/vi/SGSurface;
.super Ljava/lang/Object;
.source "SGSurface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/vi/SGSurface$1;
    }
.end annotation


# instance fields
.field mAsyncScreenshotListenersArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field mBackgroundPropertyListenerArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

.field private mRootLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

.field mSurfaceChangesDrawnListenerArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field mSurfaceListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;

.field private mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

.field private mView:Landroid/view/View;

.field private mWidgets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/sgi/ui/SGWidget;",
            ">;"
        }
    .end annotation
.end field

.field private swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 588
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGSurface()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;-><init>(JZ)V

    .line 589
    return-void
.end method

.method private constructor <init>(JZ)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 33
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 36
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCMemOwn:Z

    .line 37
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    .line 38
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mSurfaceListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mAsyncScreenshotListenersArray:Ljava/util/ArrayList;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mSurfaceChangesDrawnListenerArray:Ljava/util/ArrayList;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mBackgroundPropertyListenerArray:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    .line 43
    return-void
.end method

.method constructor <init>(Lcom/samsung/android/sdk/sgi/vi/SGContext;Landroid/view/View;Lcom/samsung/android/sdk/sgi/base/SGVector2i;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 75
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGSurface()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;-><init>(JZ)V

    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, p1, p3, v2, v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->init(Lcom/samsung/android/sdk/sgi/vi/SGContext;Lcom/samsung/android/sdk/sgi/base/SGVector2i;ZI)V

    .line 77
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v0

    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getHandle()J

    move-result-wide v2

    invoke-virtual {v0, p0, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    .line 78
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getRootNative()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mRootLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 79
    iput-object p2, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mView:Landroid/view/View;

    .line 80
    return-void
.end method

.method constructor <init>(Lcom/samsung/android/sdk/sgi/vi/SGContext;Landroid/view/View;Lcom/samsung/android/sdk/sgi/base/SGVector2i;ZI)V
    .locals 4

    .prologue
    .line 86
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGSurface()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;-><init>(JZ)V

    .line 87
    invoke-direct {p0, p1, p3, p4, p5}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->init(Lcom/samsung/android/sdk/sgi/vi/SGContext;Lcom/samsung/android/sdk/sgi/base/SGVector2i;ZI)V

    .line 88
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v0

    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getHandle()J

    move-result-wide v2

    invoke-virtual {v0, p0, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    .line 89
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getRootNative()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mRootLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 90
    iput-object p2, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mView:Landroid/view/View;

    .line 91
    return-void
.end method

.method private addChangesDrawnListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerBase;)V
    .locals 6

    .prologue
    .line 671
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_addChangesDrawnListener(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerBase;)V

    .line 672
    return-void
.end method

.method private addW(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I
    .locals 6

    .prologue
    .line 689
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_addW__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    move-result v0

    return v0
.end method

.method private addW(Lcom/samsung/android/sdk/sgi/ui/SGWidget;I)I
    .locals 7

    .prologue
    .line 692
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_addW__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)I

    move-result v0

    return v0
.end method

.method private bringWToF(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 6

    .prologue
    .line 704
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_bringWToF(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 705
    return-void
.end method

.method private final checkOnTouchUp(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 544
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getPointerCount()I

    move-result v2

    move v1, v0

    .line 545
    :goto_0
    if-ge v1, v2, :cond_1

    .line 546
    invoke-virtual {p1, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction(I)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v3

    .line 547
    sget-object v4, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-eq v3, v4, :cond_0

    sget-object v4, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->CANCEL:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-eq v3, v4, :cond_0

    .line 551
    :goto_1
    return v0

    .line 545
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 551
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private final dispatchHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 462
    new-instance v6, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    invoke-direct {v6, p1, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Z)V

    .line 463
    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getPointerCount()I

    move-result v7

    .line 464
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v5, v0

    :goto_0
    if-lez v5, :cond_8

    .line 465
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    add-int/lit8 v3, v5, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move v4, v2

    move v3, v1

    .line 467
    :goto_1
    if-ge v4, v7, :cond_1

    .line 469
    invoke-virtual {v6, v4}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getRawX(I)F

    move-result v8

    invoke-virtual {v6, v4}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getRawY(I)F

    move-result v9

    invoke-virtual {v0, v8, v9}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getLocationInWindow(FF)Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    .line 470
    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v9

    .line 471
    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v8

    .line 472
    if-eqz v3, :cond_0

    invoke-virtual {v0, v9, v8}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isLocalPointOnWidget(FF)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v1

    .line 473
    :goto_2
    if-ne v3, v1, :cond_1

    .line 474
    invoke-virtual {v6, v4, v9}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setX(IF)V

    .line 475
    invoke-virtual {v6, v4, v8}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setY(IF)V

    .line 467
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_0
    move v3, v2

    .line 472
    goto :goto_2

    .line 482
    :cond_1
    if-eqz v3, :cond_a

    .line 484
    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 486
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    .line 487
    if-nez v0, :cond_2

    .line 488
    iput-object v10, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 510
    :cond_2
    :goto_3
    if-eqz v0, :cond_7

    move v0, v1

    .line 520
    :goto_4
    return v0

    .line 493
    :cond_3
    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v3

    .line 494
    sget-object v4, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_ENTER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    invoke-virtual {v6, v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setAction(ILcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V

    .line 495
    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v4

    .line 496
    invoke-virtual {v6, v2, v3}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setAction(ILcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V

    .line 497
    if-eqz v4, :cond_6

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v3

    if-eqz v3, :cond_6

    move v3, v1

    .line 498
    :goto_5
    if-eqz v3, :cond_5

    .line 500
    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-eqz v4, :cond_4

    .line 502
    new-instance v4, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    invoke-direct {v4, v6}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)V

    .line 503
    sget-object v8, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_EXIT:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    invoke-virtual {v4, v2, v8}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setAction(ILcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V

    .line 504
    iget-object v8, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v8, v4}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    .line 506
    :cond_4
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    :cond_5
    move v0, v3

    goto :goto_3

    :cond_6
    move v3, v2

    .line 497
    goto :goto_5

    .line 464
    :cond_7
    add-int/lit8 v0, v5, -0x1

    move v5, v0

    goto/16 :goto_0

    .line 513
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-eqz v0, :cond_9

    .line 515
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    invoke-direct {v0, v6}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)V

    .line 516
    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_EXIT:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    invoke-virtual {v0, v2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setAction(ILcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V

    .line 517
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    .line 518
    iput-object v10, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    :cond_9
    move v0, v2

    .line 520
    goto :goto_4

    :cond_a
    move v0, v2

    goto :goto_3
.end method

.method private final dispatchTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 2

    .prologue
    .line 524
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-nez v0, :cond_3

    .line 525
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->processRawTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    .line 531
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-eqz v1, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->checkOnTouchUp(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    if-nez v0, :cond_2

    .line 532
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->uncaptureWidget()V

    .line 533
    :cond_2
    return v0

    .line 528
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getTouchEventInLocalWindow(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    move-result-object v0

    .line 529
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method private enqueueTexture(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListenerBase;I)V
    .locals 8

    .prologue
    .line 762
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListenerBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListenerBase;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    move v7, p3

    invoke-static/range {v0 .. v7}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_enqueueTexture(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Bitmap;JLcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListenerBase;I)V

    .line 763
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGSurface;)J
    .locals 2

    .prologue
    .line 46
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    goto :goto_0
.end method

.method private getHandle()J
    .locals 2

    .prologue
    .line 735
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_getHandle(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)J

    move-result-wide v0

    return-wide v0
.end method

.method private getRootNative()Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    .locals 4

    .prologue
    .line 722
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_getRootNative(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)J

    move-result-wide v2

    .line 723
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;-><init>(JZ)V

    goto :goto_0
.end method

.method private init(Lcom/samsung/android/sdk/sgi/vi/SGContext;Lcom/samsung/android/sdk/sgi/base/SGVector2i;ZI)V
    .locals 11

    .prologue
    .line 591
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGContext;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGContext;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    move v9, p3

    move v10, p4

    invoke-static/range {v0 .. v10}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_init(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/vi/SGContext;JLcom/samsung/android/sdk/sgi/base/SGVector2i;ZI)V

    .line 592
    return-void
.end method

.method private makeScreenShot(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V
    .locals 8

    .prologue
    .line 750
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)J

    move-result-wide v5

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v7, p3

    invoke-static/range {v0 .. v7}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_makeScreenShot__SWIG_7(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V

    .line 751
    return-void
.end method

.method private makeScreenShot(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V
    .locals 7

    .prologue
    .line 747
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_makeScreenShot__SWIG_6(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Bitmap;JLcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V

    .line 748
    return-void
.end method

.method private makeScreenShot(Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
    .locals 10

    .prologue
    .line 753
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)J

    move-result-wide v7

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    move-object v9, p3

    invoke-static/range {v0 .. v9}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_makeScreenShot__SWIG_8(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V

    .line 754
    return-void
.end method

.method private makeScreenShot(Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
    .locals 9

    .prologue
    .line 756
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_makeScreenShot__SWIG_9(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V

    .line 757
    return-void
.end method

.method private makeScreenShot(Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
    .locals 6

    .prologue
    .line 759
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_makeScreenShot__SWIG_10(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V

    .line 760
    return-void
.end method

.method private onHE(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 6

    .prologue
    .line 683
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_onHE(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    return v0
.end method

.method private onTE(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 6

    .prologue
    .line 680
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_onTE(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    return v0
.end method

.method private final processRawTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 554
    new-instance v6, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    invoke-direct {v6, p1, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Z)V

    .line 555
    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getPointerCount()I

    move-result v7

    .line 556
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v5, v0

    :goto_0
    if-lez v5, :cond_4

    .line 557
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    add-int/lit8 v3, v5, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 558
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_3

    move v4, v2

    move v3, v1

    .line 561
    :goto_1
    if-ge v4, v7, :cond_1

    .line 562
    invoke-virtual {v6, v4}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getRawX(I)F

    move-result v8

    invoke-virtual {v6, v4}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getRawY(I)F

    move-result v9

    invoke-virtual {v0, v8, v9}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getLocationInWindow(FF)Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    .line 563
    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v9

    .line 564
    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v8

    .line 565
    if-eqz v3, :cond_0

    invoke-virtual {v0, v9, v8}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isLocalPointOnWidget(FF)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v1

    .line 566
    :goto_2
    if-eqz v3, :cond_1

    .line 567
    invoke-virtual {v6, v4, v9}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setX(IF)V

    .line 568
    invoke-virtual {v6, v4, v8}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setY(IF)V

    .line 561
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_0
    move v3, v2

    .line 565
    goto :goto_2

    .line 574
    :cond_1
    if-eqz v3, :cond_3

    .line 575
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 576
    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 585
    :goto_3
    return v0

    .line 580
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 556
    :cond_3
    add-int/lit8 v0, v5, -0x1

    move v5, v0

    goto :goto_0

    :cond_4
    move v0, v2

    .line 585
    goto :goto_3
.end method

.method private final removeCapturing(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 381
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 382
    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 383
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 385
    :cond_1
    return-void
.end method

.method private removeW(I)V
    .locals 2

    .prologue
    .line 701
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_removeW__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGSurface;I)V

    .line 702
    return-void
.end method

.method private removeW(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 6

    .prologue
    .line 698
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_removeW__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 699
    return-void
.end method

.method private sendWToB(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 6

    .prologue
    .line 707
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_sendWToB(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 708
    return-void
.end method

.method private setDrawFrameListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;)V
    .locals 6

    .prologue
    .line 726
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_setDrawFrameListener(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;)V

    .line 727
    return-void
.end method

.method private setSizeChangeListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;)V
    .locals 6

    .prologue
    .line 729
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_setSizeChangeListener(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;)V

    .line 730
    return-void
.end method


# virtual methods
.method public addChangesDrawnListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;)V
    .locals 2

    .prologue
    .line 279
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerHolder;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGSurface;Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;)V

    .line 282
    :try_start_0
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addChangesDrawnListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    const/4 v0, 0x0

    .line 287
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerHolder;->detachListener()V

    .line 289
    :cond_0
    return-void

    .line 287
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mRootLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    move-result v0

    return v0
.end method

.method public addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;I)I
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mRootLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;I)I

    move-result v0

    return v0
.end method

.method public addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I
    .locals 3

    .prologue
    .line 313
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getParent()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    .line 314
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addW(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    move-result v1

    .line 315
    if-eqz v0, :cond_1

    .line 316
    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 322
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    return v1

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 320
    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method public addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;I)I
    .locals 3

    .prologue
    .line 334
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getParent()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    .line 335
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addW(Lcom/samsung/android/sdk/sgi/ui/SGWidget;I)I

    move-result v1

    .line 336
    if-eqz v0, :cond_1

    .line 337
    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 343
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 344
    return v1

    .line 340
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 341
    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method public bringWidgetToFront(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 2

    .prologue
    .line 361
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter widget is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 362
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getWidgetIndex(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 366
    :goto_0
    return-void

    .line 363
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->bringWToF(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 364
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 365
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public dumpProfilingInfromation(Lcom/samsung/android/sdk/sgi/vi/SGProfilingInfromationType;)V
    .locals 3

    .prologue
    .line 713
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 715
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGProfilingInfromationType;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_dumpProfilingInfromation(JLcom/samsung/android/sdk/sgi/vi/SGSurface;I)V

    .line 717
    return-void
.end method

.method public enqueueTexture(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListener;I)V
    .locals 2

    .prologue
    .line 298
    if-nez p1, :cond_0

    .line 299
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Parameter bitmap is null in SGSurface::enqueueTexture"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 301
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGBackgorundPropertyListenerHolder;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mBackgroundPropertyListenerArray:Ljava/util/ArrayList;

    invoke-direct {v0, v1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGBackgorundPropertyListenerHolder;-><init>(Ljava/util/ArrayList;Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListener;)V

    .line 302
    invoke-direct {p0, p1, v0, p3}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->enqueueTexture(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListenerBase;I)V

    .line 303
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mBackgroundPropertyListenerArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 108
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    if-nez v1, :cond_1

    .line 109
    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    invoke-direct {p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getHandle()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getHandle()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public finalize()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 50
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 52
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v0

    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getHandle()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Deregister(J)Z

    .line 53
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCMemOwn:Z

    .line 56
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->delete_SGSurface(J)V

    .line 57
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mRootLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mRootLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->finalize()V

    .line 59
    :cond_0
    iput-wide v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    .line 61
    :cond_1
    return-void
.end method

.method public getCameraDistance()F
    .locals 2

    .prologue
    .line 641
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_getCameraDistance(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)F

    move-result v0

    return v0
.end method

.method public getCameraProjection()Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 606
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_getCameraProjection(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public getCameraWorld()Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 609
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_getCameraWorld(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public getColor()I
    .locals 2

    .prologue
    .line 621
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_getColor(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)I

    move-result v0

    return v0
.end method

.method public getDrawFrameListener()Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mSurfaceListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;->mDrawFrameListener:Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;

    return-object v0
.end method

.method public getFpsIndicator()Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;
    .locals 2

    .prologue
    .line 738
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_getFpsIndicator(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;

    move-result-object v0

    return-object v0
.end method

.method public getFpsLimit()I
    .locals 2

    .prologue
    .line 627
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_getFpsLimit(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)I

    move-result v0

    return v0
.end method

.method public getOpacity()F
    .locals 2

    .prologue
    .line 615
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_getOpacity(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)F

    move-result v0

    return v0
.end method

.method public getRoot()Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mRootLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    return-object v0
.end method

.method public getShowFpsIndicator()Z
    .locals 2

    .prologue
    .line 635
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_getShowFpsIndicator(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)Z

    move-result v0

    return v0
.end method

.method public getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2i;
    .locals 4

    .prologue
    .line 597
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_getSize(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;-><init>(JZ)V

    return-object v0
.end method

.method public getSizeChangeListener()Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mSurfaceListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;->mSizeChangeListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mView:Landroid/view/View;

    return-object v0
.end method

.method public getViewPortMax()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 603
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_getViewPortMax(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getViewPortMin()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 600
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_getViewPortMin(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getWidget(I)Lcom/samsung/android/sdk/sgi/ui/SGWidget;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    return-object v0
.end method

.method public getWidget(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/ui/SGWidget;
    .locals 3

    .prologue
    .line 423
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 424
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 425
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWidgetAtPoint(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Lcom/samsung/android/sdk/sgi/ui/SGWidget;
    .locals 6

    .prologue
    .line 732
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_getWidgetAtPoint(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    return-object v0
.end method

.method public getWidgetCount()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getWidgetsCount()I

    move-result v0

    return v0
.end method

.method public getWidgetIndex(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getWidgetsCount()I
    .locals 2

    .prologue
    .line 695
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_getWidgetsCount(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getHandle()J

    move-result-wide v0

    .line 115
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 116
    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    .line 118
    :goto_0
    return v0

    :cond_0
    long-to-int v0, v0

    goto :goto_0
.end method

.method public isScreenShotAttachDepthBuffer()Z
    .locals 2

    .prologue
    .line 653
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_isScreenShotAttachDepthBuffer(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)Z

    move-result v0

    return v0
.end method

.method public isScreenShotAttachStencilBuffer()Z
    .locals 2

    .prologue
    .line 656
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_isScreenShotAttachStencilBuffer(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)Z

    move-result v0

    return v0
.end method

.method public makeScreenShot()Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 2

    .prologue
    .line 659
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_makeScreenShot__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public makeScreenShot(Landroid/graphics/Rect;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 2

    .prologue
    .line 665
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_makeScreenShot__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Rect;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public makeScreenShot(Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 7

    .prologue
    .line 668
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_makeScreenShot__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public makeScreenShot(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 6

    .prologue
    .line 662
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_makeScreenShot__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public makeScreenShot(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 741
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_makeScreenShot__SWIG_4(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Bitmap;)V

    .line 742
    return-void
.end method

.method public makeScreenShot(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 744
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_makeScreenShot__SWIG_5(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V

    .line 745
    return-void
.end method

.method public makeScreenShot(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;)V
    .locals 2

    .prologue
    .line 212
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;

    invoke-direct {v0, p0, p3}, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGSurface;Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;)V

    .line 215
    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->makeScreenShot(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    const/4 v0, 0x0

    .line 220
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->detachListener()V

    .line 222
    :cond_0
    return-void

    .line 220
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public makeScreenShot(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;)V
    .locals 2

    .prologue
    .line 194
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGSurface;Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;)V

    .line 197
    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->makeScreenShot(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    const/4 v0, 0x0

    .line 202
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->detachListener()V

    .line 204
    :cond_0
    return-void

    .line 202
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public makeScreenShot(Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V
    .locals 2

    .prologue
    .line 263
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;

    invoke-direct {v0, p0, p3}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGSurface;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V

    .line 266
    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->makeScreenShot(Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 267
    const/4 v0, 0x0

    .line 271
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;->detachListener()V

    .line 273
    :cond_0
    return-void

    .line 271
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public makeScreenShot(Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V
    .locals 2

    .prologue
    .line 245
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGSurface;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V

    .line 248
    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->makeScreenShot(Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    const/4 v0, 0x0

    .line 253
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;->detachListener()V

    .line 255
    :cond_0
    return-void

    .line 253
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public makeScreenShot(Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V
    .locals 2

    .prologue
    .line 228
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGSurface;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V

    .line 231
    :try_start_0
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->makeScreenShot(Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    const/4 v0, 0x0

    .line 236
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;->detachListener()V

    .line 238
    :cond_0
    return-void

    .line 236
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public onHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 2

    .prologue
    .line 450
    sget-object v0, Lcom/samsung/android/sdk/sgi/vi/SGSurface$1;->$SwitchMap$com$samsung$android$sdk$sgi$ui$SGTouchEvent$SGAction:[I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 458
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 456
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->dispatchHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    goto :goto_0

    .line 450
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
    .locals 6

    .prologue
    .line 686
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_onKeyEvent(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 2

    .prologue
    .line 433
    sget-object v0, Lcom/samsung/android/sdk/sgi/vi/SGSurface$1;->$SwitchMap$com$samsung$android$sdk$sgi$ui$SGTouchEvent$SGAction:[I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 441
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->dispatchTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 439
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 433
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public pushQueue()V
    .locals 2

    .prologue
    .line 719
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_pushQueue(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)V

    .line 720
    return-void
.end method

.method public removeLayer(I)V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mRootLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->removeLayer(I)V

    .line 157
    return-void
.end method

.method public removeLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mRootLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->removeLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 149
    return-void
.end method

.method public removeWidget(I)V
    .locals 1

    .prologue
    .line 405
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getWidget(I)Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->removeCapturing(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 406
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->removeW(I)V

    .line 407
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 408
    return-void
.end method

.method public removeWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 393
    if-ltz v0, :cond_0

    .line 394
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->removeCapturing(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 395
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->removeW(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 396
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 398
    :cond_0
    return-void
.end method

.method public resetViewport()V
    .locals 2

    .prologue
    .line 677
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_resetViewport(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)V

    .line 678
    return-void
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 647
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_resume(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)V

    .line 648
    return-void
.end method

.method public sendWidgetToBack(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 2

    .prologue
    .line 374
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter widget is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getWidgetIndex(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 379
    :goto_0
    return-void

    .line 376
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->sendWToB(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 377
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 378
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mWidgets:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public setCameraDistance(F)V
    .locals 2

    .prologue
    .line 638
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_setCameraDistance(JLcom/samsung/android/sdk/sgi/vi/SGSurface;F)V

    .line 639
    return-void
.end method

.method public setColor(I)V
    .locals 2

    .prologue
    .line 612
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_setColor(JLcom/samsung/android/sdk/sgi/vi/SGSurface;I)V

    .line 613
    return-void
.end method

.method public setDrawFrameListener(Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;)V
    .locals 1

    .prologue
    .line 163
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mSurfaceListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;

    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setDrawFrameListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;)V

    .line 164
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mSurfaceListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;

    iput-object p1, v0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;->mDrawFrameListener:Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;

    .line 165
    return-void

    .line 163
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFpsIndicator(Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;)V
    .locals 6

    .prologue
    .line 710
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_setFpsIndicator(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;)V

    .line 711
    return-void
.end method

.method public setFpsLimit(I)V
    .locals 2

    .prologue
    .line 624
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_setFpsLimit(JLcom/samsung/android/sdk/sgi/vi/SGSurface;I)V

    .line 625
    return-void
.end method

.method public setOpacity(F)V
    .locals 2

    .prologue
    .line 618
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_setOpacity(JLcom/samsung/android/sdk/sgi/vi/SGSurface;F)V

    .line 619
    return-void
.end method

.method public setScreenShotOptions(ZZ)V
    .locals 2

    .prologue
    .line 650
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_setScreenShotOptions(JLcom/samsung/android/sdk/sgi/vi/SGSurface;ZZ)V

    .line 651
    return-void
.end method

.method public setShowFpsIndicator(Z)V
    .locals 2

    .prologue
    .line 630
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getFpsIndicator()Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;

    move-result-object v0

    if-nez v0, :cond_0

    .line 631
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSimpleFpsIndicator;-><init>()V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setFpsIndicator(Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;)V

    .line 632
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_setShowFpsIndicator(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Z)V

    .line 633
    return-void
.end method

.method public setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)V
    .locals 6

    .prologue
    .line 594
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_setSize(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)V

    .line 595
    return-void
.end method

.method public setSizeChangeListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;)V
    .locals 1

    .prologue
    .line 178
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mSurfaceListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;

    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setSizeChangeListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;)V

    .line 179
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mSurfaceListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;

    iput-object p1, v0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;->mSizeChangeListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;

    .line 180
    return-void

    .line 178
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setViewport(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 9

    .prologue
    .line 674
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_setViewport(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 675
    return-void
.end method

.method public suspend()V
    .locals 2

    .prologue
    .line 644
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGSurface_suspend(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)V

    .line 645
    return-void
.end method

.method public uncaptureWidget()V
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-eqz v0, :cond_0

    .line 539
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->uncaptureWidget()V

    .line 540
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 542
    :cond_0
    return-void
.end method
