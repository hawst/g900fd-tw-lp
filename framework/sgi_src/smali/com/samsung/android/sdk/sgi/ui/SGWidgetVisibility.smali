.class public final enum Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;
.super Ljava/lang/Enum;
.source "SGWidgetVisibility.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

.field public static final enum GONE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

.field public static final enum INVISIBLE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

.field public static final enum VISIBLE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    const-string v1, "VISIBLE"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->VISIBLE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    const-string v1, "INVISIBLE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->INVISIBLE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    const-string v1, "GONE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->GONE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    .line 18
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->VISIBLE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->INVISIBLE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->GONE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->$VALUES:[Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->$VALUES:[Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    return-object v0
.end method
