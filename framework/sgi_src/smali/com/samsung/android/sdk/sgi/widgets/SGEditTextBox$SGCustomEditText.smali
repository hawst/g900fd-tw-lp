.class Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomEditText;
.super Landroid/widget/EditText;
.source "SGEditTextBox.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SGCustomEditText"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomEditText;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    .line 269
    invoke-direct {p0, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 270
    return-void
.end method


# virtual methods
.method public drawableStateChanged()V
    .locals 1

    .prologue
    .line 310
    invoke-super {p0}, Landroid/widget/EditText;->drawableStateChanged()V

    .line 311
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomEditText;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onDrawableStateChanged()V

    .line 312
    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomEditText;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    # invokes: Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onScrollChangedWrapper(IIII)V
    invoke-static {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->access$300(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;IIII)V

    .line 302
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->onScrollChanged(IIII)V

    .line 303
    return-void
.end method

.method protected onSelectionChanged(II)V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox$SGCustomEditText;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    # invokes: Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->onSelectionChangedWrapper(II)V
    invoke-static {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;->access$200(Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;II)V

    .line 279
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onSelectionChanged(II)V

    .line 280
    return-void
.end method

.method public setCursorVisible(Z)V
    .locals 1

    .prologue
    .line 288
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 289
    return-void
.end method
