.class public final enum Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;
.super Ljava/lang/Enum;
.source "SGTextureFilterType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

.field public static final enum LINEAR:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

.field public static final enum LINEAR_MIPMAP_LINEAR:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

.field public static final enum LINEAR_MIPMAP_NEAREST:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

.field public static final enum NEAREST:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

.field public static final enum NEAREST_MIPMAP_LINEAR:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

.field public static final enum NEAREST_MIPMAP_NEAREST:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    const-string v1, "NEAREST"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;->NEAREST:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    const-string v1, "LINEAR"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;->LINEAR:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    const-string v1, "NEAREST_MIPMAP_NEAREST"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;->NEAREST_MIPMAP_NEAREST:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    const-string v1, "LINEAR_MIPMAP_NEAREST"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;->LINEAR_MIPMAP_NEAREST:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    .line 23
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    const-string v1, "NEAREST_MIPMAP_LINEAR"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;->NEAREST_MIPMAP_LINEAR:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    .line 24
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    const-string v1, "LINEAR_MIPMAP_LINEAR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;->LINEAR_MIPMAP_LINEAR:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    .line 18
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;->NEAREST:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;->LINEAR:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;->NEAREST_MIPMAP_NEAREST:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;->LINEAR_MIPMAP_NEAREST:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;->NEAREST_MIPMAP_LINEAR:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;->LINEAR_MIPMAP_LINEAR:Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureFilterType;

    return-object v0
.end method
