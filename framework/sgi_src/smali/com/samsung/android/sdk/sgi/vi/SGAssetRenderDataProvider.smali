.class public final Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;
.super Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;
.source "SGAssetRenderDataProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;,
        Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mShaderProgramBinaries:Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;

.field private mShaderSources:Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;

.field private mShadersPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;-><init>()V

    .line 202
    const-string v0, "Shaders/"

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->init(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 203
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 218
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;-><init>()V

    .line 219
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->init(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 220
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 227
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;-><init>()V

    .line 228
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->init(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 229
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 210
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;-><init>()V

    .line 211
    const-string v0, "Shaders/"

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->init(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 212
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 14
    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->loadBuiltinShaderData(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method private getPresentProgramHash(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;
    .locals 3

    .prologue
    .line 193
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->mShaderSources:Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->getHash(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->mShaderSources:Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;

    invoke-virtual {v2, p2}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->getHash(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;-><init>(II)V

    return-object v0
.end method

.method private init(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 231
    if-eqz p3, :cond_0

    .line 232
    const-string v0, "SGAssetRenderDataProvider"

    const-string v1, "shaders path: \'%s\', binary shader programs support: enabled"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->mShaderSources:Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;

    .line 234
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->mShaderProgramBinaries:Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;

    .line 240
    :goto_0
    return-void

    .line 236
    :cond_0
    const-string v0, "SGAssetRenderDataProvider"

    const-string v1, "shaders path: \'%s\', binary shader programs support: disabled"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->mContext:Landroid/content/Context;

    .line 238
    iput-object p2, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->mShadersPath:Ljava/lang/String;

    goto :goto_0
.end method

.method static loadShaderFromAssets(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->readStream(Ljava/io/InputStream;)[B

    move-result-object v0

    return-object v0
.end method

.method static readStream(Ljava/io/InputStream;)[B
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 16
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, p0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 17
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 19
    const/16 v2, 0x4000

    new-array v2, v2, [B

    .line 20
    :goto_0
    array-length v3, v2

    invoke-virtual {v0, v2, v5, v3}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 21
    invoke-virtual {v1, v2, v5, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 23
    :cond_0
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public loadProgram(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->mShaderProgramBinaries:Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->mShaderProgramBinaries:Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->getPresentProgramHash(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->load(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;)[B

    move-result-object v0

    .line 282
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadShaderData(Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->mShaderSources:Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;

    if-eqz v0, :cond_1

    .line 260
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->mShaderSources:Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderSourceRegistry;->get(Ljava/lang/String;)[B

    move-result-object v0

    .line 268
    :cond_0
    :goto_0
    return-object v0

    .line 262
    :cond_1
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->loadBuiltinShaderData(Ljava/lang/String;)[B

    move-result-object v0

    .line 263
    if-nez v0, :cond_0

    .line 264
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->loadShaderFromAssets(Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_0
.end method

.method loadShaderFromAssets(Ljava/lang/String;)[B
    .locals 5

    .prologue
    .line 243
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->mShadersPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->readStream(Ljava/io/InputStream;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 247
    :goto_0
    return-object v0

    .line 245
    :catch_0
    move-exception v0

    .line 246
    const-string v1, "SGAssetRenderDataProvider"

    const-string v2, "shader \'%s\' load from assets fail: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public saveProgram(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->mShaderProgramBinaries:Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->mShaderProgramBinaries:Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->getPresentProgramHash(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1, p3}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->save(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;[B)V

    .line 296
    :cond_0
    return-void
.end method
