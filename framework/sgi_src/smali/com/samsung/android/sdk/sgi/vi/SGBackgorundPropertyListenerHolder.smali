.class final Lcom/samsung/android/sdk/sgi/vi/SGBackgorundPropertyListenerHolder;
.super Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListenerBase;
.source "SGBackgorundPropertyListenerHolder.java"


# instance fields
.field mContainer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field mListener:Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListener;


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListenerBase;-><init>()V

    .line 31
    iput-object p2, p0, Lcom/samsung/android/sdk/sgi/vi/SGBackgorundPropertyListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListener;

    .line 32
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGBackgorundPropertyListenerHolder;->mContainer:Ljava/util/ArrayList;

    .line 33
    return-void
.end method


# virtual methods
.method public onFinish(JI)V
    .locals 3

    .prologue
    .line 38
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGProperty;

    const/4 v1, 0x1

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 41
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGBackgorundPropertyListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListener;

    invoke-interface {v1, v0, p3}, Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListener;->onFinish(Lcom/samsung/android/sdk/sgi/render/SGProperty;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGBackgorundPropertyListenerHolder;->mContainer:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGBackgorundPropertyListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListener;

    .line 50
    return-void

    .line 43
    :catch_0
    move-exception v0

    .line 45
    const-string v1, "SGBackgroundPropertyListener::onFinish error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGVIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method
