.class public Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;
.super Lcom/samsung/android/sdk/sgi/render/SGBuffer;
.source "SGIndexBuffer.java"


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 48
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGIndexBuffer__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;-><init>(JZ)V

    .line 49
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGBuffer;-><init>(JZ)V

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V
    .locals 3

    .prologue
    .line 79
    invoke-static {p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->SwigConstructSGIndexBuffer(Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;-><init>(JZ)V

    .line 80
    return-void
.end method

.method private static SwigConstructSGIndexBuffer(Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)J
    .locals 2

    .prologue
    .line 73
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 74
    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 75
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->ordinal()I

    move-result v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->ordinal()I

    move-result v1

    invoke-static {v0, v1, p2}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGIndexBuffer__SWIG_1(III)J

    move-result-wide v0

    return-wide v0
.end method

.method private init(Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V
    .locals 6

    .prologue
    .line 52
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 53
    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 55
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->ordinal()I

    move-result v3

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->ordinal()I

    move-result v4

    move-object v2, p0

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGIndexBuffer_init(JLcom/samsung/android/sdk/sgi/render/SGIndexBuffer;III)V

    .line 57
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;)Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;
    .locals 7

    .prologue
    .line 69
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGBuffer;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGIndexBuffer___assign__(JLcom/samsung/android/sdk/sgi/render/SGIndexBuffer;JLcom/samsung/android/sdk/sgi/render/SGIndexBuffer;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGIndexBuffer(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public getPrimitiveType()Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;
    .locals 4

    .prologue
    .line 60
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGIndexBuffer_getPrimitiveType(JLcom/samsung/android/sdk/sgi/render/SGIndexBuffer;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getShortBuffer()Ljava/nio/ShortBuffer;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    return-object v0
.end method

.method public setSize(I)V
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGIndexBuffer_setSize(JLcom/samsung/android/sdk/sgi/render/SGIndexBuffer;I)V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->buffer:Ljava/nio/ByteBuffer;

    .line 66
    return-void
.end method
