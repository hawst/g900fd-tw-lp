.class public final Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;
.super Lcom/samsung/android/sdk/sgi/animation/SGAnimation;
.source "SGParticleAnimation.java"


# direct methods
.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;-><init>(JZ)V

    .line 27
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;
    .locals 7

    .prologue
    .line 225
    new-instance v6, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation___assign__(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;-><init>(JZ)V

    return-object v6
.end method

.method public addColorKeyFrame(FI)V
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_addColorKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FI)V

    .line 146
    return-void
.end method

.method public addForceKeyFrame(FFFF)V
    .locals 7

    .prologue
    .line 153
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_addForceKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFFF)V

    .line 154
    return-void
.end method

.method public addGravityKeyFrame(FFFF)V
    .locals 7

    .prologue
    .line 149
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_addGravityKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFFF)V

    .line 150
    return-void
.end method

.method public addMassKeyFrame(FF)V
    .locals 2

    .prologue
    .line 157
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_addMassKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FF)V

    .line 158
    return-void
.end method

.method public addPositionKeyFrame(FFFF)V
    .locals 7

    .prologue
    .line 141
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_addPositionKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFFF)V

    .line 142
    return-void
.end method

.method public addScaleKeyFrame(FFFF)V
    .locals 7

    .prologue
    .line 137
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_addScaleKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFFF)V

    .line 138
    return-void
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 30
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 31
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCMemOwn:Z

    .line 33
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->delete_SGParticleAnimation(J)V

    .line 35
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    .line 37
    :cond_1
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 161
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_getCount(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)I

    move-result v0

    return v0
.end method

.method public getDefaultColor()I
    .locals 2

    .prologue
    .line 209
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_getDefaultColor(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)I

    move-result v0

    return v0
.end method

.method public getDefaultDuration()I
    .locals 2

    .prologue
    .line 197
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_getDefaultDuration(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)I

    move-result v0

    return v0
.end method

.method public getDefaultForce()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 217
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_getDefaultForce(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getDefaultGravity()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 213
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_getDefaultGravity(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getDefaultMass()F
    .locals 2

    .prologue
    .line 221
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_getDefaultMass(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)F

    move-result v0

    return v0
.end method

.method public getDefaultPosition()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 205
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_getDefaultPosition(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getDefaultSize()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 201
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_getDefaultSize(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getRandomColor()I
    .locals 2

    .prologue
    .line 181
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_getRandomColor(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)I

    move-result v0

    return v0
.end method

.method public getRandomDuration()I
    .locals 2

    .prologue
    .line 169
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_getRandomDuration(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)I

    move-result v0

    return v0
.end method

.method public getRandomForce()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 189
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_getRandomForce(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getRandomGravity()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 185
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_getRandomGravity(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getRandomMass()F
    .locals 2

    .prologue
    .line 193
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_getRandomMass(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)F

    move-result v0

    return v0
.end method

.method public getRandomPosition()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 177
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_getRandomPosition(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getRandomSize()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 173
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_getRandomSize(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public isParticleBlendingEnabled()Z
    .locals 2

    .prologue
    .line 165
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_isParticleBlendingEnabled(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)Z

    move-result v0

    return v0
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 297
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setBitmap(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;Landroid/graphics/Bitmap;)V

    .line 298
    return-void
.end method

.method public setColorAnimation(III)V
    .locals 6

    .prologue
    .line 261
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setColorAnimation__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;III)V

    .line 262
    return-void
.end method

.method public setColorAnimation(IIII)V
    .locals 7

    .prologue
    .line 257
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setColorAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IIII)V

    .line 258
    return-void
.end method

.method public setColorAnimation(IIIIZ)V
    .locals 8

    .prologue
    .line 253
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    invoke-static/range {v0 .. v7}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setColorAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IIIIZ)V

    .line 254
    return-void
.end method

.method public setCount(I)V
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setCount(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;I)V

    .line 42
    return-void
.end method

.method public setDefaultColor(I)V
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setDefaultColor(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;I)V

    .line 86
    return-void
.end method

.method public setDefaultDuration(I)V
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setDefaultDuration(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;I)V

    .line 46
    return-void
.end method

.method public setDefaultForce(FFF)V
    .locals 6

    .prologue
    .line 109
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setDefaultForce__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V

    .line 110
    return-void
.end method

.method public setDefaultForce(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 113
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setDefaultForce__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 114
    return-void
.end method

.method public setDefaultGravity(FFF)V
    .locals 6

    .prologue
    .line 93
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setDefaultGravity__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V

    .line 94
    return-void
.end method

.method public setDefaultGravity(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 97
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setDefaultGravity__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 98
    return-void
.end method

.method public setDefaultMass(F)V
    .locals 2

    .prologue
    .line 125
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setDefaultMass(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;F)V

    .line 126
    return-void
.end method

.method public setDefaultPosition(FFF)V
    .locals 6

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setDefaultPosition__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V

    .line 70
    return-void
.end method

.method public setDefaultPosition(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setDefaultPosition__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 74
    return-void
.end method

.method public setDefaultSize(FFF)V
    .locals 6

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setDefaultSize__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V

    .line 54
    return-void
.end method

.method public setDefaultSize(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setDefaultSize__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 58
    return-void
.end method

.method public setForceAnimation(ILcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 10

    .prologue
    .line 285
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    invoke-static/range {v0 .. v9}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setForceAnimation__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 286
    return-void
.end method

.method public setForceAnimation(ILcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;I)V
    .locals 11

    .prologue
    .line 281
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    move v10, p4

    invoke-static/range {v0 .. v10}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setForceAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)V

    .line 282
    return-void
.end method

.method public setForceAnimation(ILcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;IZ)V
    .locals 12

    .prologue
    .line 277
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    move/from16 v10, p4

    move/from16 v11, p5

    invoke-static/range {v0 .. v11}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setForceAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;IZ)V

    .line 278
    return-void
.end method

.method public setGravityAnimation(ILcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 10

    .prologue
    .line 273
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    invoke-static/range {v0 .. v9}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setGravityAnimation__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 274
    return-void
.end method

.method public setGravityAnimation(ILcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;I)V
    .locals 11

    .prologue
    .line 269
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    move v10, p4

    invoke-static/range {v0 .. v10}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setGravityAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)V

    .line 270
    return-void
.end method

.method public setGravityAnimation(ILcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;IZ)V
    .locals 12

    .prologue
    .line 265
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    move/from16 v10, p4

    move/from16 v11, p5

    invoke-static/range {v0 .. v11}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setGravityAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;IZ)V

    .line 266
    return-void
.end method

.method public setMassAnimation(IFF)V
    .locals 6

    .prologue
    .line 293
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setMassAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IFF)V

    .line 294
    return-void
.end method

.method public setMassAnimation(IFFI)V
    .locals 7

    .prologue
    .line 289
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setMassAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IFFI)V

    .line 290
    return-void
.end method

.method public setParticleBlendingEnabled(Z)V
    .locals 2

    .prologue
    .line 133
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setParticleBlendingEnabled(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;Z)V

    .line 134
    return-void
.end method

.method public setPositionAnimation(ILcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 10

    .prologue
    .line 249
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    invoke-static/range {v0 .. v9}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setPositionAnimation__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 250
    return-void
.end method

.method public setPositionAnimation(ILcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;I)V
    .locals 11

    .prologue
    .line 245
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    move v10, p4

    invoke-static/range {v0 .. v10}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setPositionAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)V

    .line 246
    return-void
.end method

.method public setPositionAnimation(ILcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;IZ)V
    .locals 12

    .prologue
    .line 241
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    move/from16 v10, p4

    move/from16 v11, p5

    invoke-static/range {v0 .. v11}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setPositionAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;IZ)V

    .line 242
    return-void
.end method

.method public setRandomColor(I)V
    .locals 2

    .prologue
    .line 89
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setRandomColor(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;I)V

    .line 90
    return-void
.end method

.method public setRandomDuration(I)V
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setRandomDuration(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;I)V

    .line 50
    return-void
.end method

.method public setRandomForce(FFF)V
    .locals 6

    .prologue
    .line 117
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setRandomForce__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V

    .line 118
    return-void
.end method

.method public setRandomForce(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 121
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setRandomForce__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 122
    return-void
.end method

.method public setRandomGravity(FFF)V
    .locals 6

    .prologue
    .line 101
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setRandomGravity__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V

    .line 102
    return-void
.end method

.method public setRandomGravity(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 105
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setRandomGravity__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 106
    return-void
.end method

.method public setRandomMass(F)V
    .locals 2

    .prologue
    .line 129
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setRandomMass(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;F)V

    .line 130
    return-void
.end method

.method public setRandomPosition(FFF)V
    .locals 6

    .prologue
    .line 77
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setRandomPosition__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V

    .line 78
    return-void
.end method

.method public setRandomPosition(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 81
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setRandomPosition__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 82
    return-void
.end method

.method public setRandomSize(FFF)V
    .locals 6

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setRandomSize__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V

    .line 62
    return-void
.end method

.method public setRandomSize(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setRandomSize__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 66
    return-void
.end method

.method public setScaleAnimation(ILcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 10

    .prologue
    .line 237
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    invoke-static/range {v0 .. v9}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setScaleAnimation__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 238
    return-void
.end method

.method public setScaleAnimation(ILcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;I)V
    .locals 11

    .prologue
    .line 233
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    move v10, p4

    invoke-static/range {v0 .. v10}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setScaleAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)V

    .line 234
    return-void
.end method

.method public setScaleAnimation(ILcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;IZ)V
    .locals 12

    .prologue
    .line 229
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v7

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    move-object v9, p3

    move/from16 v10, p4

    move/from16 v11, p5

    invoke-static/range {v0 .. v11}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGParticleAnimation_setScaleAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;IZ)V

    .line 230
    return-void
.end method
