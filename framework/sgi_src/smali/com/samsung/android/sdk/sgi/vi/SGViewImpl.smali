.class Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;
.super Ljava/lang/Object;
.source "SGViewImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnKeyLongPress;,
        Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnKeyEventStrategy;,
        Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnHoverEventStrategy;,
        Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnTouchEventStrategy;,
        Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultCheckInputConnectionProxyStrategy;
    }
.end annotation


# instance fields
.field private isEnabledForceMemoryManagement:Z

.field private mContext:Lcom/samsung/android/sdk/sgi/vi/SGContext;

.field private mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

.field private mIsSuspended:Z

.field private mRenderDataProvider:Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;

.field private mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

.field private mSurfaceResumed:Z

.field private mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

.field private mSurfaceWasDestroyed:Z

.field private mSuspendedByUser:Z

.field private mTimer:Ljava/util/Timer;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->isEnabledForceMemoryManagement:Z

    .line 33
    iput-boolean v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceWasDestroyed:Z

    .line 34
    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceResumed:Z

    .line 35
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    .line 431
    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSuspendedByUser:Z

    .line 432
    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mIsSuspended:Z

    .line 433
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

    .line 37
    if-nez p2, :cond_1

    .line 38
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mRenderDataProvider:Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;

    .line 41
    :goto_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    .line 42
    if-eqz p3, :cond_0

    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    iget v1, p3, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mRedSize:I

    iput v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mRedSize:I

    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    iget v1, p3, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mGreenSize:I

    iput v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mGreenSize:I

    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    iget v1, p3, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBlueSize:I

    iput v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBlueSize:I

    .line 47
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    iget v1, p3, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mAlphaSize:I

    iput v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mAlphaSize:I

    .line 48
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    iget v1, p3, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mDepthSize:I

    iput v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mDepthSize:I

    .line 49
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    iget v1, p3, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mStencilSize:I

    iput v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mStencilSize:I

    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    iget v1, p3, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSampleBuffers:I

    iput v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSampleBuffers:I

    .line 51
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    iget v1, p3, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSamples:I

    iput v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSamples:I

    .line 52
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    iget v1, p3, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBackgroundThreadCount:I

    iput v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBackgroundThreadCount:I

    .line 53
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    iget-boolean v1, p3, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSeparateThreads:Z

    iput-boolean v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSeparateThreads:Z

    .line 55
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGContext;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mRenderDataProvider:Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGContext;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContext:Lcom/samsung/android/sdk/sgi/vi/SGContext;

    .line 56
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContext:Lcom/samsung/android/sdk/sgi/vi/SGContext;

    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGVector2i;

    invoke-direct {v3, v4, v4}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    iget-boolean v4, v2, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSeparateThreads:Z

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    iget v5, v2, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBackgroundThreadCount:I

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGContext;Landroid/view/View;Lcom/samsung/android/sdk/sgi/base/SGVector2i;ZI)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    .line 57
    return-void

    .line 40
    :cond_1
    iput-object p2, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mRenderDataProvider:Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;

    goto :goto_0
.end method

.method private notifySurfaceSizeChange()V
    .locals 4

    .prologue
    .line 467
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getSizeChangeListener()Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;

    move-result-object v0

    .line 468
    if-eqz v0, :cond_0

    .line 469
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2i;

    move-result-object v1

    .line 470
    new-instance v2, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getX()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getY()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;->onResize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 472
    :cond_0
    return-void
.end method

.method private resumeForcedMemoryManagement()V
    .locals 6

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->isEnabledForceMemoryManagement:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mTimer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mTimer:Ljava/util/Timer;

    .line 157
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$1;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;)V

    const-wide/32 v2, 0xea60

    const-wide/32 v4, 0x2bf20

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 164
    :cond_0
    return-void
.end method

.method private suspendForcedMemoryManagement()V
    .locals 1

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->isEnabledForceMemoryManagement:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mTimer:Ljava/util/Timer;

    .line 152
    :cond_0
    return-void
.end method

.method private suspendSip()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 121
    .line 123
    :try_start_0
    const-class v0, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    const-string v2, "suspendSip"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 124
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    .line 132
    :goto_0
    if-eqz v0, :cond_0

    .line 134
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_1
    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2

    .line 139
    :cond_0
    :goto_1
    return-void

    .line 126
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 131
    goto :goto_0

    .line 129
    :catch_1
    move-exception v0

    move-object v0, v1

    .line 130
    goto :goto_0

    .line 137
    :catch_2
    move-exception v0

    goto :goto_1

    .line 136
    :catch_3
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public attachCurrentThread()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContext:Lcom/samsung/android/sdk/sgi/vi/SGContext;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGContext;->attachCurrentThread()V

    .line 142
    return-void
.end method

.method public checkInputConnectionProxy(Landroid/view/View;Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultCheckInputConnectionProxyStrategy;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 88
    .line 91
    const/4 v3, 0x1

    :try_start_0
    new-array v3, v3, [Ljava/lang/Class;

    .line 92
    const/4 v4, 0x0

    const-class v5, Landroid/view/View;

    aput-object v5, v3, v4

    .line 93
    const-class v4, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    const-string v5, "checkInputConnectionProxy"

    invoke-virtual {v4, v5, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 101
    :goto_0
    if-eqz v0, :cond_1

    .line 103
    const/4 v3, 0x1

    :try_start_1
    invoke-virtual {v0, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 104
    const/4 v3, 0x0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 113
    :goto_1
    if-eqz v0, :cond_0

    move v0, v1

    .line 116
    :goto_2
    return v0

    .line 106
    :catch_0
    move-exception v0

    move v0, v2

    .line 111
    goto :goto_1

    .line 109
    :catch_1
    move-exception v0

    move v0, v2

    .line 110
    goto :goto_1

    .line 116
    :cond_0
    invoke-interface {p2, p1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultCheckInputConnectionProxyStrategy;->checkInputConnectionProxy(Landroid/view/View;)Z

    move-result v0

    goto :goto_2

    .line 98
    :catch_2
    move-exception v3

    goto :goto_0

    .line 95
    :catch_3
    move-exception v3

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public createHoverEvent(Landroid/view/MotionEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 225
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 226
    const/4 v0, 0x0

    .line 261
    :cond_0
    return-object v0

    .line 227
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>()V

    .line 228
    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    sub-long/2addr v6, v8

    sub-long/2addr v4, v6

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setTouchTime(Ljava/util/Date;)V

    .line 229
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v10

    .line 230
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    .line 233
    :try_start_0
    const-string v3, "lastMotionEvent"

    invoke-virtual {v1, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 234
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 235
    invoke-virtual {v1, v0, p1}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 243
    :goto_0
    const/4 v1, 0x0

    move v9, v1

    :goto_1
    if-ge v9, v10, :cond_0

    .line 245
    sget-object v6, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->NOTHING:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 246
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v3, 0x9

    if-ne v1, v3, :cond_3

    .line 248
    sget-object v6, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_ENTER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 258
    :cond_2
    :goto_2
    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v7

    sget-object v8, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->STYLUS:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    move v3, v2

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->addPointer(IFFFFLcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;FLcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;)V

    .line 243
    add-int/lit8 v1, v9, 0x1

    move v9, v1

    goto :goto_1

    .line 236
    :catch_0
    move-exception v1

    .line 237
    invoke-virtual {v1}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    .line 238
    :catch_1
    move-exception v1

    .line 239
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 240
    :catch_2
    move-exception v1

    .line 241
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 250
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v3, 0xa

    if-ne v1, v3, :cond_4

    .line 252
    sget-object v6, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_EXIT:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    goto :goto_2

    .line 254
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v3, 0x7

    if-ne v1, v3, :cond_2

    .line 256
    sget-object v6, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_MOVE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    goto :goto_2
.end method

.method public createSGTouchEvent(Landroid/view/MotionEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;
    .locals 17

    .prologue
    .line 166
    new-instance v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    invoke-direct {v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>()V

    .line 167
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v12

    .line 168
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long v14, v4, v6

    .line 169
    new-instance v3, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    add-long/2addr v4, v14

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setTouchTime(Ljava/util/Date;)V

    .line 170
    new-instance v3, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    add-long/2addr v4, v14

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setDownTime(Ljava/util/Date;)V

    .line 171
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v13

    .line 172
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v16

    .line 173
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 176
    :try_start_0
    const-string v4, "lastMotionEvent"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 177
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 178
    move-object/from16 v0, p1

    invoke-virtual {v3, v2, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 186
    :goto_0
    const/4 v3, 0x0

    move v11, v3

    :goto_1
    if-ge v11, v12, :cond_7

    .line 188
    sget-object v8, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->NOTHING:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 189
    if-ne v11, v13, :cond_1

    .line 191
    if-eqz v16, :cond_0

    const/4 v3, 0x5

    move/from16 v0, v16

    if-ne v0, v3, :cond_2

    .line 193
    :cond_0
    sget-object v8, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->DOWN:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 212
    :cond_1
    :goto_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v4

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getY(I)F

    move-result v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v9

    sget-object v10, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->FINGER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    invoke-virtual/range {v2 .. v10}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->addPointer(IFFFFLcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;FLcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;)V

    .line 186
    add-int/lit8 v3, v11, 0x1

    move v11, v3

    goto :goto_1

    .line 179
    :catch_0
    move-exception v3

    .line 180
    invoke-virtual {v3}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    .line 181
    :catch_1
    move-exception v3

    .line 182
    invoke-virtual {v3}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 183
    :catch_2
    move-exception v3

    .line 184
    invoke-virtual {v3}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 195
    :cond_2
    const/4 v3, 0x1

    move/from16 v0, v16

    if-eq v0, v3, :cond_3

    const/4 v3, 0x6

    move/from16 v0, v16

    if-ne v0, v3, :cond_4

    .line 197
    :cond_3
    sget-object v8, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    goto :goto_2

    .line 199
    :cond_4
    const/4 v3, 0x2

    move/from16 v0, v16

    if-ne v0, v3, :cond_5

    .line 201
    sget-object v8, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->MOVE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    goto :goto_2

    .line 203
    :cond_5
    const/4 v3, 0x3

    move/from16 v0, v16

    if-ne v0, v3, :cond_6

    .line 205
    sget-object v8, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->CANCEL:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    goto :goto_2

    .line 207
    :cond_6
    const/4 v3, 0x4

    move/from16 v0, v16

    if-ne v0, v3, :cond_1

    .line 209
    sget-object v8, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->OUTSIDE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    goto :goto_2

    .line 215
    :cond_7
    const/4 v3, 0x0

    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v4

    if-ge v3, v4, :cond_9

    .line 217
    new-instance v4, Ljava/util/Date;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v6

    add-long/2addr v6, v14

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->appendHistoryEntry(Ljava/util/Date;)V

    .line 218
    const/4 v4, 0x0

    :goto_4
    if-ge v4, v12, :cond_8

    .line 219
    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, Landroid/view/MotionEvent;->getHistoricalX(II)F

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, Landroid/view/MotionEvent;->getHistoricalY(II)F

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v3}, Landroid/view/MotionEvent;->getHistoricalPressure(II)F

    move-result v7

    invoke-virtual {v2, v5, v6, v7}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->appendPointerToHistory(FFF)V

    .line 218
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 215
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 222
    :cond_9
    return-object v2
.end method

.method public detachCurrentThread()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContext:Lcom/samsung/android/sdk/sgi/vi/SGContext;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGContext;->detachCurrentThread()V

    .line 145
    return-void
.end method

.method public getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    return-object v0
.end method

.method public getSurfaceStateListener()Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

    return-object v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 62
    .line 64
    const/4 v0, 0x1

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    .line 65
    const/4 v2, 0x0

    const-class v3, Landroid/view/inputmethod/EditorInfo;

    aput-object v3, v0, v2

    .line 66
    const-class v2, Lcom/samsung/android/sdk/sgi/widgets/SGEditTextBox;

    const-string v3, "onInputConnectionCreated"

    invoke-virtual {v2, v3, v0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 74
    :goto_0
    if-eqz v0, :cond_0

    .line 76
    const/4 v2, 0x1

    :try_start_1
    invoke-virtual {v0, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 77
    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputConnection;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2

    .line 82
    :goto_1
    return-object v0

    .line 68
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 73
    goto :goto_0

    .line 71
    :catch_1
    move-exception v0

    move-object v0, v1

    .line 72
    goto :goto_0

    .line 80
    :catch_2
    move-exception v0

    :cond_0
    :goto_2
    move-object v0, v1

    .line 82
    goto :goto_1

    .line 79
    :catch_3
    move-exception v0

    goto :goto_2
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnHoverEventStrategy;)Z
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    if-eqz v0, :cond_1

    .line 277
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->createHoverEvent(Landroid/view/MotionEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    move-result-object v0

    .line 278
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->onHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    .line 281
    :goto_0
    return v0

    .line 279
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 281
    :cond_1
    invoke-interface {p2, p1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnHoverEventStrategy;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyEvent(IILandroid/view/KeyEvent;Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnKeyEventStrategy;)Z
    .locals 14

    .prologue
    .line 289
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 321
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result p1

    move v3, p1

    .line 325
    :goto_0
    new-instance v2, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;

    invoke-direct {v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>()V

    .line 326
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isNumLockOn()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setNumLockOn(Z)V

    .line 327
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isCapsLockOn()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setCapsLockOn(Z)V

    .line 328
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isScrollLockOn()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setScrollLockOn(Z)V

    .line 329
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setLeftCtrlPressed(Z)V

    .line 330
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setRightCtrlPressed(Z)V

    .line 331
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setLeftAltPressed(Z)V

    .line 332
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setRightAltPressed(Z)V

    .line 333
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setLeftShiftPressed(Z)V

    .line 334
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setRightShiftPressed(Z)V

    .line 335
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isMetaPressed()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setMetaPressed(Z)V

    .line 336
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isSymPressed()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setSymPressed(Z)V

    .line 337
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isFunctionPressed()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setFunctionPressed(Z)V

    .line 338
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setSystem(Z)V

    .line 339
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->isPrintingKey()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    const/16 v5, 0x3e

    if-ne v4, v5, :cond_7

    :cond_0
    const/4 v4, 0x1

    :goto_1
    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setPrintable(Z)V

    .line 340
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_2

    .line 342
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getCharacters()Ljava/lang/String;

    move-result-object v6

    .line 343
    if-nez v6, :cond_1

    .line 344
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v5

    int-to-char v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 345
    :cond_1
    sget-object v4, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ON_KEY_DOWN:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    new-instance v5, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v12

    sub-long/2addr v10, v12

    sub-long/2addr v8, v10

    invoke-direct {v5, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getFlags()I

    move-result v7

    invoke-static {v7}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->convertFlagFromAndroid(I)Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    move-result-object v7

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setKeyEvent(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Ljava/util/Date;Ljava/lang/String;Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;)V

    .line 347
    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-interface {v0, v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnKeyEventStrategy;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 349
    :cond_2
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    .line 351
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getCharacters()Ljava/lang/String;

    move-result-object v6

    .line 352
    if-nez v6, :cond_3

    .line 353
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v5

    int-to-char v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 354
    :cond_3
    sget-object v4, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ON_KEY_UP:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    new-instance v5, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v12

    sub-long/2addr v10, v12

    sub-long/2addr v8, v10

    invoke-direct {v5, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getFlags()I

    move-result v7

    invoke-static {v7}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->convertFlagFromAndroid(I)Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    move-result-object v7

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setKeyEvent(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Ljava/util/Date;Ljava/lang/String;Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;)V

    .line 356
    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-interface {v0, v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnKeyEventStrategy;->onKeyUp(ILandroid/view/KeyEvent;)Z

    .line 358
    :cond_4
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_6

    .line 360
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getCharacters()Ljava/lang/String;

    move-result-object v6

    .line 361
    if-nez v6, :cond_5

    .line 362
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v5

    int-to-char v5, v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 363
    :cond_5
    sget-object v4, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ON_KEY_MULTIPLE:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    new-instance v5, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v12

    sub-long/2addr v10, v12

    sub-long/2addr v8, v10

    invoke-direct {v5, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getFlags()I

    move-result v7

    invoke-static {v7}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->convertFlagFromAndroid(I)Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    move-result-object v7

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setKeyEvent(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Ljava/util/Date;Ljava/lang/String;Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;)V

    .line 364
    move/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setRepeatCount(I)V

    .line 365
    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-interface {v0, v3, v4, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnKeyEventStrategy;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    .line 367
    :cond_6
    new-instance v3, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v8

    sub-long/2addr v6, v8

    sub-long/2addr v4, v6

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setEventTime(Ljava/util/Date;)V

    .line 368
    new-instance v3, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual/range {p3 .. p3}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v8

    sub-long/2addr v6, v8

    sub-long/2addr v4, v6

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setDownTime(Ljava/util/Date;)V

    .line 369
    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    if-eqz v3, :cond_8

    .line 370
    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v2

    return v2

    :sswitch_0
    move v3, p1

    .line 292
    goto/16 :goto_0

    :sswitch_1
    move v3, p1

    .line 294
    goto/16 :goto_0

    :sswitch_2
    move v3, p1

    .line 296
    goto/16 :goto_0

    :sswitch_3
    move v3, p1

    .line 298
    goto/16 :goto_0

    :sswitch_4
    move v3, p1

    .line 300
    goto/16 :goto_0

    :sswitch_5
    move v3, p1

    .line 302
    goto/16 :goto_0

    :sswitch_6
    move v3, p1

    .line 304
    goto/16 :goto_0

    :sswitch_7
    move v3, p1

    .line 310
    goto/16 :goto_0

    .line 312
    :sswitch_8
    const/16 p1, 0x42

    move v3, p1

    .line 313
    goto/16 :goto_0

    .line 316
    :sswitch_9
    const/16 p1, 0x43

    move v3, p1

    .line 317
    goto/16 :goto_0

    .line 339
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 371
    :cond_8
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Surface is null in KeyEvent"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 289
    :sswitch_data_0
    .sparse-switch
        0x39 -> :sswitch_3
        0x3a -> :sswitch_4
        0x3b -> :sswitch_5
        0x3c -> :sswitch_6
        0x3d -> :sswitch_0
        0x42 -> :sswitch_8
        0x43 -> :sswitch_9
        0x71 -> :sswitch_1
        0x72 -> :sswitch_2
        0xdd -> :sswitch_7
    .end sparse-switch
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnKeyLongPress;)Z
    .locals 11

    .prologue
    const/16 v1, 0x2711

    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 377
    const/16 v0, 0x43

    if-ne p1, v0, :cond_3

    .line 379
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>()V

    .line 380
    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v8

    sub-long/2addr v6, v8

    sub-long/2addr v4, v6

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 381
    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setEventTime(Ljava/util/Date;)V

    .line 382
    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setDownTime(Ljava/util/Date;)V

    .line 383
    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setNumLockOn(Z)V

    .line 384
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setCapsLockOn(Z)V

    .line 385
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setScrollLockOn(Z)V

    .line 386
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setLeftCtrlPressed(Z)V

    .line 387
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setRightCtrlPressed(Z)V

    .line 388
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setLeftAltPressed(Z)V

    .line 389
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setRightAltPressed(Z)V

    .line 390
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setLeftShiftPressed(Z)V

    .line 391
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setRightShiftPressed(Z)V

    .line 392
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setMetaPressed(Z)V

    .line 393
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setSymPressed(Z)V

    .line 394
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setFunctionPressed(Z)V

    .line 395
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setSystem(Z)V

    .line 396
    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setPrintable(Z)V

    .line 397
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 398
    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ON_KEY_DOWN:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->getEventTime()Ljava/util/Date;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->convertFlagFromAndroid(I)Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setKeyEvent(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Ljava/util/Date;Ljava/lang/String;Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;)V

    .line 399
    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v10, :cond_1

    .line 400
    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ON_KEY_UP:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->getEventTime()Ljava/util/Date;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result v5

    invoke-static {v5}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->convertFlagFromAndroid(I)Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->setKeyEvent(ILcom/samsung/android/sdk/sgi/ui/SGKeyAction;Ljava/util/Date;Ljava/lang/String;Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;)V

    .line 401
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    if-eqz v1, :cond_2

    .line 402
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    .line 405
    :goto_0
    return v0

    .line 403
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Surface is null in KeyEvent"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 405
    :cond_3
    invoke-interface {p3, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnKeyLongPress;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnTouchEventStrategy;)Z
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->createSGTouchEvent(Landroid/view/MotionEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    .line 269
    :goto_0
    return v0

    :cond_0
    invoke-interface {p2, p1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnTouchEventStrategy;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public resume()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 485
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceResumed:Z

    if-nez v0, :cond_1

    .line 486
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceResumed:Z

    .line 487
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->resume()V

    .line 488
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceWasDestroyed:Z

    if-nez v0, :cond_0

    .line 489
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->notifySurfaceSizeChange()V

    .line 490
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->resumeForcedMemoryManagement()V

    .line 491
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->setResumed(Z)V

    .line 493
    :cond_1
    return-void
.end method

.method public setForceMemoryManagementEnabled(Z)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 409
    if-ne p1, v0, :cond_1

    .line 411
    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->isEnabledForceMemoryManagement:Z

    .line 412
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mTimer:Ljava/util/Timer;

    .line 413
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$2;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;)V

    const-wide/32 v2, 0xea60

    const-wide/32 v4, 0x2bf20

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 426
    :cond_0
    :goto_0
    return-void

    .line 422
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->isEnabledForceMemoryManagement:Z

    .line 423
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 424
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    goto :goto_0
.end method

.method public final setResumed(Z)V
    .locals 1

    .prologue
    .line 449
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSuspendedByUser:Z

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mIsSuspended:Z

    if-nez v0, :cond_1

    :cond_0
    if-nez p1, :cond_2

    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mIsSuspended:Z

    if-nez v0, :cond_2

    .line 450
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mIsSuspended:Z

    .line 451
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

    if-eqz v0, :cond_2

    .line 452
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;->onResumed()V

    .line 454
    :cond_2
    return-void
.end method

.method public setSurfaceStateListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;)V
    .locals 0

    .prologue
    .line 435
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

    .line 436
    return-void
.end method

.method public final setSuspended(Z)V
    .locals 1

    .prologue
    .line 441
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mIsSuspended:Z

    if-nez v0, :cond_0

    .line 442
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mIsSuspended:Z

    .line 443
    iput-boolean p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSuspendedByUser:Z

    .line 444
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;->onSuspended()V

    .line 447
    :cond_0
    return-void
.end method

.method public surfaceAvailable(Landroid/view/Surface;)V
    .locals 3

    .prologue
    .line 428
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceWasDestroyed:Z

    .line 429
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContext:Lcom/samsung/android/sdk/sgi/vi/SGContext;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContextConfiguration:Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    invoke-virtual {v0, v1, p1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGContext;->attachToNativeWindow(Lcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/view/Surface;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    .line 430
    return-void
.end method

.method public surfaceChanged(II)V
    .locals 3

    .prologue
    .line 456
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;-><init>(II)V

    .line 457
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2i;

    move-result-object v1

    .line 458
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)V

    .line 459
    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->isEqual(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->notifySurfaceSizeChange()V

    .line 461
    :cond_0
    return-void
.end method

.method public surfaceDestroyed()V
    .locals 2

    .prologue
    .line 463
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceWasDestroyed:Z

    .line 464
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mContext:Lcom/samsung/android/sdk/sgi/vi/SGContext;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGContext;->detachFromNativeWindow(Lcom/samsung/android/sdk/sgi/vi/SGSurface;)V

    .line 465
    return-void
.end method

.method public suspend()V
    .locals 1

    .prologue
    .line 474
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceResumed:Z

    if-eqz v0, :cond_0

    .line 475
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->setSuspended(Z)V

    .line 476
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurfaceResumed:Z

    .line 479
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->suspendSip()V

    .line 480
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->suspendForcedMemoryManagement()V

    .line 481
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->mSurface:Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->suspend()V

    .line 483
    :cond_0
    return-void
.end method
