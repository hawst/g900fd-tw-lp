.class Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$SGVisualValueReceiverScrollPolicy;
.super Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;
.source "SGScrollableAreaScrollPolicy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SGVisualValueReceiverScrollPolicy"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$SGVisualValueReceiverScrollPolicy;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;

    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$1;)V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$SGVisualValueReceiverScrollPolicy;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;)V

    return-void
.end method


# virtual methods
.method public onPosition(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 3

    .prologue
    .line 139
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$SGVisualValueReceiverScrollPolicy;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;

    # getter for: Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->mScrollableArea:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->access$100(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->setScrollIndicatorsEnabled(Z)V

    .line 140
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$SGVisualValueReceiverScrollPolicy;->this$0:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;

    # getter for: Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->mScrollableArea:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->access$100(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getX()F

    move-result v1

    neg-float v1, v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getY()F

    move-result v2

    neg-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->scrollTo(FF)V

    .line 141
    return-void
.end method
