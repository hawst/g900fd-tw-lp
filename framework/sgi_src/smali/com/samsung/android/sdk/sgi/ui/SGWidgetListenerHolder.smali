.class final Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;
.super Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;
.source "SGWidgetListenerHolder.java"


# instance fields
.field public mHoverListener:Lcom/samsung/android/sdk/sgi/ui/SGHoverListener;

.field public mKeyEventListener:Lcom/samsung/android/sdk/sgi/ui/SGKeyEventListener;

.field public mTouchListener:Lcom/samsung/android/sdk/sgi/ui/SGTouchListener;

.field public mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

.field public mWidgetAnimationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetAnimationListener;

.field public mWidgetTransformationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 34
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mTouchListener:Lcom/samsung/android/sdk/sgi/ui/SGTouchListener;

    .line 35
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mHoverListener:Lcom/samsung/android/sdk/sgi/ui/SGHoverListener;

    .line 36
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidgetTransformationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;

    .line 37
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mKeyEventListener:Lcom/samsung/android/sdk/sgi/ui/SGKeyEventListener;

    .line 38
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidgetAnimationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetAnimationListener;

    .line 39
    return-void
.end method


# virtual methods
.method public onFinished(J)V
    .locals 2

    .prologue
    .line 170
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidgetAnimationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetAnimationListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetAnimationListener;->onFinished(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :goto_0
    return-void

    .line 172
    :catch_0
    move-exception v0

    .line 174
    const-string v1, "SGWidgetAnimationListener::onFinished error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGUIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onHover(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;J)Z
    .locals 2

    .prologue
    .line 59
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mHoverListener:Lcom/samsung/android/sdk/sgi/ui/SGHoverListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/ui/SGHoverListener;->onHover(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 65
    :goto_0
    return v0

    .line 61
    :catch_0
    move-exception v0

    .line 63
    const-string v1, "SGHoverListener::onHover error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGUIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 65
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;J)Z
    .locals 2

    .prologue
    .line 145
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mKeyEventListener:Lcom/samsung/android/sdk/sgi/ui/SGKeyEventListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEventListener;->onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 151
    :goto_0
    return v0

    .line 147
    :catch_0
    move-exception v0

    .line 149
    const-string v1, "SGKeyEventListener::onKeyEvent error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGUIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 151
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLocalTransformChanged(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
    .locals 2

    .prologue
    .line 121
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidgetTransformationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-interface {v0, v1, p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;->onLocalTransformChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :goto_0
    return-void

    .line 123
    :catch_0
    move-exception v0

    .line 125
    const-string v1, "SGWidgetTransformationListener::onLocalTransformChanged error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGUIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onOpacityChanged(JF)V
    .locals 2

    .prologue
    .line 133
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidgetTransformationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-interface {v0, v1, p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;->onOpacityChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidget;F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 139
    :goto_0
    return-void

    .line 135
    :catch_0
    move-exception v0

    .line 137
    const-string v1, "SGWidgetTransformationListener::onOpacityChanged error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGUIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onPositionChanged(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 2

    .prologue
    .line 85
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidgetTransformationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-interface {v0, v1, p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;->onPositionChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :goto_0
    return-void

    .line 87
    :catch_0
    move-exception v0

    .line 89
    const-string v1, "SGWidgetTransformationListener::onPositionChanged error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGUIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onRotationChanged(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
    .locals 2

    .prologue
    .line 97
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidgetTransformationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-interface {v0, v1, p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;->onRotationChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    :goto_0
    return-void

    .line 99
    :catch_0
    move-exception v0

    .line 101
    const-string v1, "SGWidgetTransformationListener::onRotationChanged error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGUIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onScaleChanged(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 2

    .prologue
    .line 109
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidgetTransformationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-interface {v0, v1, p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;->onScaleChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_0
    return-void

    .line 111
    :catch_0
    move-exception v0

    .line 113
    const-string v1, "SGWidgetTransformationListener::onScaleChanged error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGUIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSizeChanged(JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 2

    .prologue
    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidgetTransformationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-interface {v0, v1, p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;->onSizeChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 77
    const-string v1, "SGWidgetTransformationListener::onSizeChanged error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGUIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStarted(J)V
    .locals 2

    .prologue
    .line 158
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidgetAnimationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetAnimationListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetAnimationListener;->onStarted(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    :goto_0
    return-void

    .line 160
    :catch_0
    move-exception v0

    .line 162
    const-string v1, "SGWidgetAnimationListener::onStarted error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGUIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;J)Z
    .locals 2

    .prologue
    .line 46
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mTouchListener:Lcom/samsung/android/sdk/sgi/ui/SGTouchListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchListener;->onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 52
    :goto_0
    return v0

    .line 48
    :catch_0
    move-exception v0

    .line 50
    const-string v1, "SGTouchListener::onTouchEvent error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGUIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 52
    const/4 v0, 0x0

    goto :goto_0
.end method
