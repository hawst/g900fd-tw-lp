.class final Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;
.super Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;
.source "SGSurfaceListenerHolder.java"


# instance fields
.field public mDrawFrameListener:Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;

.field public mSizeChangeListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;-><init>()V

    .line 26
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;->mSizeChangeListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;

    .line 27
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;->mDrawFrameListener:Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;

    .line 29
    return-void
.end method


# virtual methods
.method public onFrameEnd()V
    .locals 2

    .prologue
    .line 49
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;->mDrawFrameListener:Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;->onFrameEnd()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    :goto_0
    return-void

    .line 51
    :catch_0
    move-exception v0

    .line 53
    const-string v1, "SGDrawFrameListener::onFrameEnd error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGVIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 2

    .prologue
    .line 36
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerHolder;->mSizeChangeListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;->onResize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :goto_0
    return-void

    .line 38
    :catch_0
    move-exception v0

    .line 40
    const-string v1, "SGSurfaceSizeChangeListener::onResize error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGVIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method
