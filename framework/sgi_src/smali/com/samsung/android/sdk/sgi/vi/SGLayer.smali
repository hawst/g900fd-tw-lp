.class public Lcom/samsung/android/sdk/sgi/vi/SGLayer;
.super Ljava/lang/Object;
.source "SGLayer.java"


# instance fields
.field private mAnimationListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;

.field mAsyncScreenshotListenersArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected mChildArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/sgi/vi/SGLayer;",
            ">;"
        }
    .end annotation
.end field

.field protected mGeometryGenerator:Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;

.field protected swigCMemOwn:Z

.field protected swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 511
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGLayer()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;-><init>(JZ)V

    .line 512
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->init()V

    .line 513
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 5

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCMemOwn:Z

    .line 32
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mAsyncScreenshotListenersArray:Ljava/util/ArrayList;

    .line 35
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-virtual {v0, p0, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    .line 36
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mAnimationListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/graphics/RectF;)V
    .locals 3

    .prologue
    .line 57
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGLayer()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;-><init>(JZ)V

    .line 58
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->init(Landroid/graphics/RectF;)V

    .line 59
    return-void
.end method

.method private addL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I
    .locals 6

    .prologue
    .line 647
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_addL__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    move-result v0

    return v0
.end method

.method private addL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;I)I
    .locals 7

    .prologue
    .line 650
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_addL__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)I

    move-result v0

    return v0
.end method

.method private bringLayerToF(I)V
    .locals 2

    .prologue
    .line 656
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_bringLayerToF__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)V

    .line 657
    return-void
.end method

.method private bringLayerToF(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 6

    .prologue
    .line 659
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_bringLayerToF__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 660
    return-void
.end method

.method private bringToF()V
    .locals 2

    .prologue
    .line 653
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_bringToF(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 654
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J
    .locals 2

    .prologue
    .line 40
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    goto :goto_0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 515
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_init__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 516
    return-void
.end method

.method private init(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 518
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 520
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_init__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/RectF;)V

    .line 522
    return-void
.end method

.method private makeScreenShot(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V
    .locals 8

    .prologue
    .line 839
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)J

    move-result-wide v5

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v7, p3

    invoke-static/range {v0 .. v7}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_makeScreenShot__SWIG_7(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V

    .line 840
    return-void
.end method

.method private makeScreenShot(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V
    .locals 7

    .prologue
    .line 836
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_makeScreenShot__SWIG_6(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Bitmap;JLcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V

    .line 837
    return-void
.end method

.method private makeScreenShot(Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
    .locals 10

    .prologue
    .line 842
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)J

    move-result-wide v7

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    move-object v9, p3

    invoke-static/range {v0 .. v9}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_makeScreenShot__SWIG_8(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V

    .line 843
    return-void
.end method

.method private makeScreenShot(Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
    .locals 9

    .prologue
    .line 845
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_makeScreenShot__SWIG_9(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V

    .line 846
    return-void
.end method

.method private makeScreenShot(Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
    .locals 6

    .prologue
    .line 848
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_makeScreenShot__SWIG_10(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V

    .line 849
    return-void
.end method

.method private removeAllL()V
    .locals 2

    .prologue
    .line 683
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_removeAllL(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 684
    return-void
.end method

.method private removeL(I)V
    .locals 2

    .prologue
    .line 674
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_removeL__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)V

    .line 675
    return-void
.end method

.method private removeL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 6

    .prologue
    .line 671
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_removeL__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 672
    return-void
.end method

.method private sendLayerToB(I)V
    .locals 2

    .prologue
    .line 665
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_sendLayerToB__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)V

    .line 666
    return-void
.end method

.method private sendLayerToB(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 6

    .prologue
    .line 668
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_sendLayerToB__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 669
    return-void
.end method

.method private sendToB()V
    .locals 2

    .prologue
    .line 662
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_sendToB(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 663
    return-void
.end method

.method private setGeometryGeneratorNative(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V
    .locals 6

    .prologue
    .line 635
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setGeometryGeneratorNative(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V

    .line 636
    return-void
.end method

.method private setLayerAnimationListener(Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;)V
    .locals 6

    .prologue
    .line 761
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setLayerAnimationListener(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;)V

    .line 762
    return-void
.end method

.method private swapL(II)V
    .locals 2

    .prologue
    .line 680
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_swapL__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;II)V

    .line 681
    return-void
.end method

.method private swapL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 9

    .prologue
    .line 677
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_swapL__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 678
    return-void
.end method


# virtual methods
.method public addAnimation(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)I
    .locals 6

    .prologue
    .line 638
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_addAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I

    move-result v0

    return v0
.end method

.method public addAnimation(Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)V
    .locals 9

    .prologue
    .line 791
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_addAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)V

    .line 792
    return-void
.end method

.method public addFilter(Lcom/samsung/android/sdk/sgi/vi/SGFilter;)V
    .locals 6

    .prologue
    .line 767
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGFilter;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_addFilter(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGFilter;)V

    .line 768
    return-void
.end method

.method public addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I
    .locals 4

    .prologue
    .line 210
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter layer is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParent()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    .line 212
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParentWidget()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v1

    .line 213
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    move-result v2

    .line 214
    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 234
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    return v2

    .line 217
    :cond_2
    if-eqz v1, :cond_1

    .line 221
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v3, "mChildArrayLayer"

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 222
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 223
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 224
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 226
    :catch_0
    move-exception v0

    .line 228
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 229
    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/RuntimeException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 230
    throw v1
.end method

.method public addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;I)I
    .locals 4

    .prologue
    .line 248
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter layer is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 249
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParent()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    .line 250
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParentWidget()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v1

    .line 251
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;I)I

    move-result v2

    .line 252
    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 272
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 273
    return v2

    .line 255
    :cond_2
    if-eqz v1, :cond_1

    .line 259
    :try_start_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v3, "mChildArrayLayer"

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 260
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 261
    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 262
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 264
    :catch_0
    move-exception v0

    .line 266
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 267
    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/RuntimeException;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 268
    throw v1
.end method

.method public addProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 173
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 174
    return-void
.end method

.method public bringChildToBack(I)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 471
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->sendLayerToBack(I)V

    .line 472
    return-void
.end method

.method public bringChildToBack(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 478
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->sendLayerToBack(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 479
    return-void
.end method

.method public bringChildToFront(I)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 457
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->bringLayerToFront(I)V

    .line 458
    return-void
.end method

.method public bringChildToFront(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 464
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->bringLayerToFront(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 465
    return-void
.end method

.method public bringLayerToFront(I)V
    .locals 2

    .prologue
    .line 390
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 391
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParent()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v1

    .line 392
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->bringLayerToF(I)V

    .line 393
    iget-object v1, v1, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 394
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 395
    return-void
.end method

.method public bringLayerToFront(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 2

    .prologue
    .line 401
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter layer is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 402
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParent()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    .line 403
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->bringLayerToF(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 404
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 405
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 406
    return-void
.end method

.method public bringToFront()V
    .locals 2

    .prologue
    .line 367
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParent()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    .line 368
    if-eqz v0, :cond_1

    .line 370
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->bringToF()V

    .line 371
    iget-object v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 372
    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 384
    :cond_0
    :goto_0
    return-void

    .line 376
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParentWidget()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    .line 377
    if-eqz v0, :cond_0

    .line 379
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->bringToF()V

    .line 380
    new-instance v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;

    invoke-direct {v1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 381
    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->bringLayerToFront(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    goto :goto_0
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 43
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 44
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 45
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCMemOwn:Z

    .line 46
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->delete_SGLayer(J)V

    .line 48
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    .line 50
    :cond_1
    return-void
.end method

.method public findLayerByID(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 180
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->findLayerById(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    return-object v0
.end method

.method public findLayerById(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    .locals 2

    .prologue
    .line 806
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_findLayerById(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    return-object v0
.end method

.method public findLayerByName(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    .locals 2

    .prologue
    .line 803
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_findLayerByName(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    return-object v0
.end method

.method public getChildrenCount()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getLayersCount()I

    move-result v0

    return v0
.end method

.method public getFilter(I)Lcom/samsung/android/sdk/sgi/vi/SGFilter;
    .locals 4

    .prologue
    .line 770
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getFilter(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGFilter;-><init>(JZ)V

    return-object v0
.end method

.method public getFilterCount()I
    .locals 2

    .prologue
    .line 773
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getFilterCount(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    move-result v0

    return v0
.end method

.method public getFullTransform()Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 611
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getFullTransform(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public getGeometryGenerator()Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mGeometryGenerator:Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;

    return-object v0
.end method

.method public getGeometryGeneratorParam()F
    .locals 2

    .prologue
    .line 632
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getGeometryGeneratorParam(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)F

    move-result v0

    return v0
.end method

.method public getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    return-object v0
.end method

.method public getLayer(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    .locals 3

    .prologue
    .line 196
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 197
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 198
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLayerAnimationListener()Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListener;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mAnimationListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;->getInterface()Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListener;

    move-result-object v0

    return-object v0
.end method

.method public getLayerAtPoint(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    .locals 6

    .prologue
    .line 818
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getLayerAtPoint(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    return-object v0
.end method

.method public getLayerIndex(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getLayerPosition(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 353
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getLayerIndex(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    move-result v0

    return v0
.end method

.method public getLayersCount()I
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getLocalTransform()Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 602
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getLocalTransform(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 527
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getName(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOpacity()F
    .locals 2

    .prologue
    .line 590
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getOpacity(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)F

    move-result v0

    return v0
.end method

.method public getParent()Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    .locals 2

    .prologue
    .line 815
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getParent(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    return-object v0
.end method

.method public getParentBounds()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 584
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getParentBounds(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getParentWidget()Lcom/samsung/android/sdk/sgi/ui/SGWidget;
    .locals 2

    .prologue
    .line 812
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getParentWidget(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    return-object v0
.end method

.method public getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 548
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getPosition(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getPosition3f()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 551
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getPosition3f(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getPositionPivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 731
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getPositionPivot(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getPositionPivot3f()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 734
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getPositionPivot3f(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getProgramProperty()Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;
    .locals 4

    .prologue
    .line 824
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getProgramProperty(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    .line 825
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    .line 826
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(JZ)V

    .line 827
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getProperty(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 2

    .prologue
    .line 809
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getProperty(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public getRelativeToAnotherParentTransform(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 7

    .prologue
    .line 617
    new-instance v6, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getRelativeToAnotherParentTransform(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v6
.end method

.method public getRelativeTransform(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 7

    .prologue
    .line 614
    new-instance v6, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getRelativeTransform(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v6
.end method

.method public getRotation()Lcom/samsung/android/sdk/sgi/base/SGQuaternion;
    .locals 4

    .prologue
    .line 569
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getRotation(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    return-object v0
.end method

.method public getRotation3f()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 572
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getRotation3f(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getRotationPivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 743
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getRotationPivot(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getRotationPivot3f()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 746
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getRotationPivot3f(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getScale()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 575
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getScale(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getScale3f()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 578
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getScale3f(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getScalePivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 755
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getScalePivot(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getScalePivot3f()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 758
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getScalePivot3f(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getScreenBounds()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 581
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getScreenBounds(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 539
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getSize(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;
    .locals 2

    .prologue
    .line 821
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getSurface(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    return-object v0
.end method

.method public getTransformationHint()Lcom/samsung/android/sdk/sgi/vi/SGTransformationHints;
    .locals 4

    .prologue
    .line 533
    const-class v0, Lcom/samsung/android/sdk/sgi/vi/SGTransformationHints;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/vi/SGTransformationHints;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getTransformationHint(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getVisibility()Z
    .locals 2

    .prologue
    .line 608
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_getVisibility(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z

    move-result v0

    return v0
.end method

.method public isAddedToSuface()Z
    .locals 2

    .prologue
    .line 764
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_isAddedToSuface(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z

    move-result v0

    return v0
.end method

.method public isChildrenClipStateEnabled()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 492
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->isChildrenClippingEnabled()Z

    move-result v0

    return v0
.end method

.method public isChildrenClippingEnabled()Z
    .locals 2

    .prologue
    .line 689
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_isChildrenClippingEnabled(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z

    move-result v0

    return v0
.end method

.method public isFiltersAttachDepthBuffer()Z
    .locals 2

    .prologue
    .line 797
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_isFiltersAttachDepthBuffer(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z

    move-result v0

    return v0
.end method

.method public isFiltersAttachStencilBuffer()Z
    .locals 2

    .prologue
    .line 800
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_isFiltersAttachStencilBuffer(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z

    move-result v0

    return v0
.end method

.method public isHasLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 312
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isInTransition()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 166
    const/4 v0, 0x0

    return v0
.end method

.method public isInheritOpacity()Z
    .locals 2

    .prologue
    .line 596
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_isInheritOpacity(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z

    move-result v0

    return v0
.end method

.method public isScreenShotAttachDepthBuffer()Z
    .locals 2

    .prologue
    .line 695
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_isScreenShotAttachDepthBuffer(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z

    move-result v0

    return v0
.end method

.method public isScreenShotAttachStencilBuffer()Z
    .locals 2

    .prologue
    .line 698
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_isScreenShotAttachStencilBuffer(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z

    move-result v0

    return v0
.end method

.method public makeScreenShot()Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 2

    .prologue
    .line 701
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_makeScreenShot__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public makeScreenShot(Landroid/graphics/Rect;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 2

    .prologue
    .line 707
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_makeScreenShot__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Rect;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public makeScreenShot(Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 7

    .prologue
    .line 710
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_makeScreenShot__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public makeScreenShot(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 6

    .prologue
    .line 704
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_makeScreenShot__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public makeScreenShot(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 830
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_makeScreenShot__SWIG_4(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Bitmap;)V

    .line 831
    return-void
.end method

.method public makeScreenShot(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 833
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_makeScreenShot__SWIG_5(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V

    .line 834
    return-void
.end method

.method public makeScreenShot(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;)V
    .locals 2

    .prologue
    .line 99
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;

    invoke-direct {v0, p0, p3}, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGLayer;Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;)V

    .line 102
    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->makeScreenShot(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    const/4 v0, 0x0

    .line 107
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->detachListener()V

    .line 109
    :cond_0
    return-void

    .line 107
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public makeScreenShot(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;)V
    .locals 2

    .prologue
    .line 81
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGLayer;Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;)V

    .line 84
    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->makeScreenShot(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    const/4 v0, 0x0

    .line 89
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->detachListener()V

    .line 91
    :cond_0
    return-void

    .line 89
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public makeScreenShot(Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V
    .locals 2

    .prologue
    .line 150
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;

    invoke-direct {v0, p0, p3}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGLayer;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V

    .line 153
    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->makeScreenShot(Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    const/4 v0, 0x0

    .line 158
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;->detachListener()V

    .line 160
    :cond_0
    return-void

    .line 158
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public makeScreenShot(Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V
    .locals 2

    .prologue
    .line 132
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGLayer;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V

    .line 135
    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->makeScreenShot(Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    const/4 v0, 0x0

    .line 140
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;->detachListener()V

    .line 142
    :cond_0
    return-void

    .line 140
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public makeScreenShot(Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V
    .locals 2

    .prologue
    .line 115
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGLayer;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V

    .line 118
    :try_start_0
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->makeScreenShot(Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    const/4 v0, 0x0

    .line 123
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;->detachListener()V

    .line 125
    :cond_0
    return-void

    .line 123
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public removeAllAnimations()V
    .locals 2

    .prologue
    .line 641
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_removeAllAnimations(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 642
    return-void
.end method

.method public removeAllFilters()V
    .locals 2

    .prologue
    .line 782
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_removeAllFilters(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 783
    return-void
.end method

.method public removeAllLayers()V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 303
    :goto_0
    return-void

    .line 301
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->removeAllL()V

    .line 302
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public removeAnimation(I)V
    .locals 2

    .prologue
    .line 644
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_removeAnimation(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)V

    .line 645
    return-void
.end method

.method public removeFilter(I)V
    .locals 2

    .prologue
    .line 779
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_removeFilter__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)V

    .line 780
    return-void
.end method

.method public removeFilter(Lcom/samsung/android/sdk/sgi/vi/SGFilter;)V
    .locals 6

    .prologue
    .line 776
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGFilter;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_removeFilter__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGFilter;)V

    .line 777
    return-void
.end method

.method public removeLayer(I)V
    .locals 1

    .prologue
    .line 290
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->removeL(I)V

    .line 291
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 292
    return-void
.end method

.method public removeLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 1

    .prologue
    .line 281
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->removeL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 282
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 283
    return-void
.end method

.method public removeProperty(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 623
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_removeProperty(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Ljava/lang/String;)V

    .line 624
    return-void
.end method

.method public resetFilterFrameBufferSize()V
    .locals 2

    .prologue
    .line 788
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_resetFilterFrameBufferSize(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 789
    return-void
.end method

.method public sendLayerToBack(I)V
    .locals 3

    .prologue
    .line 435
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 436
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParent()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v1

    .line 437
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->sendLayerToB(I)V

    .line 438
    iget-object v1, v1, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 439
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 440
    return-void
.end method

.method public sendLayerToBack(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 2

    .prologue
    .line 446
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter layer is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 447
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParent()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    .line 448
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->sendLayerToB(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 449
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 450
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 451
    return-void
.end method

.method public sendToBack()V
    .locals 2

    .prologue
    .line 412
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParent()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    .line 413
    if-eqz v0, :cond_1

    .line 415
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->sendToB()V

    .line 416
    iget-object v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 417
    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 429
    :cond_0
    :goto_0
    return-void

    .line 421
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getParentWidget()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    .line 422
    if-eqz v0, :cond_0

    .line 424
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->sendToB()V

    .line 425
    new-instance v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;

    invoke-direct {v1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 426
    invoke-virtual {v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;->sendLayerToBack(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    goto :goto_0
.end method

.method public setChildrenClipState(Z)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 485
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->setChildrenClipping(Z)V

    .line 486
    return-void
.end method

.method public setChildrenClipping(Z)V
    .locals 2

    .prologue
    .line 686
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setChildrenClipping(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Z)V

    .line 687
    return-void
.end method

.method public setFilterFrameBufferSize(FF)V
    .locals 2

    .prologue
    .line 785
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setFilterFrameBufferSize(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V

    .line 786
    return-void
.end method

.method public setFiltersOptions(ZZ)V
    .locals 2

    .prologue
    .line 794
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setFiltersOptions(JLcom/samsung/android/sdk/sgi/vi/SGLayer;ZZ)V

    .line 795
    return-void
.end method

.method public setGeometryGenerator(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V
    .locals 0

    .prologue
    .line 500
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->setGeometryGeneratorNative(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V

    .line 501
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mGeometryGenerator:Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;

    .line 502
    return-void
.end method

.method public setGeometryGeneratorParam(F)V
    .locals 2

    .prologue
    .line 629
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setGeometryGeneratorParam(JLcom/samsung/android/sdk/sgi/vi/SGLayer;F)V

    .line 630
    return-void
.end method

.method public setInheritOpacity(Z)V
    .locals 2

    .prologue
    .line 593
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setInheritOpacity(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Z)V

    .line 594
    return-void
.end method

.method public setLayerAnimationListener(Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListener;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mAnimationListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;->setInterface(Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListener;)V

    .line 66
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->setLayerAnimationListener(Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;)V

    .line 67
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mAnimationListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;

    goto :goto_0
.end method

.method public setLocalTransform(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
    .locals 6

    .prologue
    .line 599
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setLocalTransform(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 600
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 524
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setName(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Ljava/lang/String;)V

    .line 525
    return-void
.end method

.method public setOpacity(F)V
    .locals 2

    .prologue
    .line 587
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setOpacity(JLcom/samsung/android/sdk/sgi/vi/SGLayer;F)V

    .line 588
    return-void
.end method

.method public setPivots(FF)V
    .locals 2

    .prologue
    .line 884
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setPivots__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V

    .line 885
    return-void
.end method

.method public setPivots(FFF)V
    .locals 6

    .prologue
    .line 887
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setPivots__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FFF)V

    .line 888
    return-void
.end method

.method public setPivots(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 719
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setPivots__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 720
    return-void
.end method

.method public setPivots(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 722
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setPivots__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 723
    return-void
.end method

.method public setPosition(FF)V
    .locals 2

    .prologue
    .line 851
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setPosition__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V

    .line 852
    return-void
.end method

.method public setPosition(FFF)V
    .locals 6

    .prologue
    .line 854
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setPosition__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FFF)V

    .line 855
    return-void
.end method

.method public setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 542
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setPosition__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 543
    return-void
.end method

.method public setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 545
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setPosition__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 546
    return-void
.end method

.method public setPositionPivot(FF)V
    .locals 2

    .prologue
    .line 878
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setPositionPivot__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V

    .line 879
    return-void
.end method

.method public setPositionPivot(FFF)V
    .locals 6

    .prologue
    .line 881
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setPositionPivot__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FFF)V

    .line 882
    return-void
.end method

.method public setPositionPivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 725
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setPositionPivot__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 726
    return-void
.end method

.method public setPositionPivot(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 728
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setPositionPivot__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 729
    return-void
.end method

.method public setProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V
    .locals 6

    .prologue
    .line 626
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setProgramProperty(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V

    .line 627
    return-void
.end method

.method public setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    .locals 7

    .prologue
    .line 620
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/render/SGProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setProperty(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 621
    return-void
.end method

.method public setRotation(FFF)V
    .locals 6

    .prologue
    .line 557
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setRotation__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FFF)V

    .line 558
    return-void
.end method

.method public setRotation(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
    .locals 6

    .prologue
    .line 554
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setRotation__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 555
    return-void
.end method

.method public setRotationPivot(FF)V
    .locals 2

    .prologue
    .line 872
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setRotationPivot__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V

    .line 873
    return-void
.end method

.method public setRotationPivot(FFF)V
    .locals 6

    .prologue
    .line 875
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setRotationPivot__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FFF)V

    .line 876
    return-void
.end method

.method public setRotationPivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 737
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setRotationPivot__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 738
    return-void
.end method

.method public setRotationPivot(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 740
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setRotationPivot__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 741
    return-void
.end method

.method public setRotationX(F)V
    .locals 2

    .prologue
    .line 560
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setRotationX(JLcom/samsung/android/sdk/sgi/vi/SGLayer;F)V

    .line 561
    return-void
.end method

.method public setRotationY(F)V
    .locals 2

    .prologue
    .line 563
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setRotationY(JLcom/samsung/android/sdk/sgi/vi/SGLayer;F)V

    .line 564
    return-void
.end method

.method public setRotationZ(F)V
    .locals 2

    .prologue
    .line 566
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setRotationZ(JLcom/samsung/android/sdk/sgi/vi/SGLayer;F)V

    .line 567
    return-void
.end method

.method public setScale(FF)V
    .locals 2

    .prologue
    .line 860
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setScale__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V

    .line 861
    return-void
.end method

.method public setScale(FFF)V
    .locals 6

    .prologue
    .line 863
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setScale__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FFF)V

    .line 864
    return-void
.end method

.method public setScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 713
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setScale__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 714
    return-void
.end method

.method public setScale(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 716
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setScale__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 717
    return-void
.end method

.method public setScalePivot(FF)V
    .locals 2

    .prologue
    .line 866
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setScalePivot__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V

    .line 867
    return-void
.end method

.method public setScalePivot(FFF)V
    .locals 6

    .prologue
    .line 869
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setScalePivot__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FFF)V

    .line 870
    return-void
.end method

.method public setScalePivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 749
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setScalePivot__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 750
    return-void
.end method

.method public setScalePivot(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 752
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setScalePivot__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 753
    return-void
.end method

.method public setScreenShotOptions(ZZ)V
    .locals 2

    .prologue
    .line 692
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setScreenShotOptions(JLcom/samsung/android/sdk/sgi/vi/SGLayer;ZZ)V

    .line 693
    return-void
.end method

.method public setSize(FF)V
    .locals 2

    .prologue
    .line 857
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setSize__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V

    .line 858
    return-void
.end method

.method public setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 536
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setSize__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 537
    return-void
.end method

.method public setTransformationHint(Lcom/samsung/android/sdk/sgi/vi/SGTransformationHints;)V
    .locals 3

    .prologue
    .line 530
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGTransformationHints;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setTransformationHint(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)V

    .line 531
    return-void
.end method

.method public setVisibility(Z)V
    .locals 2

    .prologue
    .line 605
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayer_setVisibility(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Z)V

    .line 606
    return-void
.end method

.method public swapLayers(II)V
    .locals 1

    .prologue
    .line 337
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swapL(II)V

    .line 338
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-static {v0, p1, p2}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 339
    return-void
.end method

.method public swapLayers(Lcom/samsung/android/sdk/sgi/vi/SGLayer;Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 3

    .prologue
    .line 326
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->swapL(Lcom/samsung/android/sdk/sgi/vi/SGLayer;Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 327
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 328
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 329
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->mChildArray:Ljava/util/ArrayList;

    invoke-static {v2, v0, v1}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 330
    return-void
.end method
