.class public Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGProperty;
.source "SGMatrix4fProperty.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 50
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGMatrix4fProperty()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;-><init>(JZ)V

    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->init()V

    .line 52
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
    .locals 3

    .prologue
    .line 44
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGMatrix4fProperty()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;-><init>(JZ)V

    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->init(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 46
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGMatrix4fProperty_init__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;)V

    .line 56
    return-void
.end method

.method private init(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
    .locals 6

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGMatrix4fProperty_init__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 60
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;)Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;
    .locals 7

    .prologue
    .line 75
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGMatrix4fProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;JLcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGMatrix4fProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public get()Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 71
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGMatrix4fProperty_get(JLcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public set(FFFFFFFFFFFFFFFF)V
    .locals 21

    .prologue
    .line 67
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->swigCPtr:J

    move-object/from16 v4, p0

    move/from16 v5, p1

    move/from16 v6, p2

    move/from16 v7, p3

    move/from16 v8, p4

    move/from16 v9, p5

    move/from16 v10, p6

    move/from16 v11, p7

    move/from16 v12, p8

    move/from16 v13, p9

    move/from16 v14, p10

    move/from16 v15, p11

    move/from16 v16, p12

    move/from16 v17, p13

    move/from16 v18, p14

    move/from16 v19, p15

    move/from16 v20, p16

    invoke-static/range {v2 .. v20}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGMatrix4fProperty_set__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;FFFFFFFFFFFFFFFF)V

    .line 68
    return-void
.end method

.method public set(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
    .locals 6

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGMatrix4fProperty_set__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 64
    return-void
.end method
