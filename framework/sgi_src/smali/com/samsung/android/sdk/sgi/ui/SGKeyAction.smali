.class public final enum Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;
.super Ljava/lang/Enum;
.source "SGKeyAction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

.field public static final enum ON_KEY_DOWN:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

.field public static final enum ON_KEY_MULTIPLE:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

.field public static final enum ON_KEY_UP:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

.field public static final enum UNKNOWN:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    const-string v1, "ON_KEY_DOWN"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ON_KEY_DOWN:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    const-string v1, "ON_KEY_UP"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ON_KEY_UP:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    const-string v1, "ON_KEY_MULTIPLE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ON_KEY_MULTIPLE:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->UNKNOWN:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    .line 18
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ON_KEY_DOWN:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ON_KEY_UP:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->ON_KEY_MULTIPLE:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->UNKNOWN:Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->$VALUES:[Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->$VALUES:[Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/ui/SGKeyAction;

    return-object v0
.end method
