.class public abstract Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;
.super Lcom/samsung/android/sdk/sgi/ui/SGWidget;
.source "SGWidgetCanvas.java"


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 90
    invoke-static {}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->new_SGWidgetCanvas()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;-><init>(JZ)V

    .line 91
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCMemOwn:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_director_connect(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;JZZ)V

    .line 92
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;-><init>()V

    .line 85
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->init(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/graphics/Bitmap$Config;)V

    .line 86
    return-void
.end method

.method public constructor <init>(FFLandroid/graphics/Bitmap$Config;)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;-><init>()V

    .line 76
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-direct {p0, v0, p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->init(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/graphics/Bitmap$Config;)V

    .line 77
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;-><init>(JZ)V

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;-><init>()V

    .line 51
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->init(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/graphics/Bitmap$Config;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/graphics/Bitmap$Config;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;-><init>()V

    .line 63
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->init(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/graphics/Bitmap$Config;)V

    .line 64
    return-void
.end method

.method private init(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 99
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_init__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 100
    return-void
.end method

.method private init(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/graphics/Bitmap$Config;)V
    .locals 7

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_init__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/graphics/Bitmap$Config;)V

    .line 96
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 34
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 35
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCMemOwn:Z

    .line 37
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->delete_SGWidgetCanvas(J)V

    .line 39
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    .line 41
    :cond_1
    return-void
.end method

.method public getCanvasScale()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 114
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_getCanvasScale(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getContentRect()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 138
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_getContentRect(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getContentRectPivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 146
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_getContentRectPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getContentRectScale()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 154
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_getContentRectScale(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getFormat()Landroid/graphics/Bitmap$Config;
    .locals 2

    .prologue
    .line 127
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_getFormat(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)Landroid/graphics/Bitmap$Config;

    move-result-object v0

    return-object v0
.end method

.method public invalidate()V
    .locals 2

    .prologue
    .line 103
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_invalidate__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)V

    .line 104
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_invalidateSwigExplicitSGWidgetCanvas__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)V

    goto :goto_0
.end method

.method public invalidate(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 107
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;

    if-ne v0, v1, :cond_1

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_invalidate__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;Landroid/graphics/RectF;)V

    .line 111
    :goto_0
    return-void

    .line 109
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_invalidateSwigExplicitSGWidgetCanvas__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method public abstract onDraw(Landroid/graphics/Canvas;)V
.end method

.method public setCanvasScale(FF)V
    .locals 2

    .prologue
    .line 160
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_setCanvasScale__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;FF)V

    .line 161
    return-void
.end method

.method public setCanvasScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 118
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_setCanvasScale__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 119
    :goto_0
    return-void

    .line 118
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_setCanvasScaleSwigExplicitSGWidgetCanvas__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    goto :goto_0
.end method

.method public setContentRect(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 131
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_setContentRect(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;Landroid/graphics/RectF;)V

    .line 135
    return-void
.end method

.method public setContentRectPivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 142
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_setContentRectPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 143
    return-void
.end method

.method public setContentRectScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 150
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_setContentRectScale(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 151
    return-void
.end method

.method public setFormat(Landroid/graphics/Bitmap$Config;)V
    .locals 2

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetCanvas_setFormat(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;Landroid/graphics/Bitmap$Config;)V

    .line 123
    return-void
.end method
