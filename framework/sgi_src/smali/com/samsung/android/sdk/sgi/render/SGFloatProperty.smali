.class public Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGProperty;
.source "SGFloatProperty.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 50
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGFloatProperty()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;-><init>(JZ)V

    .line 51
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->init()V

    .line 52
    return-void
.end method

.method public constructor <init>(F)V
    .locals 3

    .prologue
    .line 44
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGFloatProperty()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;-><init>(JZ)V

    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->init(F)V

    .line 46
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 26
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGFloatProperty_init__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGFloatProperty;)V

    .line 56
    return-void
.end method

.method private init(F)V
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGFloatProperty_init__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGFloatProperty;F)V

    .line 60
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;)Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;
    .locals 7

    .prologue
    .line 71
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGFloatProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGFloatProperty;JLcom/samsung/android/sdk/sgi/render/SGFloatProperty;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGFloatProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public get()F
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGFloatProperty_get(JLcom/samsung/android/sdk/sgi/render/SGFloatProperty;)F

    move-result v0

    return v0
.end method

.method public set(F)V
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGFloatProperty_set(JLcom/samsung/android/sdk/sgi/render/SGFloatProperty;F)V

    .line 64
    return-void
.end method
