.class final Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;
.super Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;
.source "SGLayerAnimationListenerHolder.java"


# instance fields
.field mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

.field mListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListener;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 31
    return-void
.end method


# virtual methods
.method public getInterface()Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListener;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListener;

    return-object v0
.end method

.method public onFinished(J)V
    .locals 2

    .prologue
    .line 51
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListener;->onFinished(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :goto_0
    return-void

    .line 53
    :catch_0
    move-exception v0

    .line 55
    const-string v1, "SGLayerAnimationListener::onFinished error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGVIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStarted(J)V
    .locals 2

    .prologue
    .line 38
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListener;->onStarted(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 42
    const-string v1, "SGLayerAnimationListener::onStarted error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGVIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setInterface(Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListener;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListener;

    .line 67
    return-void
.end method
