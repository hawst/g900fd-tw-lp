.class public Lcom/samsung/android/sdk/sgi/vi/SGLayerText;
.super Lcom/samsung/android/sdk/sgi/vi/SGLayer;
.source "SGLayerText.java"


# static fields
.field public static final TEXT_DIRECTION_LTR:I = 0x3

.field public static final TEXT_DIRECTION_RTL:I = 0x4


# instance fields
.field private final BitmapMaxSize:I

.field private final MaxBitmapWidth:I

.field private final MaxTextSize:F

.field private mText:Ljava/lang/CharSequence;

.field private mTextHash:[J

.field private mTextPaint:Landroid/graphics/Paint;

.field private mUseLocalCache:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 415
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGLayerText()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;-><init>(JZ)V

    .line 416
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;-><init>(JZ)V

    .line 44
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    .line 45
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextHash:[J

    .line 46
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mText:Ljava/lang/CharSequence;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mUseLocalCache:Z

    .line 50
    const/16 v0, 0x80

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->BitmapMaxSize:I

    .line 51
    const/16 v0, 0x800

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->MaxBitmapWidth:I

    .line 52
    const v0, 0x447fc000    # 1023.0f

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->MaxTextSize:F

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/graphics/RectF;)V
    .locals 3

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;-><init>()V

    .line 63
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 64
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    iget v2, p1, Landroid/graphics/RectF;->top:F

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 65
    return-void
.end method

.method private addGlyph(J[BIII)Z
    .locals 9

    .prologue
    .line 371
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    move-object v2, p0

    move-wide v3, p1

    move-object v5, p3

    move v6, p4

    move v7, p5

    move v8, p6

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerText_addGlyph(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;J[BIII)Z

    move-result v0

    return v0
.end method

.method private cacheGlyphs(Ljava/lang/CharSequence;)V
    .locals 21

    .prologue
    .line 168
    if-eqz p1, :cond_0

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 294
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextHash:[J

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextHash:[J

    array-length v2, v2

    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-eq v2, v3, :cond_3

    .line 172
    :cond_2
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    new-array v2, v2, [J

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextHash:[J

    .line 174
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 175
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 176
    const/4 v2, 0x0

    :goto_1
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-ge v2, v5, :cond_5

    .line 178
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    .line 179
    const/4 v6, -0x1

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v7

    if-ne v6, v7, :cond_4

    .line 181
    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 176
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 186
    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 187
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v12

    .line 188
    new-array v13, v12, [J

    .line 189
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v12, :cond_6

    .line 191
    invoke-virtual {v11, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->calculateHash(C)J

    move-result-wide v4

    aput-wide v4, v13, v2

    .line 189
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 194
    :cond_6
    const/4 v2, 0x0

    :goto_3
    invoke-interface/range {p1 .. p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ge v2, v3, :cond_7

    .line 197
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextHash:[J

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->calculateHash(C)J

    move-result-wide v4

    aput-wide v4, v3, v2

    .line 194
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 200
    :cond_7
    new-array v14, v12, [B

    .line 201
    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14, v12}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->isGlyphsCached([J[BI)V

    .line 202
    const/4 v2, 0x0

    .line 204
    const/4 v3, 0x0

    :goto_4
    if-ge v3, v12, :cond_8

    .line 206
    aget-byte v4, v14, v3

    if-nez v4, :cond_9

    .line 208
    const/4 v2, 0x1

    .line 213
    :cond_8
    if-nez v2, :cond_a

    .line 216
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mUseLocalCache:Z

    if-eqz v2, :cond_0

    .line 218
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mUseLocalCache:Z

    .line 219
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->enableLocalCache(III)V

    goto/16 :goto_0

    .line 204
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 224
    :cond_a
    const/4 v5, 0x0

    .line 225
    const/4 v4, 0x0

    .line 232
    new-array v15, v12, [Lcom/samsung/android/sdk/sgi/vi/SGLayerText$1GlyphSize;

    .line 234
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Paint$FontMetrics;->top:F

    neg-float v2, v2

    float-to-int v0, v2

    move/from16 v16, v0

    .line 236
    const/4 v3, 0x0

    .line 237
    const/4 v2, 0x0

    move/from16 v19, v2

    move v2, v4

    move/from16 v4, v19

    move/from16 v20, v3

    move v3, v5

    move/from16 v5, v20

    :goto_5
    if-ge v4, v12, :cond_c

    .line 239
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    add-int/lit8 v7, v4, 0x1

    invoke-virtual {v6, v11, v4, v7}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    move-result v6

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v6, v6

    .line 240
    add-int/2addr v5, v6

    .line 241
    aget-byte v7, v14, v4

    if-eqz v7, :cond_b

    .line 237
    :goto_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 243
    :cond_b
    new-instance v7, Lcom/samsung/android/sdk/sgi/vi/SGLayerText$1GlyphSize;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText$1GlyphSize;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGLayerText;)V

    aput-object v7, v15, v4

    .line 244
    aget-object v7, v15, v4

    iput v6, v7, Lcom/samsung/android/sdk/sgi/vi/SGLayerText$1GlyphSize;->mMeasuredWidth:I

    .line 245
    aget-object v7, v15, v4

    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    iput-object v8, v7, Lcom/samsung/android/sdk/sgi/vi/SGLayerText$1GlyphSize;->mRect:Landroid/graphics/Rect;

    .line 246
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    add-int/lit8 v8, v4, 0x1

    aget-object v9, v15, v4

    iget-object v9, v9, Lcom/samsung/android/sdk/sgi/vi/SGLayerText$1GlyphSize;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v7, v11, v4, v8, v9}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 247
    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 248
    aget-object v6, v15, v4

    iget-object v6, v6, Lcom/samsung/android/sdk/sgi/vi/SGLayerText$1GlyphSize;->mRect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int v6, v6, v16

    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_6

    .line 250
    :cond_c
    add-int/lit8 v2, v2, 0x1

    .line 251
    const/16 v4, 0x80

    if-le v2, v4, :cond_d

    const/4 v4, 0x1

    .line 252
    :goto_7
    if-eqz v4, :cond_e

    .line 254
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mUseLocalCache:Z

    .line 256
    const/4 v4, 0x1

    div-int/lit8 v6, v5, 0x2

    add-int/2addr v5, v6

    mul-int/lit8 v6, v2, 0x2

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->enableLocalCache(III)V

    move v9, v3

    .line 270
    :goto_8
    sget-object v3, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v9, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v17

    .line 271
    new-instance v18, Landroid/graphics/Canvas;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 273
    const/4 v3, 0x0

    move v10, v3

    :goto_9
    if-ge v10, v12, :cond_0

    .line 275
    invoke-virtual {v11, v10}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 277
    aget-byte v4, v14, v10

    if-eqz v4, :cond_10

    .line 273
    :goto_a
    add-int/lit8 v3, v10, 0x1

    move v10, v3

    goto :goto_9

    .line 251
    :cond_d
    const/4 v4, 0x0

    goto :goto_7

    .line 260
    :cond_e
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mUseLocalCache:Z

    if-eqz v4, :cond_f

    .line 262
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mUseLocalCache:Z

    .line 263
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->enableLocalCache(III)V

    .line 266
    :cond_f
    const/16 v4, 0x80

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 267
    const/16 v3, 0x80

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_8

    .line 282
    :cond_10
    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 284
    aget-object v4, v15, v10

    iget v4, v4, Lcom/samsung/android/sdk/sgi/vi/SGLayerText$1GlyphSize;->mMeasuredWidth:I

    const/4 v5, 0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 285
    aget-object v4, v15, v10

    iget-object v4, v4, Lcom/samsung/android/sdk/sgi/vi/SGLayerText$1GlyphSize;->mRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    add-int v4, v4, v16

    const/4 v5, 0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 288
    invoke-static {v3}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move/from16 v0, v16

    int-to-float v5, v0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 289
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 290
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 292
    aget-wide v4, v13, v10

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    invoke-static {v7, v9}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-static {v8, v2}, Ljava/lang/Math;->min(II)I

    move-result v8

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->addGlyph(J[BIII)Z

    goto :goto_a
.end method

.method private calculateHash(C)J
    .locals 4

    .prologue
    .line 302
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, -0x1

    int-to-long v0, v0

    .line 303
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getTextSize()F

    move-result v2

    float-to-int v2, v2

    .line 304
    const/16 v3, 0x20

    shl-long/2addr v0, v3

    shl-int/lit8 v2, v2, 0x10

    add-int/2addr v2, p1

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 305
    return-wide v0
.end method

.method private enableLocalCache(III)V
    .locals 6

    .prologue
    .line 383
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerText_enableLocalCache(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;III)V

    .line 384
    return-void
.end method

.method private setSelectionNative(II)V
    .locals 2

    .prologue
    .line 387
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerText_setSelectionNative(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;II)V

    .line 388
    return-void
.end method

.method private setTextNative([JI)V
    .locals 2

    .prologue
    .line 359
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerText_setTextNative(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;[JI)V

    .line 360
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 34
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 35
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCMemOwn:Z

    .line 37
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->delete_SGLayerText(J)V

    .line 39
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    .line 41
    :cond_1
    return-void
.end method

.method public getSelectionBackgroundColor()I
    .locals 2

    .prologue
    .line 403
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerText_getSelectionBackgroundColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;)I

    move-result v0

    return v0
.end method

.method public getSelectionTextColor()I
    .locals 2

    .prologue
    .line 395
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerText_getSelectionTextColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;)I

    move-result v0

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mText:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTextBounds(Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 144
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-nez v0, :cond_0

    .line 145
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Typeface is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mText:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mText:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, p1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 150
    :goto_0
    return-void

    .line 149
    :cond_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v3, v3, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method

.method public getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-nez v0, :cond_0

    .line 161
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Typeface is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 164
    return-void
.end method

.method public getTextColor()I
    .locals 2

    .prologue
    .line 367
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerText_getTextColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;)I

    move-result v0

    return v0
.end method

.method public getTextDirection()I
    .locals 2

    .prologue
    .line 411
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerText_getTextDirection(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;)I

    move-result v0

    return v0
.end method

.method public getTextPaint()Landroid/graphics/Paint;
    .locals 2

    .prologue
    .line 94
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 95
    return-object v0
.end method

.method public invalidate()V
    .locals 0

    .prologue
    .line 137
    return-void
.end method

.method public isGlyphCached(J)Z
    .locals 3

    .prologue
    .line 375
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerText_isGlyphCached(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;J)Z

    move-result v0

    return v0
.end method

.method public isGlyphsCached([J[BI)V
    .locals 6

    .prologue
    .line 379
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerText_isGlyphsCached(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;[J[BI)V

    .line 380
    return-void
.end method

.method public setSelectionBackgroundColor(I)V
    .locals 2

    .prologue
    .line 399
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerText_setSelectionBackgroundColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;I)V

    .line 400
    return-void
.end method

.method public setSelectionTextColor(I)V
    .locals 2

    .prologue
    .line 391
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerText_setSelectionTextColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;I)V

    .line 392
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 104
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-nez v0, :cond_2

    .line 106
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Typeface is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v1

    .line 109
    :goto_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-ge v0, v3, :cond_4

    .line 111
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v3

    .line 112
    invoke-static {v3}, Ljava/lang/Character;->isDefined(C)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v3}, Ljava/lang/Character;->isISOControl(C)Z

    move-result v4

    if-nez v4, :cond_3

    .line 114
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 109
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 118
    :cond_4
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mText:Ljava/lang/CharSequence;

    .line 119
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mText:Ljava/lang/CharSequence;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->cacheGlyphs(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextHash:[J

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextHash:[J

    array-length v0, v0

    if-lez v0, :cond_5

    .line 122
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextHash:[J

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextHash:[J

    array-length v2, v2

    invoke-direct {p0, v0, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->setTextNative([JI)V

    .line 124
    :cond_5
    invoke-virtual {p0, v1, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->setTextSelection(II)V

    goto :goto_0
.end method

.method public setTextColor(I)V
    .locals 2

    .prologue
    .line 363
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerText_setTextColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;I)V

    .line 364
    return-void
.end method

.method public setTextDirection(I)V
    .locals 2

    .prologue
    .line 407
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerText_setTextDirection(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;I)V

    .line 408
    return-void
.end method

.method public setTextPaint(Landroid/graphics/Paint;)V
    .locals 2

    .prologue
    .line 73
    if-nez p1, :cond_0

    .line 74
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Paint is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-nez v0, :cond_1

    .line 77
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Typeface is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    const v1, 0x447fc000    # 1023.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 80
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Text size is too big"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->set(Landroid/graphics/Paint;)V

    .line 83
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 86
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mText:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->setText(Ljava/lang/CharSequence;)V

    .line 87
    return-void
.end method

.method public setTextSelection(II)V
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextHash:[J

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextHash:[J

    array-length v0, v0

    if-nez v0, :cond_1

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 316
    :cond_1
    if-ltz p2, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextHash:[J

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 318
    add-int v0, p1, p2

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextHash:[J

    array-length v1, v1

    if-lt v0, v1, :cond_2

    .line 319
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextHash:[J

    array-length v0, v0

    sub-int p2, v0, p1

    .line 322
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->setSelectionNative(II)V

    goto :goto_0
.end method

.method public trimTextToFitWidth(I)I
    .locals 2

    .prologue
    .line 330
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mText:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->trimTextToFitWidth(Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public trimTextToFitWidth(Ljava/lang/String;II)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 340
    .line 342
    const-string v0, ""

    move v0, v1

    move v3, v1

    .line 343
    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    .line 344
    add-int v0, p2, v3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-gt v0, v4, :cond_2

    .line 345
    add-int v0, p2, v3

    invoke-virtual {p1, p2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 346
    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerText;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    if-ge v0, p3, :cond_1

    move v0, v1

    .line 347
    :goto_1
    if-eqz v0, :cond_0

    .line 348
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 346
    goto :goto_1

    :cond_2
    move v0, v2

    .line 351
    goto :goto_0

    .line 353
    :cond_3
    return v3
.end method
