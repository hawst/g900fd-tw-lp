.class public Lcom/samsung/android/sdk/sgi/render/SGBuffer;
.super Ljava/lang/Object;
.source "SGBuffer.java"


# instance fields
.field protected buffer:Ljava/nio/ByteBuffer;

.field protected swigCMemOwn:Z

.field protected swigCPtr:J


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    .line 80
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGBuffer()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBuffer;-><init>(JZ)V

    .line 81
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->buffer:Ljava/nio/ByteBuffer;

    .line 26
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->swigCMemOwn:Z

    .line 27
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->swigCPtr:J

    .line 28
    return-void
.end method

.method private getBuffer()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBuffer_getBuffer(JLcom/samsung/android/sdk/sgi/render/SGBuffer;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/render/SGBuffer;)J
    .locals 2

    .prologue
    .line 31
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->swigCPtr:J

    goto :goto_0
.end method

.method private getHandle()J
    .locals 2

    .prologue
    .line 92
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBuffer_getHandle(JLcom/samsung/android/sdk/sgi/render/SGBuffer;)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 64
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/samsung/android/sdk/sgi/render/SGBuffer;

    if-nez v1, :cond_1

    .line 65
    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/samsung/android/sdk/sgi/render/SGBuffer;

    invoke-direct {p1}, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->getHandle()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->getHandle()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 35
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 36
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->swigCMemOwn:Z

    .line 38
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGBuffer(J)V

    .line 40
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->swigCPtr:J

    .line 42
    :cond_1
    return-void
.end method

.method public getByteBuffer()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->buffer:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_0

    .line 55
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->getBuffer()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->buffer:Ljava/nio/ByteBuffer;

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->buffer:Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public getDataType()Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;
    .locals 4

    .prologue
    .line 88
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBuffer_getDataType(JLcom/samsung/android/sdk/sgi/render/SGBuffer;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getUsageType()Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;
    .locals 4

    .prologue
    .line 84
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGBuffer_getUsageType(JLcom/samsung/android/sdk/sgi/render/SGBuffer;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGBuffer;->getHandle()J

    move-result-wide v0

    .line 72
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 73
    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    .line 75
    :goto_0
    return v0

    :cond_0
    long-to-int v0, v0

    goto :goto_0
.end method
