.class public Lcom/samsung/android/sdk/sgi/base/SGBox3f;
.super Ljava/lang/Object;
.source "SGBox3f.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGBox3f__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGBox3f;-><init>(JZ)V

    .line 47
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCMemOwn:Z

    .line 27
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCPtr:J

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 50
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGBox3f__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGBox3f;-><init>(JZ)V

    .line 51
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/base/SGBox3f;)J
    .locals 2

    .prologue
    .line 31
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public createTransformed(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)Lcom/samsung/android/sdk/sgi/base/SGBox3f;
    .locals 7

    .prologue
    .line 62
    new-instance v6, Lcom/samsung/android/sdk/sgi/base/SGBox3f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGBox3f_createTransformed(JLcom/samsung/android/sdk/sgi/base/SGBox3f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGBox3f;-><init>(JZ)V

    return-object v6
.end method

.method public extend(Lcom/samsung/android/sdk/sgi/base/SGBox3f;)V
    .locals 6

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGBox3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGBox3f_extend(JLcom/samsung/android/sdk/sgi/base/SGBox3f;JLcom/samsung/android/sdk/sgi/base/SGBox3f;)V

    .line 59
    return-void
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 35
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 36
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCMemOwn:Z

    .line 38
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->delete_SGBox3f(J)V

    .line 40
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCPtr:J

    .line 42
    :cond_1
    return-void
.end method

.method public getMax()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 74
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGBox3f_getMax(JLcom/samsung/android/sdk/sgi/base/SGBox3f;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getMin()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 66
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGBox3f_getMin(JLcom/samsung/android/sdk/sgi/base/SGBox3f;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public resetToZero()V
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGBox3f_resetToZero(JLcom/samsung/android/sdk/sgi/base/SGBox3f;)V

    .line 55
    return-void
.end method

.method public setBox(Lcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 9

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGBox3f_setBox(JLcom/samsung/android/sdk/sgi/base/SGBox3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 83
    return-void
.end method

.method public setMax(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGBox3f_setMax(JLcom/samsung/android/sdk/sgi/base/SGBox3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 79
    return-void
.end method

.method public setMin(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGBox3f;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGBox3f_setMin(JLcom/samsung/android/sdk/sgi/base/SGBox3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 71
    return-void
.end method
