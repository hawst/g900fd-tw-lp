.class public Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox;
.super Lcom/samsung/android/sdk/sgi/widgets/SGButton;
.source "SGCheckBox.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox$1;,
        Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox$SGStateListeningCheckBox;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;-><init>(Landroid/content/Context;)V

    .line 51
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox;->setupDefaultTextColors(Landroid/content/Context;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox;->setupDefaultTextColors(Landroid/content/Context;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 59
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox;->setupDefaultTextColors(Landroid/content/Context;)V

    .line 60
    return-void
.end method


# virtual methods
.method protected bridge synthetic createComponent(Landroid/content/Context;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox;->createComponent(Landroid/content/Context;)Landroid/widget/CheckBox;

    move-result-object v0

    return-object v0
.end method

.method protected createComponent(Landroid/content/Context;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox$SGStateListeningCheckBox;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox$SGStateListeningCheckBox;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox;Landroid/content/Context;)V

    return-object v0
.end method

.method protected bridge synthetic createComponent(Landroid/content/Context;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox;->createComponent(Landroid/content/Context;)Landroid/widget/CheckBox;

    move-result-object v0

    return-object v0
.end method

.method public final isChecked()Z
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox;->androidComponent:Landroid/widget/TextView;

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 2

    .prologue
    .line 121
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGButton;->onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    .line 122
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox$1;->$SwitchMap$com$samsung$android$sdk$sgi$ui$SGTouchEvent$SGAction:[I

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction(I)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 131
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox;->invalidate()V

    .line 133
    const/4 v0, 0x1

    return v0

    .line 126
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox;->toggle()V

    goto :goto_0

    .line 122
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox;->androidComponent:Landroid/widget/TextView;

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 112
    return-void
.end method

.method public final setChecked(Z)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox;->androidComponent:Landroid/widget/TextView;

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 97
    return-void
.end method

.method setupDefaultTextColors(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 65
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 66
    const v1, 0x1010036

    aput v1, v0, v3

    .line 67
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox;->androidComponent:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 71
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 72
    return-void
.end method

.method public final toggle()V
    .locals 2

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox;->isChecked()Z

    move-result v1

    .line 104
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGCheckBox;->androidComponent:Landroid/widget/TextView;

    check-cast v0, Landroid/widget/CheckBox;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 105
    return-void

    .line 104
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
