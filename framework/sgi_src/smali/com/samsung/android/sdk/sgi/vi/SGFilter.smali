.class public final Lcom/samsung/android/sdk/sgi/vi/SGFilter;
.super Ljava/lang/Object;
.source "SGFilter.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 70
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGFilter()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGFilter;-><init>(JZ)V

    .line 71
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->init()V

    .line 72
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCMemOwn:Z

    .line 32
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCPtr:J

    .line 33
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGFilter;)J
    .locals 2

    .prologue
    .line 36
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCPtr:J

    goto :goto_0
.end method

.method private getHandle()J
    .locals 2

    .prologue
    .line 107
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilter_getHandle(JLcom/samsung/android/sdk/sgi/vi/SGFilter;)J

    move-result-wide v0

    return-wide v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilter_init(JLcom/samsung/android/sdk/sgi/vi/SGFilter;)V

    .line 76
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/vi/SGFilter;)Lcom/samsung/android/sdk/sgi/vi/SGFilter;
    .locals 7

    .prologue
    .line 103
    new-instance v6, Lcom/samsung/android/sdk/sgi/vi/SGFilter;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGFilter;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilter___assign__(JLcom/samsung/android/sdk/sgi/vi/SGFilter;JLcom/samsung/android/sdk/sgi/vi/SGFilter;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGFilter;-><init>(JZ)V

    return-object v6
.end method

.method public addFilterPass(Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;)V
    .locals 6

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilter_addFilterPass(JLcom/samsung/android/sdk/sgi/vi/SGFilter;JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;)V

    .line 80
    return-void
.end method

.method public clone()Lcom/samsung/android/sdk/sgi/vi/SGFilter;
    .locals 4

    .prologue
    .line 99
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilter_clone(JLcom/samsung/android/sdk/sgi/vi/SGFilter;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGFilter;-><init>(JZ)V

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->clone()Lcom/samsung/android/sdk/sgi/vi/SGFilter;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 54
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/samsung/android/sdk/sgi/vi/SGFilter;

    if-nez v1, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lcom/samsung/android/sdk/sgi/vi/SGFilter;

    invoke-direct {p1}, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->getHandle()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->getHandle()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 40
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 41
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCMemOwn:Z

    .line 43
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->delete_SGFilter(J)V

    .line 45
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCPtr:J

    .line 47
    :cond_1
    return-void
.end method

.method public getFilterPass(I)Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;
    .locals 4

    .prologue
    .line 91
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCPtr:J

    invoke-static {v2, v3, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilter_getFilterPass(JLcom/samsung/android/sdk/sgi/vi/SGFilter;I)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;-><init>(JZ)V

    return-object v0
.end method

.method public getFilterPassCount()I
    .locals 2

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilter_getFilterPassCount(JLcom/samsung/android/sdk/sgi/vi/SGFilter;)I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->getHandle()J

    move-result-wide v0

    .line 62
    const/16 v2, 0x20

    ushr-long v2, v0, v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 63
    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    .line 65
    :goto_0
    return v0

    :cond_0
    long-to-int v0, v0

    goto :goto_0
.end method

.method public removeFilterPass(I)V
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilter_removeFilterPass__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGFilter;I)V

    .line 88
    return-void
.end method

.method public removeFilterPass(Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;)V
    .locals 6

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilter;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilter_removeFilterPass__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGFilter;JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;)V

    .line 84
    return-void
.end method
