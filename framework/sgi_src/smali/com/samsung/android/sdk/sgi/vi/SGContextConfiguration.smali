.class public final Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;
.super Ljava/lang/Object;
.source "SGContextConfiguration.java"


# instance fields
.field public mAlphaSize:I

.field public mBackgroundThreadCount:I

.field public mBlueSize:I

.field public mDepthSize:I

.field public mGreenSize:I

.field public mRedSize:I

.field public mSampleBuffers:I

.field public mSamples:I

.field public mSeparateThreads:Z

.field public mStencilSize:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mRedSize:I

    .line 41
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mGreenSize:I

    .line 42
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBlueSize:I

    .line 43
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mAlphaSize:I

    .line 44
    const/16 v0, 0x10

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mDepthSize:I

    .line 45
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mStencilSize:I

    .line 46
    iput v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSampleBuffers:I

    .line 47
    iput v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSamples:I

    .line 48
    iput v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBackgroundThreadCount:I

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSeparateThreads:Z

    .line 50
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mRedSize:I

    .line 58
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mGreenSize:I

    .line 59
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBlueSize:I

    .line 60
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mAlphaSize:I

    .line 61
    const/16 v0, 0x10

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mDepthSize:I

    .line 62
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mStencilSize:I

    .line 63
    iput v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSampleBuffers:I

    .line 64
    iput v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSamples:I

    .line 65
    iput p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBackgroundThreadCount:I

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSeparateThreads:Z

    .line 67
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mRedSize:I

    .line 76
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mGreenSize:I

    .line 77
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBlueSize:I

    .line 78
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mAlphaSize:I

    .line 79
    const/16 v0, 0x10

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mDepthSize:I

    .line 80
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mStencilSize:I

    .line 81
    iput v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSampleBuffers:I

    .line 82
    iput v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSamples:I

    .line 83
    iput v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBackgroundThreadCount:I

    .line 84
    iput-boolean p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSeparateThreads:Z

    .line 85
    return-void
.end method


# virtual methods
.method public setRGB565()V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 97
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mRedSize:I

    .line 98
    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mGreenSize:I

    .line 99
    iput v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBlueSize:I

    .line 100
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mAlphaSize:I

    .line 101
    return-void
.end method
