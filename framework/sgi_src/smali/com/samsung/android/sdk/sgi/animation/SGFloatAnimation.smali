.class public final Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;
.super Lcom/samsung/android/sdk/sgi/animation/SGPropertyAnimation;
.source "SGFloatAnimation.java"


# direct methods
.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/animation/SGPropertyAnimation;-><init>(JZ)V

    .line 27
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;)Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;
    .locals 7

    .prologue
    .line 65
    new-instance v6, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGFloatAnimation___assign__(JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;-><init>(JZ)V

    return-object v6
.end method

.method public addKeyFrame(FF)Z
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGFloatAnimation_addKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;FF)Z

    move-result v0

    return v0
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 30
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 31
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->swigCMemOwn:Z

    .line 33
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->delete_SGFloatAnimation(J)V

    .line 35
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->swigCPtr:J

    .line 37
    :cond_1
    return-void
.end method

.method public getEndValue()F
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGFloatAnimation_getEndValue(JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;)F

    move-result v0

    return v0
.end method

.method public getStartValue()F
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGFloatAnimation_getStartValue(JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;)F

    move-result v0

    return v0
.end method

.method public removeKeyFrame(F)Z
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGFloatAnimation_removeKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;F)Z

    move-result v0

    return v0
.end method

.method public setEndValue(F)V
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGFloatAnimation_setEndValue(JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;F)V

    .line 50
    return-void
.end method

.method public setStartValue(F)V
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGFloatAnimation_setStartValue(JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;F)V

    .line 42
    return-void
.end method
