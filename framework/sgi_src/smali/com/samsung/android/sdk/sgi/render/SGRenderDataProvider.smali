.class public abstract Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;
.super Ljava/lang/Object;
.source "SGRenderDataProvider.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method protected constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 46
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGRenderDataProvider()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;-><init>(JZ)V

    .line 47
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;->swigCPtr:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;->swigCMemOwn:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGRenderDataProvider_director_connect(Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;JZZ)V

    .line 48
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;->swigCMemOwn:Z

    .line 27
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;->swigCPtr:J

    .line 28
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;)J
    .locals 2

    .prologue
    .line 31
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;->swigCPtr:J

    goto :goto_0
.end method

.method protected static loadBuiltinShaderData(Ljava/lang/String;)[B
    .locals 1

    .prologue
    .line 57
    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGRenderDataProvider_loadBuiltinShaderData(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 35
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 36
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;->swigCMemOwn:Z

    .line 38
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGRenderDataProvider(J)V

    .line 40
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;->swigCPtr:J

    .line 42
    :cond_1
    return-void
.end method

.method public abstract loadProgram(Ljava/lang/String;Ljava/lang/String;)[B
.end method

.method public abstract loadShaderData(Ljava/lang/String;)[B
.end method

.method public abstract saveProgram(Ljava/lang/String;Ljava/lang/String;[B)V
.end method
