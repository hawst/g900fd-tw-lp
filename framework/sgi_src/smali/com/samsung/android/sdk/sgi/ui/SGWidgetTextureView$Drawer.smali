.class Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;
.super Ljava/lang/Object;
.source "SGWidgetTextureView.java"

# interfaces
.implements Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Drawer"
.end annotation


# instance fields
.field handler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)V
    .locals 2

    .prologue
    .line 90
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->handler:Landroid/os/Handler;

    .line 92
    return-void
.end method


# virtual methods
.method public onDraw(I)V
    .locals 7

    .prologue
    .line 96
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    monitor-enter v1

    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->access$100(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Landroid/graphics/SurfaceTexture;

    move-result-object v0

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    new-instance v2, Landroid/graphics/SurfaceTexture;

    invoke-direct {v2, p1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    # setter for: Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;
    invoke-static {v0, v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->access$102(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;Landroid/graphics/SurfaceTexture;)Landroid/graphics/SurfaceTexture;

    .line 101
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->access$100(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Landroid/graphics/SurfaceTexture;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mOnFrameAvailableListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$OnFrameAvailableListenerImpl;
    invoke-static {v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->access$200(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$OnFrameAvailableListenerImpl;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 102
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 106
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSetBufferSizeMethod:Ljava/lang/reflect/Method;
    invoke-static {v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->access$300(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Ljava/lang/reflect/Method;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;
    invoke-static {v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->access$100(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Landroid/graphics/SurfaceTexture;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v6

    float-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    float-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSetSurfaceTextureMethod:Ljava/lang/reflect/Method;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->access$500(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Ljava/lang/reflect/Method;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mTextureView:Landroid/view/TextureView;
    invoke-static {v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->access$400(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Landroid/view/TextureView;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;
    invoke-static {v5}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->access$100(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Landroid/graphics/SurfaceTexture;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer$1;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 144
    :goto_0
    :try_start_2
    monitor-exit v1

    .line 145
    return-void

    .line 119
    :catch_0
    move-exception v0

    .line 121
    const-string v2, "SGSurfaceRenderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error on setSurfaceTexture into TextureView, - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 122
    :catch_1
    move-exception v0

    .line 124
    :try_start_3
    const-string v2, "SGSurfaceRenderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error on setSurfaceTexture into TextureView, - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 128
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->frames:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->access$600(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->access$100(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Landroid/graphics/SurfaceTexture;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 130
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->this$0:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;

    # getter for: Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->frames:Ljava/util/concurrent/atomic/AtomicInteger;
    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->access$600(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndDecrement()I

    goto :goto_1

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer$2;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
