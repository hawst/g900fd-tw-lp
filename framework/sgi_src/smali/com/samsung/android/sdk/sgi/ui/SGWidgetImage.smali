.class public Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;
.super Lcom/samsung/android/sdk/sgi/ui/SGWidget;
.source "SGWidgetImage.java"


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 117
    invoke-static {}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->new_SGWidgetImage()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;-><init>(JZ)V

    .line 118
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCMemOwn:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_director_connect(Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;JZZ)V

    .line 119
    return-void
.end method

.method public constructor <init>(FFI)V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;-><init>()V

    .line 103
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-direct {p0, p3, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->init(ILcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 104
    return-void
.end method

.method public constructor <init>(FFLandroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;-><init>()V

    .line 77
    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter bitmap is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-direct {p0, p3, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->init(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 79
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;-><init>(JZ)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;-><init>()V

    .line 62
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter bitmap is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->init(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;-><init>()V

    .line 50
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->init(Landroid/graphics/RectF;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;I)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;-><init>()V

    .line 113
    invoke-direct {p0, p2, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->init(ILcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 114
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;-><init>()V

    .line 91
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter bitmap is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    invoke-direct {p0, p2, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->init(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 93
    return-void
.end method

.method private init(ILcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 7

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v4

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_init__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;IJLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 123
    return-void
.end method

.method private init(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 7

    .prologue
    .line 207
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_init__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;Landroid/graphics/Bitmap;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 208
    return-void
.end method

.method private init(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 126
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_init__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;Landroid/graphics/RectF;)V

    .line 130
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 34
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 35
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCMemOwn:Z

    .line 37
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->delete_SGWidgetImage(J)V

    .line 39
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    .line 41
    :cond_1
    return-void
.end method

.method public getBlendMode()Lcom/samsung/android/sdk/sgi/vi/SGBlendMode;
    .locals 4

    .prologue
    .line 156
    const-class v0, Lcom/samsung/android/sdk/sgi/vi/SGBlendMode;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/vi/SGBlendMode;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_getBlendMode(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getColor()I
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_getColor(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)I

    move-result v0

    return v0
.end method

.method public getContentRect()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 167
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_getContentRect(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getContentRectPivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 175
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_getContentRectPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getContentRectScale()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 183
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_getContentRectScale(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public isAlphaBlendingEnabled()Z
    .locals 2

    .prologue
    .line 199
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_isAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)Z

    move-result v0

    return v0
.end method

.method public isPreMultipliedRGBAEnabled()Z
    .locals 2

    .prologue
    .line 191
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_isPreMultipliedRGBAEnabled(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)Z

    move-result v0

    return v0
.end method

.method public setAlphaBlendingEnabled(Z)V
    .locals 2

    .prologue
    .line 195
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_setAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;Z)V

    .line 196
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 203
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_setBitmap(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;Landroid/graphics/Bitmap;)V

    .line 204
    return-void
.end method

.method public setBlendMode(Lcom/samsung/android/sdk/sgi/vi/SGBlendMode;)V
    .locals 3

    .prologue
    .line 149
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 151
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGBlendMode;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_setBlendMode(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;I)V

    .line 153
    return-void
.end method

.method public setColor(I)V
    .locals 2

    .prologue
    .line 141
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_setColor(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;I)V

    .line 142
    return-void
.end method

.method public setContentRect(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 160
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_setContentRect(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;Landroid/graphics/RectF;)V

    .line 164
    return-void
.end method

.method public setContentRectPivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 171
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_setContentRectPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 172
    return-void
.end method

.method public setContentRectScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 179
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_setContentRectScale(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 180
    return-void
.end method

.method public setPreMultipliedRGBAEnabled(Z)V
    .locals 2

    .prologue
    .line 187
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_setPreMultipliedRGBAEnabled(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;Z)V

    .line 188
    return-void
.end method

.method public setTexture(Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    .locals 6

    .prologue
    .line 134
    if-eqz p1, :cond_0

    .line 135
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidgetImage_setTexture(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;JLcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 138
    :goto_0
    return-void

    .line 137
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
