.class public interface abstract Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;
.super Ljava/lang/Object;
.source "SGAnimationListener.java"


# virtual methods
.method public abstract onCancelled(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
.end method

.method public abstract onDiscarded(I)V
.end method

.method public abstract onFinished(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
.end method

.method public abstract onRepeated(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
.end method

.method public abstract onStarted(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
.end method
