.class Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;
.super Ljava/lang/Object;
.source "SGAnimationListenerBase.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method protected constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 48
    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->new_SGAnimationListenerBase__SWIG_0()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;-><init>(JZ)V

    .line 49
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCMemOwn:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationListenerBase_director_connect(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;JZZ)V

    .line 50
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    invoke-virtual {v0, p0, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    .line 51
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCMemOwn:Z

    .line 28
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    .line 29
    return-void
.end method

.method protected constructor <init>(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 78
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->new_SGAnimationListenerBase__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;-><init>(JZ)V

    .line 79
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCMemOwn:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationListenerBase_director_connect(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;JZZ)V

    .line 80
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    invoke-virtual {v0, p0, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    .line 81
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)J
    .locals 2

    .prologue
    .line 32
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public finalize()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 36
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Deregister(J)Z

    .line 37
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 38
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCMemOwn:Z

    .line 40
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->delete_SGAnimationListenerBase(J)V

    .line 42
    :cond_0
    iput-wide v4, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    .line 44
    :cond_1
    return-void
.end method

.method public getCurrentValue(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;)V
    .locals 6

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationListenerBase_getCurrentValue(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;)V

    .line 75
    return-void
.end method

.method public onCancelled(I)V
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationListenerBase_onCancelled(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V

    .line 63
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationListenerBase_onCancelledSwigExplicitSGAnimationListenerBase(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V

    goto :goto_0
.end method

.method public onDiscarded(I)V
    .locals 2

    .prologue
    .line 70
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationListenerBase_onDiscarded(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V

    .line 71
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationListenerBase_onDiscardedSwigExplicitSGAnimationListenerBase(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V

    goto :goto_0
.end method

.method public onFinished(I)V
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationListenerBase_onFinished(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V

    .line 67
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationListenerBase_onFinishedSwigExplicitSGAnimationListenerBase(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V

    goto :goto_0
.end method

.method public onRepeated(I)V
    .locals 2

    .prologue
    .line 58
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationListenerBase_onRepeated(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V

    .line 59
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationListenerBase_onRepeatedSwigExplicitSGAnimationListenerBase(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V

    goto :goto_0
.end method

.method public onStarted(I)V
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationListenerBase_onStarted(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V

    .line 55
    :goto_0
    return-void

    .line 54
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationListenerBase_onStartedSwigExplicitSGAnimationListenerBase(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V

    goto :goto_0
.end method
