.class public abstract Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;
.super Ljava/lang/Object;
.source "SGAnimationValueInterpolator.java"


# instance fields
.field protected swigCMemOwn:Z

.field protected swigCPtr:J


# direct methods
.method protected constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 47
    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->new_SGAnimationValueInterpolator()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;-><init>(JZ)V

    .line 48
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->swigCPtr:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->swigCMemOwn:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationValueInterpolator_director_connect(Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;JZZ)V

    .line 49
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->init()V

    .line 50
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->swigCPtr:J

    invoke-virtual {v0, p0, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    .line 51
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->swigCMemOwn:Z

    .line 28
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->swigCPtr:J

    .line 29
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;)J
    .locals 2

    .prologue
    .line 32
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->swigCPtr:J

    goto :goto_0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationValueInterpolator_init(JLcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;)V

    .line 55
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 36
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 37
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->swigCMemOwn:Z

    .line 39
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->delete_SGAnimationValueInterpolator(J)V

    .line 41
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->swigCPtr:J

    .line 43
    :cond_1
    return-void
.end method

.method public abstract interpolate(FFF)F
.end method

.method public abstract interpolate(FLcom/samsung/android/sdk/sgi/base/SGQuaternion;Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)Lcom/samsung/android/sdk/sgi/base/SGQuaternion;
.end method

.method public abstract interpolate(FLcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Lcom/samsung/android/sdk/sgi/base/SGVector2f;
.end method

.method public abstract interpolate(FLcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)Lcom/samsung/android/sdk/sgi/base/SGVector3f;
.end method

.method public abstract interpolate(FLcom/samsung/android/sdk/sgi/base/SGVector4f;Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;
.end method
