.class public final Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;
.super Ljava/lang/Object;
.source "SGFilterPass.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 51
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGFilterPass__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;-><init>(JZ)V

    .line 52
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->init()V

    .line 53
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCMemOwn:Z

    .line 32
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    .line 33
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 3

    .prologue
    .line 100
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGFilterPass__SWIG_1(Z)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;-><init>(JZ)V

    .line 101
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->init()V

    .line 102
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;)J
    .locals 2

    .prologue
    .line 36
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    goto :goto_0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilterPass_init__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;)V

    .line 61
    return-void
.end method

.method private init(Z)V
    .locals 2

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilterPass_init__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;Z)V

    .line 57
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 40
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 41
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCMemOwn:Z

    .line 43
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->delete_SGFilterPass(J)V

    .line 45
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    .line 47
    :cond_1
    return-void
.end method

.method public getProgramProperty()Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;
    .locals 4

    .prologue
    .line 105
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilterPass_getProgramProperty(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;)J

    move-result-wide v2

    .line 106
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    .line 107
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(JZ)V

    .line 108
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getProperty(I)Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 2

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilterPass_getProperty__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;I)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public getProperty(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 2

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilterPass_getProperty__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public getPropertyCount()I
    .locals 2

    .prologue
    .line 92
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilterPass_getPropertyCount(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;)I

    move-result v0

    return v0
.end method

.method public isAlphaBlendingEnabled()Z
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilterPass_isAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;)Z

    move-result v0

    return v0
.end method

.method public removeProperty(I)V
    .locals 2

    .prologue
    .line 80
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilterPass_removeProperty__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;I)V

    .line 81
    return-void
.end method

.method public removeProperty(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilterPass_removeProperty__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public setAlphaBlendingEnabled(Z)V
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilterPass_setAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;Z)V

    .line 65
    return-void
.end method

.method public setProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V
    .locals 6

    .prologue
    .line 88
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilterPass_setProgramProperty(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V

    .line 89
    return-void
.end method

.method public setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    .locals 7

    .prologue
    .line 72
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/render/SGProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFilterPass_setProperty(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 73
    return-void
.end method
