.class public final enum Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;
.super Ljava/lang/Enum;
.source "SGTouchEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SGPointerType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

.field public static final enum ERASER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

.field public static final enum FINGER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

.field public static final enum MOUSE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

.field public static final enum STYLUS:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

.field public static final enum UNKNOWN:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 321
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->UNKNOWN:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    .line 322
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    const-string v1, "FINGER"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->FINGER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    .line 323
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    const-string v1, "ERASER"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->ERASER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    .line 324
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    const-string v1, "MOUSE"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->MOUSE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    .line 325
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    const-string v1, "STYLUS"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->STYLUS:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    .line 320
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->UNKNOWN:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->FINGER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->ERASER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->MOUSE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->STYLUS:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->$VALUES:[Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 320
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;
    .locals 1

    .prologue
    .line 320
    const-class v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;
    .locals 1

    .prologue
    .line 320
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->$VALUES:[Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGPointerType;

    return-object v0
.end method
