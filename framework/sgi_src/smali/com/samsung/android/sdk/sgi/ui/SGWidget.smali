.class public Lcom/samsung/android/sdk/sgi/ui/SGWidget;
.super Ljava/lang/Object;
.source "SGWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/ui/SGWidget$1;,
        Lcom/samsung/android/sdk/sgi/ui/SGWidget$PerformClick;,
        Lcom/samsung/android/sdk/sgi/ui/SGWidget$CheckForLongPress;
    }
.end annotation


# instance fields
.field mAsyncScreenshotListenersArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field mChildArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/sgi/ui/SGWidget;",
            ">;"
        }
    .end annotation
.end field

.field mChildArrayLayer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/sgi/vi/SGLayer;",
            ">;"
        }
    .end annotation
.end field

.field mClickListener:Lcom/samsung/android/sdk/sgi/ui/SGClickListener;

.field mGeometryGenerator:Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;

.field private mHasPerformedLongPress:Z

.field private mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

.field mLongClickListener:Lcom/samsung/android/sdk/sgi/ui/SGLongClickListener;

.field private mPendingCheckForLongPress:Lcom/samsung/android/sdk/sgi/ui/SGWidget$CheckForLongPress;

.field private mPerformClick:Lcom/samsung/android/sdk/sgi/ui/SGWidget$PerformClick;

.field private mPressed:Z

.field private mSelfCaptured:Z

.field private mSelfHovered:Z

.field private mTouchCanceled:Z

.field private mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

.field mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

.field swigCMemOwn:Z

.field swigCPtr:J


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1141
    invoke-static {}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->new_SGWidget()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;-><init>(JZ)V

    .line 1142
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCMemOwn:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_director_connect(Lcom/samsung/android/sdk/sgi/ui/SGWidget;JZZ)V

    .line 1143
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfCaptured:Z

    .line 45
    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfHovered:Z

    .line 46
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 47
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 51
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCMemOwn:Z

    .line 52
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArrayLayer:Ljava/util/ArrayList;

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mAsyncScreenshotListenersArray:Ljava/util/ArrayList;

    .line 56
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-virtual {v0, p0, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    .line 57
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mClickListener:Lcom/samsung/android/sdk/sgi/ui/SGClickListener;

    .line 59
    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mLongClickListener:Lcom/samsung/android/sdk/sgi/ui/SGLongClickListener;

    .line 60
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;-><init>()V

    .line 84
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->init(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPressed:Z

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->performLongClick()Z

    move-result v0

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Z)Z
    .locals 0

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHasPerformedLongPress:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->performClick()Z

    move-result v0

    return v0
.end method

.method private addW(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I
    .locals 6

    .prologue
    .line 1461
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_addW__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    move-result v0

    return v0
.end method

.method private addW(Lcom/samsung/android/sdk/sgi/ui/SGWidget;I)I
    .locals 7

    .prologue
    .line 1465
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_addW__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)I

    move-result v0

    return v0
.end method

.method private bringToF()V
    .locals 2

    .prologue
    .line 1405
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_bringToF(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1406
    return-void
.end method

.method private bringWToF(I)V
    .locals 2

    .prologue
    .line 1409
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_bringWToF__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)V

    .line 1410
    return-void
.end method

.method private bringWToF(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 6

    .prologue
    .line 1413
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_bringWToF__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1414
    return-void
.end method

.method private cancelClick()V
    .locals 2

    .prologue
    .line 370
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPerformClick:Lcom/samsung/android/sdk/sgi/ui/SGWidget$PerformClick;

    if-eqz v0, :cond_0

    .line 372
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    .line 373
    if-eqz v0, :cond_0

    .line 375
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getView()Landroid/view/View;

    move-result-object v0

    .line 376
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPerformClick:Lcom/samsung/android/sdk/sgi/ui/SGWidget$PerformClick;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 379
    :cond_0
    return-void
.end method

.method private cancelLongClick()V
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPendingCheckForLongPress:Lcom/samsung/android/sdk/sgi/ui/SGWidget$CheckForLongPress;

    if-eqz v0, :cond_0

    .line 334
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    .line 335
    if-eqz v0, :cond_0

    .line 337
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getView()Landroid/view/View;

    move-result-object v0

    .line 338
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPendingCheckForLongPress:Lcom/samsung/android/sdk/sgi/ui/SGWidget$CheckForLongPress;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 341
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHasPerformedLongPress:Z

    .line 342
    return-void
.end method

.method private checkForClick()Z
    .locals 2

    .prologue
    .line 352
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    .line 353
    if-eqz v0, :cond_1

    .line 355
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getView()Landroid/view/View;

    move-result-object v0

    .line 356
    if-eqz v0, :cond_1

    .line 358
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPerformClick:Lcom/samsung/android/sdk/sgi/ui/SGWidget$PerformClick;

    if-nez v1, :cond_0

    new-instance v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget$PerformClick;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget$PerformClick;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPerformClick:Lcom/samsung/android/sdk/sgi/ui/SGWidget$PerformClick;

    .line 359
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPerformClick:Lcom/samsung/android/sdk/sgi/ui/SGWidget$PerformClick;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    move-result v0

    .line 362
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkForLongClick()V
    .locals 4

    .prologue
    .line 278
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    .line 279
    if-eqz v0, :cond_1

    .line 282
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getView()Landroid/view/View;

    move-result-object v0

    .line 283
    if-eqz v0, :cond_1

    .line 285
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHasPerformedLongPress:Z

    .line 287
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPendingCheckForLongPress:Lcom/samsung/android/sdk/sgi/ui/SGWidget$CheckForLongPress;

    if-nez v1, :cond_0

    .line 288
    new-instance v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget$CheckForLongPress;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget$CheckForLongPress;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPendingCheckForLongPress:Lcom/samsung/android/sdk/sgi/ui/SGWidget$CheckForLongPress;

    .line 290
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPendingCheckForLongPress:Lcom/samsung/android/sdk/sgi/ui/SGWidget$CheckForLongPress;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 293
    :cond_1
    return-void
.end method

.method private final checkOnTouchUp(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 691
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getPointerCount()I

    move-result v2

    move v1, v0

    .line 692
    :goto_0
    if-ge v1, v2, :cond_1

    .line 693
    invoke-virtual {p1, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction(I)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v3

    .line 694
    sget-object v4, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-eq v3, v4, :cond_0

    sget-object v4, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->CANCEL:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-eq v3, v4, :cond_0

    .line 699
    :goto_1
    return v0

    .line 692
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 699
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method private final dispatchSelfHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 593
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isHoverable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 618
    :goto_0
    return v0

    .line 597
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getHoverListener()Lcom/samsung/android/sdk/sgi/ui/SGHoverListener;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 598
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getHoverListener()Lcom/samsung/android/sdk/sgi/ui/SGHoverListener;

    move-result-object v1

    invoke-interface {v1, p1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGHoverListener;->onHover(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v1

    .line 600
    :goto_1
    if-nez v1, :cond_1

    .line 601
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->onHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v1

    .line 603
    :cond_1
    if-eqz v1, :cond_2

    .line 605
    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGWidget$1;->$SwitchMap$com$samsung$android$sdk$sgi$ui$SGTouchEvent$SGAction:[I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_2
    :goto_2
    :pswitch_0
    move v0, v1

    .line 618
    goto :goto_0

    .line 608
    :pswitch_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setHovered(Z)V

    goto :goto_2

    .line 611
    :pswitch_2
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setHovered(Z)V

    goto :goto_2

    :cond_3
    move v1, v0

    goto :goto_1

    .line 605
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private final dispatchSelfTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 746
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isEventActive()Z

    move-result v1

    if-nez v1, :cond_1

    .line 757
    :cond_0
    :goto_0
    return v0

    .line 751
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getTouchListener()Lcom/samsung/android/sdk/sgi/ui/SGTouchListener;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 752
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getTouchListener()Lcom/samsung/android/sdk/sgi/ui/SGTouchListener;

    move-result-object v0

    invoke-interface {v0, p1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchListener;->onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    .line 755
    :cond_2
    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method private final exitHoveredWidget(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 622
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-eqz v1, :cond_0

    .line 624
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getTouchEventInLocalWindow(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    move-result-object v1

    .line 625
    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_EXIT:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setAction(ILcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V

    .line 626
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    .line 628
    :cond_0
    return v0
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J
    .locals 2

    .prologue
    .line 64
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    goto :goto_0
.end method

.method private getLocalTouch(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;
    .locals 7

    .prologue
    .line 1493
    new-instance v6, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getLocalTouch(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(JZ)V

    return-object v6
.end method

.method private init(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 1146
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_init(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 1147
    return-void
.end method

.method private isLayerInvalid()Z
    .locals 2

    .prologue
    .line 1345
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isLayerInvalid(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method private makeScreenShot(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;)V
    .locals 8

    .prologue
    .line 1652
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;)J

    move-result-wide v5

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v7, p3

    invoke-static/range {v0 .. v7}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_makeScreenShot__SWIG_7(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;)V

    .line 1653
    return-void
.end method

.method private makeScreenShot(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;)V
    .locals 7

    .prologue
    .line 1648
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_makeScreenShot__SWIG_6(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/Bitmap;JLcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;)V

    .line 1649
    return-void
.end method

.method private makeScreenShot(Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;)V
    .locals 10

    .prologue
    .line 1664
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v4

    invoke-static {p3}, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;)J

    move-result-wide v7

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    move-object v9, p3

    invoke-static/range {v0 .. v9}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_makeScreenShot__SWIG_10(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;)V

    .line 1665
    return-void
.end method

.method private makeScreenShot(Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;)V
    .locals 9

    .prologue
    .line 1660
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_makeScreenShot__SWIG_9(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;)V

    .line 1661
    return-void
.end method

.method private makeScreenShot(Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;)V
    .locals 6

    .prologue
    .line 1656
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_makeScreenShot__SWIG_8(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;)V

    .line 1657
    return-void
.end method

.method private performClick()Z
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mClickListener:Lcom/samsung/android/sdk/sgi/ui/SGClickListener;

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mClickListener:Lcom/samsung/android/sdk/sgi/ui/SGClickListener;

    invoke-interface {v0, p0}, Lcom/samsung/android/sdk/sgi/ui/SGClickListener;->onClick(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    .line 306
    const/4 v0, 0x1

    .line 308
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private performLongClick()Z
    .locals 2

    .prologue
    .line 319
    const/4 v0, 0x0

    .line 321
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mLongClickListener:Lcom/samsung/android/sdk/sgi/ui/SGLongClickListener;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mLongClickListener:Lcom/samsung/android/sdk/sgi/ui/SGLongClickListener;

    invoke-interface {v0, p0}, Lcom/samsung/android/sdk/sgi/ui/SGLongClickListener;->onLongClick(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    .line 323
    :cond_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHasPerformedLongPress:Z

    .line 324
    return v0
.end method

.method private final processRawTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 703
    new-instance v6, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    invoke-direct {v6, p1, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Z)V

    .line 704
    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getPointerCount()I

    move-result v7

    .line 706
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v5, v0

    :goto_0
    if-lez v5, :cond_5

    .line 707
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    add-int/lit8 v3, v5, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 709
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isVisible()Z

    move-result v3

    if-eqz v3, :cond_4

    move v4, v2

    move v3, v1

    .line 712
    :goto_1
    if-ge v4, v7, :cond_1

    .line 713
    invoke-virtual {v6, v4}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getRawX(I)F

    move-result v8

    invoke-virtual {v6, v4}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getRawY(I)F

    move-result v9

    invoke-virtual {v0, v8, v9}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getLocationInWindow(FF)Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    .line 714
    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v9

    .line 715
    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v8

    .line 716
    if-eqz v3, :cond_0

    invoke-virtual {v0, v9, v8}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isLocalPointOnWidget(FF)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v1

    .line 718
    :goto_2
    if-eqz v3, :cond_1

    .line 719
    invoke-virtual {v6, v4, v9}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setX(IF)V

    .line 720
    invoke-virtual {v6, v4, v8}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setY(IF)V

    .line 712
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_0
    move v3, v2

    .line 716
    goto :goto_2

    .line 727
    :cond_1
    if-eqz v3, :cond_3

    .line 728
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 729
    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 742
    :goto_3
    return v0

    .line 733
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 737
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isLayerInvalid()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    .line 738
    goto :goto_3

    .line 706
    :cond_4
    add-int/lit8 v0, v5, -0x1

    move v5, v0

    goto :goto_0

    :cond_5
    move v0, v2

    .line 742
    goto :goto_3
.end method

.method private removeAllW()V
    .locals 2

    .prologue
    .line 1481
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_removeAllW(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1482
    return-void
.end method

.method private removeCapturing(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 911
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 912
    :cond_0
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfCaptured:Z

    .line 913
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfHovered:Z

    .line 914
    iput-object v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 915
    iput-object v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 917
    :cond_1
    return-void
.end method

.method private removeW(I)V
    .locals 2

    .prologue
    .line 1473
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_removeW__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)V

    .line 1474
    return-void
.end method

.method private removeW(II)V
    .locals 2

    .prologue
    .line 1477
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_removeW__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;II)V

    .line 1478
    return-void
.end method

.method private removeW(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 6

    .prologue
    .line 1469
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_removeW__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1470
    return-void
.end method

.method private final sendHoverToChildren(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 536
    .line 537
    new-instance v7, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    invoke-direct {v7, p1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Z)V

    .line 539
    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getPointerCount()I

    move-result v8

    .line 540
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v6, v0

    move v1, v3

    :goto_0
    if-lez v6, :cond_3

    .line 541
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    add-int/lit8 v4, v6, -0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 542
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isEventActive()Z

    move-result v4

    if-eqz v4, :cond_7

    move v5, v3

    move v4, v2

    .line 546
    :goto_1
    if-ge v5, v8, :cond_1

    .line 547
    invoke-virtual {v7, v5}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getRawX(I)F

    move-result v9

    invoke-virtual {v7, v5}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getRawY(I)F

    move-result v10

    invoke-virtual {v0, v9, v10}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getLocationInWindow(FF)Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v9

    .line 548
    invoke-virtual {v9}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v10

    .line 549
    invoke-virtual {v9}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v9

    .line 550
    if-eqz v4, :cond_0

    invoke-virtual {v0, v10, v9}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isLocalPointOnWidget(FF)Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v2

    .line 552
    :goto_2
    if-ne v4, v2, :cond_1

    .line 553
    invoke-virtual {v7, v5, v10}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setX(IF)V

    .line 554
    invoke-virtual {v7, v5, v9}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setY(IF)V

    .line 546
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_0
    move v4, v3

    .line 550
    goto :goto_2

    .line 561
    :cond_1
    if-eqz v4, :cond_2

    .line 562
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 563
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v1, v7}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v1

    .line 564
    if-nez v1, :cond_2

    .line 565
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 583
    :cond_2
    :goto_3
    if-nez v1, :cond_3

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isLayerInvalid()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 589
    :cond_3
    return v1

    .line 569
    :cond_4
    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v1

    .line 570
    sget-object v4, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_ENTER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    invoke-virtual {v7, v3, v4}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setAction(ILcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V

    .line 571
    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v4

    .line 573
    invoke-virtual {v7, v3, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setAction(ILcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V

    .line 574
    if-eqz v4, :cond_6

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v2

    .line 576
    :goto_4
    if-eqz v1, :cond_5

    .line 577
    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->exitHoveredWidget(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    .line 579
    :cond_5
    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    goto :goto_3

    :cond_6
    move v1, v3

    .line 574
    goto :goto_4

    :cond_7
    move v0, v1

    .line 540
    add-int/lit8 v1, v6, -0x1

    move v6, v1

    move v1, v0

    goto/16 :goto_0
.end method

.method private sendToB()V
    .locals 2

    .prologue
    .line 1417
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_sendToB(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1418
    return-void
.end method

.method private sendWToB(I)V
    .locals 2

    .prologue
    .line 1421
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_sendWToB__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)V

    .line 1422
    return-void
.end method

.method private sendWToB(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 6

    .prologue
    .line 1425
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_sendWToB__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1426
    return-void
.end method

.method private setHoverListener(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V
    .locals 6

    .prologue
    .line 1608
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setHoverListener(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V

    .line 1609
    return-void
.end method

.method private setKeyEventListener(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V
    .locals 6

    .prologue
    .line 1616
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setKeyEventListener(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V

    .line 1617
    return-void
.end method

.method private setTouchListener(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V
    .locals 6

    .prologue
    .line 1604
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setTouchListener(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V

    .line 1605
    return-void
.end method

.method private setWidgetAnimationListener(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V
    .locals 6

    .prologue
    .line 1620
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setWidgetAnimationListener(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V

    .line 1621
    return-void
.end method

.method private setWidgetTransformationListener(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V
    .locals 6

    .prologue
    .line 1612
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setWidgetTransformationListener(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V

    .line 1613
    return-void
.end method

.method private swapW(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 9

    .prologue
    .line 1485
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_swapW(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1486
    return-void
.end method


# virtual methods
.method public addAnimation(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)I
    .locals 6

    .prologue
    .line 1365
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_addAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I

    move-result v0

    return v0
.end method

.method public addAnimation(Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)V
    .locals 9

    .prologue
    .line 1588
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGFilterPass;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_addAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)V

    .line 1589
    return-void
.end method

.method public addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I
    .locals 2

    .prologue
    .line 873
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter widget is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 874
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getParent()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    .line 875
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->addW(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    move-result v1

    .line 876
    if-eqz v0, :cond_1

    .line 877
    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeCapturing(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 878
    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 880
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 881
    return v1
.end method

.method public addWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;I)I
    .locals 2

    .prologue
    .line 896
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter widget is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 897
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getParent()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    .line 898
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->addW(Lcom/samsung/android/sdk/sgi/ui/SGWidget;I)I

    move-result v1

    .line 899
    if-eqz v0, :cond_1

    .line 900
    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeCapturing(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 901
    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 903
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 904
    return v1
.end method

.method public bringToFront()V
    .locals 3

    .prologue
    .line 1044
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getParent()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    .line 1045
    if-eqz v0, :cond_1

    .line 1047
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->bringToF()V

    .line 1048
    iget-object v1, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1049
    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1059
    :cond_0
    :goto_0
    return-void

    .line 1053
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    .line 1054
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getWidgetIndex(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 1056
    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->bringWidgetToFront(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    goto :goto_0
.end method

.method public bringWidgetToFront(I)V
    .locals 2

    .prologue
    .line 1104
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 1105
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getParent()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v1

    .line 1106
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->bringWToF(I)V

    .line 1107
    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1108
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1109
    return-void
.end method

.method public bringWidgetToFront(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 2

    .prologue
    .line 1068
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter widget is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1069
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getParent()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    .line 1070
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->bringWToF(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1071
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1072
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1073
    return-void
.end method

.method public clearFocus()V
    .locals 2

    .prologue
    .line 1552
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_clearFocus(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1553
    :goto_0
    return-void

    .line 1552
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_clearFocusSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    goto :goto_0
.end method

.method public containsChild(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1035
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 1036
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public dispatchHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 483
    .line 485
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget$1;->$SwitchMap$com$samsung$android$sdk$sgi$ui$SGTouchEvent$SGAction:[I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 532
    :cond_0
    :goto_0
    return v0

    .line 497
    :pswitch_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v0

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_EXIT:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    if-ne v0, v2, :cond_2

    .line 498
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-eqz v0, :cond_1

    .line 499
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->exitHoveredWidget(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    .line 500
    iput-object v3, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 506
    :goto_1
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfHovered:Z

    goto :goto_0

    .line 503
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchSelfHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    goto :goto_1

    .line 510
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->sendHoverToChildren(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    .line 512
    if-nez v0, :cond_4

    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-eqz v0, :cond_3

    .line 515
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->exitHoveredWidget(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    .line 516
    iput-object v3, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 519
    :cond_3
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchSelfHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfHovered:Z

    .line 520
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfHovered:Z

    goto :goto_0

    .line 524
    :cond_4
    iget-boolean v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfHovered:Z

    if-eqz v2, :cond_0

    .line 525
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfHovered:Z

    .line 526
    new-instance v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    invoke-direct {v2, p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)V

    .line 527
    sget-object v3, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_EXIT:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    invoke-virtual {v2, v1, v3}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->setAction(ILcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V

    .line 528
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchSelfHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    goto :goto_0

    .line 485
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public dispatchKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
    .locals 6

    .prologue
    .line 1532
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_dispatchKeyEvent(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_dispatchKeyEventSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 652
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-nez v1, :cond_6

    .line 654
    iget-boolean v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfCaptured:Z

    if-eqz v1, :cond_1

    .line 655
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchSelfTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfCaptured:Z

    .line 656
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfCaptured:Z

    .line 683
    :cond_0
    :goto_0
    return v0

    .line 659
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isTouchFirstSendToParentEnabled()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 660
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchSelfTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 661
    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfCaptured:Z

    goto :goto_0

    .line 664
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->processRawTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    .line 679
    :cond_3
    :goto_1
    if-eqz v0, :cond_4

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->checkOnTouchUp(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 680
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->uncaptureWidget()V

    goto :goto_0

    .line 667
    :cond_5
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->processRawTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    .line 668
    if-nez v0, :cond_3

    .line 669
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchSelfTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfCaptured:Z

    .line 670
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfCaptured:Z

    goto :goto_0

    .line 675
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getTouchEventInLocalWindow(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    move-result-object v0

    .line 676
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    goto :goto_1
.end method

.method public dispatchTouchEventToChildren(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 6

    .prologue
    .line 1508
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_dispatchTouchEventToChildren(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    return v0
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 68
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 69
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCMemOwn:Z

    .line 71
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->delete_SGWidget(J)V

    .line 73
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    .line 75
    :cond_1
    return-void
.end method

.method public getFullTransform()Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 1265
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getFullTransform(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public getHoverListener()Lcom/samsung/android/sdk/sgi/ui/SGHoverListener;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mHoverListener:Lcom/samsung/android/sdk/sgi/ui/SGHoverListener;

    return-object v0
.end method

.method public getKeyEventListener()Lcom/samsung/android/sdk/sgi/ui/SGKeyEventListener;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mKeyEventListener:Lcom/samsung/android/sdk/sgi/ui/SGKeyEventListener;

    return-object v0
.end method

.method public getLocalTransform()Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 4

    .prologue
    .line 1269
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getLocalTransform(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v0
.end method

.method public getLocationInWindow(FF)Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 1381
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getLocationInWindow__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getLocationInWindow(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 7

    .prologue
    .line 1377
    new-instance v6, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getLocationInWindow__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v6
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1154
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getName(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOnClickListener()Lcom/samsung/android/sdk/sgi/ui/SGClickListener;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mClickListener:Lcom/samsung/android/sdk/sgi/ui/SGClickListener;

    return-object v0
.end method

.method public getOnLongClickListener()Lcom/samsung/android/sdk/sgi/ui/SGLongClickListener;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mLongClickListener:Lcom/samsung/android/sdk/sgi/ui/SGLongClickListener;

    return-object v0
.end method

.method public getOpacity()F
    .locals 2

    .prologue
    .line 1305
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getOpacity(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)F

    move-result v0

    return v0
.end method

.method public getParent()Lcom/samsung/android/sdk/sgi/ui/SGWidget;
    .locals 2

    .prologue
    .line 1632
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getParent(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    return-object v0
.end method

.method public getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 1273
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getPosition(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getPosition3f()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 1277
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getPosition3f(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getPositionPivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 1297
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getPositionPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getPositionPivot3f()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 1301
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getPositionPivot3f(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getRelativeToAnotherParentTransform(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 7

    .prologue
    .line 1489
    new-instance v6, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getRelativeToAnotherParentTransform(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v6
.end method

.method public getRelativeToParentTransform()Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getFullTransform()Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    move-result-object v0

    return-object v0
.end method

.method public getRelativeTransform()Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 249
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getRelativeTransform(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    move-result-object v0

    return-object v0
.end method

.method public getRelativeTransform(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 7

    .prologue
    .line 1449
    new-instance v6, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getRelativeTransform(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    return-object v6
.end method

.method public getRotation()Lcom/samsung/android/sdk/sgi/base/SGQuaternion;
    .locals 4

    .prologue
    .line 1317
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getRotation(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    return-object v0
.end method

.method public getRotation3f()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 1321
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getRotation3f(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getRotationPivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 1325
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getRotationPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getRotationPivot3f()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 1329
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getRotationPivot3f(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getScale()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 1281
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getScale(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getScale3f()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 1285
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getScale3f(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getScalePivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 1289
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getScalePivot(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getScalePivot3f()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 1293
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getScalePivot3f(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 1313
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getSize(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;
    .locals 2

    .prologue
    .line 1624
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getSurface(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    return-object v0
.end method

.method public getTouchCapturedWidget()Lcom/samsung/android/sdk/sgi/ui/SGWidget;
    .locals 2

    .prologue
    .line 1628
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getTouchCapturedWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    return-object v0
.end method

.method public getTouchEventInLocalWindow(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;
    .locals 2

    .prologue
    .line 637
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getLocalTouch(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    move-result-object v0

    .line 638
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->cacheAction(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;)V

    .line 639
    return-object v0
.end method

.method public getTouchListener()Lcom/samsung/android/sdk/sgi/ui/SGTouchListener;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mTouchListener:Lcom/samsung/android/sdk/sgi/ui/SGTouchListener;

    return-object v0
.end method

.method public getTransform()Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getLocalTransform()Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    move-result-object v0

    return-object v0
.end method

.method public getTransformationHint()Lcom/samsung/android/sdk/sgi/vi/SGTransformationHints;
    .locals 4

    .prologue
    .line 1516
    const-class v0, Lcom/samsung/android/sdk/sgi/vi/SGTransformationHints;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/vi/SGTransformationHints;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getTransformationHint(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getVisibility()Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;
    .locals 4

    .prologue
    .line 1337
    const-class v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getVisibility(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getWidget(I)Lcom/samsung/android/sdk/sgi/ui/SGWidget;
    .locals 1

    .prologue
    .line 984
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    return-object v0
.end method

.method public getWidget(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/ui/SGWidget;
    .locals 3

    .prologue
    .line 993
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 994
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 995
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWidgetAnimationListener()Lcom/samsung/android/sdk/sgi/ui/SGWidgetAnimationListener;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidgetAnimationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetAnimationListener;

    return-object v0
.end method

.method public getWidgetAtPoint(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Lcom/samsung/android/sdk/sgi/ui/SGWidget;
    .locals 6

    .prologue
    .line 1636
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_getWidgetAtPoint(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    return-object v0
.end method

.method public getWidgetIndex(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I
    .locals 1

    .prologue
    .line 1024
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getWidgetTransformationListener()Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidgetTransformationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;

    return-object v0
.end method

.method public getWidgetsCount()I
    .locals 1

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public hideCursor()V
    .locals 2

    .prologue
    .line 1437
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_hideCursor(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1438
    return-void
.end method

.method public invalidate()V
    .locals 2

    .prologue
    .line 1497
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_invalidate__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1498
    :goto_0
    return-void

    .line 1497
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_invalidateSwigExplicitSGWidget__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    goto :goto_0
.end method

.method public invalidate(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 1501
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1503
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-ne v0, v1, :cond_1

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_invalidate__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/RectF;)V

    .line 1505
    :goto_0
    return-void

    .line 1503
    :cond_1
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_invalidateSwigExplicitSGWidget__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/RectF;)V

    goto :goto_0
.end method

.method public isAddedToSurface()Z
    .locals 2

    .prologue
    .line 1445
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isAddedToSurface(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method public isChildrenClippingEnabled()Z
    .locals 2

    .prologue
    .line 1457
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isChildrenClippingEnabled(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method public isEventActive()Z
    .locals 2

    .prologue
    .line 1341
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isEventActive(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method public isFiltersAttachDepthBuffer()Z
    .locals 2

    .prologue
    .line 1596
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isFiltersAttachDepthBuffer(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method public isFiltersAttachStencilBuffer()Z
    .locals 2

    .prologue
    .line 1600
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isFiltersAttachStencilBuffer(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method public isFocusable()Z
    .locals 2

    .prologue
    .line 1536
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isFocusable(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isFocusableSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    goto :goto_0
.end method

.method public isHoverable()Z
    .locals 2

    .prologue
    .line 1357
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isHoverable(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method public isHovered()Z
    .locals 2

    .prologue
    .line 1349
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isHovered(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method public isInFocus()Z
    .locals 2

    .prologue
    .line 1544
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isInFocus(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isInFocusSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    goto :goto_0
.end method

.method public isInTransition()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 209
    const/4 v0, 0x0

    return v0
.end method

.method public isInheritOpacity()Z
    .locals 2

    .prologue
    .line 1309
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isInheritOpacity(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method public isLocalPointOnWidget(FF)Z
    .locals 2

    .prologue
    .line 1397
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isLocalPointOnWidget__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)Z

    move-result v0

    return v0
.end method

.method public isLocalPointOnWidget(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Z
    .locals 6

    .prologue
    .line 1393
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isLocalPointOnWidget__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)Z

    move-result v0

    return v0
.end method

.method public isRawPointOnWidget(FF)Z
    .locals 2

    .prologue
    .line 1389
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isRawPointOnWidget__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)Z

    move-result v0

    return v0
.end method

.method public isRawPointOnWidget(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Z
    .locals 6

    .prologue
    .line 1385
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isRawPointOnWidget__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)Z

    move-result v0

    return v0
.end method

.method public isScreenShotAttachDepthBuffer()Z
    .locals 2

    .prologue
    .line 1564
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isScreenShotAttachDepthBuffer(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method public isScreenShotAttachStencilBuffer()Z
    .locals 2

    .prologue
    .line 1568
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isScreenShotAttachStencilBuffer(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method protected final isSelfCaptured()Z
    .locals 1

    .prologue
    .line 687
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfCaptured:Z

    return v0
.end method

.method public isTouchFirstSendToParentEnabled()Z
    .locals 2

    .prologue
    .line 1261
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isTouchFirstSendToParentEnabled(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method public isTouchFocusable()Z
    .locals 2

    .prologue
    .line 1548
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isTouchFocusable(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 2

    .prologue
    .line 1333
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_isVisible(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method public makeScreenShot()Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 2

    .prologue
    .line 1572
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_makeScreenShot__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public makeScreenShot(Landroid/graphics/Rect;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 2

    .prologue
    .line 1580
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_makeScreenShot__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/Rect;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public makeScreenShot(Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 7

    .prologue
    .line 1584
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v4

    move-object v2, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_makeScreenShot__SWIG_3(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public makeScreenShot(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 6

    .prologue
    .line 1576
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_makeScreenShot__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v0

    return-object v0
.end method

.method public makeScreenShot(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 1640
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_makeScreenShot__SWIG_4(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/Bitmap;)V

    .line 1641
    return-void
.end method

.method public makeScreenShot(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 1644
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_makeScreenShot__SWIG_5(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V

    .line 1645
    return-void
.end method

.method public makeScreenShot(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;)V
    .locals 2

    .prologue
    .line 796
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGBitmapScreenshotListenerHolder;

    invoke-direct {v0, p0, p3}, Lcom/samsung/android/sdk/sgi/ui/SGBitmapScreenshotListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;)V

    .line 799
    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->makeScreenShot(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 800
    const/4 v0, 0x0

    .line 804
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGBitmapScreenshotListenerHolder;->detachListener()V

    .line 806
    :cond_0
    return-void

    .line 804
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGBitmapScreenshotListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public makeScreenShot(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;)V
    .locals 2

    .prologue
    .line 777
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGBitmapScreenshotListenerHolder;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/sdk/sgi/ui/SGBitmapScreenshotListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;)V

    .line 780
    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->makeScreenShot(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 781
    const/4 v0, 0x0

    .line 785
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGBitmapScreenshotListenerHolder;->detachListener()V

    .line 787
    :cond_0
    return-void

    .line 785
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGBitmapScreenshotListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public makeScreenShot(Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V
    .locals 2

    .prologue
    .line 850
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;

    invoke-direct {v0, p0, p3}, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V

    .line 853
    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->makeScreenShot(Landroid/graphics/Rect;Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 854
    const/4 v0, 0x0

    .line 858
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;->detachListener()V

    .line 860
    :cond_0
    return-void

    .line 858
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public makeScreenShot(Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V
    .locals 2

    .prologue
    .line 831
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V

    .line 834
    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->makeScreenShot(Lcom/samsung/android/sdk/sgi/base/SGVector2i;Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 835
    const/4 v0, 0x0

    .line 839
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;->detachListener()V

    .line 841
    :cond_0
    return-void

    .line 839
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public makeScreenShot(Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V
    .locals 2

    .prologue
    .line 813
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;)V

    .line 816
    :try_start_0
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->makeScreenShot(Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 817
    const/4 v0, 0x0

    .line 821
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;->detachListener()V

    .line 823
    :cond_0
    return-void

    .line 821
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerHolder;->detachListener()V

    :cond_1
    throw v1
.end method

.method public moveCursor(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;I)V
    .locals 10

    .prologue
    .line 1433
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    move v9, p3

    invoke-static/range {v0 .. v9}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_moveCursor(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;I)V

    .line 1434
    return-void
.end method

.method public onEventActiveChanged()V
    .locals 2

    .prologue
    .line 1441
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_onEventActiveChanged(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1442
    :goto_0
    return-void

    .line 1441
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_onEventActiveChangedSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    goto :goto_0
.end method

.method public onFocusChanged()V
    .locals 2

    .prologue
    .line 1556
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_onFocusChanged(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1557
    :goto_0
    return-void

    .line 1556
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_onFocusChangedSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    goto :goto_0
.end method

.method public onHoverChanged(Z)V
    .locals 2

    .prologue
    .line 1520
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_onHoverChanged(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V

    .line 1521
    :goto_0
    return-void

    .line 1520
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_onHoverChangedSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V

    goto :goto_0
.end method

.method public onHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 6

    .prologue
    .line 1524
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_onHoverEvent(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_onHoverEventSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
    .locals 6

    .prologue
    .line 1528
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_onKeyEvent(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;->getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_onKeyEventSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 389
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mClickListener:Lcom/samsung/android/sdk/sgi/ui/SGClickListener;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mLongClickListener:Lcom/samsung/android/sdk/sgi/ui/SGLongClickListener;

    if-eqz v0, :cond_2

    .line 391
    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget$1;->$SwitchMap$com$samsung$android$sdk$sgi$ui$SGTouchEvent$SGAction:[I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getAction()Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_0
    move v1, v2

    .line 473
    :cond_2
    return v1

    .line 394
    :pswitch_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPressed:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCanceled:Z

    if-nez v0, :cond_3

    .line 398
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isTouchFocusable()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isInFocus()Z

    move-result v0

    if-nez v0, :cond_6

    .line 400
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setFocus()Z

    move-result v0

    .line 403
    :goto_1
    iget-boolean v3, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHasPerformedLongPress:Z

    if-nez v3, :cond_3

    .line 405
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->cancelLongClick()V

    .line 407
    if-nez v0, :cond_3

    .line 409
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->checkForClick()Z

    move-result v0

    if-nez v0, :cond_3

    .line 411
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->performClick()Z

    .line 416
    :cond_3
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPressed:Z

    .line 417
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCanceled:Z

    goto :goto_0

    .line 422
    :pswitch_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 423
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 424
    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v4

    .line 426
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCanceled:Z

    .line 428
    rsub-int/lit8 v1, v4, 0x0

    if-ge v0, v1, :cond_4

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    int-to-float v5, v4

    add-float/2addr v1, v5

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_4

    rsub-int/lit8 v0, v4, 0x0

    if-ge v3, v0, :cond_4

    int-to-float v0, v3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    int-to-float v3, v4

    add-float/2addr v1, v3

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 431
    :cond_4
    iput-boolean v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPressed:Z

    .line 433
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mLongClickListener:Lcom/samsung/android/sdk/sgi/ui/SGLongClickListener;

    if-eqz v0, :cond_1

    .line 434
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->checkForLongClick()V

    goto :goto_0

    .line 441
    :pswitch_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 442
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    .line 443
    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v4

    .line 445
    rsub-int/lit8 v5, v4, 0x0

    if-lt v0, v5, :cond_5

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v5

    int-to-float v6, v4

    add-float/2addr v5, v6

    cmpl-float v0, v0, v5

    if-gez v0, :cond_5

    rsub-int/lit8 v0, v4, 0x0

    if-lt v3, v0, :cond_5

    int-to-float v0, v3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    int-to-float v4, v4

    add-float/2addr v3, v4

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_1

    .line 448
    :cond_5
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPressed:Z

    if-eqz v0, :cond_1

    .line 451
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->cancelLongClick()V

    .line 452
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->cancelClick()V

    .line 453
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPressed:Z

    goto/16 :goto_0

    .line 460
    :pswitch_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->cancelLongClick()V

    .line 461
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->cancelClick()V

    .line 462
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mPressed:Z

    .line 463
    iput-boolean v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCanceled:Z

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto/16 :goto_1

    .line 391
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public removeAllAnimations()V
    .locals 2

    .prologue
    .line 1369
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_removeAllAnimations(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1370
    return-void
.end method

.method public removeAllWidgets()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 967
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 969
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfCaptured:Z

    .line 970
    iput-boolean v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfHovered:Z

    .line 971
    iput-object v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 972
    iput-object v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mHoverCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 973
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeAllW()V

    .line 974
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 976
    :cond_0
    return-void
.end method

.method public removeAnimation(I)V
    .locals 2

    .prologue
    .line 1373
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_removeAnimation(JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)V

    .line 1374
    return-void
.end method

.method public removeWidget(I)V
    .locals 1

    .prologue
    .line 939
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getWidget(I)Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeCapturing(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 940
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeW(I)V

    .line 941
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 942
    return-void
.end method

.method public removeWidget(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 1

    .prologue
    .line 925
    if-eqz p1, :cond_0

    .line 927
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeCapturing(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 928
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeW(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 929
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 931
    :cond_0
    return-void
.end method

.method public removeWidgets(II)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 953
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeW(II)V

    move v0, p1

    .line 954
    :goto_0
    if-gt v0, p2, :cond_0

    .line 955
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getWidget(I)Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->removeCapturing(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 956
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 954
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 958
    :cond_0
    return-void
.end method

.method public sendToBack()V
    .locals 3

    .prologue
    .line 1081
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getParent()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    .line 1082
    if-eqz v0, :cond_1

    .line 1084
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->sendToB()V

    .line 1085
    iget-object v1, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1086
    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1096
    :cond_0
    :goto_0
    return-void

    .line 1090
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    .line 1091
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getWidgetIndex(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 1093
    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->sendWidgetToBack(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    goto :goto_0
.end method

.method public sendWidgetToBack(I)V
    .locals 3

    .prologue
    .line 1117
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 1118
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getParent()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v1

    .line 1119
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->sendWToB(I)V

    .line 1120
    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1121
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1124
    return-void
.end method

.method public sendWidgetToBack(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 2

    .prologue
    .line 1133
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter widget is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1134
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->getParent()Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    move-result-object v0

    .line 1135
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->sendWToB(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1136
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1137
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1138
    return-void
.end method

.method public setChildrenClipping(Z)V
    .locals 2

    .prologue
    .line 1453
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setChildrenClipping(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V

    .line 1454
    return-void
.end method

.method public setChildrenClippingEnabled(Z)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 241
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setChildrenClipping(Z)V

    .line 242
    return-void
.end method

.method public setEventActive(Z)V
    .locals 2

    .prologue
    .line 1253
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setEventActive(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V

    .line 1254
    return-void
.end method

.method public setFiltersOptions(ZZ)V
    .locals 2

    .prologue
    .line 1592
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setFiltersOptions(JLcom/samsung/android/sdk/sgi/ui/SGWidget;ZZ)V

    .line 1593
    return-void
.end method

.method public setFocus()Z
    .locals 2

    .prologue
    .line 1401
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setFocus(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z

    move-result v0

    return v0
.end method

.method public setFocusable(Z)V
    .locals 2

    .prologue
    .line 1540
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setFocusable(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V

    .line 1541
    :goto_0
    return-void

    .line 1540
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setFocusableSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V

    goto :goto_0
.end method

.method public setHoverListener(Lcom/samsung/android/sdk/sgi/ui/SGHoverListener;)V
    .locals 1

    .prologue
    .line 108
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setHoverListener(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V

    .line 109
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    iput-object p1, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mHoverListener:Lcom/samsung/android/sdk/sgi/ui/SGHoverListener;

    .line 110
    return-void

    .line 108
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setHoverable(Z)V
    .locals 2

    .prologue
    .line 1361
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setHoverable(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V

    .line 1362
    return-void
.end method

.method public setHovered(Z)V
    .locals 2

    .prologue
    .line 1353
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setHovered(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V

    .line 1354
    return-void
.end method

.method public setInheritOpacity(Z)V
    .locals 2

    .prologue
    .line 1218
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setInheritOpacity(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V

    .line 1219
    return-void
.end method

.method public setKeyEventListener(Lcom/samsung/android/sdk/sgi/ui/SGKeyEventListener;)V
    .locals 1

    .prologue
    .line 143
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setKeyEventListener(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V

    .line 144
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    iput-object p1, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mKeyEventListener:Lcom/samsung/android/sdk/sgi/ui/SGKeyEventListener;

    .line 145
    return-void

    .line 143
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLocalTransform(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
    .locals 6

    .prologue
    .line 1158
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setLocalTransform(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 1159
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1150
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setName(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Ljava/lang/String;)V

    .line 1151
    return-void
.end method

.method public setOnClickListener(Lcom/samsung/android/sdk/sgi/ui/SGClickListener;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mClickListener:Lcom/samsung/android/sdk/sgi/ui/SGClickListener;

    .line 178
    return-void
.end method

.method public setOnLongClickListener(Lcom/samsung/android/sdk/sgi/ui/SGLongClickListener;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mLongClickListener:Lcom/samsung/android/sdk/sgi/ui/SGLongClickListener;

    .line 194
    return-void
.end method

.method public setOpacity(F)V
    .locals 2

    .prologue
    .line 1214
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setOpacity(JLcom/samsung/android/sdk/sgi/ui/SGWidget;F)V

    .line 1215
    return-void
.end method

.method public setPivots(FF)V
    .locals 2

    .prologue
    .line 1712
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setPivots__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)V

    .line 1713
    return-void
.end method

.method public setPivots(FFF)V
    .locals 6

    .prologue
    .line 1716
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setPivots__SWIG_3(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FFF)V

    .line 1717
    return-void
.end method

.method public setPivots(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 1182
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setPivots__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 1183
    return-void
.end method

.method public setPivots(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 1186
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setPivots__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 1187
    return-void
.end method

.method public setPosition(FF)V
    .locals 2

    .prologue
    .line 1668
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setPosition__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)V

    .line 1669
    return-void
.end method

.method public setPosition(FFF)V
    .locals 6

    .prologue
    .line 1672
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setPosition__SWIG_3(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FFF)V

    .line 1673
    return-void
.end method

.method public setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 1162
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setPosition__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 1163
    return-void
.end method

.method public setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 1166
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setPosition__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 1167
    return-void
.end method

.method public setPositionPivot(FF)V
    .locals 2

    .prologue
    .line 1704
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setPositionPivot__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)V

    .line 1705
    return-void
.end method

.method public setPositionPivot(FFF)V
    .locals 6

    .prologue
    .line 1708
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setPositionPivot__SWIG_3(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FFF)V

    .line 1709
    return-void
.end method

.method public setPositionPivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 1206
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setPositionPivot__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 1207
    return-void
.end method

.method public setPositionPivot(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 1210
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setPositionPivot__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 1211
    return-void
.end method

.method public setRotation(FFF)V
    .locals 6

    .prologue
    .line 1226
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setRotation__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FFF)V

    .line 1227
    return-void
.end method

.method public setRotation(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
    .locals 6

    .prologue
    .line 1222
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setRotation__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 1223
    return-void
.end method

.method public setRotation(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 1242
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setRotation__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 1243
    return-void
.end method

.method public setRotationPivot(FF)V
    .locals 2

    .prologue
    .line 1696
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setRotationPivot__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)V

    .line 1697
    return-void
.end method

.method public setRotationPivot(FFF)V
    .locals 6

    .prologue
    .line 1700
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setRotationPivot__SWIG_3(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FFF)V

    .line 1701
    return-void
.end method

.method public setRotationPivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 1198
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setRotationPivot__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 1199
    return-void
.end method

.method public setRotationPivot(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 1202
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setRotationPivot__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 1203
    return-void
.end method

.method public setRotationX(F)V
    .locals 2

    .prologue
    .line 1230
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setRotationX(JLcom/samsung/android/sdk/sgi/ui/SGWidget;F)V

    .line 1231
    return-void
.end method

.method public setRotationY(F)V
    .locals 2

    .prologue
    .line 1234
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setRotationY(JLcom/samsung/android/sdk/sgi/ui/SGWidget;F)V

    .line 1235
    return-void
.end method

.method public setRotationZ(F)V
    .locals 2

    .prologue
    .line 1238
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setRotationZ(JLcom/samsung/android/sdk/sgi/ui/SGWidget;F)V

    .line 1239
    return-void
.end method

.method public setScale(FF)V
    .locals 2

    .prologue
    .line 1680
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setScale__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)V

    .line 1681
    return-void
.end method

.method public setScale(FFF)V
    .locals 6

    .prologue
    .line 1684
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setScale__SWIG_3(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FFF)V

    .line 1685
    return-void
.end method

.method public setScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 1174
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setScale__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 1175
    return-void
.end method

.method public setScale(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 1178
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setScale__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 1179
    return-void
.end method

.method public setScalePivot(FF)V
    .locals 2

    .prologue
    .line 1688
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setScalePivot__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)V

    .line 1689
    return-void
.end method

.method public setScalePivot(FFF)V
    .locals 6

    .prologue
    .line 1692
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setScalePivot__SWIG_3(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FFF)V

    .line 1693
    return-void
.end method

.method public setScalePivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 1190
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setScalePivot__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 1191
    return-void
.end method

.method public setScalePivot(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 1194
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setScalePivot__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 1195
    return-void
.end method

.method public setScreenShotOptions(ZZ)V
    .locals 2

    .prologue
    .line 1560
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setScreenShotOptions(JLcom/samsung/android/sdk/sgi/ui/SGWidget;ZZ)V

    .line 1561
    return-void
.end method

.method public setSize(FF)V
    .locals 2

    .prologue
    .line 1676
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setSize__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)V

    .line 1677
    return-void
.end method

.method public setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 1170
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setSize__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 1171
    return-void
.end method

.method public setTouchFirstSendToParent(Z)V
    .locals 2

    .prologue
    .line 1257
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setTouchFirstSendToParent(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V

    .line 1258
    return-void
.end method

.method public setTouchListener(Lcom/samsung/android/sdk/sgi/ui/SGTouchListener;)V
    .locals 1

    .prologue
    .line 92
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setTouchListener(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V

    .line 93
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    iput-object p1, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mTouchListener:Lcom/samsung/android/sdk/sgi/ui/SGTouchListener;

    .line 94
    return-void

    .line 92
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTransform(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 217
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setLocalTransform(Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 218
    return-void
.end method

.method public setTransformationHint(Lcom/samsung/android/sdk/sgi/vi/SGTransformationHints;)V
    .locals 3

    .prologue
    .line 1512
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/vi/SGTransformationHints;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setTransformationHint(JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)V

    .line 1513
    return-void
.end method

.method public setVisibility(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;)V
    .locals 3

    .prologue
    .line 1246
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1248
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->ordinal()I

    move-result v2

    invoke-static {v0, v1, p0, v2}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_setVisibility(JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)V

    .line 1250
    return-void
.end method

.method public setWidgetAnimationListener(Lcom/samsung/android/sdk/sgi/ui/SGWidgetAnimationListener;)V
    .locals 1

    .prologue
    .line 160
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setWidgetAnimationListener(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V

    .line 161
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    iput-object p1, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidgetAnimationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetAnimationListener;

    .line 162
    return-void

    .line 160
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setWidgetTransformationListener(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;)V
    .locals 1

    .prologue
    .line 126
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setWidgetTransformationListener(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V

    .line 127
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mWidgetListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;

    iput-object p1, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerHolder;->mWidgetTransformationListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTransformationListener;

    .line 128
    return-void

    .line 126
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showCursor(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;I)V
    .locals 10

    .prologue
    .line 1429
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    move v9, p3

    invoke-static/range {v0 .. v9}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGWidget_showCursor(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;I)V

    .line 1430
    return-void
.end method

.method public swapWidgets(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 3

    .prologue
    .line 1012
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->swapW(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V

    .line 1013
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1014
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 1015
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mChildArray:Ljava/util/ArrayList;

    invoke-static {v2, v0, v1}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 1016
    return-void
.end method

.method public uncaptureWidget()V
    .locals 1

    .prologue
    .line 763
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mSelfCaptured:Z

    .line 765
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    if-eqz v0, :cond_0

    .line 766
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->uncaptureWidget()V

    .line 767
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mTouchCapturedWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 769
    :cond_0
    return-void
.end method
