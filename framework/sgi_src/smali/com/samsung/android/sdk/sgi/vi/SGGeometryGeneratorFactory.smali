.class public final Lcom/samsung/android/sdk/sgi/vi/SGGeometryGeneratorFactory;
.super Ljava/lang/Object;
.source "SGGeometryGeneratorFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createCircleGeometryGenerator(Lcom/samsung/android/sdk/sgi/base/SGVector2f;FI)Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;
    .locals 4

    .prologue
    .line 33
    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v0

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGGeometryGeneratorFactory_createCircleGeometryGenerator(JLcom/samsung/android/sdk/sgi/base/SGVector2f;FI)J

    move-result-wide v2

    .line 35
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 36
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGeneratorHolder;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGeneratorHolder;-><init>(JZ)V

    goto :goto_0
.end method

.method public static createRectGeometryGenerator(Landroid/graphics/RectF;Landroid/graphics/RectF;)Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;
    .locals 4

    .prologue
    .line 40
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_1
    invoke-static {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGGeometryGeneratorFactory_createRectGeometryGenerator(Landroid/graphics/RectF;Landroid/graphics/RectF;)J

    move-result-wide v2

    .line 45
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 46
    :goto_0
    return-object v0

    :cond_2
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGeneratorHolder;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGeneratorHolder;-><init>(JZ)V

    goto :goto_0
.end method

.method public static createRoundBorderGeometryGenerator(Landroid/graphics/RectF;FFFFI)Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;
    .locals 4

    .prologue
    .line 51
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    invoke-static/range {p0 .. p5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGGeometryGeneratorFactory_createRoundBorderGeometryGenerator(Landroid/graphics/RectF;FFFFI)J

    move-result-wide v2

    .line 55
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 56
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGeneratorHolder;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGeneratorHolder;-><init>(JZ)V

    goto :goto_0
.end method

.method public static createRoundRectGeometryGenerator(Landroid/graphics/RectF;FFI)Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;
    .locals 4

    .prologue
    .line 61
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGGeometryGeneratorFactory_createRoundRectGeometryGenerator(Landroid/graphics/RectF;FFI)J

    move-result-wide v2

    .line 65
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 66
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGeneratorHolder;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGeneratorHolder;-><init>(JZ)V

    goto :goto_0
.end method

.method public static createTriangleGeometryGenerator(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;
    .locals 9

    .prologue
    .line 71
    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v0

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v6

    move-object v2, p0

    move-object v5, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGGeometryGeneratorFactory_createTriangleGeometryGenerator(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v2

    .line 73
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 74
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGeneratorHolder;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGeneratorHolder;-><init>(JZ)V

    goto :goto_0
.end method

.method private deleteGeometryGeneratorFactory()V
    .locals 0

    .prologue
    .line 29
    return-void
.end method
