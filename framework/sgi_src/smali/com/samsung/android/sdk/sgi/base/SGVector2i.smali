.class public Lcom/samsung/android/sdk/sgi/base/SGVector2i;
.super Ljava/lang/Object;
.source "SGVector2i.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGVector2i__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;-><init>(JZ)V

    .line 47
    return-void
.end method

.method public constructor <init>(II)V
    .locals 3

    .prologue
    .line 54
    invoke-static {p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGVector2i__SWIG_2(II)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;-><init>(JZ)V

    .line 55
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCMemOwn:Z

    .line 27
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)V
    .locals 3

    .prologue
    .line 50
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGVector2i__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;-><init>(JZ)V

    .line 51
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J
    .locals 2

    .prologue
    .line 31
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)V
    .locals 6

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_add(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)V

    .line 111
    return-void
.end method

.method public distanceSqr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)I
    .locals 6

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_distanceSqr(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)I

    move-result v0

    return v0
.end method

.method public divide(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)V
    .locals 6

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_divide(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)V

    .line 123
    return-void
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 35
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 36
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCMemOwn:Z

    .line 38
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->delete_SGVector2i(J)V

    .line 40
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    .line 42
    :cond_1
    return-void
.end method

.method public getDistance(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)F
    .locals 6

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_getDistance(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)F

    move-result v0

    return v0
.end method

.method public getDotProduct(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)I
    .locals 6

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_getDotProduct(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)I

    move-result v0

    return v0
.end method

.method public getLength()F
    .locals 2

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_getLength(JLcom/samsung/android/sdk/sgi/base/SGVector2i;)F

    move-result v0

    return v0
.end method

.method public getX()I
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_getX(JLcom/samsung/android/sdk/sgi/base/SGVector2i;)I

    move-result v0

    return v0
.end method

.method public getY()I
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_getY(JLcom/samsung/android/sdk/sgi/base/SGVector2i;)I

    move-result v0

    return v0
.end method

.method public isEqual(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)Z
    .locals 6

    .prologue
    .line 106
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_isEqual(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)Z

    move-result v0

    return v0
.end method

.method public multiply(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)V
    .locals 6

    .prologue
    .line 118
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_multiply(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)V

    .line 119
    return-void
.end method

.method public normalize()V
    .locals 2

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_normalize(JLcom/samsung/android/sdk/sgi/base/SGVector2i;)V

    .line 99
    return-void
.end method

.method public scale(I)V
    .locals 2

    .prologue
    .line 102
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_scale(JLcom/samsung/android/sdk/sgi/base/SGVector2i;I)V

    .line 103
    return-void
.end method

.method public set(II)V
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_set(JLcom/samsung/android/sdk/sgi/base/SGVector2i;II)V

    .line 75
    return-void
.end method

.method public setAt(II)V
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_setAt(JLcom/samsung/android/sdk/sgi/base/SGVector2i;II)V

    .line 79
    return-void
.end method

.method public setX(I)V
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_setX(JLcom/samsung/android/sdk/sgi/base/SGVector2i;I)V

    .line 63
    return-void
.end method

.method public setY(I)V
    .locals 2

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_setY(JLcom/samsung/android/sdk/sgi/base/SGVector2i;I)V

    .line 71
    return-void
.end method

.method public subtract(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)V
    .locals 6

    .prologue
    .line 114
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2i;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2i;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGVector2i_subtract(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)V

    .line 115
    return-void
.end method
