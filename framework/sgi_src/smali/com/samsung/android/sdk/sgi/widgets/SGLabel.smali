.class public Lcom/samsung/android/sdk/sgi/widgets/SGLabel;
.super Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;
.source "SGLabel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/widgets/SGLabel$SGStateListeningView;
    }
.end annotation


# static fields
.field protected static final mDefaultSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;


# instance fields
.field protected androidComponent:Landroid/widget/TextView;

.field mAutoSizeEnabled:Z

.field private mBackgroundColor:I

.field private mIncludeFontPadding:Z

.field private mTextMeasureView:Landroid/widget/TextView;

.field private maxLines:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/high16 v1, 0x41a00000    # 20.0f

    .line 51
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, v1, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mDefaultSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 72
    const-string v0, ""

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 1

    .prologue
    .line 131
    const-string v0, ""

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/base/SGVector2f;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 59
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, p2, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/graphics/Bitmap$Config;)V

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mIncludeFontPadding:Z

    .line 48
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->maxLines:I

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mBackgroundColor:I

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mTextMeasureView:Landroid/widget/TextView;

    .line 61
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->createComponent(Landroid/content/Context;)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    .line 62
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->maxLines:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 64
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->setupDefaultTextColors(Landroid/content/Context;)V

    .line 65
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 80
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mDefaultSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/graphics/Bitmap$Config;)V

    .line 47
    iput-boolean v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mIncludeFontPadding:Z

    .line 48
    const v0, 0x7fffffff

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->maxLines:I

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mBackgroundColor:I

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mTextMeasureView:Landroid/widget/TextView;

    .line 82
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->createComponent(Landroid/content/Context;)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    .line 83
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->maxLines:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 85
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->setupDefaultTextColors(Landroid/content/Context;)V

    .line 86
    iput-boolean v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mAutoSizeEnabled:Z

    .line 87
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->makeAutosize()V

    .line 88
    return-void
.end method

.method private getMeasureSpec()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 497
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 498
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>()V

    .line 525
    :goto_0
    return-object v0

    .line 500
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mTextMeasureView:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 501
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mTextMeasureView:Landroid/widget/TextView;

    .line 503
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mTextMeasureView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 504
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mTextMeasureView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 505
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mTextMeasureView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 506
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 507
    if-nez v0, :cond_2

    .line 508
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    .line 510
    :cond_2
    if-eqz v0, :cond_3

    .line 511
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mTextMeasureView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 513
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mTextMeasureView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 514
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mTextMeasureView:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 515
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mTextMeasureView:Landroid/widget/TextView;

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->maxLines:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 517
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 518
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 520
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mTextMeasureView:Landroid/widget/TextView;

    invoke-virtual {v2, v0, v1}, Landroid/widget/TextView;->measure(II)V

    .line 522
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mTextMeasureView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    .line 523
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mTextMeasureView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    .line 525
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    int-to-float v1, v1

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    goto :goto_0
.end method

.method private makeAutosize()V
    .locals 5

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 94
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mAutoSizeEnabled:Z

    if-nez v0, :cond_0

    .line 108
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-static {v4, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v4, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->measure(II)V

    .line 99
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 100
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 101
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v2, v0, v1}, Landroid/widget/TextView;->measure(II)V

    .line 102
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    .line 103
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 105
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 106
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v2, v4, v4, v0, v1}, Landroid/widget/TextView;->layout(IIII)V

    .line 107
    int-to-float v0, v0

    int-to-float v1, v1

    invoke-super {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->setSize(FF)V

    goto :goto_0
.end method


# virtual methods
.method protected createComponent(Landroid/content/Context;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 139
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel$SGStateListeningView;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel$SGStateListeningView;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGLabel;Landroid/content/Context;)V

    return-object v0
.end method

.method public getBackground()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final getBackgroundColor()I
    .locals 1

    .prologue
    .line 274
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mBackgroundColor:I

    return v0
.end method

.method public final getCurrentTextColor()I
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v0

    return v0
.end method

.method public final getDrawingCacheBackgroundColor()I
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getDrawingCacheBackgroundColor()I

    move-result v0

    return v0
.end method

.method public getEllipsize()Landroid/text/TextUtils$TruncateAt;
    .locals 1

    .prologue
    .line 571
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getEllipsize()Landroid/text/TextUtils$TruncateAt;

    move-result-object v0

    return-object v0
.end method

.method public getGravity()I
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getGravity()I

    move-result v0

    return v0
.end method

.method public getHint()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getIncludeFontPadding()Z
    .locals 1

    .prologue
    .line 311
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mIncludeFontPadding:Z

    return v0
.end method

.method protected getMaxLines()I
    .locals 1

    .prologue
    .line 435
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->maxLines:I

    return v0
.end method

.method public final getPaddingBottom()I
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v0

    return v0
.end method

.method public final getPaddingLeft()I
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v0

    return v0
.end method

.method public final getPaddingRight()I
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v0

    return v0
.end method

.method public final getPaddingTop()I
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v0

    return v0
.end method

.method public final getPaint()Landroid/text/TextPaint;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    return-object v0
.end method

.method public final getText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getTextColors()Landroid/content/res/ColorStateList;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method public getTextHeight()F
    .locals 1

    .prologue
    .line 491
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->getMeasureSpec()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    return v0
.end method

.method public final getTextScaleX()F
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextScaleX()F

    move-result v0

    return v0
.end method

.method public final getTextSize()F
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextSize()F

    move-result v0

    return v0
.end method

.method public getTextWidth()F
    .locals 1

    .prologue
    .line 484
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->getMeasureSpec()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    return v0
.end method

.method public final getTypeFace()Landroid/graphics/Typeface;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method public final isEnabled()Z
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 631
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    .line 632
    return-void
.end method

.method protected onDrawableStateChanged()V
    .locals 0

    .prologue
    .line 393
    return-void
.end method

.method public final setBackgroundColor(I)V
    .locals 2

    .prologue
    .line 264
    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mBackgroundColor:I

    .line 265
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    iget v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mBackgroundColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 266
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 267
    return-void
.end method

.method public final setBackgroundResource(I)V
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 402
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->makeAutosize()V

    .line 403
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 404
    return-void
.end method

.method public final setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 411
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->makeAutosize()V

    .line 412
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 413
    return-void
.end method

.method public setEllipsize(Landroid/text/TextUtils$TruncateAt;)V
    .locals 1

    .prologue
    .line 561
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 562
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->makeAutosize()V

    .line 563
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 564
    return-void
.end method

.method public setEms(I)V
    .locals 2

    .prologue
    .line 549
    int-to-float v0, p1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->getTextHeight()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 550
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEms(I)V

    .line 551
    int-to-float v0, p1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->getTextHeight()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->setSize(FF)V

    .line 553
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 532
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 533
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->setEventActive(Z)V

    .line 534
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 535
    return-void
.end method

.method public setGravity(I)V
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setGravity(I)V

    .line 349
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 350
    return-void
.end method

.method public setHint(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 174
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->makeAutosize()V

    .line 175
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 176
    return-void
.end method

.method public setHorizontallyScrolling(Z)V
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setHorizontallyScrolling(Z)V

    .line 598
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 599
    return-void
.end method

.method public final setIncludeFontPadding(Z)V
    .locals 1

    .prologue
    .line 301
    iput-boolean p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mIncludeFontPadding:Z

    .line 302
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 303
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->makeAutosize()V

    .line 304
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 305
    return-void
.end method

.method public setLineSpacing(FF)V
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 444
    return-void
.end method

.method public setMaxLines(I)V
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    iput p1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->maxLines:I

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 427
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->makeAutosize()V

    .line 428
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 429
    return-void
.end method

.method public setPadding(IIII)V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 292
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->makeAutosize()V

    .line 293
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 294
    return-void
.end method

.method public setSingleLine()V
    .locals 1

    .prologue
    .line 589
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->setSingleLine(Z)V

    .line 590
    return-void
.end method

.method public setSingleLine(Z)V
    .locals 1

    .prologue
    .line 581
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 582
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 583
    return-void
.end method

.method public setSize(FF)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 617
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->setSize(FF)V

    .line 618
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    float-to-int v2, p1

    float-to-int v3, p2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 619
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    float-to-int v1, p1

    float-to-int v2, p2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/widget/TextView;->layout(IIII)V

    .line 620
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    float-to-int v1, p1

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    float-to-int v2, p2

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->measure(II)V

    .line 622
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 623
    iput-boolean v4, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->mAutoSizeEnabled:Z

    .line 624
    return-void
.end method

.method public setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 2

    .prologue
    .line 607
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->setSize(FF)V

    .line 608
    return-void
.end method

.method public setText(I)V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 157
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->makeAutosize()V

    .line 158
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 159
    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->makeAutosize()V

    .line 148
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 149
    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 200
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 201
    return-void
.end method

.method public setTextColor(Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 192
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 193
    return-void
.end method

.method public final setTextScaleX(F)V
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextScaleX(F)V

    .line 249
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->makeAutosize()V

    .line 250
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 251
    return-void
.end method

.method public setTextSize(F)V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 222
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->makeAutosize()V

    .line 223
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 224
    return-void
.end method

.method public setTextSize(IF)V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 233
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->makeAutosize()V

    .line 234
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 235
    return-void
.end method

.method public setTypeFace(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 451
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->makeAutosize()V

    .line 452
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 453
    return-void
.end method

.method public setTypeFace(Landroid/graphics/Typeface;I)V
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 476
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->makeAutosize()V

    .line 477
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->invalidate()V

    .line 478
    return-void
.end method

.method setupDefaultTextColors(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 114
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 115
    const v1, 0x101009a

    aput v1, v0, v3

    .line 116
    const v1, 0x1010036

    aput v1, v0, v4

    .line 117
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    const v2, -0x333334

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setHintTextColor(I)V

    .line 120
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 121
    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGLabel;->androidComponent:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 123
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 124
    return-void
.end method
