.class public final Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;
.super Lcom/samsung/android/sdk/sgi/vi/SGLayer;
.source "SGLayerCanvas.java"


# instance fields
.field private mCanvasRedrawListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerHolder;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 85
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGLayerCanvas()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;-><init>(JZ)V

    .line 86
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->init()V

    .line 87
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;-><init>(JZ)V

    .line 32
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerHolder;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerHolder;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->mCanvasRedrawListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerHolder;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap$Config;)V
    .locals 3

    .prologue
    .line 61
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGLayerCanvas()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;-><init>(JZ)V

    .line 62
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->init()V

    .line 63
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->setFormat(Landroid/graphics/Bitmap$Config;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/graphics/RectF;)V
    .locals 3

    .prologue
    .line 51
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGLayerCanvas()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;-><init>(JZ)V

    .line 52
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->init(Landroid/graphics/RectF;)V

    .line 53
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerCanvas_init__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;)V

    .line 91
    return-void
.end method

.method private init(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 94
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerCanvas_init__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;Landroid/graphics/RectF;)V

    .line 98
    return-void
.end method

.method private setRedrawListener(Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerBase;)V
    .locals 6

    .prologue
    .line 129
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerBase;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerBase;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerCanvas_setRedrawListener(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerBase;)V

    .line 130
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 36
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 37
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCMemOwn:Z

    .line 39
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->delete_SGLayerCanvas(J)V

    .line 41
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    .line 43
    :cond_1
    return-void
.end method

.method public getCanvasScale()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 114
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerCanvas_getCanvasScale(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getContentRect()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 140
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerCanvas_getContentRect(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getContentRectPivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 148
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerCanvas_getContentRectPivot(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getContentRectScale()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 156
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerCanvas_getContentRectScale(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getFormat()Landroid/graphics/Bitmap$Config;
    .locals 2

    .prologue
    .line 102
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerCanvas_getFormat(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;)Landroid/graphics/Bitmap$Config;

    move-result-object v0

    return-object v0
.end method

.method public getRedrawListener()Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListener;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->mCanvasRedrawListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerHolder;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerHolder;->getInterface()Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListener;

    move-result-object v0

    return-object v0
.end method

.method public invalidate()V
    .locals 2

    .prologue
    .line 118
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerCanvas_invalidate__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;)V

    .line 119
    return-void
.end method

.method public invalidate(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 122
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerCanvas_invalidate__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;Landroid/graphics/RectF;)V

    .line 126
    return-void
.end method

.method public setCanvasScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerCanvas_setCanvasScale(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 111
    return-void
.end method

.method public setContentRect(Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    .line 133
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerCanvas_setContentRect(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;Landroid/graphics/RectF;)V

    .line 137
    return-void
.end method

.method public setContentRectPivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 144
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerCanvas_setContentRectPivot(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 145
    return-void
.end method

.method public setContentRectScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 152
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerCanvas_setContentRectScale(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 153
    return-void
.end method

.method public setFormat(Landroid/graphics/Bitmap$Config;)V
    .locals 2

    .prologue
    .line 106
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGLayerCanvas_setFormat(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;Landroid/graphics/Bitmap$Config;)V

    .line 107
    return-void
.end method

.method public setRedrawListener(Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListener;)V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->mCanvasRedrawListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerHolder;->setInterface(Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListener;)V

    .line 73
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->setRedrawListener(Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerBase;)V

    .line 74
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;->mCanvasRedrawListener:Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerHolder;

    goto :goto_0
.end method
