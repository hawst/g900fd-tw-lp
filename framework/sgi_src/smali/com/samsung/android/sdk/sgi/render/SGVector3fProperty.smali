.class public Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGProperty;
.source "SGVector3fProperty.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 49
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGVector3fProperty()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;-><init>(JZ)V

    .line 50
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->init()V

    .line 51
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 3

    .prologue
    .line 44
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGVector3fProperty()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;-><init>(JZ)V

    .line 45
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->init(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 46
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGVector3fProperty_init__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGVector3fProperty;)V

    .line 55
    return-void
.end method

.method private init(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGVector3fProperty_init__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGVector3fProperty;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 59
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;)Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;
    .locals 7

    .prologue
    .line 74
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGVector3fProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGVector3fProperty;JLcom/samsung/android/sdk/sgi/render/SGVector3fProperty;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGVector3fProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public get()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 70
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGVector3fProperty_get(JLcom/samsung/android/sdk/sgi/render/SGVector3fProperty;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public set(FFF)V
    .locals 6

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGVector3fProperty_set__SWIG_1(JLcom/samsung/android/sdk/sgi/render/SGVector3fProperty;FFF)V

    .line 67
    return-void
.end method

.method public set(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V
    .locals 6

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGVector3fProperty_set__SWIG_0(JLcom/samsung/android/sdk/sgi/render/SGVector3fProperty;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 63
    return-void
.end method
