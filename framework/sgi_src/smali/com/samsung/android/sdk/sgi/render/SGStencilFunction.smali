.class public final enum Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;
.super Ljava/lang/Enum;
.source "SGStencilFunction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

.field public static final enum ALWAYS:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

.field public static final enum EQUAL:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

.field public static final enum GEQUAL:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

.field public static final enum GREATER:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

.field public static final enum LEQUAL:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

.field public static final enum LESS:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

.field public static final enum NEVER:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

.field public static final enum NOT_EQUAL:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    const-string v1, "ALWAYS"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->ALWAYS:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    const-string v1, "NEVER"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->NEVER:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    const-string v1, "LESS"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->LESS:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    const-string v1, "LEQUAL"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->LEQUAL:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    .line 23
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    const-string v1, "GREATER"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->GREATER:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    .line 24
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    const-string v1, "GEQUAL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->GEQUAL:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    .line 25
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    const-string v1, "EQUAL"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->EQUAL:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    .line 26
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    const-string v1, "NOT_EQUAL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->NOT_EQUAL:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    .line 18
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->ALWAYS:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->NEVER:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->LESS:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->LEQUAL:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->GREATER:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->GEQUAL:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->EQUAL:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->NOT_EQUAL:Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGStencilFunction;

    return-object v0
.end method
