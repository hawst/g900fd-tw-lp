.class public abstract Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;
.super Ljava/lang/Object;
.source "SGFpsIndicator.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 128
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGFpsIndicator()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;-><init>(JZ)V

    .line 129
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCMemOwn:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFpsIndicator_director_connect(Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;JZZ)V

    .line 130
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    invoke-virtual {v0, p0, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    .line 131
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCMemOwn:Z

    .line 32
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/graphics/RectF;F)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;-><init>()V

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->init(Landroid/graphics/RectF;F)V

    .line 59
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;-><init>()V

    .line 68
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->init(Z)V

    .line 69
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;)J
    .locals 2

    .prologue
    .line 36
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    goto :goto_0
.end method

.method private init(Landroid/graphics/RectF;F)V
    .locals 2

    .prologue
    .line 134
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "parameter RectF is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 136
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFpsIndicator_init__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;Landroid/graphics/RectF;F)V

    .line 138
    return-void
.end method

.method private init(Z)V
    .locals 2

    .prologue
    .line 141
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFpsIndicator_init__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;Z)V

    .line 142
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 40
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Deregister(J)Z

    .line 41
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 42
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCMemOwn:Z

    .line 44
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->delete_SGFpsIndicator(J)V

    .line 46
    :cond_0
    iput-wide v4, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    .line 48
    :cond_1
    return-void
.end method

.method public getAccumulativeFps()F
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->getFps()F

    move-result v0

    return v0
.end method

.method public getApplyAnimationFps()F
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->getFps()F

    move-result v0

    return v0
.end method

.method public getDrawWidgetCanvasFps()F
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->getFps()F

    move-result v0

    return v0
.end method

.method public getFlashFps()F
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->getFps()F

    move-result v0

    return v0
.end method

.method public getFps()F
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFpsIndicator_getFps(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;)F

    move-result v0

    return v0
.end method

.method protected getPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 167
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFpsIndicator_getPosition(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getRenderFps()F
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->getFps()F

    move-result v0

    return v0
.end method

.method public getScreenSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 149
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFpsIndicator_getScreenSize(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method protected getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 4

    .prologue
    .line 163
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFpsIndicator_getSize(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    return-object v0
.end method

.method public getSyncUIAndVisualModelsFps()F
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->getFps()F

    move-result v0

    return v0
.end method

.method public getSyncVisualAndRenderModelsFps()F
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->getFps()F

    move-result v0

    return v0
.end method

.method protected abstract onDraw(Landroid/graphics/Bitmap;)V
.end method

.method protected setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 159
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFpsIndicator_setPosition(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 160
    return-void
.end method

.method protected setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 6

    .prologue
    .line 155
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGFpsIndicator_setSize(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 156
    return-void
.end method
