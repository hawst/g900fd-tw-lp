.class public final enum Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;
.super Ljava/lang/Enum;
.source "SGAccelerateTimingFunctionType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

.field public static final enum ACCELERATE:Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

.field public static final enum ACCELERATE_DECELERATE:Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

.field public static final enum DECELERATE:Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

    const-string v1, "ACCELERATE"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;->ACCELERATE:Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

    const-string v1, "DECELERATE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;->DECELERATE:Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

    const-string v1, "ACCELERATE_DECELERATE"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;->ACCELERATE_DECELERATE:Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

    .line 18
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;->ACCELERATE:Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;->DECELERATE:Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;->ACCELERATE_DECELERATE:Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;->$VALUES:[Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;->$VALUES:[Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/animation/SGAccelerateTimingFunctionType;

    return-object v0
.end method
