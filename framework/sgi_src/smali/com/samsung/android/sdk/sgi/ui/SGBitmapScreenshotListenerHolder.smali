.class final Lcom/samsung/android/sdk/sgi/ui/SGBitmapScreenshotListenerHolder;
.super Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;
.source "SGBitmapScreenshotListenerHolder.java"


# instance fields
.field mListener:Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;

.field mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGBitmapScreenshotListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    .line 32
    iput-object p2, p0, Lcom/samsung/android/sdk/sgi/ui/SGBitmapScreenshotListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;

    .line 33
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGBitmapScreenshotListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mAsyncScreenshotListenersArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    return-void
.end method


# virtual methods
.method public detachListener()V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGBitmapScreenshotListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mAsyncScreenshotListenersArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 39
    return-void
.end method

.method public onCompleted(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGBitmapScreenshotListenerHolder;->mWidget:Lcom/samsung/android/sdk/sgi/ui/SGWidget;

    iget-object v0, v0, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->mAsyncScreenshotListenersArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 47
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGBitmapScreenshotListenerHolder;->mListener:Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGBitmapScreenshotListener;->onCompleted(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 51
    const-string v1, "SGBitmapScreenshotListener::onCompleted error: uncaught exception"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGUIException;->handle(Ljava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method
