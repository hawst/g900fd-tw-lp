.class Lcom/samsung/android/sdk/sgi/ui/SGJNI;
.super Ljava/lang/Object;
.source "SGJNI.java"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 26
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->initLibrary()V

    .line 600
    invoke-static {}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->swig_module_init()V

    .line 601
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native SGGraphicBufferScreenshotListenerUIBase_change_ownership(Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;JZ)V
.end method

.method public static final native SGGraphicBufferScreenshotListenerUIBase_director_connect(Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;JZZ)V
.end method

.method public static final native SGGraphicBufferScreenshotListenerUIBase_onCompleted(JLcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;Landroid/graphics/Bitmap;)V
.end method

.method public static final native SGKeyEvent_getCharacter(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Ljava/lang/String;
.end method

.method public static final native SGKeyEvent_getDownTime(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Ljava/util/Date;
.end method

.method public static final native SGKeyEvent_getEventTime(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Ljava/util/Date;
.end method

.method public static final native SGKeyEvent_getKeyAction(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)I
.end method

.method public static final native SGKeyEvent_getKeyCode(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)I
.end method

.method public static final native SGKeyEvent_getKeyFlag(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)I
.end method

.method public static final native SGKeyEvent_getRepeatCount(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)I
.end method

.method public static final native SGKeyEvent_isCapsLockOn(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGKeyEvent_isFunctionPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGKeyEvent_isLeftAltPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGKeyEvent_isLeftCtrlPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGKeyEvent_isLeftShiftPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGKeyEvent_isMetaPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGKeyEvent_isNumLockOn(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGKeyEvent_isPrintable(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGKeyEvent_isRightAltPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGKeyEvent_isRightCtrlPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGKeyEvent_isRightShiftPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGKeyEvent_isScrollLockOn(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGKeyEvent_isSymPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGKeyEvent_isSystem(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGKeyEvent_resetKeyEvent(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)V
.end method

.method public static final native SGKeyEvent_setCapsLockOn(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V
.end method

.method public static final native SGKeyEvent_setDownTime(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Ljava/util/Date;)V
.end method

.method public static final native SGKeyEvent_setEventTime(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Ljava/util/Date;)V
.end method

.method public static final native SGKeyEvent_setFunctionPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V
.end method

.method public static final native SGKeyEvent_setKeyEvent__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;IILjava/util/Date;Ljava/lang/String;I)V
.end method

.method public static final native SGKeyEvent_setKeyEvent__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;IILjava/util/Date;Ljava/lang/String;)V
.end method

.method public static final native SGKeyEvent_setLeftAltPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V
.end method

.method public static final native SGKeyEvent_setLeftCtrlPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V
.end method

.method public static final native SGKeyEvent_setLeftShiftPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V
.end method

.method public static final native SGKeyEvent_setMetaPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V
.end method

.method public static final native SGKeyEvent_setNumLockOn(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V
.end method

.method public static final native SGKeyEvent_setPrintable(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V
.end method

.method public static final native SGKeyEvent_setRepeatCount(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;I)V
.end method

.method public static final native SGKeyEvent_setRightAltPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V
.end method

.method public static final native SGKeyEvent_setRightCtrlPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V
.end method

.method public static final native SGKeyEvent_setRightShiftPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V
.end method

.method public static final native SGKeyEvent_setScrollLockOn(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V
.end method

.method public static final native SGKeyEvent_setSymPressed(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V
.end method

.method public static final native SGKeyEvent_setSystem(JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;Z)V
.end method

.method public static final native SGPropertyScreenshotListenerUIBase_change_ownership(Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;JZ)V
.end method

.method public static final native SGPropertyScreenshotListenerUIBase_director_connect(Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;JZZ)V
.end method

.method public static final native SGPropertyScreenshotListenerUIBase_onCompleted(JLcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;J)V
.end method

.method public static final native SGSurfaceRendererBase_change_ownership(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;JZ)V
.end method

.method public static final native SGSurfaceRendererBase_director_connect(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;JZZ)V
.end method

.method public static final native SGSurfaceRendererBase_onDraw(JLcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;I)V
.end method

.method public static final native SGTouchEvent_addPointer(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;IFFFFIFI)V
.end method

.method public static final native SGTouchEvent_appendHistoryEntry(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Ljava/util/Date;)V
.end method

.method public static final native SGTouchEvent_appendPointerToHistory(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;FFF)V
.end method

.method public static final native SGTouchEvent_getAction(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)I
.end method

.method public static final native SGTouchEvent_getDefaultAction(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)I
.end method

.method public static final native SGTouchEvent_getDownTime(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Ljava/util/Date;
.end method

.method public static final native SGTouchEvent_getHistoricalEventTime(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)Ljava/util/Date;
.end method

.method public static final native SGTouchEvent_getHistoricalPressure(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;II)F
.end method

.method public static final native SGTouchEvent_getHistoricalX(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;II)F
.end method

.method public static final native SGTouchEvent_getHistoricalY(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;II)F
.end method

.method public static final native SGTouchEvent_getHistorySize(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)I
.end method

.method public static final native SGTouchEvent_getPointerCount(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)I
.end method

.method public static final native SGTouchEvent_getPointerId(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)I
.end method

.method public static final native SGTouchEvent_getPressure__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)F
.end method

.method public static final native SGTouchEvent_getPressure__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)F
.end method

.method public static final native SGTouchEvent_getRawX__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)F
.end method

.method public static final native SGTouchEvent_getRawX__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)F
.end method

.method public static final native SGTouchEvent_getRawY__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)F
.end method

.method public static final native SGTouchEvent_getRawY__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)F
.end method

.method public static final native SGTouchEvent_getTouchTime(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Ljava/util/Date;
.end method

.method public static final native SGTouchEvent_getX__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)F
.end method

.method public static final native SGTouchEvent_getX__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)F
.end method

.method public static final native SGTouchEvent_getY__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)F
.end method

.method public static final native SGTouchEvent_getY__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;I)F
.end method

.method public static final native SGTouchEvent_setA(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;II)V
.end method

.method public static final native SGTouchEvent_setDownTime(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Ljava/util/Date;)V
.end method

.method public static final native SGTouchEvent_setTouchTime(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Ljava/util/Date;)V
.end method

.method public static final native SGTouchEvent_setX(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;IF)V
.end method

.method public static final native SGTouchEvent_setY(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;IF)V
.end method

.method public static final native SGWidgetCanvas_SWIGUpcast(J)J
.end method

.method public static final native SGWidgetCanvas_change_ownership(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;JZ)V
.end method

.method public static final native SGWidgetCanvas_director_connect(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;JZZ)V
.end method

.method public static final native SGWidgetCanvas_getCanvasScale(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)J
.end method

.method public static final native SGWidgetCanvas_getContentRect(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)Landroid/graphics/RectF;
.end method

.method public static final native SGWidgetCanvas_getContentRectPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)J
.end method

.method public static final native SGWidgetCanvas_getContentRectScale(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)J
.end method

.method public static final native SGWidgetCanvas_getFormat(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)Landroid/graphics/Bitmap$Config;
.end method

.method public static final native SGWidgetCanvas_init__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/graphics/Bitmap$Config;)V
.end method

.method public static final native SGWidgetCanvas_init__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidgetCanvas_invalidateSwigExplicitSGWidgetCanvas__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)V
.end method

.method public static final native SGWidgetCanvas_invalidateSwigExplicitSGWidgetCanvas__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;Landroid/graphics/RectF;)V
.end method

.method public static final native SGWidgetCanvas_invalidate__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)V
.end method

.method public static final native SGWidgetCanvas_invalidate__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;Landroid/graphics/RectF;)V
.end method

.method public static final native SGWidgetCanvas_onDraw(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;Landroid/graphics/Canvas;)V
.end method

.method public static final native SGWidgetCanvas_setCanvasScaleSwigExplicitSGWidgetCanvas__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidgetCanvas_setCanvasScale__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidgetCanvas_setCanvasScale__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;FF)V
.end method

.method public static final native SGWidgetCanvas_setContentRect(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;Landroid/graphics/RectF;)V
.end method

.method public static final native SGWidgetCanvas_setContentRectPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidgetCanvas_setContentRectScale(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidgetCanvas_setFormat(JLcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;Landroid/graphics/Bitmap$Config;)V
.end method

.method public static final native SGWidgetDecorator_addFilter(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGFilter;)V
.end method

.method public static final native SGWidgetDecorator_addL__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)I
.end method

.method public static final native SGWidgetDecorator_addL__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)I
.end method

.method public static final native SGWidgetDecorator_bringLToF__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;I)V
.end method

.method public static final native SGWidgetDecorator_bringLToF__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGWidgetDecorator_findLayerById(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
.end method

.method public static final native SGWidgetDecorator_findLayerByName(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
.end method

.method public static final native SGWidgetDecorator_getFilter(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;I)J
.end method

.method public static final native SGWidgetDecorator_getFilterCount(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;)I
.end method

.method public static final native SGWidgetDecorator_getGeometryGeneratorParam(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;)F
.end method

.method public static final native SGWidgetDecorator_getHandle(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;)J
.end method

.method public static final native SGWidgetDecorator_getProgramProperty__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)Z
.end method

.method public static final native SGWidgetDecorator_getProgramProperty__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;)J
.end method

.method public static final native SGWidgetDecorator_getProperty__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/render/SGProperty;)Z
.end method

.method public static final native SGWidgetDecorator_getProperty__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGWidgetDecorator_init(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidgetDecorator_removeAllFilters(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;)V
.end method

.method public static final native SGWidgetDecorator_removeAllL(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;)V
.end method

.method public static final native SGWidgetDecorator_removeFilter__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGFilter;)V
.end method

.method public static final native SGWidgetDecorator_removeFilter__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;I)V
.end method

.method public static final native SGWidgetDecorator_removeL__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGWidgetDecorator_removeL__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;II)V
.end method

.method public static final native SGWidgetDecorator_removeL__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;I)V
.end method

.method public static final native SGWidgetDecorator_removeProperty(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;Ljava/lang/String;)V
.end method

.method public static final native SGWidgetDecorator_resetFilterFrameBufferSize(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;)V
.end method

.method public static final native SGWidgetDecorator_sendLToB__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;I)V
.end method

.method public static final native SGWidgetDecorator_sendLToB__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGWidgetDecorator_setFilterFrameBufferSize(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;FF)V
.end method

.method public static final native SGWidgetDecorator_setGeometryGeneratorNative(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V
.end method

.method public static final native SGWidgetDecorator_setGeometryGeneratorParam(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;F)V
.end method

.method public static final native SGWidgetDecorator_setProgramProperty(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V
.end method

.method public static final native SGWidgetDecorator_setProperty(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/render/SGProperty;)V
.end method

.method public static final native SGWidgetDecorator_swapL(JLcom/samsung/android/sdk/sgi/ui/SGWidgetDecorator;JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGWidgetImage_SWIGUpcast(J)J
.end method

.method public static final native SGWidgetImage_change_ownership(Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;JZ)V
.end method

.method public static final native SGWidgetImage_director_connect(Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;JZZ)V
.end method

.method public static final native SGWidgetImage_getBlendMode(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)I
.end method

.method public static final native SGWidgetImage_getColor(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)I
.end method

.method public static final native SGWidgetImage_getContentRect(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)Landroid/graphics/RectF;
.end method

.method public static final native SGWidgetImage_getContentRectPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)J
.end method

.method public static final native SGWidgetImage_getContentRectScale(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)J
.end method

.method public static final native SGWidgetImage_init__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;IJLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidgetImage_init__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;Landroid/graphics/RectF;)V
.end method

.method public static final native SGWidgetImage_init__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;Landroid/graphics/Bitmap;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidgetImage_isAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)Z
.end method

.method public static final native SGWidgetImage_isPreMultipliedRGBAEnabled(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)Z
.end method

.method public static final native SGWidgetImage_setAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;Z)V
.end method

.method public static final native SGWidgetImage_setBitmap(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;Landroid/graphics/Bitmap;)V
.end method

.method public static final native SGWidgetImage_setBlendMode(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;I)V
.end method

.method public static final native SGWidgetImage_setColor(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;I)V
.end method

.method public static final native SGWidgetImage_setContentRect(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;Landroid/graphics/RectF;)V
.end method

.method public static final native SGWidgetImage_setContentRectPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidgetImage_setContentRectScale(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidgetImage_setPreMultipliedRGBAEnabled(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;Z)V
.end method

.method public static final native SGWidgetImage_setTexture(JLcom/samsung/android/sdk/sgi/ui/SGWidgetImage;JLcom/samsung/android/sdk/sgi/render/SGProperty;)V
.end method

.method public static final native SGWidgetListenerBase_change_ownership(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JZ)V
.end method

.method public static final native SGWidgetListenerBase_director_connect(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JZZ)V
.end method

.method public static final native SGWidgetListenerBase_onFinished(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidgetListenerBase_onFinishedSwigExplicitSGWidgetListenerBase(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidgetListenerBase_onHover(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidgetListenerBase_onKeyEvent(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidgetListenerBase_onLocalTransformChanged(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
.end method

.method public static final native SGWidgetListenerBase_onLocalTransformChangedSwigExplicitSGWidgetListenerBase(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
.end method

.method public static final native SGWidgetListenerBase_onOpacityChanged(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;F)V
.end method

.method public static final native SGWidgetListenerBase_onOpacityChangedSwigExplicitSGWidgetListenerBase(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;F)V
.end method

.method public static final native SGWidgetListenerBase_onPositionChanged(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGWidgetListenerBase_onPositionChangedSwigExplicitSGWidgetListenerBase(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGWidgetListenerBase_onRotationChanged(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGWidgetListenerBase_onRotationChangedSwigExplicitSGWidgetListenerBase(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGWidgetListenerBase_onScaleChanged(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGWidgetListenerBase_onScaleChangedSwigExplicitSGWidgetListenerBase(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGWidgetListenerBase_onSizeChanged(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidgetListenerBase_onSizeChangedSwigExplicitSGWidgetListenerBase(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidgetListenerBase_onStarted(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidgetListenerBase_onStartedSwigExplicitSGWidgetListenerBase(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidgetListenerBase_onTouchEvent(JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidgetSurface_SWIGUpcast(J)J
.end method

.method public static final native SGWidgetSurface_change_ownership(Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;JZ)V
.end method

.method public static final native SGWidgetSurface_director_connect(Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;JZZ)V
.end method

.method public static final native SGWidgetSurface_getContentRect(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)Landroid/graphics/RectF;
.end method

.method public static final native SGWidgetSurface_getContentRectPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)J
.end method

.method public static final native SGWidgetSurface_getContentRectScale(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)J
.end method

.method public static final native SGWidgetSurface_init(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;)V
.end method

.method public static final native SGWidgetSurface_invalidateSwigExplicitSGWidgetSurface__SWIG_0_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;Landroid/graphics/RectF;)V
.end method

.method public static final native SGWidgetSurface_invalidateSwigExplicitSGWidgetSurface__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)V
.end method

.method public static final native SGWidgetSurface_invalidate__SWIG_0_0(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;Landroid/graphics/RectF;)V
.end method

.method public static final native SGWidgetSurface_invalidate__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)V
.end method

.method public static final native SGWidgetSurface_setContentRect(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;Landroid/graphics/RectF;)V
.end method

.method public static final native SGWidgetSurface_setContentRectPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidgetSurface_setContentRectScale(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidgetSurface_setRenderer(JLcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;JLcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;)V
.end method

.method public static final native SGWidget_addAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I
.end method

.method public static final native SGWidget_addAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)V
.end method

.method public static final native SGWidget_addW__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)I
.end method

.method public static final native SGWidget_addW__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)I
.end method

.method public static final native SGWidget_bringToF(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_bringWToF__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)V
.end method

.method public static final native SGWidget_bringWToF__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_change_ownership(Lcom/samsung/android/sdk/sgi/ui/SGWidget;JZ)V
.end method

.method public static final native SGWidget_clearFocus(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_clearFocusSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_director_connect(Lcom/samsung/android/sdk/sgi/ui/SGWidget;JZZ)V
.end method

.method public static final native SGWidget_dispatchKeyEvent(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGWidget_dispatchKeyEventSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGWidget_dispatchTouchEventToChildren(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
.end method

.method public static final native SGWidget_getFullTransform(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getLocalTouch(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)J
.end method

.method public static final native SGWidget_getLocalTransform(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getLocationInWindow__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)J
.end method

.method public static final native SGWidget_getLocationInWindow__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)J
.end method

.method public static final native SGWidget_getName(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Ljava/lang/String;
.end method

.method public static final native SGWidget_getOpacity(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)F
.end method

.method public static final native SGWidget_getParent(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/ui/SGWidget;
.end method

.method public static final native SGWidget_getPosition(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getPosition3f(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getPositionPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getPositionPivot3f(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getRelativeToAnotherParentTransform(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getRelativeTransform(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getRotation(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getRotation3f(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getRotationPivot(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getRotationPivot3f(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getScale(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getScale3f(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getScalePivot(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getScalePivot3f(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getSize(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)J
.end method

.method public static final native SGWidget_getSurface(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/vi/SGSurface;
.end method

.method public static final native SGWidget_getTouchCapturedWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/ui/SGWidget;
.end method

.method public static final native SGWidget_getTransformationHint(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)I
.end method

.method public static final native SGWidget_getVisibility(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)I
.end method

.method public static final native SGWidget_getWidgetAtPoint(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)Lcom/samsung/android/sdk/sgi/ui/SGWidget;
.end method

.method public static final native SGWidget_hideCursor(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_init(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidget_invalidateSwigExplicitSGWidget__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_invalidateSwigExplicitSGWidget__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/RectF;)V
.end method

.method public static final native SGWidget_invalidate__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_invalidate__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/RectF;)V
.end method

.method public static final native SGWidget_isAddedToSurface(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isChildrenClippingEnabled(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isEventActive(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isFiltersAttachDepthBuffer(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isFiltersAttachStencilBuffer(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isFocusable(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isFocusableSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isHoverable(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isHovered(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isInFocus(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isInFocusSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isInheritOpacity(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isLayerInvalid(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isLocalPointOnWidget__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)Z
.end method

.method public static final native SGWidget_isLocalPointOnWidget__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)Z
.end method

.method public static final native SGWidget_isRawPointOnWidget__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)Z
.end method

.method public static final native SGWidget_isRawPointOnWidget__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)Z
.end method

.method public static final native SGWidget_isScreenShotAttachDepthBuffer(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isScreenShotAttachStencilBuffer(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isTouchFirstSendToParentEnabled(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isTouchFocusable(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_isVisible(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_makeScreenShot__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGWidget_makeScreenShot__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGWidget_makeScreenShot__SWIG_10(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;)V
.end method

.method public static final native SGWidget_makeScreenShot__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/Rect;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGWidget_makeScreenShot__SWIG_3(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGWidget_makeScreenShot__SWIG_4(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/Bitmap;)V
.end method

.method public static final native SGWidget_makeScreenShot__SWIG_5(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
.end method

.method public static final native SGWidget_makeScreenShot__SWIG_6(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/Bitmap;JLcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;)V
.end method

.method public static final native SGWidget_makeScreenShot__SWIG_7(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;)V
.end method

.method public static final native SGWidget_makeScreenShot__SWIG_8(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;)V
.end method

.method public static final native SGWidget_makeScreenShot__SWIG_9(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;)V
.end method

.method public static final native SGWidget_moveCursor(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;I)V
.end method

.method public static final native SGWidget_onEventActiveChanged(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_onEventActiveChangedSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_onFocusChanged(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_onFocusChangedSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_onHoverChanged(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V
.end method

.method public static final native SGWidget_onHoverChangedSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V
.end method

.method public static final native SGWidget_onHoverEvent(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
.end method

.method public static final native SGWidget_onHoverEventSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
.end method

.method public static final native SGWidget_onKeyEvent(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGWidget_onKeyEventSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGWidget_removeAllAnimations(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_removeAllW(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_removeAnimation(JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)V
.end method

.method public static final native SGWidget_removeW__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_removeW__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)V
.end method

.method public static final native SGWidget_removeW__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;II)V
.end method

.method public static final native SGWidget_sendToB(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_sendWToB__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)V
.end method

.method public static final native SGWidget_sendWToB__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGWidget_setChildrenClipping(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V
.end method

.method public static final native SGWidget_setEventActive(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V
.end method

.method public static final native SGWidget_setFiltersOptions(JLcom/samsung/android/sdk/sgi/ui/SGWidget;ZZ)V
.end method

.method public static final native SGWidget_setFocus(JLcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
.end method

.method public static final native SGWidget_setFocusable(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V
.end method

.method public static final native SGWidget_setFocusableSwigExplicitSGWidget(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V
.end method

.method public static final native SGWidget_setHoverListener(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V
.end method

.method public static final native SGWidget_setHoverable(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V
.end method

.method public static final native SGWidget_setHovered(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V
.end method

.method public static final native SGWidget_setInheritOpacity(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V
.end method

.method public static final native SGWidget_setKeyEventListener(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V
.end method

.method public static final native SGWidget_setLocalTransform(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
.end method

.method public static final native SGWidget_setName(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Ljava/lang/String;)V
.end method

.method public static final native SGWidget_setOpacity(JLcom/samsung/android/sdk/sgi/ui/SGWidget;F)V
.end method

.method public static final native SGWidget_setPivots__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidget_setPivots__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGWidget_setPivots__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)V
.end method

.method public static final native SGWidget_setPivots__SWIG_3(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FFF)V
.end method

.method public static final native SGWidget_setPositionPivot__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidget_setPositionPivot__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGWidget_setPositionPivot__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)V
.end method

.method public static final native SGWidget_setPositionPivot__SWIG_3(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FFF)V
.end method

.method public static final native SGWidget_setPosition__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidget_setPosition__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGWidget_setPosition__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)V
.end method

.method public static final native SGWidget_setPosition__SWIG_3(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FFF)V
.end method

.method public static final native SGWidget_setRotationPivot__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidget_setRotationPivot__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGWidget_setRotationPivot__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)V
.end method

.method public static final native SGWidget_setRotationPivot__SWIG_3(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FFF)V
.end method

.method public static final native SGWidget_setRotationX(JLcom/samsung/android/sdk/sgi/ui/SGWidget;F)V
.end method

.method public static final native SGWidget_setRotationY(JLcom/samsung/android/sdk/sgi/ui/SGWidget;F)V
.end method

.method public static final native SGWidget_setRotationZ(JLcom/samsung/android/sdk/sgi/ui/SGWidget;F)V
.end method

.method public static final native SGWidget_setRotation__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGWidget_setRotation__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FFF)V
.end method

.method public static final native SGWidget_setRotation__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGWidget_setScalePivot__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidget_setScalePivot__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGWidget_setScalePivot__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)V
.end method

.method public static final native SGWidget_setScalePivot__SWIG_3(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FFF)V
.end method

.method public static final native SGWidget_setScale__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidget_setScale__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGWidget_setScale__SWIG_2(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)V
.end method

.method public static final native SGWidget_setScale__SWIG_3(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FFF)V
.end method

.method public static final native SGWidget_setScreenShotOptions(JLcom/samsung/android/sdk/sgi/ui/SGWidget;ZZ)V
.end method

.method public static final native SGWidget_setSize__SWIG_0(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGWidget_setSize__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGWidget;FF)V
.end method

.method public static final native SGWidget_setTouchFirstSendToParent(JLcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V
.end method

.method public static final native SGWidget_setTouchListener(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V
.end method

.method public static final native SGWidget_setTransformationHint(JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)V
.end method

.method public static final native SGWidget_setVisibility(JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)V
.end method

.method public static final native SGWidget_setWidgetAnimationListener(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V
.end method

.method public static final native SGWidget_setWidgetTransformationListener(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;)V
.end method

.method public static final native SGWidget_showCursor(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;I)V
.end method

.method public static final native SGWidget_swapW(JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static SwigDirector_SGGraphicBufferScreenshotListenerUIBase_onCompleted(Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 406
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;->onCompleted(Landroid/graphics/Bitmap;)V

    .line 407
    return-void
.end method

.method public static SwigDirector_SGPropertyScreenshotListenerUIBase_onCompleted(Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;J)V
    .locals 1

    .prologue
    .line 409
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGPropertyScreenshotListenerUIBase;->onCompleted(J)V

    .line 410
    return-void
.end method

.method public static SwigDirector_SGSurfaceRendererBase_onDraw(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;I)V
    .locals 0

    .prologue
    .line 412
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;->onDraw(I)V

    .line 413
    return-void
.end method

.method public static SwigDirector_SGWidgetCanvas_clearFocus(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)V
    .locals 0

    .prologue
    .line 481
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->clearFocus()V

    .line 482
    return-void
.end method

.method public static SwigDirector_SGWidgetCanvas_dispatchKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;J)Z
    .locals 3

    .prologue
    .line 469
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->dispatchKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetCanvas_invalidate__SWIG_0(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)V
    .locals 0

    .prologue
    .line 454
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->invalidate()V

    .line 455
    return-void
.end method

.method public static SwigDirector_SGWidgetCanvas_invalidate__SWIG_1(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 457
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->invalidate(Landroid/graphics/RectF;)V

    .line 458
    return-void
.end method

.method public static SwigDirector_SGWidgetCanvas_isFocusable(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)Z
    .locals 1

    .prologue
    .line 472
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->isFocusable()Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetCanvas_isInFocus(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)Z
    .locals 1

    .prologue
    .line 478
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->isInFocus()Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetCanvas_onDraw(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 490
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->onDraw(Landroid/graphics/Canvas;)V

    .line 491
    return-void
.end method

.method public static SwigDirector_SGWidgetCanvas_onEventActiveChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)V
    .locals 0

    .prologue
    .line 451
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->onEventActiveChanged()V

    .line 452
    return-void
.end method

.method public static SwigDirector_SGWidgetCanvas_onFocusChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;)V
    .locals 0

    .prologue
    .line 484
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->onFocusChanged()V

    .line 485
    return-void
.end method

.method public static SwigDirector_SGWidgetCanvas_onHoverChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;Z)V
    .locals 0

    .prologue
    .line 460
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->onHoverChanged(Z)V

    .line 461
    return-void
.end method

.method public static SwigDirector_SGWidgetCanvas_onHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;J)Z
    .locals 3

    .prologue
    .line 463
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->onHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetCanvas_onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;J)Z
    .locals 3

    .prologue
    .line 466
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetCanvas_setCanvasScale__SWIG_0(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;J)V
    .locals 3

    .prologue
    .line 487
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->setCanvasScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 488
    return-void
.end method

.method public static SwigDirector_SGWidgetCanvas_setFocusable(Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;Z)V
    .locals 0

    .prologue
    .line 475
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetCanvas;->setFocusable(Z)V

    .line 476
    return-void
.end method

.method public static SwigDirector_SGWidgetImage_clearFocus(Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)V
    .locals 0

    .prologue
    .line 523
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->clearFocus()V

    .line 524
    return-void
.end method

.method public static SwigDirector_SGWidgetImage_dispatchKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;J)Z
    .locals 3

    .prologue
    .line 511
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->dispatchKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetImage_invalidate__SWIG_0(Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)V
    .locals 0

    .prologue
    .line 496
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->invalidate()V

    .line 497
    return-void
.end method

.method public static SwigDirector_SGWidgetImage_invalidate__SWIG_1(Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 499
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->invalidate(Landroid/graphics/RectF;)V

    .line 500
    return-void
.end method

.method public static SwigDirector_SGWidgetImage_isFocusable(Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)Z
    .locals 1

    .prologue
    .line 514
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->isFocusable()Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetImage_isInFocus(Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)Z
    .locals 1

    .prologue
    .line 520
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->isInFocus()Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetImage_onEventActiveChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)V
    .locals 0

    .prologue
    .line 493
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->onEventActiveChanged()V

    .line 494
    return-void
.end method

.method public static SwigDirector_SGWidgetImage_onFocusChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;)V
    .locals 0

    .prologue
    .line 526
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->onFocusChanged()V

    .line 527
    return-void
.end method

.method public static SwigDirector_SGWidgetImage_onHoverChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;Z)V
    .locals 0

    .prologue
    .line 502
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->onHoverChanged(Z)V

    .line 503
    return-void
.end method

.method public static SwigDirector_SGWidgetImage_onHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;J)Z
    .locals 3

    .prologue
    .line 505
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->onHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetImage_onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;J)Z
    .locals 3

    .prologue
    .line 508
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetImage_setFocusable(Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;Z)V
    .locals 0

    .prologue
    .line 517
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetImage;->setFocusable(Z)V

    .line 518
    return-void
.end method

.method public static SwigDirector_SGWidgetListenerBase_onFinished(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;J)V
    .locals 1

    .prologue
    .line 592
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->onFinished(J)V

    .line 593
    return-void
.end method

.method public static SwigDirector_SGWidgetListenerBase_onHover(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JJ)Z
    .locals 3

    .prologue
    .line 568
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(JZ)V

    invoke-virtual {p0, v0, p3, p4}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->onHover(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;J)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetListenerBase_onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JJ)Z
    .locals 3

    .prologue
    .line 595
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>(JZ)V

    invoke-virtual {p0, v0, p3, p4}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;J)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetListenerBase_onLocalTransformChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JJ)V
    .locals 3

    .prologue
    .line 583
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    const/4 v1, 0x0

    invoke-direct {v0, p3, p4, v1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>(JZ)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->onLocalTransformChanged(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V

    .line 584
    return-void
.end method

.method public static SwigDirector_SGWidgetListenerBase_onOpacityChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JF)V
    .locals 1

    .prologue
    .line 586
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->onOpacityChanged(JF)V

    .line 587
    return-void
.end method

.method public static SwigDirector_SGWidgetListenerBase_onPositionChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JJ)V
    .locals 3

    .prologue
    .line 574
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    const/4 v1, 0x0

    invoke-direct {v0, p3, p4, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->onPositionChanged(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 575
    return-void
.end method

.method public static SwigDirector_SGWidgetListenerBase_onRotationChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JJ)V
    .locals 3

    .prologue
    .line 577
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    const/4 v1, 0x0

    invoke-direct {v0, p3, p4, v1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->onRotationChanged(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 578
    return-void
.end method

.method public static SwigDirector_SGWidgetListenerBase_onScaleChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JJ)V
    .locals 3

    .prologue
    .line 580
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    const/4 v1, 0x0

    invoke-direct {v0, p3, p4, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->onScaleChanged(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 581
    return-void
.end method

.method public static SwigDirector_SGWidgetListenerBase_onSizeChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JJ)V
    .locals 3

    .prologue
    .line 571
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/4 v1, 0x0

    invoke-direct {v0, p3, p4, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->onSizeChanged(JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 572
    return-void
.end method

.method public static SwigDirector_SGWidgetListenerBase_onStarted(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;J)V
    .locals 1

    .prologue
    .line 589
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->onStarted(J)V

    .line 590
    return-void
.end method

.method public static SwigDirector_SGWidgetListenerBase_onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;JJ)Z
    .locals 3

    .prologue
    .line 565
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(JZ)V

    invoke-virtual {p0, v0, p3, p4}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetListenerBase;->onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;J)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetSurface_clearFocus(Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)V
    .locals 0

    .prologue
    .line 559
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->clearFocus()V

    .line 560
    return-void
.end method

.method public static SwigDirector_SGWidgetSurface_dispatchKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;J)Z
    .locals 3

    .prologue
    .line 547
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->dispatchKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetSurface_invalidate__SWIG_0_0(Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 535
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->invalidate(Landroid/graphics/RectF;)V

    .line 536
    return-void
.end method

.method public static SwigDirector_SGWidgetSurface_invalidate__SWIG_1(Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)V
    .locals 0

    .prologue
    .line 532
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->invalidate()V

    .line 533
    return-void
.end method

.method public static SwigDirector_SGWidgetSurface_isFocusable(Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)Z
    .locals 1

    .prologue
    .line 550
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->isFocusable()Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetSurface_isInFocus(Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)Z
    .locals 1

    .prologue
    .line 556
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->isInFocus()Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetSurface_onEventActiveChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)V
    .locals 0

    .prologue
    .line 529
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->onEventActiveChanged()V

    .line 530
    return-void
.end method

.method public static SwigDirector_SGWidgetSurface_onFocusChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;)V
    .locals 0

    .prologue
    .line 562
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->onFocusChanged()V

    .line 563
    return-void
.end method

.method public static SwigDirector_SGWidgetSurface_onHoverChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;Z)V
    .locals 0

    .prologue
    .line 538
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->onHoverChanged(Z)V

    .line 539
    return-void
.end method

.method public static SwigDirector_SGWidgetSurface_onHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;J)Z
    .locals 3

    .prologue
    .line 541
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->onHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetSurface_onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;J)Z
    .locals 3

    .prologue
    .line 544
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidgetSurface_setFocusable(Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;Z)V
    .locals 0

    .prologue
    .line 553
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setFocusable(Z)V

    .line 554
    return-void
.end method

.method public static SwigDirector_SGWidget_clearFocus(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 0

    .prologue
    .line 445
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->clearFocus()V

    .line 446
    return-void
.end method

.method public static SwigDirector_SGWidget_dispatchKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGWidget;J)Z
    .locals 3

    .prologue
    .line 433
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->dispatchKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidget_invalidate__SWIG_0(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 0

    .prologue
    .line 418
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->invalidate()V

    .line 419
    return-void
.end method

.method public static SwigDirector_SGWidget_invalidate__SWIG_1(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 421
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->invalidate(Landroid/graphics/RectF;)V

    .line 422
    return-void
.end method

.method public static SwigDirector_SGWidget_isFocusable(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
    .locals 1

    .prologue
    .line 436
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isFocusable()Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidget_isInFocus(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Z
    .locals 1

    .prologue
    .line 442
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->isInFocus()Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidget_onEventActiveChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 0

    .prologue
    .line 415
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->onEventActiveChanged()V

    .line 416
    return-void
.end method

.method public static SwigDirector_SGWidget_onFocusChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidget;)V
    .locals 0

    .prologue
    .line 448
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->onFocusChanged()V

    .line 449
    return-void
.end method

.method public static SwigDirector_SGWidget_onHoverChanged(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V
    .locals 0

    .prologue
    .line 424
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->onHoverChanged(Z)V

    .line 425
    return-void
.end method

.method public static SwigDirector_SGWidget_onHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGWidget;J)Z
    .locals 3

    .prologue
    .line 427
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->onHoverEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidget_onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGWidget;J)Z
    .locals 3

    .prologue
    .line 430
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->onKeyEvent(Lcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGWidget_setFocusable(Lcom/samsung/android/sdk/sgi/ui/SGWidget;Z)V
    .locals 0

    .prologue
    .line 439
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidget;->setFocusable(Z)V

    .line 440
    return-void
.end method

.method public static final native delete_SGGraphicBufferScreenshotListenerUIBase(J)V
.end method

.method public static final native delete_SGKeyEvent(J)V
.end method

.method public static final native delete_SGPropertyScreenshotListenerUIBase(J)V
.end method

.method public static final native delete_SGSurfaceRendererBase(J)V
.end method

.method public static final native delete_SGTouchEvent(J)V
.end method

.method public static final native delete_SGWidget(J)V
.end method

.method public static final native delete_SGWidgetCanvas(J)V
.end method

.method public static final native delete_SGWidgetDecorator(J)V
.end method

.method public static final native delete_SGWidgetImage(J)V
.end method

.method public static final native delete_SGWidgetListenerBase(J)V
.end method

.method public static final native delete_SGWidgetSurface(J)V
.end method

.method public static final native new_SGGraphicBufferScreenshotListenerUIBase()J
.end method

.method public static final native new_SGKeyEvent__SWIG_0()J
.end method

.method public static final native new_SGKeyEvent__SWIG_1(III)J
.end method

.method public static final native new_SGKeyEvent__SWIG_2(II)J
.end method

.method public static final native new_SGKeyEvent__SWIG_3(IILjava/util/Date;Ljava/lang/String;I)J
.end method

.method public static final native new_SGKeyEvent__SWIG_4(IILjava/util/Date;Ljava/lang/String;)J
.end method

.method public static final native new_SGPropertyScreenshotListenerUIBase()J
.end method

.method public static final native new_SGSurfaceRendererBase()J
.end method

.method public static final native new_SGTouchEvent__SWIG_0()J
.end method

.method public static final native new_SGTouchEvent__SWIG_1(JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)J
.end method

.method public static final native new_SGWidget()J
.end method

.method public static final native new_SGWidgetCanvas()J
.end method

.method public static final native new_SGWidgetDecorator()J
.end method

.method public static final native new_SGWidgetImage()J
.end method

.method public static final native new_SGWidgetListenerBase()J
.end method

.method public static final native new_SGWidgetSurface()J
.end method

.method private static final native swig_module_init()V
.end method
