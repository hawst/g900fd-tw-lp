.class Lcom/samsung/android/sdk/sgi/animation/SGJNI;
.super Ljava/lang/Object;
.source "SGJNI.java"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 22
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->initLibrary()V

    .line 430
    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->swig_module_init()V

    .line 431
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native SGAnimationFactory_createContentRectAnimation()J
.end method

.method public static final native SGAnimationFactory_createContentRectScaleAnimation()J
.end method

.method public static final native SGAnimationFactory_createCustomFloatAnimation(Ljava/lang/String;)J
.end method

.method public static final native SGAnimationFactory_createCustomVector2fAnimation(Ljava/lang/String;)J
.end method

.method public static final native SGAnimationFactory_createCustomVector3fAnimation(Ljava/lang/String;)J
.end method

.method public static final native SGAnimationFactory_createCustomVector4fAnimation(Ljava/lang/String;)J
.end method

.method public static final native SGAnimationFactory_createGeometryAnimation()J
.end method

.method public static final native SGAnimationFactory_createGroupAnimation(Z)J
.end method

.method public static final native SGAnimationFactory_createOpacityAnimation()J
.end method

.method public static final native SGAnimationFactory_createParticleAnimation()J
.end method

.method public static final native SGAnimationFactory_createPositionAnimation()J
.end method

.method public static final native SGAnimationFactory_createPositionPivotAnimation()J
.end method

.method public static final native SGAnimationFactory_createRotationAnimation()J
.end method

.method public static final native SGAnimationFactory_createRotationPivotAnimation()J
.end method

.method public static final native SGAnimationFactory_createScaleAnimation()J
.end method

.method public static final native SGAnimationFactory_createScalePivotAnimation()J
.end method

.method public static final native SGAnimationFactory_createSizeAnimation()J
.end method

.method public static final native SGAnimationFactory_createSpriteAnimation()J
.end method

.method public static final native SGAnimationFactory_createTransitionAnimation__SWIG_0(I)J
.end method

.method public static final native SGAnimationFactory_createTransitionAnimation__SWIG_1(II)J
.end method

.method public static final native SGAnimationListenerBase_change_ownership(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;JZ)V
.end method

.method public static final native SGAnimationListenerBase_director_connect(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;JZZ)V
.end method

.method public static final native SGAnimationListenerBase_getCurrentValue(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;)V
.end method

.method public static final native SGAnimationListenerBase_onCancelled(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V
.end method

.method public static final native SGAnimationListenerBase_onCancelledSwigExplicitSGAnimationListenerBase(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V
.end method

.method public static final native SGAnimationListenerBase_onDiscarded(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V
.end method

.method public static final native SGAnimationListenerBase_onDiscardedSwigExplicitSGAnimationListenerBase(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V
.end method

.method public static final native SGAnimationListenerBase_onFinished(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V
.end method

.method public static final native SGAnimationListenerBase_onFinishedSwigExplicitSGAnimationListenerBase(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V
.end method

.method public static final native SGAnimationListenerBase_onRepeated(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V
.end method

.method public static final native SGAnimationListenerBase_onRepeatedSwigExplicitSGAnimationListenerBase(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V
.end method

.method public static final native SGAnimationListenerBase_onStarted(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V
.end method

.method public static final native SGAnimationListenerBase_onStartedSwigExplicitSGAnimationListenerBase(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V
.end method

.method public static final native SGAnimationTimingFunction_change_ownership(Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;JZ)V
.end method

.method public static final native SGAnimationTimingFunction_director_connect(Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;JZZ)V
.end method

.method public static final native SGAnimationTimingFunction_getInterpolationTime(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;F)F
.end method

.method public static final native SGAnimationTimingFunction_init(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;)V
.end method

.method public static final native SGAnimationTransaction___assign__(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)J
.end method

.method public static final native SGAnimationTransaction_begin(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)Z
.end method

.method public static final native SGAnimationTransaction_enableSynchronizedStart(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;Z)V
.end method

.method public static final native SGAnimationTransaction_end(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)Z
.end method

.method public static final native SGAnimationTransaction_getAnimationListenerNative(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;
.end method

.method public static final native SGAnimationTransaction_getCurrentAnimationId(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)I
.end method

.method public static final native SGAnimationTransaction_getDefaultDuration()I
.end method

.method public static final native SGAnimationTransaction_getDefaultEnabled()Z
.end method

.method public static final native SGAnimationTransaction_getDefaultOffset()I
.end method

.method public static final native SGAnimationTransaction_getDefaultTimingFunction()Ljava/lang/Object;
.end method

.method public static final native SGAnimationTransaction_getDuration(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)I
.end method

.method public static final native SGAnimationTransaction_getHandle(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)J
.end method

.method public static final native SGAnimationTransaction_getOffset(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)I
.end method

.method public static final native SGAnimationTransaction_getOnSuspendBehaviour(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)I
.end method

.method public static final native SGAnimationTransaction_getTimingFunction(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;
.end method

.method public static final native SGAnimationTransaction_init(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)V
.end method

.method public static final native SGAnimationTransaction_isDefaultDeferredStartEnabled()Z
.end method

.method public static final native SGAnimationTransaction_isDeferredStartEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)Z
.end method

.method public static final native SGAnimationTransaction_isSynchronizedStartEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;)Z
.end method

.method public static final native SGAnimationTransaction_setAnimationListener(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V
.end method

.method public static final native SGAnimationTransaction_setDefaultDeferredStartEnabled(Z)V
.end method

.method public static final native SGAnimationTransaction_setDefaultDuration(I)V
.end method

.method public static final native SGAnimationTransaction_setDefaultEnabled(Z)V
.end method

.method public static final native SGAnimationTransaction_setDefaultOffset(I)V
.end method

.method public static final native SGAnimationTransaction_setDefaultTimingFunction(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;)V
.end method

.method public static final native SGAnimationTransaction_setDeferredStartEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;Z)V
.end method

.method public static final native SGAnimationTransaction_setDuration(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;I)V
.end method

.method public static final native SGAnimationTransaction_setOffset(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;I)V
.end method

.method public static final native SGAnimationTransaction_setOnSuspendBehaviour(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;I)V
.end method

.method public static final native SGAnimationTransaction_setTimingFunction(JLcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;JLcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;)V
.end method

.method public static final native SGAnimationValueInterpolator_change_ownership(Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;JZ)V
.end method

.method public static final native SGAnimationValueInterpolator_director_connect(Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;JZZ)V
.end method

.method public static final native SGAnimationValueInterpolator_init(JLcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;)V
.end method

.method public static final native SGAnimationValueInterpolator_interpolate__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;FFF)F
.end method

.method public static final native SGAnimationValueInterpolator_interpolate__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;FJLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)J
.end method

.method public static final native SGAnimationValueInterpolator_interpolate__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;FJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J
.end method

.method public static final native SGAnimationValueInterpolator_interpolate__SWIG_3(JLcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;FJLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)J
.end method

.method public static final native SGAnimationValueInterpolator_interpolate__SWIG_4(JLcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;FJLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)J
.end method

.method public static final native SGAnimation_getAnimationListenerNative(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerHolder;
.end method

.method public static final native SGAnimation_getDirection(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I
.end method

.method public static final native SGAnimation_getDuration(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I
.end method

.method public static final native SGAnimation_getFillAfterMode(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I
.end method

.method public static final native SGAnimation_getHandle(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)J
.end method

.method public static final native SGAnimation_getOffset(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I
.end method

.method public static final native SGAnimation_getOnSuspendBehaviour(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I
.end method

.method public static final native SGAnimation_getRepeatCount(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I
.end method

.method public static final native SGAnimation_getTimingFunction(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;
.end method

.method public static final native SGAnimation_getValueInterpolator(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;
.end method

.method public static final native SGAnimation_isAutoReverseEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)Z
.end method

.method public static final native SGAnimation_isDeferredStartEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)Z
.end method

.method public static final native SGAnimation_isFillBeforeEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)Z
.end method

.method public static final native SGAnimation_setAnimationListener(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V
.end method

.method public static final native SGAnimation_setAutoReverseEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;Z)V
.end method

.method public static final native SGAnimation_setDeferredStartEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;Z)V
.end method

.method public static final native SGAnimation_setDirection(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;I)V
.end method

.method public static final native SGAnimation_setDuration(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;I)V
.end method

.method public static final native SGAnimation_setFillAfterMode(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;I)V
.end method

.method public static final native SGAnimation_setFillBeforeEnabled(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;Z)V
.end method

.method public static final native SGAnimation_setOffset(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;I)V
.end method

.method public static final native SGAnimation_setOnSuspendBehaviour(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;I)V
.end method

.method public static final native SGAnimation_setRepeatCount(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;I)V
.end method

.method public static final native SGAnimation_setTimingFunction(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;JLcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;)V
.end method

.method public static final native SGAnimation_setValueInterpolator(JLcom/samsung/android/sdk/sgi/animation/SGAnimation;JLcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;)V
.end method

.method public static final native SGFloatAnimation_SWIGUpcast(J)J
.end method

.method public static final native SGFloatAnimation___assign__(JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;)J
.end method

.method public static final native SGFloatAnimation_addKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;FF)Z
.end method

.method public static final native SGFloatAnimation_getEndValue(JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;)F
.end method

.method public static final native SGFloatAnimation_getStartValue(JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;)F
.end method

.method public static final native SGFloatAnimation_removeKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;F)Z
.end method

.method public static final native SGFloatAnimation_setEndValue(JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;F)V
.end method

.method public static final native SGFloatAnimation_setStartValue(JLcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;F)V
.end method

.method public static final native SGGroupAnimation_SWIGUpcast(J)J
.end method

.method public static final native SGGroupAnimation___assign__(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;)J
.end method

.method public static final native SGGroupAnimation_addAnimation(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I
.end method

.method public static final native SGGroupAnimation_getAnimation(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;I)Lcom/samsung/android/sdk/sgi/animation/SGAnimation;
.end method

.method public static final native SGGroupAnimation_getAnimationCount(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;)I
.end method

.method public static final native SGGroupAnimation_insertAnimation(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;IJLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I
.end method

.method public static final native SGGroupAnimation_isParallel(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;)Z
.end method

.method public static final native SGGroupAnimation_removeAllAnimations(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;)V
.end method

.method public static final native SGGroupAnimation_removeAnimation(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;I)Z
.end method

.method public static final native SGLinearValueInterpolator_SWIGUpcast(J)J
.end method

.method public static final native SGLinearValueInterpolator_interpolate__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;FFF)F
.end method

.method public static final native SGLinearValueInterpolator_interpolate__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;FJLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)J
.end method

.method public static final native SGLinearValueInterpolator_interpolate__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;FJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J
.end method

.method public static final native SGLinearValueInterpolator_interpolate__SWIG_3(JLcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;FJLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)J
.end method

.method public static final native SGLinearValueInterpolator_interpolate__SWIG_4(JLcom/samsung/android/sdk/sgi/animation/SGLinearValueInterpolator;FJLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)J
.end method

.method public static final native SGParticleAnimation_SWIGUpcast(J)J
.end method

.method public static final native SGParticleAnimation___assign__(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J
.end method

.method public static final native SGParticleAnimation_addColorKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FI)V
.end method

.method public static final native SGParticleAnimation_addForceKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFFF)V
.end method

.method public static final native SGParticleAnimation_addGravityKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFFF)V
.end method

.method public static final native SGParticleAnimation_addMassKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FF)V
.end method

.method public static final native SGParticleAnimation_addPositionKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFFF)V
.end method

.method public static final native SGParticleAnimation_addScaleKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFFF)V
.end method

.method public static final native SGParticleAnimation_getCount(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)I
.end method

.method public static final native SGParticleAnimation_getDefaultColor(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)I
.end method

.method public static final native SGParticleAnimation_getDefaultDuration(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)I
.end method

.method public static final native SGParticleAnimation_getDefaultForce(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J
.end method

.method public static final native SGParticleAnimation_getDefaultGravity(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J
.end method

.method public static final native SGParticleAnimation_getDefaultMass(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)F
.end method

.method public static final native SGParticleAnimation_getDefaultPosition(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J
.end method

.method public static final native SGParticleAnimation_getDefaultSize(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J
.end method

.method public static final native SGParticleAnimation_getRandomColor(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)I
.end method

.method public static final native SGParticleAnimation_getRandomDuration(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)I
.end method

.method public static final native SGParticleAnimation_getRandomForce(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J
.end method

.method public static final native SGParticleAnimation_getRandomGravity(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J
.end method

.method public static final native SGParticleAnimation_getRandomMass(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)F
.end method

.method public static final native SGParticleAnimation_getRandomPosition(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J
.end method

.method public static final native SGParticleAnimation_getRandomSize(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)J
.end method

.method public static final native SGParticleAnimation_isParticleBlendingEnabled(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;)Z
.end method

.method public static final native SGParticleAnimation_setBitmap(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;Landroid/graphics/Bitmap;)V
.end method

.method public static final native SGParticleAnimation_setColorAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IIIIZ)V
.end method

.method public static final native SGParticleAnimation_setColorAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IIII)V
.end method

.method public static final native SGParticleAnimation_setColorAnimation__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;III)V
.end method

.method public static final native SGParticleAnimation_setCount(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;I)V
.end method

.method public static final native SGParticleAnimation_setDefaultColor(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;I)V
.end method

.method public static final native SGParticleAnimation_setDefaultDuration(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;I)V
.end method

.method public static final native SGParticleAnimation_setDefaultForce__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V
.end method

.method public static final native SGParticleAnimation_setDefaultForce__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGParticleAnimation_setDefaultGravity__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V
.end method

.method public static final native SGParticleAnimation_setDefaultGravity__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGParticleAnimation_setDefaultMass(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;F)V
.end method

.method public static final native SGParticleAnimation_setDefaultPosition__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V
.end method

.method public static final native SGParticleAnimation_setDefaultPosition__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGParticleAnimation_setDefaultSize__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V
.end method

.method public static final native SGParticleAnimation_setDefaultSize__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGParticleAnimation_setForceAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;IZ)V
.end method

.method public static final native SGParticleAnimation_setForceAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)V
.end method

.method public static final native SGParticleAnimation_setForceAnimation__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGParticleAnimation_setGravityAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;IZ)V
.end method

.method public static final native SGParticleAnimation_setGravityAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)V
.end method

.method public static final native SGParticleAnimation_setGravityAnimation__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGParticleAnimation_setMassAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IFFI)V
.end method

.method public static final native SGParticleAnimation_setMassAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IFF)V
.end method

.method public static final native SGParticleAnimation_setParticleBlendingEnabled(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;Z)V
.end method

.method public static final native SGParticleAnimation_setPositionAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;IZ)V
.end method

.method public static final native SGParticleAnimation_setPositionAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)V
.end method

.method public static final native SGParticleAnimation_setPositionAnimation__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGParticleAnimation_setRandomColor(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;I)V
.end method

.method public static final native SGParticleAnimation_setRandomDuration(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;I)V
.end method

.method public static final native SGParticleAnimation_setRandomForce__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V
.end method

.method public static final native SGParticleAnimation_setRandomForce__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGParticleAnimation_setRandomGravity__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V
.end method

.method public static final native SGParticleAnimation_setRandomGravity__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGParticleAnimation_setRandomMass(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;F)V
.end method

.method public static final native SGParticleAnimation_setRandomPosition__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V
.end method

.method public static final native SGParticleAnimation_setRandomPosition__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGParticleAnimation_setRandomSize__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;FFF)V
.end method

.method public static final native SGParticleAnimation_setRandomSize__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGParticleAnimation_setScaleAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;IZ)V
.end method

.method public static final native SGParticleAnimation_setScaleAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)V
.end method

.method public static final native SGParticleAnimation_setScaleAnimation__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;IJLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGPropertyAnimation_SWIGUpcast(J)J
.end method

.method public static final native SGQuaternionAnimation_SWIGUpcast(J)J
.end method

.method public static final native SGQuaternionAnimation___assign__(JLcom/samsung/android/sdk/sgi/animation/SGQuaternionAnimation;JLcom/samsung/android/sdk/sgi/animation/SGQuaternionAnimation;)J
.end method

.method public static final native SGQuaternionAnimation_addKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGQuaternionAnimation;FJLcom/samsung/android/sdk/sgi/base/SGQuaternion;)Z
.end method

.method public static final native SGQuaternionAnimation_getEndValue(JLcom/samsung/android/sdk/sgi/animation/SGQuaternionAnimation;)J
.end method

.method public static final native SGQuaternionAnimation_getStartValue(JLcom/samsung/android/sdk/sgi/animation/SGQuaternionAnimation;)J
.end method

.method public static final native SGQuaternionAnimation_removeKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGQuaternionAnimation;F)Z
.end method

.method public static final native SGQuaternionAnimation_setEndValue(JLcom/samsung/android/sdk/sgi/animation/SGQuaternionAnimation;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGQuaternionAnimation_setStartValue(JLcom/samsung/android/sdk/sgi/animation/SGQuaternionAnimation;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGSpriteAnimation_SWIGUpcast(J)J
.end method

.method public static final native SGSpriteAnimation___assign__(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;)J
.end method

.method public static final native SGSpriteAnimation_addKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;FI)Z
.end method

.method public static final native SGSpriteAnimation_getEndIndex(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;)I
.end method

.method public static final native SGSpriteAnimation_getFrameSize(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;)J
.end method

.method public static final native SGSpriteAnimation_getSourceSize(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;)J
.end method

.method public static final native SGSpriteAnimation_getStartIndex(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;)I
.end method

.method public static final native SGSpriteAnimation_removeKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;F)Z
.end method

.method public static final native SGSpriteAnimation_setEndIndex(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;I)V
.end method

.method public static final native SGSpriteAnimation_setFrameSize(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;FF)V
.end method

.method public static final native SGSpriteAnimation_setSourceSize(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;FF)V
.end method

.method public static final native SGSpriteAnimation_setStartIndex(JLcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;I)V
.end method

.method public static final native SGTimingFunctionFactory_createAccelerateTimingFunction__SWIG_0(I)J
.end method

.method public static final native SGTimingFunctionFactory_createAccelerateTimingFunction__SWIG_1(IF)J
.end method

.method public static final native SGTimingFunctionFactory_createBounceEaseTimingFunction(I)J
.end method

.method public static final native SGTimingFunctionFactory_createCubicBezierTimingFunction__SWIG_0(I)J
.end method

.method public static final native SGTimingFunctionFactory_createCubicBezierTimingFunction__SWIG_1(FFFF)J
.end method

.method public static final native SGTimingFunctionFactory_createLinearTimingFunction()J
.end method

.method public static final native SGTimingFunctionFactory_createPredefinedTimingFunction(I)J
.end method

.method public static final native SGTransitionAnimation_SWIGUpcast(J)J
.end method

.method public static final native SGTransitionAnimation___assign__(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;)J
.end method

.method public static final native SGTransitionAnimation_enableAlphaBlending(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;Z)V
.end method

.method public static final native SGTransitionAnimation_enableSynchronizedStart(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;Z)V
.end method

.method public static final native SGTransitionAnimation_getTransitionDirection(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;)I
.end method

.method public static final native SGTransitionAnimation_getTransitionType(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;)I
.end method

.method public static final native SGTransitionAnimation_isAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;)Z
.end method

.method public static final native SGTransitionAnimation_isSynchronizedStartEnabled(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;)Z
.end method

.method public static final native SGTransitionAnimation_overrideSourceTexture(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;JLcom/samsung/android/sdk/sgi/render/SGProperty;)V
.end method

.method public static final native SGTransitionAnimation_overrideTargetTexture(JLcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;JLcom/samsung/android/sdk/sgi/render/SGProperty;)V
.end method

.method public static final native SGVector2fAnimation_SWIGUpcast(J)J
.end method

.method public static final native SGVector2fAnimation___assign__(JLcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;JLcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;)J
.end method

.method public static final native SGVector2fAnimation_addKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;FJLcom/samsung/android/sdk/sgi/base/SGVector2f;)Z
.end method

.method public static final native SGVector2fAnimation_getEndValue(JLcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;)J
.end method

.method public static final native SGVector2fAnimation_getStartValue(JLcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;)J
.end method

.method public static final native SGVector2fAnimation_removeKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;F)Z
.end method

.method public static final native SGVector2fAnimation_setEndValue(JLcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVector2fAnimation_setStartValue(JLcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVector3fAnimation_SWIGUpcast(J)J
.end method

.method public static final native SGVector3fAnimation___assign__(JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;)J
.end method

.method public static final native SGVector3fAnimation_addKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;FJLcom/samsung/android/sdk/sgi/base/SGVector3f;)Z
.end method

.method public static final native SGVector3fAnimation_getEndValue(JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;)J
.end method

.method public static final native SGVector3fAnimation_getStartValue(JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;)J
.end method

.method public static final native SGVector3fAnimation_removeKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;F)Z
.end method

.method public static final native SGVector3fAnimation_setEndValue(JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVector3fAnimation_setStartValue(JLcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVector4fAnimation_SWIGUpcast(J)J
.end method

.method public static final native SGVector4fAnimation___assign__(JLcom/samsung/android/sdk/sgi/animation/SGVector4fAnimation;JLcom/samsung/android/sdk/sgi/animation/SGVector4fAnimation;)J
.end method

.method public static final native SGVector4fAnimation_addKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGVector4fAnimation;FJLcom/samsung/android/sdk/sgi/base/SGVector4f;)Z
.end method

.method public static final native SGVector4fAnimation_getEndValue(JLcom/samsung/android/sdk/sgi/animation/SGVector4fAnimation;)J
.end method

.method public static final native SGVector4fAnimation_getStartValue(JLcom/samsung/android/sdk/sgi/animation/SGVector4fAnimation;)J
.end method

.method public static final native SGVector4fAnimation_removeKeyFrame(JLcom/samsung/android/sdk/sgi/animation/SGVector4fAnimation;F)Z
.end method

.method public static final native SGVector4fAnimation_setEndValue(JLcom/samsung/android/sdk/sgi/animation/SGVector4fAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGVector4fAnimation_setStartValue(JLcom/samsung/android/sdk/sgi/animation/SGVector4fAnimation;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGVisualValueReceiver_change_ownership(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JZ)V
.end method

.method public static final native SGVisualValueReceiver_director_connect(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JZZ)V
.end method

.method public static final native SGVisualValueReceiver_onContentRect(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGVisualValueReceiver_onContentRectScale(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVisualValueReceiver_onContentRectScaleSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVisualValueReceiver_onContentRectSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGVisualValueReceiver_onCustomPropertySwigExplicitSGVisualValueReceiver__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;F)V
.end method

.method public static final native SGVisualValueReceiver_onCustomPropertySwigExplicitSGVisualValueReceiver__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVisualValueReceiver_onCustomPropertySwigExplicitSGVisualValueReceiver__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVisualValueReceiver_onCustomPropertySwigExplicitSGVisualValueReceiver__SWIG_3(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGVisualValueReceiver_onCustomPropertySwigExplicitSGVisualValueReceiver__SWIG_4(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGVisualValueReceiver_onCustomProperty__SWIG_0(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;F)V
.end method

.method public static final native SGVisualValueReceiver_onCustomProperty__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVisualValueReceiver_onCustomProperty__SWIG_2(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVisualValueReceiver_onCustomProperty__SWIG_3(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGVisualValueReceiver_onCustomProperty__SWIG_4(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGVisualValueReceiver_onGeometryGeneratorParam(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;F)V
.end method

.method public static final native SGVisualValueReceiver_onGeometryGeneratorParamSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;F)V
.end method

.method public static final native SGVisualValueReceiver_onOpacity(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;F)V
.end method

.method public static final native SGVisualValueReceiver_onOpacitySwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;F)V
.end method

.method public static final native SGVisualValueReceiver_onOther(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;)V
.end method

.method public static final native SGVisualValueReceiver_onOtherSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;)V
.end method

.method public static final native SGVisualValueReceiver_onPosition(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVisualValueReceiver_onPositionPivot(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVisualValueReceiver_onPositionPivotSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVisualValueReceiver_onPositionSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVisualValueReceiver_onRotation(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGVisualValueReceiver_onRotationPivot(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVisualValueReceiver_onRotationPivotSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVisualValueReceiver_onRotationSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGVisualValueReceiver_onScale(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVisualValueReceiver_onScalePivot(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVisualValueReceiver_onScalePivotSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVisualValueReceiver_onScaleSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVisualValueReceiver_onSize(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVisualValueReceiver_onSizeSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVisualValueReceiver_onSpriteRect(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGVisualValueReceiver_onSpriteRectSwigExplicitSGVisualValueReceiver(JLcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static SwigDirector_SGAnimationListenerBase_onCancelled(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V
    .locals 0

    .prologue
    .line 419
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->onCancelled(I)V

    .line 420
    return-void
.end method

.method public static SwigDirector_SGAnimationListenerBase_onDiscarded(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V
    .locals 0

    .prologue
    .line 425
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->onDiscarded(I)V

    .line 426
    return-void
.end method

.method public static SwigDirector_SGAnimationListenerBase_onFinished(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V
    .locals 0

    .prologue
    .line 422
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->onFinished(I)V

    .line 423
    return-void
.end method

.method public static SwigDirector_SGAnimationListenerBase_onRepeated(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V
    .locals 0

    .prologue
    .line 416
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->onRepeated(I)V

    .line 417
    return-void
.end method

.method public static SwigDirector_SGAnimationListenerBase_onStarted(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;I)V
    .locals 0

    .prologue
    .line 413
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->onStarted(I)V

    .line 414
    return-void
.end method

.method public static SwigDirector_SGAnimationTimingFunction_getInterpolationTime(Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;F)F
    .locals 1

    .prologue
    .line 356
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;->getInterpolationTime(F)F

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGAnimationValueInterpolator_interpolate__SWIG_0(Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;FFF)F
    .locals 1

    .prologue
    .line 341
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->interpolate(FFF)F

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGAnimationValueInterpolator_interpolate__SWIG_1(Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;FJJ)J
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 344
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, p2, p3, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v1, p4, p5, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->interpolate(FLcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static SwigDirector_SGAnimationValueInterpolator_interpolate__SWIG_2(Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;FJJ)J
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 347
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    invoke-direct {v0, p2, p3, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    invoke-direct {v1, p4, p5, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->interpolate(FLcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static SwigDirector_SGAnimationValueInterpolator_interpolate__SWIG_3(Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;FJJ)J
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 350
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-direct {v0, p2, p3, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-direct {v1, p4, p5, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->interpolate(FLcom/samsung/android/sdk/sgi/base/SGVector4f;Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static SwigDirector_SGAnimationValueInterpolator_interpolate__SWIG_4(Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;FJJ)J
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 353
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    invoke-direct {v0, p2, p3, v2}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    invoke-direct {v1, p4, p5, v2}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationValueInterpolator;->interpolate(FLcom/samsung/android/sdk/sgi/base/SGQuaternion;Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static SwigDirector_SGVisualValueReceiver_onContentRect(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;J)V
    .locals 3

    .prologue
    .line 386
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onContentRect(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 387
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onContentRectScale(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;J)V
    .locals 3

    .prologue
    .line 389
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onContentRectScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 390
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onCustomProperty__SWIG_0(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;F)V
    .locals 0

    .prologue
    .line 395
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onCustomProperty(Ljava/lang/String;F)V

    .line 396
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onCustomProperty__SWIG_1(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 398
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/4 v1, 0x0

    invoke-direct {v0, p2, p3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onCustomProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 399
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onCustomProperty__SWIG_2(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 401
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    const/4 v1, 0x0

    invoke-direct {v0, p2, p3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onCustomProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 402
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onCustomProperty__SWIG_3(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 404
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    const/4 v1, 0x0

    invoke-direct {v0, p2, p3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onCustomProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 405
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onCustomProperty__SWIG_4(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 407
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    const/4 v1, 0x0

    invoke-direct {v0, p2, p3, v1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onCustomProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 408
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onGeometryGeneratorParam(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;F)V
    .locals 0

    .prologue
    .line 392
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onGeometryGeneratorParam(F)V

    .line 393
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onOpacity(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;F)V
    .locals 0

    .prologue
    .line 380
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onOpacity(F)V

    .line 381
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onOther(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;)V
    .locals 0

    .prologue
    .line 410
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onOther()V

    .line 411
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onPosition(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;J)V
    .locals 3

    .prologue
    .line 359
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onPosition(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 360
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onPositionPivot(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;J)V
    .locals 3

    .prologue
    .line 362
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onPositionPivot(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 363
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onRotation(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;J)V
    .locals 3

    .prologue
    .line 365
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onRotation(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 366
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onRotationPivot(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;J)V
    .locals 3

    .prologue
    .line 368
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onRotationPivot(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 369
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onScale(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;J)V
    .locals 3

    .prologue
    .line 371
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onScale(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 372
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onScalePivot(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;J)V
    .locals 3

    .prologue
    .line 374
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onScalePivot(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 375
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onSize(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;J)V
    .locals 3

    .prologue
    .line 377
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 378
    return-void
.end method

.method public static SwigDirector_SGVisualValueReceiver_onSpriteRect(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;J)V
    .locals 3

    .prologue
    .line 383
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;->onSpriteRect(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 384
    return-void
.end method

.method public static final native delete_SGAnimation(J)V
.end method

.method public static final native delete_SGAnimationFactory(J)V
.end method

.method public static final native delete_SGAnimationListenerBase(J)V
.end method

.method public static final native delete_SGAnimationTimingFunction(J)V
.end method

.method public static final native delete_SGAnimationTransaction(J)V
.end method

.method public static final native delete_SGAnimationValueInterpolator(J)V
.end method

.method public static final native delete_SGFloatAnimation(J)V
.end method

.method public static final native delete_SGGroupAnimation(J)V
.end method

.method public static final native delete_SGLinearValueInterpolator(J)V
.end method

.method public static final native delete_SGParticleAnimation(J)V
.end method

.method public static final native delete_SGPropertyAnimation(J)V
.end method

.method public static final native delete_SGQuaternionAnimation(J)V
.end method

.method public static final native delete_SGSpriteAnimation(J)V
.end method

.method public static final native delete_SGTransitionAnimation(J)V
.end method

.method public static final native delete_SGVector2fAnimation(J)V
.end method

.method public static final native delete_SGVector3fAnimation(J)V
.end method

.method public static final native delete_SGVector4fAnimation(J)V
.end method

.method public static final native delete_SGVisualValueReceiver(J)V
.end method

.method public static final native new_SGAnimationListenerBase__SWIG_0()J
.end method

.method public static final native new_SGAnimationListenerBase__SWIG_1(JLcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)J
.end method

.method public static final native new_SGAnimationTimingFunction()J
.end method

.method public static final native new_SGAnimationTransaction()J
.end method

.method public static final native new_SGAnimationValueInterpolator()J
.end method

.method public static final native new_SGLinearValueInterpolator__SWIG_1(Z)J
.end method

.method public static final native new_SGVisualValueReceiver()J
.end method

.method private static final native swig_module_init()V
.end method
