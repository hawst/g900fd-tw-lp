.class public Lcom/samsung/android/sdk/sgi/vi/SGTextureView;
.super Landroid/view/TextureView;
.source "SGTextureView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/vi/SGTextureView$SGSurfaceTextureListener;
    }
.end annotation


# instance fields
.field private isSurfaceTextureAvailable:Z

.field private mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->isSurfaceTextureAvailable:Z

    .line 49
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-direct {v0, p0, v1, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    .line 50
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->init()V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->isSurfaceTextureAvailable:Z

    .line 59
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-direct {v0, p0, v1, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    .line 60
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->init()V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-direct {p0, p1, p2, p3}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->isSurfaceTextureAvailable:Z

    .line 71
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-direct {v0, p0, v1, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    .line 72
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->init()V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;)V
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->isSurfaceTextureAvailable:Z

    .line 82
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p2, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    .line 83
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->init()V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->isSurfaceTextureAvailable:Z

    .line 105
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-direct {v0, p0, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    .line 106
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->init()V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V
    .locals 2

    .prologue
    .line 92
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->isSurfaceTextureAvailable:Z

    .line 93
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    .line 94
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->init()V

    .line 95
    return-void
.end method

.method static synthetic access$001(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/view/View;->checkInputConnectionProxy(Landroid/view/View;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;Z)Z
    .locals 0

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->isSurfaceTextureAvailable:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;)Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    return-object v0
.end method

.method static synthetic access$301(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$401(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$501(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$601(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$701(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$801(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;IILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method private init()V
    .locals 1

    .prologue
    .line 234
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$SGSurfaceTextureListener;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$SGSurfaceTextureListener;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;)V

    invoke-super {p0, v0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 235
    return-void
.end method


# virtual methods
.method public attachCurrentThread()V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->attachCurrentThread()V

    .line 205
    return-void
.end method

.method public checkInputConnectionProxy(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    new-instance v1, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$1;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;)V

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->checkInputConnectionProxy(Landroid/view/View;Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultCheckInputConnectionProxyStrategy;)Z

    move-result v0

    return v0
.end method

.method protected final createHoverEvent(Landroid/view/MotionEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->createHoverEvent(Landroid/view/MotionEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    move-result-object v0

    return-object v0
.end method

.method protected final createSGTouchEvent(Landroid/view/MotionEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->createSGTouchEvent(Landroid/view/MotionEvent;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;

    move-result-object v0

    return-object v0
.end method

.method public detachCurrentThread()V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->detachCurrentThread()V

    .line 210
    return-void
.end method

.method public getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    return-object v0
.end method

.method public getSurfaceStateListener()Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->getSurfaceStateListener()Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

    move-result-object v0

    return-object v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    return-object v0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    new-instance v1, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$3;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;)V

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->onHoverEvent(Landroid/view/MotionEvent;Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnHoverEventStrategy;)Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->onKeyEvent(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyEvent(IILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    new-instance v1, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$5;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;)V

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->onKeyEvent(IILandroid/view/KeyEvent;Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnKeyEventStrategy;)Z

    move-result v0

    return v0
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 360
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    new-instance v1, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$4;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;)V

    invoke-virtual {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->onKeyLongPress(ILandroid/view/KeyEvent;Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnKeyLongPress;)Z

    move-result v0

    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 349
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->onKeyEvent(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->onKeyEvent(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    new-instance v1, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView$2;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGTextureView;)V

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->onTouchEvent(Landroid/view/MotionEvent;Lcom/samsung/android/sdk/sgi/vi/SGViewImpl$DefaultOnTouchEventStrategy;)Z

    move-result v0

    return v0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 172
    invoke-super {p0, p1, p2}, Landroid/view/TextureView;->onVisibilityChanged(Landroid/view/View;I)V

    .line 173
    if-nez p2, :cond_0

    .line 174
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    .line 175
    if-eqz v0, :cond_0

    .line 177
    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getRoot()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    .line 178
    if-eqz v0, :cond_0

    .line 180
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->setVisibility(Z)V

    .line 184
    :cond_0
    return-void
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->resume()V

    .line 259
    return-void
.end method

.method public setAlpha(F)V
    .locals 1

    .prologue
    .line 266
    invoke-super {p0, p1}, Landroid/view/TextureView;->setAlpha(F)V

    .line 267
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setOpacity(F)V

    .line 268
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setColor(I)V

    .line 159
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 164
    const-string v0, "Sgi"

    const-string v1, "SGTextureView doesn\'t support drawables."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    return-void
.end method

.method public setForceMemoryManagementEnabled(Z)V
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->setForceMemoryManagementEnabled(Z)V

    .line 395
    return-void
.end method

.method public setSurfaceStateListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;)V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->setSurfaceStateListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;)V

    .line 240
    return-void
.end method

.method public final setSurfaceTexture(Landroid/graphics/SurfaceTexture;)V
    .locals 2

    .prologue
    .line 112
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SGTexureView: setSurfaceTexture() call is not allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V
    .locals 2

    .prologue
    .line 121
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SGTexureView: setSurfaceTextureListener() call is not allowed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setVisibility(I)V
    .locals 1

    .prologue
    .line 191
    if-nez p1, :cond_1

    .line 192
    invoke-super {p0, p1}, Landroid/view/TextureView;->setVisibility(I)V

    .line 193
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->resume()V

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    const/4 v0, 0x4

    if-eq p1, v0, :cond_2

    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    .line 196
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->suspend()V

    .line 197
    invoke-super {p0, p1}, Landroid/view/TextureView;->setVisibility(I)V

    goto :goto_0
.end method

.method public suspend()V
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;->mImpl:Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGViewImpl;->suspend()V

    .line 252
    return-void
.end method
