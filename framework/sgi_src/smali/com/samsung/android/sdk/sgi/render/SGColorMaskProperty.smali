.class public Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGProperty;
.source "SGColorMaskProperty.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 40
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGColorMaskProperty()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;-><init>(JZ)V

    .line 41
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->init()V

    .line 42
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 26
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGColorMaskProperty_init(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;)V

    .line 46
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;)Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;
    .locals 7

    .prologue
    .line 77
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGColorMaskProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGColorMaskProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public getWriteStateA()Z
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGColorMaskProperty_getWriteStateA(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;)Z

    move-result v0

    return v0
.end method

.method public getWriteStateB()Z
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGColorMaskProperty_getWriteStateB(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;)Z

    move-result v0

    return v0
.end method

.method public getWriteStateG()Z
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGColorMaskProperty_getWriteStateG(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;)Z

    move-result v0

    return v0
.end method

.method public getWriteStateR()Z
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGColorMaskProperty_getWriteStateR(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;)Z

    move-result v0

    return v0
.end method

.method public isColorMaskingEnabled()Z
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGColorMaskProperty_isColorMaskingEnabled(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;)Z

    move-result v0

    return v0
.end method

.method public setColorMaskingEnabled(Z)V
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGColorMaskProperty_setColorMaskingEnabled(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;Z)V

    .line 70
    return-void
.end method

.method public setWriteState(ZZZZ)V
    .locals 7

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGColorMaskProperty_setWriteState(JLcom/samsung/android/sdk/sgi/render/SGColorMaskProperty;ZZZZ)V

    .line 74
    return-void
.end method
