.class public Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;
.super Ljava/lang/Object;
.source "SGVisualValueProvider.java"


# instance fields
.field mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;

    .line 37
    return-void
.end method


# virtual methods
.method public getCurrentValue(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;->getCurrentValue(Lcom/samsung/android/sdk/sgi/animation/SGVisualValueReceiver;)V

    .line 51
    :cond_0
    return-void
.end method

.method setAnimationListenerBase(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;->mListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListenerBase;

    .line 42
    return-void
.end method
