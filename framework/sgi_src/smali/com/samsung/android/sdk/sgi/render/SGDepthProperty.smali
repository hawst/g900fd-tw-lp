.class public Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;
.super Lcom/samsung/android/sdk/sgi/render/SGProperty;
.source "SGDepthProperty.java"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 40
    invoke-static {}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->new_SGDepthProperty()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;-><init>(JZ)V

    .line 41
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->init()V

    .line 42
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/render/SGProperty;-><init>(JZ)V

    .line 26
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGDepthProperty_init(JLcom/samsung/android/sdk/sgi/render/SGDepthProperty;)V

    .line 46
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;)Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;
    .locals 7

    .prologue
    .line 65
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGProperty;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGDepthProperty___assign__(JLcom/samsung/android/sdk/sgi/render/SGDepthProperty;JLcom/samsung/android/sdk/sgi/render/SGDepthProperty;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;-><init>(JZ)V

    return-object v6
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 29
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->swigCMemOwn:Z

    .line 32
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->delete_SGDepthProperty(J)V

    .line 34
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->swigCPtr:J

    .line 36
    :cond_1
    return-void
.end method

.method public isDepthTestEnabled()Z
    .locals 2

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGDepthProperty_isDepthTestEnabled(JLcom/samsung/android/sdk/sgi/render/SGDepthProperty;)Z

    move-result v0

    return v0
.end method

.method public isWriteEnabled()Z
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGDepthProperty_isWriteEnabled(JLcom/samsung/android/sdk/sgi/render/SGDepthProperty;)Z

    move-result v0

    return v0
.end method

.method public setDepthTestEnabled(Z)V
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGDepthProperty_setDepthTestEnabled(JLcom/samsung/android/sdk/sgi/render/SGDepthProperty;Z)V

    .line 58
    return-void
.end method

.method public setWriteEnabled(Z)V
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/render/SGJNI;->SGDepthProperty_setWriteEnabled(JLcom/samsung/android/sdk/sgi/render/SGDepthProperty;Z)V

    .line 62
    return-void
.end method
