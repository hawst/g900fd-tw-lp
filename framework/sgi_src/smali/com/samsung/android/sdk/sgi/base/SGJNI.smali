.class Lcom/samsung/android/sdk/sgi/base/SGJNI;
.super Ljava/lang/Object;
.source "SGJNI.java"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 21
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->initLibrary()V

    .line 279
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->swig_module_init()V

    .line 280
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native SGAngleConverter_deg2Rad(D)D
.end method

.method public static final native SGAngleConverter_rad2Deg(D)D
.end method

.method public static final native SGBox3f_createTransformed(JLcom/samsung/android/sdk/sgi/base/SGBox3f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J
.end method

.method public static final native SGBox3f_extend(JLcom/samsung/android/sdk/sgi/base/SGBox3f;JLcom/samsung/android/sdk/sgi/base/SGBox3f;)V
.end method

.method public static final native SGBox3f_getMax(JLcom/samsung/android/sdk/sgi/base/SGBox3f;)J
.end method

.method public static final native SGBox3f_getMin(JLcom/samsung/android/sdk/sgi/base/SGBox3f;)J
.end method

.method public static final native SGBox3f_resetToZero(JLcom/samsung/android/sdk/sgi/base/SGBox3f;)V
.end method

.method public static final native SGBox3f_setBox(JLcom/samsung/android/sdk/sgi/base/SGBox3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGBox3f_setMax(JLcom/samsung/android/sdk/sgi/base/SGBox3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGBox3f_setMin(JLcom/samsung/android/sdk/sgi/base/SGBox3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGMatrix4f_add(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
.end method

.method public static final native SGMatrix4f_createLookAtLH(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J
.end method

.method public static final native SGMatrix4f_createLookAtRH(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J
.end method

.method public static final native SGMatrix4f_createOrthoLH__SWIG_0(FFFFFF)J
.end method

.method public static final native SGMatrix4f_createOrthoLH__SWIG_1(FFFF)J
.end method

.method public static final native SGMatrix4f_createOrthoRH__SWIG_0(FFFFFF)J
.end method

.method public static final native SGMatrix4f_createOrthoRH__SWIG_1(FFFF)J
.end method

.method public static final native SGMatrix4f_createPerspectiveFovLH(FFFF)J
.end method

.method public static final native SGMatrix4f_createPerspectiveFovRH(FFFF)J
.end method

.method public static final native SGMatrix4f_createPerspectiveLH__SWIG_0(FFFFFF)J
.end method

.method public static final native SGMatrix4f_createPerspectiveLH__SWIG_1(FFFF)J
.end method

.method public static final native SGMatrix4f_createPerspectiveRH__SWIG_0(FFFFFF)J
.end method

.method public static final native SGMatrix4f_createPerspectiveRH__SWIG_1(FFFF)J
.end method

.method public static final native SGMatrix4f_createRotationAxis(JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)J
.end method

.method public static final native SGMatrix4f_createRotationX(F)J
.end method

.method public static final native SGMatrix4f_createRotationY(F)J
.end method

.method public static final native SGMatrix4f_createRotationZ(F)J
.end method

.method public static final native SGMatrix4f_createRotation__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)J
.end method

.method public static final native SGMatrix4f_createRotation__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)J
.end method

.method public static final native SGMatrix4f_divide(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
.end method

.method public static final native SGMatrix4f_getColumn(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;I)J
.end method

.method public static final native SGMatrix4f_getDeterminant(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)F
.end method

.method public static final native SGMatrix4f_getElement__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;II)F
.end method

.method public static final native SGMatrix4f_getElement__SWIG_2(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;I)F
.end method

.method public static final native SGMatrix4f_getFullTranslation(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J
.end method

.method public static final native SGMatrix4f_getIdentity()J
.end method

.method public static final native SGMatrix4f_getQuaternion(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J
.end method

.method public static final native SGMatrix4f_getRow(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;I)J
.end method

.method public static final native SGMatrix4f_getTrace(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)F
.end method

.method public static final native SGMatrix4f_getTranslation(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J
.end method

.method public static final native SGMatrix4f_interpolateLineary(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;F)V
.end method

.method public static final native SGMatrix4f_interpolateSpherically(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;F)V
.end method

.method public static final native SGMatrix4f_inverse(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
.end method

.method public static final native SGMatrix4f_isEqual__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;F)Z
.end method

.method public static final native SGMatrix4f_isEqual__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)Z
.end method

.method public static final native SGMatrix4f_isIdentity(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)Z
.end method

.method public static final native SGMatrix4f_multiply(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
.end method

.method public static final native SGMatrix4f_multiplyByElements(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
.end method

.method public static final native SGMatrix4f_rotateAxis(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)V
.end method

.method public static final native SGMatrix4f_rotateQuaternion(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)J
.end method

.method public static final native SGMatrix4f_rotateVector__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J
.end method

.method public static final native SGMatrix4f_rotateVector__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)J
.end method

.method public static final native SGMatrix4f_rotateX(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;F)V
.end method

.method public static final native SGMatrix4f_rotateY(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;F)V
.end method

.method public static final native SGMatrix4f_rotateZ(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;F)V
.end method

.method public static final native SGMatrix4f_rotate__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGMatrix4f_rotate__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)V
.end method

.method public static final native SGMatrix4f_scale__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGMatrix4f_scale__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGMatrix4f_setColumn(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;IJLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGMatrix4f_setElement__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;IIF)V
.end method

.method public static final native SGMatrix4f_setElement__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;IF)V
.end method

.method public static final native SGMatrix4f_setRow(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;IJLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGMatrix4f_set__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
.end method

.method public static final native SGMatrix4f_set__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;FFFFFFFFFFFFFFFF)V
.end method

.method public static final native SGMatrix4f_subtract(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
.end method

.method public static final native SGMatrix4f_transformVector__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J
.end method

.method public static final native SGMatrix4f_transformVector__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)J
.end method

.method public static final native SGMatrix4f_translate(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGMatrix4f_translateVector__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J
.end method

.method public static final native SGMatrix4f_translateVector__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)J
.end method

.method public static final native SGMatrix4f_transpose(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
.end method

.method public static final native SGQuaternion_add__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGQuaternion_add__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V
.end method

.method public static final native SGQuaternion_conjugate(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGQuaternion_createRotationAxis__SWIG_0(FFFF)J
.end method

.method public static final native SGQuaternion_createRotationAxis__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)J
.end method

.method public static final native SGQuaternion_createRotationEuler__SWIG_0(FFFI)J
.end method

.method public static final native SGQuaternion_createRotationEuler__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)J
.end method

.method public static final native SGQuaternion_createRotationX(F)J
.end method

.method public static final native SGQuaternion_createRotationY(F)J
.end method

.method public static final native SGQuaternion_createRotationZ(F)J
.end method

.method public static final native SGQuaternion_divide(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V
.end method

.method public static final native SGQuaternion_exponent(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;FF)V
.end method

.method public static final native SGQuaternion_getDotProduct(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)F
.end method

.method public static final native SGQuaternion_getEulerAnglesXYZ(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)J
.end method

.method public static final native SGQuaternion_getEulerAnglesZ(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)F
.end method

.method public static final native SGQuaternion_getIdentity()J
.end method

.method public static final native SGQuaternion_getLength(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)F
.end method

.method public static final native SGQuaternion_getW(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)F
.end method

.method public static final native SGQuaternion_getX(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)F
.end method

.method public static final native SGQuaternion_getY(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)F
.end method

.method public static final native SGQuaternion_getZ(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)F
.end method

.method public static final native SGQuaternion_identity(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGQuaternion_interpolateLineary(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V
.end method

.method public static final native SGQuaternion_interpolateSpherically(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;FF)V
.end method

.method public static final native SGQuaternion_inverse(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGQuaternion_isEqual(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)Z
.end method

.method public static final native SGQuaternion_isIdentity(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)Z
.end method

.method public static final native SGQuaternion_multiply__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGQuaternion_multiply__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V
.end method

.method public static final native SGQuaternion_normalize(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGQuaternion_rotateAxis__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;FFFF)V
.end method

.method public static final native SGQuaternion_rotateAxis__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)V
.end method

.method public static final native SGQuaternion_rotateEuler__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;FFFI)V
.end method

.method public static final native SGQuaternion_rotateEuler__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)V
.end method

.method public static final native SGQuaternion_rotateX(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V
.end method

.method public static final native SGQuaternion_rotateY(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V
.end method

.method public static final native SGQuaternion_rotateZ(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V
.end method

.method public static final native SGQuaternion_setW(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V
.end method

.method public static final native SGQuaternion_setX(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V
.end method

.method public static final native SGQuaternion_setY(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V
.end method

.method public static final native SGQuaternion_setZ(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V
.end method

.method public static final native SGQuaternion_set__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V
.end method

.method public static final native SGQuaternion_set__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGQuaternion_set__SWIG_2(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;FFFF)V
.end method

.method public static final native SGQuaternion_subtract__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGQuaternion_subtract__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V
.end method

.method public static final native SGQuaternion_transformVector(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J
.end method

.method public static final native SGRegistrator_AddToManagementList(JLcom/samsung/android/sdk/sgi/base/SGRegistrator;J)Z
.end method

.method public static final native SGRegistrator_Deregister(JLcom/samsung/android/sdk/sgi/base/SGRegistrator;J)Z
.end method

.method public static final native SGRegistrator_GetObjectByPointer(JLcom/samsung/android/sdk/sgi/base/SGRegistrator;J)Ljava/lang/Object;
.end method

.method public static final native SGRegistrator_Register(JLcom/samsung/android/sdk/sgi/base/SGRegistrator;Ljava/lang/Object;J)Z
.end method

.method public static final native SGRegistrator_RemoveFromManagementList(JLcom/samsung/android/sdk/sgi/base/SGRegistrator;J)Z
.end method

.method public static final native SGRegistrator_change_ownership(Lcom/samsung/android/sdk/sgi/base/SGRegistrator;JZ)V
.end method

.method public static final native SGRegistrator_director_connect(Lcom/samsung/android/sdk/sgi/base/SGRegistrator;JZZ)V
.end method

.method public static final native SGVector2f_add(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVector2f_divide(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVector2f_getDistance(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)F
.end method

.method public static final native SGVector2f_getDotProduct(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)F
.end method

.method public static final native SGVector2f_getLength(JLcom/samsung/android/sdk/sgi/base/SGVector2f;)F
.end method

.method public static final native SGVector2f_getX(JLcom/samsung/android/sdk/sgi/base/SGVector2f;)F
.end method

.method public static final native SGVector2f_getY(JLcom/samsung/android/sdk/sgi/base/SGVector2f;)F
.end method

.method public static final native SGVector2f_interpolate(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;F)V
.end method

.method public static final native SGVector2f_inverse(JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVector2f_isEqual(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;F)Z
.end method

.method public static final native SGVector2f_multiply(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVector2f_normalize(JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVector2f_scale(JLcom/samsung/android/sdk/sgi/base/SGVector2f;F)V
.end method

.method public static final native SGVector2f_set(JLcom/samsung/android/sdk/sgi/base/SGVector2f;FF)V
.end method

.method public static final native SGVector2f_setX(JLcom/samsung/android/sdk/sgi/base/SGVector2f;F)V
.end method

.method public static final native SGVector2f_setY(JLcom/samsung/android/sdk/sgi/base/SGVector2f;F)V
.end method

.method public static final native SGVector2f_subtract(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGVector2i_add(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)V
.end method

.method public static final native SGVector2i_distanceSqr(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)I
.end method

.method public static final native SGVector2i_divide(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)V
.end method

.method public static final native SGVector2i_getDistance(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)F
.end method

.method public static final native SGVector2i_getDotProduct(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)I
.end method

.method public static final native SGVector2i_getLength(JLcom/samsung/android/sdk/sgi/base/SGVector2i;)F
.end method

.method public static final native SGVector2i_getX(JLcom/samsung/android/sdk/sgi/base/SGVector2i;)I
.end method

.method public static final native SGVector2i_getY(JLcom/samsung/android/sdk/sgi/base/SGVector2i;)I
.end method

.method public static final native SGVector2i_isEqual(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)Z
.end method

.method public static final native SGVector2i_multiply(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)V
.end method

.method public static final native SGVector2i_normalize(JLcom/samsung/android/sdk/sgi/base/SGVector2i;)V
.end method

.method public static final native SGVector2i_scale(JLcom/samsung/android/sdk/sgi/base/SGVector2i;I)V
.end method

.method public static final native SGVector2i_set(JLcom/samsung/android/sdk/sgi/base/SGVector2i;II)V
.end method

.method public static final native SGVector2i_setAt(JLcom/samsung/android/sdk/sgi/base/SGVector2i;II)V
.end method

.method public static final native SGVector2i_setX(JLcom/samsung/android/sdk/sgi/base/SGVector2i;I)V
.end method

.method public static final native SGVector2i_setY(JLcom/samsung/android/sdk/sgi/base/SGVector2i;I)V
.end method

.method public static final native SGVector2i_subtract(JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)V
.end method

.method public static final native SGVector3f_add(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVector3f_createCrossed(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J
.end method

.method public static final native SGVector3f_crossProduct(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVector3f_divide(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVector3f_getDistance(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)F
.end method

.method public static final native SGVector3f_getDotProduct(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)F
.end method

.method public static final native SGVector3f_getLength(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)F
.end method

.method public static final native SGVector3f_getX(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)F
.end method

.method public static final native SGVector3f_getY(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)F
.end method

.method public static final native SGVector3f_getZ(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)F
.end method

.method public static final native SGVector3f_interpolate(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)V
.end method

.method public static final native SGVector3f_inverse(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVector3f_isEqual(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)Z
.end method

.method public static final native SGVector3f_multiply(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVector3f_normalize(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVector3f_scale(JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)V
.end method

.method public static final native SGVector3f_set(JLcom/samsung/android/sdk/sgi/base/SGVector3f;FFF)V
.end method

.method public static final native SGVector3f_setX(JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)V
.end method

.method public static final native SGVector3f_setY(JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)V
.end method

.method public static final native SGVector3f_setZ(JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)V
.end method

.method public static final native SGVector3f_subtract(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGVector4f_add(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGVector4f_divide(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGVector4f_getAsRect(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)Landroid/graphics/RectF;
.end method

.method public static final native SGVector4f_getDistance(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)F
.end method

.method public static final native SGVector4f_getDotProduct(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)F
.end method

.method public static final native SGVector4f_getLength(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)F
.end method

.method public static final native SGVector4f_getW(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)F
.end method

.method public static final native SGVector4f_getX(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)F
.end method

.method public static final native SGVector4f_getY(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)F
.end method

.method public static final native SGVector4f_getZ(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)F
.end method

.method public static final native SGVector4f_interpolate(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;F)V
.end method

.method public static final native SGVector4f_inverse(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGVector4f_isEqual(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;F)Z
.end method

.method public static final native SGVector4f_multiply(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGVector4f_normalize(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static final native SGVector4f_scale(JLcom/samsung/android/sdk/sgi/base/SGVector4f;F)V
.end method

.method public static final native SGVector4f_set(JLcom/samsung/android/sdk/sgi/base/SGVector4f;FFFF)V
.end method

.method public static final native SGVector4f_setW(JLcom/samsung/android/sdk/sgi/base/SGVector4f;F)V
.end method

.method public static final native SGVector4f_setX(JLcom/samsung/android/sdk/sgi/base/SGVector4f;F)V
.end method

.method public static final native SGVector4f_setY(JLcom/samsung/android/sdk/sgi/base/SGVector4f;F)V
.end method

.method public static final native SGVector4f_setZ(JLcom/samsung/android/sdk/sgi/base/SGVector4f;F)V
.end method

.method public static final native SGVector4f_subtract(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)V
.end method

.method public static SwigDirector_SGRegistrator_AddToManagementList(Lcom/samsung/android/sdk/sgi/base/SGRegistrator;J)Z
    .locals 1

    .prologue
    .line 268
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->AddToManagementList(J)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGRegistrator_Deregister(Lcom/samsung/android/sdk/sgi/base/SGRegistrator;J)Z
    .locals 1

    .prologue
    .line 265
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->Deregister(J)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGRegistrator_GetObjectByPointer(Lcom/samsung/android/sdk/sgi/base/SGRegistrator;J)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->GetObjectByPointer(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static SwigDirector_SGRegistrator_Register(Lcom/samsung/android/sdk/sgi/base/SGRegistrator;Ljava/lang/Object;J)Z
    .locals 2

    .prologue
    .line 262
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->Register(Ljava/lang/Object;J)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGRegistrator_RemoveFromManagementList(Lcom/samsung/android/sdk/sgi/base/SGRegistrator;J)Z
    .locals 1

    .prologue
    .line 271
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->RemoveFromManagementList(J)Z

    move-result v0

    return v0
.end method

.method public static final native delete_SGAngleConverter(J)V
.end method

.method public static final native delete_SGBox3f(J)V
.end method

.method public static final native delete_SGMatrix4f(J)V
.end method

.method public static final native delete_SGQuaternion(J)V
.end method

.method public static final native delete_SGRegistrator(J)V
.end method

.method public static final native delete_SGVector2f(J)V
.end method

.method public static final native delete_SGVector2i(J)V
.end method

.method public static final native delete_SGVector3f(J)V
.end method

.method public static final native delete_SGVector4f(J)V
.end method

.method public static final native new_SGBox3f__SWIG_0()J
.end method

.method public static final native new_SGBox3f__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector3f;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J
.end method

.method public static final native new_SGMatrix4f__SWIG_0()J
.end method

.method public static final native new_SGMatrix4f__SWIG_1(FFFFFFFFFFFFFFFF)J
.end method

.method public static final native new_SGMatrix4f__SWIG_2(JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;JLcom/samsung/android/sdk/sgi/base/SGVector4f;)J
.end method

.method public static final native new_SGMatrix4f__SWIG_3(JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)J
.end method

.method public static final native new_SGQuaternion__SWIG_0()J
.end method

.method public static final native new_SGQuaternion__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)J
.end method

.method public static final native new_SGQuaternion__SWIG_2(FFFF)J
.end method

.method public static final native new_SGQuaternion__SWIG_3(F)J
.end method

.method public static final native new_SGRegistrator()J
.end method

.method public static final native new_SGVector2f__SWIG_0()J
.end method

.method public static final native new_SGVector2f__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector2f;)J
.end method

.method public static final native new_SGVector2f__SWIG_2(FF)J
.end method

.method public static final native new_SGVector2i__SWIG_0()J
.end method

.method public static final native new_SGVector2i__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector2i;)J
.end method

.method public static final native new_SGVector2i__SWIG_2(II)J
.end method

.method public static final native new_SGVector3f__SWIG_0()J
.end method

.method public static final native new_SGVector3f__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J
.end method

.method public static final native new_SGVector3f__SWIG_2(FFF)J
.end method

.method public static final native new_SGVector4f__SWIG_0()J
.end method

.method public static final native new_SGVector4f__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector4f;)J
.end method

.method public static final native new_SGVector4f__SWIG_2(FFFF)J
.end method

.method public static final native new_SGVector4f__SWIG_3(Landroid/graphics/RectF;)J
.end method

.method private static final native swig_module_init()V
.end method
