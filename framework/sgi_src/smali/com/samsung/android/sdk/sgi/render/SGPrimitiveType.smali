.class public final enum Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;
.super Ljava/lang/Enum;
.source "SGPrimitiveType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

.field public static final enum LINES:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

.field public static final enum LINE_LOOP:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

.field public static final enum LINE_STRIP:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

.field public static final enum POINTS:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

.field public static final enum POLYGON:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

.field public static final enum QUADS:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

.field public static final enum QUAD_STRIP:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

.field public static final enum TRIANGLES:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

.field public static final enum TRIANGLE_FAN:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

.field public static final enum TRIANGLE_STRIP:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    const-string v1, "POINTS"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->POINTS:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    const-string v1, "LINE_STRIP"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->LINE_STRIP:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    const-string v1, "LINE_LOOP"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->LINE_LOOP:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    const-string v1, "LINES"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->LINES:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    .line 23
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    const-string v1, "TRIANGLE_STRIP"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->TRIANGLE_STRIP:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    .line 24
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    const-string v1, "TRIANGLE_FAN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->TRIANGLE_FAN:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    .line 25
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    const-string v1, "TRIANGLES"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->TRIANGLES:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    .line 26
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    const-string v1, "QUAD_STRIP"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->QUAD_STRIP:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    .line 27
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    const-string v1, "QUADS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->QUADS:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    .line 28
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    const-string v1, "POLYGON"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->POLYGON:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    .line 18
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->POINTS:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->LINE_STRIP:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->LINE_LOOP:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->LINES:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->TRIANGLE_STRIP:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->TRIANGLE_FAN:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->TRIANGLES:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->QUAD_STRIP:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->QUADS:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->POLYGON:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    return-object v0
.end method
