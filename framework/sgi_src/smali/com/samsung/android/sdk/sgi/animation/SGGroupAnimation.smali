.class public final Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;
.super Lcom/samsung/android/sdk/sgi/animation/SGAnimation;
.source "SGGroupAnimation.java"


# direct methods
.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;-><init>(JZ)V

    .line 27
    return-void
.end method


# virtual methods
.method public __assign__(Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;)Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;
    .locals 7

    .prologue
    .line 69
    new-instance v6, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGGroupAnimation___assign__(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;-><init>(JZ)V

    return-object v6
.end method

.method public addAnimation(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)I
    .locals 6

    .prologue
    .line 45
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGGroupAnimation_addAnimation(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I

    move-result v0

    return v0
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 30
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 31
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->swigCMemOwn:Z

    .line 33
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->delete_SGGroupAnimation(J)V

    .line 35
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->swigCPtr:J

    .line 37
    :cond_1
    return-void
.end method

.method public getAnimation(I)Lcom/samsung/android/sdk/sgi/animation/SGAnimation;
    .locals 2

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGGroupAnimation_getAnimation(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;I)Lcom/samsung/android/sdk/sgi/animation/SGAnimation;

    move-result-object v0

    return-object v0
.end method

.method public getAnimationCount()I
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGGroupAnimation_getAnimationCount(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;)I

    move-result v0

    return v0
.end method

.method public insertAnimation(ILcom/samsung/android/sdk/sgi/animation/SGAnimation;)I
    .locals 7

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->swigCPtr:J

    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/animation/SGAnimation;->getCPtr(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)J

    move-result-wide v4

    move-object v2, p0

    move v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGGroupAnimation_insertAnimation(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;IJLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I

    move-result v0

    return v0
.end method

.method public isParallel()Z
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGGroupAnimation_isParallel(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;)Z

    move-result v0

    return v0
.end method

.method public removeAllAnimations()V
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGGroupAnimation_removeAllAnimations(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;)V

    .line 62
    return-void
.end method

.method public removeAnimation(I)Z
    .locals 2

    .prologue
    .line 65
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGGroupAnimation_removeAnimation(JLcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;I)Z

    move-result v0

    return v0
.end method
