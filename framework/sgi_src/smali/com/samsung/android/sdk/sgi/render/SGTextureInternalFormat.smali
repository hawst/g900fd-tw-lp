.class public final enum Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;
.super Ljava/lang/Enum;
.source "SGTextureInternalFormat.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

.field public static final enum DEPTH_24_STENCIL_8:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

.field public static final enum DEPTH_COMPONENT:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

.field public static final enum DEPTH_COMPONENT_16:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

.field public static final enum DEPTH_COMPONENT_24:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

.field public static final enum DEPTH_COMPONENT_32:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

.field public static final enum FOUR_COMPONENTS:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

.field public static final enum ONE_COMPONENT:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

.field public static final enum RGB:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

.field public static final enum RGBA:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

.field public static final enum STENCIL_INDEX:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

.field public static final enum STENCIL_INDEX_8:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

.field public static final enum THREE_COMPONENTS:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

.field public static final enum TWO_COMPONENTS:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    const-string v1, "ONE_COMPONENT"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->ONE_COMPONENT:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    const-string v1, "TWO_COMPONENTS"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->TWO_COMPONENTS:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    const-string v1, "THREE_COMPONENTS"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->THREE_COMPONENTS:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    const-string v1, "FOUR_COMPONENTS"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->FOUR_COMPONENTS:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    .line 23
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    const-string v1, "DEPTH_COMPONENT"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->DEPTH_COMPONENT:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    .line 24
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    const-string v1, "DEPTH_COMPONENT_16"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->DEPTH_COMPONENT_16:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    .line 25
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    const-string v1, "DEPTH_COMPONENT_24"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->DEPTH_COMPONENT_24:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    .line 26
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    const-string v1, "DEPTH_COMPONENT_32"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->DEPTH_COMPONENT_32:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    .line 27
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    const-string v1, "STENCIL_INDEX"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->STENCIL_INDEX:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    .line 28
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    const-string v1, "STENCIL_INDEX_8"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->STENCIL_INDEX_8:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    .line 29
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    const-string v1, "DEPTH_24_STENCIL_8"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->DEPTH_24_STENCIL_8:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    .line 30
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    const-string v1, "RGB"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->RGB:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    .line 31
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    const-string v1, "RGBA"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->RGBA:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    .line 18
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->ONE_COMPONENT:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->TWO_COMPONENTS:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->THREE_COMPONENTS:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->FOUR_COMPONENTS:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->DEPTH_COMPONENT:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->DEPTH_COMPONENT_16:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->DEPTH_COMPONENT_24:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->DEPTH_COMPONENT_32:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->STENCIL_INDEX:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->STENCIL_INDEX_8:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->DEPTH_24_STENCIL_8:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->RGB:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->RGBA:Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->$VALUES:[Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/render/SGTextureInternalFormat;

    return-object v0
.end method
