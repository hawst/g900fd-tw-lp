.class public Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;
.super Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;
.source "SGWidgetVideo.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$2;,
        Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$Drawer;,
        Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private afd:Landroid/content/res/AssetFileDescriptor;

.field private frames:Ljava/util/concurrent/atomic/AtomicInteger;

.field private imageRectTransform:[F

.field private mDrawer:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mOnFrameAvailableListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl;

.field private mSetBufferSizeMethod:Ljava/lang/reflect/Method;

.field private mSetSurfaceTextureMethod:Ljava/lang/reflect/Method;

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mTextureView:Landroid/view/TextureView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/view/TextureView;)V
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V

    .line 58
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->frames:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 91
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mOnFrameAvailableListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl;

    .line 250
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->obtainCallMethods()V

    .line 251
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->setTextureView(Landroid/view/TextureView;)V

    .line 252
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v0, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 253
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 254
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->onUpdate()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;)Landroid/graphics/SurfaceTexture;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;Landroid/graphics/SurfaceTexture;)Landroid/graphics/SurfaceTexture;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;)Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mOnFrameAvailableListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;)Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSetBufferSizeMethod:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;)Landroid/view/TextureView;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mTextureView:Landroid/view/TextureView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;)Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSetSurfaceTextureMethod:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->frames:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method private obtainCallMethods()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 336
    :try_start_0
    const-class v0, Landroid/view/TextureView;

    const-string v1, "setSurfaceTexture"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/graphics/SurfaceTexture;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSetSurfaceTextureMethod:Ljava/lang/reflect/Method;

    .line 337
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v1

    .line 338
    const-class v1, Landroid/graphics/SurfaceTexture;

    const-string v2, "setDefaultBufferSize"

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSetBufferSizeMethod:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 345
    return-void

    .line 339
    :catch_0
    move-exception v0

    .line 341
    iput-object v5, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSetSurfaceTextureMethod:Ljava/lang/reflect/Method;

    .line 342
    iput-object v5, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSetBufferSizeMethod:Ljava/lang/reflect/Method;

    .line 343
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "API level 16 is minimal for using this class"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private onUpdate()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v1, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 315
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->frames:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 316
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->imageRectTransform:[F

    invoke-virtual {v0, v2}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 318
    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>()V

    move v2, v1

    .line 319
    :goto_0
    if-ge v2, v8, :cond_1

    move v0, v1

    .line 320
    :goto_1
    if-ge v0, v8, :cond_0

    .line 321
    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->imageRectTransform:[F

    mul-int/lit8 v5, v2, 0x4

    add-int/2addr v5, v0

    aget v4, v4, v5

    invoke-virtual {v3, v0, v2, v4}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->setElement(IIF)V

    .line 320
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 319
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 323
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-direct {v0, v6, v6, v6, v7}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(FFFF)V

    .line 324
    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->transformVector(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    move-result-object v0

    .line 325
    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-direct {v1, v7, v7, v6, v7}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(FFFF)V

    .line 326
    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->transformVector(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    move-result-object v1

    .line 328
    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getX()F

    move-result v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getY()F

    move-result v0

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getX()F

    move-result v4

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getY()F

    move-result v1

    invoke-direct {v2, v3, v0, v4, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->setContentRect(Landroid/graphics/RectF;)V

    .line 329
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->invalidate()V

    .line 330
    return-void
.end method

.method private declared-synchronized resizeSurfaceTexture(II)V
    .locals 5

    .prologue
    .line 349
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 353
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSetBufferSizeMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTextureListener()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v0

    .line 355
    if-eqz v0, :cond_0

    .line 356
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-interface {v0, v1, p1, p2}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 357
    :catch_0
    move-exception v0

    .line 359
    :try_start_2
    const-string v1, "SGSurfaceRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error on setDefaultBufferSize into SurfaceTexture, - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 349
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 360
    :catch_1
    move-exception v0

    .line 362
    :try_start_3
    const-string v1, "SGSurfaceRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error on setDefaultBufferSize into SurfaceTexture, - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public destroyMediaPlayer()V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 189
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 191
    :cond_0
    return-void
.end method

.method public finalize()V
    .locals 0

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->destroyMediaPlayer()V

    .line 160
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->finalize()V

    .line 161
    return-void
.end method

.method public bridge synthetic getContentRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->getContentRect()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentRectPivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->getContentRectPivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentRectScale()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->getContentRectScale()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    return-object v0
.end method

.method public final getMediaPlayer()Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method public bridge synthetic getRenderer()Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->getRenderer()Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invalidate()V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->invalidate()V

    return-void
.end method

.method public bridge synthetic invalidate(Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->invalidate(Landroid/graphics/RectF;)V

    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 4

    .prologue
    .line 212
    if-nez p1, :cond_0

    .line 228
    :goto_0
    return-void

    .line 213
    :cond_0
    :try_start_0
    new-instance v0, Landroid/view/Surface;

    invoke-direct {v0, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    .line 214
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 215
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 216
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 217
    :catch_0
    move-exception v0

    .line 218
    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IllegalArgumentException "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 219
    :catch_1
    move-exception v0

    .line 220
    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SecurityException "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 221
    :catch_2
    move-exception v0

    .line 222
    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IllegalStateException "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 223
    :catch_3
    move-exception v0

    .line 224
    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NullPointerException "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 225
    :catch_4
    move-exception v0

    .line 226
    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IOException "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->destroyMediaPlayer()V

    .line 238
    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 244
    return-void
.end method

.method public onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mTextureView:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    .line 302
    invoke-static {p1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->createMotionEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 303
    if-eqz v1, :cond_0

    .line 305
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 306
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 310
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic setContentRect(Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setContentRect(Landroid/graphics/RectF;)V

    return-void
.end method

.method public bridge synthetic setContentRectPivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setContentRectPivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    return-void
.end method

.method public bridge synthetic setContentRectScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setContentRectScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    return-void
.end method

.method public bridge synthetic setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V

    return-void
.end method

.method public bridge synthetic setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;)V
    .locals 0

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;)V

    return-void
.end method

.method public setSize(FF)V
    .locals 2

    .prologue
    .line 264
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setSize(FF)V

    .line 265
    float-to-int v0, p1

    float-to-int v1, p2

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->resizeSurfaceTexture(II)V

    .line 266
    return-void
.end method

.method public setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 2

    .prologue
    .line 275
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 276
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->resizeSurfaceTexture(II)V

    .line 277
    return-void
.end method

.method public setSource(Landroid/content/res/AssetFileDescriptor;)V
    .locals 6

    .prologue
    .line 170
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->afd:Landroid/content/res/AssetFileDescriptor;

    .line 172
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 179
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->afd:Landroid/content/res/AssetFileDescriptor;

    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->afd:Landroid/content/res/AssetFileDescriptor;

    invoke-virtual {v2}, Landroid/content/res/AssetFileDescriptor;->getStartOffset()J

    move-result-wide v2

    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->afd:Landroid/content/res/AssetFileDescriptor;

    invoke-virtual {v4}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaPlayer;->setDataSource(Ljava/io/FileDescriptor;JJ)V

    .line 180
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 184
    :goto_0
    return-void

    .line 181
    :catch_0
    move-exception v0

    .line 182
    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setTextureView(Landroid/view/TextureView;)V
    .locals 2

    .prologue
    .line 281
    if-nez p1, :cond_0

    .line 282
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "textureView parameter is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mTextureView:Landroid/view/TextureView;

    if-nez v0, :cond_1

    .line 285
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$Drawer;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$Drawer;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mDrawer:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

    .line 286
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mDrawer:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V

    .line 287
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mTextureView:Landroid/view/TextureView;

    .line 288
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v0, p0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 289
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->imageRectTransform:[F

    .line 291
    :cond_1
    return-void
.end method

.method public setVisibility(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;)V
    .locals 2

    .prologue
    .line 368
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setVisibility(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;)V

    .line 369
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$2;->$SwitchMap$com$samsung$android$sdk$sgi$ui$SGWidgetVisibility:[I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 393
    :cond_0
    :goto_0
    return-void

    .line 371
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mTextureView:Landroid/view/TextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setVisibility(I)V

    .line 372
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->frames:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_1

    .line 374
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->invalidate()V

    .line 376
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mOnFrameAvailableListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$OnFrameAvailableListenerImpl;

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    goto :goto_0

    .line 380
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mTextureView:Landroid/view/TextureView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setVisibility(I)V

    .line 381
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    new-instance v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo$1;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;)V

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    goto :goto_0

    .line 390
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mTextureView:Landroid/view/TextureView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setVisibility(I)V

    goto :goto_0

    .line 369
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public startMediaPlayer()V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->getVisibility()Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->VISIBLE:Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 199
    :cond_0
    return-void
.end method

.method public stopMediaPlayer()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVideo;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 207
    :cond_0
    return-void
.end method
