.class public final Lcom/samsung/android/sdk/sgi/animation/SGAnimationFactory;
.super Ljava/lang/Object;
.source "SGAnimationFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createContentRectAnimation()Lcom/samsung/android/sdk/sgi/animation/SGVector4fAnimation;
    .locals 4

    .prologue
    .line 91
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGVector4fAnimation;

    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createContentRectAnimation()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGVector4fAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createContentRectScaleAnimation()Lcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;
    .locals 4

    .prologue
    .line 95
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;

    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createContentRectScaleAnimation()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createCustomFloatAnimation(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;
    .locals 4

    .prologue
    .line 99
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createCustomFloatAnimation(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createCustomVector2fAnimation(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;
    .locals 4

    .prologue
    .line 103
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createCustomVector2fAnimation(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createCustomVector3fAnimation(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;
    .locals 4

    .prologue
    .line 107
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createCustomVector3fAnimation(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createCustomVector4fAnimation(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/animation/SGVector4fAnimation;
    .locals 4

    .prologue
    .line 111
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGVector4fAnimation;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createCustomVector4fAnimation(Ljava/lang/String;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGVector4fAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createGeometryAnimation()Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;
    .locals 4

    .prologue
    .line 87
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;

    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createGeometryAnimation()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createGroupAnimation(Z)Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;
    .locals 4

    .prologue
    .line 51
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createGroupAnimation(Z)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createOpacityAnimation()Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;
    .locals 4

    .prologue
    .line 83
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;

    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createOpacityAnimation()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createParticleAnimation()Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;
    .locals 4

    .prologue
    .line 47
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;

    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createParticleAnimation()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGParticleAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createPositionAnimation()Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;
    .locals 4

    .prologue
    .line 59
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;

    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createPositionAnimation()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createPositionPivotAnimation()Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;
    .locals 4

    .prologue
    .line 63
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;

    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createPositionPivotAnimation()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createRotationAnimation()Lcom/samsung/android/sdk/sgi/animation/SGQuaternionAnimation;
    .locals 4

    .prologue
    .line 75
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGQuaternionAnimation;

    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createRotationAnimation()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGQuaternionAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createRotationPivotAnimation()Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;
    .locals 4

    .prologue
    .line 79
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;

    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createRotationPivotAnimation()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createScaleAnimation()Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;
    .locals 4

    .prologue
    .line 67
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;

    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createScaleAnimation()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createScalePivotAnimation()Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;
    .locals 4

    .prologue
    .line 71
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;

    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createScalePivotAnimation()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createSizeAnimation()Lcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;
    .locals 4

    .prologue
    .line 55
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;

    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createSizeAnimation()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGVector2fAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createSpriteAnimation()Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;
    .locals 4

    .prologue
    .line 43
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;

    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createSpriteAnimation()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGSpriteAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createTransitionAnimation(Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;)Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;
    .locals 4

    .prologue
    .line 28
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 30
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->ordinal()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createTransitionAnimation__SWIG_0(I)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;-><init>(JZ)V

    return-object v0
.end method

.method public static createTransitionAnimation(Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;Lcom/samsung/android/sdk/sgi/animation/SGTransitionDirectionType;)Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;
    .locals 4

    .prologue
    .line 35
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 36
    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 38
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionType;->ordinal()I

    move-result v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionDirectionType;->ordinal()I

    move-result v2

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/sgi/animation/SGJNI;->SGAnimationFactory_createTransitionAnimation__SWIG_1(II)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/animation/SGTransitionAnimation;-><init>(JZ)V

    return-object v0
.end method
