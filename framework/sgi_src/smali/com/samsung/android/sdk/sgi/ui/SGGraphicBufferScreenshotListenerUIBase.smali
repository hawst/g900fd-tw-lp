.class abstract Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;
.super Ljava/lang/Object;
.source "SGGraphicBufferScreenshotListenerUIBase.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method protected constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 50
    invoke-static {}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->new_SGGraphicBufferScreenshotListenerUIBase()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;-><init>(JZ)V

    .line 51
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;->swigCPtr:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;->swigCMemOwn:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->SGGraphicBufferScreenshotListenerUIBase_director_connect(Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;JZZ)V

    .line 52
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;->swigCMemOwn:Z

    .line 32
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;->swigCPtr:J

    .line 33
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;)J
    .locals 2

    .prologue
    .line 36
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;->swigCPtr:J

    goto :goto_0
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 40
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 41
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGJNI;->delete_SGGraphicBufferScreenshotListenerUIBase(J)V

    .line 42
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGGraphicBufferScreenshotListenerUIBase;->swigCPtr:J

    .line 44
    :cond_0
    return-void
.end method

.method public abstract onCompleted(Landroid/graphics/Bitmap;)V
.end method
