.class final Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;
.super Ljava/lang/Object;
.source "SGAssetRenderDataProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ShaderProgramBinaryRegistry"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;
    }
.end annotation


# static fields
.field private static final DATA_FILE_NAME:Ljava/lang/String; = "shader-program-binary-info"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mRegistry:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->mContext:Landroid/content/Context;

    .line 135
    :try_start_0
    new-instance v1, Ljava/io/ObjectInputStream;

    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->mContext:Landroid/content/Context;

    const-string v2, "shader-program-binary-info"

    invoke-virtual {v0, v2}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 136
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->mRegistry:Ljava/util/HashMap;

    .line 137
    invoke-virtual {v1}, Ljava/io/ObjectInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :goto_0
    return-void

    .line 138
    :catch_0
    move-exception v0

    .line 139
    const-string v0, "SGAssetRenderDataProvider"

    const-string v1, "shader program binary registry is empty"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->mRegistry:Ljava/util/HashMap;

    goto :goto_0
.end method

.method private getHash(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->mRegistry:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;

    return-object v0
.end method

.method private getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private save()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 145
    :try_start_0
    new-instance v0, Ljava/io/ObjectOutputStream;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->mContext:Landroid/content/Context;

    const-string v2, "shader-program-binary-info"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 147
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->mRegistry:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 148
    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :goto_0
    return-void

    .line 149
    :catch_0
    move-exception v0

    .line 150
    const-string v1, "SGAssetRenderDataProvider"

    const-string v2, "shader program binary save failed: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setHash(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;)V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->mRegistry:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->save()V

    .line 162
    return-void
.end method


# virtual methods
.method public load(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;)[B
    .locals 6

    .prologue
    .line 164
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->getHash(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;

    move-result-object v0

    .line 165
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;->vertex:I

    iget v2, p3, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;->vertex:I

    if-ne v1, v2, :cond_0

    iget v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;->fragment:I

    iget v1, p3, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;->fragment:I

    if-ne v0, v1, :cond_0

    .line 167
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider;->readStream(Ljava/io/InputStream;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 175
    :goto_0
    return-object v0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    const-string v1, "SGAssetRenderDataProvider"

    const-string v2, "shader program binary load failed (hit: vertex { name: %s hash: %H }, fragment { name: %s hash: %H }): %s"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    iget v5, p3, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;->vertex:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p2, v3, v4

    const/4 v4, 0x3

    iget v5, p3, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;->fragment:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public save(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;[B)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->mContext:Landroid/content/Context;

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/io/FileOutputStream;->write([B)V

    .line 181
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry;->setHash(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :goto_0
    return-void

    .line 182
    :catch_0
    move-exception v0

    .line 183
    const-string v1, "SGAssetRenderDataProvider"

    const-string v2, "shader program binary save failed (vertex { name: %s hash: %H }, fragment { name: %s hash: %H }): %s"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    const/4 v4, 0x1

    iget v5, p3, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;->vertex:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p2, v3, v4

    const/4 v4, 0x3

    iget v5, p3, Lcom/samsung/android/sdk/sgi/vi/SGAssetRenderDataProvider$ShaderProgramBinaryRegistry$ProgramHash;->fragment:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
