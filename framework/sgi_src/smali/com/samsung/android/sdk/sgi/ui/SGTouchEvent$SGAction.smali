.class public final enum Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;
.super Ljava/lang/Enum;
.source "SGTouchEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SGAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

.field public static final enum CANCEL:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

.field public static final enum DOWN:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

.field public static final enum HOVER_BUTTON_DOWN:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

.field public static final enum HOVER_BUTTON_UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

.field public static final enum HOVER_ENTER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

.field public static final enum HOVER_EXIT:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

.field public static final enum HOVER_MOVE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

.field public static final enum MOVE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

.field public static final enum NOTHING:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

.field public static final enum OUTSIDE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

.field public static final enum TOUCH_BUTTON_DOWN:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

.field public static final enum TOUCH_BUTTON_UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

.field public static final enum UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 304
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    const-string v1, "NOTHING"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->NOTHING:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 305
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    const-string v1, "DOWN"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->DOWN:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 306
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    const-string v1, "MOVE"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->MOVE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 307
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    const-string v1, "UP"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 308
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    const-string v1, "CANCEL"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->CANCEL:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 309
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    const-string v1, "OUTSIDE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->OUTSIDE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 310
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    const-string v1, "HOVER_ENTER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_ENTER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 311
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    const-string v1, "HOVER_MOVE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_MOVE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 312
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    const-string v1, "HOVER_EXIT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_EXIT:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 313
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    const-string v1, "HOVER_BUTTON_DOWN"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_BUTTON_DOWN:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 314
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    const-string v1, "HOVER_BUTTON_UP"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_BUTTON_UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 315
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    const-string v1, "TOUCH_BUTTON_DOWN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->TOUCH_BUTTON_DOWN:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 316
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    const-string v1, "TOUCH_BUTTON_UP"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->TOUCH_BUTTON_UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    .line 303
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->NOTHING:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->DOWN:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->MOVE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->CANCEL:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->OUTSIDE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_ENTER:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_MOVE:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_EXIT:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_BUTTON_DOWN:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->HOVER_BUTTON_UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->TOUCH_BUTTON_DOWN:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->TOUCH_BUTTON_UP:Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->$VALUES:[Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 303
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;
    .locals 1

    .prologue
    .line 303
    const-class v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;
    .locals 1

    .prologue
    .line 303
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->$VALUES:[Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent$SGAction;

    return-object v0
.end method
