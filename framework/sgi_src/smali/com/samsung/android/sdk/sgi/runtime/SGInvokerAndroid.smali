.class final Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid;
.super Ljava/lang/Object;
.source "SGInvokerAndroid.java"


# instance fields
.field private handler:Landroid/os/Handler;

.field private mNativeInstance:J

.field private runnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid;->mNativeInstance:J

    .line 25
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid;->handler:Landroid/os/Handler;

    .line 27
    new-instance v0, Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid$1;-><init>(Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid;->runnable:Ljava/lang/Runnable;

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid;)J
    .locals 2

    .prologue
    .line 16
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid;->mNativeInstance:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid;J)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid;->nativeInvokeCallback(J)V

    return-void
.end method

.method private native nativeInvokeCallback(J)V
.end method


# virtual methods
.method public forceInvokeImpl()V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    .line 49
    return-void
.end method

.method public invokeImpl()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 44
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/runtime/SGInvokerAndroid;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 39
    return-void
.end method
