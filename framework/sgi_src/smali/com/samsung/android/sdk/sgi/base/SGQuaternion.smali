.class public Lcom/samsung/android/sdk/sgi/base/SGQuaternion;
.super Ljava/lang/Object;
.source "SGQuaternion.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 46
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGQuaternion__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    .line 47
    return-void
.end method

.method public constructor <init>(F)V
    .locals 3

    .prologue
    .line 58
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGQuaternion__SWIG_3(F)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    .line 59
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 3

    .prologue
    .line 54
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGQuaternion__SWIG_2(FFFF)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    .line 55
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCMemOwn:Z

    .line 27
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
    .locals 3

    .prologue
    .line 50
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->new_SGQuaternion__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    .line 51
    return-void
.end method

.method public static createRotationAxis(FFFF)Lcom/samsung/android/sdk/sgi/base/SGQuaternion;
    .locals 4

    .prologue
    .line 166
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_createRotationAxis__SWIG_0(FFFF)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    return-object v0
.end method

.method public static createRotationAxis(Lcom/samsung/android/sdk/sgi/base/SGVector3f;F)Lcom/samsung/android/sdk/sgi/base/SGQuaternion;
    .locals 4

    .prologue
    .line 170
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v2

    invoke-static {v2, v3, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_createRotationAxis__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    return-object v0
.end method

.method public static createRotationEuler(FFFLcom/samsung/android/sdk/sgi/base/SGRotationOrder;)Lcom/samsung/android/sdk/sgi/base/SGQuaternion;
    .locals 4

    .prologue
    .line 174
    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 176
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    invoke-virtual {p3}, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->ordinal()I

    move-result v1

    invoke-static {p0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_createRotationEuler__SWIG_0(FFFI)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    return-object v0
.end method

.method public static createRotationEuler(Lcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;)Lcom/samsung/android/sdk/sgi/base/SGQuaternion;
    .locals 4

    .prologue
    .line 181
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 183
    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->ordinal()I

    move-result v1

    invoke-static {v2, v3, p0, v1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_createRotationEuler__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    return-object v0
.end method

.method public static createRotationX(F)Lcom/samsung/android/sdk/sgi/base/SGQuaternion;
    .locals 4

    .prologue
    .line 134
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_createRotationX(F)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    return-object v0
.end method

.method public static createRotationY(F)Lcom/samsung/android/sdk/sgi/base/SGQuaternion;
    .locals 4

    .prologue
    .line 138
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_createRotationY(F)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    return-object v0
.end method

.method public static createRotationZ(F)Lcom/samsung/android/sdk/sgi/base/SGQuaternion;
    .locals 4

    .prologue
    .line 142
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_createRotationZ(F)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    return-object v0
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J
    .locals 2

    .prologue
    .line 31
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    goto :goto_0
.end method

.method public static getIdentity()Lcom/samsung/android/sdk/sgi/base/SGQuaternion;
    .locals 4

    .prologue
    .line 218
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;

    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_getIdentity()J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;-><init>(JZ)V

    return-object v0
.end method


# virtual methods
.method public add(F)V
    .locals 2

    .prologue
    .line 110
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_add__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V

    .line 111
    return-void
.end method

.method public add(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
    .locals 6

    .prologue
    .line 106
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_add__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 107
    return-void
.end method

.method public conjugate()V
    .locals 2

    .prologue
    .line 202
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_conjugate(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 203
    return-void
.end method

.method public divide(F)V
    .locals 2

    .prologue
    .line 130
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_divide(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V

    .line 131
    return-void
.end method

.method public exponent(FF)V
    .locals 2

    .prologue
    .line 234
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_exponent(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;FF)V

    .line 235
    return-void
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 35
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 36
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCMemOwn:Z

    .line 38
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->delete_SGQuaternion(J)V

    .line 40
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    .line 42
    :cond_1
    return-void
.end method

.method public getDotProduct(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)F
    .locals 6

    .prologue
    .line 242
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_getDotProduct(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)F

    move-result v0

    return v0
.end method

.method public getEulerAnglesXYZ()Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 4

    .prologue
    .line 210
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v2, v3, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_getEulerAnglesXYZ(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v2

    const/4 v1, 0x1

    invoke-direct {v0, v2, v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v0
.end method

.method public getEulerAnglesZ()F
    .locals 2

    .prologue
    .line 214
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_getEulerAnglesZ(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)F

    move-result v0

    return v0
.end method

.method public getLength()F
    .locals 2

    .prologue
    .line 238
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_getLength(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)F

    move-result v0

    return v0
.end method

.method public getW()F
    .locals 2

    .prologue
    .line 102
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_getW(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)F

    move-result v0

    return v0
.end method

.method public getX()F
    .locals 2

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_getX(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)F

    move-result v0

    return v0
.end method

.method public getY()F
    .locals 2

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_getY(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)F

    move-result v0

    return v0
.end method

.method public getZ()F
    .locals 2

    .prologue
    .line 98
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_getZ(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)F

    move-result v0

    return v0
.end method

.method public identity()V
    .locals 2

    .prologue
    .line 222
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_identity(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 223
    return-void
.end method

.method public interpolateLineary(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V
    .locals 7

    .prologue
    .line 258
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_interpolateLineary(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V

    .line 259
    return-void
.end method

.method public interpolateSpherically(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;FF)V
    .locals 8

    .prologue
    .line 254
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    move v7, p3

    invoke-static/range {v0 .. v7}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_interpolateSpherically(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;FF)V

    .line 255
    return-void
.end method

.method public inverse()V
    .locals 2

    .prologue
    .line 230
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_inverse(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 231
    return-void
.end method

.method public isEqual(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;F)Z
    .locals 7

    .prologue
    .line 250
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_isEqual(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)Z

    move-result v0

    return v0
.end method

.method public isIdentity(F)Z
    .locals 2

    .prologue
    .line 226
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_isIdentity(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)Z

    move-result v0

    return v0
.end method

.method public multiply(F)V
    .locals 2

    .prologue
    .line 126
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_multiply__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V

    .line 127
    return-void
.end method

.method public multiply(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
    .locals 6

    .prologue
    .line 122
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_multiply__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 123
    return-void
.end method

.method public normalize()V
    .locals 2

    .prologue
    .line 246
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_normalize(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 247
    return-void
.end method

.method public rotateAxis(FFFF)V
    .locals 7

    .prologue
    .line 158
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_rotateAxis__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;FFFF)V

    .line 159
    return-void
.end method

.method public rotateAxis(Lcom/samsung/android/sdk/sgi/base/SGVector3f;F)V
    .locals 7

    .prologue
    .line 162
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move v6, p2

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_rotateAxis__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGVector3f;F)V

    .line 163
    return-void
.end method

.method public rotateEuler(FFFLcom/samsung/android/sdk/sgi/base/SGRotationOrder;)V
    .locals 7

    .prologue
    .line 188
    if-nez p4, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 190
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-virtual {p4}, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->ordinal()I

    move-result v6

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_rotateEuler__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;FFFI)V

    .line 192
    return-void
.end method

.method public rotateEuler(Lcom/samsung/android/sdk/sgi/base/SGVector3f;Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;)V
    .locals 7

    .prologue
    .line 195
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 197
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    invoke-virtual {p2}, Lcom/samsung/android/sdk/sgi/base/SGRotationOrder;->ordinal()I

    move-result v6

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_rotateEuler__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGVector3f;I)V

    .line 199
    return-void
.end method

.method public rotateX(F)V
    .locals 2

    .prologue
    .line 146
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_rotateX(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V

    .line 147
    return-void
.end method

.method public rotateY(F)V
    .locals 2

    .prologue
    .line 150
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_rotateY(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V

    .line 151
    return-void
.end method

.method public rotateZ(F)V
    .locals 2

    .prologue
    .line 154
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_rotateZ(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V

    .line 155
    return-void
.end method

.method public set(F)V
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_set__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V

    .line 63
    return-void
.end method

.method public set(FFFF)V
    .locals 7

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    move-object v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_set__SWIG_2(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;FFFF)V

    .line 71
    return-void
.end method

.method public set(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
    .locals 6

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_set__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 67
    return-void
.end method

.method public setW(F)V
    .locals 2

    .prologue
    .line 86
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_setW(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V

    .line 87
    return-void
.end method

.method public setX(F)V
    .locals 2

    .prologue
    .line 74
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_setX(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V

    .line 75
    return-void
.end method

.method public setY(F)V
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_setY(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V

    .line 79
    return-void
.end method

.method public setZ(F)V
    .locals 2

    .prologue
    .line 82
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_setZ(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V

    .line 83
    return-void
.end method

.method public subtract(F)V
    .locals 2

    .prologue
    .line 118
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {v0, v1, p0, p1}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_subtract__SWIG_1(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;F)V

    .line 119
    return-void
.end method

.method public subtract(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
    .locals 6

    .prologue
    .line 114
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGQuaternion;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_subtract__SWIG_0(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V

    .line 115
    return-void
.end method

.method public transformVector(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)Lcom/samsung/android/sdk/sgi/base/SGVector3f;
    .locals 7

    .prologue
    .line 206
    new-instance v6, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/base/SGQuaternion;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/base/SGJNI;->SGQuaternion_transformVector(JLcom/samsung/android/sdk/sgi/base/SGQuaternion;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {v6, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>(JZ)V

    return-object v6
.end method
