.class public Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;
.super Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;
.source "SGWidgetTextureView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$2;,
        Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;,
        Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$OnFrameAvailableListenerImpl;
    }
.end annotation


# instance fields
.field private frames:Ljava/util/concurrent/atomic/AtomicInteger;

.field private imageRectTransform:[F

.field private mDrawer:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

.field private mOnFrameAvailableListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$OnFrameAvailableListenerImpl;

.field private mSetBufferSizeMethod:Ljava/lang/reflect/Method;

.field private mSetSurfaceTextureMethod:Ljava/lang/reflect/Method;

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mTextureView:Landroid/view/TextureView;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Landroid/view/TextureView;)V
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;-><init>(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V

    .line 54
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->frames:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 83
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$OnFrameAvailableListenerImpl;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$OnFrameAvailableListenerImpl;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mOnFrameAvailableListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$OnFrameAvailableListenerImpl;

    .line 156
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->obtainCallMethods()V

    .line 157
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->setTextureView(Landroid/view/TextureView;)V

    .line 158
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->onUpdate()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Landroid/graphics/SurfaceTexture;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;Landroid/graphics/SurfaceTexture;)Landroid/graphics/SurfaceTexture;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$OnFrameAvailableListenerImpl;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mOnFrameAvailableListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$OnFrameAvailableListenerImpl;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSetBufferSizeMethod:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Landroid/view/TextureView;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mTextureView:Landroid/view/TextureView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Ljava/lang/reflect/Method;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSetSurfaceTextureMethod:Ljava/lang/reflect/Method;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->frames:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method private obtainCallMethods()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 241
    :try_start_0
    const-class v0, Landroid/view/TextureView;

    const-string v1, "setSurfaceTexture"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/graphics/SurfaceTexture;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSetSurfaceTextureMethod:Ljava/lang/reflect/Method;

    .line 242
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v2, v0, v1

    .line 243
    const-class v1, Landroid/graphics/SurfaceTexture;

    const-string v2, "setDefaultBufferSize"

    invoke-virtual {v1, v2, v0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSetBufferSizeMethod:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    return-void

    .line 244
    :catch_0
    move-exception v0

    .line 246
    iput-object v5, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSetSurfaceTextureMethod:Ljava/lang/reflect/Method;

    .line 247
    iput-object v5, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSetBufferSizeMethod:Ljava/lang/reflect/Method;

    .line 248
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "API level 16 is minimal for using this class"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private onUpdate()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v1, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 220
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->frames:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 221
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v2, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->imageRectTransform:[F

    invoke-virtual {v0, v2}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 223
    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>()V

    move v2, v1

    .line 224
    :goto_0
    if-ge v2, v8, :cond_1

    move v0, v1

    .line 225
    :goto_1
    if-ge v0, v8, :cond_0

    .line 226
    iget-object v4, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->imageRectTransform:[F

    mul-int/lit8 v5, v2, 0x4

    add-int/2addr v5, v0

    aget v4, v4, v5

    invoke-virtual {v3, v0, v2, v4}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->setElement(IIF)V

    .line 225
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 224
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 228
    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-direct {v0, v6, v6, v6, v7}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(FFFF)V

    .line 229
    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->transformVector(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    move-result-object v0

    .line 230
    new-instance v1, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-direct {v1, v7, v7, v6, v7}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>(FFFF)V

    .line 231
    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->transformVector(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    move-result-object v1

    .line 233
    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getX()F

    move-result v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getY()F

    move-result v0

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getX()F

    move-result v4

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->getY()F

    move-result v1

    invoke-direct {v2, v3, v0, v4, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->setContentRect(Landroid/graphics/RectF;)V

    .line 234
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->invalidate()V

    .line 235
    return-void
.end method

.method private declared-synchronized resizeSurfaceTexture(II)V
    .locals 5

    .prologue
    .line 254
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 258
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSetBufferSizeMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTextureListener()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v0

    .line 260
    if-eqz v0, :cond_0

    .line 261
    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-interface {v0, v1, p1, p2}, Landroid/view/TextureView$SurfaceTextureListener;->onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 262
    :catch_0
    move-exception v0

    .line 264
    :try_start_2
    const-string v1, "SGSurfaceRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error on setDefaultBufferSize into SurfaceTexture, - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 254
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 265
    :catch_1
    move-exception v0

    .line 267
    :try_start_3
    const-string v1, "SGSurfaceRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error on setDefaultBufferSize into SurfaceTexture, - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic finalize()V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->finalize()V

    return-void
.end method

.method public bridge synthetic getContentRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->getContentRect()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentRectPivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->getContentRectPivot()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentRectScale()Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->getContentRectScale()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getRenderer()Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->getRenderer()Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic invalidate()V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->invalidate()V

    return-void
.end method

.method public bridge synthetic invalidate(Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->invalidate(Landroid/graphics/RectF;)V

    return-void
.end method

.method public onTouchEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mTextureView:Landroid/view/TextureView;

    if-eqz v0, :cond_0

    .line 207
    invoke-static {p1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;->createMotionEvent(Lcom/samsung/android/sdk/sgi/ui/SGTouchEvent;Lcom/samsung/android/sdk/sgi/ui/SGWidget;)Landroid/view/MotionEvent;

    move-result-object v1

    .line 208
    if-eqz v1, :cond_0

    .line 210
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mTextureView:Landroid/view/TextureView;

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 211
    invoke-virtual {v1}, Landroid/view/MotionEvent;->recycle()V

    .line 215
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic setContentRect(Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setContentRect(Landroid/graphics/RectF;)V

    return-void
.end method

.method public bridge synthetic setContentRectPivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setContentRectPivot(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    return-void
.end method

.method public bridge synthetic setContentRectScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setContentRectScale(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    return-void
.end method

.method public bridge synthetic setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V

    return-void
.end method

.method public bridge synthetic setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;)V
    .locals 0

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRendererBase;)V

    return-void
.end method

.method public setSize(FF)V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setSize(FF)V

    .line 170
    float-to-int v0, p1

    float-to-int v1, p2

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->resizeSurfaceTexture(II)V

    .line 171
    return-void
.end method

.method public setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 2

    .prologue
    .line 180
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 181
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->resizeSurfaceTexture(II)V

    .line 182
    return-void
.end method

.method public setTextureView(Landroid/view/TextureView;)V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mTextureView:Landroid/view/TextureView;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 191
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$Drawer;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mDrawer:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

    .line 192
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mDrawer:Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->setRenderer(Lcom/samsung/android/sdk/sgi/ui/SGSurfaceRenderer;)V

    .line 193
    iput-object p1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mTextureView:Landroid/view/TextureView;

    .line 194
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->imageRectTransform:[F

    .line 196
    :cond_0
    return-void
.end method

.method public setVisibility(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;)V
    .locals 2

    .prologue
    .line 277
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetSurface;->setVisibility(Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;)V

    .line 278
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$2;->$SwitchMap$com$samsung$android$sdk$sgi$ui$SGWidgetVisibility:[I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetVisibility;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 302
    :cond_0
    :goto_0
    return-void

    .line 280
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mTextureView:Landroid/view/TextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setVisibility(I)V

    .line 281
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->frames:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-lez v0, :cond_1

    .line 283
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->invalidate()V

    .line 285
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mOnFrameAvailableListener:Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$OnFrameAvailableListenerImpl;

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    goto :goto_0

    .line 289
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mTextureView:Landroid/view/TextureView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setVisibility(I)V

    .line 290
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    new-instance v1, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView$1;-><init>(Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;)V

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    goto :goto_0

    .line 299
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/ui/SGWidgetTextureView;->mTextureView:Landroid/view/TextureView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setVisibility(I)V

    goto :goto_0

    .line 278
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
