.class public final enum Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;
.super Ljava/lang/Enum;
.source "SGKeyFlag.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

.field public static final enum CANCELED:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

.field public static final enum CANCELED_LONG_PRESS:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

.field public static final enum EDITOR_ACTION:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

.field public static final enum FALLBACK:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

.field public static final enum FROM_SYSTEM:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

.field public static final enum KEEP_TOUCH_MODE:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

.field public static final enum LONG_PRESS:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

.field public static final enum NONE:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

.field public static final enum SOFT_KEYBOARD:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

.field public static final enum TRACKING:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

.field public static final enum VIRTUAL_HARD_KEY:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

.field public static final enum WOKE_HERE:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 19
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->NONE:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    .line 20
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    const-string v1, "CANCELED"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->CANCELED:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    .line 21
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    const-string v1, "CANCELED_LONG_PRESS"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->CANCELED_LONG_PRESS:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    .line 22
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    const-string v1, "EDITOR_ACTION"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->EDITOR_ACTION:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    .line 23
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    const-string v1, "FALLBACK"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->FALLBACK:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    .line 24
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    const-string v1, "FROM_SYSTEM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->FROM_SYSTEM:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    .line 25
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    const-string v1, "KEEP_TOUCH_MODE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->KEEP_TOUCH_MODE:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    .line 26
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    const-string v1, "LONG_PRESS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->LONG_PRESS:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    .line 27
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    const-string v1, "SOFT_KEYBOARD"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->SOFT_KEYBOARD:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    .line 28
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    const-string v1, "TRACKING"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->TRACKING:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    .line 29
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    const-string v1, "VIRTUAL_HARD_KEY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->VIRTUAL_HARD_KEY:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    .line 30
    new-instance v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    const-string v1, "WOKE_HERE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->WOKE_HERE:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    .line 18
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->NONE:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->CANCELED:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->CANCELED_LONG_PRESS:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->EDITOR_ACTION:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->FALLBACK:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->FROM_SYSTEM:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->KEEP_TOUCH_MODE:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->LONG_PRESS:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->SOFT_KEYBOARD:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->TRACKING:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->VIRTUAL_HARD_KEY:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->WOKE_HERE:Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->$VALUES:[Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->$VALUES:[Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/ui/SGKeyFlag;

    return-object v0
.end method
