.class public final Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;
.super Ljava/lang/Object;
.source "SGConfiguration.java"


# static fields
.field private static mInstance:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->mInstance:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getBuildDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGConfiguration_getBuildDate()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getVersionInformation(I)I
    .locals 1

    .prologue
    .line 76
    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGConfiguration_getVersionInformation(I)I

    move-result v0

    return v0
.end method

.method public static getVersionInformation()Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;-><init>()V

    .line 30
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->getVersionInformation(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mMajor:I

    .line 31
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->getVersionInformation(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mMinor:I

    .line 32
    const/4 v1, 0x2

    invoke-static {v1}, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->getVersionInformation(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mPatch:I

    .line 33
    const/4 v1, 0x3

    invoke-static {v1}, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->getVersionInformation(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mBuild:I

    .line 34
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->getBuildDate()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mDate:Ljava/lang/String;

    .line 35
    return-object v0
.end method

.method public static initLibrary()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 42
    const-class v1, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;

    monitor-enter v1

    .line 44
    :try_start_0
    sget-boolean v0, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->mInstance:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 47
    :try_start_1
    const-string v0, "Sgi"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 53
    :goto_0
    :try_start_2
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/vi/SGContext;->initMemoryRegistrator(Lcom/samsung/android/sdk/sgi/base/SGRegistrator;)V

    .line 54
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->mInstance:Z

    .line 55
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->getVersionInformation()Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;

    move-result-object v0

    .line 56
    iget v2, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mMinor:I

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    iget v3, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mPatch:I

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    .line 57
    iget v3, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mMajor:I

    if-ne v3, v5, :cond_0

    const/16 v3, 0x302

    if-ge v2, v3, :cond_1

    .line 59
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wrong SGI library version "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mMajor:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mMinor:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Lcom/samsung/android/sdk/sgi/vi/SGVersionInformation;->mPatch:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " expected version: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 60
    const-string v2, "SGI"

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    new-instance v2, Ljava/lang/Error;

    invoke-direct {v2, v0}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v2

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 49
    :catch_0
    move-exception v0

    .line 50
    :try_start_3
    const-string v2, "SGI"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Native code library failed to load. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    goto/16 :goto_0

    .line 64
    :cond_1
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    return-void
.end method

.method public static isDebugInfoEnabled()Z
    .locals 1

    .prologue
    .line 70
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGConfiguration_isDebugInfoEnabled()Z

    move-result v0

    return v0
.end method

.method public static setDebugInfoEnabled(Z)V
    .locals 0

    .prologue
    .line 67
    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGConfiguration_setDebugInfoEnabled(Z)V

    .line 68
    return-void
.end method
