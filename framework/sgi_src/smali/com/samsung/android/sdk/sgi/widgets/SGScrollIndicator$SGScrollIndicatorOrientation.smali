.class public final enum Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;
.super Ljava/lang/Enum;
.source "SGScrollIndicator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SGScrollIndicatorOrientation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

.field public static final enum HORIZONTAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

.field public static final enum VERTICAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 297
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    const-string v1, "HORIZONTAL"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->HORIZONTAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    .line 298
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    const-string v1, "VERTICAL"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->VERTICAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    .line 296
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->HORIZONTAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->VERTICAL:Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->$VALUES:[Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 296
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;
    .locals 1

    .prologue
    .line 296
    const-class v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;
    .locals 1

    .prologue
    .line 296
    sget-object v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->$VALUES:[Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    invoke-virtual {v0}, [Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/sgi/widgets/SGScrollIndicator$SGScrollIndicatorOrientation;

    return-object v0
.end method
