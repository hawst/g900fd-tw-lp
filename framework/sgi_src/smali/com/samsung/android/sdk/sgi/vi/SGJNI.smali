.class Lcom/samsung/android/sdk/sgi/vi/SGJNI;
.super Ljava/lang/Object;
.source "SGJNI.java"


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 25
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGConfiguration;->initLibrary()V

    .line 411
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->swig_module_init()V

    .line 412
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final native SGBackgroundPropertyListenerBase_change_ownership(Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListenerBase;JZ)V
.end method

.method public static final native SGBackgroundPropertyListenerBase_director_connect(Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListenerBase;JZZ)V
.end method

.method public static final native SGBackgroundPropertyListenerBase_onFinish(JLcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListenerBase;JI)V
.end method

.method public static final native SGConfiguration_getBuildDate()Ljava/lang/String;
.end method

.method public static final native SGConfiguration_getVersionInformation(I)I
.end method

.method public static final native SGConfiguration_isDebugInfoEnabled()Z
.end method

.method public static final native SGConfiguration_setDebugInfoEnabled(Z)V
.end method

.method public static final native SGContext_attachCurrentThread(JLcom/samsung/android/sdk/sgi/vi/SGContext;)V
.end method

.method public static final native SGContext_attachToNativeWindow(JLcom/samsung/android/sdk/sgi/vi/SGContext;JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/view/Surface;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V
.end method

.method public static final native SGContext_cleanup(JLcom/samsung/android/sdk/sgi/vi/SGContext;)V
.end method

.method public static final native SGContext_detachCurrentThread(JLcom/samsung/android/sdk/sgi/vi/SGContext;)V
.end method

.method public static final native SGContext_detachFromNativeWindow(JLcom/samsung/android/sdk/sgi/vi/SGContext;JLcom/samsung/android/sdk/sgi/vi/SGSurface;)V
.end method

.method public static final native SGContext_initMemoryRegistrator(JLcom/samsung/android/sdk/sgi/base/SGRegistrator;)V
.end method

.method public static final native SGFilterPass_getProgramProperty(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;)J
.end method

.method public static final native SGFilterPass_getPropertyCount(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;)I
.end method

.method public static final native SGFilterPass_getProperty__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;I)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGFilterPass_getProperty__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGFilterPass_init__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;Z)V
.end method

.method public static final native SGFilterPass_init__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;)V
.end method

.method public static final native SGFilterPass_isAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;)Z
.end method

.method public static final native SGFilterPass_removeProperty__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;Ljava/lang/String;)V
.end method

.method public static final native SGFilterPass_removeProperty__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;I)V
.end method

.method public static final native SGFilterPass_setAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;Z)V
.end method

.method public static final native SGFilterPass_setProgramProperty(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V
.end method

.method public static final native SGFilterPass_setProperty(JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/render/SGProperty;)V
.end method

.method public static final native SGFilter___assign__(JLcom/samsung/android/sdk/sgi/vi/SGFilter;JLcom/samsung/android/sdk/sgi/vi/SGFilter;)J
.end method

.method public static final native SGFilter_addFilterPass(JLcom/samsung/android/sdk/sgi/vi/SGFilter;JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;)V
.end method

.method public static final native SGFilter_clone(JLcom/samsung/android/sdk/sgi/vi/SGFilter;)J
.end method

.method public static final native SGFilter_getFilterPass(JLcom/samsung/android/sdk/sgi/vi/SGFilter;I)J
.end method

.method public static final native SGFilter_getFilterPassCount(JLcom/samsung/android/sdk/sgi/vi/SGFilter;)I
.end method

.method public static final native SGFilter_getHandle(JLcom/samsung/android/sdk/sgi/vi/SGFilter;)J
.end method

.method public static final native SGFilter_init(JLcom/samsung/android/sdk/sgi/vi/SGFilter;)V
.end method

.method public static final native SGFilter_removeFilterPass__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGFilter;JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;)V
.end method

.method public static final native SGFilter_removeFilterPass__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGFilter;I)V
.end method

.method public static final native SGFpsIndicator_change_ownership(Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;JZ)V
.end method

.method public static final native SGFpsIndicator_director_connect(Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;JZZ)V
.end method

.method public static final native SGFpsIndicator_getFps(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;)F
.end method

.method public static final native SGFpsIndicator_getPosition(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;)J
.end method

.method public static final native SGFpsIndicator_getScreenSize(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;)J
.end method

.method public static final native SGFpsIndicator_getSize(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;)J
.end method

.method public static final native SGFpsIndicator_init__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;Landroid/graphics/RectF;F)V
.end method

.method public static final native SGFpsIndicator_init__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;Z)V
.end method

.method public static final native SGFpsIndicator_onDraw(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;Landroid/graphics/Bitmap;)V
.end method

.method public static final native SGFpsIndicator_setPosition(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGFpsIndicator_setSize(JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGGeometryGeneratorFactory_createCircleGeometryGenerator(JLcom/samsung/android/sdk/sgi/base/SGVector2f;FI)J
.end method

.method public static final native SGGeometryGeneratorFactory_createRectGeometryGenerator(Landroid/graphics/RectF;Landroid/graphics/RectF;)J
.end method

.method public static final native SGGeometryGeneratorFactory_createRoundBorderGeometryGenerator(Landroid/graphics/RectF;FFFFI)J
.end method

.method public static final native SGGeometryGeneratorFactory_createRoundRectGeometryGenerator(Landroid/graphics/RectF;FFI)J
.end method

.method public static final native SGGeometryGeneratorFactory_createTriangleGeometryGenerator(JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)J
.end method

.method public static final native SGGeometryGenerator_change_ownership(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;JZ)V
.end method

.method public static final native SGGeometryGenerator_director_connect(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;JZZ)V
.end method

.method public static final native SGGeometryGenerator_generate(JLcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;FJLcom/samsung/android/sdk/sgi/render/SGGeometry;FF)J
.end method

.method public static final native SGGeometryGenerator_init(JLcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V
.end method

.method public static final native SGGeometryGenerator_isBelongsToGeometry(JLcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)Z
.end method

.method public static final native SGGraphicBufferScreenshotListenerVIBase_change_ownership(Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;JZ)V
.end method

.method public static final native SGGraphicBufferScreenshotListenerVIBase_director_connect(Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;JZZ)V
.end method

.method public static final native SGGraphicBufferScreenshotListenerVIBase_onCompleted(JLcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;Landroid/graphics/Bitmap;)V
.end method

.method public static final native SGLayerAnimationListenerBase_change_ownership(Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;JZ)V
.end method

.method public static final native SGLayerAnimationListenerBase_director_connect(Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;JZZ)V
.end method

.method public static final native SGLayerAnimationListenerBase_onFinished(JLcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGLayerAnimationListenerBase_onFinishedSwigExplicitSGLayerAnimationListenerBase(JLcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGLayerAnimationListenerBase_onStarted(JLcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGLayerAnimationListenerBase_onStartedSwigExplicitSGLayerAnimationListenerBase(JLcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGLayerCanvasRedrawListenerBase_change_ownership(Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerBase;JZ)V
.end method

.method public static final native SGLayerCanvasRedrawListenerBase_director_connect(Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerBase;JZZ)V
.end method

.method public static final native SGLayerCanvasRedrawListenerBase_onDraw(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerBase;JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Canvas;)V
.end method

.method public static final native SGLayerCanvas_SWIGUpcast(J)J
.end method

.method public static final native SGLayerCanvas_getCanvasScale(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;)J
.end method

.method public static final native SGLayerCanvas_getContentRect(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;)Landroid/graphics/RectF;
.end method

.method public static final native SGLayerCanvas_getContentRectPivot(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;)J
.end method

.method public static final native SGLayerCanvas_getContentRectScale(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;)J
.end method

.method public static final native SGLayerCanvas_getFormat(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;)Landroid/graphics/Bitmap$Config;
.end method

.method public static final native SGLayerCanvas_init__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;)V
.end method

.method public static final native SGLayerCanvas_init__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;Landroid/graphics/RectF;)V
.end method

.method public static final native SGLayerCanvas_invalidate__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;)V
.end method

.method public static final native SGLayerCanvas_invalidate__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;Landroid/graphics/RectF;)V
.end method

.method public static final native SGLayerCanvas_setCanvasScale(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGLayerCanvas_setContentRect(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;Landroid/graphics/RectF;)V
.end method

.method public static final native SGLayerCanvas_setContentRectPivot(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGLayerCanvas_setContentRectScale(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGLayerCanvas_setFormat(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;Landroid/graphics/Bitmap$Config;)V
.end method

.method public static final native SGLayerCanvas_setRedrawListener(JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvas;JLcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerBase;)V
.end method

.method public static final native SGLayerImage_SWIGUpcast(J)J
.end method

.method public static final native SGLayerImage_getBlendMode(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;)I
.end method

.method public static final native SGLayerImage_getColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;)I
.end method

.method public static final native SGLayerImage_getContentRect(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;)Landroid/graphics/RectF;
.end method

.method public static final native SGLayerImage_getContentRectPivot(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;)J
.end method

.method public static final native SGLayerImage_getContentRectScale(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;)J
.end method

.method public static final native SGLayerImage_isAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;)Z
.end method

.method public static final native SGLayerImage_isPreMultipliedRGBAEnabled(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;)Z
.end method

.method public static final native SGLayerImage_setAlphaBlendingEnabled(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;Z)V
.end method

.method public static final native SGLayerImage_setBitmap(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;Landroid/graphics/Bitmap;)V
.end method

.method public static final native SGLayerImage_setBlendMode(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;I)V
.end method

.method public static final native SGLayerImage_setColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;I)V
.end method

.method public static final native SGLayerImage_setContentRect(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;Landroid/graphics/RectF;)V
.end method

.method public static final native SGLayerImage_setContentRectPivot(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGLayerImage_setContentRectScale(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGLayerImage_setPreMultipliedRGBAEnabled(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;Z)V
.end method

.method public static final native SGLayerImage_setTexture(JLcom/samsung/android/sdk/sgi/vi/SGLayerImage;JLcom/samsung/android/sdk/sgi/render/SGProperty;)V
.end method

.method public static final native SGLayerText_SWIGUpcast(J)J
.end method

.method public static final native SGLayerText_addGlyph(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;J[BIII)Z
.end method

.method public static final native SGLayerText_enableLocalCache(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;III)V
.end method

.method public static final native SGLayerText_getSelectionBackgroundColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;)I
.end method

.method public static final native SGLayerText_getSelectionTextColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;)I
.end method

.method public static final native SGLayerText_getTextColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;)I
.end method

.method public static final native SGLayerText_getTextDirection(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;)I
.end method

.method public static final native SGLayerText_isGlyphCached(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;J)Z
.end method

.method public static final native SGLayerText_isGlyphsCached(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;[J[BI)V
.end method

.method public static final native SGLayerText_setSelectionBackgroundColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;I)V
.end method

.method public static final native SGLayerText_setSelectionNative(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;II)V
.end method

.method public static final native SGLayerText_setSelectionTextColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;I)V
.end method

.method public static final native SGLayerText_setTextColor(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;I)V
.end method

.method public static final native SGLayerText_setTextDirection(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;I)V
.end method

.method public static final native SGLayerText_setTextNative(JLcom/samsung/android/sdk/sgi/vi/SGLayerText;[JI)V
.end method

.method public static final native SGLayer_addAnimation__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)I
.end method

.method public static final native SGLayer_addAnimation__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGFilterPass;JLcom/samsung/android/sdk/sgi/animation/SGAnimation;)V
.end method

.method public static final native SGLayer_addFilter(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGFilter;)V
.end method

.method public static final native SGLayer_addL__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)I
.end method

.method public static final native SGLayer_addL__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)I
.end method

.method public static final native SGLayer_bringLayerToF__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)V
.end method

.method public static final native SGLayer_bringLayerToF__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGLayer_bringToF(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGLayer_findLayerById(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
.end method

.method public static final native SGLayer_findLayerByName(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
.end method

.method public static final native SGLayer_getFilter(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)J
.end method

.method public static final native SGLayer_getFilterCount(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)I
.end method

.method public static final native SGLayer_getFullTransform(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getGeometryGeneratorParam(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)F
.end method

.method public static final native SGLayer_getLayerAtPoint(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
.end method

.method public static final native SGLayer_getLocalTransform(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getName(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Ljava/lang/String;
.end method

.method public static final native SGLayer_getOpacity(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)F
.end method

.method public static final native SGLayer_getParent(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayer;
.end method

.method public static final native SGLayer_getParentBounds(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Landroid/graphics/RectF;
.end method

.method public static final native SGLayer_getParentWidget(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Lcom/samsung/android/sdk/sgi/ui/SGWidget;
.end method

.method public static final native SGLayer_getPosition(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getPosition3f(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getPositionPivot(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getPositionPivot3f(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getProgramProperty(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getProperty(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGLayer_getRelativeToAnotherParentTransform(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getRelativeTransform(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getRotation(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getRotation3f(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getRotationPivot(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getRotationPivot3f(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getScale(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getScale3f(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getScalePivot(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getScalePivot3f(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getScreenBounds(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Landroid/graphics/RectF;
.end method

.method public static final native SGLayer_getSize(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)J
.end method

.method public static final native SGLayer_getSurface(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Lcom/samsung/android/sdk/sgi/vi/SGSurface;
.end method

.method public static final native SGLayer_getTransformationHint(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)I
.end method

.method public static final native SGLayer_getVisibility(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z
.end method

.method public static final native SGLayer_init__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGLayer_init__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/RectF;)V
.end method

.method public static final native SGLayer_isAddedToSuface(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z
.end method

.method public static final native SGLayer_isChildrenClippingEnabled(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z
.end method

.method public static final native SGLayer_isFiltersAttachDepthBuffer(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z
.end method

.method public static final native SGLayer_isFiltersAttachStencilBuffer(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z
.end method

.method public static final native SGLayer_isInheritOpacity(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z
.end method

.method public static final native SGLayer_isScreenShotAttachDepthBuffer(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z
.end method

.method public static final native SGLayer_isScreenShotAttachStencilBuffer(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Z
.end method

.method public static final native SGLayer_makeScreenShot__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGLayer_makeScreenShot__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGLayer_makeScreenShot__SWIG_10(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
.end method

.method public static final native SGLayer_makeScreenShot__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Rect;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGLayer_makeScreenShot__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGLayer_makeScreenShot__SWIG_4(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Bitmap;)V
.end method

.method public static final native SGLayer_makeScreenShot__SWIG_5(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
.end method

.method public static final native SGLayer_makeScreenShot__SWIG_6(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Bitmap;JLcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V
.end method

.method public static final native SGLayer_makeScreenShot__SWIG_7(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V
.end method

.method public static final native SGLayer_makeScreenShot__SWIG_8(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
.end method

.method public static final native SGLayer_makeScreenShot__SWIG_9(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
.end method

.method public static final native SGLayer_removeAllAnimations(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGLayer_removeAllFilters(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGLayer_removeAllL(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGLayer_removeAnimation(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)V
.end method

.method public static final native SGLayer_removeFilter__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGFilter;)V
.end method

.method public static final native SGLayer_removeFilter__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)V
.end method

.method public static final native SGLayer_removeL__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGLayer_removeL__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)V
.end method

.method public static final native SGLayer_removeProperty(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Ljava/lang/String;)V
.end method

.method public static final native SGLayer_resetFilterFrameBufferSize(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGLayer_sendLayerToB__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)V
.end method

.method public static final native SGLayer_sendLayerToB__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGLayer_sendToB(JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGLayer_setChildrenClipping(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Z)V
.end method

.method public static final native SGLayer_setFilterFrameBufferSize(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V
.end method

.method public static final native SGLayer_setFiltersOptions(JLcom/samsung/android/sdk/sgi/vi/SGLayer;ZZ)V
.end method

.method public static final native SGLayer_setGeometryGeneratorNative(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V
.end method

.method public static final native SGLayer_setGeometryGeneratorParam(JLcom/samsung/android/sdk/sgi/vi/SGLayer;F)V
.end method

.method public static final native SGLayer_setInheritOpacity(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Z)V
.end method

.method public static final native SGLayer_setLayerAnimationListener(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;)V
.end method

.method public static final native SGLayer_setLocalTransform(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGMatrix4f;)V
.end method

.method public static final native SGLayer_setName(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Ljava/lang/String;)V
.end method

.method public static final native SGLayer_setOpacity(JLcom/samsung/android/sdk/sgi/vi/SGLayer;F)V
.end method

.method public static final native SGLayer_setPivots__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGLayer_setPivots__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGLayer_setPivots__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V
.end method

.method public static final native SGLayer_setPivots__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FFF)V
.end method

.method public static final native SGLayer_setPositionPivot__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGLayer_setPositionPivot__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGLayer_setPositionPivot__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V
.end method

.method public static final native SGLayer_setPositionPivot__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FFF)V
.end method

.method public static final native SGLayer_setPosition__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGLayer_setPosition__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGLayer_setPosition__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V
.end method

.method public static final native SGLayer_setPosition__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FFF)V
.end method

.method public static final native SGLayer_setProgramProperty(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V
.end method

.method public static final native SGLayer_setProperty(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Ljava/lang/String;JLcom/samsung/android/sdk/sgi/render/SGProperty;)V
.end method

.method public static final native SGLayer_setRotationPivot__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGLayer_setRotationPivot__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGLayer_setRotationPivot__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V
.end method

.method public static final native SGLayer_setRotationPivot__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FFF)V
.end method

.method public static final native SGLayer_setRotationX(JLcom/samsung/android/sdk/sgi/vi/SGLayer;F)V
.end method

.method public static final native SGLayer_setRotationY(JLcom/samsung/android/sdk/sgi/vi/SGLayer;F)V
.end method

.method public static final native SGLayer_setRotationZ(JLcom/samsung/android/sdk/sgi/vi/SGLayer;F)V
.end method

.method public static final native SGLayer_setRotation__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGQuaternion;)V
.end method

.method public static final native SGLayer_setRotation__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FFF)V
.end method

.method public static final native SGLayer_setScalePivot__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGLayer_setScalePivot__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGLayer_setScalePivot__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V
.end method

.method public static final native SGLayer_setScalePivot__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FFF)V
.end method

.method public static final native SGLayer_setScale__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGLayer_setScale__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector3f;)V
.end method

.method public static final native SGLayer_setScale__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V
.end method

.method public static final native SGLayer_setScale__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FFF)V
.end method

.method public static final native SGLayer_setScreenShotOptions(JLcom/samsung/android/sdk/sgi/vi/SGLayer;ZZ)V
.end method

.method public static final native SGLayer_setSize__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGLayer_setSize__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;FF)V
.end method

.method public static final native SGLayer_setTransformationHint(JLcom/samsung/android/sdk/sgi/vi/SGLayer;I)V
.end method

.method public static final native SGLayer_setVisibility(JLcom/samsung/android/sdk/sgi/vi/SGLayer;Z)V
.end method

.method public static final native SGLayer_swapL__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;JLcom/samsung/android/sdk/sgi/vi/SGLayer;)V
.end method

.method public static final native SGLayer_swapL__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGLayer;II)V
.end method

.method public static final native SGScreenshotPropertyListenerVIBase_change_ownership(Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;JZ)V
.end method

.method public static final native SGScreenshotPropertyListenerVIBase_director_connect(Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;JZZ)V
.end method

.method public static final native SGScreenshotPropertyListenerVIBase_onCompleted(JLcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;J)V
.end method

.method public static final native SGSurfaceChangesDrawnListenerBase_change_ownership(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerBase;JZ)V
.end method

.method public static final native SGSurfaceChangesDrawnListenerBase_director_connect(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerBase;JZZ)V
.end method

.method public static final native SGSurfaceChangesDrawnListenerBase_onChangesDrawn(JLcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerBase;)V
.end method

.method public static final native SGSurfaceListenerBase_change_ownership(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;JZ)V
.end method

.method public static final native SGSurfaceListenerBase_director_connect(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;JZZ)V
.end method

.method public static final native SGSurfaceListenerBase_onFrameEnd(JLcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;)V
.end method

.method public static final native SGSurfaceListenerBase_onResize(JLcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGSurface_addChangesDrawnListener(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerBase;)V
.end method

.method public static final native SGSurface_addW__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)I
.end method

.method public static final native SGSurface_addW__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGWidget;I)I
.end method

.method public static final native SGSurface_bringWToF(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGSurface_dumpProfilingInfromation(JLcom/samsung/android/sdk/sgi/vi/SGSurface;I)V
.end method

.method public static final native SGSurface_enqueueTexture(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Bitmap;JLcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListenerBase;I)V
.end method

.method public static final native SGSurface_getCameraDistance(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)F
.end method

.method public static final native SGSurface_getCameraProjection(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)J
.end method

.method public static final native SGSurface_getCameraWorld(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)J
.end method

.method public static final native SGSurface_getColor(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)I
.end method

.method public static final native SGSurface_getFpsIndicator(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;
.end method

.method public static final native SGSurface_getFpsLimit(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)I
.end method

.method public static final native SGSurface_getHandle(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)J
.end method

.method public static final native SGSurface_getOpacity(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)F
.end method

.method public static final native SGSurface_getRootNative(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)J
.end method

.method public static final native SGSurface_getShowFpsIndicator(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)Z
.end method

.method public static final native SGSurface_getSize(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)J
.end method

.method public static final native SGSurface_getViewPortMax(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)J
.end method

.method public static final native SGSurface_getViewPortMin(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)J
.end method

.method public static final native SGSurface_getWidgetAtPoint(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)Lcom/samsung/android/sdk/sgi/ui/SGWidget;
.end method

.method public static final native SGSurface_getWidgetsCount(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)I
.end method

.method public static final native SGSurface_init(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/vi/SGContext;JLcom/samsung/android/sdk/sgi/base/SGVector2i;ZI)V
.end method

.method public static final native SGSurface_isScreenShotAttachDepthBuffer(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)Z
.end method

.method public static final native SGSurface_isScreenShotAttachStencilBuffer(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)Z
.end method

.method public static final native SGSurface_makeScreenShot__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGSurface_makeScreenShot__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGSurface_makeScreenShot__SWIG_10(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
.end method

.method public static final native SGSurface_makeScreenShot__SWIG_2(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Rect;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGSurface_makeScreenShot__SWIG_3(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
.end method

.method public static final native SGSurface_makeScreenShot__SWIG_4(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Bitmap;)V
.end method

.method public static final native SGSurface_makeScreenShot__SWIG_5(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
.end method

.method public static final native SGSurface_makeScreenShot__SWIG_6(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Bitmap;JLcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V
.end method

.method public static final native SGSurface_makeScreenShot__SWIG_7(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Bitmap;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;)V
.end method

.method public static final native SGSurface_makeScreenShot__SWIG_8(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/graphics/Rect;JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
.end method

.method public static final native SGSurface_makeScreenShot__SWIG_9(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2i;JLcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;)V
.end method

.method public static final native SGSurface_onHE(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
.end method

.method public static final native SGSurface_onKeyEvent(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGKeyEvent;)Z
.end method

.method public static final native SGSurface_onTE(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGTouchEvent;)Z
.end method

.method public static final native SGSurface_pushQueue(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)V
.end method

.method public static final native SGSurface_removeW__SWIG_0(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGSurface_removeW__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGSurface;I)V
.end method

.method public static final native SGSurface_resetViewport(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)V
.end method

.method public static final native SGSurface_resume(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)V
.end method

.method public static final native SGSurface_sendWToB(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/ui/SGWidget;)V
.end method

.method public static final native SGSurface_setCameraDistance(JLcom/samsung/android/sdk/sgi/vi/SGSurface;F)V
.end method

.method public static final native SGSurface_setColor(JLcom/samsung/android/sdk/sgi/vi/SGSurface;I)V
.end method

.method public static final native SGSurface_setDrawFrameListener(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;)V
.end method

.method public static final native SGSurface_setFpsIndicator(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;)V
.end method

.method public static final native SGSurface_setFpsLimit(JLcom/samsung/android/sdk/sgi/vi/SGSurface;I)V
.end method

.method public static final native SGSurface_setOpacity(JLcom/samsung/android/sdk/sgi/vi/SGSurface;F)V
.end method

.method public static final native SGSurface_setScreenShotOptions(JLcom/samsung/android/sdk/sgi/vi/SGSurface;ZZ)V
.end method

.method public static final native SGSurface_setShowFpsIndicator(JLcom/samsung/android/sdk/sgi/vi/SGSurface;Z)V
.end method

.method public static final native SGSurface_setSize(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2i;)V
.end method

.method public static final native SGSurface_setSizeChangeListener(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;)V
.end method

.method public static final native SGSurface_setViewport(JLcom/samsung/android/sdk/sgi/vi/SGSurface;JLcom/samsung/android/sdk/sgi/base/SGVector2f;JLcom/samsung/android/sdk/sgi/base/SGVector2f;)V
.end method

.method public static final native SGSurface_suspend(JLcom/samsung/android/sdk/sgi/vi/SGSurface;)V
.end method

.method public static SwigDirector_SGBackgroundPropertyListenerBase_onFinish(Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListenerBase;JI)V
    .locals 1

    .prologue
    .line 394
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGBackgroundPropertyListenerBase;->onFinish(JI)V

    .line 395
    return-void
.end method

.method public static SwigDirector_SGFpsIndicator_onDraw(Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 403
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGFpsIndicator;->onDraw(Landroid/graphics/Bitmap;)V

    .line 404
    return-void
.end method

.method public static SwigDirector_SGGeometryGenerator_generate(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;FJFF)J
    .locals 2

    .prologue
    .line 373
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, v0, p4, p5}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->generate(FLcom/samsung/android/sdk/sgi/render/SGGeometry;FF)Lcom/samsung/android/sdk/sgi/render/SGGeometry;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGGeometry;)J

    move-result-wide v0

    return-wide v0

    :cond_0
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGGeometry;

    const/4 v1, 0x0

    invoke-direct {v0, p2, p3, v1}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;-><init>(JZ)V

    goto :goto_0
.end method

.method public static SwigDirector_SGGeometryGenerator_isBelongsToGeometry(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;J)Z
    .locals 3

    .prologue
    .line 376
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->isBelongsToGeometry(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Z

    move-result v0

    return v0
.end method

.method public static SwigDirector_SGGraphicBufferScreenshotListenerVIBase_onCompleted(Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 379
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGGraphicBufferScreenshotListenerVIBase;->onCompleted(Landroid/graphics/Bitmap;)V

    .line 380
    return-void
.end method

.method public static SwigDirector_SGLayerAnimationListenerBase_onFinished(Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;J)V
    .locals 1

    .prologue
    .line 388
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;->onFinished(J)V

    .line 389
    return-void
.end method

.method public static SwigDirector_SGLayerAnimationListenerBase_onStarted(Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;J)V
    .locals 1

    .prologue
    .line 385
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerAnimationListenerBase;->onStarted(J)V

    .line 386
    return-void
.end method

.method public static SwigDirector_SGLayerCanvasRedrawListenerBase_onDraw(Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerBase;JLandroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 391
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerCanvasRedrawListenerBase;->onDraw(JLandroid/graphics/Canvas;)V

    .line 392
    return-void
.end method

.method public static SwigDirector_SGScreenshotPropertyListenerVIBase_onCompleted(Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;J)V
    .locals 1

    .prologue
    .line 382
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGScreenshotPropertyListenerVIBase;->onCompleted(J)V

    .line 383
    return-void
.end method

.method public static SwigDirector_SGSurfaceChangesDrawnListenerBase_onChangesDrawn(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerBase;)V
    .locals 0

    .prologue
    .line 406
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerBase;->onChangesDrawn()V

    .line 407
    return-void
.end method

.method public static SwigDirector_SGSurfaceListenerBase_onFrameEnd(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;)V
    .locals 0

    .prologue
    .line 400
    invoke-virtual {p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;->onFrameEnd()V

    .line 401
    return-void
.end method

.method public static SwigDirector_SGSurfaceListenerBase_onResize(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;J)V
    .locals 3

    .prologue
    .line 397
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(JZ)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/sgi/vi/SGSurfaceListenerBase;->onResize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 398
    return-void
.end method

.method public static final native delete_SGBackgroundPropertyListenerBase(J)V
.end method

.method public static final native delete_SGConfiguration(J)V
.end method

.method public static final native delete_SGContext(J)V
.end method

.method public static final native delete_SGFilter(J)V
.end method

.method public static final native delete_SGFilterPass(J)V
.end method

.method public static final native delete_SGFpsIndicator(J)V
.end method

.method public static final native delete_SGGeometryGenerator(J)V
.end method

.method public static final native delete_SGGeometryGeneratorFactory(J)V
.end method

.method public static final native delete_SGGraphicBufferScreenshotListenerVIBase(J)V
.end method

.method public static final native delete_SGLayer(J)V
.end method

.method public static final native delete_SGLayerAnimationListenerBase(J)V
.end method

.method public static final native delete_SGLayerCanvas(J)V
.end method

.method public static final native delete_SGLayerCanvasRedrawListenerBase(J)V
.end method

.method public static final native delete_SGLayerImage(J)V
.end method

.method public static final native delete_SGLayerText(J)V
.end method

.method public static final native delete_SGScreenshotPropertyListenerVIBase(J)V
.end method

.method public static final native delete_SGSurface(J)V
.end method

.method public static final native delete_SGSurfaceChangesDrawnListenerBase(J)V
.end method

.method public static final native delete_SGSurfaceListenerBase(J)V
.end method

.method public static final native new_SGBackgroundPropertyListenerBase()J
.end method

.method public static final native new_SGContext__SWIG_0()J
.end method

.method public static final native new_SGContext__SWIG_1(Landroid/content/Context;JLcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;)J
.end method

.method public static final native new_SGFilter()J
.end method

.method public static final native new_SGFilterPass__SWIG_0()J
.end method

.method public static final native new_SGFilterPass__SWIG_1(Z)J
.end method

.method public static final native new_SGFpsIndicator()J
.end method

.method public static final native new_SGGeometryGenerator()J
.end method

.method public static final native new_SGGraphicBufferScreenshotListenerVIBase()J
.end method

.method public static final native new_SGLayer()J
.end method

.method public static final native new_SGLayerAnimationListenerBase()J
.end method

.method public static final native new_SGLayerCanvas()J
.end method

.method public static final native new_SGLayerCanvasRedrawListenerBase()J
.end method

.method public static final native new_SGLayerImage()J
.end method

.method public static final native new_SGLayerText()J
.end method

.method public static final native new_SGScreenshotPropertyListenerVIBase()J
.end method

.method public static final native new_SGSurface()J
.end method

.method public static final native new_SGSurfaceChangesDrawnListenerBase__SWIG_0()J
.end method

.method public static final native new_SGSurfaceChangesDrawnListenerBase__SWIG_1(JLcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListenerBase;)J
.end method

.method public static final native new_SGSurfaceListenerBase()J
.end method

.method private static final native swig_module_init()V
.end method
