.class public abstract Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;
.super Ljava/lang/Object;
.source "SGGeometryGenerator.java"


# instance fields
.field protected swigCMemOwn:Z

.field protected swigCPtr:J


# direct methods
.method protected constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 52
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGGeometryGenerator()J

    move-result-wide v0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;-><init>(JZ)V

    .line 53
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->swigCPtr:J

    iget-boolean v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->swigCMemOwn:Z

    invoke-static {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGGeometryGenerator_director_connect(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;JZZ)V

    .line 54
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->init()V

    .line 55
    return-void
.end method

.method protected constructor <init>(JZ)V
    .locals 5

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->swigCMemOwn:Z

    .line 32
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->swigCPtr:J

    .line 33
    invoke-static {}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->getInstance()Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;

    move-result-object v0

    iget-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->swigCPtr:J

    invoke-virtual {v0, p0, v2, v3}, Lcom/samsung/android/sdk/sgi/base/SGMemoryRegistrator;->Register(Ljava/lang/Object;J)Z

    .line 34
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)J
    .locals 2

    .prologue
    .line 37
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->swigCPtr:J

    goto :goto_0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGGeometryGenerator_init(JLcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V

    .line 59
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 41
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 42
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->swigCMemOwn:Z

    .line 44
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->delete_SGGeometryGenerator(J)V

    .line 46
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;->swigCPtr:J

    .line 48
    :cond_1
    return-void
.end method

.method protected abstract generate(FLcom/samsung/android/sdk/sgi/render/SGGeometry;FF)Lcom/samsung/android/sdk/sgi/render/SGGeometry;
.end method

.method protected abstract isBelongsToGeometry(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Z
.end method
