.class final Lcom/samsung/android/sdk/sgi/vi/SGContext;
.super Ljava/lang/Object;
.source "SGContext.java"


# instance fields
.field protected swigCMemOwn:Z

.field private swigCPtr:J


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 51
    invoke-static {}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGContext__SWIG_0()J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGContext;-><init>(JZ)V

    .line 52
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-boolean p3, p0, Lcom/samsung/android/sdk/sgi/vi/SGContext;->swigCMemOwn:Z

    .line 32
    iput-wide p1, p0, Lcom/samsung/android/sdk/sgi/vi/SGContext;->swigCPtr:J

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;)V
    .locals 3

    .prologue
    .line 79
    invoke-static {p2}, Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;->getCPtr(Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;)J

    move-result-wide v0

    invoke-static {p1, v0, v1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->new_SGContext__SWIG_1(Landroid/content/Context;JLcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGContext;-><init>(JZ)V

    .line 80
    return-void
.end method

.method public static getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGContext;)J
    .locals 2

    .prologue
    .line 36
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContext;->swigCPtr:J

    goto :goto_0
.end method

.method public static initMemoryRegistrator(Lcom/samsung/android/sdk/sgi/base/SGRegistrator;)V
    .locals 2

    .prologue
    .line 63
    invoke-static {p0}, Lcom/samsung/android/sdk/sgi/base/SGRegistrator;->getCPtr(Lcom/samsung/android/sdk/sgi/base/SGRegistrator;)J

    move-result-wide v0

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGContext_initMemoryRegistrator(JLcom/samsung/android/sdk/sgi/base/SGRegistrator;)V

    .line 64
    return-void
.end method


# virtual methods
.method public attachCurrentThread()V
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContext;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGContext_attachCurrentThread(JLcom/samsung/android/sdk/sgi/vi/SGContext;)V

    .line 56
    return-void
.end method

.method public attachToNativeWindow(Lcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/view/Surface;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V
    .locals 8

    .prologue
    .line 67
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContext;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGSurface;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    invoke-static/range {v0 .. v7}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGContext_attachToNativeWindow(JLcom/samsung/android/sdk/sgi/vi/SGContext;JLcom/samsung/android/sdk/sgi/vi/SGSurface;Landroid/view/Surface;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    .line 68
    return-void
.end method

.method public cleanup()V
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContext;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGContext_cleanup(JLcom/samsung/android/sdk/sgi/vi/SGContext;)V

    .line 72
    return-void
.end method

.method public detachCurrentThread()V
    .locals 2

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContext;->swigCPtr:J

    invoke-static {v0, v1, p0}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGContext_detachCurrentThread(JLcom/samsung/android/sdk/sgi/vi/SGContext;)V

    .line 60
    return-void
.end method

.method public detachFromNativeWindow(Lcom/samsung/android/sdk/sgi/vi/SGSurface;)V
    .locals 6

    .prologue
    .line 75
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContext;->swigCPtr:J

    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->getCPtr(Lcom/samsung/android/sdk/sgi/vi/SGSurface;)J

    move-result-wide v3

    move-object v2, p0

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->SGContext_detachFromNativeWindow(JLcom/samsung/android/sdk/sgi/vi/SGContext;JLcom/samsung/android/sdk/sgi/vi/SGSurface;)V

    .line 76
    return-void
.end method

.method public finalize()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 40
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContext;->swigCPtr:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 41
    iget-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContext;->swigCMemOwn:Z

    if-eqz v0, :cond_0

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContext;->swigCMemOwn:Z

    .line 43
    iget-wide v0, p0, Lcom/samsung/android/sdk/sgi/vi/SGContext;->swigCPtr:J

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGJNI;->delete_SGContext(J)V

    .line 45
    :cond_0
    iput-wide v2, p0, Lcom/samsung/android/sdk/sgi/vi/SGContext;->swigCPtr:J

    .line 47
    :cond_1
    return-void
.end method
