.class public Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;
.super Ljava/lang/Object;
.source "SGScrollableAreaScrollPolicy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$1;,
        Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$ParabolicDecelerateTimingFunction;,
        Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$SGVisualValueReceiverScrollPolicy;
    }
.end annotation


# instance fields
.field protected mDuration:I

.field private mScrollableArea:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;",
            ">;"
        }
    .end annotation
.end field

.field private mVisualValueReceiverScrollPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$SGVisualValueReceiverScrollPolicy;

.field private ratioForDuration:F

.field private ratioScrollSpeed:F


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;I)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const v0, 0x3f333333    # 0.7f

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->ratioScrollSpeed:F

    .line 43
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->ratioForDuration:F

    .line 51
    iput p2, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->mDuration:I

    .line 52
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$SGVisualValueReceiverScrollPolicy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$SGVisualValueReceiverScrollPolicy;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$1;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->mVisualValueReceiverScrollPolicy:Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$SGVisualValueReceiverScrollPolicy;

    .line 53
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->mScrollableArea:Ljava/lang/ref/WeakReference;

    .line 60
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->mScrollableArea:Ljava/lang/ref/WeakReference;

    return-object v0
.end method


# virtual methods
.method protected calculateDestinationOnScroll(FF)Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    return-object v0
.end method

.method protected destinationOnFling(FF)Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .locals 7

    .prologue
    const/high16 v6, 0x457a0000    # 4000.0f

    const/high16 v5, 0x40a00000    # 5.0f

    const/high16 v4, 0x40400000    # 3.0f

    const/4 v3, 0x0

    .line 107
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->mScrollableArea:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->getScrolledPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 108
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->mScrollableArea:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->getScrolledPosition()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 109
    new-instance v2, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-direct {v2, v3, v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    .line 111
    cmpl-float v3, p2, v6

    if-lez v3, :cond_0

    .line 112
    div-float v3, p2, v4

    add-float/2addr v0, v3

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setY(F)V

    .line 116
    :goto_0
    cmpl-float v0, p1, v6

    if-lez v0, :cond_1

    .line 117
    div-float v0, p1, v4

    add-float/2addr v0, v1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setX(F)V

    .line 122
    :goto_1
    return-object v2

    .line 114
    :cond_0
    div-float v3, p2, v5

    add-float/2addr v0, v3

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setY(F)V

    goto :goto_0

    .line 119
    :cond_1
    div-float v0, p1, v5

    add-float/2addr v0, v1

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->setX(F)V

    goto :goto_1
.end method

.method getAnimation()Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;
    .locals 2

    .prologue
    .line 80
    new-instance v1, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-direct {v1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;-><init>()V

    .line 81
    iget-object v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->mScrollableArea:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableArea;->getAnimationListener()Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->setAnimationListener(Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;)V

    .line 82
    new-instance v0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$ParabolicDecelerateTimingFunction;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy$ParabolicDecelerateTimingFunction;-><init>(Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;)V

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->setTimingFunction(Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;)V

    .line 83
    iget v0, p0, Lcom/samsung/android/sdk/sgi/widgets/SGScrollableAreaScrollPolicy;->mDuration:I

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->setDuration(I)V

    .line 84
    return-object v1
.end method

.method protected onUp(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 92
    return-void
.end method
