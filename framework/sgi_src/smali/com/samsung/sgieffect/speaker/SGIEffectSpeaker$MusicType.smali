.class public interface abstract Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MusicType;
.super Ljava/lang/Object;
.source "SGIEffectSpeaker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MusicType"
.end annotation


# static fields
.field public static final MUSIC_BASE:I = 0x0

.field public static final MUSIC_COUNT:I = 0x4

.field public static final MUSIC_HIGH:I = 0x3

.field public static final MUSIC_LOW:I = 0x1

.field public static final MUSIC_MID:I = 0x2
