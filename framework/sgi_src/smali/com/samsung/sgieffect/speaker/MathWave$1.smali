.class Lcom/samsung/sgieffect/speaker/MathWave$1;
.super Landroid/os/Handler;
.source "MathWave.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/sgieffect/speaker/MathWave;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/sgieffect/speaker/MathWave;


# direct methods
.method constructor <init>(Lcom/samsung/sgieffect/speaker/MathWave;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/sgieffect/speaker/MathWave$1;->this$0:Lcom/samsung/sgieffect/speaker/MathWave;

    .line 111
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 113
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 121
    :goto_0
    return-void

    .line 115
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$1;->this$0:Lcom/samsung/sgieffect/speaker/MathWave;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/speaker/MathWave;->updateWaves()V

    .line 117
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$1;->this$0:Lcom/samsung/sgieffect/speaker/MathWave;

    # getter for: Lcom/samsung/sgieffect/speaker/MathWave;->mListener:Lcom/samsung/sgieffect/speaker/MathWave$MathWaveListener;
    invoke-static {v0}, Lcom/samsung/sgieffect/speaker/MathWave;->access$1(Lcom/samsung/sgieffect/speaker/MathWave;)Lcom/samsung/sgieffect/speaker/MathWave$MathWaveListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/MathWave$1;->this$0:Lcom/samsung/sgieffect/speaker/MathWave;

    iget-object v1, v1, Lcom/samsung/sgieffect/speaker/MathWave;->mWaves:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/samsung/sgieffect/speaker/MathWave$MathWaveListener;->onDataChanged(Ljava/util/ArrayList;)V

    .line 119
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/MathWave$1;->this$0:Lcom/samsung/sgieffect/speaker/MathWave;

    # getter for: Lcom/samsung/sgieffect/speaker/MathWave;->mDelayedTime:I
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/MathWave;->access$2(Lcom/samsung/sgieffect/speaker/MathWave;)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/sgieffect/speaker/MathWave$1;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 113
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
