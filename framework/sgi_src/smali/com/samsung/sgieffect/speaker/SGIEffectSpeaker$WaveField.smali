.class Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;
.super Ljava/lang/Object;
.source "SGIEffectSpeaker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WaveField"
.end annotation


# instance fields
.field public maxSlidesCount:I

.field public scaleGrid:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;",
            ">;"
        }
    .end annotation
.end field

.field public slideGrid:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;",
            ">;"
        }
    .end annotation
.end field

.field public slideXCount:I

.field public slideYCount:I

.field final synthetic this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;


# direct methods
.method public constructor <init>(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)V
    .locals 1

    .prologue
    .line 226
    iput-object p1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 228
    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->WAVE_COUNT_X:I
    invoke-static {p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$0(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)I

    move-result v0

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideXCount:I

    .line 229
    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->WAVE_COUNT_Y:I
    invoke-static {p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$1(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)I

    move-result v0

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideYCount:I

    .line 230
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideGrid:Ljava/util/ArrayList;

    .line 231
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->scaleGrid:Ljava/util/ArrayList;

    .line 232
    return-void
.end method


# virtual methods
.method public release()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 236
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v2}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v2}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Vector;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v2}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 237
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v2}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v2}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 238
    .local v0, "baseSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    if-eqz v0, :cond_1

    .line 239
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideGrid:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 245
    .end local v0    # "baseSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    :cond_1
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideGrid:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 246
    iput-object v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideGrid:Ljava/util/ArrayList;

    .line 248
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->scaleGrid:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 249
    iput-object v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->scaleGrid:Ljava/util/ArrayList;

    .line 250
    return-void

    .line 239
    .restart local v0    # "baseSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 240
    .local v1, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getParent()Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v3

    if-ne v3, v0, :cond_0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->removeLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    goto :goto_0
.end method
