.class public Lcom/samsung/sgieffect/speaker/MathWaveThread;
.super Ljava/lang/Thread;
.source "MathWaveThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sgieffect/speaker/MathWaveThread$MathWaveListener;,
        Lcom/samsung/sgieffect/speaker/MathWaveThread$Wave;
    }
.end annotation


# instance fields
.field private mListener:Lcom/samsung/sgieffect/speaker/MathWaveThread$MathWaveListener;

.field mMaxRadius:F

.field public mWaves:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sgieffect/speaker/MathWaveThread$Wave;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 7
    const v0, 0x453b8000    # 3000.0f

    iput v0, p0, Lcom/samsung/sgieffect/speaker/MathWaveThread;->mMaxRadius:F

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/MathWaveThread;->mWaves:Ljava/util/ArrayList;

    .line 81
    return-void
.end method


# virtual methods
.method public addWave(FFFFF)V
    .locals 7
    .param p1, "effectiveRadius"    # F
    .param p2, "radius"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "speed"    # F

    .prologue
    .line 94
    new-instance v0, Lcom/samsung/sgieffect/speaker/MathWaveThread$Wave;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/samsung/sgieffect/speaker/MathWaveThread$Wave;-><init>(Lcom/samsung/sgieffect/speaker/MathWaveThread;FFFFF)V

    .line 95
    .local v0, "wave":Lcom/samsung/sgieffect/speaker/MathWaveThread$Wave;
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/MathWaveThread;->mWaves:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 100
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/sgieffect/speaker/MathWaveThread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    return-void

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/sgieffect/speaker/MathWaveThread;->updateWaves()V

    .line 102
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/MathWaveThread;->mListener:Lcom/samsung/sgieffect/speaker/MathWaveThread$MathWaveListener;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/MathWaveThread;->mWaves:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-interface {v1, v0}, Lcom/samsung/sgieffect/speaker/MathWaveThread$MathWaveListener;->onDataChanged(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public setListener(Lcom/samsung/sgieffect/speaker/MathWaveThread$MathWaveListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/sgieffect/speaker/MathWaveThread$MathWaveListener;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/samsung/sgieffect/speaker/MathWaveThread;->mListener:Lcom/samsung/sgieffect/speaker/MathWaveThread$MathWaveListener;

    .line 77
    return-void
.end method

.method public setMaxRangeOfRadius(F)V
    .locals 0
    .param p1, "size"    # F

    .prologue
    .line 11
    iput p1, p0, Lcom/samsung/sgieffect/speaker/MathWaveThread;->mMaxRadius:F

    .line 12
    return-void
.end method

.method public updateWaves()V
    .locals 3

    .prologue
    .line 84
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/MathWaveThread;->mWaves:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "n":I
    :goto_0
    if-gez v0, :cond_0

    .line 91
    return-void

    .line 85
    :cond_0
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/MathWaveThread;->mWaves:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/speaker/MathWaveThread$Wave;

    .line 86
    .local v1, "wave":Lcom/samsung/sgieffect/speaker/MathWaveThread$Wave;
    invoke-virtual {v1}, Lcom/samsung/sgieffect/speaker/MathWaveThread$Wave;->update()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 87
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/MathWaveThread;->mWaves:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 84
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method
