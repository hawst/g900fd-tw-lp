.class Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;
.super Ljava/lang/Object;
.source "CollectColorNode.java"


# instance fields
.field private mKey:I

.field private mValue:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected getKey()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->mKey:I

    return v0
.end method

.method protected getValue()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->mValue:I

    return v0
.end method

.method protected setKey(I)V
    .locals 0
    .param p1, "key"    # I

    .prologue
    .line 8
    iput p1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->mKey:I

    .line 9
    return-void
.end method

.method protected setNode(II)V
    .locals 0
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 14
    iput p1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->mKey:I

    .line 15
    iput p2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->mValue:I

    .line 16
    return-void
.end method

.method protected setValue(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 11
    iput p1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->mValue:I

    .line 12
    return-void
.end method
