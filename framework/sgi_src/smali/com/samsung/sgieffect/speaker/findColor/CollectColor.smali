.class public Lcom/samsung/sgieffect/speaker/findColor/CollectColor;
.super Ljava/lang/Object;
.source "CollectColor.java"


# instance fields
.field private final ColorMask:I

.field private final TargetSize:I

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapSmall:Landroid/graphics/Bitmap;

.field private mDefaultColors:[I

.field private mFindResultIndex:I

.field private mHash:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mHeight:I

.field private mRand:Ljava/util/Random;

.field private mResourceID:I

.field private mResources:Landroid/content/res/Resources;

.field private mResult:[I

.field private mResultCount:I

.field private mSort:Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 5
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    const/16 v4, 0x27

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/16 v1, 0x3c

    iput v1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->TargetSize:I

    .line 15
    const v1, -0x1f1f20

    iput v1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->ColorMask:I

    .line 37
    if-nez p1, :cond_1

    .line 52
    :cond_0
    return-void

    .line 40
    :cond_1
    iput-object p1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mResources:Landroid/content/res/Resources;

    .line 41
    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    iput-object v1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mHash:Ljava/util/Hashtable;

    .line 42
    new-instance v1, Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;

    invoke-direct {v1}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;-><init>()V

    iput-object v1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mSort:Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;

    .line 44
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    iput-object v1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mRand:Ljava/util/Random;

    .line 45
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mRand:Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/Random;->setSeed(J)V

    .line 49
    new-array v1, v4, [I

    iput-object v1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mDefaultColors:[I

    .line 50
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 51
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mDefaultColors:[I

    const/4 v2, 0x0

    aput v2, v1, v0

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private addHash(I)V
    .locals 4
    .param p1, "key"    # I

    .prologue
    .line 147
    const v0, -0x1f1f20

    and-int/2addr p1, v0

    .line 148
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mHash:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mHash:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mHash:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    :goto_0
    return-void

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mHash:Ljava/util/Hashtable;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private copyToArrayForThread(Ljava/util/Hashtable;)[Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)[",
            "Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;"
        }
    .end annotation

    .prologue
    .line 156
    .local p1, "hash":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget v6, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mResultCount:I

    new-array v2, v6, [Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;

    .line 157
    .local v2, "nodes":[Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;
    const/4 v4, 0x0

    .line 159
    .local v4, "nodes_index":I
    const/4 v3, 0x0

    .line 160
    .local v3, "nodesIsFull":Z
    invoke-virtual {p1}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 162
    .local v0, "e":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, v2

    if-lt v1, v6, :cond_1

    .line 165
    :cond_0
    :goto_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-nez v6, :cond_2

    .line 180
    iput v4, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mFindResultIndex:I

    .line 182
    iget-object v6, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mSort:Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;

    invoke-virtual {v6, v2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;->makeTree([Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)V

    .line 183
    const-string v6, "teja.kim"

    const-string v7, "end_albumjacket thread"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    return-object v2

    .line 163
    :cond_1
    new-instance v6, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;

    invoke-direct {v6}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;-><init>()V

    aput-object v6, v2, v1

    .line 162
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 166
    :cond_2
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 168
    .local v5, "temp_key":I
    if-eqz v3, :cond_3

    .line 169
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {p0, v5, v6, v2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->insert(II[Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)Z

    goto :goto_1

    .line 171
    :cond_3
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct {p0, v5, v6, v2, v4}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->insert(II[Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;I)Z

    .line 172
    add-int/lit8 v4, v4, 0x1

    .line 173
    array-length v6, v2

    if-ne v4, v6, :cond_0

    .line 174
    const/4 v3, 0x1

    .line 175
    iget-object v6, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mSort:Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;

    invoke-virtual {v6, v2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;->makeTree([Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)V

    goto :goto_1
.end method

.method private decodeImage()V
    .locals 6

    .prologue
    const/16 v4, 0x3c

    const/4 v5, 0x1

    .line 108
    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v3, :cond_4

    .line 109
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 110
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v5, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 111
    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mResources:Landroid/content/res/Resources;

    iget v4, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mResourceID:I

    invoke-static {v3, v4, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mBitmap:Landroid/graphics/Bitmap;

    .line 112
    iget v3, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-le v3, v4, :cond_1

    iget v0, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 113
    .local v0, "length":I
    :goto_0
    div-int/lit8 v0, v0, 0x3c

    .line 114
    const v2, 0x8000

    .line 115
    .local v2, "sampleSize":I
    :cond_0
    and-int v3, v2, v0

    if-eqz v3, :cond_2

    .line 120
    :goto_1
    if-nez v2, :cond_3

    .line 121
    iput v5, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 124
    :goto_2
    const/4 v3, 0x0

    iput-boolean v3, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 126
    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mResources:Landroid/content/res/Resources;

    iget v4, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mResourceID:I

    invoke-static {v3, v4, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mBitmapSmall:Landroid/graphics/Bitmap;

    .line 132
    .end local v0    # "length":I
    .end local v1    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v2    # "sampleSize":I
    :goto_3
    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mBitmapSmall:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iput v3, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mWidth:I

    .line 133
    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mBitmapSmall:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iput v3, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mHeight:I

    .line 134
    return-void

    .line 112
    .restart local v1    # "options":Landroid/graphics/BitmapFactory$Options;
    :cond_1
    iget v0, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    goto :goto_0

    .line 116
    .restart local v0    # "length":I
    .restart local v2    # "sampleSize":I
    :cond_2
    shr-int/lit8 v2, v2, 0x1

    .line 117
    if-nez v2, :cond_0

    goto :goto_1

    .line 123
    :cond_3
    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    goto :goto_2

    .line 128
    .end local v0    # "length":I
    .end local v1    # "options":Landroid/graphics/BitmapFactory$Options;
    .end local v2    # "sampleSize":I
    :cond_4
    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v3, v4, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mBitmapSmall:Landroid/graphics/Bitmap;

    .line 129
    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_3
.end method

.method private getInt(III)I
    .locals 3
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I

    .prologue
    .line 266
    const/high16 v1, -0x1000000

    shl-int/lit8 v2, p1, 0x10

    or-int/2addr v1, v2

    shl-int/lit8 v2, p2, 0x8

    or-int/2addr v1, v2

    or-int v0, v1, p3

    .line 267
    .local v0, "result":I
    return v0
.end method

.method private getRGB([I)[I
    .locals 6
    .param p1, "color"    # [I

    .prologue
    .line 271
    array-length v4, p1

    mul-int/lit8 v4, v4, 0x3

    new-array v3, v4, [I

    .line 272
    .local v3, "result":[I
    const/4 v1, 0x0

    .line 273
    .local v1, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, p1

    if-lt v0, v4, :cond_0

    .line 278
    return-object v3

    .line 274
    :cond_0
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .local v2, "index":I
    aget v4, p1, v0

    const/high16 v5, 0xff0000

    and-int/2addr v4, v5

    shr-int/lit8 v4, v4, 0x10

    aput v4, v3, v1

    .line 275
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    aget v4, p1, v0

    const v5, 0xff00

    and-int/2addr v4, v5

    shr-int/lit8 v4, v4, 0x8

    aput v4, v3, v2

    .line 276
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    aget v4, p1, v0

    and-int/lit16 v4, v4, 0xff

    aput v4, v3, v1

    .line 273
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    goto :goto_0
.end method

.method private getResult([Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)[I
    .locals 3
    .param p1, "nodes"    # [Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;

    .prologue
    .line 204
    iget v2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mResultCount:I

    new-array v1, v2, [I

    .line 205
    .local v1, "result":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    .line 213
    return-object v1

    .line 206
    :cond_0
    iget v2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mFindResultIndex:I

    if-gt v2, v0, :cond_1

    .line 207
    const/4 v2, 0x0

    aget v2, v1, v2

    invoke-direct {p0, v2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->getSimAverage(I)I

    move-result v2

    aput v2, v1, v0

    .line 205
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 211
    :cond_1
    aget-object v2, p1, v0

    invoke-virtual {v2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->getKey()I

    move-result v2

    aput v2, v1, v0

    goto :goto_1
.end method

.method private getSimAverage(I)I
    .locals 11
    .param p1, "color"    # I

    .prologue
    const/16 v10, 0xff

    const/4 v9, 0x0

    .line 217
    const/16 v4, 0x8

    .line 218
    .local v4, "multi":I
    const/high16 v8, 0xff0000

    and-int/2addr v8, p1

    shr-int/lit8 v5, v8, 0x10

    .line 219
    .local v5, "r":I
    const v8, 0xff00

    and-int/2addr v8, p1

    shr-int/lit8 v3, v8, 0x8

    .line 220
    .local v3, "g":I
    and-int/lit16 v2, p1, 0xff

    .line 223
    .local v2, "b":I
    add-int v8, v5, v3

    add-int/2addr v8, v2

    div-int/lit8 v8, v8, 0x3

    div-int/lit8 v8, v8, 0x2

    div-int/2addr v8, v4

    int-to-float v1, v8

    .line 224
    .local v1, "avrF":F
    float-to-int v0, v1

    .line 226
    .local v0, "average":I
    const/4 v6, 0x0

    .line 227
    .local v6, "result":I
    if-nez v0, :cond_0

    .line 228
    invoke-direct {p0, v9, v9, v9}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->getInt(III)I

    move-result v6

    move v7, v6

    .line 262
    .end local v6    # "result":I
    .local v7, "result":I
    :goto_0
    return v7

    .line 233
    .end local v7    # "result":I
    .restart local v6    # "result":I
    :cond_0
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v8}, Ljava/util/Random;->nextBoolean()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 234
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v8, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v8

    mul-int/2addr v8, v4

    add-int/2addr v5, v8

    .line 238
    :goto_1
    if-le v5, v10, :cond_5

    .line 239
    const/16 v5, 0xff

    .line 243
    :cond_1
    :goto_2
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v8}, Ljava/util/Random;->nextBoolean()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 244
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v8, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v8

    mul-int/2addr v8, v4

    add-int/2addr v3, v8

    .line 247
    :goto_3
    if-le v3, v10, :cond_7

    .line 248
    const/16 v3, 0xff

    .line 252
    :cond_2
    :goto_4
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v8}, Ljava/util/Random;->nextBoolean()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 253
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v8, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v8

    mul-int/2addr v8, v4

    add-int/2addr v2, v8

    .line 256
    :goto_5
    if-le v2, v10, :cond_9

    .line 257
    const/16 v2, 0xff

    .line 261
    :cond_3
    :goto_6
    invoke-direct {p0, v5, v3, v2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->getInt(III)I

    move-result v6

    move v7, v6

    .line 262
    .end local v6    # "result":I
    .restart local v7    # "result":I
    goto :goto_0

    .line 236
    .end local v7    # "result":I
    .restart local v6    # "result":I
    :cond_4
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v8, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v8

    mul-int/2addr v8, v4

    sub-int/2addr v5, v8

    goto :goto_1

    .line 240
    :cond_5
    if-gez v5, :cond_1

    .line 241
    const/4 v5, 0x0

    goto :goto_2

    .line 246
    :cond_6
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v8, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v8

    mul-int/2addr v8, v4

    sub-int/2addr v3, v8

    goto :goto_3

    .line 249
    :cond_7
    if-gez v3, :cond_2

    .line 250
    const/4 v3, 0x0

    goto :goto_4

    .line 255
    :cond_8
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mRand:Ljava/util/Random;

    invoke-virtual {v8, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v8

    mul-int/2addr v8, v4

    sub-int/2addr v2, v8

    goto :goto_5

    .line 258
    :cond_9
    if-gez v2, :cond_3

    .line 259
    const/4 v2, 0x0

    goto :goto_6
.end method

.method private insert(II[Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)Z
    .locals 2
    .param p1, "key"    # I
    .param p2, "value"    # I
    .param p3, "nodes"    # [Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;

    .prologue
    const/4 v0, 0x0

    .line 189
    aget-object v1, p3, v0

    invoke-virtual {v1}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->getValue()I

    move-result v1

    if-ge v1, p2, :cond_0

    .line 190
    aget-object v0, p3, v0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->setNode(II)V

    .line 191
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mSort:Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;

    invoke-virtual {v0, p3}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;->checkTree([Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)V

    .line 192
    const/4 v0, 0x1

    .line 194
    :cond_0
    return v0
.end method

.method private insert(II[Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;I)Z
    .locals 1
    .param p1, "key"    # I
    .param p2, "value"    # I
    .param p3, "nodes"    # [Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;
    .param p4, "nodes_count"    # I

    .prologue
    .line 198
    aget-object v0, p3, p4

    invoke-virtual {v0, p1}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->setKey(I)V

    .line 199
    aget-object v0, p3, p4

    invoke-virtual {v0, p2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->setValue(I)V

    .line 200
    const/4 v0, 0x1

    return v0
.end method

.method private runDetail()V
    .locals 4

    .prologue
    .line 138
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mHeight:I

    if-lt v0, v3, :cond_0

    .line 144
    return-void

    .line 139
    :cond_0
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v3, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mWidth:I

    if-lt v1, v3, :cond_1

    .line 138
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_1
    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mBitmapSmall:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v1, v0}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    .line 141
    .local v2, "key":I
    invoke-direct {p0, v2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->addHash(I)V

    .line 139
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public setDefaultColor([I)V
    .locals 3
    .param p1, "colors"    # [I

    .prologue
    .line 76
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_0

    .line 81
    return-void

    .line 78
    :cond_0
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mDefaultColors:[I

    aget v2, p1, v0

    aput v2, v1, v0

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setImage(II)[I
    .locals 3
    .param p1, "resoureID"    # I
    .param p2, "resultCount"    # I

    .prologue
    .line 55
    const/4 v2, 0x1

    if-ge p2, v2, :cond_0

    .line 56
    const/4 v2, 0x0

    .line 70
    :goto_0
    return-object v2

    .line 58
    :cond_0
    const/4 v2, 0x0

    iput v2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mFindResultIndex:I

    .line 59
    iput p1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mResourceID:I

    .line 60
    iput p2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mResultCount:I

    .line 61
    invoke-direct {p0}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->decodeImage()V

    .line 62
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mHash:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->clear()V

    .line 63
    invoke-direct {p0}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->runDetail()V

    .line 65
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mHash:Ljava/util/Hashtable;

    invoke-direct {p0, v2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->copyToArrayForThread(Ljava/util/Hashtable;)[Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;

    move-result-object v1

    .line 66
    .local v1, "nodes":[Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mSort:Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;

    invoke-virtual {v2, v1}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;->sort([Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)V

    .line 67
    invoke-direct {p0, v1}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->getResult([Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)[I

    move-result-object v0

    .line 68
    .local v0, "colors":[I
    invoke-direct {p0, v0}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->getRGB([I)[I

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mResult:[I

    .line 70
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mResult:[I

    goto :goto_0
.end method

.method public setImage(Landroid/graphics/Bitmap;I)[I
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "resultCount"    # I

    .prologue
    .line 84
    const/4 v2, 0x1

    if-ge p2, v2, :cond_0

    .line 85
    const/4 v2, 0x0

    .line 104
    :goto_0
    return-object v2

    .line 88
    :cond_0
    if-nez p1, :cond_1

    .line 89
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mDefaultColors:[I

    goto :goto_0

    .line 91
    :cond_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mFindResultIndex:I

    .line 92
    iput-object p1, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mBitmap:Landroid/graphics/Bitmap;

    .line 93
    iput p2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mResultCount:I

    .line 94
    invoke-direct {p0}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->decodeImage()V

    .line 95
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mHash:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->clear()V

    .line 96
    invoke-direct {p0}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->runDetail()V

    .line 97
    const-string v2, "teja.kim"

    const-string v3, "start_albumjacket thread"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mHash:Ljava/util/Hashtable;

    invoke-direct {p0, v2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->copyToArrayForThread(Ljava/util/Hashtable;)[Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;

    move-result-object v1

    .line 100
    .local v1, "nodes":[Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mSort:Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;

    invoke-virtual {v2, v1}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;->sort([Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)V

    .line 101
    invoke-direct {p0, v1}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->getResult([Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)[I

    move-result-object v0

    .line 102
    .local v0, "colors":[I
    invoke-direct {p0, v0}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->getRGB([I)[I

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mResult:[I

    .line 104
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->mResult:[I

    goto :goto_0
.end method
