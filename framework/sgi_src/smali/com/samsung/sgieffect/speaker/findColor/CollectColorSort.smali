.class Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;
.super Ljava/lang/Object;
.source "CollectColorSort.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected checkTree([Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)V
    .locals 2
    .param p1, "nodes"    # [Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;

    .prologue
    .line 59
    const/4 v0, 0x0

    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;->siftDown([Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;II)Z

    .line 60
    return-void
.end method

.method protected makeTree([Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)V
    .locals 2
    .param p1, "nodes"    # [Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;

    .prologue
    .line 14
    array-length v1, p1

    add-int/lit8 v1, v1, -0x2

    div-int/lit8 v0, v1, 0x2

    .line 15
    .local v0, "start":I
    :goto_0
    if-gez v0, :cond_0

    .line 19
    return-void

    .line 16
    :cond_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;->siftDown([Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;II)Z

    .line 17
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method protected siftDown([Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;II)Z
    .locals 5
    .param p1, "array"    # [Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 22
    move v1, p2

    .line 26
    .local v1, "root":I
    :goto_0
    mul-int/lit8 v3, v1, 0x2

    add-int/lit8 v3, v3, 0x1

    if-le v3, p3, :cond_0

    .line 43
    const/4 v3, 0x0

    :goto_1
    return v3

    .line 27
    :cond_0
    mul-int/lit8 v3, v1, 0x2

    add-int/lit8 v0, v3, 0x1

    .line 28
    .local v0, "child":I
    move v2, v1

    .line 30
    .local v2, "swap":I
    aget-object v3, p1, v2

    invoke-virtual {v3}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->getValue()I

    move-result v3

    aget-object v4, p1, v0

    invoke-virtual {v4}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->getValue()I

    move-result v4

    if-le v3, v4, :cond_1

    .line 31
    move v2, v0

    .line 33
    :cond_1
    add-int/lit8 v3, v0, 0x1

    if-gt v3, p3, :cond_2

    aget-object v3, p1, v2

    invoke-virtual {v3}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->getValue()I

    move-result v3

    add-int/lit8 v4, v0, 0x1

    aget-object v4, p1, v4

    invoke-virtual {v4}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->getValue()I

    move-result v4

    if-le v3, v4, :cond_2

    .line 34
    add-int/lit8 v2, v0, 0x1

    .line 36
    :cond_2
    if-eq v2, v1, :cond_3

    .line 37
    aget-object v3, p1, v2

    aget-object v4, p1, v1

    invoke-virtual {p0, v3, v4}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;->swap(Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)V

    .line 38
    move v1, v2

    goto :goto_0

    .line 41
    :cond_3
    const/4 v3, 0x1

    goto :goto_1
.end method

.method protected sort([Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)V
    .locals 4
    .param p1, "nodes"    # [Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;

    .prologue
    const/4 v3, 0x0

    .line 5
    array-length v1, p1

    add-int/lit8 v0, v1, -0x1

    .line 6
    .local v0, "end":I
    :goto_0
    if-gtz v0, :cond_0

    .line 11
    return-void

    .line 7
    :cond_0
    aget-object v1, p1, v3

    aget-object v2, p1, v0

    invoke-virtual {p0, v1, v2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;->swap(Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)V

    .line 8
    add-int/lit8 v0, v0, -0x1

    .line 9
    invoke-virtual {p0, p1, v3, v0}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorSort;->siftDown([Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;II)Z

    goto :goto_0
.end method

.method protected swap(Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;)V
    .locals 2
    .param p1, "a1"    # Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;
    .param p2, "a2"    # Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;

    .prologue
    .line 49
    invoke-virtual {p1}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->getKey()I

    move-result v0

    .line 50
    .local v0, "temp":I
    invoke-virtual {p2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->getKey()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->setKey(I)V

    .line 51
    invoke-virtual {p2, v0}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->setKey(I)V

    .line 53
    invoke-virtual {p1}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->getValue()I

    move-result v0

    .line 54
    invoke-virtual {p2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->getValue()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->setValue(I)V

    .line 55
    invoke-virtual {p2, v0}, Lcom/samsung/sgieffect/speaker/findColor/CollectColorNode;->setValue(I)V

    .line 56
    return-void
.end method
