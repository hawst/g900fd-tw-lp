.class public Lcom/samsung/sgieffect/speaker/MathWave;
.super Ljava/lang/Object;
.source "MathWave.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sgieffect/speaker/MathWave$MathWaveListener;,
        Lcom/samsung/sgieffect/speaker/MathWave$Wave;
    }
.end annotation


# static fields
.field private static final MSG_UPDATE:I = 0x1


# instance fields
.field private mDelayedTime:I

.field private mListener:Lcom/samsung/sgieffect/speaker/MathWave$MathWaveListener;

.field private mMaxRadius:F

.field private mUpdateHandler:Landroid/os/Handler;

.field public mWaves:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sgieffect/speaker/MathWave$Wave;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mWaves:Ljava/util/ArrayList;

    .line 142
    const v0, 0x453b8000    # 3000.0f

    iput v0, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mMaxRadius:F

    .line 144
    const/16 v0, 0x37

    iput v0, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mDelayedTime:I

    .line 111
    new-instance v0, Lcom/samsung/sgieffect/speaker/MathWave$1;

    invoke-direct {v0, p0}, Lcom/samsung/sgieffect/speaker/MathWave$1;-><init>(Lcom/samsung/sgieffect/speaker/MathWave;)V

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mUpdateHandler:Landroid/os/Handler;

    .line 123
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/sgieffect/speaker/MathWave;)F
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mMaxRadius:F

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/sgieffect/speaker/MathWave;)Lcom/samsung/sgieffect/speaker/MathWave$MathWaveListener;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mListener:Lcom/samsung/sgieffect/speaker/MathWave$MathWaveListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/sgieffect/speaker/MathWave;)I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mDelayedTime:I

    return v0
.end method


# virtual methods
.method public addWave(FFFFFZ)V
    .locals 8
    .param p1, "effectiveRadius"    # F
    .param p2, "radius"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "speed"    # F
    .param p6, "isHigh"    # Z

    .prologue
    .line 136
    new-instance v0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/samsung/sgieffect/speaker/MathWave$Wave;-><init>(Lcom/samsung/sgieffect/speaker/MathWave;FFFFFZ)V

    .line 137
    .local v0, "wave":Lcom/samsung/sgieffect/speaker/MathWave$Wave;
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mWaves:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    return-void
.end method

.method public setListener(Lcom/samsung/sgieffect/speaker/MathWave$MathWaveListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/sgieffect/speaker/MathWave$MathWaveListener;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mListener:Lcom/samsung/sgieffect/speaker/MathWave$MathWaveListener;

    .line 108
    return-void
.end method

.method public setMaxFps(I)V
    .locals 1
    .param p1, "fps"    # I

    .prologue
    .line 19
    const/16 v0, 0x3e8

    div-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mDelayedTime:I

    .line 20
    return-void
.end method

.method public setMaxRangeOfRadius(F)V
    .locals 0
    .param p1, "size"    # F

    .prologue
    .line 15
    iput p1, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mMaxRadius:F

    .line 16
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mUpdateHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 25
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mUpdateHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 29
    return-void
.end method

.method public updateWaves()V
    .locals 3

    .prologue
    .line 126
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mWaves:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "n":I
    :goto_0
    if-gez v0, :cond_0

    .line 133
    return-void

    .line 127
    :cond_0
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mWaves:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/speaker/MathWave$Wave;

    .line 128
    .local v1, "wave":Lcom/samsung/sgieffect/speaker/MathWave$Wave;
    invoke-virtual {v1}, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->update()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 129
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/MathWave;->mWaves:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 126
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method
