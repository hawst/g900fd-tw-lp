.class public Lcom/samsung/sgieffect/speaker/MathWave$Wave;
.super Ljava/lang/Object;
.source "MathWave.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sgieffect/speaker/MathWave;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Wave"
.end annotation


# instance fields
.field private mCenterX:F

.field private mCenterY:F

.field private mCurrentRadius:F

.field private mEffectiveRadius:F

.field private mInnerDist:F

.field private mIsHigh:Z

.field private mOuterDist:F

.field private mSpeed:F

.field final synthetic this$0:Lcom/samsung/sgieffect/speaker/MathWave;


# direct methods
.method public constructor <init>(Lcom/samsung/sgieffect/speaker/MathWave;FFFFFZ)V
    .locals 2
    .param p2, "effectiveRadius"    # F
    .param p3, "radius"    # F
    .param p4, "x"    # F
    .param p5, "y"    # F
    .param p6, "speed"    # F
    .param p7, "isHigh"    # Z

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->this$0:Lcom/samsung/sgieffect/speaker/MathWave;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput p3, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCurrentRadius:F

    .line 35
    iput p4, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCenterX:F

    .line 36
    iput p5, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCenterY:F

    .line 37
    iput p2, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mEffectiveRadius:F

    .line 38
    iget v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mEffectiveRadius:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mEffectiveRadius:F

    .line 39
    :cond_0
    iput p6, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mSpeed:F

    .line 40
    iput-boolean p7, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mIsHigh:Z

    .line 41
    return-void
.end method

.method public constructor <init>(Lcom/samsung/sgieffect/speaker/MathWave;Lcom/samsung/sgieffect/speaker/MathWave$Wave;)V
    .locals 1
    .param p2, "wave"    # Lcom/samsung/sgieffect/speaker/MathWave$Wave;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->this$0:Lcom/samsung/sgieffect/speaker/MathWave;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iget v0, p2, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCurrentRadius:F

    iput v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCurrentRadius:F

    .line 45
    iget v0, p2, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCenterX:F

    iput v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCenterX:F

    .line 46
    iget v0, p2, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCenterY:F

    iput v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCenterY:F

    .line 47
    iget v0, p2, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mSpeed:F

    iput v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mSpeed:F

    .line 48
    iget v0, p2, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mEffectiveRadius:F

    iput v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mEffectiveRadius:F

    .line 49
    iget-boolean v0, p2, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mIsHigh:Z

    iput-boolean v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mIsHigh:Z

    .line 50
    return-void
.end method


# virtual methods
.method public getScale(FF)F
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v5, 0x0

    .line 66
    iget v6, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCenterX:F

    sub-float v0, p1, v6

    .line 67
    .local v0, "distX":F
    iget v6, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCenterY:F

    sub-float v2, p2, v6

    .line 68
    .local v2, "distY":F
    mul-float v6, v0, v0

    mul-float v7, v2, v2

    add-float v1, v6, v7

    .line 69
    .local v1, "distXY":F
    iget v6, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mOuterDist:F

    cmpl-float v6, v1, v6

    if-lez v6, :cond_1

    .line 75
    :cond_0
    :goto_0
    return v5

    .line 70
    :cond_1
    iget v6, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mInnerDist:F

    cmpg-float v6, v1, v6

    if-ltz v6, :cond_0

    .line 72
    float-to-double v6, v1

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-float v3, v6

    .line 74
    .local v3, "distance":F
    iget v5, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCurrentRadius:F

    iget v6, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mEffectiveRadius:F

    const/high16 v7, 0x3f000000    # 0.5f

    mul-float/2addr v6, v7

    sub-float/2addr v5, v6

    sub-float v5, v3, v5

    iget v6, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mEffectiveRadius:F

    div-float v4, v5, v6

    .line 75
    .local v4, "sinvalue":F
    const v5, 0x40490fd8

    mul-float/2addr v5, v4

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    double-to-float v5, v6

    goto :goto_0
.end method

.method public isHigh()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mIsHigh:Z

    return v0
.end method

.method public update()Z
    .locals 5

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 54
    iget v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCurrentRadius:F

    iget v1, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mSpeed:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCurrentRadius:F

    .line 55
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->this$0:Lcom/samsung/sgieffect/speaker/MathWave;

    # getter for: Lcom/samsung/sgieffect/speaker/MathWave;->mMaxRadius:F
    invoke-static {v0}, Lcom/samsung/sgieffect/speaker/MathWave;->access$0(Lcom/samsung/sgieffect/speaker/MathWave;)F

    move-result v0

    iget v1, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCurrentRadius:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    .line 60
    :goto_0
    return v0

    .line 58
    :cond_0
    iget v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCurrentRadius:F

    iget v1, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mEffectiveRadius:F

    mul-float/2addr v1, v4

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mOuterDist:F

    .line 59
    iget v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mCurrentRadius:F

    iget v1, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mEffectiveRadius:F

    mul-float/2addr v1, v4

    sub-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->mInnerDist:F

    .line 60
    const/4 v0, 0x0

    goto :goto_0
.end method
