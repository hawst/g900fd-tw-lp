.class Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;
.super Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;
.source "SGIEffectSpeaker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SGIEffectSpeakerAnimCallback"
.end annotation


# instance fields
.field mSensorX:F

.field mSensorY:F

.field final synthetic this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

.field x:F

.field y:F

.field z:F


# direct methods
.method public constructor <init>(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)V
    .locals 1

    .prologue
    const/high16 v0, 0x41200000    # 10.0f

    .line 1874
    iput-object p1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-direct {p0}, Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;-><init>()V

    .line 2001
    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->mSensorX:F

    .line 2002
    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->mSensorY:F

    .line 1874
    return-void
.end method

.method private initNodeDJSkin(Lcom/samsung/sgieffect/main/SGIEffectNode;)V
    .locals 2
    .param p1, "node"    # Lcom/samsung/sgieffect/main/SGIEffectNode;

    .prologue
    .line 1897
    invoke-virtual {p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 1898
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1899
    return-void
.end method

.method private initNodeDefaultSkin(Lcom/samsung/sgieffect/main/SGIEffectNode;)V
    .locals 4
    .param p1, "node"    # Lcom/samsung/sgieffect/main/SGIEffectNode;

    .prologue
    const/16 v2, 0xff

    const/high16 v3, 0x3f800000    # 1.0f

    .line 1889
    invoke-virtual {p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 1890
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1891
    const/4 v1, 0x0

    invoke-static {v1, v2, v2, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    move-object v1, v0

    .line 1892
    check-cast v1, Lcom/samsung/sgieffect/main/SGISlideEmulation;

    const/high16 v2, 0x42800000    # 64.0f

    invoke-virtual {v1, v3, v3, v3, v2}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightColor(FFFF)V

    .line 1893
    check-cast v0, Lcom/samsung/sgieffect/main/SGISlideEmulation;

    .end local v0    # "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightPower(F)V

    .line 1894
    return-void
.end method

.method private initPannelNodeDJSkin(Lcom/samsung/sgieffect/main/SGIEffectNode;)V
    .locals 7
    .param p1, "node"    # Lcom/samsung/sgieffect/main/SGIEffectNode;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x7

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1954
    invoke-virtual {p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 1956
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-le v1, v2, :cond_0

    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 1957
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1960
    :cond_0
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-le v1, v4, :cond_1

    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_1

    .line 1961
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1964
    :cond_1
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-le v1, v5, :cond_2

    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_2

    .line 1965
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1968
    :cond_2
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-le v1, v6, :cond_3

    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_3

    .line 1969
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1972
    :cond_3
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    const/16 v2, 0xa

    if-le v1, v2, :cond_4

    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_4

    .line 1973
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1976
    :cond_4
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    const/16 v2, 0xc

    if-le v1, v2, :cond_5

    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_5

    .line 1977
    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1979
    :cond_5
    return-void
.end method

.method private initPannelNodeDefaultSkin(Lcom/samsung/sgieffect/main/SGIEffectNode;)V
    .locals 10
    .param p1, "node"    # Lcom/samsung/sgieffect/main/SGIEffectNode;

    .prologue
    const v9, 0x3f4ccccd    # 0.8f

    const v8, 0x3a83126f    # 0.001f

    const/4 v7, 0x0

    const/16 v6, 0x99

    const/high16 v5, 0x3f800000    # 1.0f

    .line 1914
    invoke-virtual {p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    check-cast v0, Lcom/samsung/sgieffect/main/SGISlideEmulation;

    .line 1917
    .local v0, "slide":Lcom/samsung/sgieffect/main/SGISlideEmulation;
    invoke-virtual {v0, v5}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setCustomLightPower(F)V

    .line 1918
    const v1, -0x42333333    # -0.1f

    const/high16 v2, 0x41300000    # 11.0f

    invoke-virtual {v0, v8, v1, v9, v2}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightColor(FFFF)V

    .line 1919
    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v7, v1, v5, v5}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setViewDirection(FFFF)V

    .line 1920
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    const/4 v2, 0x4

    if-le v1, v2, :cond_4

    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_4

    .line 1921
    invoke-virtual {v0, v5}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setCustomLightPower(F)V

    .line 1922
    const v1, 0x411ccccd    # 9.8f

    invoke-virtual {v0, v8, v7, v9, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightColor(FFFF)V

    .line 1923
    const v1, 0x3f0ccccd    # 0.55f

    invoke-virtual {v0, v7, v1, v5, v5}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setViewDirection(FFFF)V

    .line 1924
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setColor(I)V

    .line 1929
    :goto_0
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_0

    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 1930
    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setCustomLightPower(F)V

    .line 1931
    const v1, -0x42333333    # -0.1f

    const/high16 v2, 0x41280000    # 10.5f

    invoke-virtual {v0, v8, v1, v9, v2}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightColor(FFFF)V

    .line 1932
    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v7, v1, v5, v5}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setViewDirection(FFFF)V

    .line 1933
    const/16 v1, 0xff

    invoke-static {v1, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setColor(I)V

    .line 1936
    :cond_0
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_1

    .line 1937
    invoke-virtual {v0, v5}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setCustomLightPower(F)V

    .line 1938
    const v1, -0x42333333    # -0.1f

    const/high16 v2, 0x41280000    # 10.5f

    invoke-virtual {v0, v8, v1, v9, v2}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightColor(FFFF)V

    .line 1939
    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v7, v1, v5, v5}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setViewDirection(FFFF)V

    .line 1940
    const/16 v1, 0xff

    invoke-static {v1, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setColor(I)V

    .line 1943
    :cond_1
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    const/4 v2, 0x3

    if-le v1, v2, :cond_2

    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_2

    .line 1944
    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setCustomLightPower(F)V

    .line 1945
    const v1, -0x42333333    # -0.1f

    const/high16 v2, 0x41300000    # 11.0f

    invoke-virtual {v0, v8, v1, v9, v2}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightColor(FFFF)V

    .line 1948
    :cond_2
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_3

    .line 1949
    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setCustomLightPower(F)V

    .line 1951
    :cond_3
    return-void

    .line 1926
    :cond_4
    const/16 v1, 0xff

    const/16 v2, 0x66

    const/16 v3, 0x66

    const/16 v4, 0x66

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setColor(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public callbackClickAnimation(Lcom/samsung/sgieffect/main/SGIEffectNode;Landroid/view/MotionEvent;I)Z
    .locals 1
    .param p1, "node"    # Lcom/samsung/sgieffect/main/SGIEffectNode;
    .param p2, "event"    # Landroid/view/MotionEvent;
    .param p3, "index"    # I

    .prologue
    .line 1988
    const/4 v0, 0x0

    return v0
.end method

.method public callbackExtraTask(Lcom/samsung/sgieffect/main/SGIEffectNode;Landroid/view/MotionEvent;I)I
    .locals 1
    .param p1, "node"    # Lcom/samsung/sgieffect/main/SGIEffectNode;
    .param p2, "event"    # Landroid/view/MotionEvent;
    .param p3, "index"    # I

    .prologue
    .line 1983
    const/4 v0, 0x0

    return v0
.end method

.method public callbackHoveringAnimation(Lcom/samsung/sgieffect/main/SGIEffectNode;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "node"    # Lcom/samsung/sgieffect/main/SGIEffectNode;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1998
    const/4 v0, 0x0

    return v0
.end method

.method public callbackInitialize(Lcom/samsung/sgieffect/main/SGIEffectNode;)Z
    .locals 2
    .param p1, "node"    # Lcom/samsung/sgieffect/main/SGIEffectNode;

    .prologue
    .line 1878
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I
    invoke-static {v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$3(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)I

    move-result v0

    if-nez v0, :cond_1

    .line 1879
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->initNodeDefaultSkin(Lcom/samsung/sgieffect/main/SGIEffectNode;)V

    .line 1885
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 1881
    :cond_1
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I
    invoke-static {v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$3(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1882
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->initNodeDJSkin(Lcom/samsung/sgieffect/main/SGIEffectNode;)V

    goto :goto_0
.end method

.method public callbackLongPressAnimation(Lcom/samsung/sgieffect/main/SGIEffectNode;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "node"    # Lcom/samsung/sgieffect/main/SGIEffectNode;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1993
    const/4 v0, 0x0

    return v0
.end method

.method public callbackPannelInitialize(Lcom/samsung/sgieffect/main/SGIEffectNode;)Z
    .locals 2
    .param p1, "node"    # Lcom/samsung/sgieffect/main/SGIEffectNode;

    .prologue
    .line 1903
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I
    invoke-static {v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$3(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)I

    move-result v0

    if-nez v0, :cond_1

    .line 1904
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->initPannelNodeDefaultSkin(Lcom/samsung/sgieffect/main/SGIEffectNode;)V

    .line 1910
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 1906
    :cond_1
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I
    invoke-static {v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$3(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1907
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->initPannelNodeDJSkin(Lcom/samsung/sgieffect/main/SGIEffectNode;)V

    goto :goto_0
.end method

.method public callbackSensor(Lcom/samsung/sgieffect/main/SGIEffectNode;[FI)Z
    .locals 11
    .param p1, "node"    # Lcom/samsung/sgieffect/main/SGIEffectNode;
    .param p2, "value"    # [F
    .param p3, "type"    # I

    .prologue
    const v10, 0x40f66667    # 7.7000003f

    const v9, 0x409ccccd    # 4.9f

    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v6, 0x40600000    # 3.5f

    .line 2028
    iget-object v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I
    invoke-static {v4}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$3(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)I

    move-result v4

    if-eqz v4, :cond_0

    .line 2077
    :goto_0
    return v8

    .line 2031
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v3

    check-cast v3, Lcom/samsung/sgieffect/main/SGISlideEmulation;

    .line 2032
    .local v3, "slide":Lcom/samsung/sgieffect/main/SGISlideEmulation;
    aget v4, p2, v8

    iput v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->x:F

    .line 2033
    const/4 v4, 0x1

    aget v4, p2, v4

    iput v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    .line 2034
    const/4 v4, 0x2

    aget v4, p2, v4

    iput v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    .line 2036
    iget-object v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v4}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 2037
    .local v1, "rubberinnode":Lcom/samsung/sgieffect/main/SGIEffectNode;
    if-ne p1, v1, :cond_3

    .line 2038
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    cmpg-float v4, v4, v6

    if-gez v4, :cond_1

    iput v6, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    .line 2039
    :cond_1
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    cmpg-float v4, v4, v6

    if-gez v4, :cond_2

    iput v6, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    .line 2041
    :cond_2
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->x:F

    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    iget v6, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightDirection(FFFF)V

    .line 2042
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->x:F

    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    iget v6, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setViewDirection(FFFF)V

    goto :goto_0

    .line 2047
    :cond_3
    iget-object v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v4}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 2048
    .local v2, "rubberoutnode":Lcom/samsung/sgieffect/main/SGIEffectNode;
    if-ne p1, v2, :cond_6

    .line 2049
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    cmpg-float v4, v4, v6

    if-gez v4, :cond_4

    iput v6, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    .line 2050
    :cond_4
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    cmpg-float v4, v4, v6

    if-gez v4, :cond_5

    iput v6, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    .line 2052
    :cond_5
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->x:F

    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    iget v6, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightDirection(FFFF)V

    .line 2053
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->x:F

    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    iget v6, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setViewDirection(FFFF)V

    goto :goto_0

    .line 2058
    :cond_6
    iget-object v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v4}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    const/4 v5, 0x4

    if-le v4, v5, :cond_9

    .line 2059
    iget-object v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;
    invoke-static {v4}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 2060
    .local v0, "lastnode":Lcom/samsung/sgieffect/main/SGIEffectNode;
    if-ne p1, v0, :cond_9

    .line 2061
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    cmpg-float v4, v4, v9

    if-gez v4, :cond_7

    iput v9, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    .line 2062
    :cond_7
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    cmpg-float v4, v4, v9

    if-gez v4, :cond_8

    iput v9, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    .line 2064
    :cond_8
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->x:F

    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    iget v6, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightDirection(FFFF)V

    .line 2065
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->x:F

    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    iget v6, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setViewDirection(FFFF)V

    goto/16 :goto_0

    .line 2072
    .end local v0    # "lastnode":Lcom/samsung/sgieffect/main/SGIEffectNode;
    :cond_9
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    cmpg-float v4, v4, v10

    if-gez v4, :cond_a

    iput v10, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    .line 2073
    :cond_a
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    cmpg-float v4, v4, v10

    if-gez v4, :cond_b

    iput v10, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    .line 2075
    :cond_b
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->x:F

    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->y:F

    iget v6, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->z:F

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightDirection(FFFF)V

    goto/16 :goto_0
.end method

.method public callbackSensor(Lcom/samsung/sgieffect/main/SGISlideEmulation;[FI)Z
    .locals 6
    .param p1, "slide"    # Lcom/samsung/sgieffect/main/SGISlideEmulation;
    .param p2, "value"    # [F
    .param p3, "type"    # I

    .prologue
    const v5, 0x409ccccd    # 4.9f

    const/4 v4, 0x0

    .line 2009
    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;->this$0:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    # getter for: Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I
    invoke-static {v3}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->access$3(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)I

    move-result v3

    if-eqz v3, :cond_0

    .line 2021
    :goto_0
    return v4

    .line 2012
    :cond_0
    aget v0, p2, v4

    .line 2013
    .local v0, "x":F
    const/4 v3, 0x1

    aget v1, p2, v3

    .line 2014
    .local v1, "y":F
    const/4 v3, 0x2

    aget v2, p2, v3

    .line 2016
    .local v2, "z":F
    cmpg-float v3, v1, v5

    if-gez v3, :cond_1

    const v1, 0x409ccccd    # 4.9f

    .line 2017
    :cond_1
    cmpg-float v3, v2, v5

    if-gez v3, :cond_2

    const v2, 0x409ccccd    # 4.9f

    .line 2019
    :cond_2
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightDirection(FFFF)V

    goto :goto_0
.end method
