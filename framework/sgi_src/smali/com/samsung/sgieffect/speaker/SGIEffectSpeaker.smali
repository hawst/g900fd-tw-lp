.class public Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;
.super Lcom/samsung/sgieffect/main/SGIEffectView;
.source "SGIEffectSpeaker.java"

# interfaces
.implements Lcom/samsung/sgieffect/speaker/MathWave$MathWaveListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$DJSkinIndex;,
        Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$DefaultSkinIndex;,
        Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;,
        Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskType;,
        Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MusicType;,
        Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;,
        Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SkinType;,
        Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;,
        Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;,
        Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;
    }
.end annotation


# static fields
.field private static final RECOVER_ANIMATION_DURATION:I = 0x19

.field private static final SENSOR_SCALE:F = 0.7f

.field private static final SENSOR_SCALE_CENTER:F = 1.1f

.field private static final SENSOR_SCALE_RUBBER:F = 0.5f


# instance fields
.field private final FLASH_ANIMATION_DURATION:I

.field private final FPS_LIMIT:I

.field private final FUNKEY_BACK_COLOR:I

.field private final OTHERS_BOTTOM_FX_SCALE:F

.field private WAVE_COUNT_X:I

.field private WAVE_COUNT_Y:I

.field private mAnimationCallback:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;

.field private mAnimationDuration:I

.field private mAnimationSpeed:F

.field private mAtlas:Landroid/graphics/Bitmap;

.field private mChangedMaskType:Z

.field private mClassicAnimationSet:Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;

.field private mClassicLightPowerAnimation:Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;

.field private mClassicScaleAnimation:Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;

.field private mClassicScaleVectorEnd:Lcom/samsung/android/sdk/sgi/base/SGVector3f;

.field private mClassicScaleVectorStart:Lcom/samsung/android/sdk/sgi/base/SGVector3f;

.field private mCurrentSkin:I

.field private mEffectScale:F

.field private mImages:[Landroid/graphics/Bitmap;

.field private mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

.field private mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

.field private mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

.field private mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

.field private mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

.field private mIsAddBlend:Z

.field private mLastFrequencyDepth:F

.field private mMaskType:I

.field mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

.field private mMusicBase:F

.field private mMusicHigh:F

.field private mMusicLow:F

.field private mMusicMid:F

.field private mNextFrequencyDepth:F

.field private mOtherFirstRotateZ:F

.field private mOtherFourthRotateZ:F

.field private mOtherSecondRotateZ:F

.field private mOtherThirdRotateZ:F

.field private mRandom:Ljava/util/Random;

.field private mRotateZ:F

.field private mTexCoordData:[Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;

.field public mUseDepthAnimation:Z

.field mWaveColors:[F

.field mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

.field private mWaveImagesCount:I

.field mWaveOrientLandscape:Z

.field mWaveOrientPortraits:Z

.field public maxSlidesCount:I

.field private particleFitForSquare:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 269
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/main/SGIEffectView;-><init>(Landroid/content/Context;)V

    .line 45
    iput-boolean v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mIsAddBlend:Z

    .line 113
    iput-object v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 114
    iput-object v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 115
    iput-object v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 116
    iput-object v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 117
    iput-object v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 119
    iput v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    .line 121
    iput v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMaskType:I

    .line 123
    iput-boolean v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mChangedMaskType:Z

    .line 129
    const v0, 0x40333333    # 2.8f

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->OTHERS_BOTTOM_FX_SCALE:F

    .line 161
    const/16 v0, 0x15e

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->FLASH_ANIMATION_DURATION:I

    .line 162
    const/16 v0, 0x12

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->FPS_LIMIT:I

    .line 163
    const v0, -0xf6f6f7

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->FUNKEY_BACK_COLOR:I

    .line 165
    iput v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicBase:F

    .line 166
    iput v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicLow:F

    .line 167
    iput v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicMid:F

    .line 168
    iput v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicHigh:F

    .line 172
    const/16 v0, 0x19

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAnimationDuration:I

    .line 174
    iput v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mRotateZ:F

    .line 176
    iput v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mOtherFirstRotateZ:F

    .line 177
    iput v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mOtherSecondRotateZ:F

    .line 178
    iput v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mOtherThirdRotateZ:F

    .line 179
    iput v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mOtherFourthRotateZ:F

    .line 181
    const v0, 0x3f666666    # 0.9f

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mEffectScale:F

    .line 182
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAnimationSpeed:F

    .line 184
    iput v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mLastFrequencyDepth:F

    .line 185
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mNextFrequencyDepth:F

    .line 187
    iput-boolean v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mUseDepthAnimation:Z

    .line 212
    const/16 v0, 0xfa

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->maxSlidesCount:I

    .line 261
    const/16 v0, 0xe

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->WAVE_COUNT_X:I

    .line 262
    const/16 v0, 0x1c

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->WAVE_COUNT_Y:I

    .line 265
    iput-boolean v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->particleFitForSquare:Z

    .line 266
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mRandom:Ljava/util/Random;

    .line 358
    iput v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveImagesCount:I

    .line 449
    iput-object v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mTexCoordData:[Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;

    .line 735
    iput-boolean v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveOrientLandscape:Z

    .line 747
    iput-boolean v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveOrientPortraits:Z

    .line 2082
    new-instance v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;

    invoke-direct {v0, p0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;-><init>(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)V

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAnimationCallback:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;

    .line 271
    invoke-virtual {p0, v2}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setZOrderOnTop(Z)V

    .line 272
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAnimationCallback:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$SGIEffectSpeakerAnimCallback;

    invoke-virtual {p0, v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setAnimationCallback(Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;)V

    .line 274
    invoke-virtual {p0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    new-instance v1, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$1;

    invoke-direct {v1, p0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$1;-><init>(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addChangesDrawnListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;)V

    .line 283
    return-void
.end method

.method static synthetic access$0(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)I
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->WAVE_COUNT_X:I

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)I
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->WAVE_COUNT_Y:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)Ljava/util/Vector;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    return v0
.end method

.method private animateBase(I)V
    .locals 3
    .param p1, "duration"    # I

    .prologue
    .line 1855
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-gtz v1, :cond_0

    .line 1858
    :goto_0
    return-void

    .line 1856
    :cond_0
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 1857
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    const v1, 0x3fa66666    # 1.3f

    const v2, 0x3eeb851f    # 0.46f

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateDefaultSkinObject(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;IFF)V

    goto :goto_0
.end method

.method private animateBaseFX(I)V
    .locals 6
    .param p1, "aDuration"    # I

    .prologue
    const/4 v5, 0x1

    .line 1567
    iget-object v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-gt v4, v5, :cond_0

    .line 1582
    :goto_0
    return-void

    .line 1568
    :cond_0
    iget-object v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v4}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 1571
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicBase:F

    const v5, 0x4019999a    # 2.4f

    mul-float/2addr v4, v5

    const v5, 0x40333333    # 2.8f

    add-float v2, v4, v5

    .line 1572
    .local v2, "toScale":F
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicBase:F

    const/high16 v5, 0x40000000    # 2.0f

    mul-float v1, v4, v5

    .line 1574
    .local v1, "toOpacity":F
    new-instance v3, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;-><init>()V

    .line 1575
    .local v3, "transaction":Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;
    invoke-virtual {v3, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->setDuration(I)V

    .line 1576
    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->begin()Z

    .line 1578
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v2, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1579
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1581
    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->end()Z

    goto :goto_0
.end method

.method private animateCenter(I)V
    .locals 3
    .param p1, "duration"    # I

    .prologue
    const/4 v2, 0x3

    .line 1849
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-gt v1, v2, :cond_0

    .line 1852
    :goto_0
    return-void

    .line 1850
    :cond_0
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 1851
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    const v1, 0x3fb33333    # 1.4f

    const v2, 0x3f19999a    # 0.6f

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateDefaultSkinObject(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;IFF)V

    goto :goto_0
.end method

.method private animateDJSkin(I)V
    .locals 0
    .param p1, "aDuration"    # I

    .prologue
    .line 1554
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateBaseFX(I)V

    .line 1555
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateLowFX(I)V

    .line 1556
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateMidFX(I)V

    .line 1557
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateHighFX(I)V

    .line 1558
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateTopBase(I)V

    .line 1559
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateTopFX(I)V

    .line 1560
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateOtherFirstCircle(I)V

    .line 1561
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateOtherSecondCircle(I)V

    .line 1562
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateOtherThirdCircle(I)V

    .line 1563
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateOtherFourthCircle(I)V

    .line 1564
    return-void
.end method

.method private animateDefaultSkin(I)V
    .locals 0
    .param p1, "aDuration"    # I

    .prologue
    .line 1546
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateCenter(I)V

    .line 1547
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateBase(I)V

    .line 1548
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateRubberIn(I)V

    .line 1549
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateRubberOut(I)V

    .line 1550
    return-void
.end method

.method private animateDefaultSkinObject(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;IFF)V
    .locals 7
    .param p1, "slide"    # Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .param p2, "duration"    # I
    .param p3, "startbase"    # F
    .param p4, "tobase"    # F

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    const/high16 v6, -0x3fc00000    # -3.0f

    const v5, -0x40cccccd    # -0.7f

    .line 1788
    iget v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mLastFrequencyDepth:F

    sub-float/2addr v2, v4

    mul-float/2addr v2, p4

    iget v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mEffectScale:F

    mul-float/2addr v2, v3

    add-float v1, p3, v2

    .line 1789
    .local v1, "startScale":F
    iget v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mNextFrequencyDepth:F

    sub-float/2addr v2, v4

    mul-float/2addr v2, p4

    iget v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mEffectScale:F

    mul-float/2addr v2, v3

    add-float v0, p3, v2

    .line 1792
    .local v0, "endScale":F
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicAnimationSet:Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->removeAllAnimations()V

    .line 1793
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicAnimationSet:Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;

    sget-object v3, Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;->QUINT_O_50:Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;

    invoke-static {v3}, Lcom/samsung/android/sdk/sgi/animation/SGTimingFunctionFactory;->createPredefinedTimingFunction(Lcom/samsung/android/sdk/sgi/animation/SGPredefinedTimingFunctionType;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->setTimingFunction(Lcom/samsung/android/sdk/sgi/animation/SGAnimationTimingFunction;)V

    .line 1794
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicAnimationSet:Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;

    invoke-virtual {v2, p2}, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->setDuration(I)V

    .line 1796
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicScaleVectorStart:Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    invoke-virtual {v2, v1, v1, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->set(FFF)V

    .line 1797
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicScaleVectorEnd:Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    invoke-virtual {v2, v0, v0, v0}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;->set(FFF)V

    .line 1800
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicScaleAnimation:Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;

    invoke-virtual {v2, p2}, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->setDuration(I)V

    .line 1801
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicScaleAnimation:Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;

    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicScaleVectorStart:Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->setStartValue(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 1802
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicScaleAnimation:Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;

    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicScaleVectorEnd:Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;->setEndValue(Lcom/samsung/android/sdk/sgi/base/SGVector3f;)V

    .line 1803
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicAnimationSet:Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;

    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicScaleAnimation:Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->addAnimation(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)I

    .line 1805
    iget-boolean v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mUseDepthAnimation:Z

    if-eqz v2, :cond_0

    .line 1807
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicLightPowerAnimation:Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;

    invoke-virtual {v2, p2}, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->setDuration(I)V

    .line 1808
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicLightPowerAnimation:Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;

    iget v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mLastFrequencyDepth:F

    mul-float/2addr v3, v6

    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mEffectScale:F

    mul-float/2addr v3, v4

    add-float/2addr v3, v5

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->setStartValue(F)V

    .line 1809
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicLightPowerAnimation:Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;

    iget v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mNextFrequencyDepth:F

    mul-float/2addr v3, v6

    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mEffectScale:F

    mul-float/2addr v3, v4

    add-float/2addr v3, v5

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->setEndValue(F)V

    .line 1810
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicAnimationSet:Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;

    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicLightPowerAnimation:Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;->addAnimation(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)I

    .line 1815
    :goto_0
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1816
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicAnimationSet:Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;

    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->addAnimation(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)I

    .line 1817
    return-void

    :cond_0
    move-object v2, p1

    .line 1812
    check-cast v2, Lcom/samsung/sgieffect/main/SGISlideEmulation;

    const v3, -0x4059999a    # -1.3f

    invoke-virtual {v2, v3}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightPower(F)V

    goto :goto_0
.end method

.method private animateHighFX(I)V
    .locals 6
    .param p1, "aDuration"    # I

    .prologue
    const/16 v5, 0x8

    .line 1621
    iget-object v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-gt v4, v5, :cond_0

    .line 1636
    :goto_0
    return-void

    .line 1622
    :cond_0
    iget-object v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v4}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 1625
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicHigh:F

    const/high16 v5, 0x41000000    # 8.0f

    mul-float/2addr v4, v5

    const v5, 0x3f99999a    # 1.2f

    add-float v2, v4, v5

    .line 1626
    .local v2, "toScale":F
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicHigh:F

    const v5, 0x3fb33333    # 1.4f

    mul-float/2addr v4, v5

    const v5, 0x3e99999a    # 0.3f

    add-float v1, v4, v5

    .line 1628
    .local v1, "toOpacity":F
    new-instance v3, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;-><init>()V

    .line 1629
    .local v3, "transaction":Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;
    invoke-virtual {v3, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->setDuration(I)V

    .line 1630
    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->begin()Z

    .line 1632
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v2, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1633
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1635
    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->end()Z

    goto :goto_0
.end method

.method private animateLowFX(I)V
    .locals 6
    .param p1, "aDuration"    # I

    .prologue
    const/4 v5, 0x2

    .line 1585
    iget-object v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-gt v4, v5, :cond_0

    .line 1600
    :goto_0
    return-void

    .line 1586
    :cond_0
    iget-object v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v4}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 1589
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicLow:F

    const v5, 0x3f4ccccd    # 0.8f

    mul-float/2addr v4, v5

    const v5, 0x40266666    # 2.6f

    add-float v2, v4, v5

    .line 1590
    .local v2, "toScale":F
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicLow:F

    const v5, 0x3f333333    # 0.7f

    mul-float/2addr v4, v5

    const/high16 v5, 0x3f000000    # 0.5f

    add-float v1, v4, v5

    .line 1592
    .local v1, "toOpacity":F
    new-instance v3, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;-><init>()V

    .line 1593
    .local v3, "transaction":Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;
    invoke-virtual {v3, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->setDuration(I)V

    .line 1594
    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->begin()Z

    .line 1596
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v2, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1597
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1599
    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->end()Z

    goto :goto_0
.end method

.method private animateMidFX(I)V
    .locals 6
    .param p1, "aDuration"    # I

    .prologue
    const/4 v5, 0x7

    .line 1603
    iget-object v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-gt v4, v5, :cond_0

    .line 1618
    :goto_0
    return-void

    .line 1604
    :cond_0
    iget-object v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v4}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 1607
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicMid:F

    const/high16 v5, 0x41000000    # 8.0f

    mul-float/2addr v4, v5

    const v5, 0x4019999a    # 2.4f

    add-float v2, v4, v5

    .line 1608
    .local v2, "toScale":F
    iget v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicMid:F

    .line 1610
    .local v1, "toOpacity":F
    new-instance v3, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;-><init>()V

    .line 1611
    .local v3, "transaction":Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;
    invoke-virtual {v3, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->setDuration(I)V

    .line 1612
    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->begin()Z

    .line 1614
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v2, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1615
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1617
    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->end()Z

    goto :goto_0
.end method

.method private animateOtherFirstCircle(I)V
    .locals 10
    .param p1, "aDuration"    # I

    .prologue
    const/4 v9, 0x3

    const v8, 0x40333333    # 2.8f

    const/high16 v7, 0x3f800000    # 1.0f

    .line 1685
    iget-object v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v5, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1686
    .local v0, "bottom_fx":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicBase:F

    const/high16 v6, 0x40200000    # 2.5f

    mul-float/2addr v5, v6

    add-float v2, v5, v8

    .line 1687
    .local v2, "toScale":F
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicBase:F

    mul-float v1, v5, v8

    .line 1689
    .local v1, "toOpacity":F
    new-instance v4, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-direct {v4}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;-><init>()V

    .line 1690
    .local v4, "transaction":Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;
    invoke-virtual {v4, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->setDuration(I)V

    .line 1691
    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->begin()Z

    .line 1693
    invoke-virtual {v0, v2, v2, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1694
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1697
    iget-object v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v5, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1698
    .local v3, "top_fx":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicBase:F

    const v6, 0x4079999a    # 3.9f

    mul-float/2addr v5, v6

    const v6, 0x3f99999a    # 1.2f

    add-float v2, v5, v6

    .line 1699
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicBase:F

    const v6, 0x3f333333    # 0.7f

    mul-float/2addr v5, v6

    const/high16 v6, 0x3f000000    # 0.5f

    add-float v1, v5, v6

    .line 1701
    invoke-virtual {v3, v2, v2, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1702
    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1704
    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->end()Z

    .line 1705
    return-void
.end method

.method private animateOtherFourthCircle(I)V
    .locals 10
    .param p1, "aDuration"    # I

    .prologue
    const/4 v9, 0x6

    const v8, 0x40333333    # 2.8f

    const/high16 v7, 0x3f800000    # 1.0f

    .line 1765
    iget-object v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v5, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1766
    .local v0, "bottom_fx":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicHigh:F

    const/high16 v6, 0x40200000    # 2.5f

    mul-float/2addr v5, v6

    add-float v2, v5, v8

    .line 1767
    .local v2, "toScale":F
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicHigh:F

    mul-float v1, v5, v8

    .line 1769
    .local v1, "toOpacity":F
    new-instance v4, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-direct {v4}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;-><init>()V

    .line 1770
    .local v4, "transaction":Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;
    invoke-virtual {v4, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->setDuration(I)V

    .line 1771
    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->begin()Z

    .line 1773
    invoke-virtual {v0, v2, v2, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1774
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1777
    iget-object v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v5, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1778
    .local v3, "top_fx":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicHigh:F

    const v6, 0x4079999a    # 3.9f

    mul-float/2addr v5, v6

    const v6, 0x3f99999a    # 1.2f

    add-float v2, v5, v6

    .line 1779
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicHigh:F

    const v6, 0x3f333333    # 0.7f

    mul-float/2addr v5, v6

    const/high16 v6, 0x3f000000    # 0.5f

    add-float v1, v5, v6

    .line 1781
    invoke-virtual {v3, v2, v2, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1782
    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1784
    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->end()Z

    .line 1785
    return-void
.end method

.method private animateOtherSecondCircle(I)V
    .locals 10
    .param p1, "aDuration"    # I

    .prologue
    const/4 v9, 0x4

    const v8, 0x40333333    # 2.8f

    const/high16 v7, 0x3f800000    # 1.0f

    .line 1711
    iget-object v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v5, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1712
    .local v0, "bottom_fx":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicLow:F

    const/high16 v6, 0x40200000    # 2.5f

    mul-float/2addr v5, v6

    add-float v2, v5, v8

    .line 1713
    .local v2, "toScale":F
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicLow:F

    mul-float v1, v5, v8

    .line 1715
    .local v1, "toOpacity":F
    new-instance v4, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-direct {v4}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;-><init>()V

    .line 1716
    .local v4, "transaction":Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;
    invoke-virtual {v4, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->setDuration(I)V

    .line 1717
    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->begin()Z

    .line 1719
    invoke-virtual {v0, v2, v2, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1720
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1723
    iget-object v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v5, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1724
    .local v3, "top_fx":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicLow:F

    const v6, 0x4079999a    # 3.9f

    mul-float/2addr v5, v6

    const v6, 0x3f99999a    # 1.2f

    add-float v2, v5, v6

    .line 1725
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicLow:F

    const v6, 0x3f333333    # 0.7f

    mul-float/2addr v5, v6

    const/high16 v6, 0x3f000000    # 0.5f

    add-float v1, v5, v6

    .line 1727
    invoke-virtual {v3, v2, v2, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1728
    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1730
    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->end()Z

    .line 1731
    return-void
.end method

.method private animateOtherThirdCircle(I)V
    .locals 10
    .param p1, "aDuration"    # I

    .prologue
    const/4 v9, 0x5

    const v8, 0x40333333    # 2.8f

    const/high16 v7, 0x3f800000    # 1.0f

    .line 1738
    iget-object v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v5, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1739
    .local v0, "bottom_fx":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicMid:F

    const/high16 v6, 0x40200000    # 2.5f

    mul-float/2addr v5, v6

    add-float v2, v5, v8

    .line 1740
    .local v2, "toScale":F
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicMid:F

    mul-float v1, v5, v8

    .line 1742
    .local v1, "toOpacity":F
    new-instance v4, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-direct {v4}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;-><init>()V

    .line 1743
    .local v4, "transaction":Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;
    invoke-virtual {v4, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->setDuration(I)V

    .line 1744
    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->begin()Z

    .line 1746
    invoke-virtual {v0, v2, v2, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1747
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1750
    iget-object v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v5, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1751
    .local v3, "top_fx":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicMid:F

    const v6, 0x4079999a    # 3.9f

    mul-float/2addr v5, v6

    const v6, 0x3f99999a    # 1.2f

    add-float v2, v5, v6

    .line 1752
    iget v5, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicMid:F

    const v6, 0x3f333333    # 0.7f

    mul-float/2addr v5, v6

    const/high16 v6, 0x3f000000    # 0.5f

    add-float v1, v5, v6

    .line 1754
    invoke-virtual {v3, v2, v2, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1755
    invoke-virtual {v3, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1757
    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->end()Z

    .line 1758
    return-void
.end method

.method private animateRubberIn(I)V
    .locals 3
    .param p1, "duration"    # I

    .prologue
    const/4 v2, 0x2

    .line 1861
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-gt v1, v2, :cond_0

    .line 1864
    :goto_0
    return-void

    .line 1862
    :cond_0
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 1863
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    const v1, 0x3f8b851f    # 1.09f

    const v2, 0x3e8f5c29    # 0.28f

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateDefaultSkinObject(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;IFF)V

    goto :goto_0
.end method

.method private animateRubberOut(I)V
    .locals 3
    .param p1, "duration"    # I

    .prologue
    const/4 v2, 0x1

    .line 1867
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-gt v1, v2, :cond_0

    .line 1870
    :goto_0
    return-void

    .line 1868
    :cond_0
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 1869
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    const v1, 0x3f866666    # 1.05f

    const v2, 0x3e0f5c29    # 0.14f

    invoke-direct {p0, v0, p1, v1, v2}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateDefaultSkinObject(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;IFF)V

    goto :goto_0
.end method

.method private animateTopBase(I)V
    .locals 7
    .param p1, "aDuration"    # I

    .prologue
    const/16 v6, 0x9

    const/high16 v5, 0x3f800000    # 1.0f

    const v4, 0x3eb33333    # 0.35f

    .line 1639
    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-gt v3, v6, :cond_0

    .line 1657
    :goto_0
    return-void

    .line 1640
    :cond_0
    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v3, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v3}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 1643
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mNextFrequencyDepth:F

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_1

    .line 1644
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1650
    .local v1, "toScale":F
    :goto_1
    new-instance v2, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-direct {v2}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;-><init>()V

    .line 1651
    .local v2, "transaction":Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;
    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->setDuration(I)V

    .line 1652
    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->begin()Z

    .line 1654
    invoke-virtual {v0, v1, v1, v5}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1656
    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->end()Z

    goto :goto_0

    .line 1647
    .end local v1    # "toScale":F
    .end local v2    # "transaction":Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;
    :cond_1
    iget v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mNextFrequencyDepth:F

    sub-float/2addr v3, v4

    const v4, 0x3d8f5c29    # 0.07f

    mul-float/2addr v3, v4

    add-float v1, v3, v5

    .restart local v1    # "toScale":F
    goto :goto_1
.end method

.method private animateTopFX(I)V
    .locals 7
    .param p1, "aDuration"    # I

    .prologue
    const/16 v6, 0xa

    const/high16 v5, 0x3f800000    # 1.0f

    const v4, 0x3eb33333    # 0.35f

    .line 1660
    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-gt v3, v6, :cond_0

    .line 1679
    :goto_0
    return-void

    .line 1661
    :cond_0
    iget-object v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v3, v6}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v3}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 1664
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mNextFrequencyDepth:F

    cmpg-float v3, v3, v4

    if-gtz v3, :cond_1

    .line 1665
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1671
    .local v1, "toScale":F
    :goto_1
    new-instance v2, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;

    invoke-direct {v2}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;-><init>()V

    .line 1672
    .local v2, "transaction":Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;
    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->setDuration(I)V

    .line 1673
    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->begin()Z

    .line 1675
    invoke-virtual {v0, v1, v1, v5}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1676
    iget v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mNextFrequencyDepth:F

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1678
    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;->end()Z

    goto :goto_0

    .line 1668
    .end local v1    # "toScale":F
    .end local v2    # "transaction":Lcom/samsung/android/sdk/sgi/animation/SGAnimationTransaction;
    :cond_1
    iget v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mNextFrequencyDepth:F

    sub-float/2addr v3, v4

    const v4, 0x3d8f5c29    # 0.07f

    mul-float/2addr v3, v4

    add-float v1, v3, v5

    .restart local v1    # "toScale":F
    goto :goto_1
.end method

.method private buildWaveSkin()V
    .locals 30

    .prologue
    .line 473
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    move-object/from16 v24, v0

    if-eqz v24, :cond_0

    .line 475
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->release()V

    .line 476
    const/16 v24, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    .line 479
    :cond_0
    new-instance v24, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;-><init>(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    .line 480
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->maxSlidesCount:I

    move/from16 v25, v0

    move/from16 v0, v25

    move-object/from16 v1, v24

    iput v0, v1, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->maxSlidesCount:I

    .line 483
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 484
    .local v13, "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    invoke-virtual {v13}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v6

    .line 486
    .local v6, "baseSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v13}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getRegion()Landroid/graphics/RectF;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->width()F

    move-result v18

    .line 487
    .local v18, "width":F
    invoke-virtual {v13}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getRegion()Landroid/graphics/RectF;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->height()F

    move-result v9

    .line 489
    .local v9, "height":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideXCount:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v20, v18, v24

    .line 490
    .local v20, "xstep":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideYCount:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v23, v9, v24

    .line 493
    .local v23, "ystep":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideYCount:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    const/high16 v25, 0x3f000000    # 0.5f

    mul-float v8, v24, v25

    .line 494
    .local v8, "halfYCount":F
    const/high16 v24, 0x3f000000    # 0.5f

    mul-float v24, v24, v9

    const/high16 v25, 0x3f000000    # 0.5f

    mul-float v25, v25, v23

    sub-float v22, v24, v25

    .line 495
    .local v22, "yOffset":F
    const/4 v5, 0x0

    .line 499
    .local v5, "addingYOffset":F
    move/from16 v15, v20

    .line 500
    .local v15, "particleWidth":F
    move/from16 v14, v23

    .line 503
    .local v14, "particleHeigh":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveOrientLandscape:Z

    move/from16 v24, v0

    if-eqz v24, :cond_3

    .line 505
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->particleFitForSquare:Z

    move/from16 v24, v0

    if-eqz v24, :cond_1

    .line 506
    cmpl-float v24, v20, v23

    if-lez v24, :cond_2

    .line 507
    sub-float v5, v20, v23

    .line 510
    :goto_0
    move/from16 v14, v20

    .line 531
    :cond_1
    :goto_1
    new-instance v17, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    sget-object v24, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    sget-object v25, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;)V

    .line 532
    .local v17, "textureProperty":Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAtlas:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 534
    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11}, Landroid/graphics/RectF;-><init>()V

    .line 538
    .local v11, "mRect":Landroid/graphics/RectF;
    const/16 v24, 0xff

    const/16 v25, 0x0

    const/16 v26, 0x0

    const/16 v27, 0x0

    invoke-static/range {v24 .. v27}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    .line 539
    .local v7, "color":I
    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 541
    const/4 v12, 0x0

    .local v12, "n":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->maxSlidesCount:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-lt v12, v0, :cond_5

    .line 565
    const/16 v21, 0x0

    .local v21, "y":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideYCount:I

    move/from16 v24, v0

    move/from16 v0, v21

    move/from16 v1, v24

    if-lt v0, v1, :cond_6

    .line 571
    return-void

    .line 509
    .end local v7    # "color":I
    .end local v11    # "mRect":Landroid/graphics/RectF;
    .end local v12    # "n":I
    .end local v17    # "textureProperty":Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;
    .end local v21    # "y":I
    :cond_2
    sub-float v5, v23, v20

    goto :goto_0

    .line 513
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveOrientPortraits:Z

    move/from16 v24, v0

    if-eqz v24, :cond_1

    .line 515
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->particleFitForSquare:Z

    move/from16 v24, v0

    if-eqz v24, :cond_1

    .line 516
    cmpl-float v24, v20, v23

    if-lez v24, :cond_4

    .line 517
    sub-float v5, v20, v23

    .line 520
    :goto_4
    move/from16 v15, v23

    goto :goto_1

    .line 519
    :cond_4
    sub-float v5, v23, v20

    goto :goto_4

    .line 542
    .restart local v7    # "color":I
    .restart local v11    # "mRect":Landroid/graphics/RectF;
    .restart local v12    # "n":I
    .restart local v17    # "textureProperty":Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;
    :cond_5
    new-instance v16, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    new-instance v24, Landroid/graphics/RectF;

    const/16 v25, 0x0

    mul-float v25, v25, v20

    const/16 v26, 0x0

    mul-float v26, v26, v23

    move-object/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2, v15, v14}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>(Landroid/graphics/RectF;)V

    .line 543
    .local v16, "tempslide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 544
    move-object/from16 v0, v16

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 546
    const/16 v24, 0x0

    const/16 v25, 0x0

    const/high16 v26, 0x3f800000    # 1.0f

    move-object/from16 v0, v16

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 548
    invoke-virtual/range {v16 .. v17}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setTexture(Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 550
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mRandom:Ljava/util/Random;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveImagesCount:I

    move/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/util/Random;->nextInt(I)I

    move-result v10

    .line 551
    .local v10, "index":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mTexCoordData:[Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;

    move-object/from16 v24, v0

    aget-object v24, v24, v10

    move-object/from16 v0, v24

    iget v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;->mULeft:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mTexCoordData:[Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;

    move-object/from16 v25, v0

    aget-object v25, v25, v10

    move-object/from16 v0, v25

    iget v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;->mVTop:F

    move/from16 v25, v0

    .line 552
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mTexCoordData:[Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;

    move-object/from16 v26, v0

    aget-object v26, v26, v10

    move-object/from16 v0, v26

    iget v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;->mURight:F

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mTexCoordData:[Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;

    move-object/from16 v27, v0

    aget-object v27, v27, v10

    move-object/from16 v0, v27

    iget v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;->mVBottom:F

    move/from16 v27, v0

    .line 551
    move/from16 v0, v24

    move/from16 v1, v25

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 554
    move-object/from16 v0, v16

    invoke-virtual {v0, v11}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setContentRect(Landroid/graphics/RectF;)V

    .line 555
    sget-object v24, Lcom/samsung/android/sdk/sgi/vi/SGBlendMode;->MULTIPLY:Lcom/samsung/android/sdk/sgi/vi/SGBlendMode;

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBlendMode(Lcom/samsung/android/sdk/sgi/vi/SGBlendMode;)V

    .line 556
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideGrid:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 560
    const/high16 v24, 0x3f000000    # 0.5f

    const/high16 v25, 0x3f000000    # 0.5f

    move-object/from16 v0, v16

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 561
    const/high16 v24, 0x3f000000    # 0.5f

    const/high16 v25, 0x3f000000    # 0.5f

    move-object/from16 v0, v16

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setPositionPivot(FF)V

    .line 541
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2

    .line 566
    .end local v10    # "index":I
    .end local v16    # "tempslide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .restart local v21    # "y":I
    :cond_6
    const/16 v19, 0x0

    .local v19, "x":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideXCount:I

    move/from16 v24, v0

    move/from16 v0, v19

    move/from16 v1, v24

    if-lt v0, v1, :cond_7

    .line 565
    add-int/lit8 v21, v21, 0x1

    goto/16 :goto_3

    .line 567
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->scaleGrid:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    new-instance v25, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;

    const/16 v26, 0x0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v27, v0

    mul-float v27, v27, v20

    const/high16 v28, 0x3f000000    # 0.5f

    mul-float v28, v28, v20

    add-float v27, v27, v28

    mul-float v28, v23, v8

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v29, v0

    mul-float v29, v29, v23

    sub-float v28, v28, v29

    add-float v28, v28, v22

    add-float v28, v28, v5

    move-object/from16 v0, v25

    move-object/from16 v1, p0

    move/from16 v2, v26

    move/from16 v3, v27

    move/from16 v4, v28

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;-><init>(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;FFF)V

    invoke-virtual/range {v24 .. v25}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 566
    add-int/lit8 v19, v19, 0x1

    goto :goto_5
.end method

.method private getCurrentMaskInfo()Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;
    .locals 2

    .prologue
    .line 1102
    const/4 v0, 0x0

    .line 1104
    .local v0, "info":Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;
    iget v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMaskType:I

    packed-switch v1, :pswitch_data_0

    .line 1124
    :goto_0
    if-nez v0, :cond_0

    .line 1125
    const/4 v0, 0x0

    .line 1128
    .end local v0    # "info":Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;
    :cond_0
    return-object v0

    .line 1106
    .restart local v0    # "info":Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 1107
    goto :goto_0

    .line 1109
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 1110
    goto :goto_0

    .line 1112
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 1113
    goto :goto_0

    .line 1115
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 1116
    goto :goto_0

    .line 1118
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 1119
    goto :goto_0

    .line 1104
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private releaseFunkySkin()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 330
    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 331
    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 332
    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 333
    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 334
    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 335
    return-void
.end method

.method private setOthersCircle(ILandroid/graphics/Bitmap;Z)V
    .locals 25
    .param p1, "aMaskType"    # I
    .param p2, "aMaskBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "isSetBitmap"    # Z

    .prologue
    .line 1133
    const/4 v13, 0x0

    .line 1135
    .local v13, "info":Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->getCurrentMaskInfo()Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    move-result-object v13

    .line 1137
    if-nez v13, :cond_0

    .line 1138
    const-string v20, "SGIEffect"

    const-string v21, "Mask Info null, please check mask info"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1391
    :goto_0
    return-void

    .line 1142
    :cond_0
    new-instance v4, Landroid/graphics/RectF;

    const/16 v20, 0x0

    const/16 v21, 0x0

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomWidth:F

    move/from16 v22, v0

    const/high16 v23, 0x41200000    # 10.0f

    add-float v22, v22, v23

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomHeight:F

    move/from16 v23, v0

    const/high16 v24, 0x41200000    # 10.0f

    add-float v23, v23, v24

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-direct {v4, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1143
    .local v4, "bottomRegion":Landroid/graphics/RectF;
    new-instance v12, Landroid/graphics/RectF;

    const/16 v20, 0x0

    const/16 v21, 0x0

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxWidth:F

    move/from16 v22, v0

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxHeight:F

    move/from16 v23, v0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-direct {v12, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1157
    .local v12, "fxRegion":Landroid/graphics/RectF;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    move-object/from16 v20, v0

    const/16 v21, 0x3

    invoke-virtual/range {v20 .. v21}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 1158
    .local v5, "bottom_base_node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    invoke-virtual {v5, v4}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setRegion(Landroid/graphics/RectF;)V

    .line 1159
    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getRegion()Landroid/graphics/RectF;

    move-result-object v16

    .line 1160
    .local v16, "rect":Landroid/graphics/RectF;
    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v6

    .line 1162
    .local v6, "bottom_base_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->width()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    move/from16 v21, v0

    mul-float v11, v20, v21

    .line 1163
    .local v11, "bottom_width":F
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->height()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    move/from16 v21, v0

    mul-float v10, v20, v21

    .line 1165
    .local v10, "bottom_height":F
    invoke-virtual {v6, v11, v10}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 1166
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1167
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1168
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x40000000    # 2.0f

    div-float v21, v11, v21

    sub-float v20, v20, v21

    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    const/high16 v22, 0x40000000    # 2.0f

    div-float v22, v10, v22

    sub-float v21, v21, v22

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setPosition(FF)V

    .line 1169
    if-eqz p3, :cond_1

    .line 1171
    const v20, -0xf6f6f7

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 1174
    :cond_1
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1176
    .local v8, "bottom_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v12}, Landroid/graphics/RectF;->width()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    move/from16 v21, v0

    mul-float v19, v20, v21

    .line 1177
    .local v19, "top_fx_width":F
    invoke-virtual {v12}, Landroid/graphics/RectF;->height()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    move/from16 v21, v0

    mul-float v17, v20, v21

    .line 1178
    .local v17, "top_fx_height":F
    move/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 1179
    const/high16 v20, 0x40000000    # 2.0f

    div-float v14, v11, v20

    .line 1180
    .local v14, "pos_x":F
    const/high16 v20, 0x40000000    # 2.0f

    div-float v15, v10, v20

    .line 1181
    .local v15, "pos_y":F
    const/high16 v20, 0x40000000    # 2.0f

    div-float v20, v19, v20

    sub-float v20, v14, v20

    const/high16 v21, 0x40000000    # 2.0f

    div-float v21, v17, v21

    sub-float v21, v15, v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setPosition(FF)V

    .line 1182
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1183
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1184
    const v20, 0x3f2e147b    # 0.68f

    const v21, 0x3f2e147b    # 0.68f

    const/high16 v22, 0x3f800000    # 1.0f

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v8, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1185
    if-eqz p3, :cond_2

    .line 1186
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1189
    :cond_2
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1191
    .local v18, "top_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v12}, Landroid/graphics/RectF;->width()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    move/from16 v21, v0

    mul-float v9, v20, v21

    .line 1192
    .local v9, "bottom_fx_width":F
    invoke-virtual {v12}, Landroid/graphics/RectF;->height()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    move/from16 v21, v0

    mul-float v7, v20, v21

    .line 1193
    .local v7, "bottom_fx_height":F
    move-object/from16 v0, v18

    invoke-virtual {v0, v9, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 1195
    const/high16 v20, 0x40000000    # 2.0f

    div-float v14, v11, v20

    .line 1196
    const/high16 v20, 0x40000000    # 2.0f

    div-float v15, v10, v20

    .line 1197
    const/high16 v20, 0x40000000    # 2.0f

    div-float v20, v9, v20

    sub-float v20, v14, v20

    const/high16 v21, 0x40000000    # 2.0f

    div-float v21, v7, v21

    sub-float v21, v15, v21

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setPosition(FF)V

    .line 1198
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1199
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1200
    const v20, 0x3f2e147b    # 0.68f

    const v21, 0x3f2e147b    # 0.68f

    const/high16 v22, 0x3f800000    # 1.0f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1202
    if-eqz p3, :cond_3

    .line 1203
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1204
    :cond_3
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    if-nez v20, :cond_4

    .line 1205
    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 1214
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "bottom_base_node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    check-cast v5, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 1215
    .restart local v5    # "bottom_base_node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    invoke-virtual {v5, v4}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setRegion(Landroid/graphics/RectF;)V

    .line 1216
    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getRegion()Landroid/graphics/RectF;

    move-result-object v16

    .line 1217
    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v6

    .line 1219
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->width()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    move/from16 v21, v0

    mul-float v11, v20, v21

    .line 1220
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->height()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    move/from16 v21, v0

    mul-float v10, v20, v21

    .line 1222
    invoke-virtual {v6, v11, v10}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 1223
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x40000000    # 2.0f

    div-float v21, v11, v21

    sub-float v20, v20, v21

    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    const/high16 v22, 0x40000000    # 2.0f

    div-float v22, v10, v22

    sub-float v21, v21, v22

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setPosition(FF)V

    .line 1224
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1225
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1226
    if-eqz p3, :cond_5

    .line 1228
    const v20, -0xf6f6f7

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 1231
    :cond_5
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v8

    .end local v8    # "bottom_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    check-cast v8, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1233
    .restart local v8    # "bottom_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v12}, Landroid/graphics/RectF;->width()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    move/from16 v21, v0

    mul-float v19, v20, v21

    .line 1234
    invoke-virtual {v12}, Landroid/graphics/RectF;->height()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    move/from16 v21, v0

    mul-float v17, v20, v21

    .line 1235
    move/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 1237
    const/high16 v20, 0x40000000    # 2.0f

    div-float v14, v11, v20

    .line 1238
    const/high16 v20, 0x40000000    # 2.0f

    div-float v15, v10, v20

    .line 1239
    const/high16 v20, 0x40000000    # 2.0f

    div-float v20, v19, v20

    sub-float v20, v14, v20

    const/high16 v21, 0x40000000    # 2.0f

    div-float v21, v17, v21

    sub-float v21, v15, v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setPosition(FF)V

    .line 1240
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1241
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1242
    const v20, 0x3f2e147b    # 0.68f

    const v21, 0x3f2e147b    # 0.68f

    const/high16 v22, 0x3f800000    # 1.0f

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v8, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1243
    if-eqz p3, :cond_6

    .line 1244
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1247
    :cond_6
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v18

    .end local v18    # "top_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    check-cast v18, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1249
    .restart local v18    # "top_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v12}, Landroid/graphics/RectF;->width()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    move/from16 v21, v0

    mul-float v9, v20, v21

    .line 1250
    invoke-virtual {v12}, Landroid/graphics/RectF;->height()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    move/from16 v21, v0

    mul-float v7, v20, v21

    .line 1251
    move-object/from16 v0, v18

    invoke-virtual {v0, v9, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 1253
    const/high16 v20, 0x40000000    # 2.0f

    div-float v14, v11, v20

    .line 1254
    const/high16 v20, 0x40000000    # 2.0f

    div-float v15, v10, v20

    .line 1255
    const/high16 v20, 0x40000000    # 2.0f

    div-float v20, v9, v20

    sub-float v20, v14, v20

    const/high16 v21, 0x40000000    # 2.0f

    div-float v21, v7, v21

    sub-float v21, v15, v21

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setPosition(FF)V

    .line 1256
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1257
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1258
    const v20, 0x3f2e147b    # 0.68f

    const v21, 0x3f2e147b    # 0.68f

    const/high16 v22, 0x3f800000    # 1.0f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1260
    if-eqz p3, :cond_7

    .line 1261
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1262
    :cond_7
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    if-nez v20, :cond_8

    .line 1263
    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 1272
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    move-object/from16 v20, v0

    const/16 v21, 0x5

    invoke-virtual/range {v20 .. v21}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "bottom_base_node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    check-cast v5, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 1273
    .restart local v5    # "bottom_base_node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    invoke-virtual {v5, v4}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setRegion(Landroid/graphics/RectF;)V

    .line 1274
    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getRegion()Landroid/graphics/RectF;

    move-result-object v16

    .line 1275
    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v6

    .line 1277
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->width()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    move/from16 v21, v0

    mul-float v11, v20, v21

    .line 1278
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->height()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    move/from16 v21, v0

    mul-float v10, v20, v21

    .line 1280
    invoke-virtual {v6, v11, v10}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 1281
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x40000000    # 2.0f

    div-float v21, v11, v21

    sub-float v20, v20, v21

    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    const/high16 v22, 0x40000000    # 2.0f

    div-float v22, v10, v22

    sub-float v21, v21, v22

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setPosition(FF)V

    .line 1282
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1283
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1284
    if-eqz p3, :cond_9

    .line 1286
    const v20, -0xf6f6f7

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 1289
    :cond_9
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v8

    .end local v8    # "bottom_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    check-cast v8, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1291
    .restart local v8    # "bottom_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v12}, Landroid/graphics/RectF;->width()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    move/from16 v21, v0

    mul-float v19, v20, v21

    .line 1292
    invoke-virtual {v12}, Landroid/graphics/RectF;->height()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    move/from16 v21, v0

    mul-float v17, v20, v21

    .line 1293
    move/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 1295
    const/high16 v20, 0x40000000    # 2.0f

    div-float v14, v11, v20

    .line 1296
    const/high16 v20, 0x40000000    # 2.0f

    div-float v15, v10, v20

    .line 1297
    const/high16 v20, 0x40000000    # 2.0f

    div-float v20, v19, v20

    sub-float v20, v14, v20

    const/high16 v21, 0x40000000    # 2.0f

    div-float v21, v17, v21

    sub-float v21, v15, v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setPosition(FF)V

    .line 1298
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1299
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1300
    const v20, 0x3f2e147b    # 0.68f

    const v21, 0x3f2e147b    # 0.68f

    const/high16 v22, 0x3f800000    # 1.0f

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v8, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1304
    if-eqz p3, :cond_a

    .line 1305
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1308
    :cond_a
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v18

    .end local v18    # "top_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    check-cast v18, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1310
    .restart local v18    # "top_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v12}, Landroid/graphics/RectF;->width()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    move/from16 v21, v0

    mul-float v9, v20, v21

    .line 1311
    invoke-virtual {v12}, Landroid/graphics/RectF;->height()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    move/from16 v21, v0

    mul-float v7, v20, v21

    .line 1312
    move-object/from16 v0, v18

    invoke-virtual {v0, v9, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 1314
    const/high16 v20, 0x40000000    # 2.0f

    div-float v14, v11, v20

    .line 1315
    const/high16 v20, 0x40000000    # 2.0f

    div-float v15, v10, v20

    .line 1316
    const/high16 v20, 0x40000000    # 2.0f

    div-float v20, v9, v20

    sub-float v20, v14, v20

    const/high16 v21, 0x40000000    # 2.0f

    div-float v21, v7, v21

    sub-float v21, v15, v21

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setPosition(FF)V

    .line 1317
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1318
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1319
    const v20, 0x3f2e147b    # 0.68f

    const v21, 0x3f2e147b    # 0.68f

    const/high16 v22, 0x3f800000    # 1.0f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1321
    if-eqz p3, :cond_b

    .line 1322
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1323
    :cond_b
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    if-nez v20, :cond_c

    .line 1324
    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 1333
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    move-object/from16 v20, v0

    const/16 v21, 0x6

    invoke-virtual/range {v20 .. v21}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "bottom_base_node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    check-cast v5, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 1334
    .restart local v5    # "bottom_base_node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    invoke-virtual {v5, v4}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setRegion(Landroid/graphics/RectF;)V

    .line 1335
    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getRegion()Landroid/graphics/RectF;

    move-result-object v16

    .line 1336
    invoke-virtual {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v6

    .line 1338
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->width()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    move/from16 v21, v0

    mul-float v11, v20, v21

    .line 1339
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/RectF;->height()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    move/from16 v21, v0

    mul-float v10, v20, v21

    .line 1341
    invoke-virtual {v6, v11, v10}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 1342
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x40000000    # 2.0f

    div-float v21, v11, v21

    sub-float v20, v20, v21

    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    const/high16 v22, 0x40000000    # 2.0f

    div-float v22, v10, v22

    sub-float v21, v21, v22

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setPosition(FF)V

    .line 1343
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1344
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v6, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1345
    if-eqz p3, :cond_d

    .line 1347
    const v20, -0xf6f6f7

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 1350
    :cond_d
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v8

    .end local v8    # "bottom_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    check-cast v8, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1352
    .restart local v8    # "bottom_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v12}, Landroid/graphics/RectF;->width()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    move/from16 v21, v0

    mul-float v19, v20, v21

    .line 1353
    invoke-virtual {v12}, Landroid/graphics/RectF;->height()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    move/from16 v21, v0

    mul-float v17, v20, v21

    .line 1354
    move/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 1356
    const/high16 v20, 0x40000000    # 2.0f

    div-float v14, v11, v20

    .line 1357
    const/high16 v20, 0x40000000    # 2.0f

    div-float v15, v10, v20

    .line 1358
    const/high16 v20, 0x40000000    # 2.0f

    div-float v20, v19, v20

    sub-float v20, v14, v20

    const/high16 v21, 0x40000000    # 2.0f

    div-float v21, v17, v21

    sub-float v21, v15, v21

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setPosition(FF)V

    .line 1359
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1360
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1361
    const v20, 0x3f2e147b    # 0.68f

    const v21, 0x3f2e147b    # 0.68f

    const/high16 v22, 0x3f800000    # 1.0f

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v8, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1363
    if-eqz p3, :cond_e

    .line 1364
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1367
    :cond_e
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v18

    .end local v18    # "top_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    check-cast v18, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1369
    .restart local v18    # "top_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v12}, Landroid/graphics/RectF;->width()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    move/from16 v21, v0

    mul-float v9, v20, v21

    .line 1370
    invoke-virtual {v12}, Landroid/graphics/RectF;->height()F

    move-result v20

    iget v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    move/from16 v21, v0

    mul-float v7, v20, v21

    .line 1371
    move-object/from16 v0, v18

    invoke-virtual {v0, v9, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 1373
    const/high16 v20, 0x40000000    # 2.0f

    div-float v14, v11, v20

    .line 1374
    const/high16 v20, 0x40000000    # 2.0f

    div-float v15, v10, v20

    .line 1375
    const/high16 v20, 0x40000000    # 2.0f

    div-float v20, v9, v20

    sub-float v20, v14, v20

    const/high16 v21, 0x40000000    # 2.0f

    div-float v21, v7, v21

    sub-float v21, v15, v21

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setPosition(FF)V

    .line 1376
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1377
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1378
    const v20, 0x3f2e147b    # 0.68f

    const v21, 0x3f2e147b    # 0.68f

    const/high16 v22, 0x3f800000    # 1.0f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 1380
    if-eqz p3, :cond_f

    .line 1381
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1382
    :cond_f
    iget-object v0, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    move-object/from16 v20, v0

    if-nez v20, :cond_10

    .line 1383
    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 1390
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMaskBitmapTexture2DProperty:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->setBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0
.end method

.method private setPannelDepthAnimation(I)V
    .locals 2
    .param p1, "aDuration"    # I

    .prologue
    .line 1534
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 1542
    :cond_0
    :goto_0
    return-void

    .line 1536
    :cond_1
    iget v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    if-nez v0, :cond_2

    .line 1537
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateDefaultSkin(I)V

    goto :goto_0

    .line 1539
    :cond_2
    iget v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1540
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->animateDJSkin(I)V

    goto :goto_0
.end method


# virtual methods
.method public addHighWave(FFFFF)V
    .locals 7
    .param p1, "effectiveRadius"    # F
    .param p2, "radius"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "speed"    # F

    .prologue
    .line 678
    iget v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    if-eqz v0, :cond_0

    .line 679
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    const/4 v6, 0x1

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/sgieffect/speaker/MathWave;->addWave(FFFFFZ)V

    .line 681
    :cond_0
    return-void
.end method

.method public addLowWave(FFFFF)V
    .locals 7
    .param p1, "effectiveRadius"    # F
    .param p2, "radius"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "speed"    # F

    .prologue
    .line 670
    iget v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    const/4 v6, 0x0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/sgieffect/speaker/MathWave;->addWave(FFFFFZ)V

    .line 673
    :cond_0
    return-void
.end method

.method public createOthersCircleBase()V
    .locals 0

    .prologue
    .line 1098
    return-void
.end method

.method public createOthersCircleFX()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v8, 0x3f000000    # 0.5f

    .line 968
    new-instance v1, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-direct {v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;-><init>()V

    .line 969
    .local v1, "first_node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    iget-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v7, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 970
    new-instance v3, Lcom/samsung/sgieffect/main/CircleLayerImage;

    invoke-direct {v3}, Lcom/samsung/sgieffect/main/CircleLayerImage;-><init>()V

    .line 971
    .local v3, "root_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v7, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 972
    invoke-virtual {v1, v3}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 973
    invoke-virtual {v3, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 974
    invoke-virtual {v3, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 975
    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 976
    invoke-virtual {v3, v10}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setChildrenClipState(Z)V

    .line 979
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 980
    .local v0, "bottom_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 982
    invoke-virtual {v0, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 983
    invoke-virtual {v0, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 984
    invoke-virtual {v0, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 987
    new-instance v6, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-direct {v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 988
    .local v6, "top_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 990
    invoke-virtual {v6, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 991
    invoke-virtual {v6, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 992
    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 997
    new-instance v4, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-direct {v4}, Lcom/samsung/sgieffect/main/SGIEffectNode;-><init>()V

    .line 998
    .local v4, "second_node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    iget-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v7, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 999
    new-instance v3, Lcom/samsung/sgieffect/main/CircleLayerImage;

    .end local v3    # "root_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-direct {v3}, Lcom/samsung/sgieffect/main/CircleLayerImage;-><init>()V

    .line 1000
    .restart local v3    # "root_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v7, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 1001
    invoke-virtual {v4, v3}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 1002
    invoke-virtual {v3, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1003
    invoke-virtual {v3, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1004
    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1005
    invoke-virtual {v3, v10}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setChildrenClipState(Z)V

    .line 1008
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .end local v0    # "bottom_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 1009
    .restart local v0    # "bottom_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 1011
    invoke-virtual {v0, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1012
    invoke-virtual {v0, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1013
    invoke-virtual {v0, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1016
    new-instance v6, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .end local v6    # "top_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-direct {v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 1017
    .restart local v6    # "top_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 1019
    invoke-virtual {v6, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1020
    invoke-virtual {v6, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1021
    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1026
    new-instance v5, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-direct {v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;-><init>()V

    .line 1027
    .local v5, "third_node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    iget-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v7, v5}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1028
    new-instance v3, Lcom/samsung/sgieffect/main/CircleLayerImage;

    .end local v3    # "root_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-direct {v3}, Lcom/samsung/sgieffect/main/CircleLayerImage;-><init>()V

    .line 1029
    .restart local v3    # "root_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v7, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 1030
    invoke-virtual {v5, v3}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 1031
    invoke-virtual {v3, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1032
    invoke-virtual {v3, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1033
    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1034
    invoke-virtual {v3, v10}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setChildrenClipState(Z)V

    .line 1037
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .end local v0    # "bottom_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 1038
    .restart local v0    # "bottom_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 1040
    invoke-virtual {v0, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1041
    invoke-virtual {v0, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1042
    invoke-virtual {v0, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1045
    new-instance v6, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .end local v6    # "top_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-direct {v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 1046
    .restart local v6    # "top_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 1048
    invoke-virtual {v6, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1049
    invoke-virtual {v6, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1050
    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1055
    new-instance v2, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-direct {v2}, Lcom/samsung/sgieffect/main/SGIEffectNode;-><init>()V

    .line 1056
    .local v2, "fourth_node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    iget-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v7, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 1057
    new-instance v3, Lcom/samsung/sgieffect/main/CircleLayerImage;

    .end local v3    # "root_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-direct {v3}, Lcom/samsung/sgieffect/main/CircleLayerImage;-><init>()V

    .line 1058
    .restart local v3    # "root_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v7, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 1059
    invoke-virtual {v2, v3}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 1060
    invoke-virtual {v3, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1061
    invoke-virtual {v3, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1062
    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1063
    invoke-virtual {v3, v10}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setChildrenClipState(Z)V

    .line 1066
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .end local v0    # "bottom_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 1067
    .restart local v0    # "bottom_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 1069
    invoke-virtual {v0, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1070
    invoke-virtual {v0, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1071
    invoke-virtual {v0, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1074
    new-instance v6, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .end local v6    # "top_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-direct {v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 1075
    .restart local v6    # "top_fx_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 1077
    invoke-virtual {v6, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 1078
    invoke-virtual {v6, v8, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 1079
    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 1081
    return-void
.end method

.method public createWaveTextureAtlas()V
    .locals 15

    .prologue
    .line 361
    const/16 v7, 0x438

    .line 362
    .local v7, "ATLAS_WIDTH":I
    const/4 v6, 0x0

    .line 364
    .local v6, "ATLAS_HEIGHT":I
    const/4 v11, 0x0

    .local v11, "xPos":I
    const/4 v12, 0x0

    .line 366
    .local v12, "yPos":I
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mImages:[Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    aget-object v8, v0, v1

    .line 369
    .local v8, "baseBitmap":Landroid/graphics/Bitmap;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mImages:[Landroid/graphics/Bitmap;

    array-length v0, v0

    if-lt v10, v0, :cond_0

    .line 383
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    add-int v6, v12, v0

    .line 385
    const/16 v0, 0x438

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v6, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAtlas:Landroid/graphics/Bitmap;

    .line 386
    new-instance v9, Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAtlas:Landroid/graphics/Bitmap;

    invoke-direct {v9, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 388
    .local v9, "canvas":Landroid/graphics/Canvas;
    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 389
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mImages:[Landroid/graphics/Bitmap;

    array-length v0, v0

    new-array v0, v0, [Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mTexCoordData:[Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;

    .line 391
    const/4 v10, 0x0

    :goto_1
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mImages:[Landroid/graphics/Bitmap;

    array-length v0, v0

    if-lt v10, v0, :cond_4

    .line 419
    const/4 v10, 0x0

    :goto_2
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mImages:[Landroid/graphics/Bitmap;

    array-length v0, v0

    if-lt v10, v0, :cond_6

    .line 428
    return-void

    .line 370
    .end local v9    # "canvas":Landroid/graphics/Canvas;
    :cond_0
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mImages:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v10

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 371
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mImages:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v10

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 372
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "All bitmaps MUST have same dimensions !"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375
    :cond_2
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    add-int/2addr v0, v11

    const/16 v1, 0x438

    if-gt v0, v1, :cond_3

    .line 376
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    add-int/2addr v11, v0

    .line 369
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 378
    :cond_3
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    add-int/2addr v12, v0

    .line 379
    const/4 v11, 0x0

    goto :goto_3

    .line 393
    .restart local v9    # "canvas":Landroid/graphics/Canvas;
    :cond_4
    int-to-float v0, v11

    const/high16 v1, 0x44870000    # 1080.0f

    div-float v2, v0, v1

    .line 394
    .local v2, "uLeft":F
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    add-int/2addr v0, v11

    int-to-float v0, v0

    const/high16 v1, 0x44870000    # 1080.0f

    div-float v4, v0, v1

    .line 395
    .local v4, "uRight":F
    int-to-float v0, v12

    int-to-float v1, v6

    div-float v3, v0, v1

    .line 396
    .local v3, "vTop":F
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    add-int/2addr v0, v12

    int-to-float v0, v0

    int-to-float v1, v6

    div-float v5, v0, v1

    .line 398
    .local v5, "vBottom":F
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    add-int/2addr v0, v11

    const/16 v1, 0x438

    if-gt v0, v1, :cond_5

    .line 399
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mImages:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v10

    int-to-float v1, v11

    int-to-float v13, v12

    const/4 v14, 0x0

    invoke-virtual {v9, v0, v1, v13, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 401
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    add-int/2addr v11, v0

    .line 415
    :goto_4
    iget-object v13, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mTexCoordData:[Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;

    new-instance v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$TexCoordData;-><init>(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;FFFF)V

    aput-object v0, v13, v10

    .line 391
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 403
    :cond_5
    const/4 v11, 0x0

    .line 404
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    add-int/2addr v12, v0

    .line 406
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mImages:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v10

    int-to-float v1, v11

    int-to-float v13, v12

    const/4 v14, 0x0

    invoke-virtual {v9, v0, v1, v13, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 409
    int-to-float v0, v11

    const/high16 v1, 0x44870000    # 1080.0f

    div-float v2, v0, v1

    .line 410
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    add-int/2addr v0, v11

    int-to-float v0, v0

    const/high16 v1, 0x44870000    # 1080.0f

    div-float v4, v0, v1

    .line 411
    int-to-float v0, v12

    int-to-float v1, v6

    div-float v3, v0, v1

    .line 412
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    add-int/2addr v0, v12

    int-to-float v0, v0

    int-to-float v1, v6

    div-float v5, v0, v1

    goto :goto_4

    .line 420
    .end local v2    # "uLeft":F
    .end local v3    # "vTop":F
    .end local v4    # "uRight":F
    .end local v5    # "vBottom":F
    :cond_6
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mImages:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v10

    if-eqz v0, :cond_7

    .line 421
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mImages:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v10

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 422
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mImages:[Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    aput-object v1, v0, v10

    .line 419
    :cond_7
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_2
.end method

.method public enableAddBlend(Z)V
    .locals 0
    .param p1, "aValue"    # Z

    .prologue
    .line 590
    iput-boolean p1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mIsAddBlend:Z

    .line 591
    return-void
.end method

.method public enableParticleFitForRectangle()V
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->particleFitForSquare:Z

    .line 355
    return-void
.end method

.method public finalize()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 577
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    if-eqz v0, :cond_0

    .line 578
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/speaker/MathWave;->stop()V

    .line 579
    iput-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    .line 581
    :cond_0
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    if-eqz v0, :cond_1

    .line 582
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->release()V

    .line 583
    iput-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    .line 586
    :cond_1
    invoke-super {p0}, Lcom/samsung/sgieffect/main/SGIEffectView;->finalize()V

    .line 587
    return-void
.end method

.method public getAnimationDuration()I
    .locals 1

    .prologue
    .line 1528
    iget v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAnimationDuration:I

    return v0
.end method

.method public getAnimationSpeed()F
    .locals 1

    .prologue
    .line 1408
    iget v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAnimationSpeed:F

    return v0
.end method

.method public initialize()V
    .locals 2

    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setFpsLimit(I)V

    .line 288
    invoke-super {p0}, Lcom/samsung/sgieffect/main/SGIEffectView;->initialize()V

    .line 290
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationFactory;->createGroupAnimation(Z)Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicAnimationSet:Lcom/samsung/android/sdk/sgi/animation/SGGroupAnimation;

    .line 291
    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationFactory;->createScaleAnimation()Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicScaleAnimation:Lcom/samsung/android/sdk/sgi/animation/SGVector3fAnimation;

    .line 292
    const-string v0, "uLightPower"

    invoke-static {v0}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationFactory;->createCustomFloatAnimation(Ljava/lang/String;)Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicLightPowerAnimation:Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;

    .line 294
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicScaleVectorStart:Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    .line 295
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector3f;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mClassicScaleVectorEnd:Lcom/samsung/android/sdk/sgi/base/SGVector3f;

    .line 296
    return-void
.end method

.method public onDataChanged(Ljava/util/ArrayList;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/sgieffect/speaker/MathWave$Wave;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 596
    .local p1, "waves":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/sgieffect/speaker/MathWave$Wave;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    if-eqz v13, :cond_0

    if-nez p1, :cond_1

    .line 662
    :cond_0
    return-void

    .line 600
    :cond_1
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_0

    .line 604
    const/4 v8, 0x0

    .local v8, "n":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    iget v13, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->maxSlidesCount:I

    if-lt v8, v13, :cond_4

    .line 611
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    iget v13, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideXCount:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    iget v14, v14, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideYCount:I

    mul-int v3, v13, v14

    .line 612
    .local v3, "count":I
    const/4 v12, 0x0

    .line 613
    .local v12, "slideIdx":I
    const/4 v6, 0x0

    .line 615
    .local v6, "isHigh":Z
    const/4 v8, 0x0

    :goto_1
    if-ge v8, v3, :cond_0

    .line 616
    const/4 v10, 0x0

    .line 617
    .local v10, "scale":F
    const/4 v7, 0x0

    .local v7, "k":I
    :goto_2
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lt v7, v13, :cond_6

    .line 627
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    iget-object v13, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->scaleGrid:Ljava/util/ArrayList;

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;

    .line 628
    .local v9, "point":Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;
    const/4 v13, 0x0

    cmpg-float v13, v10, v13

    if-gtz v13, :cond_8

    .line 629
    const/4 v13, 0x0

    iput v13, v9, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;->mHeight:F

    .line 615
    :cond_3
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 605
    .end local v3    # "count":I
    .end local v6    # "isHigh":Z
    .end local v7    # "k":I
    .end local v9    # "point":Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;
    .end local v10    # "scale":F
    .end local v12    # "slideIdx":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    iget-object v13, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideGrid:Ljava/util/ArrayList;

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 606
    .local v11, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v11}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getVisibility()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 607
    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setVisibility(Z)V

    .line 604
    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 618
    .end local v11    # "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .restart local v3    # "count":I
    .restart local v6    # "isHigh":Z
    .restart local v7    # "k":I
    .restart local v10    # "scale":F
    .restart local v12    # "slideIdx":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    if-eqz v13, :cond_7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    iget-object v13, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->scaleGrid:Ljava/util/ArrayList;

    if-eqz v13, :cond_7

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    iget-object v13, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->scaleGrid:Ljava/util/ArrayList;

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_7

    .line 619
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    iget-object v13, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->scaleGrid:Ljava/util/ArrayList;

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;

    .line 620
    .restart local v9    # "point":Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/sgieffect/speaker/MathWave$Wave;

    iget v14, v9, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;->mX:F

    iget v15, v9, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;->mY:F

    invoke-virtual {v13, v14, v15}, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->getScale(FF)F

    move-result v10

    .line 621
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/sgieffect/speaker/MathWave$Wave;

    invoke-virtual {v13}, Lcom/samsung/sgieffect/speaker/MathWave$Wave;->isHigh()Z

    move-result v6

    .line 623
    const/4 v13, 0x0

    cmpl-float v13, v10, v13

    if-gtz v13, :cond_2

    .line 617
    .end local v9    # "point":Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;
    :cond_7
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 633
    .restart local v9    # "point":Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;
    :cond_8
    iget v13, v9, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;->mHeight:F

    sub-float v13, v10, v13

    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v13

    const v14, 0x3a83126f    # 0.001f

    cmpg-float v13, v13, v14

    if-ltz v13, :cond_3

    .line 636
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    iget v13, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->maxSlidesCount:I

    rem-int v12, v8, v13

    .line 637
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    iget-object v13, v13, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideGrid:Ljava/util/ArrayList;

    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 638
    .restart local v11    # "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    const/4 v13, 0x1

    invoke-virtual {v11, v13}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setVisibility(Z)V

    .line 640
    iput v10, v9, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;->mHeight:F

    .line 641
    iget v13, v9, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;->mX:F

    iget v14, v9, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WavePoint;->mY:F

    invoke-virtual {v11, v13, v14}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setPosition(FF)V

    .line 642
    const/high16 v13, 0x3f800000    # 1.0f

    invoke-virtual {v11, v10, v10, v13}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScale(FFF)V

    .line 644
    const/4 v13, 0x4

    new-array v1, v13, [F

    fill-array-data v1, :array_0

    .line 645
    .local v1, "color":[F
    const/4 v5, 0x0

    .line 646
    .local v5, "index":I
    if-eqz v6, :cond_9

    .line 648
    const/4 v13, 0x0

    const/high16 v14, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveColors:[F

    const/16 v16, 0x0

    aget v15, v15, v16

    sub-float/2addr v14, v15

    aput v14, v1, v13

    .line 649
    const/4 v13, 0x1

    const/high16 v14, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveColors:[F

    const/16 v16, 0x1

    aget v15, v15, v16

    sub-float/2addr v14, v15

    aput v14, v1, v13

    .line 650
    const/4 v13, 0x2

    const/high16 v14, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveColors:[F

    const/16 v16, 0x2

    aget v15, v15, v16

    sub-float/2addr v14, v15

    aput v14, v1, v13

    .line 651
    const/high16 v13, 0x437f0000    # 255.0f

    const/4 v14, 0x3

    aget v14, v1, v14

    mul-float/2addr v13, v14

    float-to-int v13, v13

    const/high16 v14, 0x437f0000    # 255.0f

    const/4 v15, 0x0

    aget v15, v1, v15

    mul-float/2addr v14, v15

    float-to-int v14, v14

    const/high16 v15, 0x437f0000    # 255.0f

    const/16 v16, 0x1

    aget v16, v1, v16

    mul-float v15, v15, v16

    float-to-int v15, v15

    const/high16 v16, 0x437f0000    # 255.0f

    const/16 v17, 0x2

    aget v17, v1, v17

    mul-float v16, v16, v17

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    invoke-static/range {v13 .. v16}, Landroid/graphics/Color;->argb(IIII)I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    goto/16 :goto_3

    .line 654
    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveColors:[F

    array-length v13, v13

    div-int/lit8 v13, v13, 0x3

    add-int/lit8 v2, v13, -0x1

    .line 655
    .local v2, "colorCount":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mRandom:Ljava/util/Random;

    invoke-virtual {v13}, Ljava/util/Random;->nextInt()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Math;->abs(I)I

    move-result v13

    rem-int/2addr v13, v2

    add-int/lit8 v5, v13, 0x1

    .line 656
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_4
    const/4 v13, 0x3

    if-lt v4, v13, :cond_a

    .line 659
    const/high16 v13, 0x437f0000    # 255.0f

    const/4 v14, 0x3

    aget v14, v1, v14

    mul-float/2addr v13, v14

    float-to-int v13, v13

    const/high16 v14, 0x437f0000    # 255.0f

    const/4 v15, 0x0

    aget v15, v1, v15

    mul-float/2addr v14, v15

    float-to-int v14, v14

    const/high16 v15, 0x437f0000    # 255.0f

    const/16 v16, 0x1

    aget v16, v1, v16

    mul-float v15, v15, v16

    float-to-int v15, v15

    const/high16 v16, 0x437f0000    # 255.0f

    const/16 v17, 0x2

    aget v17, v1, v17

    mul-float v16, v16, v17

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v16, v0

    invoke-static/range {v13 .. v16}, Landroid/graphics/Color;->argb(IIII)I

    move-result v13

    invoke-virtual {v11, v13}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    goto/16 :goto_3

    .line 657
    :cond_a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveColors:[F

    mul-int/lit8 v14, v5, 0x3

    add-int/2addr v14, v4

    aget v13, v13, v14

    aput v13, v1, v4

    .line 656
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 644
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/16 v3, 0xc

    .line 301
    iget v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 302
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v1, v3, :cond_1

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 306
    :cond_1
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v1, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 307
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 308
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mChangedMaskType:Z

    goto :goto_0

    .line 312
    .end local v0    # "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    :cond_2
    iget v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 313
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    if-eqz v1, :cond_0

    .line 314
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    invoke-virtual {v1}, Lcom/samsung/sgieffect/speaker/MathWave;->stop()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 322
    iget v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 323
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/speaker/MathWave;->start()V

    .line 327
    :cond_0
    return-void
.end method

.method public releaseWaveAtlasBitmap()V
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAtlas:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAtlas:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 433
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAtlas:Landroid/graphics/Bitmap;

    .line 435
    :cond_0
    return-void
.end method

.method public setAnimationDuration(I)V
    .locals 0
    .param p1, "duration"    # I

    .prologue
    .line 1394
    iput p1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAnimationDuration:I

    .line 1395
    return-void
.end method

.method public setAnimationSpeed(F)V
    .locals 0
    .param p1, "speed"    # F
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1405
    return-void
.end method

.method public setDecibel(F)V
    .locals 3
    .param p1, "fDB"    # F

    .prologue
    .line 1507
    const/high16 v1, 0x41c80000    # 25.0f

    add-float/2addr v1, p1

    const/high16 v2, 0x42200000    # 40.0f

    div-float v0, v1, v2

    .line 1508
    .local v0, "filteredDB":F
    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mNextFrequencyDepth:F

    .line 1509
    iget v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAnimationDuration:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAnimationSpeed:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-direct {p0, v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setPannelDepthAnimation(I)V

    .line 1510
    iget v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mNextFrequencyDepth:F

    iput v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mLastFrequencyDepth:F

    .line 1511
    return-void
.end method

.method public setDecibelRatio(F)V
    .locals 3
    .param p1, "fDBRatio"    # F

    .prologue
    .line 1514
    move v0, p1

    .line 1515
    .local v0, "filteredDB":F
    iget v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1516
    move v0, p1

    .line 1517
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 1518
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1522
    :cond_0
    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mNextFrequencyDepth:F

    .line 1523
    iget v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAnimationDuration:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAnimationSpeed:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-direct {p0, v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setPannelDepthAnimation(I)V

    .line 1524
    iget v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mNextFrequencyDepth:F

    iput v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mLastFrequencyDepth:F

    .line 1525
    return-void
.end method

.method public setMaskInfomation(ILcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;)V
    .locals 2
    .param p1, "aMaskType"    # I
    .param p2, "info"    # Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .prologue
    .line 843
    packed-switch p1, :pswitch_data_0

    .line 962
    :goto_0
    return-void

    .line 845
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    if-nez v0, :cond_0

    .line 846
    new-instance v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    invoke-direct {v0, p0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;-><init>(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)V

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 848
    :cond_0
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomWidth:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomWidth:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomHeight:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomHeight:F

    .line 849
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxWidth:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxWidth:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxHeight:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxHeight:F

    .line 850
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopWidth:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopWidth:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopHeight:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopHeight:F

    .line 851
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomBase:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomBase:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopBase:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopBase:Landroid/graphics/Bitmap;

    .line 852
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    .line 853
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    .line 854
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    .line 855
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    .line 856
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    .line 857
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    .line 858
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    .line 860
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    .line 861
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    .line 862
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    .line 863
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskA:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    goto/16 :goto_0

    .line 868
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    if-nez v0, :cond_1

    .line 869
    new-instance v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    invoke-direct {v0, p0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;-><init>(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)V

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 871
    :cond_1
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomWidth:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomWidth:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomHeight:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomHeight:F

    .line 872
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxWidth:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxWidth:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxHeight:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxHeight:F

    .line 873
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopWidth:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopWidth:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopHeight:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopHeight:F

    .line 874
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomBase:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomBase:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopBase:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopBase:Landroid/graphics/Bitmap;

    .line 875
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    .line 876
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    .line 877
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    .line 878
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    .line 879
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    .line 880
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    .line 881
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    .line 883
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    .line 884
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    .line 885
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    .line 886
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskB:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    goto/16 :goto_0

    .line 891
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    if-nez v0, :cond_2

    .line 892
    new-instance v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    invoke-direct {v0, p0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;-><init>(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)V

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 894
    :cond_2
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomWidth:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomWidth:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomHeight:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomHeight:F

    .line 895
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxWidth:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxWidth:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxHeight:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxHeight:F

    .line 896
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopWidth:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopWidth:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopHeight:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopHeight:F

    .line 897
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomBase:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomBase:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopBase:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopBase:Landroid/graphics/Bitmap;

    .line 898
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    .line 899
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    .line 900
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    .line 901
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    .line 902
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    .line 903
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    .line 904
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    .line 906
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    .line 907
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    .line 908
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    .line 909
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskC:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    goto/16 :goto_0

    .line 914
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    if-nez v0, :cond_3

    .line 915
    new-instance v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    invoke-direct {v0, p0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;-><init>(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)V

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 917
    :cond_3
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomWidth:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomWidth:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomHeight:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomHeight:F

    .line 918
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxWidth:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxWidth:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxHeight:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxHeight:F

    .line 919
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopWidth:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopWidth:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopHeight:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopHeight:F

    .line 920
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomBase:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomBase:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopBase:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopBase:Landroid/graphics/Bitmap;

    .line 921
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    .line 922
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    .line 923
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    .line 924
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    .line 925
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    .line 926
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    .line 927
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    .line 929
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    .line 930
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    .line 931
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    .line 932
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskD:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    goto/16 :goto_0

    .line 937
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    if-nez v0, :cond_4

    .line 938
    new-instance v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    invoke-direct {v0, p0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;-><init>(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)V

    iput-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    .line 940
    :cond_4
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomWidth:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomWidth:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomHeight:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomHeight:F

    .line 941
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxWidth:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxWidth:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxHeight:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxHeight:F

    .line 942
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopWidth:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopWidth:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopHeight:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopHeight:F

    .line 943
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomBase:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomBase:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopBase:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopBase:Landroid/graphics/Bitmap;

    .line 944
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    .line 945
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    .line 946
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Top:Landroid/graphics/Bitmap;

    .line 947
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    .line 948
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    .line 949
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    .line 950
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget-object v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    .line 952
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    .line 953
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    .line 954
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    .line 955
    iget-object v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mInfoOfMaskE:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    iget v1, p2, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    goto/16 :goto_0

    .line 843
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public setMaskType(ILandroid/graphics/Bitmap;Z)V
    .locals 4
    .param p1, "aMaskType"    # I
    .param p2, "aMaskBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "isSetBitmap"    # Z

    .prologue
    const/16 v3, 0xc

    .line 804
    iput p1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMaskType:I

    .line 806
    iget-boolean v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mChangedMaskType:Z

    if-eqz v2, :cond_1

    .line 807
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v2, v3, :cond_0

    .line 823
    :goto_0
    return-void

    .line 811
    :cond_0
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v2, v3}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v2}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v1

    .line 813
    .local v1, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-static {}, Lcom/samsung/android/sdk/sgi/animation/SGAnimationFactory;->createOpacityAnimation()Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;

    move-result-object v0

    .line 814
    .local v0, "keyOpacityAni":Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;
    const/high16 v2, 0x43af0000    # 350.0f

    iget v3, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAnimationSpeed:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->setDuration(I)V

    .line 815
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->setStartValue(F)V

    .line 816
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;->setEndValue(F)V

    .line 818
    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->addAnimation(Lcom/samsung/android/sdk/sgi/animation/SGAnimation;)I

    .line 820
    .end local v0    # "keyOpacityAni":Lcom/samsung/android/sdk/sgi/animation/SGFloatAnimation;
    .end local v1    # "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mChangedMaskType:Z

    .line 822
    iget v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMaskType:I

    invoke-direct {p0, v2, p2, p3}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setOthersCircle(ILandroid/graphics/Bitmap;Z)V

    goto :goto_0
.end method

.method public setMovementScale(F)V
    .locals 0
    .param p1, "scale"    # F
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1400
    return-void
.end method

.method public setMusiBase(F)V
    .locals 0
    .param p1, "aValue"    # F

    .prologue
    .line 826
    iput p1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicBase:F

    .line 827
    return-void
.end method

.method public setMusiHigh(F)V
    .locals 0
    .param p1, "aValue"    # F

    .prologue
    .line 838
    iput p1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicHigh:F

    .line 839
    return-void
.end method

.method public setMusiLow(F)V
    .locals 0
    .param p1, "aValue"    # F

    .prologue
    .line 830
    iput p1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicLow:F

    .line 831
    return-void
.end method

.method public setMusiMid(F)V
    .locals 0
    .param p1, "aValue"    # F

    .prologue
    .line 834
    iput p1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMusicMid:F

    .line 835
    return-void
.end method

.method public setSkinMode(I)V
    .locals 6
    .param p1, "aSkinMode"    # I

    .prologue
    const/16 v5, 0xc

    const/4 v4, 0x0

    .line 757
    iput-boolean v4, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mChangedMaskType:Z

    .line 759
    iget v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    if-eq v2, p1, :cond_5

    .line 761
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    if-eqz v2, :cond_0

    .line 762
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    invoke-virtual {v2}, Lcom/samsung/sgieffect/speaker/MathWave;->stop()V

    .line 765
    :cond_0
    iget v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 768
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-lt v2, v5, :cond_1

    .line 769
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v2, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v2}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v1

    .line 770
    .local v1, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 773
    .end local v1    # "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    :cond_1
    invoke-direct {p0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->releaseFunkySkin()V

    .line 776
    :cond_2
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAnimationSpeed:F

    .line 779
    if-nez p1, :cond_3

    .line 780
    const/high16 v2, 0x3fa00000    # 1.25f

    iput v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mAnimationSpeed:F

    .line 783
    :cond_3
    iput p1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    .line 787
    iget v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    .line 788
    invoke-direct {p0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->buildWaveSkin()V

    .line 790
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    if-nez v2, :cond_4

    .line 791
    new-instance v2, Lcom/samsung/sgieffect/speaker/MathWave;

    invoke-direct {v2}, Lcom/samsung/sgieffect/speaker/MathWave;-><init>()V

    iput-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    .line 794
    :cond_4
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v2, v4}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 795
    .local v0, "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getRegion()Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {v0}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getRegion()Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    add-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/samsung/sgieffect/speaker/MathWave;->setMaxRangeOfRadius(F)V

    .line 796
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    invoke-virtual {v2, p0}, Lcom/samsung/sgieffect/speaker/MathWave;->setListener(Lcom/samsung/sgieffect/speaker/MathWave$MathWaveListener;)V

    .line 797
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    const/16 v3, 0x12

    invoke-virtual {v2, v3}, Lcom/samsung/sgieffect/speaker/MathWave;->setMaxFps(I)V

    .line 798
    iget-object v2, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mMathWave:Lcom/samsung/sgieffect/speaker/MathWave;

    invoke-virtual {v2}, Lcom/samsung/sgieffect/speaker/MathWave;->start()V

    .line 801
    .end local v0    # "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    :cond_5
    return-void
.end method

.method public setTimeDelta(J)V
    .locals 11
    .param p1, "aTimeDelta"    # J

    .prologue
    .line 1412
    iget v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 1416
    const/high16 v8, 0x41100000    # 9.0f

    const/high16 v9, 0x447a0000    # 1000.0f

    long-to-float v10, p1

    div-float/2addr v9, v10

    div-float v0, v8, v9

    .line 1417
    .local v0, "deltaZ":F
    iget v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mRotateZ:F

    add-float v4, v8, v0

    .line 1418
    .local v4, "rotateZ":F
    const/high16 v8, 0x43b40000    # 360.0f

    cmpl-float v8, v4, v8

    if-lez v8, :cond_2

    .line 1419
    const/high16 v8, 0x43b40000    # 360.0f

    sub-float/2addr v4, v8

    .line 1427
    :cond_0
    :goto_0
    const v8, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v8, v4

    const/high16 v9, 0x43340000    # 180.0f

    div-float/2addr v8, v9

    iput v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mRotateZ:F

    .line 1431
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v8

    const/16 v9, 0xa

    if-gt v8, v9, :cond_3

    .line 1504
    .end local v0    # "deltaZ":F
    .end local v4    # "rotateZ":F
    :cond_1
    :goto_1
    return-void

    .line 1421
    .restart local v0    # "deltaZ":F
    .restart local v4    # "rotateZ":F
    :cond_2
    const/high16 v8, -0x3c4c0000    # -360.0f

    cmpg-float v8, v4, v8

    if-gez v8, :cond_0

    .line 1422
    const/high16 v8, 0x43b40000    # 360.0f

    add-float/2addr v4, v8

    goto :goto_0

    .line 1432
    :cond_3
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    const/16 v9, 0xa

    invoke-virtual {v8, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v8}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v6

    .line 1435
    .local v6, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mRotateZ:F

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationZ(F)V

    .line 1437
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v8

    const/16 v9, 0x8

    if-le v8, v9, :cond_1

    .line 1438
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v8}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v6

    .line 1440
    iget v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mRotateZ:F

    neg-float v8, v8

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationZ(F)V

    .line 1442
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v8

    const/4 v9, 0x7

    if-le v8, v9, :cond_1

    .line 1443
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    const/4 v9, 0x7

    invoke-virtual {v8, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v8}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v6

    .line 1445
    iget v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mRotateZ:F

    invoke-virtual {v6, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationZ(F)V

    .line 1447
    const/4 v3, 0x0

    .line 1448
    .local v3, "info":Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;
    invoke-direct {p0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->getCurrentMaskInfo()Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    move-result-object v3

    .line 1450
    if-eqz v3, :cond_1

    .line 1455
    iget v8, v3, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    const/high16 v9, 0x42700000    # 60.0f

    div-float/2addr v8, v9

    const/high16 v9, 0x447a0000    # 1000.0f

    long-to-float v10, p1

    div-float/2addr v9, v10

    div-float v0, v8, v9

    .line 1456
    iget v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mOtherFirstRotateZ:F

    add-float v4, v8, v0

    .line 1457
    const/high16 v8, 0x43b40000    # 360.0f

    cmpl-float v8, v4, v8

    if-lez v8, :cond_8

    .line 1458
    const/high16 v8, 0x43b40000    # 360.0f

    sub-float/2addr v4, v8

    .line 1464
    :cond_4
    :goto_2
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    const/4 v9, 0x3

    invoke-virtual {v8, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v8}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1465
    .local v1, "first_other_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mOtherFirstRotateZ:F

    const v9, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v8, v9

    const/high16 v9, 0x43340000    # 180.0f

    div-float/2addr v8, v9

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationZ(F)V

    .line 1468
    iget v8, v3, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    const/high16 v9, 0x42700000    # 60.0f

    div-float/2addr v8, v9

    const/high16 v9, 0x447a0000    # 1000.0f

    long-to-float v10, p1

    div-float/2addr v9, v10

    div-float v0, v8, v9

    .line 1469
    iget v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mOtherSecondRotateZ:F

    add-float v4, v8, v0

    .line 1470
    const/high16 v8, 0x43b40000    # 360.0f

    cmpl-float v8, v4, v8

    if-lez v8, :cond_9

    .line 1471
    const/high16 v8, 0x43b40000    # 360.0f

    sub-float/2addr v4, v8

    .line 1477
    :cond_5
    :goto_3
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v8}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1478
    .local v5, "second_other_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mOtherSecondRotateZ:F

    const v9, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v8, v9

    const/high16 v9, 0x43340000    # 180.0f

    div-float/2addr v8, v9

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationZ(F)V

    .line 1481
    iget v8, v3, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    const/high16 v9, 0x42700000    # 60.0f

    div-float/2addr v8, v9

    const/high16 v9, 0x447a0000    # 1000.0f

    long-to-float v10, p1

    div-float/2addr v9, v10

    div-float v0, v8, v9

    .line 1482
    iget v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mOtherThirdRotateZ:F

    add-float v4, v8, v0

    .line 1483
    const/high16 v8, 0x43b40000    # 360.0f

    cmpl-float v8, v4, v8

    if-lez v8, :cond_a

    .line 1484
    const/high16 v8, 0x43b40000    # 360.0f

    sub-float/2addr v4, v8

    .line 1489
    :cond_6
    :goto_4
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    const/4 v9, 0x5

    invoke-virtual {v8, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v8}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1490
    .local v7, "third_other_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mOtherThirdRotateZ:F

    const v9, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v8, v9

    const/high16 v9, 0x43340000    # 180.0f

    div-float/2addr v8, v9

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationZ(F)V

    .line 1493
    iget v8, v3, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    const/high16 v9, 0x42700000    # 60.0f

    div-float/2addr v8, v9

    const/high16 v9, 0x447a0000    # 1000.0f

    long-to-float v10, p1

    div-float/2addr v9, v10

    div-float v0, v8, v9

    .line 1494
    iget v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mOtherFourthRotateZ:F

    add-float v4, v8, v0

    .line 1495
    const/high16 v8, 0x43b40000    # 360.0f

    cmpl-float v8, v4, v8

    if-lez v8, :cond_b

    .line 1496
    const/high16 v8, 0x43b40000    # 360.0f

    sub-float/2addr v4, v8

    .line 1501
    :cond_7
    :goto_5
    iget-object v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    const/4 v9, 0x6

    invoke-virtual {v8, v9}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v8}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 1502
    .local v2, "fourth_other_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    iget v8, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mOtherFourthRotateZ:F

    const v9, 0x40490fdb    # (float)Math.PI

    mul-float/2addr v8, v9

    const/high16 v9, 0x43340000    # 180.0f

    div-float/2addr v8, v9

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationZ(F)V

    goto/16 :goto_1

    .line 1460
    .end local v1    # "first_other_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .end local v2    # "fourth_other_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .end local v5    # "second_other_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .end local v7    # "third_other_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    :cond_8
    const/high16 v8, -0x3c4c0000    # -360.0f

    cmpg-float v8, v4, v8

    if-gez v8, :cond_4

    .line 1461
    const/high16 v8, 0x43b40000    # 360.0f

    add-float/2addr v4, v8

    goto/16 :goto_2

    .line 1473
    .restart local v1    # "first_other_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    :cond_9
    const/high16 v8, -0x3c4c0000    # -360.0f

    cmpg-float v8, v4, v8

    if-gez v8, :cond_5

    .line 1474
    const/high16 v8, 0x43b40000    # 360.0f

    add-float/2addr v4, v8

    goto/16 :goto_3

    .line 1486
    .restart local v5    # "second_other_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    :cond_a
    const/high16 v8, -0x3c4c0000    # -360.0f

    cmpg-float v8, v4, v8

    if-gez v8, :cond_6

    .line 1487
    const/high16 v8, 0x43b40000    # 360.0f

    add-float/2addr v4, v8

    goto :goto_4

    .line 1498
    .restart local v7    # "third_other_slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    :cond_b
    const/high16 v8, -0x3c4c0000    # -360.0f

    cmpg-float v8, v4, v8

    if-gez v8, :cond_7

    .line 1499
    const/high16 v8, 0x43b40000    # 360.0f

    add-float/2addr v4, v8

    goto :goto_5
.end method

.method public setWaveColors([I)V
    .locals 13
    .param p1, "colors"    # [I

    .prologue
    .line 701
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveColors:[F

    .line 702
    array-length v7, p1

    new-array v7, v7, [F

    iput-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveColors:[F

    .line 703
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v7, p1

    if-lt v3, v7, :cond_1

    .line 707
    iget v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mCurrentSkin:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_0

    .line 709
    iget-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 710
    .local v6, "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    invoke-virtual {v6}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 712
    .local v0, "baseSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    const/4 v4, 0x0

    .line 713
    .local v4, "index":I
    const/4 v7, 0x4

    new-array v1, v7, [F

    fill-array-data v1, :array_0

    .line 714
    .local v1, "color":[F
    const/4 v7, 0x0

    const/4 v8, 0x0

    aput v8, v1, v7

    .line 715
    const/4 v7, 0x1

    const/4 v8, 0x0

    aput v8, v1, v7

    .line 716
    const/4 v7, 0x2

    const/4 v8, 0x0

    aput v8, v1, v7

    .line 719
    const/high16 v7, 0x437f0000    # 255.0f

    const/4 v8, 0x3

    aget v8, v1, v8

    mul-float/2addr v7, v8

    float-to-int v7, v7

    const/high16 v8, 0x437f0000    # 255.0f

    const/4 v9, 0x0

    aget v9, v1, v9

    mul-float/2addr v8, v9

    float-to-int v8, v8

    const/high16 v9, 0x437f0000    # 255.0f

    const/4 v10, 0x1

    aget v10, v1, v10

    mul-float/2addr v9, v10

    float-to-int v9, v9

    const/high16 v10, 0x437f0000    # 255.0f

    const/4 v11, 0x2

    aget v11, v1, v11

    mul-float/2addr v10, v11

    float-to-int v10, v10

    invoke-static {v7, v8, v9, v10}, Landroid/graphics/Color;->argb(IIII)I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 721
    const/4 v5, 0x0

    .local v5, "n":I
    :goto_1
    iget-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    iget-object v7, v7, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideGrid:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lt v5, v7, :cond_2

    .line 731
    .end local v0    # "baseSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .end local v1    # "color":[F
    .end local v4    # "index":I
    .end local v5    # "n":I
    .end local v6    # "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    :cond_0
    return-void

    .line 704
    :cond_1
    iget-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveColors:[F

    aget v8, p1, v3

    int-to-float v8, v8

    const/high16 v9, 0x437f0000    # 255.0f

    div-float/2addr v8, v9

    aput v8, v7, v3

    .line 703
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 722
    .restart local v0    # "baseSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .restart local v1    # "color":[F
    .restart local v4    # "index":I
    .restart local v5    # "n":I
    .restart local v6    # "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    :cond_2
    iget-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveColors:[F

    array-length v7, v7

    div-int/lit8 v7, v7, 0x3

    add-int/lit8 v2, v7, -0x1

    .line 723
    .local v2, "colorCount":I
    iget-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mRandom:Ljava/util/Random;

    invoke-virtual {v7}, Ljava/util/Random;->nextInt()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    rem-int/2addr v7, v2

    add-int/lit8 v4, v7, 0x1

    .line 724
    const/4 v3, 0x0

    :goto_2
    const/4 v7, 0x3

    if-lt v3, v7, :cond_3

    .line 728
    iget-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveField:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;

    iget-object v7, v7, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$WaveField;->slideGrid:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    const/high16 v8, 0x437f0000    # 255.0f

    const/4 v9, 0x3

    aget v9, v1, v9

    mul-float/2addr v8, v9

    float-to-int v8, v8

    const/high16 v9, 0x437f0000    # 255.0f

    const/4 v10, 0x0

    aget v10, v1, v10

    mul-float/2addr v9, v10

    float-to-int v9, v9

    const/high16 v10, 0x437f0000    # 255.0f

    const/4 v11, 0x1

    aget v11, v1, v11

    mul-float/2addr v10, v11

    float-to-int v10, v10

    const/high16 v11, 0x437f0000    # 255.0f

    const/4 v12, 0x2

    aget v12, v1, v12

    mul-float/2addr v11, v12

    float-to-int v11, v11

    invoke-static {v8, v9, v10, v11}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 721
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 726
    :cond_3
    iget-object v7, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveColors:[F

    mul-int/lit8 v8, v4, 0x3

    add-int/2addr v8, v3

    aget v7, v7, v8

    aput v7, v1, v3

    .line 724
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 713
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public setWaveHidden()V
    .locals 3

    .prologue
    .line 688
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 689
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 690
    return-void
.end method

.method public setWaveLandscape()V
    .locals 1

    .prologue
    .line 738
    const/16 v0, 0x1c

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->WAVE_COUNT_X:I

    .line 739
    const/16 v0, 0xe

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->WAVE_COUNT_Y:I

    .line 741
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveOrientLandscape:Z

    .line 742
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveOrientPortraits:Z

    .line 743
    return-void
.end method

.method public setWaveMaxSlide(I)V
    .locals 0
    .param p1, "slideCount"    # I

    .prologue
    .line 350
    iput p1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->maxSlidesCount:I

    .line 351
    return-void
.end method

.method public setWavePortraits()V
    .locals 1

    .prologue
    .line 750
    const/16 v0, 0xe

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->WAVE_COUNT_X:I

    .line 751
    const/16 v0, 0x1c

    iput v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->WAVE_COUNT_Y:I

    .line 753
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveOrientPortraits:Z

    .line 754
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveOrientLandscape:Z

    .line 755
    return-void
.end method

.method public setWaveShow()V
    .locals 3

    .prologue
    .line 694
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mPannelNodes:Ljava/util/Vector;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 695
    .local v0, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 696
    return-void
.end method

.method public setWaveTextures([Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bitmaps"    # [Landroid/graphics/Bitmap;

    .prologue
    .line 340
    array-length v1, p1

    new-array v1, v1, [Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mImages:[Landroid/graphics/Bitmap;

    .line 341
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-lt v0, v1, :cond_0

    .line 346
    array-length v1, p1

    iput v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mWaveImagesCount:I

    .line 347
    return-void

    .line 342
    :cond_0
    iget-object v1, p0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->mImages:[Landroid/graphics/Bitmap;

    aget-object v2, p1, v0

    aput-object v2, v1, v0

    .line 341
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
