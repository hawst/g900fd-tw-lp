.class public Lcom/samsung/sgieffect/main/CircleLayerImage;
.super Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
.source "CircleLayerImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sgieffect/main/CircleLayerImage$CircleGeometryGenerator;
    }
.end annotation


# static fields
.field private static final CIRCLE_DENSITY:I = 0x3c


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 21
    new-instance v0, Lcom/samsung/sgieffect/main/CircleLayerImage$CircleGeometryGenerator;

    const/16 v1, 0x3c

    invoke-direct {v0, p0, v1}, Lcom/samsung/sgieffect/main/CircleLayerImage$CircleGeometryGenerator;-><init>(Lcom/samsung/sgieffect/main/CircleLayerImage;I)V

    .line 22
    .local v0, "innerGG":Lcom/samsung/sgieffect/main/CircleLayerImage$CircleGeometryGenerator;
    invoke-virtual {p0, v0}, Lcom/samsung/sgieffect/main/CircleLayerImage;->setGeometryGenerator(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V

    .line 23
    return-void
.end method
