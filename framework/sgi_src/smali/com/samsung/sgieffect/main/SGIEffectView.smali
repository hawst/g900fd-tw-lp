.class public Lcom/samsung/sgieffect/main/SGIEffectView;
.super Lcom/samsung/android/sdk/sgi/vi/SGView;
.source "SGIEffectView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;
    }
.end annotation


# static fields
.field protected static mHideAnimationListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

.field protected static mListener:Lcom/samsung/sgieffect/main/SGIEffectListener;

.field private static mTypeExtraState:I


# instance fields
.field private final BUILD_TIME:Ljava/lang/String;

.field private final BUILD_VERSION:Ljava/lang/String;

.field public final MSG_SURFACE_RESIZE:I

.field protected mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

.field private mCurrentRootPosition:Landroid/graphics/PointF;

.field private mEndTime:J

.field private mFirstRenderFinished:Z

.field private mInitializationDuration:J

.field protected mIsHidden:Z

.field protected mLastHoveredNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

.field protected mLastPressedNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

.field protected mMaskBitmapTexture2DProperty:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

.field protected mNodes:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/sgieffect/main/SGIEffectNode;",
            ">;"
        }
    .end annotation
.end field

.field protected mPannelNodes:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/sgieffect/main/SGIEffectNode;",
            ">;"
        }
    .end annotation
.end field

.field protected mPrevSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

.field protected mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

.field protected mScrollY:I

.field protected mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

.field private mStartTime:J

.field protected mTopOffset:I

.field protected mTouchDispatcher:Landroid/view/View;

.field private mViewHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/sgieffect/main/SGIEffectView;->mListener:Lcom/samsung/sgieffect/main/SGIEffectListener;

    .line 613
    new-instance v0, Lcom/samsung/sgieffect/main/SGIEffectView$1;

    invoke-direct {v0}, Lcom/samsung/sgieffect/main/SGIEffectView$1;-><init>()V

    sput-object v0, Lcom/samsung/sgieffect/main/SGIEffectView;->mHideAnimationListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v7, 0x44340000    # 720.0f

    const/4 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 80
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGView;-><init>(Landroid/content/Context;)V

    .line 37
    const-string v0, "(2014/02/04)"

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->BUILD_VERSION:Ljava/lang/String;

    .line 38
    const-string v0, "(Feb 04 TIME:16:30)"

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->BUILD_TIME:Ljava/lang/String;

    .line 40
    const v0, 0x9001

    iput v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->MSG_SURFACE_RESIZE:I

    .line 49
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/high16 v1, 0x44a00000    # 1280.0f

    invoke-direct {v0, v7, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 50
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/high16 v1, 0x44a00000    # 1280.0f

    invoke-direct {v0, v7, v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mPrevSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .line 51
    iput-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 52
    iput-boolean v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mIsHidden:Z

    .line 53
    iput-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    .line 55
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    .line 56
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mPannelNodes:Ljava/util/Vector;

    .line 58
    iput-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastPressedNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 59
    iput-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastHoveredNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 60
    iput-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mTouchDispatcher:Landroid/view/View;

    .line 62
    iput-wide v4, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mStartTime:J

    .line 63
    iput-wide v4, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mEndTime:J

    .line 64
    iput-wide v4, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mInitializationDuration:J

    .line 72
    iput v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mScrollY:I

    .line 73
    iput v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mTopOffset:I

    .line 75
    iput-boolean v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mFirstRenderFinished:Z

    .line 77
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    sget-object v1, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;)V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mMaskBitmapTexture2DProperty:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    .line 653
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v6, v6}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCurrentRootPosition:Landroid/graphics/PointF;

    .line 784
    new-instance v0, Lcom/samsung/sgieffect/main/SGIEffectView$2;

    invoke-direct {v0, p0}, Lcom/samsung/sgieffect/main/SGIEffectView$2;-><init>(Lcom/samsung/sgieffect/main/SGIEffectView;)V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mViewHandler:Landroid/os/Handler;

    .line 82
    const-string v0, "SGIEffect"

    const-string v1, "SGI EFFECT-JAR-RELEASE DATE(2014/02/04)"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    const-string v0, "SGIEffect"

    const-string v1, "SGI EFFECT-JAR-BUILD TIME  (Feb 04 TIME:16:30)"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-virtual {p0}, Lcom/samsung/sgieffect/main/SGIEffectView;->initializePostEffect()V

    .line 88
    invoke-virtual {p0}, Lcom/samsung/sgieffect/main/SGIEffectView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    new-instance v1, Lcom/samsung/sgieffect/main/SGIEffectView$3;

    invoke-direct {v1, p0}, Lcom/samsung/sgieffect/main/SGIEffectView$3;-><init>(Lcom/samsung/sgieffect/main/SGIEffectView;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setDrawFrameListener(Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;)V

    .line 120
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/sgieffect/main/SGIEffectView;->setZOrderOnTop(Z)V

    .line 122
    invoke-virtual {p0}, Lcom/samsung/sgieffect/main/SGIEffectView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    new-instance v1, Lcom/samsung/sgieffect/main/SGIEffectView$4;

    invoke-direct {v1, p0}, Lcom/samsung/sgieffect/main/SGIEffectView$4;-><init>(Lcom/samsung/sgieffect/main/SGIEffectView;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setSizeChangeListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;)V

    .line 133
    invoke-virtual {p0}, Lcom/samsung/sgieffect/main/SGIEffectView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    new-instance v1, Lcom/samsung/sgieffect/main/SGIEffectView$5;

    invoke-direct {v1, p0}, Lcom/samsung/sgieffect/main/SGIEffectView$5;-><init>(Lcom/samsung/sgieffect/main/SGIEffectView;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addChangesDrawnListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;)V

    .line 144
    return-void
.end method

.method static synthetic access$0()I
    .locals 1

    .prologue
    .line 69
    sget v0, Lcom/samsung/sgieffect/main/SGIEffectView;->mTypeExtraState:I

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/sgieffect/main/SGIEffectView;)Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mFirstRenderFinished:Z

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/sgieffect/main/SGIEffectView;Z)V
    .locals 0

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mFirstRenderFinished:Z

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/sgieffect/main/SGIEffectView;J)V
    .locals 1

    .prologue
    .line 63
    iput-wide p1, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mEndTime:J

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/sgieffect/main/SGIEffectView;)J
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mEndTime:J

    return-wide v0
.end method

.method static synthetic access$5(Lcom/samsung/sgieffect/main/SGIEffectView;)J
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mStartTime:J

    return-wide v0
.end method

.method static synthetic access$6(Lcom/samsung/sgieffect/main/SGIEffectView;J)V
    .locals 1

    .prologue
    .line 64
    iput-wide p1, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mInitializationDuration:J

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/sgieffect/main/SGIEffectView;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 784
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mViewHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private processOnHover(Landroid/view/MotionEvent;FF)V
    .locals 6
    .param p1, "inputEvent"    # Landroid/view/MotionEvent;
    .param p2, "x"    # F
    .param p3, "y"    # F

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0xa

    .line 495
    if-nez p1, :cond_1

    .line 496
    const-string v2, "SGI Effect View"

    const-string v3, "Motion event is null, please check touch event handler"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    :cond_0
    :goto_0
    return-void

    .line 501
    :cond_1
    invoke-virtual {p0, p2, p3}, Lcom/samsung/sgieffect/main/SGIEffectView;->getIntersectedNode(FF)Lcom/samsung/sgieffect/main/SGIEffectNode;

    move-result-object v1

    .line 503
    .local v1, "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    if-eqz v1, :cond_7

    .line 504
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v4, :cond_3

    .line 505
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastHoveredNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    if-eqz v2, :cond_2

    .line 506
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastHoveredNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastHoveredNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v3}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getIndex()I

    move-result v3

    invoke-virtual {v2, v3, v4, p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->onHovering(IILandroid/view/MotionEvent;)Z

    .line 507
    iput-object v5, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastHoveredNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 509
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getIndex()I

    move-result v2

    invoke-virtual {v1, v2, v4, p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->onHovering(IILandroid/view/MotionEvent;)Z

    goto :goto_0

    .line 511
    :cond_3
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastHoveredNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    if-eq v2, v1, :cond_5

    const/4 v0, 0x1

    .line 512
    .local v0, "hoverChanged":Z
    :goto_1
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastHoveredNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    if-eqz v2, :cond_4

    if-eqz v0, :cond_4

    .line 513
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastHoveredNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastHoveredNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v3}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getIndex()I

    move-result v3

    invoke-virtual {v2, v3, v4, p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->onHovering(IILandroid/view/MotionEvent;)Z

    .line 515
    :cond_4
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastHoveredNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 516
    invoke-virtual {v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getIndex()I

    move-result v3

    if-eqz v0, :cond_6

    const/16 v2, 0x9

    :goto_2
    invoke-virtual {v1, v3, v2, p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->onHovering(IILandroid/view/MotionEvent;)Z

    goto :goto_0

    .line 511
    .end local v0    # "hoverChanged":Z
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 516
    .restart local v0    # "hoverChanged":Z
    :cond_6
    const/4 v2, 0x7

    goto :goto_2

    .line 519
    .end local v0    # "hoverChanged":Z
    :cond_7
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastHoveredNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    if-eqz v2, :cond_0

    .line 520
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastHoveredNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastHoveredNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v3}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getIndex()I

    move-result v3

    invoke-virtual {v2, v3, v4, p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->onHovering(IILandroid/view/MotionEvent;)Z

    .line 521
    iput-object v5, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastHoveredNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    goto :goto_0
.end method

.method private processOnTouch(Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;Landroid/view/MotionEvent;FFI)Z
    .locals 5
    .param p1, "type"    # Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;
    .param p2, "inputEvent"    # Landroid/view/MotionEvent;
    .param p3, "x"    # F
    .param p4, "y"    # F
    .param p5, "index"    # I

    .prologue
    const/4 v2, 0x0

    .line 541
    if-nez p2, :cond_1

    .line 542
    const-string v3, "SGI Effect View"

    const-string v4, "Motion event is null, please check touch event handler"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    :cond_0
    :goto_0
    return v2

    .line 551
    :cond_1
    iget v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mScrollY:I

    iget v4, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mTopOffset:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    sub-float v3, p4, v3

    invoke-virtual {p0, p3, v3}, Lcom/samsung/sgieffect/main/SGIEffectView;->getIntersectedNode(FF)Lcom/samsung/sgieffect/main/SGIEffectNode;

    move-result-object v1

    .line 554
    .local v1, "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    sput v2, Lcom/samsung/sgieffect/main/SGIEffectView;->mTypeExtraState:I

    .line 555
    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    if-eqz v3, :cond_2

    .line 556
    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    invoke-virtual {v3, v1, p2, p5}, Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;->callbackExtraTask(Lcom/samsung/sgieffect/main/SGIEffectNode;Landroid/view/MotionEvent;I)I

    move-result v3

    sput v3, Lcom/samsung/sgieffect/main/SGIEffectView;->mTypeExtraState:I

    .line 558
    :cond_2
    if-eqz v1, :cond_0

    .line 562
    invoke-virtual {v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 564
    .local v0, "intersectedSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    if-nez v0, :cond_3

    .line 565
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastPressedNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    goto :goto_0

    .line 568
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    if-eqz v2, :cond_4

    .line 569
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_5

    .line 570
    :cond_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    if-ne v2, p5, :cond_5

    .line 571
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastPressedNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 576
    :cond_5
    sget-object v2, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;->REQUEST_CLICK_RESULT:Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    if-ne p1, v2, :cond_7

    .line 577
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    invoke-virtual {v2, v1, p2, p5}, Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;->callbackClickAnimation(Lcom/samsung/sgieffect/main/SGIEffectNode;Landroid/view/MotionEvent;I)Z

    .line 578
    :cond_6
    sget v2, Lcom/samsung/sgieffect/main/SGIEffectView;->mTypeExtraState:I

    invoke-virtual {v1, p5, v2, p2}, Lcom/samsung/sgieffect/main/SGIEffectNode;->onClick(IILandroid/view/MotionEvent;)Z

    .line 580
    :cond_7
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public addNode(Lcom/samsung/sgieffect/main/SGIEffectNode;)Z
    .locals 1
    .param p1, "node"    # Lcom/samsung/sgieffect/main/SGIEffectNode;

    .prologue
    .line 440
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 442
    :goto_0
    return v0

    .line 441
    :cond_0
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 442
    invoke-virtual {p0, p1}, Lcom/samsung/sgieffect/main/SGIEffectView;->cookingSlide(Lcom/samsung/sgieffect/main/SGIEffectNode;)Z

    move-result v0

    goto :goto_0
.end method

.method public addPannel(FFFFFFFF)V
    .locals 7
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F
    .param p5, "red"    # F
    .param p6, "green"    # F
    .param p7, "blue"    # F
    .param p8, "alpha"    # F

    .prologue
    .line 187
    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    if-nez v3, :cond_1

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    new-instance v0, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-direct {v0}, Lcom/samsung/sgieffect/main/SGIEffectNode;-><init>()V

    .line 191
    .local v0, "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    invoke-virtual {v0, p5, p6, p7, p8}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setColor(FFFF)V

    .line 192
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setRegion(FFFF)V

    .line 194
    invoke-virtual {v0}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getRegion()Landroid/graphics/RectF;

    move-result-object v1

    .line 196
    .local v1, "region":Landroid/graphics/RectF;
    new-instance v2, Lcom/samsung/sgieffect/main/SGISlideEmulation;

    invoke-direct {v2, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;-><init>(Landroid/graphics/RectF;)V

    .line 197
    .local v2, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v3, p8

    float-to-int v3, v3

    const/high16 v4, 0x437f0000    # 255.0f

    mul-float/2addr v4, p5

    float-to-int v4, v4

    const/high16 v5, 0x437f0000    # 255.0f

    mul-float/2addr v5, p6

    float-to-int v5, v5

    const/high16 v6, 0x437f0000    # 255.0f

    mul-float/2addr v6, p7

    float-to-int v6, v6

    invoke-static {v3, v4, v5, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 198
    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 200
    const/high16 v3, 0x3f000000    # 0.5f

    const/high16 v4, 0x3f000000    # 0.5f

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 201
    const/high16 v3, 0x3f000000    # 0.5f

    const/high16 v4, 0x3f000000    # 0.5f

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 202
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 204
    invoke-virtual {v0, v2}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 206
    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 207
    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    invoke-virtual {v3, v0}, Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;->callbackPannelInitialize(Lcom/samsung/sgieffect/main/SGIEffectNode;)Z

    goto :goto_0
.end method

.method public addPannel(FFFFLandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;F)V
    .locals 11
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F
    .param p5, "diffuse"    # Landroid/graphics/Bitmap;
    .param p6, "normal"    # Landroid/graphics/Bitmap;
    .param p7, "specular"    # Landroid/graphics/Bitmap;
    .param p8, "normalScalar"    # F

    .prologue
    .line 243
    iget-object v7, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    if-nez v7, :cond_1

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 245
    :cond_1
    new-instance v2, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-direct {v2}, Lcom/samsung/sgieffect/main/SGIEffectNode;-><init>()V

    .line 247
    .local v2, "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setColorGUI(Landroid/graphics/Bitmap;)V

    .line 248
    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setNormalMapGUI(Landroid/graphics/Bitmap;)V

    .line 249
    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setSpecularMapGUI(Landroid/graphics/Bitmap;)V

    .line 250
    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setRegion(FFFF)V

    .line 251
    move/from16 v0, p8

    invoke-virtual {v2, v0}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setNormalHeight(F)V

    .line 253
    invoke-virtual {v2}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getRegion()Landroid/graphics/RectF;

    move-result-object v4

    .line 255
    .local v4, "region":Landroid/graphics/RectF;
    new-instance v5, Lcom/samsung/sgieffect/main/SGISlideEmulation;

    invoke-direct {v5, v4}, Lcom/samsung/sgieffect/main/SGISlideEmulation;-><init>(Landroid/graphics/RectF;)V

    .line 256
    .local v5, "slide":Lcom/samsung/sgieffect/main/SGISlideEmulation;
    const/4 v7, -0x1

    invoke-virtual {v5, v7}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setColor(I)V

    .line 257
    iget-object v7, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v7, v5}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 259
    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, 0x42800000    # 64.0f

    invoke-virtual {v5, v7, v8, v9, v10}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightColor(FFFF)V

    .line 261
    invoke-virtual {v2}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getColorGUIImage()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 263
    .local v1, "diffuseImage":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getNormalMapGUIImage()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 264
    .local v3, "normalImage":Landroid/graphics/Bitmap;
    invoke-virtual {v2}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSpecularMapGUIImage()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 266
    .local v6, "specularImage":Landroid/graphics/Bitmap;
    const/high16 v7, 0x3f000000    # 0.5f

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-virtual {v5, v7, v8}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setRotationPivot(FF)V

    .line 267
    const/high16 v7, 0x3f000000    # 0.5f

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-virtual {v5, v7, v8}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setScalePivot(FF)V

    .line 268
    invoke-virtual {v5, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 270
    if-eqz v3, :cond_3

    .line 271
    invoke-virtual {v5, v3}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setNormalBitmap(Landroid/graphics/Bitmap;)V

    .line 272
    if-eqz v6, :cond_2

    invoke-virtual {v5, v6}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setSpecularBitmap(Landroid/graphics/Bitmap;)V

    .line 274
    :cond_2
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setUseNormalMap(Z)V

    .line 276
    move/from16 v0, p8

    invoke-virtual {v5, v0}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightPower(F)V

    .line 277
    const/4 v7, 0x0

    const/high16 v8, -0x41000000    # -0.5f

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-virtual {v5, v7, v8, v9, v10}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setViewDirection(FFFF)V

    .line 278
    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-virtual {v5, v7, v8, v9, v10}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightDirection(FFFF)V

    .line 284
    :goto_1
    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {v5, v7}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setOpacity(F)V

    .line 286
    invoke-virtual {v2, v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 288
    iget-object v7, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v7, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 289
    iget-object v7, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    invoke-virtual {v7, v2}, Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;->callbackPannelInitialize(Lcom/samsung/sgieffect/main/SGIEffectNode;)Z

    goto/16 :goto_0

    .line 281
    :cond_3
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setUseNormalMap(Z)V

    goto :goto_1
.end method

.method public addPannel(FFFFLandroid/graphics/Bitmap;Z)V
    .locals 5
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F
    .param p5, "diffuse"    # Landroid/graphics/Bitmap;
    .param p6, "isMask"    # Z

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 212
    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    if-nez v3, :cond_1

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    new-instance v0, Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-direct {v0}, Lcom/samsung/sgieffect/main/SGIEffectNode;-><init>()V

    .line 216
    .local v0, "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setRegion(FFFF)V

    .line 218
    invoke-virtual {v0}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getRegion()Landroid/graphics/RectF;

    move-result-object v1

    .line 220
    .local v1, "region":Landroid/graphics/RectF;
    new-instance v2, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-direct {v2, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>(Landroid/graphics/RectF;)V

    .line 221
    .local v2, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 222
    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 224
    invoke-virtual {v2, v4, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 225
    invoke-virtual {v2, v4, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 226
    if-nez p6, :cond_2

    .line 227
    invoke-virtual {v2, p5}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 233
    :goto_1
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 235
    invoke-virtual {v0, v2}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 237
    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 238
    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    invoke-virtual {v3, v0}, Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;->callbackPannelInitialize(Lcom/samsung/sgieffect/main/SGIEffectNode;)Z

    goto :goto_0

    .line 229
    :cond_2
    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mMaskBitmapTexture2DProperty:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {v3, p5}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 230
    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mMaskBitmapTexture2DProperty:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setTexture(Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    goto :goto_1
.end method

.method public clearNodes()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 682
    iget-object v10, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    if-lez v10, :cond_0

    .line 683
    const/4 v5, 0x0

    .local v5, "n":I
    :goto_0
    iget-object v10, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    if-lt v5, v10, :cond_3

    .line 710
    iget-object v10, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->clear()V

    .line 714
    .end local v5    # "n":I
    :cond_0
    iget-object v10, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    if-lez v10, :cond_1

    .line 715
    const/4 v5, 0x0

    .restart local v5    # "n":I
    :goto_1
    iget-object v10, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    if-lt v5, v10, :cond_8

    .line 741
    iget-object v10, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->clear()V

    .line 743
    .end local v5    # "n":I
    :cond_1
    iput-object v11, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastPressedNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 745
    iget-object v10, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    if-eqz v10, :cond_2

    .line 746
    iget-object v10, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v10}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getChildrenCount()I

    move-result v10

    add-int/lit8 v3, v10, -0x1

    .local v3, "i":I
    :goto_2
    if-gez v3, :cond_d

    .line 762
    .end local v3    # "i":I
    :cond_2
    const/4 v10, 0x0

    iput-boolean v10, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mFirstRenderFinished:Z

    .line 763
    return-void

    .line 684
    .restart local v5    # "n":I
    :cond_3
    iget-object v10, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 685
    .local v6, "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    invoke-virtual {v6}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getContentSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    .line 686
    .local v2, "contentSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v6}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v9

    .line 688
    .local v9, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v6}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getBaseSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 689
    .local v0, "baseSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v6}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getOverlayContentSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v7

    .line 693
    .local v7, "overlaySlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    if-eqz v7, :cond_4

    .line 694
    invoke-virtual {v7, v11}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 696
    :cond_4
    if-eqz v0, :cond_5

    .line 697
    invoke-virtual {v0, v11}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 699
    :cond_5
    if-eqz v2, :cond_6

    .line 700
    invoke-virtual {v2, v11}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 702
    :cond_6
    if-eqz v9, :cond_7

    .line 703
    invoke-virtual {v9, v11}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 706
    :cond_7
    invoke-virtual {v6, v11}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 707
    invoke-virtual {v6, v11}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setContentSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 683
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 716
    .end local v0    # "baseSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .end local v2    # "contentSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .end local v6    # "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    .end local v7    # "overlaySlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .end local v9    # "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    :cond_8
    iget-object v10, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v10, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 717
    .restart local v6    # "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    invoke-virtual {v6}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getContentSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v2

    .line 718
    .restart local v2    # "contentSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v6}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v9

    .line 719
    .restart local v9    # "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v6}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getBaseSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v0

    .line 720
    .restart local v0    # "baseSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v6}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getOverlayContentSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v7

    .line 724
    .restart local v7    # "overlaySlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    if-eqz v7, :cond_9

    .line 725
    invoke-virtual {v7, v11}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 727
    :cond_9
    if-eqz v0, :cond_a

    .line 728
    invoke-virtual {v0, v11}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 730
    :cond_a
    if-eqz v2, :cond_b

    .line 731
    invoke-virtual {v2, v11}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 733
    :cond_b
    if-eqz v9, :cond_c

    .line 734
    invoke-virtual {v9, v11}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 737
    :cond_c
    invoke-virtual {v6, v11}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 738
    invoke-virtual {v6, v11}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setContentSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 715
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 747
    .end local v0    # "baseSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .end local v2    # "contentSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .end local v5    # "n":I
    .end local v6    # "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    .end local v7    # "overlaySlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .end local v9    # "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .restart local v3    # "i":I
    :cond_d
    iget-object v10, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v10, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 748
    .local v8, "removeSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    if-eqz v8, :cond_e

    .line 751
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_3
    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getChildrenCount()I

    move-result v10

    if-lt v4, v10, :cond_f

    .line 755
    invoke-virtual {v8, v11}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 756
    iget-object v10, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v10, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->removeLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 746
    .end local v4    # "j":I
    :cond_e
    add-int/lit8 v3, v3, -0x1

    goto/16 :goto_2

    .line 752
    .restart local v4    # "j":I
    :cond_f
    invoke-virtual {v8, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 753
    .local v1, "childSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 751
    add-int/lit8 v4, v4, 0x1

    goto :goto_3
.end method

.method protected cookingSlide(Lcom/samsung/sgieffect/main/SGIEffectNode;)Z
    .locals 24
    .param p1, "node"    # Lcom/samsung/sgieffect/main/SGIEffectNode;

    .prologue
    .line 320
    if-nez p1, :cond_0

    .line 321
    const-string v20, "SGI Effect View"

    const-string v21, "Node is null, Cooking slide is failed."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    const/16 v20, 0x0

    .line 424
    :goto_0
    return v20

    .line 325
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-object/from16 v20, v0

    if-nez v20, :cond_1

    .line 326
    const-string v20, "SGI Effect View"

    const-string v21, "RootSlide is null, Cooking slide is failed."

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    const/16 v20, 0x0

    goto :goto_0

    .line 330
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getRegion()Landroid/graphics/RectF;

    move-result-object v14

    .line 331
    .local v14, "region":Landroid/graphics/RectF;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getColor()I

    move-result v8

    .line 333
    .local v8, "color":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->hasBase()Z

    move-result v20

    if-eqz v20, :cond_2

    .line 334
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getBaseGUIImage()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 336
    .local v6, "baseMap":Landroid/graphics/Bitmap;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->isBaseRegionSetted()Z

    move-result v20

    if-eqz v20, :cond_7

    .line 337
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getBaseRegion()Landroid/graphics/RectF;

    move-result-object v15

    .line 339
    .local v15, "regionBase":Landroid/graphics/RectF;
    :goto_1
    new-instance v7, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-direct {v7, v15}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>(Landroid/graphics/RectF;)V

    .line 340
    .local v7, "baseSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setColor(I)V

    .line 341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 343
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v7, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setRotationPivot(FF)V

    .line 344
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v7, v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setScalePivot(FF)V

    .line 345
    invoke-virtual {v7, v6}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 347
    const/high16 v20, 0x3f800000    # 1.0f

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setOpacity(F)V

    .line 349
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setBaseSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 352
    .end local v6    # "baseMap":Landroid/graphics/Bitmap;
    .end local v7    # "baseSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .end local v15    # "regionBase":Landroid/graphics/RectF;
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getColorGUIImage()Landroid/graphics/Bitmap;

    move-result-object v11

    .line 353
    .local v11, "diffuseMap":Landroid/graphics/Bitmap;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getNormalMapGUIImage()Landroid/graphics/Bitmap;

    move-result-object v12

    .line 354
    .local v12, "normalMap":Landroid/graphics/Bitmap;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSpecularMapGUIImage()Landroid/graphics/Bitmap;

    move-result-object v19

    .line 356
    .local v19, "specularMap":Landroid/graphics/Bitmap;
    new-instance v18, Lcom/samsung/sgieffect/main/SGISlideEmulation;

    move-object/from16 v0, v18

    invoke-direct {v0, v14}, Lcom/samsung/sgieffect/main/SGISlideEmulation;-><init>(Landroid/graphics/RectF;)V

    .line 357
    .local v18, "slide":Lcom/samsung/sgieffect/main/SGISlideEmulation;
    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setColor(I)V

    .line 358
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 360
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setRotationPivot(FF)V

    .line 361
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setScalePivot(FF)V

    .line 362
    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 364
    if-eqz v12, :cond_8

    .line 365
    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setNormalBitmap(Landroid/graphics/Bitmap;)V

    .line 366
    if-eqz v19, :cond_3

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setSpecularBitmap(Landroid/graphics/Bitmap;)V

    .line 368
    :cond_3
    const/16 v20, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setUseNormalMap(Z)V

    .line 370
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getNormalHeight()F

    move-result v20

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightPower(F)V

    .line 371
    const/16 v20, 0x0

    const/high16 v21, -0x41000000    # -0.5f

    const/high16 v22, 0x3f800000    # 1.0f

    const/high16 v23, 0x3f800000    # 1.0f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setViewDirection(FFFF)V

    .line 372
    const/16 v20, 0x0

    const/high16 v21, 0x3f800000    # 1.0f

    const/16 v22, 0x0

    const/high16 v23, 0x3f800000    # 1.0f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightDirection(FFFF)V

    .line 378
    :goto_2
    const/high16 v20, 0x3f800000    # 1.0f

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setOpacity(F)V

    .line 380
    const/16 v20, 0x0

    const/high16 v21, -0x41000000    # -0.5f

    const/high16 v22, 0x3f800000    # 1.0f

    const/high16 v23, 0x3f800000    # 1.0f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setViewDirection(FFFF)V

    .line 381
    const/16 v20, 0x0

    const/high16 v21, 0x3f800000    # 1.0f

    const/16 v22, 0x0

    const/high16 v23, 0x3f800000    # 1.0f

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setLightDirection(FFFF)V

    .line 383
    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 385
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->hasContent()Z

    move-result v20

    if-eqz v20, :cond_4

    .line 386
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getContentGUIImage()Landroid/graphics/Bitmap;

    move-result-object v9

    .line 388
    .local v9, "contentMap":Landroid/graphics/Bitmap;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->isContentRegionSetted()Z

    move-result v20

    if-eqz v20, :cond_9

    .line 389
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getContentRegion()Landroid/graphics/RectF;

    move-result-object v16

    .line 391
    .local v16, "regionContent":Landroid/graphics/RectF;
    :goto_3
    new-instance v10, Lcom/samsung/sgieffect/main/SGISlideEmulation;

    move-object/from16 v0, v16

    invoke-direct {v10, v0}, Lcom/samsung/sgieffect/main/SGISlideEmulation;-><init>(Landroid/graphics/RectF;)V

    .line 392
    .local v10, "contentslide":Lcom/samsung/sgieffect/main/SGISlideEmulation;
    invoke-virtual {v10, v8}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setColor(I)V

    .line 393
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 395
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setRotationPivot(FF)V

    .line 396
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v10, v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setScalePivot(FF)V

    .line 397
    invoke-virtual {v10, v9}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 398
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setUseNormalMap(Z)V

    .line 399
    const/high16 v20, 0x3f800000    # 1.0f

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setOpacity(F)V

    .line 401
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setContentSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 404
    .end local v9    # "contentMap":Landroid/graphics/Bitmap;
    .end local v10    # "contentslide":Lcom/samsung/sgieffect/main/SGISlideEmulation;
    .end local v16    # "regionContent":Landroid/graphics/RectF;
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->hasOverlayContent()Z

    move-result v20

    if-eqz v20, :cond_5

    .line 405
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getOverlayContentGUIImage()Landroid/graphics/Bitmap;

    move-result-object v13

    .line 406
    .local v13, "overlayContentMap":Landroid/graphics/Bitmap;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->isOverlayContentRegionSetted()Z

    move-result v20

    if-eqz v20, :cond_a

    .line 407
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getOverlayContentRegion()Landroid/graphics/RectF;

    move-result-object v17

    .line 409
    .local v17, "regionOverlayContent":Landroid/graphics/RectF;
    :goto_4
    new-instance v5, Lcom/samsung/sgieffect/main/SGISlideEmulation;

    move-object/from16 v0, v17

    invoke-direct {v5, v0}, Lcom/samsung/sgieffect/main/SGISlideEmulation;-><init>(Landroid/graphics/RectF;)V

    .line 410
    .local v5, "OverlayContentslide":Lcom/samsung/sgieffect/main/SGISlideEmulation;
    invoke-virtual {v5, v8}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setColor(I)V

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 413
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setRotationPivot(FF)V

    .line 414
    const/high16 v20, 0x3f000000    # 0.5f

    const/high16 v21, 0x3f000000    # 0.5f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setScalePivot(FF)V

    .line 415
    invoke-virtual {v5, v13}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 416
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setUseNormalMap(Z)V

    .line 417
    const/high16 v20, 0x3f800000    # 1.0f

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setOpacity(F)V

    .line 419
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/samsung/sgieffect/main/SGIEffectNode;->setOverlayContentSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 422
    .end local v5    # "OverlayContentslide":Lcom/samsung/sgieffect/main/SGISlideEmulation;
    .end local v13    # "overlayContentMap":Landroid/graphics/Bitmap;
    .end local v17    # "regionOverlayContent":Landroid/graphics/RectF;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    move-object/from16 v20, v0

    if-eqz v20, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;->callbackInitialize(Lcom/samsung/sgieffect/main/SGIEffectNode;)Z

    .line 424
    :cond_6
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 337
    .end local v11    # "diffuseMap":Landroid/graphics/Bitmap;
    .end local v12    # "normalMap":Landroid/graphics/Bitmap;
    .end local v18    # "slide":Lcom/samsung/sgieffect/main/SGISlideEmulation;
    .end local v19    # "specularMap":Landroid/graphics/Bitmap;
    .restart local v6    # "baseMap":Landroid/graphics/Bitmap;
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getRegion()Landroid/graphics/RectF;

    move-result-object v15

    goto/16 :goto_1

    .line 375
    .end local v6    # "baseMap":Landroid/graphics/Bitmap;
    .restart local v11    # "diffuseMap":Landroid/graphics/Bitmap;
    .restart local v12    # "normalMap":Landroid/graphics/Bitmap;
    .restart local v18    # "slide":Lcom/samsung/sgieffect/main/SGISlideEmulation;
    .restart local v19    # "specularMap":Landroid/graphics/Bitmap;
    :cond_8
    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setUseNormalMap(Z)V

    goto/16 :goto_2

    .line 389
    .restart local v9    # "contentMap":Landroid/graphics/Bitmap;
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getRegion()Landroid/graphics/RectF;

    move-result-object v16

    goto/16 :goto_3

    .line 407
    .end local v9    # "contentMap":Landroid/graphics/Bitmap;
    .restart local v13    # "overlayContentMap":Landroid/graphics/Bitmap;
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getRegion()Landroid/graphics/RectF;

    move-result-object v17

    goto :goto_4
.end method

.method protected createSlide()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x3f000000    # 0.5f

    .line 175
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    if-nez v0, :cond_0

    .line 176
    new-instance v0, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v3

    invoke-direct {v1, v5, v5, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>(Landroid/graphics/RectF;)V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 178
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v0, v4, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->setRotationPivot(FF)V

    .line 179
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v0, v4, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->setScalePivot(FF)V

    .line 180
    invoke-virtual {p0}, Lcom/samsung/sgieffect/main/SGIEffectView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 182
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 670
    invoke-virtual {p0}, Lcom/samsung/sgieffect/main/SGIEffectView;->clearNodes()V

    .line 671
    invoke-virtual {p0}, Lcom/samsung/sgieffect/main/SGIEffectView;->destroySlide()Z

    .line 672
    invoke-virtual {p0}, Lcom/samsung/sgieffect/main/SGIEffectView;->destroyRoot()V

    .line 673
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mIsHidden:Z

    .line 674
    return-void
.end method

.method protected destroyRoot()V
    .locals 2

    .prologue
    .line 777
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    if-eqz v0, :cond_0

    .line 778
    invoke-virtual {p0}, Lcom/samsung/sgieffect/main/SGIEffectView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->removeLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 779
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    .line 781
    :cond_0
    return-void
.end method

.method protected destroySlide()Z
    .locals 3

    .prologue
    .line 766
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    if-eqz v2, :cond_0

    .line 767
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getChildrenCount()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_1

    .line 773
    .end local v0    # "i":I
    :cond_0
    const/4 v2, 0x1

    return v2

    .line 768
    .restart local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->getLayer(I)Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 769
    .local v1, "removeSlide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    if-eqz v1, :cond_2

    .line 770
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->removeLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)V

    .line 767
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public finalize()V
    .locals 1

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/samsung/sgieffect/main/SGIEffectView;->clearNodes()V

    .line 159
    invoke-virtual {p0}, Lcom/samsung/sgieffect/main/SGIEffectView;->destroySlide()Z

    .line 160
    invoke-virtual {p0}, Lcom/samsung/sgieffect/main/SGIEffectView;->destroyRoot()V

    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mIsHidden:Z

    .line 162
    return-void
.end method

.method public findNode(I)Lcom/samsung/sgieffect/main/SGIEffectNode;
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v2, 0x0

    .line 428
    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-nez v3, :cond_1

    move-object v1, v2

    .line 436
    :cond_0
    :goto_0
    return-object v1

    .line 430
    :cond_1
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_1
    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    if-lt v0, v3, :cond_2

    move-object v1, v2

    .line 436
    goto :goto_0

    .line 431
    :cond_2
    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v3, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 432
    .local v1, "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getIndex()I

    move-result v3

    if-eq v3, p1, :cond_0

    .line 430
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getAnimationCallback()Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    return-object v0
.end method

.method public getHit(FFLandroid/graphics/RectF;)Z
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "rect"    # Landroid/graphics/RectF;

    .prologue
    .line 302
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, p1, p2, p1, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p3, v0}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v0

    return v0
.end method

.method protected getIntersectedNode(FF)Lcom/samsung/sgieffect/main/SGIEffectNode;
    .locals 5
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 306
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    iget-object v4, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-lt v0, v4, :cond_1

    .line 316
    const/4 v1, 0x0

    :cond_0
    return-object v1

    .line 307
    :cond_1
    iget-object v4, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v4, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 308
    .local v1, "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    if-eqz v1, :cond_2

    .line 309
    invoke-virtual {v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    move-result-object v3

    .line 310
    .local v3, "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getScreenBounds()Landroid/graphics/RectF;

    move-result-object v2

    .line 311
    .local v2, "rect":Landroid/graphics/RectF;
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, p1, p2, p1, p2}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v2, v4}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 306
    .end local v2    # "rect":Landroid/graphics/RectF;
    .end local v3    # "slide":Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getIntersectedObject(Landroid/view/MotionEvent;)Lcom/samsung/sgieffect/main/SGIEffectNode;
    .locals 2
    .param p1, "inputEvent"    # Landroid/view/MotionEvent;

    .prologue
    .line 465
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 466
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/sgieffect/main/SGIEffectView;->getIntersectedNode(FF)Lcom/samsung/sgieffect/main/SGIEffectNode;

    move-result-object v0

    goto :goto_0
.end method

.method public getNodeSize()I
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method public getPannelNode(I)Lcom/samsung/sgieffect/main/SGIEffectNode;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 293
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 v0, 0x0

    .line 294
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/sgieffect/main/SGIEffectNode;

    goto :goto_0
.end method

.method public getRoot()Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    .locals 1

    .prologue
    .line 677
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    return-object v0
.end method

.method public initialize()V
    .locals 2

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/samsung/sgieffect/main/SGIEffectView;->createSlide()Z

    .line 154
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mStartTime:J

    .line 155
    return-void
.end method

.method protected initializePostEffect()V
    .locals 0

    .prologue
    .line 149
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mTouchDispatcher:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mTouchDispatcher:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 171
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected reLayout()Z
    .locals 1

    .prologue
    .line 461
    const/4 v0, 0x0

    return v0
.end method

.method public setAnimationCallback(Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    .prologue
    .line 450
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    .line 451
    return-void
.end method

.method public setBackGround(FFFFLandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;F)V
    .locals 0
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F
    .param p5, "diffuse"    # Landroid/graphics/Bitmap;
    .param p6, "normal"    # Landroid/graphics/Bitmap;
    .param p7, "specular"    # Landroid/graphics/Bitmap;
    .param p8, "normalScalar"    # F

    .prologue
    .line 298
    invoke-virtual/range {p0 .. p8}, Lcom/samsung/sgieffect/main/SGIEffectView;->addPannel(FFFFLandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;F)V

    .line 299
    return-void
.end method

.method public setListener(Lcom/samsung/sgieffect/main/SGIEffectListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/sgieffect/main/SGIEffectListener;

    .prologue
    .line 454
    sput-object p1, Lcom/samsung/sgieffect/main/SGIEffectView;->mListener:Lcom/samsung/sgieffect/main/SGIEffectListener;

    .line 455
    return-void
.end method

.method public setPannelPosition(II)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 656
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCurrentRootPosition:Landroid/graphics/PointF;

    int-to-float v1, p1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 658
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCurrentRootPosition:Landroid/graphics/PointF;

    int-to-float v1, p2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 659
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->setPosition(FF)V

    .line 661
    :cond_0
    return-void
.end method

.method public setScrollPositionY(I)V
    .locals 0
    .param p1, "aValue"    # I

    .prologue
    .line 585
    iput p1, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mScrollY:I

    .line 586
    return-void
.end method

.method public setTopOffset(I)V
    .locals 0
    .param p1, "aValue"    # I

    .prologue
    .line 590
    iput p1, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mTopOffset:I

    .line 591
    return-void
.end method

.method public setTouchDispatcher(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mTouchDispatcher:Landroid/view/View;

    .line 166
    return-void
.end method

.method public surfaceShow(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 664
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    if-eqz v0, :cond_0

    .line 665
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mRootSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayer;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->setVisibility(Z)V

    .line 667
    :cond_0
    return-void
.end method

.method public validateOnHover(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "inputEvent"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 527
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    .line 528
    .local v0, "touchCount":I
    if-nez v0, :cond_0

    .line 532
    :goto_0
    return v3

    .line 529
    :cond_0
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    .line 530
    .local v1, "x":F
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    .line 531
    .local v2, "y":F
    invoke-direct {p0, p1, v1, v2}, Lcom/samsung/sgieffect/main/SGIEffectView;->processOnHover(Landroid/view/MotionEvent;FF)V

    .line 532
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public validateOnLongClick()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 487
    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastPressedNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    if-nez v1, :cond_0

    .line 491
    :goto_0
    return v0

    .line 488
    :cond_0
    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastPressedNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastPressedNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v2}, Lcom/samsung/sgieffect/main/SGIEffectNode;->getIndex()I

    move-result v2

    invoke-virtual {v1, v2, v0, v3}, Lcom/samsung/sgieffect/main/SGIEffectNode;->onLongPress(IILandroid/view/MotionEvent;)Z

    .line 489
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastPressedNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    invoke-virtual {v0, v1, v3}, Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;->callbackLongPressAnimation(Lcom/samsung/sgieffect/main/SGIEffectNode;Landroid/view/MotionEvent;)Z

    .line 490
    :cond_1
    iput-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mLastPressedNode:Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 491
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public validateOnSensorChange([FI)Z
    .locals 3
    .param p1, "value"    # [F
    .param p2, "type"    # I

    .prologue
    .line 470
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-lt v0, v2, :cond_0

    .line 477
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-lt v0, v2, :cond_2

    .line 483
    const/4 v2, 0x1

    return v2

    .line 471
    :cond_0
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 472
    .local v1, "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    if-eqz v1, :cond_1

    .line 473
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    invoke-virtual {v2, v1, p1, p2}, Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;->callbackSensor(Lcom/samsung/sgieffect/main/SGIEffectNode;[FI)Z

    .line 470
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 478
    .end local v1    # "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    :cond_2
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 479
    .restart local v1    # "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    if-eqz v1, :cond_3

    .line 480
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mCallback:Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;

    invoke-virtual {v2, v1, p1, p2}, Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;->callbackSensor(Lcom/samsung/sgieffect/main/SGIEffectNode;[FI)Z

    .line 477
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public validateOnTouch(Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "type"    # Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;
    .param p2, "inputEvent"    # Landroid/view/MotionEvent;

    .prologue
    .line 594
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    .line 598
    .local v7, "touchCount":I
    const/4 v6, 0x0

    .line 599
    .local v6, "resultCount":I
    const/4 v5, 0x0

    .local v5, "n":I
    :goto_0
    if-lt v5, v7, :cond_0

    .line 607
    if-nez v6, :cond_2

    .line 608
    const/4 v0, 0x0

    .line 610
    :goto_1
    return v0

    .line 600
    :cond_0
    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    .line 601
    .local v3, "x":F
    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iget v1, p0, Lcom/samsung/sgieffect/main/SGIEffectView;->mScrollY:I

    int-to-float v1, v1

    sub-float v4, v0, v1

    .local v4, "y":F
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 602
    invoke-direct/range {v0 .. v5}, Lcom/samsung/sgieffect/main/SGIEffectView;->processOnTouch(Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;Landroid/view/MotionEvent;FFI)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 603
    add-int/lit8 v6, v6, 0x1

    .line 599
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 610
    .end local v3    # "x":F
    .end local v4    # "y":F
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method
