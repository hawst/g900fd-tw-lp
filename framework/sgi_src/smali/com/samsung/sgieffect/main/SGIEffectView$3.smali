.class Lcom/samsung/sgieffect/main/SGIEffectView$3;
.super Ljava/lang/Object;
.source "SGIEffectView.java"

# interfaces
.implements Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/sgieffect/main/SGIEffectView;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/sgieffect/main/SGIEffectView;


# direct methods
.method constructor <init>(Lcom/samsung/sgieffect/main/SGIEffectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectView$3;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFrameEnd()V
    .locals 8

    .prologue
    .line 92
    sget-object v2, Lcom/samsung/sgieffect/main/SGIEffectView;->mListener:Lcom/samsung/sgieffect/main/SGIEffectListener;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView$3;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    # getter for: Lcom/samsung/sgieffect/main/SGIEffectView;->mFirstRenderFinished:Z
    invoke-static {v2}, Lcom/samsung/sgieffect/main/SGIEffectView;->access$1(Lcom/samsung/sgieffect/main/SGIEffectView;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 93
    sget-object v2, Lcom/samsung/sgieffect/main/SGIEffectView;->mListener:Lcom/samsung/sgieffect/main/SGIEffectListener;

    invoke-interface {v2}, Lcom/samsung/sgieffect/main/SGIEffectListener;->onFirstRenderFinished()V

    .line 94
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView$3;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/samsung/sgieffect/main/SGIEffectView;->access$2(Lcom/samsung/sgieffect/main/SGIEffectView;Z)V

    .line 96
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView$3;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    iget-object v2, v2, Lcom/samsung/sgieffect/main/SGIEffectView;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 97
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView$3;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    iget-object v2, v2, Lcom/samsung/sgieffect/main/SGIEffectView;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-lt v0, v2, :cond_2

    .line 103
    .end local v0    # "n":I
    :cond_0
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView$3;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    iget-object v2, v2, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 104
    const/4 v0, 0x0

    .restart local v0    # "n":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView$3;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    iget-object v2, v2, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-lt v0, v2, :cond_3

    .line 113
    .end local v0    # "n":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView$3;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/samsung/sgieffect/main/SGIEffectView;->access$3(Lcom/samsung/sgieffect/main/SGIEffectView;J)V

    .line 114
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView$3;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView$3;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    # getter for: Lcom/samsung/sgieffect/main/SGIEffectView;->mEndTime:J
    invoke-static {v3}, Lcom/samsung/sgieffect/main/SGIEffectView;->access$4(Lcom/samsung/sgieffect/main/SGIEffectView;)J

    move-result-wide v4

    iget-object v3, p0, Lcom/samsung/sgieffect/main/SGIEffectView$3;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    # getter for: Lcom/samsung/sgieffect/main/SGIEffectView;->mStartTime:J
    invoke-static {v3}, Lcom/samsung/sgieffect/main/SGIEffectView;->access$5(Lcom/samsung/sgieffect/main/SGIEffectView;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v2, v4, v5}, Lcom/samsung/sgieffect/main/SGIEffectView;->access$6(Lcom/samsung/sgieffect/main/SGIEffectView;J)V

    .line 117
    return-void

    .line 98
    .restart local v0    # "n":I
    :cond_2
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView$3;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    iget-object v2, v2, Lcom/samsung/sgieffect/main/SGIEffectView;->mPannelNodes:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 99
    .local v1, "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    invoke-virtual {v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->recycleBitmaps()V

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    .end local v1    # "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    :cond_3
    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView$3;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    iget-object v2, v2, Lcom/samsung/sgieffect/main/SGIEffectView;->mNodes:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/sgieffect/main/SGIEffectNode;

    .line 106
    .restart local v1    # "node":Lcom/samsung/sgieffect/main/SGIEffectNode;
    invoke-virtual {v1}, Lcom/samsung/sgieffect/main/SGIEffectNode;->recycleBitmaps()V

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
