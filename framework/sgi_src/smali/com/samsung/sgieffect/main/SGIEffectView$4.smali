.class Lcom/samsung/sgieffect/main/SGIEffectView$4;
.super Ljava/lang/Object;
.source "SGIEffectView.java"

# interfaces
.implements Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/sgieffect/main/SGIEffectView;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/sgieffect/main/SGIEffectView;


# direct methods
.method constructor <init>(Lcom/samsung/sgieffect/main/SGIEffectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectView$4;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 2
    .param p1, "arg0"    # Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .prologue
    .line 125
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 126
    .local v0, "msg":Landroid/os/Message;
    const v1, 0x9001

    iput v1, v0, Landroid/os/Message;->what:I

    .line 127
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 128
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/os/Message;->arg2:I

    .line 129
    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectView$4;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    # getter for: Lcom/samsung/sgieffect/main/SGIEffectView;->mViewHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/sgieffect/main/SGIEffectView;->access$7(Lcom/samsung/sgieffect/main/SGIEffectView;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 130
    return-void
.end method
