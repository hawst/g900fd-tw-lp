.class public Lcom/samsung/sgieffect/main/SGISlideEmulation;
.super Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
.source "SGISlideEmulation.java"


# static fields
.field static final FRAGMENT_SHADER_TEXTURE_NORMAL:Ljava/lang/String; = "precision highp float;\nvarying vec2       vTexcoord;\nvarying vec3       vVaryingNormal;\nvarying vec3       vVaryingLightDir;\nuniform mat4       SGWorldInverseTranspose;\nuniform vec4       SGColor;uniform sampler2D  SGTexture;\nuniform sampler2D  SGNormalTexture;\nuniform sampler2D  SGSpecularTexture;\nuniform float      uLightPower;\nuniform vec4       uLightColor;\nuniform float      uCustomLightPower;\nvec4       Ambient = vec4(0.1,0.1,0.1,1.0);\nvec4       Diffuse = vec4(0.7,0.7,0.7,1.0);\nvoid main()\n{\n   float maxVariance = 5.0;\n   float minVariance = maxVariance / 2.0;\n   vec2 scaledCoord = vTexcoord;\n;   vec4 texturecolor = texture2D(SGTexture, scaledCoord);\n   vec3 normalbasis = texture2D(SGNormalTexture, scaledCoord).rgb;\n   vec3 specularbasis = texture2D(SGSpecularTexture, scaledCoord).rgb;\n   vec3 filterColor = texturecolor.rgb;\n   vec3 normalmap = (normalbasis * maxVariance - minVariance);\n   normalmap.xy *= uLightPower;\n   vec3 eyeSpaceNormal = normalize(normalmap);\n   vec3 lightDir = vVaryingLightDir;\n   vec3 eyeVec = vVaryingNormal;\n   vec3 diffusemap = texturecolor.rgb;\n   vec3 aNormal = vVaryingNormal + eyeSpaceNormal;\n   float diffuseIntensity = max(uLightColor.g, dot(aNormal, vVaryingLightDir));\n   vec3 colour = diffuseIntensity * diffusemap + Ambient.rgb * texturecolor.a;\n   vec4 vFragColour = vec4(colour, texturecolor.a);\n   vec3 vReflection = normalize(reflect(-aNormal, vVaryingLightDir));\n   float specularIntensity = max(0.0, dot(aNormal, vReflection));\n   if (diffuseIntensity > uLightColor.b)\n   {\n       float fSpec = pow(specularIntensity, uLightColor.a);\n       vFragColour.rgb += vec3(fSpec * uLightColor.r * uCustomLightPower);\n   }\n   filterColor = vFragColour.rgb * specularbasis.r + diffusemap * (1.0 - specularbasis.r);\n   gl_FragColor = vec4(filterColor,texturecolor.a)* SGColor;\n}\n"

.field static final VERTEX_SHADER_TEXTURE_NORMAL:Ljava/lang/String; = "uniform mat4\t\tSGWorldViewProjection;\nattribute vec3\t\tSGPositions;\nattribute vec2 \tSGTextureCoords;uniform vec4\t\tuLightDirection;\nuniform vec4\t\tuViewDirection;\nvarying vec2\t\tvTexcoord;\nvarying vec3\t\tvVaryingNormal;\nvarying vec3\t\tvVaryingLightDir;\nvoid main()\n{\n\tvec4 n = uViewDirection;\n\tvVaryingNormal = n.xyz;\n\tvVaryingLightDir = uLightDirection.xyz;\n   vTexcoord = SGTextureCoords;\n\tgl_Position = SGWorldViewProjection * vec4(SGPositions, 1.0);\n}\n"


# instance fields
.field private mCustomLightPowerProperty:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

.field private mLightColor:Lcom/samsung/android/sdk/sgi/base/SGVector4f;

.field private mLightColorProperty:Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;

.field private mLightDirection:Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;

.field private mLightDirectionVector4f:Lcom/samsung/android/sdk/sgi/base/SGVector4f;

.field private mLightPower:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

.field private mNormalTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

.field private mSpecularTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

.field private mViewDirection:Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;

.field private mViewDirectionVector4f:Lcom/samsung/android/sdk/sgi/base/SGVector4f;

.field program_texture_normal:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    .line 196
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 169
    new-instance v1, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->VERTEX:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v3, "uniform mat4\t\tSGWorldViewProjection;\nattribute vec3\t\tSGPositions;\nattribute vec2 \tSGTextureCoords;uniform vec4\t\tuLightDirection;\nuniform vec4\t\tuViewDirection;\nvarying vec2\t\tvTexcoord;\nvarying vec3\t\tvVaryingNormal;\nvarying vec3\t\tvVaryingLightDir;\nvoid main()\n{\n\tvec4 n = uViewDirection;\n\tvVaryingNormal = n.xyz;\n\tvVaryingLightDir = uLightDirection.xyz;\n   vTexcoord = SGTextureCoords;\n\tgl_Position = SGWorldViewProjection * vec4(SGPositions, 1.0);\n}\n"

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 170
    .local v1, "vertex_texture_normal":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->FRAGMENT:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v3, "precision highp float;\nvarying vec2       vTexcoord;\nvarying vec3       vVaryingNormal;\nvarying vec3       vVaryingLightDir;\nuniform mat4       SGWorldInverseTranspose;\nuniform vec4       SGColor;uniform sampler2D  SGTexture;\nuniform sampler2D  SGNormalTexture;\nuniform sampler2D  SGSpecularTexture;\nuniform float      uLightPower;\nuniform vec4       uLightColor;\nuniform float      uCustomLightPower;\nvec4       Ambient = vec4(0.1,0.1,0.1,1.0);\nvec4       Diffuse = vec4(0.7,0.7,0.7,1.0);\nvoid main()\n{\n   float maxVariance = 5.0;\n   float minVariance = maxVariance / 2.0;\n   vec2 scaledCoord = vTexcoord;\n;   vec4 texturecolor = texture2D(SGTexture, scaledCoord);\n   vec3 normalbasis = texture2D(SGNormalTexture, scaledCoord).rgb;\n   vec3 specularbasis = texture2D(SGSpecularTexture, scaledCoord).rgb;\n   vec3 filterColor = texturecolor.rgb;\n   vec3 normalmap = (normalbasis * maxVariance - minVariance);\n   normalmap.xy *= uLightPower;\n   vec3 eyeSpaceNormal = normalize(normalmap);\n   vec3 lightDir = vVaryingLightDir;\n   vec3 eyeVec = vVaryingNormal;\n   vec3 diffusemap = texturecolor.rgb;\n   vec3 aNormal = vVaryingNormal + eyeSpaceNormal;\n   float diffuseIntensity = max(uLightColor.g, dot(aNormal, vVaryingLightDir));\n   vec3 colour = diffuseIntensity * diffusemap + Ambient.rgb * texturecolor.a;\n   vec4 vFragColour = vec4(colour, texturecolor.a);\n   vec3 vReflection = normalize(reflect(-aNormal, vVaryingLightDir));\n   float specularIntensity = max(0.0, dot(aNormal, vReflection));\n   if (diffuseIntensity > uLightColor.b)\n   {\n       float fSpec = pow(specularIntensity, uLightColor.a);\n       vFragColour.rgb += vec3(fSpec * uLightColor.r * uCustomLightPower);\n   }\n   filterColor = vFragColour.rgb * specularbasis.r + diffusemap * (1.0 - specularbasis.r);\n   gl_FragColor = vec4(filterColor,texturecolor.a)* SGColor;\n}\n"

    invoke-direct {v0, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 171
    .local v0, "fragment_texture_normal":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-direct {v2, v1, v0}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;)V

    iput-object v2, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->program_texture_normal:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    .line 177
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    sget-object v3, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    sget-object v4, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;)V

    iput-object v2, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mNormalTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    .line 178
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    sget-object v3, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    sget-object v4, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;)V

    iput-object v2, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mSpecularTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    .line 197
    invoke-direct {p0}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->createProperties()V

    .line 198
    return-void
.end method

.method public constructor <init>(Landroid/graphics/RectF;)V
    .locals 5
    .param p1, "regionOverlayContent"    # Landroid/graphics/RectF;

    .prologue
    .line 191
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>(Landroid/graphics/RectF;)V

    .line 169
    new-instance v1, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->VERTEX:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v3, "uniform mat4\t\tSGWorldViewProjection;\nattribute vec3\t\tSGPositions;\nattribute vec2 \tSGTextureCoords;uniform vec4\t\tuLightDirection;\nuniform vec4\t\tuViewDirection;\nvarying vec2\t\tvTexcoord;\nvarying vec3\t\tvVaryingNormal;\nvarying vec3\t\tvVaryingLightDir;\nvoid main()\n{\n\tvec4 n = uViewDirection;\n\tvVaryingNormal = n.xyz;\n\tvVaryingLightDir = uLightDirection.xyz;\n   vTexcoord = SGTextureCoords;\n\tgl_Position = SGWorldViewProjection * vec4(SGPositions, 1.0);\n}\n"

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 170
    .local v1, "vertex_texture_normal":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->FRAGMENT:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v3, "precision highp float;\nvarying vec2       vTexcoord;\nvarying vec3       vVaryingNormal;\nvarying vec3       vVaryingLightDir;\nuniform mat4       SGWorldInverseTranspose;\nuniform vec4       SGColor;uniform sampler2D  SGTexture;\nuniform sampler2D  SGNormalTexture;\nuniform sampler2D  SGSpecularTexture;\nuniform float      uLightPower;\nuniform vec4       uLightColor;\nuniform float      uCustomLightPower;\nvec4       Ambient = vec4(0.1,0.1,0.1,1.0);\nvec4       Diffuse = vec4(0.7,0.7,0.7,1.0);\nvoid main()\n{\n   float maxVariance = 5.0;\n   float minVariance = maxVariance / 2.0;\n   vec2 scaledCoord = vTexcoord;\n;   vec4 texturecolor = texture2D(SGTexture, scaledCoord);\n   vec3 normalbasis = texture2D(SGNormalTexture, scaledCoord).rgb;\n   vec3 specularbasis = texture2D(SGSpecularTexture, scaledCoord).rgb;\n   vec3 filterColor = texturecolor.rgb;\n   vec3 normalmap = (normalbasis * maxVariance - minVariance);\n   normalmap.xy *= uLightPower;\n   vec3 eyeSpaceNormal = normalize(normalmap);\n   vec3 lightDir = vVaryingLightDir;\n   vec3 eyeVec = vVaryingNormal;\n   vec3 diffusemap = texturecolor.rgb;\n   vec3 aNormal = vVaryingNormal + eyeSpaceNormal;\n   float diffuseIntensity = max(uLightColor.g, dot(aNormal, vVaryingLightDir));\n   vec3 colour = diffuseIntensity * diffusemap + Ambient.rgb * texturecolor.a;\n   vec4 vFragColour = vec4(colour, texturecolor.a);\n   vec3 vReflection = normalize(reflect(-aNormal, vVaryingLightDir));\n   float specularIntensity = max(0.0, dot(aNormal, vReflection));\n   if (diffuseIntensity > uLightColor.b)\n   {\n       float fSpec = pow(specularIntensity, uLightColor.a);\n       vFragColour.rgb += vec3(fSpec * uLightColor.r * uCustomLightPower);\n   }\n   filterColor = vFragColour.rgb * specularbasis.r + diffusemap * (1.0 - specularbasis.r);\n   gl_FragColor = vec4(filterColor,texturecolor.a)* SGColor;\n}\n"

    invoke-direct {v0, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 171
    .local v0, "fragment_texture_normal":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-direct {v2, v1, v0}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;)V

    iput-object v2, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->program_texture_normal:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    .line 177
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    sget-object v3, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    sget-object v4, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;)V

    iput-object v2, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mNormalTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    .line 178
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    sget-object v3, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    sget-object v4, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;)V

    iput-object v2, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mSpecularTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    .line 192
    invoke-direct {p0}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->createProperties()V

    .line 193
    return-void
.end method

.method private createProperties()V
    .locals 1

    .prologue
    .line 201
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mViewDirectionVector4f:Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    .line 202
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mViewDirection:Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;

    .line 203
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightDirectionVector4f:Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    .line 204
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightDirection:Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;

    .line 205
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightPower:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    .line 206
    new-instance v0, Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightColor:Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    .line 207
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightColorProperty:Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;

    .line 208
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    invoke-direct {v0}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mCustomLightPowerProperty:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    .line 209
    return-void
.end method


# virtual methods
.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v0, 0x0

    .line 212
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 216
    if-nez p1, :cond_0

    .line 217
    invoke-virtual {p0, v0}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setNormalBitmap(Landroid/graphics/Bitmap;)V

    .line 218
    invoke-virtual {p0, v0}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setSpecularBitmap(Landroid/graphics/Bitmap;)V

    .line 220
    :cond_0
    return-void
.end method

.method public setCustomLightPower(F)V
    .locals 1
    .param p1, "customLightPower"    # F

    .prologue
    .line 273
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mCustomLightPowerProperty:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->set(F)V

    .line 274
    return-void
.end method

.method public setLightColor(FFFF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "w"    # F

    .prologue
    .line 268
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightColor:Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->set(FFFF)V

    .line 269
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightColorProperty:Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;

    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightColor:Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;->set(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 270
    return-void
.end method

.method public setLightDirection(FFFF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "w"    # F

    .prologue
    .line 258
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightDirectionVector4f:Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->set(FFFF)V

    .line 259
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightDirectionVector4f:Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->normalize()V

    .line 260
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightDirection:Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;

    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightDirectionVector4f:Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;->set(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 261
    return-void
.end method

.method public setLightPower(F)V
    .locals 1
    .param p1, "scalar"    # F

    .prologue
    .line 264
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightPower:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->set(F)V

    .line 265
    return-void
.end method

.method public setNormalBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 223
    if-eqz p1, :cond_0

    .line 224
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mNormalTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 227
    :cond_0
    return-void
.end method

.method public setSpecularBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 230
    if-eqz p1, :cond_0

    .line 231
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mSpecularTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 234
    :cond_0
    return-void
.end method

.method public setUseNormalMap(Z)V
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    .line 237
    if-eqz p1, :cond_0

    .line 238
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->program_texture_normal:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-virtual {p0, v0}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V

    .line 240
    const-string v0, "uViewDirection"

    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mViewDirection:Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 241
    const-string v0, "uLightDirection"

    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightDirection:Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 242
    const-string v0, "uLightPower"

    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightPower:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 243
    const-string v0, "uLightColor"

    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mLightColorProperty:Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 244
    const-string v0, "uCustomLightPower"

    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mCustomLightPowerProperty:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 246
    const-string v0, "SGNormalTexture"

    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mNormalTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 247
    const-string v0, "SGSpecularTexture"

    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mSpecularTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/sgieffect/main/SGISlideEmulation;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 249
    :cond_0
    return-void
.end method

.method public setViewDirection(FFFF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "w"    # F

    .prologue
    .line 252
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mViewDirectionVector4f:Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->set(FFFF)V

    .line 253
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mViewDirectionVector4f:Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/base/SGVector4f;->normalize()V

    .line 254
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mViewDirection:Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;

    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGISlideEmulation;->mViewDirectionVector4f:Lcom/samsung/android/sdk/sgi/base/SGVector4f;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGVector4fProperty;->set(Lcom/samsung/android/sdk/sgi/base/SGVector4f;)V

    .line 255
    return-void
.end method
