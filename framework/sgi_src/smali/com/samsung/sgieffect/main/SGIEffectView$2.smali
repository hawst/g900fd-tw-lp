.class Lcom/samsung/sgieffect/main/SGIEffectView$2;
.super Landroid/os/Handler;
.source "SGIEffectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sgieffect/main/SGIEffectView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/sgieffect/main/SGIEffectView;


# direct methods
.method constructor <init>(Lcom/samsung/sgieffect/main/SGIEffectView;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectView$2;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    .line 784
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 786
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 793
    :goto_0
    return-void

    .line 788
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView$2;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    iget-object v0, v0, Lcom/samsung/sgieffect/main/SGIEffectView;->mPrevSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectView$2;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    iget-object v1, v1, Lcom/samsung/sgieffect/main/SGIEffectView;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/sgieffect/main/SGIEffectView$2;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    iget-object v2, v2, Lcom/samsung/sgieffect/main/SGIEffectView;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->set(FF)V

    .line 789
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView$2;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    iget-object v0, v0, Lcom/samsung/sgieffect/main/SGIEffectView;->mSize:Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget v1, p1, Landroid/os/Message;->arg1:I

    int-to-float v1, v1

    iget v2, p1, Landroid/os/Message;->arg2:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->set(FF)V

    .line 790
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectView$2;->this$0:Lcom/samsung/sgieffect/main/SGIEffectView;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/main/SGIEffectView;->reLayout()Z

    goto :goto_0

    .line 786
    :pswitch_data_0
    .packed-switch 0x9001
        :pswitch_0
    .end packed-switch
.end method
