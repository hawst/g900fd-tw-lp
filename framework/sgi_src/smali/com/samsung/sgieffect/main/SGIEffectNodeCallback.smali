.class public abstract Lcom/samsung/sgieffect/main/SGIEffectNodeCallback;
.super Ljava/lang/Object;
.source "SGIEffectNodeCallback.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract callbackClickAnimation(Lcom/samsung/sgieffect/main/SGIEffectNode;Landroid/view/MotionEvent;I)Z
.end method

.method public abstract callbackExtraTask(Lcom/samsung/sgieffect/main/SGIEffectNode;Landroid/view/MotionEvent;I)I
.end method

.method public abstract callbackHoveringAnimation(Lcom/samsung/sgieffect/main/SGIEffectNode;Landroid/view/MotionEvent;)Z
.end method

.method public abstract callbackInitialize(Lcom/samsung/sgieffect/main/SGIEffectNode;)Z
.end method

.method public abstract callbackLongPressAnimation(Lcom/samsung/sgieffect/main/SGIEffectNode;Landroid/view/MotionEvent;)Z
.end method

.method public abstract callbackPannelInitialize(Lcom/samsung/sgieffect/main/SGIEffectNode;)Z
.end method

.method public abstract callbackSensor(Lcom/samsung/sgieffect/main/SGIEffectNode;[FI)Z
.end method

.method public abstract callbackSensor(Lcom/samsung/sgieffect/main/SGISlideEmulation;[FI)Z
.end method
