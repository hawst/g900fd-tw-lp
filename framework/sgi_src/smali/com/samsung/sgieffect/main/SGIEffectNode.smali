.class public Lcom/samsung/sgieffect/main/SGIEffectNode;
.super Ljava/lang/Object;
.source "SGIEffectNode.java"

# interfaces
.implements Lcom/samsung/sgieffect/main/SGIEffectNodeEvent;


# instance fields
.field private mBaseBitmap:Landroid/graphics/Bitmap;

.field private mBaseRegion:Landroid/graphics/RectF;

.field private mBaseRegionSetted:Z

.field private mBaseSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

.field private mColor:I

.field private mColorBitmap:Landroid/graphics/Bitmap;

.field private mContentBitmap:Landroid/graphics/Bitmap;

.field private mContentRegion:Landroid/graphics/RectF;

.field private mContentRegionSetted:Z

.field private mContentSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

.field private mHasBase:Z

.field private mHasContent:Z

.field private mHasOverlayContent:Z

.field private mIndex:I

.field private mNormalBitmap:Landroid/graphics/Bitmap;

.field private mNormalHeight:F

.field private mOverlayContentBitmap:Landroid/graphics/Bitmap;

.field private mOverlayContentRegion:Landroid/graphics/RectF;

.field private mOverlayContentRegionSetted:Z

.field private mOverlayContentSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

.field private mRegion:Landroid/graphics/RectF;

.field private mSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

.field private mSpecularBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    iput-boolean v2, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseRegionSetted:Z

    .line 244
    iput-boolean v2, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentRegionSetted:Z

    .line 245
    iput-boolean v2, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentRegionSetted:Z

    .line 247
    iput-boolean v2, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mHasContent:Z

    .line 248
    iput-boolean v2, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mHasBase:Z

    .line 249
    iput-boolean v2, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mHasOverlayContent:Z

    .line 251
    iput v2, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mIndex:I

    .line 252
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mNormalHeight:F

    .line 253
    iput v2, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mColor:I

    .line 255
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mRegion:Landroid/graphics/RectF;

    .line 256
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseRegion:Landroid/graphics/RectF;

    .line 257
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentRegion:Landroid/graphics/RectF;

    .line 258
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentRegion:Landroid/graphics/RectF;

    .line 262
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 263
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 264
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 265
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 267
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentBitmap:Landroid/graphics/Bitmap;

    .line 268
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentBitmap:Landroid/graphics/Bitmap;

    .line 269
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mColorBitmap:Landroid/graphics/Bitmap;

    .line 270
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseBitmap:Landroid/graphics/Bitmap;

    .line 271
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mNormalBitmap:Landroid/graphics/Bitmap;

    .line 272
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mSpecularBitmap:Landroid/graphics/Bitmap;

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mColor:I

    .line 20
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mRegion:Landroid/graphics/RectF;

    .line 21
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseRegion:Landroid/graphics/RectF;

    .line 22
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentRegion:Landroid/graphics/RectF;

    .line 23
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentRegion:Landroid/graphics/RectF;

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V
    .locals 0
    .param p1, "slide"    # Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/sgieffect/main/SGIEffectNode;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 29
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V
    .locals 0
    .param p1, "slide"    # Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .param p2, "contentSlide"    # Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/samsung/sgieffect/main/SGIEffectNode;-><init>(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V

    .line 33
    iput-object p2, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 34
    return-void
.end method


# virtual methods
.method public cancelUpload()V
    .locals 0

    .prologue
    .line 150
    return-void
.end method

.method public getBaseGUIImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getBaseRegion()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseRegion:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getBaseSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    return-object v0
.end method

.method public getColor()I
    .locals 1

    .prologue
    .line 191
    iget v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mColor:I

    return v0
.end method

.method public getColorGUIImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mColorBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getContentGUIImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getContentRegion()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentRegion:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getContentSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    return-object v0
.end method

.method public getIndex()I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mIndex:I

    return v0
.end method

.method public getNormalHeight()F
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mNormalHeight:F

    return v0
.end method

.method public getNormalMapGUIImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mNormalBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getOverlayContentGUIImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getOverlayContentRegion()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentRegion:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getOverlayContentSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    return-object v0
.end method

.method public getRegion()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mRegion:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getSlide()Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    return-object v0
.end method

.method public getSpecularMapGUIImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mSpecularBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public hasBase()Z
    .locals 1

    .prologue
    .line 211
    iget-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mHasBase:Z

    return v0
.end method

.method public hasContent()Z
    .locals 1

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mHasContent:Z

    return v0
.end method

.method public hasOverlayContent()Z
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mHasOverlayContent:Z

    return v0
.end method

.method public isBaseRegionSetted()Z
    .locals 1

    .prologue
    .line 203
    iget-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseRegionSetted:Z

    return v0
.end method

.method public isContentRegionSetted()Z
    .locals 1

    .prologue
    .line 199
    iget-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentRegionSetted:Z

    return v0
.end method

.method public isOverlayContentRegionSetted()Z
    .locals 1

    .prologue
    .line 195
    iget-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentRegionSetted:Z

    return v0
.end method

.method public onClick(IILandroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "idx"    # I
    .param p2, "type"    # I
    .param p3, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 310
    const/4 v0, 0x0

    return v0
.end method

.method public onHovering(IILandroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "idx"    # I
    .param p2, "type"    # I
    .param p3, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 322
    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(IILandroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "idx"    # I
    .param p2, "type"    # I
    .param p3, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 316
    const/4 v0, 0x0

    return v0
.end method

.method public recycleBitmaps()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 276
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 278
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentBitmap:Landroid/graphics/Bitmap;

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 282
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 283
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentBitmap:Landroid/graphics/Bitmap;

    .line 286
    :cond_1
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mColorBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 287
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mColorBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 288
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mColorBitmap:Landroid/graphics/Bitmap;

    .line 291
    :cond_2
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 292
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 293
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseBitmap:Landroid/graphics/Bitmap;

    .line 296
    :cond_3
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mNormalBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 297
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mNormalBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 298
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mNormalBitmap:Landroid/graphics/Bitmap;

    .line 301
    :cond_4
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mSpecularBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    .line 302
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mSpecularBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 303
    iput-object v1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mSpecularBitmap:Landroid/graphics/Bitmap;

    .line 305
    :cond_5
    return-void
.end method

.method public setBaseGUI(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "baseBmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseBitmap:Landroid/graphics/Bitmap;

    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mHasBase:Z

    .line 138
    return-void
.end method

.method public setBaseRegion(FFFF)V
    .locals 2
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseRegion:Landroid/graphics/RectF;

    iput p1, v0, Landroid/graphics/RectF;->left:F

    .line 88
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseRegion:Landroid/graphics/RectF;

    iput p2, v0, Landroid/graphics/RectF;->top:F

    .line 89
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseRegion:Landroid/graphics/RectF;

    add-float v1, p1, p3

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 90
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseRegion:Landroid/graphics/RectF;

    add-float v1, p2, p4

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseRegionSetted:Z

    .line 92
    return-void
.end method

.method public setBaseRegion(Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "region"    # Landroid/graphics/RectF;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseRegion:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseRegionSetted:Z

    .line 97
    return-void
.end method

.method public setBaseSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V
    .locals 0
    .param p1, "slide"    # Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mBaseSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 46
    return-void
.end method

.method public setColor(FFFF)V
    .locals 4
    .param p1, "fR"    # F
    .param p2, "fG"    # F
    .param p3, "fB"    # F
    .param p4, "fAlpha"    # F

    .prologue
    const/high16 v3, 0x437f0000    # 255.0f

    .line 118
    mul-float v0, p4, v3

    float-to-int v0, v0

    mul-float v1, p1, v3

    float-to-int v1, v1

    mul-float v2, p2, v3

    float-to-int v2, v2

    mul-float/2addr v3, p3

    float-to-int v3, v3

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    iput v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mColor:I

    .line 119
    return-void
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mColor:I

    .line 115
    return-void
.end method

.method public setColorGUI(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "colorBmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 132
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mColorBitmap:Landroid/graphics/Bitmap;

    .line 133
    return-void
.end method

.method public setContentGUI(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "colorBmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentBitmap:Landroid/graphics/Bitmap;

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mHasContent:Z

    .line 124
    return-void
.end method

.method public setContentRegion(FFFF)V
    .locals 2
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentRegion:Landroid/graphics/RectF;

    iput p1, v0, Landroid/graphics/RectF;->left:F

    .line 80
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentRegion:Landroid/graphics/RectF;

    iput p2, v0, Landroid/graphics/RectF;->top:F

    .line 81
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentRegion:Landroid/graphics/RectF;

    add-float v1, p1, p3

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 82
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentRegion:Landroid/graphics/RectF;

    add-float v1, p2, p4

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentRegionSetted:Z

    .line 84
    return-void
.end method

.method public setContentRegion(Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "region"    # Landroid/graphics/RectF;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentRegion:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentRegionSetted:Z

    .line 76
    return-void
.end method

.method public setContentSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V
    .locals 0
    .param p1, "slide"    # Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mContentSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 50
    return-void
.end method

.method public setIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mIndex:I

    .line 59
    return-void
.end method

.method public setNormalHeight(F)V
    .locals 0
    .param p1, "height"    # F

    .prologue
    .line 37
    iput p1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mNormalHeight:F

    .line 38
    return-void
.end method

.method public setNormalMapGUI(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "normalmapBmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mNormalBitmap:Landroid/graphics/Bitmap;

    .line 142
    return-void
.end method

.method public setOverlayContentGUI(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "colorBmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentBitmap:Landroid/graphics/Bitmap;

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mHasOverlayContent:Z

    .line 129
    return-void
.end method

.method public setOverlayContentRegion(FFFF)V
    .locals 2
    .param p1, "left"    # F
    .param p2, "top"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentRegion:Landroid/graphics/RectF;

    iput p1, v0, Landroid/graphics/RectF;->left:F

    .line 106
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentRegion:Landroid/graphics/RectF;

    iput p2, v0, Landroid/graphics/RectF;->top:F

    .line 107
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentRegion:Landroid/graphics/RectF;

    add-float v1, p1, p3

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 108
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentRegion:Landroid/graphics/RectF;

    add-float v1, p2, p4

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentRegionSetted:Z

    .line 110
    return-void
.end method

.method public setOverlayContentRegion(Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "region"    # Landroid/graphics/RectF;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentRegion:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentRegionSetted:Z

    .line 102
    return-void
.end method

.method public setOverlayContentSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V
    .locals 0
    .param p1, "slide"    # Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mOverlayContentSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 54
    return-void
.end method

.method public setRegion(FFFF)V
    .locals 2
    .param p1, "fLeft"    # F
    .param p2, "fTop"    # F
    .param p3, "fWidth"    # F
    .param p4, "fHeight"    # F

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mRegion:Landroid/graphics/RectF;

    iput p1, v0, Landroid/graphics/RectF;->left:F

    .line 67
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mRegion:Landroid/graphics/RectF;

    iput p2, v0, Landroid/graphics/RectF;->top:F

    .line 68
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mRegion:Landroid/graphics/RectF;

    add-float v1, p1, p3

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 69
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mRegion:Landroid/graphics/RectF;

    add-float v1, p2, p4

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 70
    return-void
.end method

.method public setRegion(Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "region"    # Landroid/graphics/RectF;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mRegion:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 63
    return-void
.end method

.method public setSlide(Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V
    .locals 0
    .param p1, "slide"    # Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mSlide:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 42
    return-void
.end method

.method public setSpecularMapGUI(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "specularmapBmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/samsung/sgieffect/main/SGIEffectNode;->mSpecularBitmap:Landroid/graphics/Bitmap;

    .line 146
    return-void
.end method
