.class Lcom/samsung/sgieffect/main/SGIEffectView$1;
.super Ljava/lang/Object;
.source "SGIEffectView.java"

# interfaces
.implements Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sgieffect/main/SGIEffectView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public onCancelled(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    .prologue
    .line 618
    return-void
.end method

.method public onDiscarded(I)V
    .locals 0
    .param p1, "arg0"    # I

    .prologue
    .line 650
    return-void
.end method

.method public onFinished(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    .prologue
    .line 622
    sget-object v0, Lcom/samsung/sgieffect/main/SGIEffectView;->mListener:Lcom/samsung/sgieffect/main/SGIEffectListener;

    if-eqz v0, :cond_0

    .line 627
    # getter for: Lcom/samsung/sgieffect/main/SGIEffectView;->mTypeExtraState:I
    invoke-static {}, Lcom/samsung/sgieffect/main/SGIEffectView;->access$0()I

    move-result v0

    if-nez v0, :cond_0

    .line 628
    sget-object v0, Lcom/samsung/sgieffect/main/SGIEffectView;->mListener:Lcom/samsung/sgieffect/main/SGIEffectListener;

    invoke-interface {v0}, Lcom/samsung/sgieffect/main/SGIEffectListener;->onAnimateHideFinished()V

    .line 632
    :cond_0
    return-void
.end method

.method public onRepeated(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    .prologue
    .line 638
    return-void
.end method

.method public onStarted(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    .prologue
    .line 644
    return-void
.end method
