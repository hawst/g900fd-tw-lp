.class public final enum Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;
.super Ljava/lang/Enum;
.source "SGIEffectView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sgieffect/main/SGIEffectView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RequestType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

.field public static final enum REQUEST_CLICK_RESULT:Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

.field public static final enum REQUEST_HOVER_RESULT:Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

.field public static final enum REQUEST_KEY_RESULT:Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

.field public static final enum REQUEST_LONGPRESS_RESULT:Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 43
    new-instance v0, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    const-string v1, "REQUEST_CLICK_RESULT"

    invoke-direct {v0, v1, v2}, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;->REQUEST_CLICK_RESULT:Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    .line 44
    new-instance v0, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    const-string v1, "REQUEST_LONGPRESS_RESULT"

    invoke-direct {v0, v1, v3}, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;->REQUEST_LONGPRESS_RESULT:Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    .line 45
    new-instance v0, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    const-string v1, "REQUEST_HOVER_RESULT"

    invoke-direct {v0, v1, v4}, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;->REQUEST_HOVER_RESULT:Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    .line 46
    new-instance v0, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    const-string v1, "REQUEST_KEY_RESULT"

    invoke-direct {v0, v1, v5}, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;->REQUEST_KEY_RESULT:Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    .line 42
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    sget-object v1, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;->REQUEST_CLICK_RESULT:Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;->REQUEST_LONGPRESS_RESULT:Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;->REQUEST_HOVER_RESULT:Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;->REQUEST_KEY_RESULT:Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;->ENUM$VALUES:[Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;
    .locals 1

    .prologue
    .line 1
    const-class v0, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;->ENUM$VALUES:[Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    array-length v1, v0

    new-array v2, v1, [Lcom/samsung/sgieffect/main/SGIEffectView$RequestType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
