.class public Lcom/samsung/sgieffect/main/CircleLayerImage$CircleGeometryGenerator;
.super Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;
.source "CircleLayerImage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/sgieffect/main/CircleLayerImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CircleGeometryGenerator"
.end annotation


# instance fields
.field private final mDensity:I

.field final synthetic this$0:Lcom/samsung/sgieffect/main/CircleLayerImage;


# direct methods
.method public constructor <init>(Lcom/samsung/sgieffect/main/CircleLayerImage;I)V
    .locals 0
    .param p2, "circleDensity"    # I

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/sgieffect/main/CircleLayerImage$CircleGeometryGenerator;->this$0:Lcom/samsung/sgieffect/main/CircleLayerImage;

    .line 28
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;-><init>()V

    .line 29
    iput p2, p0, Lcom/samsung/sgieffect/main/CircleLayerImage$CircleGeometryGenerator;->mDensity:I

    .line 30
    return-void
.end method


# virtual methods
.method public generate(FLcom/samsung/android/sdk/sgi/render/SGGeometry;FF)Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .locals 14
    .param p1, "aParam"    # F
    .param p2, "aPreviousGeometry"    # Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .param p3, "aHeight"    # F
    .param p4, "aWidth"    # F

    .prologue
    .line 40
    new-instance v4, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;

    sget-object v10, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->TRIANGLE_FAN:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    sget-object v11, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    iget v12, p0, Lcom/samsung/sgieffect/main/CircleLayerImage$CircleGeometryGenerator;->mDensity:I

    add-int/lit8 v12, v12, 0x2

    invoke-direct {v4, v10, v11, v12}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;-><init>(Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 42
    .local v4, "indexBuffer":Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;
    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->getShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v6

    .line 43
    .local v6, "sib":Ljava/nio/ShortBuffer;
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v6, v10, v11}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    .line 45
    new-instance v5, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/4 v10, 0x3

    sget-object v11, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v12, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    iget v13, p0, Lcom/samsung/sgieffect/main/CircleLayerImage$CircleGeometryGenerator;->mDensity:I

    add-int/lit8 v13, v13, 0x2

    invoke-direct {v5, v10, v11, v12, v13}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 48
    .local v5, "positionBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual {v5}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    .line 49
    .local v0, "fpb":Ljava/nio/FloatBuffer;
    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 51
    const/4 v10, 0x0

    const/high16 v11, 0x3f000000    # 0.5f

    invoke-virtual {v0, v10, v11}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    .line 52
    const/4 v10, 0x1

    const/high16 v11, 0x3f000000    # 0.5f

    invoke-virtual {v0, v10, v11}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    .line 53
    const/4 v10, 0x2

    const/4 v11, 0x0

    invoke-virtual {v0, v10, v11}, Ljava/nio/FloatBuffer;->put(IF)Ljava/nio/FloatBuffer;

    .line 56
    new-instance v7, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/4 v10, 0x2

    sget-object v11, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v12, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    iget v13, p0, Lcom/samsung/sgieffect/main/CircleLayerImage$CircleGeometryGenerator;->mDensity:I

    add-int/lit8 v13, v13, 0x2

    invoke-direct {v7, v10, v11, v12, v13}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 58
    .local v7, "texcoordBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    .line 60
    .local v1, "ftb":Ljava/nio/FloatBuffer;
    const/high16 v10, 0x3f000000    # 0.5f

    invoke-virtual {v1, v10}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 61
    const/high16 v10, 0x3f000000    # 0.5f

    invoke-virtual {v1, v10}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 64
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget v10, p0, Lcom/samsung/sgieffect/main/CircleLayerImage$CircleGeometryGenerator;->mDensity:I

    if-le v3, v10, :cond_0

    .line 81
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGGeometry;

    invoke-direct {v2, v4}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;-><init>(Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;)V

    .line 82
    .local v2, "geometry":Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    const-string v10, "SGTextureCoords"

    invoke-virtual {v2, v10, v7}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 83
    const-string v10, "SGPositions"

    invoke-virtual {v2, v10, v5}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 85
    return-object v2

    .line 66
    .end local v2    # "geometry":Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    :cond_0
    const/16 v10, 0x168

    iget v11, p0, Lcom/samsung/sgieffect/main/CircleLayerImage$CircleGeometryGenerator;->mDensity:I

    div-int/2addr v10, v11

    mul-int/2addr v10, v3

    int-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v10, v12

    double-to-float v10, v10

    const/high16 v11, 0x3f000000    # 0.5f

    add-float v8, v10, v11

    .line 68
    .local v8, "x":F
    const/16 v10, 0x168

    iget v11, p0, Lcom/samsung/sgieffect/main/CircleLayerImage$CircleGeometryGenerator;->mDensity:I

    div-int/2addr v10, v11

    mul-int/2addr v10, v3

    int-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v10, v12

    double-to-float v10, v10

    const/high16 v11, 0x3f000000    # 0.5f

    add-float v9, v10, v11

    .line 70
    .local v9, "y":F
    invoke-virtual {v0, v8}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 71
    invoke-virtual {v0, v9}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 72
    const/4 v10, 0x0

    invoke-virtual {v0, v10}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 75
    add-int/lit8 v10, v3, 0x1

    int-to-short v10, v10

    add-int/lit8 v11, v3, 0x1

    int-to-short v11, v11

    invoke-virtual {v6, v10, v11}, Ljava/nio/ShortBuffer;->put(IS)Ljava/nio/ShortBuffer;

    .line 78
    invoke-virtual {v1, v8}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 79
    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v9

    invoke-virtual {v1, v10}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 64
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method protected isBelongsToGeometry(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Z
    .locals 1
    .param p1, "aCheckDot"    # Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method
