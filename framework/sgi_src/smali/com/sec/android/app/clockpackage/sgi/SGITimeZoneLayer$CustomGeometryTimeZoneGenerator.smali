.class Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;
.super Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;
.source "SGITimeZoneLayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomGeometryTimeZoneGenerator"
.end annotation


# instance fields
.field private mIndex:I

.field final synthetic this$0:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;I)V
    .locals 0
    .param p2, "index"    # I

    .prologue
    .line 205
    iput-object p1, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    .line 206
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;-><init>()V

    .line 207
    iput p2, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;->mIndex:I

    .line 208
    return-void
.end method


# virtual methods
.method public generate(FLcom/samsung/android/sdk/sgi/render/SGGeometry;FF)Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .locals 27
    .param p1, "aParam"    # F
    .param p2, "aPreviousGeometry"    # Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .param p3, "aHeight"    # F
    .param p4, "aWidth"    # F

    .prologue
    .line 217
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;->mIndex:I

    move/from16 v23, v0

    if-gez v23, :cond_0

    .line 218
    new-instance v10, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;

    sget-object v23, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->TRIANGLES:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    sget-object v24, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    const/16 v25, 0x3

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-direct {v10, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;-><init>(Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 219
    .local v10, "indexBuffer":Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;
    invoke-virtual {v10}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->getShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v11

    .line 220
    .local v11, "indices":Ljava/nio/ShortBuffer;
    invoke-virtual {v11}, Ljava/nio/ShortBuffer;->clear()Ljava/nio/Buffer;

    .line 221
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 222
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 223
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 225
    new-instance v15, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/16 v23, 0x3

    sget-object v24, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v25, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    const/16 v26, 0x1

    move/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    move/from16 v3, v26

    invoke-direct {v15, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 226
    .local v15, "positionBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual {v15}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v16

    .line 227
    .local v16, "positions":Ljava/nio/FloatBuffer;
    invoke-virtual/range {v16 .. v16}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 228
    const/16 v23, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 229
    const/16 v23, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 230
    const/16 v23, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 232
    new-instance v18, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/16 v23, 0x2

    sget-object v24, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v25, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    const/16 v26, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v23

    move-object/from16 v2, v24

    move-object/from16 v3, v25

    move/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 233
    .local v18, "texcoordBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v17

    .line 234
    .local v17, "texcoord":Ljava/nio/FloatBuffer;
    invoke-virtual/range {v17 .. v17}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 235
    const/16 v23, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 236
    const/16 v23, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 238
    new-instance v8, Lcom/samsung/android/sdk/sgi/render/SGGeometry;

    invoke-direct {v8, v10}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;-><init>(Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;)V

    .line 239
    .local v8, "geometry":Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    const-string v23, "SGPositions"

    move-object/from16 v0, v23

    invoke-virtual {v8, v0, v15}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 240
    const-string v23, "SGTextureCoords"

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 302
    :goto_0
    return-object v8

    .line 244
    .end local v8    # "geometry":Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .end local v10    # "indexBuffer":Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;
    .end local v11    # "indices":Ljava/nio/ShortBuffer;
    .end local v15    # "positionBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .end local v16    # "positions":Ljava/nio/FloatBuffer;
    .end local v17    # "texcoord":Ljava/nio/FloatBuffer;
    .end local v18    # "texcoordBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    :cond_0
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->context:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v23

    const-string v24, "geometry/timezones.bin"

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v12

    .line 245
    .local v12, "is":Ljava/io/InputStream;
    new-instance v6, Ljava/io/DataInputStream;

    invoke-direct {v6, v12}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 247
    .local v6, "dataStream":Ljava/io/DataInputStream;
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readInt()I

    move-result v19

    .line 248
    .local v19, "timezoneCount":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;->mIndex:I

    move/from16 v23, v0

    move/from16 v0, v23

    move/from16 v1, v19

    if-lt v0, v1, :cond_1

    .line 249
    new-instance v23, Ljava/lang/RuntimeException;

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "Timezone index is greater than: "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v23
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    .end local v6    # "dataStream":Ljava/io/DataInputStream;
    .end local v12    # "is":Ljava/io/InputStream;
    .end local v19    # "timezoneCount":I
    :catch_0
    move-exception v7

    .line 305
    .local v7, "e":Ljava/lang/Exception;
    new-instance v23, Ljava/lang/RuntimeException;

    const-string v24, "Couldn\'t read geometry file. (assets/geometry/timezones.bin)"

    invoke-direct/range {v23 .. v24}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v23

    .line 252
    .end local v7    # "e":Ljava/lang/Exception;
    .restart local v6    # "dataStream":Ljava/io/DataInputStream;
    .restart local v12    # "is":Ljava/io/InputStream;
    .restart local v19    # "timezoneCount":I
    :cond_1
    :try_start_1
    move/from16 v0, v19

    new-array v0, v0, [S

    move-object/from16 v20, v0

    .line 253
    .local v20, "triangleCounts":[S
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    move/from16 v0, v19

    if-lt v9, v0, :cond_2

    .line 257
    const/4 v9, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;->mIndex:I

    move/from16 v23, v0

    move/from16 v0, v23

    if-lt v9, v0, :cond_3

    .line 262
    new-instance v10, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;

    sget-object v23, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->TRIANGLES:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    sget-object v24, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;->mIndex:I

    move/from16 v25, v0

    aget-short v25, v20, v25

    mul-int/lit8 v25, v25, 0x3

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-direct {v10, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;-><init>(Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 263
    .restart local v10    # "indexBuffer":Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;
    invoke-virtual {v10}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->getShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v11

    .line 266
    .restart local v11    # "indices":Ljava/nio/ShortBuffer;
    new-instance v15, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/16 v23, 0x3

    sget-object v24, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v25, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;->mIndex:I

    move/from16 v26, v0

    aget-short v26, v20, v26

    mul-int/lit8 v26, v26, 0x3

    move/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    move/from16 v3, v26

    invoke-direct {v15, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 267
    .restart local v15    # "positionBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual {v15}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v16

    .line 268
    .restart local v16    # "positions":Ljava/nio/FloatBuffer;
    invoke-virtual/range {v16 .. v16}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 271
    new-instance v13, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/16 v23, 0x3

    sget-object v24, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v25, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;->mIndex:I

    move/from16 v26, v0

    aget-short v26, v20, v26

    mul-int/lit8 v26, v26, 0x3

    move/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    move/from16 v3, v26

    invoke-direct {v13, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 272
    .local v13, "normalBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual {v13}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v14

    .line 273
    .local v14, "normals":Ljava/nio/FloatBuffer;
    invoke-virtual {v14}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 276
    new-instance v18, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/16 v23, 0x2

    sget-object v24, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v25, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;->mIndex:I

    move/from16 v26, v0

    aget-short v26, v20, v26

    mul-int/lit8 v26, v26, 0x3

    move-object/from16 v0, v18

    move/from16 v1, v23

    move-object/from16 v2, v24

    move-object/from16 v3, v25

    move/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 277
    .restart local v18    # "texcoordBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v17

    .line 278
    .restart local v17    # "texcoord":Ljava/nio/FloatBuffer;
    invoke-virtual/range {v17 .. v17}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 280
    const/4 v9, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;->mIndex:I

    move/from16 v23, v0

    aget-short v23, v20, v23

    mul-int/lit8 v23, v23, 0x3

    move/from16 v0, v23

    if-lt v9, v0, :cond_4

    .line 298
    new-instance v8, Lcom/samsung/android/sdk/sgi/render/SGGeometry;

    invoke-direct {v8, v10}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;-><init>(Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;)V

    .line 299
    .restart local v8    # "geometry":Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    const-string v23, "SGTextureCoords"

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v8, v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 300
    const-string v23, "SGPositions"

    move-object/from16 v0, v23

    invoke-virtual {v8, v0, v15}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 301
    const-string v23, "SGNormals"

    move-object/from16 v0, v23

    invoke-virtual {v8, v0, v13}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    goto/16 :goto_0

    .line 254
    .end local v8    # "geometry":Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .end local v10    # "indexBuffer":Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;
    .end local v11    # "indices":Ljava/nio/ShortBuffer;
    .end local v13    # "normalBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .end local v14    # "normals":Ljava/nio/FloatBuffer;
    .end local v15    # "positionBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .end local v16    # "positions":Ljava/nio/FloatBuffer;
    .end local v17    # "texcoord":Ljava/nio/FloatBuffer;
    .end local v18    # "texcoordBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    :cond_2
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readShort()S

    move-result v23

    aput-short v23, v20, v9

    .line 253
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    .line 258
    :cond_3
    aget-short v23, v20, v9

    mul-int/lit8 v23, v23, 0x3

    mul-int/lit8 v23, v23, 0x8

    mul-int/lit8 v23, v23, 0x4

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v6, v0, v1}, Ljava/io/DataInputStream;->skip(J)J

    .line 257
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2

    .line 281
    .restart local v10    # "indexBuffer":Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;
    .restart local v11    # "indices":Ljava/nio/ShortBuffer;
    .restart local v13    # "normalBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .restart local v14    # "normals":Ljava/nio/FloatBuffer;
    .restart local v15    # "positionBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .restart local v16    # "positions":Ljava/nio/FloatBuffer;
    .restart local v17    # "texcoord":Ljava/nio/FloatBuffer;
    .restart local v18    # "texcoordBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    :cond_4
    int-to-short v0, v9

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 283
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readFloat()F

    move-result v23

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 284
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readFloat()F

    move-result v23

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 285
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readFloat()F

    move-result v23

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 287
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readFloat()F

    move-result v23

    move/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 288
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readFloat()F

    move-result v23

    move/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 289
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readFloat()F

    move-result v23

    move/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 291
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readFloat()F

    move-result v21

    .line 292
    .local v21, "u":F
    invoke-virtual {v6}, Ljava/io/DataInputStream;->readFloat()F

    move-result v22

    .line 293
    .local v22, "v":F
    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 294
    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 280
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_3
.end method

.method protected isBelongsToGeometry(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Z
    .locals 1
    .param p1, "aCheckDot"    # Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .prologue
    .line 212
    const/4 v0, 0x0

    return v0
.end method
