.class Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;
.super Ljava/lang/Object;
.source "SGIPlayer.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->processGestureEvent(FF)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

.field private final synthetic val$fromValueH:F

.field private final synthetic val$fromValueV:F

.field private final synthetic val$toValueH:F

.field private final synthetic val$toValueV:F


# direct methods
.method constructor <init>(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;FFFF)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iput p2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->val$fromValueH:F

    iput p3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->val$toValueH:F

    iput p4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->val$fromValueV:F

    iput p5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->val$toValueV:F

    .line 381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 8
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    const v7, 0x3f8ccccd    # 1.1f

    const/high16 v6, 0x3f800000    # 1.0f

    const v5, -0x40733333    # -1.1f

    .line 384
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 385
    .local v0, "value":F
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->val$fromValueH:F

    mul-float/2addr v2, v0

    iget v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->val$toValueH:F

    sub-float v4, v6, v0

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, v1, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    .line 386
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->val$fromValueV:F

    mul-float/2addr v2, v0

    iget v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->val$toValueV:F

    sub-float v4, v6, v0

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, v1, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    .line 387
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v1, v1, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    cmpl-float v1, v1, v7

    if-lez v1, :cond_1

    .line 388
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iput v7, v1, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    .line 394
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGlobeLayer:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$6(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget-object v2, v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v5, v5, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setRotation([FFFF)V

    .line 395
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mBillboardLayer:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$5(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget-object v2, v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v5, v5, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setRotation([FFFF)V

    .line 396
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAtmosphereLayer:Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$8(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget-object v2, v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v5, v5, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setRotation([FFFF)V

    .line 397
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mLatLongLayer:Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$9(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget-object v2, v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v5, v5, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setRotation([FFFF)V

    .line 398
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneLayer:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$10(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget-object v2, v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v5, v5, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setRotation([FFFF)V

    .line 400
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$11(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/samsung/android/sdk/sgi/vi/SGView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/vi/SGView;->invalidate()V

    .line 401
    return-void

    .line 389
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v1, v1, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    cmpg-float v1, v1, v5

    if-gez v1, :cond_0

    .line 390
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iput v5, v1, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    goto/16 :goto_0
.end method
