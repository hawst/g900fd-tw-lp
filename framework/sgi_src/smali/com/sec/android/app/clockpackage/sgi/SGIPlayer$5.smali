.class Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;
.super Ljava/lang/Object;
.source "SGIPlayer.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->updateGlobeZoomLevel(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 216
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 217
    .local v0, "value":F
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iput v0, v1, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    .line 220
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGlobeLayer:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$6(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget-object v2, v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F
    invoke-static {v4}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$7(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)F

    move-result v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v5, v5, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setDistance([FFFF)V

    .line 221
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mBillboardLayer:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$5(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget-object v2, v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F
    invoke-static {v4}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$7(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)F

    move-result v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v5, v5, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setDistance([FFFF)V

    .line 222
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAtmosphereLayer:Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$8(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget-object v2, v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F
    invoke-static {v4}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$7(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)F

    move-result v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v5, v5, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setDistance([FFFF)V

    .line 223
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mLatLongLayer:Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$9(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget-object v2, v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F
    invoke-static {v4}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$7(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)F

    move-result v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v5, v5, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setDistance([FFFF)V

    .line 224
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneLayer:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$10(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget-object v2, v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F
    invoke-static {v4}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$7(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)F

    move-result v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v5, v5, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setDistance([FFFF)V

    .line 226
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$11(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/samsung/android/sdk/sgi/vi/SGView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/vi/SGView;->invalidate()V

    .line 227
    return-void
.end method
