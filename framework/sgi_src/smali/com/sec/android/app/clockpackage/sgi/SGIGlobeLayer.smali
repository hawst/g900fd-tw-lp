.class public Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;
.super Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
.source "SGIGlobeLayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;
    }
.end annotation


# static fields
.field static final FRAGMENT_SHADER:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D dayTexture;\nvarying vec2 vTextureCoords;\nvarying float nightColor;void main(){\n\tvec4 dayColor = texture2D(dayTexture, vTextureCoords);\n\tgl_FragColor = dayColor * nightColor;}\n"

.field static final VERTEX_SHADER:Ljava/lang/String; = "attribute vec3 SGPositions;\nattribute vec3 SGNormals;\nattribute vec2 SGTextureCoords;\nuniform mat4 MyWorldViewProjection;\nuniform vec3 lightDirec;\nvarying vec2 vTextureCoords;\nvarying float nightColor;void main(){\n\tvTextureCoords = SGTextureCoords;\n\tgl_Position = MyWorldViewProjection * vec4(SGPositions, 1.0);\n\tnightColor = 0.5 + 0.5*smoothstep(0.0, 0.5, dot(SGNormals, lightDirec) +0.4);}\n"


# instance fields
.field calendar:Ljava/util/Calendar;

.field handler:Landroid/os/Handler;

.field private mBlendingProperty:Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

.field private mCompressedDayTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

.field private mCullfaceProperty:Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;

.field private mCurrentDistance:F

.field private mDayTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

.field private mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

.field private mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

.field private mProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

.field private mlightDirec:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

.field projMat:[F

.field tempMat:[F

.field viewMat:[F


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const/16 v7, 0x32

    const/4 v6, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    const/16 v4, 0x10

    .line 113
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 76
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    .line 77
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mlightDirec:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    .line 79
    new-array v3, v4, [F

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->projMat:[F

    .line 80
    new-array v3, v4, [F

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->viewMat:[F

    .line 81
    new-array v3, v4, [F

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    .line 83
    const-string v3, "Etc/Greenwich"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->calendar:Ljava/util/Calendar;

    .line 86
    const/4 v3, 0x0

    iput v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mCurrentDistance:F

    .line 114
    new-instance v1, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;

    invoke-direct {v1, p0, v7, v7, v5}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;-><init>(Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;IIF)V

    .line 115
    .local v1, "sphereGeometry":Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setGeometryGenerator(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V

    .line 116
    invoke-virtual {p0, v5}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setGeometryGeneratorParam(F)V

    .line 118
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    sget-object v3, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->VERTEX:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v4, "attribute vec3 SGPositions;\nattribute vec3 SGNormals;\nattribute vec2 SGTextureCoords;\nuniform mat4 MyWorldViewProjection;\nuniform vec3 lightDirec;\nvarying vec2 vTextureCoords;\nvarying float nightColor;void main(){\n\tvTextureCoords = SGTextureCoords;\n\tgl_Position = MyWorldViewProjection * vec4(SGPositions, 1.0);\n\tnightColor = 0.5 + 0.5*smoothstep(0.0, 0.5, dot(SGNormals, lightDirec) +0.4);}\n"

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 119
    .local v2, "vertex":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    sget-object v3, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->FRAGMENT:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v4, "precision mediump float;\nuniform sampler2D dayTexture;\nvarying vec2 vTextureCoords;\nvarying float nightColor;void main(){\n\tvec4 dayColor = texture2D(dayTexture, vTextureCoords);\n\tgl_FragColor = dayColor * nightColor;}\n"

    invoke-direct {v0, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 120
    .local v0, "fragment":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-direct {v3, v2, v0}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;)V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    .line 122
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    sget-object v4, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    sget-object v5, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;)V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mDayTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    .line 123
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    sget-object v4, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    sget-object v5, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;)V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mCompressedDayTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    .line 125
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    .line 126
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->setDepthTestEnabled(Z)V

    .line 127
    const-string v3, "SGDepthProperty"

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 129
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mBlendingProperty:Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

    .line 130
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mBlendingProperty:Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->setAlphaBlendingEnabled(Z)V

    .line 131
    const-string v3, "SGAlphaBlend"

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mBlendingProperty:Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 133
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mCullfaceProperty:Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;

    .line 134
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mCullfaceProperty:Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;

    sget-object v4, Lcom/samsung/android/sdk/sgi/render/SGFrontType;->CW:Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->setFrontType(Lcom/samsung/android/sdk/sgi/render/SGFrontType;)V

    .line 135
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mCullfaceProperty:Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->setFaceCullingEnabled(Z)V

    .line 136
    const-string v3, "SGCullface"

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mCullfaceProperty:Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 138
    invoke-virtual {p0}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->updateNightImage()V

    .line 140
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V

    .line 141
    return-void
.end method

.method public static perspectiveM([FIFFFF)V
    .locals 8
    .param p0, "m"    # [F
    .param p1, "offset"    # I
    .param p2, "fovy"    # F
    .param p3, "aspect"    # F
    .param p4, "zNear"    # F
    .param p5, "zFar"    # F

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 89
    float-to-double v2, p2

    const-wide v4, 0x3f81df46a2529d39L    # 0.008726646259971648

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v2, v2

    div-float v0, v7, v2

    .line 90
    .local v0, "f":F
    sub-float v2, p4, p5

    div-float v1, v7, v2

    .line 92
    .local v1, "rangeReciprocal":F
    add-int/lit8 v2, p1, 0x0

    div-float v3, v0, p3

    aput v3, p0, v2

    .line 93
    add-int/lit8 v2, p1, 0x1

    aput v6, p0, v2

    .line 94
    add-int/lit8 v2, p1, 0x2

    aput v6, p0, v2

    .line 95
    add-int/lit8 v2, p1, 0x3

    aput v6, p0, v2

    .line 97
    add-int/lit8 v2, p1, 0x4

    aput v6, p0, v2

    .line 98
    add-int/lit8 v2, p1, 0x5

    aput v0, p0, v2

    .line 99
    add-int/lit8 v2, p1, 0x6

    aput v6, p0, v2

    .line 100
    add-int/lit8 v2, p1, 0x7

    aput v6, p0, v2

    .line 102
    add-int/lit8 v2, p1, 0x8

    aput v6, p0, v2

    .line 103
    add-int/lit8 v2, p1, 0x9

    aput v6, p0, v2

    .line 104
    add-int/lit8 v2, p1, 0xa

    add-float v3, p5, p4

    mul-float/2addr v3, v1

    aput v3, p0, v2

    .line 105
    add-int/lit8 v2, p1, 0xb

    const/high16 v3, -0x40800000    # -1.0f

    aput v3, p0, v2

    .line 107
    add-int/lit8 v2, p1, 0xc

    aput v6, p0, v2

    .line 108
    add-int/lit8 v2, p1, 0xd

    aput v6, p0, v2

    .line 109
    add-int/lit8 v2, p1, 0xe

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, p5

    mul-float/2addr v3, p4

    mul-float/2addr v3, v1

    aput v3, p0, v2

    .line 110
    add-int/lit8 v2, p1, 0xf

    aput v6, p0, v2

    .line 111
    return-void
.end method

.method private setCurrentDistance(F)V
    .locals 0
    .param p1, "currentDistance"    # F

    .prologue
    .line 202
    iput p1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mCurrentDistance:F

    .line 203
    return-void
.end method


# virtual methods
.method public getCurrentDistance()F
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mCurrentDistance:F

    return v0
.end method

.method public setCompressedDayImage(Ljava/nio/channels/FileChannel;)V
    .locals 2
    .param p1, "fc"    # Ljava/nio/channels/FileChannel;

    .prologue
    .line 216
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGCompressedTextureFactory;->createTexture(Ljava/nio/channels/FileChannel;)Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mCompressedDayTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    .line 217
    const-string v0, "dayTexture"

    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mCompressedDayTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 218
    return-void
.end method

.method public setDistance([FFFF)V
    .locals 20
    .param p1, "eye"    # [F
    .param p2, "distance"    # F
    .param p3, "hRotation"    # F
    .param p4, "vRotation"    # F

    .prologue
    .line 176
    const/4 v2, 0x0

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 177
    const/4 v2, 0x1

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    aput v3, p1, v2

    .line 178
    const/4 v2, 0x2

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 179
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->viewMat:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, p1, v4

    const/4 v5, 0x1

    aget v5, p1, v5

    const/4 v6, 0x2

    aget v6, p1, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 181
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setCurrentDistance(F)V

    .line 183
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 184
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->projMat:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->viewMat:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 185
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 186
    const-string v2, "MyWorldViewProjection"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 187
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "dayImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mDayTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 207
    const-string v0, "dayTexture"

    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mDayTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 208
    return-void
.end method

.method public setRotation([FFFF)V
    .locals 20
    .param p1, "eye"    # [F
    .param p2, "distance"    # F
    .param p3, "hRotation"    # F
    .param p4, "vRotation"    # F

    .prologue
    .line 162
    const/4 v2, 0x0

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 163
    const/4 v2, 0x1

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    aput v3, p1, v2

    .line 164
    const/4 v2, 0x2

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 165
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->viewMat:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, p1, v4

    const/4 v5, 0x1

    aget v5, p1, v5

    const/4 v6, 0x2

    aget v6, p1, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 167
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setCurrentDistance(F)V

    .line 169
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 170
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->projMat:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->viewMat:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 172
    const-string v2, "MyWorldViewProjection"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 173
    return-void
.end method

.method public setSize(IIFFFF[FFFF)V
    .locals 20
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "FOVY"    # F
    .param p4, "ASPECT_RATIO"    # F
    .param p5, "NEAR_PLANE"    # F
    .param p6, "FAR_PLANE"    # F
    .param p7, "eye"    # [F
    .param p8, "distance"    # F
    .param p9, "hRotation"    # F
    .param p10, "vRotation"    # F

    .prologue
    .line 144
    move/from16 v0, p1

    int-to-float v2, v0

    move/from16 v0, p2

    int-to-float v3, v0

    move-object/from16 v0, p0

    invoke-super {v0, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 146
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->projMat:[F

    const/4 v3, 0x0

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    invoke-static/range {v2 .. v7}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->perspectiveM([FIFFFF)V

    .line 148
    const/4 v2, 0x0

    move/from16 v0, p9

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p8

    move/from16 v0, p10

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p7, v2

    .line 149
    const/4 v2, 0x1

    move/from16 v0, p10

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p8

    aput v3, p7, v2

    .line 150
    const/4 v2, 0x2

    move/from16 v0, p9

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p8

    move/from16 v0, p10

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p7, v2

    .line 151
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->viewMat:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, p7, v4

    const/4 v5, 0x1

    aget v5, p7, v5

    const/4 v6, 0x2

    aget v6, p7, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 153
    move-object/from16 v0, p0

    move/from16 v1, p8

    invoke-direct {v0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setCurrentDistance(F)V

    .line 155
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 156
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->projMat:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->viewMat:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 157
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->tempMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 158
    const-string v2, "MyWorldViewProjection"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 159
    return-void
.end method

.method public updateNightImage()V
    .locals 8

    .prologue
    const v7, 0x40c8f5c3    # 6.28f

    .line 190
    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->calendar:Ljava/util/Calendar;

    const/16 v5, 0xb

    invoke-virtual {v4, v5}, Ljava/util/Calendar;->get(I)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->calendar:Ljava/util/Calendar;

    const/16 v6, 0xc

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x42700000    # 60.0f

    div-float/2addr v5, v6

    add-float/2addr v4, v5

    const/high16 v5, 0x41c00000    # 24.0f

    div-float/2addr v4, v5

    sub-float v0, v3, v4

    .line 191
    .local v0, "hourAtGMT":F
    mul-float v3, v7, v0

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v1, v4

    .line 192
    .local v1, "lightDirX":F
    mul-float v3, v7, v0

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v2, v4

    .line 193
    .local v2, "lightDirZ":F
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mlightDirec:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4, v2}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->set(FFF)V

    .line 194
    const-string v3, "lightDirec"

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->mlightDirec:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 195
    return-void
.end method
