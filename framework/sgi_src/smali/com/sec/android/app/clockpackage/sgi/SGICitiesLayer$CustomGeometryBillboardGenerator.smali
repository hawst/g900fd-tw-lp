.class Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;
.super Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;
.source "SGICitiesLayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomGeometryBillboardGenerator"
.end annotation


# static fields
.field private static final NORMAL_TEXT_SIZE:I = 0x19

.field private static final SELECTED_TEXT_SIZE:I = 0x28


# instance fields
.field ATLAS_HEIGHT:I

.field final ATLAS_WIDTH:I

.field billboardDatas:[Lcom/sec/android/app/clockpackage/sgi/BillboardData;

.field final synthetic this$0:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;)V
    .locals 30

    .prologue
    .line 348
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    .line 349
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;-><init>()V

    .line 340
    const/16 v26, 0x800

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->ATLAS_WIDTH:I

    .line 341
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->ATLAS_HEIGHT:I

    .line 351
    invoke-static {}, Lcom/sec/android/app/clockpackage/worldclock/CityManager;->getCitiesByOffset()[Lcom/sec/android/app/clockpackage/worldclock/City;

    move-result-object v7

    .line 352
    .local v7, "cities":[Lcom/sec/android/app/clockpackage/worldclock/City;
    array-length v5, v7

    .line 353
    .local v5, "COUNT":I
    new-array v0, v5, [Lcom/sec/android/app/clockpackage/sgi/BillboardData;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->billboardDatas:[Lcom/sec/android/app/clockpackage/sgi/BillboardData;

    .line 362
    new-instance v18, Landroid/graphics/Paint;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Paint;-><init>()V

    .line 363
    .local v18, "paint":Landroid/graphics/Paint;
    const/16 v26, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 364
    sget-object v26, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 365
    sget-object v26, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    move-object/from16 v0, v18

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 367
    const/high16 v26, 0x42200000    # 40.0f

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 368
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v8

    .line 369
    .local v8, "fontMetrics":Landroid/graphics/Paint$FontMetrics;
    iget v0, v8, Landroid/graphics/Paint$FontMetrics;->top:F

    move/from16 v26, v0

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    neg-int v0, v0

    move/from16 v20, v0

    .line 370
    .local v20, "selectedTop":I
    iget v0, v8, Landroid/graphics/Paint$FontMetrics;->top:F

    move/from16 v26, v0

    move/from16 v0, v26

    neg-float v0, v0

    move/from16 v26, v0

    iget v0, v8, Landroid/graphics/Paint$FontMetrics;->bottom:F

    move/from16 v27, v0

    add-float v26, v26, v27

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v19, v0

    .line 372
    .local v19, "selectedHeight":I
    const/high16 v26, 0x41c80000    # 25.0f

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 373
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v8

    .line 374
    iget v0, v8, Landroid/graphics/Paint$FontMetrics;->top:F

    move/from16 v26, v0

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    neg-int v0, v0

    move/from16 v17, v0

    .line 377
    .local v17, "normalTop":I
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->context:Landroid/app/Activity;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f02047e

    invoke-static/range {v26 .. v27}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 378
    .local v11, "markerNormalDown":Landroid/graphics/Bitmap;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->context:Landroid/app/Activity;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f02047f

    invoke-static/range {v26 .. v27}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 379
    .local v13, "markerSelectedDown":Landroid/graphics/Bitmap;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->context:Landroid/app/Activity;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f020480

    invoke-static/range {v26 .. v27}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 380
    .local v12, "markerNormalUp":Landroid/graphics/Bitmap;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->context:Landroid/app/Activity;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f020481

    invoke-static/range {v26 .. v27}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v14

    .line 382
    .local v14, "markerSelectedUp":Landroid/graphics/Bitmap;
    if-nez v11, :cond_0

    .line 383
    const-string v26, "jsyjbaba"

    const-string v27, "markerNormalDown = null"

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    :cond_0
    if-nez v13, :cond_1

    .line 385
    const-string v26, "jsyjbaba"

    const-string v27, "markerSelectedDown = null"

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    :cond_1
    if-nez v12, :cond_2

    .line 387
    const-string v26, "jsyjbaba"

    const-string v27, "markerNormalUp = null"

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    :cond_2
    if-nez v14, :cond_3

    .line 389
    const-string v26, "jsyjbaba"

    const-string v27, "markerSelectedUp = null"

    invoke-static/range {v26 .. v27}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    :cond_3
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v26

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_4

    .line 391
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v26

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_4

    .line 392
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v26

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_4

    .line 394
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v26

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_4

    .line 395
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v26

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_4

    .line 396
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v26

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_5

    .line 397
    :cond_4
    new-instance v26, Ljava/lang/RuntimeException;

    const-string v27, "Marker bitmaps MUST have same dimensions !"

    invoke-direct/range {v26 .. v27}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v26

    .line 400
    :cond_5
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    .line 401
    .local v15, "markerWidth":I
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    .line 402
    .local v10, "markerHeight":I
    const/16 v24, 0x0

    .line 403
    .local v24, "xPos":I
    move/from16 v25, v10

    .line 404
    .local v25, "yPos":I
    new-instance v21, Landroid/graphics/Rect;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/Rect;-><init>()V

    .line 405
    .local v21, "textBounds":Landroid/graphics/Rect;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-lt v9, v5, :cond_6

    .line 421
    add-int v26, v25, v19

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->ATLAS_HEIGHT:I

    .line 422
    sget-object v26, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "------------>Atlas size: 2048 x "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->ATLAS_HEIGHT:I

    move/from16 v28, v0

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 423
    const/16 v26, 0x800

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->ATLAS_HEIGHT:I

    move/from16 v27, v0

    sget-object v28, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static/range {v26 .. v28}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->access$0(Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;Landroid/graphics/Bitmap;)V

    .line 424
    new-instance v6, Landroid/graphics/Canvas;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->atlas:Landroid/graphics/Bitmap;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->access$1(Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;)Landroid/graphics/Bitmap;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-direct {v6, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 426
    .local v6, "canvas":Landroid/graphics/Canvas;
    const/16 v26, 0x0

    const/16 v27, 0x0

    const/16 v28, 0x0

    move/from16 v0, v26

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v6, v11, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 427
    int-to-float v0, v15

    move/from16 v26, v0

    const/16 v27, 0x0

    const/16 v28, 0x0

    move/from16 v0, v26

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v6, v13, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 428
    mul-int/lit8 v26, v15, 0x2

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    const/16 v27, 0x0

    const/16 v28, 0x0

    move/from16 v0, v26

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v6, v12, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 429
    mul-int/lit8 v26, v15, 0x3

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    const/16 v27, 0x0

    const/16 v28, 0x0

    move/from16 v0, v26

    move/from16 v1, v27

    move-object/from16 v2, v28

    invoke-virtual {v6, v14, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 431
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V

    .line 432
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    .line 433
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->recycle()V

    .line 434
    invoke-virtual {v14}, Landroid/graphics/Bitmap;->recycle()V

    .line 435
    const/4 v11, 0x0

    .line 436
    const/4 v13, 0x0

    .line 437
    const/4 v12, 0x0

    .line 438
    const/4 v14, 0x0

    .line 440
    const/16 v24, 0x0

    .line 441
    move/from16 v25, v10

    .line 443
    const/4 v9, 0x0

    :goto_1
    if-lt v9, v5, :cond_9

    .line 481
    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->atlas:Landroid/graphics/Bitmap;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->access$1(Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;)Landroid/graphics/Bitmap;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setImage(Landroid/graphics/Bitmap;)V

    .line 482
    return-void

    .line 406
    .end local v6    # "canvas":Landroid/graphics/Canvas;
    :cond_6
    aget-object v26, v7, v9

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/clockpackage/worldclock/City;->getName()Ljava/lang/String;

    move-result-object v16

    .line 407
    .local v16, "name":Ljava/lang/String;
    aget-object v26, v7, v9

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/clockpackage/worldclock/City;->getDBSelected()Z

    move-result v26

    if-eqz v26, :cond_7

    .line 408
    const/high16 v26, 0x42200000    # 40.0f

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 412
    :goto_2
    const/16 v26, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v27

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    move/from16 v2, v26

    move/from16 v3, v27

    move-object/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 413
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    add-int/lit8 v22, v26, 0xa

    .line 414
    .local v22, "textWidth":I
    add-int v26, v24, v22

    const/16 v27, 0x800

    move/from16 v0, v26

    move/from16 v1, v27

    if-ge v0, v1, :cond_8

    .line 415
    add-int v24, v24, v22

    .line 405
    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 410
    .end local v22    # "textWidth":I
    :cond_7
    const/high16 v26, 0x41c80000    # 25.0f

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    goto :goto_2

    .line 417
    .restart local v22    # "textWidth":I
    :cond_8
    add-int v25, v25, v19

    .line 418
    move/from16 v24, v22

    goto :goto_3

    .line 445
    .end local v16    # "name":Ljava/lang/String;
    .end local v22    # "textWidth":I
    .restart local v6    # "canvas":Landroid/graphics/Canvas;
    :cond_9
    aget-object v26, v7, v9

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/clockpackage/worldclock/City;->getName()Ljava/lang/String;

    move-result-object v16

    .line 448
    .restart local v16    # "name":Ljava/lang/String;
    aget-object v26, v7, v9

    invoke-virtual/range {v26 .. v26}, Lcom/sec/android/app/clockpackage/worldclock/City;->getDBSelected()Z

    move-result v26

    if-eqz v26, :cond_a

    .line 449
    const/high16 v26, 0x40400000    # 3.0f

    const/high16 v27, 0x3f800000    # 1.0f

    const/high16 v28, 0x3f800000    # 1.0f

    const/high16 v29, -0x1000000

    move-object/from16 v0, v18

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    move/from16 v4, v29

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 450
    const/high16 v26, 0x42200000    # 40.0f

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 451
    const/16 v26, -0x100

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 452
    move/from16 v23, v20

    .line 460
    .local v23, "top":I
    :goto_4
    const/16 v26, 0x0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v27

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    move/from16 v2, v26

    move/from16 v3, v27

    move-object/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 461
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v26, v0

    add-int/lit8 v22, v26, 0xa

    .line 462
    .restart local v22    # "textWidth":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->ATLAS_HEIGHT:I

    move/from16 v26, v0

    move/from16 v0, v25

    move/from16 v1, v26

    if-lt v0, v1, :cond_b

    .line 463
    new-instance v26, Ljava/lang/RuntimeException;

    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "Atlas size not enough to hold all cities! Conside increasing ATLAS_WIDTH/ATLAS_HEIGHT in "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v26

    .line 454
    .end local v22    # "textWidth":I
    .end local v23    # "top":I
    :cond_a
    const/high16 v26, 0x40400000    # 3.0f

    const/high16 v27, 0x3f800000    # 1.0f

    const/high16 v28, 0x3f800000    # 1.0f

    const/high16 v29, -0x1000000

    move-object/from16 v0, v18

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    move/from16 v4, v29

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 455
    const/high16 v26, 0x41c80000    # 25.0f

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 456
    const/16 v26, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 457
    move/from16 v23, v17

    .restart local v23    # "top":I
    goto :goto_4

    .line 466
    .restart local v22    # "textWidth":I
    :cond_b
    add-int v26, v24, v22

    const/16 v27, 0x800

    move/from16 v0, v26

    move/from16 v1, v27

    if-ge v0, v1, :cond_c

    .line 467
    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v26, v0

    add-int v27, v25, v23

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, v16

    move/from16 v1, v26

    move/from16 v2, v27

    move-object/from16 v3, v18

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 468
    add-int v24, v24, v22

    .line 443
    :goto_5
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    .line 470
    :cond_c
    const/16 v24, 0x0

    .line 471
    add-int v25, v25, v19

    .line 475
    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v26, v0

    add-int v27, v25, v23

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, v16

    move/from16 v1, v26

    move/from16 v2, v27

    move-object/from16 v3, v18

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 476
    add-int v24, v24, v22

    goto :goto_5
.end method


# virtual methods
.method public generate(FLcom/samsung/android/sdk/sgi/render/SGGeometry;FF)Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .locals 68
    .param p1, "aParam"    # F
    .param p2, "aPreviousGeometry"    # Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .param p3, "aHeight"    # F
    .param p4, "aWidth"    # F

    .prologue
    .line 495
    invoke-static {}, Lcom/sec/android/app/clockpackage/worldclock/CityManager;->getCitiesByOffset()[Lcom/sec/android/app/clockpackage/worldclock/City;

    move-result-object v23

    .line 496
    .local v23, "cities":[Lcom/sec/android/app/clockpackage/worldclock/City;
    move-object/from16 v0, v23

    array-length v13, v0

    .line 504
    .local v13, "COUNT":I
    new-instance v28, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;

    sget-object v4, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->TRIANGLES:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    sget-object v8, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    mul-int/lit8 v9, v13, 0xc

    move-object/from16 v0, v28

    invoke-direct {v0, v4, v8, v9}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;-><init>(Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 505
    .local v28, "indexBuffer":Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;
    invoke-virtual/range {v28 .. v28}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->getShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v29

    .line 506
    .local v29, "indices":Ljava/nio/ShortBuffer;
    invoke-virtual/range {v29 .. v29}, Ljava/nio/ShortBuffer;->clear()Ljava/nio/Buffer;

    .line 509
    new-instance v47, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/4 v4, 0x3

    sget-object v8, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v9, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    mul-int/lit8 v10, v13, 0x8

    move-object/from16 v0, v47

    invoke-direct {v0, v4, v8, v9, v10}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 510
    .local v47, "positionBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual/range {v47 .. v47}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v48

    .line 511
    .local v48, "positions":Ljava/nio/FloatBuffer;
    invoke-virtual/range {v48 .. v48}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 514
    new-instance v31, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/4 v4, 0x4

    sget-object v8, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v9, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    mul-int/lit8 v10, v13, 0x8

    move-object/from16 v0, v31

    invoke-direct {v0, v4, v8, v9, v10}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 515
    .local v31, "locationBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual/range {v31 .. v31}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v32

    .line 516
    .local v32, "locations":Ljava/nio/FloatBuffer;
    invoke-virtual/range {v32 .. v32}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 519
    new-instance v53, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/4 v4, 0x2

    sget-object v8, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v9, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    mul-int/lit8 v10, v13, 0x8

    move-object/from16 v0, v53

    invoke-direct {v0, v4, v8, v9, v10}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 520
    .local v53, "texcoordBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual/range {v53 .. v53}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v52

    .line 521
    .local v52, "texcoord":Ljava/nio/FloatBuffer;
    invoke-virtual/range {v52 .. v52}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 524
    new-instance v46, Landroid/graphics/Paint;

    invoke-direct/range {v46 .. v46}, Landroid/graphics/Paint;-><init>()V

    .line 525
    .local v46, "paint":Landroid/graphics/Paint;
    const/4 v4, 0x1

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 526
    sget-object v4, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 527
    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 529
    const/high16 v4, 0x42200000    # 40.0f

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 530
    invoke-virtual/range {v46 .. v46}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v24

    .line 531
    .local v24, "fontMetrics":Landroid/graphics/Paint$FontMetrics;
    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    neg-float v4, v4

    move-object/from16 v0, v24

    iget v8, v0, Landroid/graphics/Paint$FontMetrics;->bottom:F

    add-float/2addr v4, v8

    float-to-int v0, v4

    move/from16 v49, v0

    .line 533
    .local v49, "selectedHeight":I
    const/high16 v4, 0x41c80000    # 25.0f

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 534
    invoke-virtual/range {v46 .. v46}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v24

    .line 535
    move-object/from16 v0, v24

    iget v4, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    neg-float v4, v4

    move-object/from16 v0, v24

    iget v8, v0, Landroid/graphics/Paint$FontMetrics;->bottom:F

    add-float/2addr v4, v8

    float-to-int v0, v4

    move/from16 v45, v0

    .line 536
    .local v45, "normalHeight":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    invoke-virtual {v4}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v50

    .line 537
    .local v50, "size":Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    invoke-virtual/range {v50 .. v50}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v4

    invoke-virtual/range {v50 .. v50}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v8

    cmpg-float v4, v4, v8

    if-gez v4, :cond_2

    invoke-virtual/range {v50 .. v50}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v51

    .line 539
    .local v51, "smallerDimension":F
    :goto_0
    new-instance v43, Landroid/util/DisplayMetrics;

    invoke-direct/range {v43 .. v43}, Landroid/util/DisplayMetrics;-><init>()V

    .line 540
    .local v43, "metrics":Landroid/util/DisplayMetrics;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    iget-object v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->context:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    move-object/from16 v0, v43

    invoke-virtual {v4, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 541
    move-object/from16 v0, v43

    iget v4, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v4, v4

    const/high16 v8, 0x43f00000    # 480.0f

    div-float v16, v4, v8

    .line 542
    .local v16, "TEXT_SIZE_ADJ":F
    const/high16 v4, 0x3f000000    # 0.5f

    cmpg-float v4, v16, v4

    if-gez v4, :cond_0

    .line 543
    const/high16 v16, 0x3f000000    # 0.5f

    .line 545
    :cond_0
    const v4, 0x3e99999a    # 0.3f

    mul-float v17, v4, v16

    .line 546
    .local v17, "TEXT_SIZE_FACTOR":F
    const/high16 v4, 0x42480000    # 50.0f

    mul-float v4, v4, v17

    div-float v15, v4, v51

    .line 547
    .local v15, "MARKER_HALF_WIDTH":F
    move v14, v15

    .line 548
    .local v14, "MARKER_HALF_HEIGHT":F
    const v12, 0x44dc2333    # 1761.1f

    .line 549
    .local v12, "BILLBOARD_WINDOW_FACTOR":F
    const v11, 0x3a6bedfa    # 9.0E-4f

    .line 551
    .local v11, "ARROW_TEXT_OFFSET_FACTOR":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    iget-object v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->context:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v8, 0x7f02047e

    invoke-static {v4, v8}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v37

    .line 552
    .local v37, "markerNormalDown":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    iget-object v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->context:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v8, 0x7f02047f

    invoke-static {v4, v8}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v40

    .line 553
    .local v40, "markerSelectedDown":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    iget-object v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->context:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v8, 0x7f020480

    invoke-static {v4, v8}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v38

    .line 554
    .local v38, "markerNormalUp":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    iget-object v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->context:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v8, 0x7f020481

    invoke-static {v4, v8}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v41

    .line 556
    .local v41, "markerSelectedUp":Landroid/graphics/Bitmap;
    invoke-virtual/range {v37 .. v37}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual/range {v40 .. v40}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    if-ne v4, v8, :cond_1

    .line 557
    invoke-virtual/range {v37 .. v37}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual/range {v38 .. v38}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    if-ne v4, v8, :cond_1

    .line 558
    invoke-virtual/range {v37 .. v37}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    if-ne v4, v8, :cond_1

    .line 560
    invoke-virtual/range {v37 .. v37}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual/range {v40 .. v40}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    if-ne v4, v8, :cond_1

    .line 561
    invoke-virtual/range {v37 .. v37}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual/range {v38 .. v38}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    if-ne v4, v8, :cond_1

    .line 562
    invoke-virtual/range {v37 .. v37}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    if-eq v4, v8, :cond_3

    .line 563
    :cond_1
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v8, "Marker bitmaps MUST have same dimensions !"

    invoke-direct {v4, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 537
    .end local v11    # "ARROW_TEXT_OFFSET_FACTOR":F
    .end local v12    # "BILLBOARD_WINDOW_FACTOR":F
    .end local v14    # "MARKER_HALF_HEIGHT":F
    .end local v15    # "MARKER_HALF_WIDTH":F
    .end local v16    # "TEXT_SIZE_ADJ":F
    .end local v17    # "TEXT_SIZE_FACTOR":F
    .end local v37    # "markerNormalDown":Landroid/graphics/Bitmap;
    .end local v38    # "markerNormalUp":Landroid/graphics/Bitmap;
    .end local v40    # "markerSelectedDown":Landroid/graphics/Bitmap;
    .end local v41    # "markerSelectedUp":Landroid/graphics/Bitmap;
    .end local v43    # "metrics":Landroid/util/DisplayMetrics;
    .end local v51    # "smallerDimension":F
    :cond_2
    invoke-virtual/range {v50 .. v50}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v51

    goto/16 :goto_0

    .line 566
    .restart local v11    # "ARROW_TEXT_OFFSET_FACTOR":F
    .restart local v12    # "BILLBOARD_WINDOW_FACTOR":F
    .restart local v14    # "MARKER_HALF_HEIGHT":F
    .restart local v15    # "MARKER_HALF_WIDTH":F
    .restart local v16    # "TEXT_SIZE_ADJ":F
    .restart local v17    # "TEXT_SIZE_FACTOR":F
    .restart local v37    # "markerNormalDown":Landroid/graphics/Bitmap;
    .restart local v38    # "markerNormalUp":Landroid/graphics/Bitmap;
    .restart local v40    # "markerSelectedDown":Landroid/graphics/Bitmap;
    .restart local v41    # "markerSelectedUp":Landroid/graphics/Bitmap;
    .restart local v43    # "metrics":Landroid/util/DisplayMetrics;
    .restart local v51    # "smallerDimension":F
    :cond_3
    invoke-virtual/range {v37 .. v37}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v42

    .line 567
    .local v42, "markerWidth":I
    invoke-virtual/range {v37 .. v37}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v35

    .line 569
    .local v35, "markerHeight":I
    invoke-virtual/range {v37 .. v37}, Landroid/graphics/Bitmap;->recycle()V

    .line 570
    invoke-virtual/range {v40 .. v40}, Landroid/graphics/Bitmap;->recycle()V

    .line 571
    invoke-virtual/range {v38 .. v38}, Landroid/graphics/Bitmap;->recycle()V

    .line 572
    invoke-virtual/range {v41 .. v41}, Landroid/graphics/Bitmap;->recycle()V

    .line 573
    const/16 v37, 0x0

    .line 574
    const/16 v40, 0x0

    .line 575
    const/16 v38, 0x0

    .line 576
    const/16 v41, 0x0

    .line 578
    const/16 v61, 0x0

    .line 579
    .local v61, "xPos":I
    move/from16 v65, v35

    .line 582
    .local v65, "yPos":I
    new-instance v54, Landroid/graphics/Rect;

    invoke-direct/range {v54 .. v54}, Landroid/graphics/Rect;-><init>()V

    .line 583
    .local v54, "textBounds":Landroid/graphics/Rect;
    const/16 v27, 0x0

    .local v27, "i":I
    :goto_1
    move/from16 v0, v27

    if-lt v0, v13, :cond_4

    .line 772
    new-instance v25, Lcom/samsung/android/sdk/sgi/render/SGGeometry;

    move-object/from16 v0, v25

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;-><init>(Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;)V

    .line 773
    .local v25, "geometry":Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    const-string v4, "SGTextureCoords"

    move-object/from16 v0, v25

    move-object/from16 v1, v53

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 774
    const-string v4, "SGPositions"

    move-object/from16 v0, v25

    move-object/from16 v1, v47

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 775
    const-string v4, "position"

    move-object/from16 v0, v25

    move-object/from16 v1, v31

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 777
    return-object v25

    .line 585
    .end local v25    # "geometry":Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    :cond_4
    aget-object v4, v23, v27

    invoke-virtual {v4}, Lcom/sec/android/app/clockpackage/worldclock/City;->getName()Ljava/lang/String;

    move-result-object v44

    .line 586
    .local v44, "name":Ljava/lang/String;
    aget-object v4, v23, v27

    invoke-virtual {v4}, Lcom/sec/android/app/clockpackage/worldclock/City;->getXTextOffset()I

    move-result v4

    int-to-float v4, v4

    const v8, 0x3a6bedfa    # 9.0E-4f

    mul-float v18, v4, v8

    .line 587
    .local v18, "arrowTextXOffset":F
    aget-object v4, v23, v27

    invoke-virtual {v4}, Lcom/sec/android/app/clockpackage/worldclock/City;->getYTextOffset()I

    move-result v4

    int-to-float v4, v4

    const v8, 0x3a6bedfa    # 9.0E-4f

    mul-float v19, v4, v8

    .line 590
    .local v19, "arrowTextYOffset":F
    aget-object v4, v23, v27

    invoke-virtual {v4}, Lcom/sec/android/app/clockpackage/worldclock/City;->getDBSelected()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 591
    const/high16 v4, 0x40400000    # 3.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, -0x1000000

    move-object/from16 v0, v46

    invoke-virtual {v0, v4, v8, v9, v10}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 592
    const/high16 v4, 0x42200000    # 40.0f

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 593
    const/16 v4, -0x100

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 594
    move/from16 v26, v49

    .line 602
    .local v26, "height":I
    :goto_2
    const/4 v4, 0x0

    invoke-virtual/range {v44 .. v44}, Ljava/lang/String;->length()I

    move-result v8

    move-object/from16 v0, v46

    move-object/from16 v1, v44

    move-object/from16 v2, v54

    invoke-virtual {v0, v1, v4, v8, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 603
    move-object/from16 v0, v54

    iget v4, v0, Landroid/graphics/Rect;->right:I

    add-int/lit8 v55, v4, 0xa

    .line 605
    .local v55, "textWidth":I
    move/from16 v0, v61

    int-to-float v4, v0

    const/high16 v8, 0x45000000    # 2048.0f

    div-float v56, v4, v8

    .line 606
    .local v56, "uLeft":F
    add-int v4, v61, v55

    int-to-float v4, v4

    const/high16 v8, 0x45000000    # 2048.0f

    div-float v57, v4, v8

    .line 607
    .local v57, "uRight":F
    move/from16 v0, v65

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->ATLAS_HEIGHT:I

    int-to-float v8, v8

    div-float v59, v4, v8

    .line 608
    .local v59, "vTop":F
    add-int v4, v65, v26

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->ATLAS_HEIGHT:I

    int-to-float v8, v8

    div-float v58, v4, v8

    .line 610
    .local v58, "vBottom":F
    add-int v4, v61, v55

    const/16 v8, 0x800

    if-ge v4, v8, :cond_6

    .line 611
    add-int v61, v61, v55

    .line 630
    :goto_3
    mul-int/lit8 v4, v27, 0x8

    add-int/lit8 v4, v4, 0x0

    int-to-short v4, v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 631
    mul-int/lit8 v4, v27, 0x8

    add-int/lit8 v4, v4, 0x1

    int-to-short v4, v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 632
    mul-int/lit8 v4, v27, 0x8

    add-int/lit8 v4, v4, 0x2

    int-to-short v4, v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 634
    mul-int/lit8 v4, v27, 0x8

    add-int/lit8 v4, v4, 0x0

    int-to-short v4, v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 635
    mul-int/lit8 v4, v27, 0x8

    add-int/lit8 v4, v4, 0x2

    int-to-short v4, v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 636
    mul-int/lit8 v4, v27, 0x8

    add-int/lit8 v4, v4, 0x3

    int-to-short v4, v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 639
    mul-int/lit8 v4, v27, 0x8

    add-int/lit8 v4, v4, 0x4

    int-to-short v4, v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 640
    mul-int/lit8 v4, v27, 0x8

    add-int/lit8 v4, v4, 0x5

    int-to-short v4, v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 641
    mul-int/lit8 v4, v27, 0x8

    add-int/lit8 v4, v4, 0x6

    int-to-short v4, v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 643
    mul-int/lit8 v4, v27, 0x8

    add-int/lit8 v4, v4, 0x4

    int-to-short v4, v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 644
    mul-int/lit8 v4, v27, 0x8

    add-int/lit8 v4, v4, 0x6

    int-to-short v4, v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 645
    mul-int/lit8 v4, v27, 0x8

    add-int/lit8 v4, v4, 0x7

    int-to-short v4, v4

    move-object/from16 v0, v29

    invoke-virtual {v0, v4}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 647
    aget-object v4, v23, v27

    iget v4, v4, Lcom/sec/android/app/clockpackage/worldclock/City;->mLatitudeBillboard:F

    neg-float v4, v4

    const v8, 0x4048f5c3    # 3.14f

    mul-float/2addr v4, v8

    const/high16 v8, 0x43340000    # 180.0f

    div-float v30, v4, v8

    .line 648
    .local v30, "latitude":F
    aget-object v4, v23, v27

    iget v4, v4, Lcom/sec/android/app/clockpackage/worldclock/City;->mLongitudeBillboard:F

    const v8, 0x4048f5c3    # 3.14f

    mul-float/2addr v4, v8

    const/high16 v8, 0x43340000    # 180.0f

    div-float v33, v4, v8

    .line 649
    .local v33, "longitude":F
    move/from16 v0, v30

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v4, v8

    move/from16 v0, v33

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v8, v8

    mul-float v5, v4, v8

    .line 650
    .local v5, "locationX":F
    move/from16 v0, v33

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v6, v8

    .line 651
    .local v6, "locationY":F
    move/from16 v0, v30

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v4, v8

    move/from16 v0, v33

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v8, v8

    mul-float v7, v4, v8

    .line 653
    .local v7, "locationZ":F
    move/from16 v0, v55

    int-to-float v4, v0

    mul-float v4, v4, v17

    div-float v60, v4, v51

    .line 654
    .local v60, "xHalfSize":F
    move/from16 v0, v26

    int-to-float v4, v0

    mul-float v4, v4, v17

    div-float v62, v4, v51

    .line 656
    .local v62, "yHalfSize":F
    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr v4, v14

    add-float v8, v4, v62

    aget-object v4, v23, v27

    iget v4, v4, Lcom/sec/android/app/clockpackage/worldclock/City;->mArrowDirection:I

    if-nez v4, :cond_8

    const/4 v4, 0x1

    :goto_4
    int-to-float v4, v4

    mul-float v64, v8, v4

    .line 657
    .local v64, "yOffsetText":F
    aget-object v4, v23, v27

    iget v4, v4, Lcom/sec/android/app/clockpackage/worldclock/City;->mArrowDirection:I

    if-nez v4, :cond_9

    const/4 v4, 0x1

    :goto_5
    int-to-float v4, v4

    mul-float v63, v14, v4

    .line 658
    .local v63, "yOffsetMarker":F
    aget-object v4, v23, v27

    invoke-virtual {v4}, Lcom/sec/android/app/clockpackage/worldclock/City;->getDBSelected()Z

    move-result v4

    if-eqz v4, :cond_a

    const/4 v4, -0x1

    :goto_6
    int-to-float v0, v4

    move/from16 v66, v0

    .line 660
    .local v66, "zoomLevel":F
    const v4, 0x44dc2333    # 1761.1f

    mul-float v4, v4, v60

    float-to-int v0, v4

    move/from16 v21, v0

    .line 661
    .local v21, "billboardHalfWidth":I
    add-float v4, v62, v14

    const v8, 0x44dc2333    # 1761.1f

    mul-float/2addr v4, v8

    float-to-int v0, v4

    move/from16 v20, v0

    .line 662
    .local v20, "billboardHalfHeight":I
    add-float v4, v62, v14

    const v8, 0x44dc2333    # 1761.1f

    mul-float/2addr v4, v8

    float-to-int v8, v4

    aget-object v4, v23, v27

    iget v4, v4, Lcom/sec/android/app/clockpackage/worldclock/City;->mArrowDirection:I

    if-nez v4, :cond_b

    const/4 v4, 0x1

    :goto_7
    mul-int v22, v8, v4

    .line 663
    .local v22, "billboardYOffset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->billboardDatas:[Lcom/sec/android/app/clockpackage/sgi/BillboardData;

    move-object/from16 v67, v0

    new-instance v4, Lcom/sec/android/app/clockpackage/sgi/BillboardData;

    move/from16 v0, v22

    int-to-float v8, v0

    move/from16 v0, v21

    int-to-float v9, v0

    move/from16 v0, v20

    int-to-float v10, v0

    invoke-direct/range {v4 .. v10}, Lcom/sec/android/app/clockpackage/sgi/BillboardData;-><init>(FFFFFF)V

    aput-object v4, v67, v27

    .line 666
    move/from16 v0, v60

    neg-float v4, v0

    add-float v4, v4, v18

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 667
    add-float v4, v62, v64

    add-float v4, v4, v19

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 668
    const/4 v4, 0x0

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 669
    move-object/from16 v0, v52

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 670
    move-object/from16 v0, v52

    move/from16 v1, v59

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 671
    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 672
    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 673
    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 674
    move-object/from16 v0, v32

    move/from16 v1, v66

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 676
    move/from16 v0, v60

    neg-float v4, v0

    add-float v4, v4, v18

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 677
    move/from16 v0, v62

    neg-float v4, v0

    add-float v4, v4, v64

    add-float v4, v4, v19

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 678
    const/4 v4, 0x0

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 679
    move-object/from16 v0, v52

    move/from16 v1, v56

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 680
    move-object/from16 v0, v52

    move/from16 v1, v58

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 681
    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 682
    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 683
    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 684
    move-object/from16 v0, v32

    move/from16 v1, v66

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 686
    add-float v4, v60, v18

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 687
    move/from16 v0, v62

    neg-float v4, v0

    add-float v4, v4, v64

    add-float v4, v4, v19

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 688
    const/4 v4, 0x0

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 689
    move-object/from16 v0, v52

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 690
    move-object/from16 v0, v52

    move/from16 v1, v58

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 691
    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 692
    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 693
    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 694
    move-object/from16 v0, v32

    move/from16 v1, v66

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 696
    add-float v4, v60, v18

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 697
    add-float v4, v62, v64

    add-float v4, v4, v19

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 698
    const/4 v4, 0x0

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 699
    move-object/from16 v0, v52

    move/from16 v1, v57

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 700
    move-object/from16 v0, v52

    move/from16 v1, v59

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 701
    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 702
    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 703
    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 704
    move-object/from16 v0, v32

    move/from16 v1, v66

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 707
    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v34, v0

    .line 708
    .local v34, "markerBottom":F
    aget-object v4, v23, v27

    invoke-virtual {v4}, Lcom/sec/android/app/clockpackage/worldclock/City;->getDBSelected()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 709
    aget-object v4, v23, v27

    iget v4, v4, Lcom/sec/android/app/clockpackage/worldclock/City;->mArrowDirection:I

    if-nez v4, :cond_c

    .line 710
    move/from16 v0, v42

    int-to-float v0, v0

    move/from16 v36, v0

    .line 711
    .local v36, "markerLeft":F
    mul-int/lit8 v4, v42, 0x2

    int-to-float v0, v4

    move/from16 v39, v0

    .line 725
    .local v39, "markerRight":F
    :goto_8
    const/high16 v4, 0x45000000    # 2048.0f

    div-float v36, v36, v4

    .line 726
    const/high16 v4, 0x45000000    # 2048.0f

    div-float v39, v39, v4

    .line 727
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->ATLAS_HEIGHT:I

    int-to-float v4, v4

    div-float v34, v34, v4

    .line 730
    neg-float v4, v15

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 731
    add-float v4, v14, v63

    add-float v4, v4, v19

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 732
    const/4 v4, 0x0

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 733
    move-object/from16 v0, v52

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 734
    const/4 v4, 0x0

    move-object/from16 v0, v52

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 735
    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 736
    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 737
    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 738
    move-object/from16 v0, v32

    move/from16 v1, v66

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 740
    neg-float v4, v15

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 741
    neg-float v4, v14

    add-float v4, v4, v63

    add-float v4, v4, v19

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 742
    const/4 v4, 0x0

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 743
    move-object/from16 v0, v52

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 744
    move-object/from16 v0, v52

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 745
    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 746
    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 747
    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 748
    move-object/from16 v0, v32

    move/from16 v1, v66

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 750
    move-object/from16 v0, v48

    invoke-virtual {v0, v15}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 751
    neg-float v4, v14

    add-float v4, v4, v63

    add-float v4, v4, v19

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 752
    const/4 v4, 0x0

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 753
    move-object/from16 v0, v52

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 754
    move-object/from16 v0, v52

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 755
    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 756
    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 757
    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 758
    move-object/from16 v0, v32

    move/from16 v1, v66

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 760
    move-object/from16 v0, v48

    invoke-virtual {v0, v15}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 761
    add-float v4, v14, v63

    add-float v4, v4, v19

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 762
    const/4 v4, 0x0

    move-object/from16 v0, v48

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 763
    move-object/from16 v0, v52

    move/from16 v1, v39

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 764
    const/4 v4, 0x0

    move-object/from16 v0, v52

    invoke-virtual {v0, v4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 765
    move-object/from16 v0, v32

    invoke-virtual {v0, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 766
    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 767
    move-object/from16 v0, v32

    invoke-virtual {v0, v7}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 768
    move-object/from16 v0, v32

    move/from16 v1, v66

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 583
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_1

    .line 596
    .end local v5    # "locationX":F
    .end local v6    # "locationY":F
    .end local v7    # "locationZ":F
    .end local v20    # "billboardHalfHeight":I
    .end local v21    # "billboardHalfWidth":I
    .end local v22    # "billboardYOffset":I
    .end local v26    # "height":I
    .end local v30    # "latitude":F
    .end local v33    # "longitude":F
    .end local v34    # "markerBottom":F
    .end local v36    # "markerLeft":F
    .end local v39    # "markerRight":F
    .end local v55    # "textWidth":I
    .end local v56    # "uLeft":F
    .end local v57    # "uRight":F
    .end local v58    # "vBottom":F
    .end local v59    # "vTop":F
    .end local v60    # "xHalfSize":F
    .end local v62    # "yHalfSize":F
    .end local v63    # "yOffsetMarker":F
    .end local v64    # "yOffsetText":F
    .end local v66    # "zoomLevel":F
    :cond_5
    const/high16 v4, 0x40400000    # 3.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, -0x1000000

    move-object/from16 v0, v46

    invoke-virtual {v0, v4, v8, v9, v10}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 597
    const/high16 v4, 0x41c80000    # 25.0f

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 598
    const/4 v4, -0x1

    move-object/from16 v0, v46

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 599
    move/from16 v26, v45

    .restart local v26    # "height":I
    goto/16 :goto_2

    .line 613
    .restart local v55    # "textWidth":I
    .restart local v56    # "uLeft":F
    .restart local v57    # "uRight":F
    .restart local v58    # "vBottom":F
    .restart local v59    # "vTop":F
    :cond_6
    const/16 v61, 0x0

    .line 614
    add-int v65, v65, v49

    .line 616
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->ATLAS_HEIGHT:I

    move/from16 v0, v65

    if-lt v0, v4, :cond_7

    .line 617
    new-instance v4, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Atlas size not enough to hold all cities! Conside increasing ATLAS_WIDTH/ATLAS_HEIGHT in "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 621
    :cond_7
    move/from16 v0, v61

    int-to-float v4, v0

    const/high16 v8, 0x45000000    # 2048.0f

    div-float v56, v4, v8

    .line 622
    add-int v4, v61, v55

    int-to-float v4, v4

    const/high16 v8, 0x45000000    # 2048.0f

    div-float v57, v4, v8

    .line 623
    move/from16 v0, v65

    int-to-float v4, v0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->ATLAS_HEIGHT:I

    int-to-float v8, v8

    div-float v59, v4, v8

    .line 624
    add-int v4, v65, v26

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->ATLAS_HEIGHT:I

    int-to-float v8, v8

    div-float v58, v4, v8

    .line 626
    add-int v61, v61, v55

    goto/16 :goto_3

    .line 656
    .restart local v5    # "locationX":F
    .restart local v6    # "locationY":F
    .restart local v7    # "locationZ":F
    .restart local v30    # "latitude":F
    .restart local v33    # "longitude":F
    .restart local v60    # "xHalfSize":F
    .restart local v62    # "yHalfSize":F
    :cond_8
    const/4 v4, -0x1

    goto/16 :goto_4

    .line 657
    .restart local v64    # "yOffsetText":F
    :cond_9
    const/4 v4, -0x1

    goto/16 :goto_5

    .line 658
    .restart local v63    # "yOffsetMarker":F
    :cond_a
    aget-object v4, v23, v27

    iget v4, v4, Lcom/sec/android/app/clockpackage/worldclock/City;->mZoomLevel:I

    goto/16 :goto_6

    .line 662
    .restart local v20    # "billboardHalfHeight":I
    .restart local v21    # "billboardHalfWidth":I
    .restart local v66    # "zoomLevel":F
    :cond_b
    const/4 v4, -0x1

    goto/16 :goto_7

    .line 713
    .restart local v22    # "billboardYOffset":I
    .restart local v34    # "markerBottom":F
    :cond_c
    mul-int/lit8 v4, v42, 0x3

    int-to-float v0, v4

    move/from16 v36, v0

    .line 714
    .restart local v36    # "markerLeft":F
    mul-int/lit8 v4, v42, 0x4

    int-to-float v0, v4

    move/from16 v39, v0

    .line 716
    .restart local v39    # "markerRight":F
    goto/16 :goto_8

    .line 717
    .end local v36    # "markerLeft":F
    .end local v39    # "markerRight":F
    :cond_d
    aget-object v4, v23, v27

    iget v4, v4, Lcom/sec/android/app/clockpackage/worldclock/City;->mArrowDirection:I

    if-nez v4, :cond_e

    .line 718
    const/16 v36, 0x0

    .line 719
    .restart local v36    # "markerLeft":F
    move/from16 v0, v42

    int-to-float v0, v0

    move/from16 v39, v0

    .line 720
    .restart local v39    # "markerRight":F
    goto/16 :goto_8

    .line 721
    .end local v36    # "markerLeft":F
    .end local v39    # "markerRight":F
    :cond_e
    mul-int/lit8 v4, v42, 0x2

    int-to-float v0, v4

    move/from16 v36, v0

    .line 722
    .restart local v36    # "markerLeft":F
    mul-int/lit8 v4, v42, 0x3

    int-to-float v0, v4

    move/from16 v39, v0

    .restart local v39    # "markerRight":F
    goto/16 :goto_8
.end method

.method getBillboardDatas()[Lcom/sec/android/app/clockpackage/sgi/BillboardData;
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->billboardDatas:[Lcom/sec/android/app/clockpackage/sgi/BillboardData;

    return-object v0
.end method

.method protected isBelongsToGeometry(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Z
    .locals 1
    .param p1, "aCheckDot"    # Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .prologue
    .line 490
    const/4 v0, 0x0

    return v0
.end method
