.class Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;
.super Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;
.source "SGILatLongLayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomGeometryLatLongGenerator"
.end annotation


# instance fields
.field private mRadius:F

.field private mSegments:I

.field private mSteps:I

.field final synthetic this$0:Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;IIF)V
    .locals 0
    .param p2, "segments"    # I
    .param p3, "steps"    # I
    .param p4, "radius"    # F

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;

    .line 152
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;-><init>()V

    .line 154
    iput p2, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSegments:I

    .line 155
    iput p3, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSteps:I

    .line 156
    iput p4, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mRadius:F

    .line 157
    return-void
.end method


# virtual methods
.method public generate(FLcom/samsung/android/sdk/sgi/render/SGGeometry;FF)Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .locals 34
    .param p1, "aParam"    # F
    .param p2, "aPreviousGeometry"    # Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .param p3, "aHeight"    # F
    .param p4, "aWidth"    # F

    .prologue
    .line 168
    new-instance v11, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;

    .line 169
    sget-object v28, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->LINES:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    sget-object v29, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    .line 170
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSegments:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSteps:I

    move/from16 v31, v0

    mul-int v30, v30, v31

    mul-int/lit8 v30, v30, 0x2

    .line 168
    move-object/from16 v0, v28

    move-object/from16 v1, v29

    move/from16 v2, v30

    invoke-direct {v11, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;-><init>(Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 171
    .local v11, "indexBuffer":Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;
    invoke-virtual {v11}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->getShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v12

    .line 174
    .local v12, "indices":Ljava/nio/ShortBuffer;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSegments:I

    move/from16 v18, v0

    .line 175
    .local v18, "segments":I
    const/4 v13, 0x0

    .local v13, "k":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSteps:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-lt v13, v0, :cond_0

    .line 186
    new-instance v15, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/16 v28, 0x3

    .line 187
    sget-object v29, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v30, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    .line 188
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSegments:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSteps:I

    move/from16 v32, v0

    mul-int v31, v31, v32

    .line 186
    move/from16 v0, v28

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move/from16 v3, v31

    invoke-direct {v15, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 191
    .local v15, "positionBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/16 v28, 0x4

    .line 192
    sget-object v29, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v30, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    .line 193
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSegments:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSteps:I

    move/from16 v32, v0

    mul-int v31, v31, v32

    .line 191
    move/from16 v0, v28

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    move/from16 v3, v31

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 195
    .local v6, "colorBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual {v15}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v16

    .line 196
    .local v16, "positions":Ljava/nio/FloatBuffer;
    invoke-virtual/range {v16 .. v16}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 198
    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v5

    .line 199
    .local v5, "color":Ljava/nio/FloatBuffer;
    invoke-virtual {v5}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 201
    const/16 v28, 0x4

    move/from16 v0, v28

    new-array v7, v0, [F

    fill-array-data v7, :array_0

    .line 202
    .local v7, "colorGray":[F
    const/16 v28, 0x4

    move/from16 v0, v28

    new-array v8, v0, [F

    fill-array-data v8, :array_1

    .line 204
    .local v8, "colorRed":[F
    const/high16 v19, -0x3f600000    # -5.0f

    .local v19, "step":F
    :goto_1
    const/high16 v28, 0x40a00000    # 5.0f

    cmpg-float v28, v19, v28

    if-lez v28, :cond_3

    .line 232
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSegments:I

    move/from16 v28, v0

    move/from16 v0, v28

    new-array v0, v0, [F

    move-object/from16 v23, v0

    .line 233
    .local v23, "xGMT":[F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSegments:I

    move/from16 v28, v0

    move/from16 v0, v28

    new-array v0, v0, [F

    move-object/from16 v25, v0

    .line 234
    .local v25, "yGMT":[F
    const/16 v27, 0x0

    .line 237
    .local v27, "zGMT":F
    const/4 v4, 0x0

    .local v4, "angIndeg":I
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSegments:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-lt v4, v0, :cond_6

    .line 255
    const/high16 v17, 0x41700000    # 15.0f

    .local v17, "sectorAngle":F
    :goto_3
    const/high16 v28, 0x43340000    # 180.0f

    cmpg-float v28, v17, v28

    if-ltz v28, :cond_7

    .line 275
    new-instance v9, Lcom/samsung/android/sdk/sgi/render/SGGeometry;

    invoke-direct {v9, v11}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;-><init>(Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;)V

    .line 276
    .local v9, "geometry":Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    const-string v28, "SGPositions"

    move-object/from16 v0, v28

    invoke-virtual {v9, v0, v15}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 277
    const-string v28, "color"

    move-object/from16 v0, v28

    invoke-virtual {v9, v0, v6}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 279
    return-object v9

    .line 176
    .end local v4    # "angIndeg":I
    .end local v5    # "color":Ljava/nio/FloatBuffer;
    .end local v6    # "colorBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .end local v7    # "colorGray":[F
    .end local v8    # "colorRed":[F
    .end local v9    # "geometry":Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .end local v15    # "positionBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .end local v16    # "positions":Ljava/nio/FloatBuffer;
    .end local v17    # "sectorAngle":F
    .end local v19    # "step":F
    .end local v23    # "xGMT":[F
    .end local v25    # "yGMT":[F
    .end local v27    # "zGMT":F
    :cond_0
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_4
    move/from16 v0, v18

    if-lt v10, v0, :cond_1

    .line 175
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_0

    .line 177
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSegments:I

    move/from16 v28, v0

    mul-int v28, v28, v13

    add-int v28, v28, v10

    move/from16 v0, v28

    int-to-short v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    invoke-virtual {v12, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 178
    add-int/lit8 v28, v10, 0x1

    move/from16 v0, v28

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 179
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSegments:I

    move/from16 v28, v0

    mul-int v28, v28, v13

    add-int v28, v28, v10

    sub-int v28, v28, v10

    move/from16 v0, v28

    int-to-short v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    invoke-virtual {v12, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 176
    :goto_5
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    .line 181
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSegments:I

    move/from16 v28, v0

    mul-int v28, v28, v13

    add-int v28, v28, v10

    add-int/lit8 v28, v28, 0x1

    move/from16 v0, v28

    int-to-short v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    invoke-virtual {v12, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    goto :goto_5

    .line 205
    .end local v10    # "i":I
    .restart local v5    # "color":Ljava/nio/FloatBuffer;
    .restart local v6    # "colorBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .restart local v7    # "colorGray":[F
    .restart local v8    # "colorRed":[F
    .restart local v15    # "positionBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .restart local v16    # "positions":Ljava/nio/FloatBuffer;
    .restart local v19    # "step":F
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mRadius:F

    move/from16 v28, v0

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    const v30, 0x4182e148    # 16.36f

    mul-float v30, v30, v19

    move/from16 v0, v30

    float-to-double v0, v0

    move-wide/from16 v30, v0

    const-wide v32, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double v30, v30, v32

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->cos(D)D

    move-result-wide v30

    mul-double v28, v28, v30

    move-wide/from16 v0, v28

    double-to-float v14, v0

    .line 206
    .local v14, "nR":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mRadius:F

    move/from16 v28, v0

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    const v30, 0x4182e148    # 16.36f

    mul-float v30, v30, v19

    move/from16 v0, v30

    float-to-double v0, v0

    move-wide/from16 v30, v0

    const-wide v32, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double v30, v30, v32

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->sin(D)D

    move-result-wide v30

    mul-double v28, v28, v30

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v24, v0

    .line 207
    .local v24, "y":F
    const/4 v4, 0x0

    .local v4, "angIndeg":F
    :goto_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSegments:I

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    cmpg-float v28, v4, v28

    if-ltz v28, :cond_4

    .line 204
    const/high16 v28, 0x3f800000    # 1.0f

    add-float v19, v19, v28

    goto/16 :goto_1

    .line 208
    :cond_4
    const/high16 v28, 0x40900000    # 4.5f

    mul-float v28, v28, v4

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    const-wide v30, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double v20, v28, v30

    .line 210
    .local v20, "theta":D
    float-to-double v0, v14

    move-wide/from16 v28, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->cos(D)D

    move-result-wide v30

    mul-double v28, v28, v30

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v22, v0

    .line 211
    .local v22, "x":F
    float-to-double v0, v14

    move-wide/from16 v28, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->sin(D)D

    move-result-wide v30

    mul-double v28, v28, v30

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v26, v0

    .line 213
    .local v26, "z":F
    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 214
    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 215
    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 217
    const/16 v28, 0x0

    cmpl-float v28, v24, v28

    if-nez v28, :cond_5

    .line 218
    const/16 v28, 0x0

    aget v28, v8, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 219
    const/16 v28, 0x1

    aget v28, v8, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 220
    const/16 v28, 0x2

    aget v28, v8, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 221
    const/16 v28, 0x3

    aget v28, v8, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 207
    :goto_7
    const/high16 v28, 0x3f800000    # 1.0f

    add-float v4, v4, v28

    goto/16 :goto_6

    .line 224
    :cond_5
    const/16 v28, 0x0

    aget v28, v7, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 225
    const/16 v28, 0x1

    aget v28, v7, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 226
    const/16 v28, 0x2

    aget v28, v7, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 227
    const/16 v28, 0x3

    aget v28, v7, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    goto :goto_7

    .line 238
    .end local v14    # "nR":F
    .end local v20    # "theta":D
    .end local v22    # "x":F
    .end local v24    # "y":F
    .end local v26    # "z":F
    .local v4, "angIndeg":I
    .restart local v23    # "xGMT":[F
    .restart local v25    # "yGMT":[F
    .restart local v27    # "zGMT":F
    :cond_6
    int-to-float v0, v4

    move/from16 v28, v0

    const/high16 v29, 0x40900000    # 4.5f

    mul-float v28, v28, v29

    const v29, 0x3c8efa35

    mul-float v20, v28, v29

    .line 240
    .local v20, "theta":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mRadius:F

    move/from16 v28, v0

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v30, v0

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->cos(D)D

    move-result-wide v30

    mul-double v28, v28, v30

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v28, v0

    aput v28, v23, v4

    .line 241
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mRadius:F

    move/from16 v28, v0

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v30, v0

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->sin(D)D

    move-result-wide v30

    mul-double v28, v28, v30

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v28, v0

    aput v28, v25, v4

    .line 242
    const/16 v27, 0x0

    .line 244
    aget v28, v23, v4

    move-object/from16 v0, v16

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 245
    aget v28, v25, v4

    move-object/from16 v0, v16

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 246
    move-object/from16 v0, v16

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 248
    const/16 v28, 0x0

    aget v28, v8, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 249
    const/16 v28, 0x1

    aget v28, v8, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 250
    const/16 v28, 0x2

    aget v28, v8, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 251
    const/16 v28, 0x3

    aget v28, v8, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 237
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 256
    .end local v20    # "theta":F
    .restart local v17    # "sectorAngle":F
    :cond_7
    const v28, 0x3c8efa35

    mul-float v20, v17, v28

    .line 257
    .restart local v20    # "theta":F
    const/4 v4, 0x0

    :goto_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;->mSegments:I

    move/from16 v28, v0

    move/from16 v0, v28

    if-lt v4, v0, :cond_8

    .line 255
    const/high16 v28, 0x41700000    # 15.0f

    add-float v17, v17, v28

    goto/16 :goto_3

    .line 259
    :cond_8
    aget v28, v23, v4

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v30, v0

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->cos(D)D

    move-result-wide v30

    mul-double v28, v28, v30

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v22, v0

    .line 260
    .restart local v22    # "x":F
    aget v24, v25, v4

    .line 261
    .restart local v24    # "y":F
    aget v28, v23, v4

    move/from16 v0, v28

    float-to-double v0, v0

    move-wide/from16 v28, v0

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v30, v0

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->sin(D)D

    move-result-wide v30

    mul-double v28, v28, v30

    move-wide/from16 v0, v28

    double-to-float v0, v0

    move/from16 v26, v0

    .line 263
    .restart local v26    # "z":F
    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 264
    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 265
    move-object/from16 v0, v16

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 267
    const/16 v28, 0x0

    aget v28, v7, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 268
    const/16 v28, 0x1

    aget v28, v7, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 269
    const/16 v28, 0x2

    aget v28, v7, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 270
    const/16 v28, 0x3

    aget v28, v7, v28

    move/from16 v0, v28

    invoke-virtual {v5, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 257
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 201
    nop

    :array_0
    .array-data 4
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f19999a    # 0.6f
    .end array-data

    .line 202
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f19999a    # 0.6f
    .end array-data
.end method

.method protected isBelongsToGeometry(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Z
    .locals 1
    .param p1, "aCheckDot"    # Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .prologue
    .line 161
    const/4 v0, 0x0

    return v0
.end method
