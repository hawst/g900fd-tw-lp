.class public Lcom/sec/android/app/clockpackage/sgi/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final CORRECTION_FACTOR_HEIGHT:F = 1920.0f

.field public static final CORRECTION_FACTOR_WIDTH:F = 1080.0f

.field public static final INVALID_ZOOM_LEVEL:I = -0x1

.field public static final ROTATION_ANIM_DURATION:I = 0x2ee

.field public static final ROTATION_SLOW_DOWN_FACTOR:I = 0x3a98

.field public static final TOTAL_ZOOM_LEVEL:I = 0x5

.field public static final ZOOM_ANIM_DURATION:I = 0x2ee

.field public static final ZOOM_LEVEL_0:F = 8.4f

.field public static final ZOOM_LEVEL_1:F = 6.85f

.field public static final ZOOM_LEVEL_2:F = 5.3f

.field public static final ZOOM_LEVEL_3:F = 3.75f

.field public static final ZOOM_LEVEL_4:F = 2.2f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
