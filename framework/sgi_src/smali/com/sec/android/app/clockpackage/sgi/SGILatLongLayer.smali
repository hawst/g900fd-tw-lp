.class public Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;
.super Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
.source "SGILatLongLayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;
    }
.end annotation


# static fields
.field static final FRAGMENT_SHADER:Ljava/lang/String; = "precision highp float;\nvarying vec4 vColor;void main(){\n    gl_FragColor = vColor;\n}\n"

.field static final VERTEX_SHADER:Ljava/lang/String; = "attribute vec3 SGPositions;\nattribute vec4 color;\nuniform mat4 ModelViewProjection;\nvarying vec4 vColor;void main(){\n    vColor = color;\n    gl_Position = ModelViewProjection * vec4(SGPositions, 1.0);\n}\n"


# instance fields
.field private mBlendingProperty:Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

.field private mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

.field private mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

.field private mProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

.field projMat:[F

.field projMatrix4f:Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

.field tempMat:[F

.field viewMat:[F

.field viewMatrix4f:Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/16 v4, 0x10

    .line 82
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 49
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    .line 51
    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMatrix4f:Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    .line 52
    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->viewMatrix4f:Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    .line 53
    new-array v3, v4, [F

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    .line 54
    new-array v3, v4, [F

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->viewMat:[F

    .line 55
    new-array v3, v4, [F

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    .line 83
    new-instance v1, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;

    const/16 v3, 0x50

    const/16 v4, 0x17

    const v5, 0x3f8020c5    # 1.001f

    invoke-direct {v1, p0, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;-><init>(Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;IIF)V

    .line 84
    .local v1, "latlongWireframeGeometry":Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer$CustomGeometryLatLongGenerator;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setGeometryGenerator(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V

    .line 85
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setGeometryGeneratorParam(F)V

    .line 87
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    sget-object v3, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->VERTEX:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v4, "attribute vec3 SGPositions;\nattribute vec4 color;\nuniform mat4 ModelViewProjection;\nvarying vec4 vColor;void main(){\n    vColor = color;\n    gl_Position = ModelViewProjection * vec4(SGPositions, 1.0);\n}\n"

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 88
    .local v2, "vertex":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    sget-object v3, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->FRAGMENT:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v4, "precision highp float;\nvarying vec4 vColor;void main(){\n    gl_FragColor = vColor;\n}\n"

    invoke-direct {v0, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 89
    .local v0, "fragment":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-direct {v3, v2, v0}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;)V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->mProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    .line 91
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    .line 92
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->setDepthTestEnabled(Z)V

    .line 93
    const-string v3, "SGDepthProperty"

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 95
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->mBlendingProperty:Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

    .line 96
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->mBlendingProperty:Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->setAlphaBlendingEnabled(Z)V

    .line 97
    const-string v3, "SGAlphaBlend"

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->mBlendingProperty:Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 99
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->mProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V

    .line 100
    return-void
.end method

.method public static perspectiveM([FIFFFF)V
    .locals 8
    .param p0, "m"    # [F
    .param p1, "offset"    # I
    .param p2, "fovy"    # F
    .param p3, "aspect"    # F
    .param p4, "zNear"    # F
    .param p5, "zFar"    # F

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 58
    float-to-double v2, p2

    const-wide v4, 0x3f81df46a2529d39L    # 0.008726646259971648

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v2, v2

    div-float v0, v7, v2

    .line 59
    .local v0, "f":F
    sub-float v2, p4, p5

    div-float v1, v7, v2

    .line 61
    .local v1, "rangeReciprocal":F
    add-int/lit8 v2, p1, 0x0

    div-float v3, v0, p3

    aput v3, p0, v2

    .line 62
    add-int/lit8 v2, p1, 0x1

    aput v6, p0, v2

    .line 63
    add-int/lit8 v2, p1, 0x2

    aput v6, p0, v2

    .line 64
    add-int/lit8 v2, p1, 0x3

    aput v6, p0, v2

    .line 66
    add-int/lit8 v2, p1, 0x4

    aput v6, p0, v2

    .line 67
    add-int/lit8 v2, p1, 0x5

    aput v0, p0, v2

    .line 68
    add-int/lit8 v2, p1, 0x6

    aput v6, p0, v2

    .line 69
    add-int/lit8 v2, p1, 0x7

    aput v6, p0, v2

    .line 71
    add-int/lit8 v2, p1, 0x8

    aput v6, p0, v2

    .line 72
    add-int/lit8 v2, p1, 0x9

    aput v6, p0, v2

    .line 73
    add-int/lit8 v2, p1, 0xa

    add-float v3, p5, p4

    mul-float/2addr v3, v1

    aput v3, p0, v2

    .line 74
    add-int/lit8 v2, p1, 0xb

    const/high16 v3, -0x40800000    # -1.0f

    aput v3, p0, v2

    .line 76
    add-int/lit8 v2, p1, 0xc

    aput v6, p0, v2

    .line 77
    add-int/lit8 v2, p1, 0xd

    aput v6, p0, v2

    .line 78
    add-int/lit8 v2, p1, 0xe

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, p5

    mul-float/2addr v3, p4

    mul-float/2addr v3, v1

    aput v3, p0, v2

    .line 79
    add-int/lit8 v2, p1, 0xf

    aput v6, p0, v2

    .line 80
    return-void
.end method


# virtual methods
.method public setDistance([FFFF)V
    .locals 20
    .param p1, "eye"    # [F
    .param p2, "distance"    # F
    .param p3, "hRotation"    # F
    .param p4, "vRotation"    # F

    .prologue
    .line 132
    const/4 v2, 0x0

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 133
    const/4 v2, 0x1

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    aput v3, p1, v2

    .line 134
    const/4 v2, 0x2

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 135
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->viewMat:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, p1, v4

    const/4 v5, 0x1

    aget v5, p1, v5

    const/4 v6, 0x2

    aget v6, p1, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 137
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 138
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->viewMat:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 139
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 140
    const-string v2, "ModelViewProjection"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 141
    return-void
.end method

.method public setRotation([FFFF)V
    .locals 20
    .param p1, "eye"    # [F
    .param p2, "distance"    # F
    .param p3, "hRotation"    # F
    .param p4, "vRotation"    # F

    .prologue
    .line 120
    const/4 v2, 0x0

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 121
    const/4 v2, 0x1

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    aput v3, p1, v2

    .line 122
    const/4 v2, 0x2

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 123
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->viewMat:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, p1, v4

    const/4 v5, 0x1

    aget v5, p1, v5

    const/4 v6, 0x2

    aget v6, p1, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 125
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 126
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->viewMat:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 127
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 128
    const-string v2, "ModelViewProjection"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 129
    return-void
.end method

.method public setSize(IIFFFF[FFFF)V
    .locals 20
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "FOVY"    # F
    .param p4, "ASPECT_RATIO"    # F
    .param p5, "NEAR_PLANE"    # F
    .param p6, "FAR_PLANE"    # F
    .param p7, "eye"    # [F
    .param p8, "distance"    # F
    .param p9, "hRotation"    # F
    .param p10, "vRotation"    # F

    .prologue
    .line 103
    move/from16 v0, p1

    int-to-float v2, v0

    move/from16 v0, p2

    int-to-float v3, v0

    move-object/from16 v0, p0

    invoke-super {v0, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 105
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/4 v3, 0x0

    const/high16 v7, 0x447a0000    # 1000.0f

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    invoke-static/range {v2 .. v7}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->perspectiveM([FIFFFF)V

    .line 106
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMatrix4f:Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->set(FFFFFFFFFFFFFFFF)V

    .line 108
    const/4 v2, 0x0

    move/from16 v0, p9

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p8

    move/from16 v0, p10

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p7, v2

    .line 109
    const/4 v2, 0x1

    move/from16 v0, p10

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p8

    aput v3, p7, v2

    .line 110
    const/4 v2, 0x2

    move/from16 v0, p9

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p8

    move/from16 v0, p10

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p7, v2

    .line 111
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->viewMat:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, p7, v4

    const/4 v5, 0x1

    aget v5, p7, v5

    const/4 v6, 0x2

    aget v6, p7, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 113
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->projMat:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->viewMat:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 115
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->tempMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 116
    const-string v2, "ModelViewProjection"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 117
    return-void
.end method
