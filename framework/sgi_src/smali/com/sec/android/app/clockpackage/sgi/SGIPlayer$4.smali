.class Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$4;
.super Ljava/lang/Object;
.source "SGIPlayer.java"

# interfaces
.implements Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;-><init>(Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;Landroid/view/ViewGroup;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$4;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChangesDrawn()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$4;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mDayBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$2(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$4;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGradientBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$3(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$4;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$4(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$4;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mBillboardLayer:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;
    invoke-static {v0}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$5(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->recycleAtlas()V

    .line 137
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 138
    return-void
.end method
