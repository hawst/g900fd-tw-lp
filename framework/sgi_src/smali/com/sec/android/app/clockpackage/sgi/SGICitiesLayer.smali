.class public Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;
.super Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
.source "SGICitiesLayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;
    }
.end annotation


# static fields
.field static final FRAGMENT_SHADER:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D SGTexture;\nvarying vec2 vTextureCoords;\nvarying float alpha;void main(){\n\tvec4 sceneColor = texture2D(SGTexture, vTextureCoords);\n\tgl_FragColor = sceneColor;\n\tgl_FragColor.a *= alpha;}\n"

.field static final VERTEX_SHADER:Ljava/lang/String; = "attribute vec3 SGPositions;\nattribute vec2 SGTextureCoords;\nattribute vec4 position;uniform mat4 MyWorldViewProjection;\nuniform mat4 MyWorldView;\nuniform vec3 MyCamPos;\nuniform float currentZoomLevel;\nvarying vec2 vTextureCoords;\nvarying float alpha;void main(){\n\tvec3 vVector = (vec4(0.0, 1.0, 0.0, 1.0)*MyWorldView).xyz;\tvec3 hVector = (vec4(1.0, 0.0, 0.0, 1.0)*MyWorldView).xyz;\tvec3 camVector = (vec4(0.0, 0.0, 1.0, 1.0)*MyWorldView).xyz;\talpha = 1.0 - smoothstep(0.2, 0.4, 1.0 - dot(camVector, position.xyz));\tfloat dis = (0.4)*length(position.xyz-MyCamPos);\tif(alpha < 0.001 || (position.w > currentZoomLevel)){\t\tgl_Position = vec4(100.0);\t\tvTextureCoords = vec2(0.0);\t}else{\t\tgl_Position = MyWorldViewProjection * vec4(position.xyz + SGPositions.x*hVector*dis + SGPositions.y*vVector*dis, 1.0);\n\t\tvTextureCoords = SGTextureCoords;\n\t}}\n"


# instance fields
.field private atlas:Landroid/graphics/Bitmap;

.field billboardGeometry:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;

.field context:Landroid/app/Activity;

.field handler:Landroid/os/Handler;

.field private mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

.field private mCurrentZoomLevel:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

.field private mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

.field private mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

.field private mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

.field private mProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

.field private mTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

.field projMat:[F

.field tempMat:[F

.field viewMat:[F


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 5
    .param p1, "context"    # Landroid/app/Activity;

    .prologue
    const/16 v3, 0x10

    .line 135
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 94
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    invoke-direct {v2}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    .line 95
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    invoke-direct {v2}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    .line 96
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    invoke-direct {v2}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    .line 98
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    invoke-direct {v2}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCurrentZoomLevel:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    .line 100
    new-array v2, v3, [F

    iput-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->projMat:[F

    .line 101
    new-array v2, v3, [F

    iput-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    .line 102
    new-array v2, v3, [F

    iput-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    .line 136
    iput-object p1, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->context:Landroid/app/Activity;

    .line 137
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->handler:Landroid/os/Handler;

    .line 139
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    sget-object v3, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    sget-object v4, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;)V

    iput-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    .line 141
    new-instance v2, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;

    invoke-direct {v2, p0}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;-><init>(Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;)V

    iput-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->billboardGeometry:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;

    .line 142
    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->billboardGeometry:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setGeometryGenerator(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V

    .line 143
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setGeometryGeneratorParam(F)V

    .line 145
    new-instance v1, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->VERTEX:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v3, "attribute vec3 SGPositions;\nattribute vec2 SGTextureCoords;\nattribute vec4 position;uniform mat4 MyWorldViewProjection;\nuniform mat4 MyWorldView;\nuniform vec3 MyCamPos;\nuniform float currentZoomLevel;\nvarying vec2 vTextureCoords;\nvarying float alpha;void main(){\n\tvec3 vVector = (vec4(0.0, 1.0, 0.0, 1.0)*MyWorldView).xyz;\tvec3 hVector = (vec4(1.0, 0.0, 0.0, 1.0)*MyWorldView).xyz;\tvec3 camVector = (vec4(0.0, 0.0, 1.0, 1.0)*MyWorldView).xyz;\talpha = 1.0 - smoothstep(0.2, 0.4, 1.0 - dot(camVector, position.xyz));\tfloat dis = (0.4)*length(position.xyz-MyCamPos);\tif(alpha < 0.001 || (position.w > currentZoomLevel)){\t\tgl_Position = vec4(100.0);\t\tvTextureCoords = vec2(0.0);\t}else{\t\tgl_Position = MyWorldViewProjection * vec4(position.xyz + SGPositions.x*hVector*dis + SGPositions.y*vVector*dis, 1.0);\n\t\tvTextureCoords = SGTextureCoords;\n\t}}\n"

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 146
    .local v1, "vertex":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->FRAGMENT:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v3, "precision mediump float;\nuniform sampler2D SGTexture;\nvarying vec2 vTextureCoords;\nvarying float alpha;void main(){\n\tvec4 sceneColor = texture2D(SGTexture, vTextureCoords);\n\tgl_FragColor = sceneColor;\n\tgl_FragColor.a *= alpha;}\n"

    invoke-direct {v0, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 147
    .local v0, "fragment":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-direct {v2, v1, v0}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;)V

    iput-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    .line 149
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    invoke-direct {v2}, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    .line 150
    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->setDepthTestEnabled(Z)V

    .line 151
    const-string v2, "SGDepthProperty"

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 153
    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCurrentZoomLevel:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->set(F)V

    .line 154
    const-string v2, "currentZoomLevel"

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCurrentZoomLevel:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-virtual {p0, v2}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V

    .line 157
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->atlas:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$1(Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->atlas:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private dot([F[F)F
    .locals 4
    .param p1, "v1"    # [F
    .param p2, "v2"    # [F

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 316
    aget v0, p1, v1

    aget v1, p2, v1

    mul-float/2addr v0, v1

    aget v1, p1, v2

    aget v2, p2, v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    aget v1, p1, v3

    aget v2, p2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method private findZoomLevel(F)I
    .locals 9
    .param p1, "distance"    # F

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 321
    const v3, 0x400ccccd    # 2.2f

    sub-float v3, p1, v3

    const v4, 0x40c66666    # 6.2f

    div-float v1, v3, v4

    .line 322
    .local v1, "normalisedRange":F
    const/high16 v3, 0x40a00000    # 5.0f

    mul-float/2addr v3, v1

    float-to-int v2, v3

    .line 324
    .local v2, "zoomLevel":I
    const/4 v0, -0x1

    .line 325
    .local v0, "actualZoomLevel":I
    if-lt v2, v8, :cond_1

    const/4 v3, 0x5

    if-gt v2, v3, :cond_1

    .line 326
    const/4 v0, 0x0

    .line 336
    :cond_0
    :goto_0
    return v0

    .line 327
    :cond_1
    if-lt v2, v7, :cond_2

    if-ge v2, v8, :cond_2

    .line 328
    const/4 v0, 0x1

    goto :goto_0

    .line 329
    :cond_2
    if-lt v2, v6, :cond_3

    if-ge v2, v7, :cond_3

    .line 330
    const/4 v0, 0x2

    goto :goto_0

    .line 331
    :cond_3
    if-lt v2, v5, :cond_4

    if-ge v2, v6, :cond_4

    .line 332
    const/4 v0, 0x3

    goto :goto_0

    .line 333
    :cond_4
    if-ltz v2, :cond_0

    if-ge v2, v5, :cond_0

    .line 334
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private getCityAt(FFFFFF)Lcom/sec/android/app/clockpackage/worldclock/City;
    .locals 26
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "distance"    # F
    .param p5, "windowX"    # F
    .param p6, "windowY"    # F

    .prologue
    .line 273
    move/from16 v0, p3

    float-to-double v2, v0

    move/from16 v0, p1

    float-to-double v4, v0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    double-to-float v2, v2

    neg-float v2, v2

    const/high16 v3, 0x43340000    # 180.0f

    mul-float/2addr v2, v3

    const v3, 0x4048f5c3    # 3.14f

    div-float v23, v2, v3

    .line 274
    .local v23, "touchLongitude":F
    move/from16 v0, p2

    float-to-double v2, v0

    mul-float v4, p1, p1

    mul-float v5, p3, p3

    add-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    float-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    double-to-float v2, v2

    const/high16 v3, 0x43340000    # 180.0f

    mul-float/2addr v2, v3

    const v3, 0x4048f5c3    # 3.14f

    div-float v22, v2, v3

    .line 276
    .local v22, "touchLatitude":F
    const/high16 v2, 0x40b00000    # 5.5f

    mul-float v16, p4, v2

    .line 277
    .local v16, "cutOffDistLat":F
    const/high16 v2, 0x40b00000    # 5.5f

    mul-float v17, p4, v2

    .line 279
    .local v17, "cutOffDistLong":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->billboardGeometry:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;

    invoke-virtual {v2}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer$CustomGeometryBillboardGenerator;->getBillboardDatas()[Lcom/sec/android/app/clockpackage/sgi/BillboardData;

    move-result-object v13

    .line 280
    .local v13, "billboardDatas":[Lcom/sec/android/app/clockpackage/sgi/BillboardData;
    const/4 v2, 0x4

    new-array v11, v2, [F

    .line 282
    .local v11, "tempCoords":[F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v24

    .line 283
    .local v24, "viewSize":Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    sub-float p6, v2, p6

    .line 285
    const/16 v21, 0x0

    .line 286
    .local v21, "selectedCity":Lcom/sec/android/app/clockpackage/worldclock/City;
    invoke-static {}, Lcom/sec/android/app/clockpackage/worldclock/CityManager;->getCitiesByOffset()[Lcom/sec/android/app/clockpackage/worldclock/City;

    move-result-object v14

    .line 287
    .local v14, "cities":[Lcom/sec/android/app/clockpackage/worldclock/City;
    array-length v2, v14

    add-int/lit8 v20, v2, -0x1

    .local v20, "i":I
    :goto_0
    if-gez v20, :cond_0

    .line 312
    :goto_1
    return-object v21

    .line 288
    :cond_0
    aget-object v15, v14, v20

    .line 290
    .local v15, "city":Lcom/sec/android/app/clockpackage/worldclock/City;
    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-direct {v0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->findZoomLevel(F)I

    move-result v25

    .line 291
    .local v25, "zoomLevel":I
    iget v2, v15, Lcom/sec/android/app/clockpackage/worldclock/City;->mZoomLevel:I

    move/from16 v0, v25

    if-ge v0, v2, :cond_1

    invoke-virtual {v15}, Lcom/sec/android/app/clockpackage/worldclock/City;->getDBSelected()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 292
    :cond_1
    iget v2, v15, Lcom/sec/android/app/clockpackage/worldclock/City;->mLongitude:F

    sub-float v2, v23, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v18

    .line 293
    .local v18, "distLat":F
    iget v2, v15, Lcom/sec/android/app/clockpackage/worldclock/City;->mLatitude:F

    sub-float v2, v22, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v19

    .line 294
    .local v19, "distLong":F
    cmpg-float v2, v18, v16

    if-gez v2, :cond_2

    cmpg-float v2, v19, v17

    if-gez v2, :cond_2

    .line 296
    aget-object v2, v13, v20

    iget v2, v2, Lcom/sec/android/app/clockpackage/sgi/BillboardData;->x:F

    aget-object v3, v13, v20

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/BillboardData;->y:F

    aget-object v4, v13, v20

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/BillboardData;->z:F

    .line 297
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->projMat:[F

    const/4 v8, 0x0

    const/4 v9, 0x4

    new-array v9, v9, [I

    const/4 v10, 0x2

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v12

    float-to-int v12, v12

    aput v12, v9, v10

    const/4 v10, 0x3

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v12

    float-to-int v12, v12

    aput v12, v9, v10

    const/4 v10, 0x0

    const/4 v12, 0x0

    .line 296
    invoke-static/range {v2 .. v12}, Landroid/opengl/GLU;->gluProject(FFF[FI[FI[II[FI)I

    .line 299
    const/4 v2, 0x1

    aget v3, v11, v2

    aget-object v4, v13, v20

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/BillboardData;->vOffset:F

    add-float/2addr v3, v4

    aput v3, v11, v2

    .line 301
    const/4 v2, 0x0

    aget v2, v11, v2

    aget-object v3, v13, v20

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/BillboardData;->width:F

    sub-float/2addr v2, v3

    cmpl-float v2, p5, v2

    if-lez v2, :cond_2

    const/4 v2, 0x0

    aget v2, v11, v2

    aget-object v3, v13, v20

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/BillboardData;->width:F

    add-float/2addr v2, v3

    cmpg-float v2, p5, v2

    if-gez v2, :cond_2

    .line 302
    const/4 v2, 0x1

    aget v2, v11, v2

    aget-object v3, v13, v20

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/BillboardData;->height:F

    sub-float/2addr v2, v3

    cmpl-float v2, p6, v2

    if-lez v2, :cond_2

    const/4 v2, 0x1

    aget v2, v11, v2

    aget-object v3, v13, v20

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/BillboardData;->height:F

    add-float/2addr v2, v3

    cmpg-float v2, p6, v2

    if-gez v2, :cond_2

    .line 303
    move-object/from16 v21, v15

    .line 304
    goto/16 :goto_1

    .line 287
    .end local v18    # "distLat":F
    .end local v19    # "distLong":F
    :cond_2
    add-int/lit8 v20, v20, -0x1

    goto/16 :goto_0
.end method

.method public static perspectiveM([FIFFFF)V
    .locals 8
    .param p0, "m"    # [F
    .param p1, "offset"    # I
    .param p2, "fovy"    # F
    .param p3, "aspect"    # F
    .param p4, "zNear"    # F
    .param p5, "zFar"    # F

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 111
    float-to-double v2, p2

    const-wide v4, 0x3f81df46a2529d39L    # 0.008726646259971648

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v2, v2

    div-float v0, v7, v2

    .line 112
    .local v0, "f":F
    sub-float v2, p4, p5

    div-float v1, v7, v2

    .line 114
    .local v1, "rangeReciprocal":F
    add-int/lit8 v2, p1, 0x0

    div-float v3, v0, p3

    aput v3, p0, v2

    .line 115
    add-int/lit8 v2, p1, 0x1

    aput v6, p0, v2

    .line 116
    add-int/lit8 v2, p1, 0x2

    aput v6, p0, v2

    .line 117
    add-int/lit8 v2, p1, 0x3

    aput v6, p0, v2

    .line 119
    add-int/lit8 v2, p1, 0x4

    aput v6, p0, v2

    .line 120
    add-int/lit8 v2, p1, 0x5

    aput v0, p0, v2

    .line 121
    add-int/lit8 v2, p1, 0x6

    aput v6, p0, v2

    .line 122
    add-int/lit8 v2, p1, 0x7

    aput v6, p0, v2

    .line 124
    add-int/lit8 v2, p1, 0x8

    aput v6, p0, v2

    .line 125
    add-int/lit8 v2, p1, 0x9

    aput v6, p0, v2

    .line 126
    add-int/lit8 v2, p1, 0xa

    add-float v3, p5, p4

    mul-float/2addr v3, v1

    aput v3, p0, v2

    .line 127
    add-int/lit8 v2, p1, 0xb

    const/high16 v3, -0x40800000    # -1.0f

    aput v3, p0, v2

    .line 129
    add-int/lit8 v2, p1, 0xc

    aput v6, p0, v2

    .line 130
    add-int/lit8 v2, p1, 0xd

    aput v6, p0, v2

    .line 131
    add-int/lit8 v2, p1, 0xe

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, p5

    mul-float/2addr v3, p4

    mul-float/2addr v3, v1

    aput v3, p0, v2

    .line 132
    add-int/lit8 v2, p1, 0xf

    aput v6, p0, v2

    .line 133
    return-void
.end method


# virtual methods
.method public recycleAtlas()V
    .locals 1

    .prologue
    .line 782
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->atlas:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 783
    return-void
.end method

.method public setDistance([FFFF)V
    .locals 20
    .param p1, "eye"    # [F
    .param p2, "distance"    # F
    .param p3, "hRotation"    # F
    .param p4, "vRotation"    # F

    .prologue
    .line 206
    const/4 v2, 0x0

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 207
    const/4 v2, 0x1

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    aput v3, p1, v2

    .line 208
    const/4 v2, 0x2

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 209
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, p1, v4

    const/4 v5, 0x1

    aget v5, p1, v5

    const/4 v6, 0x2

    aget v6, p1, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 211
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 212
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->projMat:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 213
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 214
    const-string v2, "MyWorldViewProjection"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 216
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 217
    const-string v2, "MyWorldView"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 219
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    const/4 v3, 0x0

    aget v3, p1, v3

    const/4 v4, 0x1

    aget v4, p1, v4

    const/4 v5, 0x2

    aget v5, p1, v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->set(FFF)V

    .line 220
    const-string v2, "MyCamPos"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 222
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCurrentZoomLevel:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->findZoomLevel(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->set(F)V

    .line 223
    const-string v2, "currentZoomLevel"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCurrentZoomLevel:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 224
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 232
    const-string v0, "SGTexture"

    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 233
    return-void
.end method

.method public setImage(Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    .locals 1
    .param p1, "image"    # Lcom/samsung/android/sdk/sgi/render/SGProperty;

    .prologue
    .line 227
    const-string v0, "SGTexture"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 228
    return-void
.end method

.method public setPickAt(FFFFF[FF)Lcom/sec/android/app/clockpackage/worldclock/City;
    .locals 28
    .param p1, "windowX"    # F
    .param p2, "windowY"    # F
    .param p3, "FOVY"    # F
    .param p4, "ASPECT_RATIO"    # F
    .param p5, "NEAR_PLANE"    # F
    .param p6, "eye"    # [F
    .param p7, "distance"    # F

    .prologue
    .line 236
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v27

    .line 237
    .local v27, "size":Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v5

    div-float v5, p1, v5

    const/high16 v7, 0x3f000000    # 0.5f

    sub-float/2addr v5, v7

    const/high16 v7, 0x40000000    # 2.0f

    mul-float v22, v5, v7

    .line 238
    .local v22, "n2X":F
    invoke-virtual/range {v27 .. v27}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v5

    sub-float v5, v5, p2

    invoke-virtual/range {v27 .. v27}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v7

    div-float/2addr v5, v7

    const/high16 v7, 0x3f000000    # 0.5f

    sub-float/2addr v5, v7

    const/high16 v7, 0x40000000    # 2.0f

    mul-float v23, v5, v7

    .line 240
    .local v23, "n2Y":F
    const v5, 0x4048f5c3    # 3.14f

    mul-float v5, v5, p3

    const/high16 v7, 0x43340000    # 180.0f

    div-float/2addr v5, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v5, v7

    float-to-double v10, v5

    invoke-static {v10, v11}, Ljava/lang/Math;->tan(D)D

    move-result-wide v10

    double-to-float v5, v10

    mul-float v25, v5, p5

    .line 241
    .local v25, "nearPlaneHalfHeight":F
    mul-float v26, p4, v25

    .line 242
    .local v26, "nearPlaneHalfWidth":F
    const/4 v5, 0x4

    new-array v8, v5, [F

    const/4 v5, 0x0

    mul-float v7, v26, v22

    aput v7, v8, v5

    const/4 v5, 0x1

    mul-float v7, v25, v23

    aput v7, v8, v5

    const/4 v5, 0x2

    sub-float v7, p7, p5

    aput v7, v8, v5

    const/4 v5, 0x3

    const/high16 v7, 0x3f800000    # 1.0f

    aput v7, v8, v5

    .line 244
    .local v8, "touchPointOnNearPlane":[F
    const/16 v5, 0x10

    new-array v6, v5, [F

    .line 245
    .local v6, "viewTrans":[F
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v9, 0x0

    invoke-static {v6, v5, v7, v9}, Landroid/opengl/Matrix;->transposeM([FI[FI)V

    .line 247
    const/4 v5, 0x4

    new-array v4, v5, [F

    .line 248
    .local v4, "worldTouchPoint":[F
    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-static/range {v4 .. v9}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 250
    const/4 v5, 0x0

    aget v7, v4, v5

    const/4 v9, 0x0

    aget v9, p6, v9

    sub-float/2addr v7, v9

    aput v7, v4, v5

    .line 251
    const/4 v5, 0x1

    aget v7, v4, v5

    const/4 v9, 0x1

    aget v9, p6, v9

    sub-float/2addr v7, v9

    aput v7, v4, v5

    .line 252
    const/4 v5, 0x2

    aget v7, v4, v5

    const/4 v9, 0x2

    aget v9, p6, v9

    sub-float/2addr v7, v9

    aput v7, v4, v5

    .line 253
    const/4 v5, 0x0

    aget v5, v4, v5

    const/4 v7, 0x0

    aget v7, v4, v7

    mul-float/2addr v5, v7

    const/4 v7, 0x1

    aget v7, v4, v7

    const/4 v9, 0x1

    aget v9, v4, v9

    mul-float/2addr v7, v9

    add-float/2addr v5, v7

    const/4 v7, 0x2

    aget v7, v4, v7

    const/4 v9, 0x2

    aget v9, v4, v9

    mul-float/2addr v7, v9

    add-float/2addr v5, v7

    float-to-double v10, v5

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v0, v10

    move/from16 v21, v0

    .line 254
    .local v21, "mag":F
    const/4 v5, 0x3

    new-array v0, v5, [F

    move-object/from16 v19, v0

    const/4 v5, 0x0

    const/4 v7, 0x0

    aget v7, v4, v7

    div-float v7, v7, v21

    aput v7, v19, v5

    const/4 v5, 0x1

    const/4 v7, 0x1

    aget v7, v4, v7

    div-float v7, v7, v21

    aput v7, v19, v5

    const/4 v5, 0x2

    const/4 v7, 0x2

    aget v7, v4, v7

    div-float v7, v7, v21

    aput v7, v19, v5

    .line 256
    .local v19, "directionVector":[F
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->dot([F[F)F

    move-result v16

    .line 257
    .local v16, "a":F
    const/high16 v5, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->dot([F[F)F

    move-result v7

    mul-float v17, v5, v7

    .line 258
    .local v17, "b":F
    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, p6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->dot([F[F)F

    move-result v5

    const/high16 v7, 0x3f800000    # 1.0f

    sub-float v18, v5, v7

    .line 259
    .local v18, "c":F
    mul-float v5, v17, v17

    const/high16 v7, 0x40800000    # 4.0f

    mul-float v7, v7, v16

    mul-float v7, v7, v18

    sub-float v20, v5, v7

    .line 260
    .local v20, "disc":F
    const/4 v5, 0x0

    cmpl-float v5, v20, v5

    if-lez v5, :cond_0

    .line 261
    move/from16 v0, v17

    neg-float v5, v0

    move/from16 v0, v20

    float-to-double v10, v0

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v7, v10

    sub-float/2addr v5, v7

    const/high16 v7, 0x40000000    # 2.0f

    mul-float v7, v7, v16

    div-float v24, v5, v7

    .line 262
    .local v24, "nearDist":F
    const/4 v5, 0x0

    aget v7, v19, v5

    mul-float v7, v7, v24

    aput v7, v19, v5

    .line 263
    const/4 v5, 0x1

    aget v7, v19, v5

    mul-float v7, v7, v24

    aput v7, v19, v5

    .line 264
    const/4 v5, 0x2

    aget v7, v19, v5

    mul-float v7, v7, v24

    aput v7, v19, v5

    .line 266
    const/4 v5, 0x0

    aget v5, p6, v5

    const/4 v7, 0x0

    aget v7, v19, v7

    add-float v10, v5, v7

    const/4 v5, 0x1

    aget v5, p6, v5

    const/4 v7, 0x1

    aget v7, v19, v7

    add-float v11, v5, v7

    const/4 v5, 0x2

    aget v5, p6, v5

    const/4 v7, 0x2

    aget v7, v19, v7

    add-float v12, v5, v7

    move-object/from16 v9, p0

    move/from16 v13, p7

    move/from16 v14, p1

    move/from16 v15, p2

    invoke-direct/range {v9 .. v15}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->getCityAt(FFFFFF)Lcom/sec/android/app/clockpackage/worldclock/City;

    move-result-object v5

    .line 269
    .end local v24    # "nearDist":F
    :goto_0
    return-object v5

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public setRotation([FFFF)V
    .locals 20
    .param p1, "eye"    # [F
    .param p2, "distance"    # F
    .param p3, "hRotation"    # F
    .param p4, "vRotation"    # F

    .prologue
    .line 185
    const/4 v2, 0x0

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 186
    const/4 v2, 0x1

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    aput v3, p1, v2

    .line 187
    const/4 v2, 0x2

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 188
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, p1, v4

    const/4 v5, 0x1

    aget v5, p1, v5

    const/4 v6, 0x2

    aget v6, p1, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 190
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->projMat:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 192
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 193
    const-string v2, "MyWorldViewProjection"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 195
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 196
    const-string v2, "MyWorldView"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 198
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    const/4 v3, 0x0

    aget v3, p1, v3

    const/4 v4, 0x1

    aget v4, p1, v4

    const/4 v5, 0x2

    aget v5, p1, v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->set(FFF)V

    .line 199
    const-string v2, "MyCamPos"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 201
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCurrentZoomLevel:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->findZoomLevel(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->set(F)V

    .line 202
    const-string v2, "currentZoomLevel"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCurrentZoomLevel:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 203
    return-void
.end method

.method public setSize(IIFFFF[FFFF)V
    .locals 20
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "FOVY"    # F
    .param p4, "ASPECT_RATIO"    # F
    .param p5, "NEAR_PLANE"    # F
    .param p6, "FAR_PLANE"    # F
    .param p7, "eye"    # [F
    .param p8, "distance"    # F
    .param p9, "hRotation"    # F
    .param p10, "vRotation"    # F

    .prologue
    .line 160
    move/from16 v0, p1

    int-to-float v2, v0

    move/from16 v0, p2

    int-to-float v3, v0

    move-object/from16 v0, p0

    invoke-super {v0, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 162
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->projMat:[F

    const/4 v3, 0x0

    const/high16 v7, 0x447a0000    # 1000.0f

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    invoke-static/range {v2 .. v7}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->perspectiveM([FIFFFF)V

    .line 164
    const/4 v2, 0x0

    move/from16 v0, p9

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p8

    move/from16 v0, p10

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p7, v2

    .line 165
    const/4 v2, 0x1

    move/from16 v0, p10

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p8

    aput v3, p7, v2

    .line 166
    const/4 v2, 0x2

    move/from16 v0, p9

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p8

    move/from16 v0, p10

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p7, v2

    .line 167
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, p7, v4

    const/4 v5, 0x1

    aget v5, p7, v5

    const/4 v6, 0x2

    aget v6, p7, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 169
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 170
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->projMat:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 171
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->tempMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 172
    const-string v2, "MyWorldViewProjection"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 174
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->viewMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 175
    const-string v2, "MyWorldView"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 177
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    const/4 v3, 0x0

    aget v3, p7, v3

    const/4 v4, 0x1

    aget v4, p7, v4

    const/4 v5, 0x2

    aget v5, p7, v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->set(FFF)V

    .line 178
    const-string v2, "MyCamPos"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 180
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCurrentZoomLevel:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    move-object/from16 v0, p0

    move/from16 v1, p8

    invoke-direct {v0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->findZoomLevel(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;->set(F)V

    .line 181
    const-string v2, "currentZoomLevel"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->mCurrentZoomLevel:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 182
    return-void
.end method
