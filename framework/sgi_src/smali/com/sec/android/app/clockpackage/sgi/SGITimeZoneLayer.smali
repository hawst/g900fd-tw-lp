.class public Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;
.super Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
.source "SGITimeZoneLayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;
    }
.end annotation


# static fields
.field static final FRAGMENT_SHADER:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D SGTexture;\nvarying vec2 vTextureCoords;\nvoid main(){\n    gl_FragColor = texture2D(SGTexture, vTextureCoords);\n}\n"

.field static final VERTEX_SHADER:Ljava/lang/String; = "attribute vec3 SGPositions;\nattribute vec2 SGTextureCoords;\nuniform mat4 MyWorldViewProjection;\nvarying vec2 vTextureCoords;\nvoid main(){\n    vTextureCoords = SGTextureCoords;\n    gl_Position = MyWorldViewProjection * vec4(SGPositions, 1.0);\n}\n"

.field private static mTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;


# instance fields
.field context:Landroid/content/Context;

.field index:I

.field private mBlendingProperty:Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

.field private mCullfaceProperty:Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;

.field private mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

.field private mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

.field private mProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

.field projMat:[F

.field projMatrix4f:Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

.field tempMat:[F

.field viewMat:[F

.field viewMatrix4f:Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "index"    # I

    .prologue
    const/4 v6, 0x1

    const/16 v4, 0x10

    .line 102
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 66
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    .line 68
    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMatrix4f:Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    .line 69
    new-instance v3, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->viewMatrix4f:Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    .line 70
    new-array v3, v4, [F

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    .line 71
    new-array v3, v4, [F

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->viewMat:[F

    .line 72
    new-array v3, v4, [F

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    .line 103
    iput-object p1, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->context:Landroid/content/Context;

    .line 104
    iput p2, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->index:I

    .line 106
    new-instance v1, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;

    const/4 v3, -0x1

    invoke-direct {v1, p0, v3}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;-><init>(Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;I)V

    .line 107
    .local v1, "timezoneGeometry":Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setGeometryGenerator(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V

    .line 108
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setGeometryGeneratorParam(F)V

    .line 110
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    sget-object v3, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->VERTEX:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v4, "attribute vec3 SGPositions;\nattribute vec2 SGTextureCoords;\nuniform mat4 MyWorldViewProjection;\nvarying vec2 vTextureCoords;\nvoid main(){\n    vTextureCoords = SGTextureCoords;\n    gl_Position = MyWorldViewProjection * vec4(SGPositions, 1.0);\n}\n"

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 111
    .local v2, "vertex":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    sget-object v3, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->FRAGMENT:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v4, "precision mediump float;\nuniform sampler2D SGTexture;\nvarying vec2 vTextureCoords;\nvoid main(){\n    gl_FragColor = texture2D(SGTexture, vTextureCoords);\n}\n"

    invoke-direct {v0, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 112
    .local v0, "fragment":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-direct {v3, v2, v0}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;)V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    .line 114
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    sget-object v4, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->REPEAT:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    sget-object v5, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->REPEAT:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;)V

    sput-object v3, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    .line 116
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    .line 117
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->setDepthTestEnabled(Z)V

    .line 118
    const-string v3, "SGDepthProperty"

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 120
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mBlendingProperty:Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

    .line 121
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mBlendingProperty:Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->setAlphaBlendingEnabled(Z)V

    .line 122
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mBlendingProperty:Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

    sget-object v4, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    sget-object v5, Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;->ONE_MINUS_SRC_ALPHA:Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;->setFactors(Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;Lcom/samsung/android/sdk/sgi/render/SGBlendFactor;)V

    .line 123
    const-string v3, "SGAlphaBlend"

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mBlendingProperty:Lcom/samsung/android/sdk/sgi/render/SGAlphaBlendProperty;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 125
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mCullfaceProperty:Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;

    .line 126
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mCullfaceProperty:Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;

    sget-object v4, Lcom/samsung/android/sdk/sgi/render/SGFrontType;->CCW:Lcom/samsung/android/sdk/sgi/render/SGFrontType;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->setFrontType(Lcom/samsung/android/sdk/sgi/render/SGFrontType;)V

    .line 127
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mCullfaceProperty:Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;->setFaceCullingEnabled(Z)V

    .line 128
    const-string v3, "SGCullface"

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mCullfaceProperty:Lcom/samsung/android/sdk/sgi/render/SGCullFaceProperty;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 130
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V

    .line 131
    return-void
.end method

.method public static perspectiveM([FIFFFF)V
    .locals 8
    .param p0, "m"    # [F
    .param p1, "offset"    # I
    .param p2, "fovy"    # F
    .param p3, "aspect"    # F
    .param p4, "zNear"    # F
    .param p5, "zFar"    # F

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 78
    float-to-double v2, p2

    const-wide v4, 0x3f81df46a2529d39L    # 0.008726646259971648

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v2, v2

    div-float v0, v7, v2

    .line 79
    .local v0, "f":F
    sub-float v2, p4, p5

    div-float v1, v7, v2

    .line 81
    .local v1, "rangeReciprocal":F
    add-int/lit8 v2, p1, 0x0

    div-float v3, v0, p3

    aput v3, p0, v2

    .line 82
    add-int/lit8 v2, p1, 0x1

    aput v6, p0, v2

    .line 83
    add-int/lit8 v2, p1, 0x2

    aput v6, p0, v2

    .line 84
    add-int/lit8 v2, p1, 0x3

    aput v6, p0, v2

    .line 86
    add-int/lit8 v2, p1, 0x4

    aput v6, p0, v2

    .line 87
    add-int/lit8 v2, p1, 0x5

    aput v0, p0, v2

    .line 88
    add-int/lit8 v2, p1, 0x6

    aput v6, p0, v2

    .line 89
    add-int/lit8 v2, p1, 0x7

    aput v6, p0, v2

    .line 91
    add-int/lit8 v2, p1, 0x8

    aput v6, p0, v2

    .line 92
    add-int/lit8 v2, p1, 0x9

    aput v6, p0, v2

    .line 93
    add-int/lit8 v2, p1, 0xa

    add-float v3, p5, p4

    mul-float/2addr v3, v1

    aput v3, p0, v2

    .line 94
    add-int/lit8 v2, p1, 0xb

    const/high16 v3, -0x40800000    # -1.0f

    aput v3, p0, v2

    .line 96
    add-int/lit8 v2, p1, 0xc

    aput v6, p0, v2

    .line 97
    add-int/lit8 v2, p1, 0xd

    aput v6, p0, v2

    .line 98
    add-int/lit8 v2, p1, 0xe

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, p5

    mul-float/2addr v3, p4

    mul-float/2addr v3, v1

    aput v3, p0, v2

    .line 99
    add-int/lit8 v2, p1, 0xf

    aput v6, p0, v2

    .line 100
    return-void
.end method


# virtual methods
.method public setDistance([FFFF)V
    .locals 20
    .param p1, "eye"    # [F
    .param p2, "distance"    # F
    .param p3, "hRotation"    # F
    .param p4, "vRotation"    # F

    .prologue
    .line 170
    const/4 v2, 0x0

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 171
    const/4 v2, 0x1

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    aput v3, p1, v2

    .line 172
    const/4 v2, 0x2

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 173
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->viewMat:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, p1, v4

    const/4 v5, 0x1

    aget v5, p1, v5

    const/4 v6, 0x2

    aget v6, p1, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 175
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 176
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->viewMat:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 177
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 178
    const-string v2, "MyWorldViewProjection"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 179
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 186
    sget-object v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 187
    const-string v0, "SGTexture"

    sget-object v1, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 188
    return-void
.end method

.method public setImage(Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    .locals 1
    .param p1, "image"    # Lcom/samsung/android/sdk/sgi/render/SGProperty;

    .prologue
    .line 182
    const-string v0, "SGTexture"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 183
    return-void
.end method

.method public setRotation([FFFF)V
    .locals 20
    .param p1, "eye"    # [F
    .param p2, "distance"    # F
    .param p3, "hRotation"    # F
    .param p4, "vRotation"    # F

    .prologue
    .line 158
    const/4 v2, 0x0

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 159
    const/4 v2, 0x1

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    aput v3, p1, v2

    .line 160
    const/4 v2, 0x2

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 161
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->viewMat:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, p1, v4

    const/4 v5, 0x1

    aget v5, p1, v5

    const/4 v6, 0x2

    aget v6, p1, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 163
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 164
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->viewMat:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 165
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 166
    const-string v2, "MyWorldViewProjection"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 167
    return-void
.end method

.method public setSize(IIFFFF[FFFF)V
    .locals 20
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "FOVY"    # F
    .param p4, "ASPECT_RATIO"    # F
    .param p5, "NEAR_PLANE"    # F
    .param p6, "FAR_PLANE"    # F
    .param p7, "eye"    # [F
    .param p8, "distance"    # F
    .param p9, "hRotation"    # F
    .param p10, "vRotation"    # F

    .prologue
    .line 141
    move/from16 v0, p1

    int-to-float v2, v0

    move/from16 v0, p2

    int-to-float v3, v0

    move-object/from16 v0, p0

    invoke-super {v0, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 143
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/4 v3, 0x0

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    invoke-static/range {v2 .. v7}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->perspectiveM([FIFFFF)V

    .line 144
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMatrix4f:Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/base/SGMatrix4f;->set(FFFFFFFFFFFFFFFF)V

    .line 146
    const/4 v2, 0x0

    move/from16 v0, p9

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p8

    move/from16 v0, p10

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p7, v2

    .line 147
    const/4 v2, 0x1

    move/from16 v0, p10

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p8

    aput v3, p7, v2

    .line 148
    const/4 v2, 0x2

    move/from16 v0, p9

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p8

    move/from16 v0, p10

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p7, v2

    .line 149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->viewMat:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, p7, v4

    const/4 v5, 0x1

    aget v5, p7, v5

    const/4 v6, 0x2

    aget v6, p7, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 151
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->projMat:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->viewMat:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 153
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->tempMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 154
    const-string v2, "MyWorldViewProjection"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 155
    return-void
.end method

.method public setTimeZoneIndex(I)V
    .locals 2
    .param p1, "timezoneIndex"    # I

    .prologue
    .line 197
    new-instance v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;-><init>(Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;I)V

    .line 198
    .local v0, "timezoneGeometry":Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer$CustomGeometryTimeZoneGenerator;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setGeometryGenerator(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V

    .line 199
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setGeometryGeneratorParam(F)V

    .line 200
    return-void
.end method

.method public setTimezoneCompressedImage(Ljava/nio/channels/FileChannel;)V
    .locals 2
    .param p1, "fc"    # Ljava/nio/channels/FileChannel;

    .prologue
    .line 191
    invoke-static {p1}, Lcom/samsung/android/sdk/sgi/render/SGCompressedTextureFactory;->createTexture(Ljava/nio/channels/FileChannel;)Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    .line 192
    const-string v0, "SGTexture"

    sget-object v1, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->mTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 193
    return-void
.end method
