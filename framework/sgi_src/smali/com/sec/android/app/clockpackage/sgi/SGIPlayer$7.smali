.class Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;
.super Ljava/lang/Object;
.source "SGIPlayer.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->moveToCity(Lcom/sec/android/app/clockpackage/worldclock/City;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

.field private final synthetic val$hRotationEndVal:F

.field private final synthetic val$hRotationStartVal:F

.field private final synthetic val$vRotationEndVal:F

.field private final synthetic val$vRotationStartVal:F


# direct methods
.method constructor <init>(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;FFFF)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iput p2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->val$hRotationStartVal:F

    iput p3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->val$hRotationEndVal:F

    iput p4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->val$vRotationStartVal:F

    iput p5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->val$vRotationEndVal:F

    .line 479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 482
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 483
    .local v0, "value":F
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->val$hRotationStartVal:F

    sub-float v3, v4, v0

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->val$hRotationEndVal:F

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    iput v2, v1, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    .line 484
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->val$vRotationStartVal:F

    sub-float v3, v4, v0

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->val$vRotationEndVal:F

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    iput v2, v1, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    .line 486
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGlobeLayer:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$6(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget-object v2, v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v5, v5, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setRotation([FFFF)V

    .line 487
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mBillboardLayer:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$5(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget-object v2, v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v5, v5, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setRotation([FFFF)V

    .line 488
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAtmosphereLayer:Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$8(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget-object v2, v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v5, v5, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setRotation([FFFF)V

    .line 489
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mLatLongLayer:Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$9(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget-object v2, v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v5, v5, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setRotation([FFFF)V

    .line 490
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneLayer:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$10(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget-object v2, v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v3, v3, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v4, v4, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget-object v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    iget v5, v5, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setRotation([FFFF)V

    .line 491
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;

    # getter for: Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;
    invoke-static {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->access$11(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/samsung/android/sdk/sgi/vi/SGView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/sgi/vi/SGView;->invalidate()V

    .line 492
    return-void
.end method
