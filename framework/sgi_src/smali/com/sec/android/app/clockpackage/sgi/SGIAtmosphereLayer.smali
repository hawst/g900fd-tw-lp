.class public Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;
.super Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
.source "SGIAtmosphereLayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;
    }
.end annotation


# static fields
.field static final FRAGMENT_SHADER:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D SGTexture;\nvarying vec2 vTextureCoords;\nvoid main(){\n\tgl_FragColor = texture2D(SGTexture, vTextureCoords);\n}\n"

.field static final VERTEX_SHADER:Ljava/lang/String; = "attribute vec3 SGPositions;\nattribute vec2 SGTextureCoords;\nuniform mat4 MyWorldViewProjection;\nuniform mat4 MyWorldView;\nvarying vec2 vTextureCoords;\nvoid main(){\n\tvec3 vVector = (vec4(0.0, 1.0, 0.0, 1.0)*MyWorldView).xyz;\tvec3 hVector = (vec4(1.0, 0.0, 0.0, 1.0)*MyWorldView).xyz;\tgl_Position = MyWorldViewProjection * vec4(SGPositions.x*hVector + SGPositions.y*vVector, 1.0);\n\tvTextureCoords = SGTextureCoords;\n}\n"


# instance fields
.field private mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

.field private mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

.field private mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

.field private mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

.field private mProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

.field private mTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

.field projMat:[F

.field tempMat:[F

.field viewMat:[F


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const/16 v4, 0x10

    .line 93
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 60
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    .line 61
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    .line 62
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    .line 64
    new-array v3, v4, [F

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->projMat:[F

    .line 65
    new-array v3, v4, [F

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    .line 66
    new-array v3, v4, [F

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    .line 94
    new-instance v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;

    const/16 v3, 0x50

    const v4, 0x3f7d70a4    # 0.99f

    const v5, 0x3f99999a    # 1.2f

    invoke-direct {v0, p0, v3, v4, v5}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;-><init>(Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;IFF)V

    .line 95
    .local v0, "billboardGeometry":Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setGeometryGenerator(Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;)V

    .line 96
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setGeometryGeneratorParam(F)V

    .line 98
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    sget-object v3, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->VERTEX:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v4, "attribute vec3 SGPositions;\nattribute vec2 SGTextureCoords;\nuniform mat4 MyWorldViewProjection;\nuniform mat4 MyWorldView;\nvarying vec2 vTextureCoords;\nvoid main(){\n\tvec3 vVector = (vec4(0.0, 1.0, 0.0, 1.0)*MyWorldView).xyz;\tvec3 hVector = (vec4(1.0, 0.0, 0.0, 1.0)*MyWorldView).xyz;\tgl_Position = MyWorldViewProjection * vec4(SGPositions.x*hVector + SGPositions.y*vVector, 1.0);\n\tvTextureCoords = SGTextureCoords;\n}\n"

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 99
    .local v2, "vertex":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v1, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    sget-object v3, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->FRAGMENT:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v4, "precision mediump float;\nuniform sampler2D SGTexture;\nvarying vec2 vTextureCoords;\nvoid main(){\n\tgl_FragColor = texture2D(SGTexture, vTextureCoords);\n}\n"

    invoke-direct {v1, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 100
    .local v1, "fragment":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-direct {v3, v2, v1}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;)V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    .line 102
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    sget-object v4, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    sget-object v5, Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;->CLAMP_TO_EDGE:Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;Lcom/samsung/android/sdk/sgi/render/SGTextureWrapType;)V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    .line 104
    new-instance v3, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    .line 105
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;->setDepthTestEnabled(Z)V

    .line 106
    const-string v3, "SGDepthProperty"

    iget-object v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mDepthProperty:Lcom/samsung/android/sdk/sgi/render/SGDepthProperty;

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 108
    iget-object v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-virtual {p0, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V

    .line 109
    return-void
.end method

.method public static perspectiveM([FIFFFF)V
    .locals 8
    .param p0, "m"    # [F
    .param p1, "offset"    # I
    .param p2, "fovy"    # F
    .param p3, "aspect"    # F
    .param p4, "zNear"    # F
    .param p5, "zFar"    # F

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 69
    float-to-double v2, p2

    const-wide v4, 0x3f81df46a2529d39L    # 0.008726646259971648

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v2, v2

    div-float v0, v7, v2

    .line 70
    .local v0, "f":F
    sub-float v2, p4, p5

    div-float v1, v7, v2

    .line 72
    .local v1, "rangeReciprocal":F
    add-int/lit8 v2, p1, 0x0

    div-float v3, v0, p3

    aput v3, p0, v2

    .line 73
    add-int/lit8 v2, p1, 0x1

    aput v6, p0, v2

    .line 74
    add-int/lit8 v2, p1, 0x2

    aput v6, p0, v2

    .line 75
    add-int/lit8 v2, p1, 0x3

    aput v6, p0, v2

    .line 77
    add-int/lit8 v2, p1, 0x4

    aput v6, p0, v2

    .line 78
    add-int/lit8 v2, p1, 0x5

    aput v0, p0, v2

    .line 79
    add-int/lit8 v2, p1, 0x6

    aput v6, p0, v2

    .line 80
    add-int/lit8 v2, p1, 0x7

    aput v6, p0, v2

    .line 82
    add-int/lit8 v2, p1, 0x8

    aput v6, p0, v2

    .line 83
    add-int/lit8 v2, p1, 0x9

    aput v6, p0, v2

    .line 84
    add-int/lit8 v2, p1, 0xa

    add-float v3, p5, p4

    mul-float/2addr v3, v1

    aput v3, p0, v2

    .line 85
    add-int/lit8 v2, p1, 0xb

    const/high16 v3, -0x40800000    # -1.0f

    aput v3, p0, v2

    .line 87
    add-int/lit8 v2, p1, 0xc

    aput v6, p0, v2

    .line 88
    add-int/lit8 v2, p1, 0xd

    aput v6, p0, v2

    .line 89
    add-int/lit8 v2, p1, 0xe

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v3, p5

    mul-float/2addr v3, p4

    mul-float/2addr v3, v1

    aput v3, p0, v2

    .line 90
    add-int/lit8 v2, p1, 0xf

    aput v6, p0, v2

    .line 91
    return-void
.end method


# virtual methods
.method public setDistance([FFFF)V
    .locals 20
    .param p1, "eye"    # [F
    .param p2, "distance"    # F
    .param p3, "hRotation"    # F
    .param p4, "vRotation"    # F

    .prologue
    .line 152
    const/4 v2, 0x0

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 153
    const/4 v2, 0x1

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    aput v3, p1, v2

    .line 154
    const/4 v2, 0x2

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 155
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, p1, v4

    const/4 v5, 0x1

    aget v5, p1, v5

    const/4 v6, 0x2

    aget v6, p1, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 157
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 158
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->projMat:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 159
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 160
    const-string v2, "MyWorldViewProjection"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 162
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 163
    const-string v2, "MyWorldView"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 165
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    const/4 v3, 0x0

    aget v3, p1, v3

    const/4 v4, 0x1

    aget v4, p1, v4

    const/4 v5, 0x2

    aget v5, p1, v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->set(FFF)V

    .line 166
    const-string v2, "MyCamPos"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 167
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 175
    const-string v0, "SGTexture"

    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mTexture:Lcom/samsung/android/sdk/sgi/render/SGBitmapTexture2DProperty;

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 176
    return-void
.end method

.method public setImage(Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    .locals 1
    .param p1, "image"    # Lcom/samsung/android/sdk/sgi/render/SGProperty;

    .prologue
    .line 170
    const-string v0, "SGTexture"

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 171
    return-void
.end method

.method public setRotation([FFFF)V
    .locals 20
    .param p1, "eye"    # [F
    .param p2, "distance"    # F
    .param p3, "hRotation"    # F
    .param p4, "vRotation"    # F

    .prologue
    .line 134
    const/4 v2, 0x0

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 135
    const/4 v2, 0x1

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    aput v3, p1, v2

    .line 136
    const/4 v2, 0x2

    move/from16 v0, p3

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p2

    move/from16 v0, p4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p1, v2

    .line 137
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, p1, v4

    const/4 v5, 0x1

    aget v5, p1, v5

    const/4 v6, 0x2

    aget v6, p1, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 139
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 140
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->projMat:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 141
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 142
    const-string v2, "MyWorldViewProjection"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 144
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 145
    const-string v2, "MyWorldView"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 147
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    const/4 v3, 0x0

    aget v3, p1, v3

    const/4 v4, 0x1

    aget v4, p1, v4

    const/4 v5, 0x2

    aget v5, p1, v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->set(FFF)V

    .line 148
    const-string v2, "MyCamPos"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 149
    return-void
.end method

.method public setSize(IIFFFF[FFFF)V
    .locals 20
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "FOVY"    # F
    .param p4, "ASPECT_RATIO"    # F
    .param p5, "NEAR_PLANE"    # F
    .param p6, "FAR_PLANE"    # F
    .param p7, "eye"    # [F
    .param p8, "distance"    # F
    .param p9, "hRotation"    # F
    .param p10, "vRotation"    # F

    .prologue
    .line 112
    move/from16 v0, p1

    int-to-float v2, v0

    move/from16 v0, p2

    int-to-float v3, v0

    move-object/from16 v0, p0

    invoke-super {v0, v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->projMat:[F

    const/4 v3, 0x0

    const/high16 v7, 0x447a0000    # 1000.0f

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    invoke-static/range {v2 .. v7}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->perspectiveM([FIFFFF)V

    .line 116
    const/4 v2, 0x0

    move/from16 v0, p9

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p8

    move/from16 v0, p10

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p7, v2

    .line 117
    const/4 v2, 0x1

    move/from16 v0, p10

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p8

    aput v3, p7, v2

    .line 118
    const/4 v2, 0x2

    move/from16 v0, p9

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v3, v3, p8

    move/from16 v0, p10

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    aput v3, p7, v2

    .line 119
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, p7, v4

    const/4 v5, 0x1

    aget v5, p7, v5

    const/4 v6, 0x2

    aget v6, p7, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    invoke-static/range {v2 .. v12}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 121
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 122
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->projMat:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 123
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->tempMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 124
    const-string v2, "MyWorldViewProjection"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mMVPMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 126
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v5, 0x1

    aget v4, v4, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v6, 0x2

    aget v5, v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v8, 0x4

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v9, 0x5

    aget v8, v8, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v10, 0x6

    aget v9, v9, v10

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/4 v11, 0x7

    aget v10, v10, v11

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/16 v12, 0x8

    aget v11, v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/16 v13, 0x9

    aget v12, v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/16 v14, 0xa

    aget v13, v13, v14

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/16 v15, 0xb

    aget v14, v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    const/16 v16, 0xc

    aget v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    move-object/from16 v16, v0

    const/16 v17, 0xd

    aget v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    move-object/from16 v17, v0

    const/16 v18, 0xe

    aget v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->viewMat:[F

    move-object/from16 v18, v0

    const/16 v19, 0xf

    aget v18, v18, v19

    invoke-virtual/range {v2 .. v18}, Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;->set(FFFFFFFFFFFFFFFF)V

    .line 127
    const-string v2, "MyWorldView"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mMVMatrix:Lcom/samsung/android/sdk/sgi/render/SGMatrix4fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 129
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    const/4 v3, 0x0

    aget v3, p7, v3

    const/4 v4, 0x1

    aget v4, p7, v4

    const/4 v5, 0x2

    aget v5, p7, v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;->set(FFF)V

    .line 130
    const-string v2, "MyCamPos"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->mCamPos:Lcom/samsung/android/sdk/sgi/render/SGVector3fProperty;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 131
    return-void
.end method
