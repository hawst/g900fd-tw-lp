.class Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;
.super Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;
.source "SGIAtmosphereLayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomGeometryRingBillboardGenerator"
.end annotation


# instance fields
.field private mRadiusInner:F

.field private mRadiusOuter:F

.field private mSectors:I

.field final synthetic this$0:Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;IFF)V
    .locals 1
    .param p2, "sectors"    # I
    .param p3, "radiusInner"    # F
    .param p4, "radiusOuter"    # F

    .prologue
    .line 182
    iput-object p1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;

    .line 183
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;-><init>()V

    .line 184
    const/4 v0, 0x5

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;->mSectors:I

    .line 185
    iput p3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;->mRadiusInner:F

    .line 186
    iput p4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;->mRadiusOuter:F

    .line 187
    return-void
.end method


# virtual methods
.method public generate(FLcom/samsung/android/sdk/sgi/render/SGGeometry;FF)Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .locals 21
    .param p1, "aParam"    # F
    .param p2, "aPreviousGeometry"    # Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .param p3, "aHeight"    # F
    .param p4, "aWidth"    # F

    .prologue
    .line 197
    new-instance v7, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;

    sget-object v17, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->TRIANGLES:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    sget-object v18, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;->mSectors:I

    move/from16 v19, v0

    mul-int/lit8 v19, v19, 0x6

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v7, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;-><init>(Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 198
    .local v7, "indexBuffer":Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;
    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->getShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v8

    .line 199
    .local v8, "indices":Ljava/nio/ShortBuffer;
    invoke-virtual {v8}, Ljava/nio/ShortBuffer;->clear()Ljava/nio/Buffer;

    .line 202
    new-instance v9, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/16 v17, 0x3

    sget-object v18, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v19, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;->mSectors:I

    move/from16 v20, v0

    mul-int/lit8 v20, v20, 0x4

    move/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v9, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 203
    .local v9, "positionBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual {v9}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v10

    .line 204
    .local v10, "positions":Ljava/nio/FloatBuffer;
    invoke-virtual {v10}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 207
    new-instance v13, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/16 v17, 0x2

    sget-object v18, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v19, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;->mSectors:I

    move/from16 v20, v0

    mul-int/lit8 v20, v20, 0x4

    move/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v13, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 208
    .local v13, "texcoordBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual {v13}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v12

    .line 209
    .local v12, "texcoord":Ljava/nio/FloatBuffer;
    invoke-virtual {v12}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 212
    const/4 v11, 0x0

    .local v11, "s":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;->mSectors:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v11, v0, :cond_0

    .line 222
    const v4, 0x40c90fdb

    .line 223
    .local v4, "M_2PI":F
    const/4 v11, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;->mSectors:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-le v11, v0, :cond_1

    .line 244
    new-instance v6, Lcom/samsung/android/sdk/sgi/render/SGGeometry;

    invoke-direct {v6, v7}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;-><init>(Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;)V

    .line 245
    .local v6, "geometry":Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    const-string v17, "SGTextureCoords"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0, v13}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 246
    const-string v17, "SGPositions"

    move-object/from16 v0, v17

    invoke-virtual {v6, v0, v9}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 248
    return-object v6

    .line 213
    .end local v4    # "M_2PI":F
    .end local v6    # "geometry":Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    :cond_0
    mul-int/lit8 v17, v11, 0x2

    add-int/lit8 v17, v17, 0x0

    move/from16 v0, v17

    int-to-short v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 214
    mul-int/lit8 v17, v11, 0x2

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    int-to-short v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 215
    mul-int/lit8 v17, v11, 0x2

    add-int/lit8 v17, v17, 0x3

    move/from16 v0, v17

    int-to-short v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 217
    mul-int/lit8 v17, v11, 0x2

    add-int/lit8 v17, v17, 0x0

    move/from16 v0, v17

    int-to-short v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 218
    mul-int/lit8 v17, v11, 0x2

    add-int/lit8 v17, v17, 0x3

    move/from16 v0, v17

    int-to-short v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 219
    mul-int/lit8 v17, v11, 0x2

    add-int/lit8 v17, v17, 0x2

    move/from16 v0, v17

    int-to-short v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 212
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 224
    .restart local v4    # "M_2PI":F
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;->mSectors:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v11, v0, :cond_2

    const/16 v17, 0x0

    :goto_2
    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;->mSectors:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v5, v17, v18

    .line 226
    .local v5, "fraction":F
    mul-float v17, v4, v5

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v15, v0

    .line 227
    .local v15, "y":F
    mul-float v17, v4, v5

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v14, v0

    .line 228
    .local v14, "x":F
    const/16 v16, 0x0

    .line 230
    .local v16, "z":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;->mRadiusInner:F

    move/from16 v17, v0

    mul-float v17, v17, v14

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 231
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;->mRadiusInner:F

    move/from16 v17, v0

    mul-float v17, v17, v15

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 232
    move/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 233
    invoke-virtual {v12, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 234
    const/high16 v17, 0x3f800000    # 1.0f

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 236
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;->mRadiusOuter:F

    move/from16 v17, v0

    mul-float v17, v17, v14

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 237
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer$CustomGeometryRingBillboardGenerator;->mRadiusOuter:F

    move/from16 v17, v0

    mul-float v17, v17, v15

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 238
    move/from16 v0, v16

    invoke-virtual {v10, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 239
    invoke-virtual {v12, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 240
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 223
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_1

    .end local v5    # "fraction":F
    .end local v14    # "x":F
    .end local v15    # "y":F
    .end local v16    # "z":F
    :cond_2
    move/from16 v17, v11

    .line 224
    goto/16 :goto_2
.end method

.method protected isBelongsToGeometry(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Z
    .locals 1
    .param p1, "aCheckDot"    # Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .prologue
    .line 191
    const/4 v0, 0x0

    return v0
.end method
