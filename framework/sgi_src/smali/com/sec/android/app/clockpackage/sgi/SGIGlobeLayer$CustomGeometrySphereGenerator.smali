.class Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;
.super Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;
.source "SGIGlobeLayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomGeometrySphereGenerator"
.end annotation


# instance fields
.field private mRadius:F

.field private mRings:I

.field private mSectors:I

.field final synthetic this$0:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;IIF)V
    .locals 2
    .param p2, "ring"    # I
    .param p3, "sectors"    # I
    .param p4, "radius"    # F

    .prologue
    const/4 v1, 0x5

    .line 226
    iput-object p1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->this$0:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    .line 227
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGGeometryGenerator;-><init>()V

    .line 228
    invoke-static {p2, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mRings:I

    .line 229
    invoke-static {p3, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mSectors:I

    .line 230
    iput p4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mRadius:F

    .line 231
    return-void
.end method


# virtual methods
.method public generate(FLcom/samsung/android/sdk/sgi/render/SGGeometry;FF)Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .locals 28
    .param p1, "aParam"    # F
    .param p2, "aPreviousGeometry"    # Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .param p3, "aHeight"    # F
    .param p4, "aWidth"    # F

    .prologue
    .line 241
    const/high16 v23, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mRings:I

    move/from16 v24, v0

    add-int/lit8 v24, v24, -0x1

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v6, v23, v24

    .line 242
    .local v6, "R":F
    const/high16 v23, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mSectors:I

    move/from16 v24, v0

    add-int/lit8 v24, v24, -0x1

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    div-float v7, v23, v24

    .line 246
    .local v7, "S":F
    new-instance v10, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;

    sget-object v23, Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;->TRIANGLES:Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;

    sget-object v24, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mRings:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mSectors:I

    move/from16 v26, v0

    mul-int v25, v25, v26

    mul-int/lit8 v25, v25, 0x6

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move/from16 v2, v25

    invoke-direct {v10, v0, v1, v2}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;-><init>(Lcom/samsung/android/sdk/sgi/render/SGPrimitiveType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 247
    .local v10, "indexBuffer":Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;
    invoke-virtual {v10}, Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;->getShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v11

    .line 250
    .local v11, "indices":Ljava/nio/ShortBuffer;
    const/16 v16, 0x0

    .local v16, "r":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mRings:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, -0x1

    move/from16 v0, v16

    move/from16 v1, v23

    if-lt v0, v1, :cond_0

    .line 263
    new-instance v14, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/16 v23, 0x3

    sget-object v24, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v25, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mRings:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mSectors:I

    move/from16 v27, v0

    mul-int v26, v26, v27

    move/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    move/from16 v3, v26

    invoke-direct {v14, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 264
    .local v14, "positionBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual {v14}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v15

    .line 265
    .local v15, "positions":Ljava/nio/FloatBuffer;
    invoke-virtual {v15}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 268
    new-instance v12, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/16 v23, 0x3

    sget-object v24, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v25, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mRings:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mSectors:I

    move/from16 v27, v0

    mul-int v26, v26, v27

    move/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    move/from16 v3, v26

    invoke-direct {v12, v0, v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 269
    .local v12, "normalBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual {v12}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v13

    .line 270
    .local v13, "normals":Ljava/nio/FloatBuffer;
    invoke-virtual {v13}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 273
    new-instance v19, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;

    const/16 v23, 0x2

    sget-object v24, Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;->FLOAT:Lcom/samsung/android/sdk/sgi/render/SGBufferDataType;

    sget-object v25, Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;->STATIC_DRAW:Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mRings:I

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mSectors:I

    move/from16 v27, v0

    mul-int v26, v26, v27

    move-object/from16 v0, v19

    move/from16 v1, v23

    move-object/from16 v2, v24

    move-object/from16 v3, v25

    move/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;-><init>(ILcom/samsung/android/sdk/sgi/render/SGBufferDataType;Lcom/samsung/android/sdk/sgi/render/SGBufferUsageType;I)V

    .line 274
    .local v19, "texcoordBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;->getByteBuffer()Ljava/nio/ByteBuffer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v18

    .line 275
    .local v18, "texcoord":Ljava/nio/FloatBuffer;
    invoke-virtual/range {v18 .. v18}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    .line 277
    const v8, 0x40490fdb    # (float)Math.PI

    .line 279
    .local v8, "fPI":F
    const/16 v16, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mRings:I

    move/from16 v23, v0

    move/from16 v0, v16

    move/from16 v1, v23

    if-lt v0, v1, :cond_2

    .line 299
    new-instance v9, Lcom/samsung/android/sdk/sgi/render/SGGeometry;

    invoke-direct {v9, v10}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;-><init>(Lcom/samsung/android/sdk/sgi/render/SGIndexBuffer;)V

    .line 300
    .local v9, "geometry":Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    const-string v23, "SGTextureCoords"

    move-object/from16 v0, v23

    move-object/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 301
    const-string v23, "SGPositions"

    move-object/from16 v0, v23

    invoke-virtual {v9, v0, v14}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 302
    const-string v23, "SGNormals"

    move-object/from16 v0, v23

    invoke-virtual {v9, v0, v12}, Lcom/samsung/android/sdk/sgi/render/SGGeometry;->addBuffer(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGBuffer;)V

    .line 304
    return-object v9

    .line 251
    .end local v8    # "fPI":F
    .end local v9    # "geometry":Lcom/samsung/android/sdk/sgi/render/SGGeometry;
    .end local v12    # "normalBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .end local v13    # "normals":Ljava/nio/FloatBuffer;
    .end local v14    # "positionBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .end local v15    # "positions":Ljava/nio/FloatBuffer;
    .end local v18    # "texcoord":Ljava/nio/FloatBuffer;
    .end local v19    # "texcoordBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    :cond_0
    const/16 v17, 0x0

    .local v17, "s":I
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mSectors:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, -0x1

    move/from16 v0, v17

    move/from16 v1, v23

    if-lt v0, v1, :cond_1

    .line 250
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_0

    .line 252
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mSectors:I

    move/from16 v23, v0

    mul-int v23, v23, v16

    add-int v23, v23, v17

    move/from16 v0, v23

    int-to-short v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 253
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mSectors:I

    move/from16 v23, v0

    mul-int v23, v23, v16

    add-int/lit8 v24, v17, 0x1

    add-int v23, v23, v24

    move/from16 v0, v23

    int-to-short v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 254
    add-int/lit8 v23, v16, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mSectors:I

    move/from16 v24, v0

    mul-int v23, v23, v24

    add-int v23, v23, v17

    move/from16 v0, v23

    int-to-short v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 256
    add-int/lit8 v23, v16, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mSectors:I

    move/from16 v24, v0

    mul-int v23, v23, v24

    add-int v23, v23, v17

    move/from16 v0, v23

    int-to-short v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 257
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mSectors:I

    move/from16 v23, v0

    mul-int v23, v23, v16

    add-int/lit8 v24, v17, 0x1

    add-int v23, v23, v24

    move/from16 v0, v23

    int-to-short v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 258
    add-int/lit8 v23, v16, 0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mSectors:I

    move/from16 v24, v0

    mul-int v23, v23, v24

    add-int/lit8 v24, v17, 0x1

    add-int v23, v23, v24

    move/from16 v0, v23

    int-to-short v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    .line 251
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_2

    .line 280
    .end local v17    # "s":I
    .restart local v8    # "fPI":F
    .restart local v12    # "normalBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .restart local v13    # "normals":Ljava/nio/FloatBuffer;
    .restart local v14    # "positionBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    .restart local v15    # "positions":Ljava/nio/FloatBuffer;
    .restart local v18    # "texcoord":Ljava/nio/FloatBuffer;
    .restart local v19    # "texcoordBuffer":Lcom/samsung/android/sdk/sgi/render/SGVertexBuffer;
    :cond_2
    const/16 v17, 0x0

    .restart local v17    # "s":I
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mSectors:I

    move/from16 v23, v0

    move/from16 v0, v17

    move/from16 v1, v23

    if-lt v0, v1, :cond_3

    .line 279
    add-int/lit8 v16, v16, 0x1

    goto/16 :goto_1

    .line 281
    :cond_3
    neg-float v0, v8

    move/from16 v23, v0

    const/high16 v24, 0x3f000000    # 0.5f

    mul-float v23, v23, v24

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v24, v24, v8

    mul-float v24, v24, v6

    add-float v23, v23, v24

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sin(D)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-float v0, v0

    move/from16 v21, v0

    .line 282
    .local v21, "y":F
    const/high16 v23, 0x40000000    # 2.0f

    mul-float v23, v23, v8

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v23, v23, v24

    mul-float v23, v23, v7

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->cos(D)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-float v0, v0

    move/from16 v23, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v24, v24, v8

    mul-float v24, v24, v6

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sin(D)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-float v0, v0

    move/from16 v24, v0

    mul-float v20, v23, v24

    .line 283
    .local v20, "x":F
    const/high16 v23, 0x40000000    # 2.0f

    mul-float v23, v23, v8

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v23, v23, v24

    mul-float v23, v23, v7

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sin(D)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-float v0, v0

    move/from16 v23, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v24, v24, v8

    mul-float v24, v24, v6

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sin(D)D

    move-result-wide v24

    move-wide/from16 v0, v24

    double-to-float v0, v0

    move/from16 v24, v0

    mul-float v22, v23, v24

    .line 285
    .local v22, "z":F
    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v23, v23, v7

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 286
    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v23, v23, v6

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 288
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mRadius:F

    move/from16 v23, v0

    mul-float v23, v23, v20

    const/high16 v24, -0x40800000    # -1.0f

    mul-float v23, v23, v24

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 289
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mRadius:F

    move/from16 v23, v0

    mul-float v23, v23, v21

    const/high16 v24, -0x40800000    # -1.0f

    mul-float v23, v23, v24

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 290
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer$CustomGeometrySphereGenerator;->mRadius:F

    move/from16 v23, v0

    mul-float v23, v23, v22

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 292
    move/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 293
    move/from16 v0, v21

    invoke-virtual {v13, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 294
    move/from16 v0, v22

    invoke-virtual {v13, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    .line 280
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_3
.end method

.method protected isBelongsToGeometry(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)Z
    .locals 1
    .param p1, "aCheckDot"    # Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .prologue
    .line 235
    const/4 v0, 0x0

    return v0
.end method
