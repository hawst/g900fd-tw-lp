.class public Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;
.super Ljava/lang/Object;
.source "SGIPlayer.java"


# instance fields
.field ASPECT_RATIO:F

.field final FAR_PLANE:F

.field final FOVY:F

.field final NEAR_PLANE:F

.field distance:F

.field private distanceTransient:F

.field private downTime:J

.field private downX:F

.field private downY:F

.field eye:[F

.field fovToUse:F

.field hRotation:F

.field private hRotationTransient:F

.field height:I

.field private mAnimator:Landroid/animation/ValueAnimator;

.field mAnimatorListener:Landroid/animation/Animator$AnimatorListener;

.field private mAtmosphereLayer:Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;

.field private mBillboardLayer:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

.field private mDayBitmap:Landroid/graphics/Bitmap;

.field private mGlobeLayer:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

.field private mGradientBitmap:Landroid/graphics/Bitmap;

.field private mLatLongLayer:Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;

.field private mRotationAnimator:Landroid/animation/ValueAnimator;

.field private mSgiGlobeActivity:Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;

.field private mTimeZoneBitmap:Landroid/graphics/Bitmap;

.field private mTimeZoneLayer:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

.field private mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

.field smallerDimension:F

.field private startGap:F

.field vRotation:F

.field private vRotationTransient:F

.field width:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;Landroid/view/ViewGroup;)V
    .locals 10
    .param p1, "context"    # Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;
    .param p2, "parentView"    # Landroid/view/ViewGroup;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const v6, 0x3dcccccd    # 0.1f

    iput v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->NEAR_PLANE:F

    const/high16 v6, 0x43fa0000    # 500.0f

    iput v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->FAR_PLANE:F

    .line 34
    const/high16 v6, 0x41600000    # 14.0f

    iput v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->FOVY:F

    .line 37
    const v6, 0x41066666    # 8.4f

    iput v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    .line 38
    const/4 v6, 0x3

    new-array v6, v6, [F

    const/4 v7, 0x0

    const/4 v8, 0x0

    aput v8, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x0

    aput v8, v6, v7

    const/4 v7, 0x2

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    aput v8, v6, v7

    iput-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    .line 44
    const/high16 v6, -0x40800000    # -1.0f

    iput v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->startGap:F

    .line 47
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAnimator:Landroid/animation/ValueAnimator;

    .line 48
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mRotationAnimator:Landroid/animation/ValueAnimator;

    .line 415
    new-instance v6, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$1;-><init>(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)V

    iput-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    .line 58
    iput-object p1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mSgiGlobeActivity:Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;

    .line 59
    invoke-static {}, Lcom/sec/android/app/clockpackage/worldclock/CityManager;->getCitiesByOffset()[Lcom/sec/android/app/clockpackage/worldclock/City;

    move-result-object v1

    .line 60
    .local v1, "cities":[Lcom/sec/android/app/clockpackage/worldclock/City;
    array-length v0, v1

    .line 61
    .local v0, "COUNT":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-lt v3, v0, :cond_0

    .line 89
    new-instance v2, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;

    invoke-direct {v2}, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;-><init>()V

    .line 90
    .local v2, "config":Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;
    const/16 v6, 0x8

    iput v6, v2, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBlueSize:I

    iput v6, v2, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mGreenSize:I

    iput v6, v2, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mRedSize:I

    .line 91
    const/16 v6, 0x8

    iput v6, v2, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mAlphaSize:I

    .line 92
    const/16 v6, 0x8

    iput v6, v2, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mDepthSize:I

    .line 93
    const/4 v6, 0x0

    iput v6, v2, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mStencilSize:I

    .line 94
    const/4 v6, 0x0

    iput v6, v2, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSamples:I

    iput v6, v2, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSampleBuffers:I

    .line 95
    const/4 v6, 0x0

    iput v6, v2, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mBackgroundThreadCount:I

    .line 96
    const/4 v6, 0x1

    iput-boolean v6, v2, Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;->mSeparateThreads:Z

    .line 97
    new-instance v6, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$2;

    invoke-direct {v6, p0, p1, v2}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$2;-><init>(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;Landroid/content/Context;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    iput-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    .line 102
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/sgi/vi/SGView;->setClickable(Z)V

    .line 103
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/sgi/vi/SGView;->setFocusable(Z)V

    .line 110
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/vi/SGView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v6

    new-instance v7, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$3;

    invoke-direct {v7, p0}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$3;-><init>(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)V

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setSizeChangeListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;)V

    .line 117
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/vi/SGView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v6

    new-instance v7, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$4;

    invoke-direct {v7, p0}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$4;-><init>(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)V

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addChangesDrawnListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;)V

    .line 149
    invoke-virtual {p1}, Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02049f

    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mDayBitmap:Landroid/graphics/Bitmap;

    .line 150
    invoke-virtual {p1}, Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020234

    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGradientBitmap:Landroid/graphics/Bitmap;

    .line 151
    invoke-virtual {p1}, Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0202d5

    invoke-static {v6, v7}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneBitmap:Landroid/graphics/Bitmap;

    .line 153
    new-instance v6, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    invoke-direct {v6}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGlobeLayer:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    .line 154
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGlobeLayer:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    const v7, -0xffff01

    invoke-virtual {v6, v7}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setColor(I)V

    .line 155
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGlobeLayer:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mDayBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setImage(Landroid/graphics/Bitmap;)V

    .line 157
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGlobeLayer:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    new-instance v7, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v7, v8, v9}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 158
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/vi/SGView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGlobeLayer:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 160
    new-instance v6, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;

    invoke-direct {v6}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mLatLongLayer:Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;

    .line 161
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mLatLongLayer:Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;

    const v7, -0xff0001

    invoke-virtual {v6, v7}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setColor(I)V

    .line 162
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mLatLongLayer:Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;

    new-instance v7, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v7, v8, v9}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 163
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/vi/SGView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mLatLongLayer:Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 165
    new-instance v6, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;

    invoke-direct {v6}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;-><init>()V

    iput-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAtmosphereLayer:Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;

    .line 166
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAtmosphereLayer:Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;

    const v7, -0xff0100

    invoke-virtual {v6, v7}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setColor(I)V

    .line 167
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAtmosphereLayer:Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;

    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGradientBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setImage(Landroid/graphics/Bitmap;)V

    .line 168
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAtmosphereLayer:Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;

    new-instance v7, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v7, v8, v9}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 169
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/vi/SGView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAtmosphereLayer:Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 171
    new-instance v6, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    const/4 v7, 0x0

    invoke-direct {v6, p1, v7}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;-><init>(Landroid/content/Context;I)V

    iput-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneLayer:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    .line 172
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneLayer:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    const v7, -0xff01

    invoke-virtual {v6, v7}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setColor(I)V

    .line 173
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneLayer:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v7}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setImage(Landroid/graphics/Bitmap;)V

    .line 175
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneLayer:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    new-instance v7, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v7, v8, v9}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 176
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/vi/SGView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneLayer:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 178
    new-instance v6, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    invoke-direct {v6, p1}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;-><init>(Landroid/app/Activity;)V

    iput-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mBillboardLayer:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    .line 179
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mBillboardLayer:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    const/16 v7, -0x100

    invoke-virtual {v6, v7}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setColor(I)V

    .line 180
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mBillboardLayer:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    new-instance v7, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v7, v8, v9}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {v6, v7}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setPosition(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 181
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/vi/SGView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mBillboardLayer:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 183
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/sgi/vi/SGView;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v6

    const/high16 v7, -0x1000000

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setColor(I)V

    .line 185
    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    invoke-virtual {p2, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 186
    return-void

    .line 62
    .end local v2    # "config":Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;
    :cond_0
    aget-object v6, v1, v3

    invoke-virtual {v6}, Lcom/sec/android/app/clockpackage/worldclock/City;->getDBSelected()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 63
    aget-object v6, v1, v3

    iget v6, v6, Lcom/sec/android/app/clockpackage/worldclock/City;->mLongitude:F

    neg-float v6, v6

    const v7, 0x4048f5c3    # 3.14f

    mul-float/2addr v6, v7

    const/high16 v7, 0x43340000    # 180.0f

    div-float v4, v6, v7

    .line 64
    .local v4, "latitude":F
    aget-object v6, v1, v3

    iget v6, v6, Lcom/sec/android/app/clockpackage/worldclock/City;->mLatitude:F

    const v7, 0x4048f5c3    # 3.14f

    mul-float/2addr v6, v7

    const/high16 v7, 0x43340000    # 180.0f

    div-float v5, v6, v7

    .line 65
    .local v5, "longitude":F
    const v6, 0x4048f5c3    # 3.14f

    cmpl-float v6, v4, v6

    if-lez v6, :cond_2

    .line 66
    const v6, 0x40c8f5c3    # 6.28f

    sub-float v6, v4, v6

    iput v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    .line 72
    :goto_1
    iput v5, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    .line 61
    .end local v4    # "latitude":F
    .end local v5    # "longitude":F
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 67
    .restart local v4    # "latitude":F
    .restart local v5    # "longitude":F
    :cond_2
    const v6, -0x3fb70a3d    # -3.14f

    cmpg-float v6, v4, v6

    if-gez v6, :cond_3

    .line 68
    const v6, 0x40c8f5c3    # 6.28f

    add-float/2addr v6, v4

    iput v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    goto :goto_1

    .line 70
    :cond_3
    iput v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    goto :goto_1
.end method

.method static synthetic access$0(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mSgiGlobeActivity:Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;II)V
    .locals 0

    .prologue
    .line 188
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->layout(II)V

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneLayer:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/samsung/android/sdk/sgi/vi/SGView;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mDayBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGradientBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mBillboardLayer:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGlobeLayer:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)F
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F

    return v0
.end method

.method static synthetic access$8(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAtmosphereLayer:Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mLatLongLayer:Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;

    return-object v0
.end method

.method private findZoomLevel(F)I
    .locals 9
    .param p1, "distance"    # F

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 437
    const v3, 0x400ccccd    # 2.2f

    sub-float v3, p1, v3

    const v4, 0x40c66666    # 6.2f

    div-float v1, v3, v4

    .line 438
    .local v1, "normalisedRange":F
    const/high16 v3, 0x40a00000    # 5.0f

    mul-float/2addr v3, v1

    float-to-int v2, v3

    .line 440
    .local v2, "zoomLevel":I
    const/4 v0, -0x1

    .line 441
    .local v0, "actualZoomLevel":I
    if-lt v2, v8, :cond_1

    const/4 v3, 0x5

    if-gt v2, v3, :cond_1

    .line 442
    const/4 v0, 0x0

    .line 452
    :cond_0
    :goto_0
    return v0

    .line 443
    :cond_1
    if-lt v2, v7, :cond_2

    if-ge v2, v8, :cond_2

    .line 444
    const/4 v0, 0x1

    goto :goto_0

    .line 445
    :cond_2
    if-lt v2, v6, :cond_3

    if-ge v2, v7, :cond_3

    .line 446
    const/4 v0, 0x2

    goto :goto_0

    .line 447
    :cond_3
    if-lt v2, v5, :cond_4

    if-ge v2, v6, :cond_4

    .line 448
    const/4 v0, 0x3

    goto :goto_0

    .line 449
    :cond_4
    if-ltz v2, :cond_0

    if-ge v2, v5, :cond_0

    .line 450
    const/4 v0, 0x4

    goto :goto_0
.end method

.method private getDistanceForZoomLevel(I)F
    .locals 1
    .param p1, "zoomLevel"    # I

    .prologue
    .line 235
    const/4 v0, 0x0

    .line 236
    .local v0, "distance":F
    packed-switch p1, :pswitch_data_0

    .line 256
    :goto_0
    return v0

    .line 238
    :pswitch_0
    const v0, 0x41066666    # 8.4f

    .line 239
    goto :goto_0

    .line 241
    :pswitch_1
    const v0, 0x40db3333    # 6.85f

    .line 242
    goto :goto_0

    .line 244
    :pswitch_2
    const v0, 0x40a9999a    # 5.3f

    .line 245
    goto :goto_0

    .line 247
    :pswitch_3
    const/high16 v0, 0x40700000    # 3.75f

    .line 248
    goto :goto_0

    .line 250
    :pswitch_4
    const v0, 0x400ccccd    # 2.2f

    .line 251
    goto :goto_0

    .line 236
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private layout(II)V
    .locals 11
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/high16 v6, 0x43fa0000    # 500.0f

    const v5, 0x3dcccccd    # 0.1f

    .line 189
    iput p1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->width:I

    .line 190
    iput p2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->height:I

    .line 192
    if-ge p1, p2, :cond_0

    move v0, p1

    :goto_0
    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->smallerDimension:F

    .line 194
    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->ASPECT_RATIO:F

    .line 195
    if-le p1, p2, :cond_1

    const/high16 v0, 0x41600000    # 14.0f

    :goto_1
    iput v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->fovToUse:F

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGlobeLayer:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    iget v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->fovToUse:F

    iget v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->ASPECT_RATIO:F

    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v9, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    move v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v10}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setSize(IIFFFF[FFFF)V

    .line 198
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mBillboardLayer:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    iget v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->fovToUse:F

    iget v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->ASPECT_RATIO:F

    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v9, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    move v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v10}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setSize(IIFFFF[FFFF)V

    .line 199
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAtmosphereLayer:Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;

    iget v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->fovToUse:F

    iget v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->ASPECT_RATIO:F

    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v9, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    move v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v10}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setSize(IIFFFF[FFFF)V

    .line 200
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mLatLongLayer:Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;

    iget v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->fovToUse:F

    iget v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->ASPECT_RATIO:F

    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v9, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    move v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v10}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setSize(IIFFFF[FFFF)V

    .line 201
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneLayer:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    iget v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->fovToUse:F

    iget v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->ASPECT_RATIO:F

    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v9, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    move v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v10}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setSize(IIFFFF[FFFF)V

    .line 202
    return-void

    :cond_0
    move v0, p2

    .line 192
    goto :goto_0

    .line 195
    :cond_1
    const/high16 v0, 0x40000000    # 2.0f

    const-wide v2, 0x3fbf42aca0000000L    # 0.12211111932992935

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    iget v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->ASPECT_RATIO:F

    float-to-double v8, v1

    div-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v0, v1

    const/high16 v1, 0x43340000    # 180.0f

    mul-float/2addr v0, v1

    const v1, 0x4048f5c3    # 3.14f

    div-float/2addr v0, v1

    goto :goto_1
.end method


# virtual methods
.method public getSGView()Lcom/samsung/android/sdk/sgi/vi/SGView;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    return-object v0
.end method

.method public moveToCity(Lcom/sec/android/app/clockpackage/worldclock/City;)V
    .locals 11
    .param p1, "city"    # Lcom/sec/android/app/clockpackage/worldclock/City;

    .prologue
    const/high16 v10, 0x43340000    # 180.0f

    const v1, 0x4048f5c3    # 3.14f

    const v9, 0x40c8f5c3    # 6.28f

    .line 460
    iget v0, p1, Lcom/sec/android/app/clockpackage/worldclock/City;->mLongitude:F

    neg-float v0, v0

    mul-float/2addr v0, v1

    div-float v6, v0, v10

    .line 461
    .local v6, "latitude":F
    iget v0, p1, Lcom/sec/android/app/clockpackage/worldclock/City;->mLatitude:F

    mul-float/2addr v0, v1

    div-float v7, v0, v10

    .line 463
    .local v7, "longitude":F
    iget v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    rem-float v2, v0, v9

    .line 464
    .local v2, "hRotationStartVal":F
    iget v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    rem-float v4, v0, v9

    .line 467
    .local v4, "vRotationStartVal":F
    sub-float v0, v6, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 468
    sub-float v3, v6, v9

    .line 474
    .local v3, "hRotationEndVal":F
    :goto_0
    move v5, v7

    .line 476
    .local v5, "vRotationEndVal":F
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v8

    .line 477
    .local v8, "mRotationAnimator":Landroid/animation/ValueAnimator;
    const-wide/16 v0, 0x3e8

    invoke-virtual {v8, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 479
    new-instance v0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$7;-><init>(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;FFFF)V

    invoke-virtual {v8, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 494
    invoke-virtual {v8}, Landroid/animation/ValueAnimator;->start()V

    .line 495
    return-void

    .line 469
    .end local v3    # "hRotationEndVal":F
    .end local v5    # "vRotationEndVal":F
    .end local v8    # "mRotationAnimator":Landroid/animation/ValueAnimator;
    :cond_0
    sub-float v0, v6, v2

    const v1, -0x3fb70a3d    # -3.14f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 470
    add-float v3, v6, v9

    .line 471
    .restart local v3    # "hRotationEndVal":F
    goto :goto_0

    .line 472
    .end local v3    # "hRotationEndVal":F
    :cond_1
    move v3, v6

    .restart local v3    # "hRotationEndVal":F
    goto :goto_0

    .line 476
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public processClickEvent(Landroid/view/MotionEvent;)V
    .locals 12
    .param p1, "evt"    # Landroid/view/MotionEvent;

    .prologue
    .line 409
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 410
    .local v10, "t0":J
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mBillboardLayer:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->fovToUse:F

    iget v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->ASPECT_RATIO:F

    const v5, 0x3dcccccd    # 0.1f

    iget-object v6, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setPickAt(FFFFF[FF)Lcom/sec/android/app/clockpackage/worldclock/City;

    move-result-object v8

    .line 411
    .local v8, "city":Lcom/sec/android/app/clockpackage/worldclock/City;
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "--------pick logic time--->"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v10

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 412
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mSgiGlobeActivity:Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;

    invoke-virtual {v0, v8}, Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;->onCityTouched(Lcom/sec/android/app/clockpackage/worldclock/City;)V

    .line 413
    return-void
.end method

.method public processGestureEvent(FF)V
    .locals 10
    .param p1, "velocityX"    # F
    .param p2, "velocityY"    # F

    .prologue
    const/4 v7, 0x0

    .line 359
    iget v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->startGap:F

    cmpl-float v0, v0, v7

    if-lez v0, :cond_0

    .line 406
    :goto_0
    return-void

    .line 363
    :cond_0
    const/high16 v0, 0x44870000    # 1080.0f

    iget v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->smallerDimension:F

    div-float/2addr v0, v1

    mul-float/2addr p1, v0

    .line 364
    const/high16 v0, 0x44f00000    # 1920.0f

    iget v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->smallerDimension:F

    div-float/2addr v0, v1

    mul-float/2addr p2, v0

    .line 365
    iget v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotationTransient:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    .line 366
    iput v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotationTransient:F

    .line 367
    iget v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    iget v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotationTransient:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    .line 368
    iput v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotationTransient:F

    .line 370
    iget v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    invoke-direct {p0, v0}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->findZoomLevel(F)I

    move-result v0

    mul-int/lit16 v0, v0, 0xea6

    add-int/lit16 v0, v0, 0x3a98

    int-to-float v6, v0

    .line 372
    .local v6, "slowDownFactor":F
    iget v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    .line 373
    .local v2, "fromValueH":F
    div-float v0, p1, v6

    iget v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    mul-float/2addr v0, v1

    add-float v3, v2, v0

    .line 374
    .local v3, "toValueH":F
    iget v4, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    .line 375
    .local v4, "fromValueV":F
    div-float v0, p2, v6

    iget v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    mul-float/2addr v0, v1

    add-float v5, v4, v0

    .line 377
    .local v5, "toValueV":F
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mRotationAnimator:Landroid/animation/ValueAnimator;

    .line 378
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mRotationAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v8, 0x2ee

    invoke-virtual {v0, v8, v9}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 379
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mRotationAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 381
    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mRotationAnimator:Landroid/animation/ValueAnimator;

    new-instance v0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$6;-><init>(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;FFFF)V

    invoke-virtual {v7, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mRotationAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mRotationAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAnimatorListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 377
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public processTouch(Landroid/view/MotionEvent;)V
    .locals 14
    .param p1, "evt"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v13, 0x3f800000    # 1.0f

    const v12, 0x41066666    # 8.4f

    const v11, 0x400ccccd    # 2.2f

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 261
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    if-le v7, v9, :cond_6

    .line 263
    const/4 v7, 0x0

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getX(I)F

    move-result v8

    sub-float/2addr v7, v8

    const/high16 v8, 0x44870000    # 1080.0f

    mul-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->smallerDimension:F

    div-float v4, v7, v8

    .line 264
    .local v4, "xDelta":F
    const/4 v7, 0x0

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getY(I)F

    move-result v7

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getY(I)F

    move-result v8

    sub-float/2addr v7, v8

    const/high16 v8, 0x44f00000    # 1920.0f

    mul-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->smallerDimension:F

    div-float v5, v7, v8

    .line 265
    .local v5, "yDelta":F
    mul-float v7, v4, v4

    mul-float v8, v5, v5

    add-float/2addr v7, v8

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v2, v8

    .line 266
    .local v2, "gap":F
    iget v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->startGap:F

    cmpg-float v7, v7, v10

    if-gez v7, :cond_0

    .line 267
    iput v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->startGap:F

    .line 271
    :cond_0
    iget v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->startGap:F

    cmpl-float v7, v2, v7

    if-lez v7, :cond_4

    .line 272
    iget v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->startGap:F

    div-float v7, v2, v7

    sub-float/2addr v7, v13

    iput v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F

    .line 278
    :goto_0
    iget v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F

    sub-float/2addr v7, v8

    cmpl-float v7, v7, v12

    if-lez v7, :cond_5

    .line 279
    iput v12, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    .line 280
    iput v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F

    .line 281
    iput v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->startGap:F

    .line 289
    :cond_1
    :goto_1
    iget v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F

    sub-float/2addr v7, v8

    invoke-direct {p0, v7}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->findZoomLevel(F)I

    move-result v6

    .line 290
    .local v6, "zoomLevel":I
    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    .line 291
    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mSgiGlobeActivity:Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;

    invoke-virtual {v7, v6}, Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;->ImagesetZoomBar(I)V

    .line 294
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGlobeLayer:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    iget-object v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v9, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F

    sub-float/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v11, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setDistance([FFFF)V

    .line 295
    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mBillboardLayer:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    iget-object v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v9, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F

    sub-float/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v11, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setDistance([FFFF)V

    .line 296
    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAtmosphereLayer:Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;

    iget-object v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v9, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F

    sub-float/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v11, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setDistance([FFFF)V

    .line 297
    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mLatLongLayer:Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;

    iget-object v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v9, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F

    sub-float/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v11, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setDistance([FFFF)V

    .line 298
    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneLayer:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    iget-object v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v9, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F

    sub-float/2addr v9, v10

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v11, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    invoke-virtual {v7, v8, v9, v10, v11}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setDistance([FFFF)V

    .line 299
    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/vi/SGView;->invalidate()V

    .line 352
    .end local v2    # "gap":F
    .end local v4    # "xDelta":F
    .end local v5    # "yDelta":F
    .end local v6    # "zoomLevel":I
    :cond_3
    :goto_2
    return-void

    .line 274
    .restart local v2    # "gap":F
    .restart local v4    # "xDelta":F
    .restart local v5    # "yDelta":F
    :cond_4
    iget v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->startGap:F

    div-float/2addr v7, v2

    sub-float/2addr v7, v13

    neg-float v7, v7

    iput v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F

    goto :goto_0

    .line 282
    :cond_5
    iget v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F

    sub-float/2addr v7, v8

    cmpg-float v7, v7, v11

    if-gez v7, :cond_1

    .line 283
    iput v11, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    .line 284
    iput v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F

    .line 285
    iput v2, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->startGap:F

    goto :goto_1

    .line 303
    .end local v2    # "gap":F
    .end local v4    # "xDelta":F
    .end local v5    # "yDelta":F
    :cond_6
    iget v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->startGap:F

    cmpg-float v7, v7, v10

    if-gez v7, :cond_b

    .line 305
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    if-nez v7, :cond_7

    .line 306
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    iput v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->downX:F

    .line 307
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iput v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->downY:F

    .line 308
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->downTime:J

    goto :goto_2

    .line 309
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_a

    .line 310
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->downX:F

    sub-float/2addr v7, v8

    const/high16 v8, 0x44870000    # 1080.0f

    mul-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->smallerDimension:F

    div-float v0, v7, v8

    .line 311
    .local v0, "dragX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->downY:F

    sub-float/2addr v7, v8

    const/high16 v8, 0x44f00000    # 1920.0f

    mul-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->smallerDimension:F

    div-float v1, v7, v8

    .line 313
    .local v1, "dragY":F
    const v7, 0x459c4000    # 5000.0f

    div-float v7, v0, v7

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    mul-float/2addr v7, v8

    iput v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotationTransient:F

    .line 314
    const v7, 0x459c4000    # 5000.0f

    div-float v7, v1, v7

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    mul-float/2addr v7, v8

    iput v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotationTransient:F

    .line 316
    iget v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotationTransient:F

    add-float v3, v7, v8

    .line 317
    .local v3, "temp":F
    const v7, 0x3f8ccccd    # 1.1f

    cmpl-float v7, v3, v7

    if-lez v7, :cond_9

    .line 318
    const v3, 0x3f8ccccd    # 1.1f

    .line 319
    iput v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    .line 320
    iput v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotationTransient:F

    .line 321
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iput v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->downY:F

    .line 329
    :cond_8
    :goto_3
    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGlobeLayer:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    iget-object v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v9, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v11, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotationTransient:F

    add-float/2addr v10, v11

    invoke-virtual {v7, v8, v9, v10, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->setRotation([FFFF)V

    .line 330
    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mBillboardLayer:Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;

    iget-object v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v9, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v11, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotationTransient:F

    add-float/2addr v10, v11

    invoke-virtual {v7, v8, v9, v10, v3}, Lcom/sec/android/app/clockpackage/sgi/SGICitiesLayer;->setRotation([FFFF)V

    .line 331
    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAtmosphereLayer:Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;

    iget-object v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v9, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v11, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotationTransient:F

    add-float/2addr v10, v11

    invoke-virtual {v7, v8, v9, v10, v3}, Lcom/sec/android/app/clockpackage/sgi/SGIAtmosphereLayer;->setRotation([FFFF)V

    .line 332
    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mLatLongLayer:Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;

    iget-object v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v9, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v11, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotationTransient:F

    add-float/2addr v10, v11

    invoke-virtual {v7, v8, v9, v10, v3}, Lcom/sec/android/app/clockpackage/sgi/SGILatLongLayer;->setRotation([FFFF)V

    .line 333
    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneLayer:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    iget-object v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->eye:[F

    iget v9, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v11, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotationTransient:F

    add-float/2addr v10, v11

    invoke-virtual {v7, v8, v9, v10, v3}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setRotation([FFFF)V

    .line 334
    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/vi/SGView;->invalidate()V

    goto/16 :goto_2

    .line 322
    :cond_9
    const v7, -0x40733333    # -1.1f

    cmpg-float v7, v3, v7

    if-gez v7, :cond_8

    .line 323
    const v3, -0x40733333    # -1.1f

    .line 324
    iput v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    .line 325
    iput v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotationTransient:F

    .line 326
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iput v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->downY:F

    goto :goto_3

    .line 335
    .end local v0    # "dragX":F
    .end local v1    # "dragY":F
    .end local v3    # "temp":F
    :cond_a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    if-ne v7, v9, :cond_3

    .line 336
    iget v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotationTransient:F

    add-float/2addr v7, v8

    const v8, 0x40c8f5c3    # 6.28f

    rem-float/2addr v7, v8

    iput v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    .line 337
    iput v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotationTransient:F

    .line 338
    iget v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotationTransient:F

    add-float/2addr v7, v8

    iput v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotation:F

    .line 339
    iput v10, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->vRotationTransient:F

    .line 341
    iget-object v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mSgiGlobeActivity:Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->hRotation:F

    invoke-virtual {v7, v8}, Lcom/sec/android/app/clockpackage/worldclock/activity/MainActivity;->setTimezoneSwitcher(F)V

    goto/16 :goto_2

    .line 343
    :cond_b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    if-ne v7, v9, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    if-ne v7, v9, :cond_3

    .line 344
    const/high16 v7, -0x40800000    # -1.0f

    iput v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->startGap:F

    .line 345
    iget v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    iget v8, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F

    sub-float/2addr v7, v8

    iput v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    .line 346
    iget v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    cmpl-float v7, v7, v12

    if-lez v7, :cond_c

    .line 347
    iput v12, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    goto/16 :goto_2

    .line 348
    :cond_c
    iget v7, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    cmpg-float v7, v7, v11

    if-gez v7, :cond_3

    .line 349
    iput v11, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    goto/16 :goto_2
.end method

.method public showTimeZone(I)V
    .locals 1
    .param p1, "timezoneIndex"    # I

    .prologue
    .line 456
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mTimeZoneLayer:Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/clockpackage/sgi/SGITimeZoneLayer;->setTimeZoneIndex(I)V

    .line 457
    return-void
.end method

.method public stopRotationAnimation()V
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mRotationAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mRotationAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mRotationAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 434
    :cond_0
    return-void
.end method

.method public updateGlobeZoomLevel(I)V
    .locals 4
    .param p1, "zoomLevel"    # I

    .prologue
    .line 206
    invoke-direct {p0, p1}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->getDistanceForZoomLevel(I)F

    move-result v1

    iput v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    .line 207
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGlobeLayer:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    invoke-virtual {v1}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->getCurrentDistance()F

    move-result v0

    .line 208
    .local v0, "prevDistance":F
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distanceTransient:F

    .line 210
    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput v0, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->distance:F

    aput v3, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAnimator:Landroid/animation/ValueAnimator;

    .line 211
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x2ee

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 213
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer$5;-><init>(Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 230
    iget-object v1, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 231
    return-void
.end method

.method public updateNightImage()V
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mGlobeLayer:Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;

    invoke-virtual {v0}, Lcom/sec/android/app/clockpackage/sgi/SGIGlobeLayer;->updateNightImage()V

    .line 500
    iget-object v0, p0, Lcom/sec/android/app/clockpackage/sgi/SGIPlayer;->mView:Lcom/samsung/android/sdk/sgi/vi/SGView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/sgi/vi/SGView;->invalidate()V

    .line 502
    :cond_0
    return-void
.end method
