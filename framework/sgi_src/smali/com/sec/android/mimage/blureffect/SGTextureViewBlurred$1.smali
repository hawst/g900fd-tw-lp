.class Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$1;
.super Ljava/lang/Object;
.source "SGTextureViewBlurred.java"

# interfaces
.implements Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;Lcom/sec/android/mimage/blureffect/SGListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$1;->this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelled(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    .prologue
    .line 70
    const-string v0, "animation cancel"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public onDiscarded(I)V
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 65
    const-string v0, "animation discard"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method public onFinished(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    .prologue
    .line 58
    const-string v0, "animation finish"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$1;->this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    # getter for: Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;
    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->access$0(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Lcom/sec/android/mimage/blureffect/FakeLayer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/blureffect/FakeLayer;->setAnimation(Z)V

    .line 61
    return-void
.end method

.method public onRepeated(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    .prologue
    .line 53
    const-string v0, "animation repeat"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method public onStarted(ILcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;)V
    .locals 2
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Lcom/samsung/android/sdk/sgi/animation/SGVisualValueProvider;

    .prologue
    .line 46
    const-string v0, "animation start"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$1;->this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    # getter for: Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;
    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->access$0(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Lcom/sec/android/mimage/blureffect/FakeLayer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/blureffect/FakeLayer;->setAnimation(Z)V

    .line 48
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$1;->this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    # getter for: Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSGListener:Lcom/sec/android/mimage/blureffect/SGListener;
    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->access$1(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Lcom/sec/android/mimage/blureffect/SGListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/mimage/blureffect/SGListener;->onStartBlurAnimation()V

    .line 49
    return-void
.end method
