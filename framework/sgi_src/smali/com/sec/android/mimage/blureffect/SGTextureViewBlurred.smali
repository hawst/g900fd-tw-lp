.class public Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;
.super Lcom/samsung/android/sdk/sgi/vi/SGTextureView;
.source "SGTextureViewBlurred.java"

# interfaces
.implements Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;
.implements Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;
.implements Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;


# instance fields
.field private mAnimationListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mDuration:I

.field private mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

.field private mFrameCount:I

.field private mHandler:Landroid/os/Handler;

.field private mIsChangedBitmap:Z

.field private mIsNeedsUpdateBlurr:Z

.field private mIsPaused:Z

.field private mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

.field private mRunnable:Ljava/lang/Runnable;

.field private mSGListener:Lcom/sec/android/mimage/blureffect/SGListener;

.field private mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;Lcom/sec/android/mimage/blureffect/SGListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "configuration"    # Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;
    .param p3, "sgListener"    # Lcom/sec/android/mimage/blureffect/SGListener;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 40
    invoke-direct {p0, p1, v1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGTextureView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/render/SGRenderDataProvider;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;)V

    .line 18
    iput-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    .line 19
    iput-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    .line 20
    iput-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    .line 24
    iput-boolean v2, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsPaused:Z

    .line 26
    iput-boolean v2, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsNeedsUpdateBlurr:Z

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsChangedBitmap:Z

    .line 29
    iput-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSGListener:Lcom/sec/android/mimage/blureffect/SGListener;

    .line 30
    iput-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mAnimationListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    .line 31
    iput-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

    .line 33
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mDuration:I

    .line 34
    iput v2, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFrameCount:I

    .line 36
    iput-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mHandler:Landroid/os/Handler;

    .line 37
    iput-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mRunnable:Ljava/lang/Runnable;

    .line 42
    iput-object p3, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSGListener:Lcom/sec/android/mimage/blureffect/SGListener;

    .line 43
    new-instance v0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$1;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$1;-><init>(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)V

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mAnimationListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    .line 74
    new-instance v0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$2;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$2;-><init>(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)V

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

    .line 90
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mHandler:Landroid/os/Handler;

    .line 91
    new-instance v0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$3;

    invoke-direct {v0, p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$3;-><init>(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)V

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mRunnable:Ljava/lang/Runnable;

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->startAnimationOnResume()V

    .line 102
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Lcom/sec/android/mimage/blureffect/FakeLayer;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Lcom/sec/android/mimage/blureffect/SGListener;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSGListener:Lcom/sec/android/mimage/blureffect/SGListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsChangedBitmap:Z

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;Z)V
    .locals 0

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsChangedBitmap:Z

    return-void
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Lcom/sec/android/mimage/blureffect/GaussianLayer;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mDuration:I

    return v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mAnimationListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    return-object v0
.end method


# virtual methods
.method public hideViews()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 201
    const-string v0, "hideViews"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->setVisibility(Z)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/blureffect/FakeLayer;->setVisibility(Z)V

    .line 207
    :cond_0
    return-void
.end method

.method public onChangesDrawn()V
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsNeedsUpdateBlurr:Z

    if-eqz v0, :cond_0

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsNeedsUpdateBlurr:Z

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSGListener:Lcom/sec/android/mimage/blureffect/SGListener;

    if-eqz v0, :cond_2

    .line 173
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSGListener:Lcom/sec/android/mimage/blureffect/SGListener;

    invoke-interface {v0}, Lcom/sec/android/mimage/blureffect/SGListener;->onDrawFinished()V

    .line 176
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->invalidate()V

    .line 177
    return-void
.end method

.method public onFrameEnd()V
    .locals 2

    .prologue
    .line 181
    iget v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFrameCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFrameCount:I

    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Frame Count "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFrameCount:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 183
    iget v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFrameCount:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 184
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFrameCount:I

    .line 185
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setDrawFrameListener(Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;)V

    .line 186
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSGListener:Lcom/sec/android/mimage/blureffect/SGListener;

    invoke-interface {v0}, Lcom/sec/android/mimage/blureffect/SGListener;->onFrameEnd()V

    .line 188
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 210
    iput-boolean v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsPaused:Z

    .line 211
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    invoke-virtual {v0, v1}, Lcom/sec/android/mimage/blureffect/FakeLayer;->setVisibility(Z)V

    .line 214
    :cond_0
    return-void
.end method

.method public onResize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 3
    .param p1, "size"    # Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .prologue
    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsNeedsUpdateBlurr:Z

    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "----- "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->setSize(FF)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/blureffect/FakeLayer;->setSize(FF)V

    .line 152
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsPaused:Z

    if-eqz v0, :cond_1

    .line 153
    const-string v0, "mIsPaused is true"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsPaused:Z

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->startBlur()V

    .line 159
    :goto_0
    return-void

    .line 157
    :cond_1
    const-string v0, "mIsPaused is false"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setDuration(I)V
    .locals 1
    .param p1, "duration"    # I

    .prologue
    .line 217
    if-gtz p1, :cond_0

    .line 218
    const-string v0, "Duration is invalid value!"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 223
    :goto_0
    return-void

    .line 222
    :cond_0
    iput p1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mDuration:I

    goto :goto_0
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 105
    const-string v0, "setImageBitmap"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Lcom/sec/android/mimage/blureffect/GaussianLayer;

    const/high16 v1, 0x41700000    # 15.0f

    invoke-direct {v0, v1}, Lcom/sec/android/mimage/blureffect/GaussianLayer;-><init>(F)V

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addChangesDrawnListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;)V

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setSizeChangeListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceSizeChangeListener;)V

    .line 112
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 114
    new-instance v0, Lcom/sec/android/mimage/blureffect/FakeLayer;

    invoke-direct {v0}, Lcom/sec/android/mimage/blureffect/FakeLayer;-><init>()V

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    .line 115
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 118
    :cond_0
    if-eqz p1, :cond_3

    .line 119
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 120
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsChangedBitmap:Z

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    .line 125
    :cond_1
    iput-object p1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mBitmap:Landroid/graphics/Bitmap;

    .line 126
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mLayer:Lcom/sec/android/mimage/blureffect/GaussianLayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 128
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    if-eqz v0, :cond_2

    .line 129
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    invoke-virtual {v0, p1}, Lcom/sec/android/mimage/blureffect/FakeLayer;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 132
    :cond_2
    iget-boolean v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mIsChangedBitmap:Z

    if-eqz v0, :cond_3

    .line 136
    const-string v0, "before SGSurfaceStateListener"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mSurfaceStateListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;

    invoke-virtual {p0, v0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->setSurfaceStateListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceStateListener;)V

    .line 140
    :cond_3
    return-void
.end method

.method public startAnimationOnResume()V
    .locals 4

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->setDrawFrameListener(Lcom/samsung/android/sdk/sgi/vi/SGDrawFrameListener;)V

    .line 228
    const-string v0, "Runnable postAtTime"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 230
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 231
    return-void
.end method

.method public startBlur()V
    .locals 3

    .prologue
    .line 191
    const-string v0, "startBlur"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    if-eqz v0, :cond_0

    .line 193
    const-string v0, "mFakeLayer is not null"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;

    iget v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mDuration:I

    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mAnimationListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/blureffect/FakeLayer;->startBlur(ILcom/samsung/android/sdk/sgi/animation/SGAnimationListener;)V

    .line 198
    :goto_0
    return-void

    .line 196
    :cond_0
    const-string v0, "mFakeLayer is null"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    goto :goto_0
.end method
