.class Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$3;
.super Ljava/lang/Object;
.source "SGTextureViewBlurred.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/sgi/vi/SGContextConfiguration;Lcom/sec/android/mimage/blureffect/SGListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;


# direct methods
.method constructor <init>(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$3;->this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$3;->this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    # getter for: Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;
    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->access$0(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Lcom/sec/android/mimage/blureffect/FakeLayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/mimage/blureffect/FakeLayer;->getAnimation()Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    const-string v0, "Runnable Animation Start"

    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$3;->this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    # getter for: Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mFakeLayer:Lcom/sec/android/mimage/blureffect/FakeLayer;
    invoke-static {v0}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->access$0(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Lcom/sec/android/mimage/blureffect/FakeLayer;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$3;->this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    # getter for: Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mDuration:I
    invoke-static {v1}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->access$5(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred$3;->this$0:Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;

    # getter for: Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->mAnimationListener:Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;
    invoke-static {v2}, Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;->access$6(Lcom/sec/android/mimage/blureffect/SGTextureViewBlurred;)Lcom/samsung/android/sdk/sgi/animation/SGAnimationListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/mimage/blureffect/FakeLayer;->startBlur(ILcom/samsung/android/sdk/sgi/animation/SGAnimationListener;)V

    .line 98
    :cond_0
    return-void
.end method
