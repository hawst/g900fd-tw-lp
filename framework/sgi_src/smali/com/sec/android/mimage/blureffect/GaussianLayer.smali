.class public Lcom/sec/android/mimage/blureffect/GaussianLayer;
.super Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
.source "GaussianLayer.java"


# static fields
.field static final FRAGMENT_SHADER:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D SGTexture;\nvarying vec2 vTexCoord;\nvarying vec2 vNeighborTexCoord0;\nvarying vec2 vNeighborTexCoord1;\nvarying vec2 vNeighborTexCoord2;\nvarying vec2 vNeighborTexCoord3;\nvarying vec2 vNeighborTexCoord4;\nvarying vec2 vNeighborTexCoord5;\nvarying vec2 vNeighborTexCoord6;\nvarying vec2 vNeighborTexCoord7;\nvarying vec2 vNeighborTexCoord8;\nvarying vec2 vNeighborTexCoord9;\nvarying vec2 vNeighborTexCoord10;\nvarying vec2 vNeighborTexCoord11;\nvoid main()\n{\n     gl_FragColor = vec4(0.0);\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord0)*0.00895781211794;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord1)*0.0215963866053;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord2)*0.0443683338718;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord3)*0.0776744219933;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord4)*0.115876621105;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord5)*0.147308056121;\n     gl_FragColor += texture2D(SGTexture, vTexCoord            )*0.159576912161;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord6)*0.147308056121;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord7)*0.115876621105;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord8)*0.0776744219933;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord9)*0.0443683338718;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord10)*0.0215963866053;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord11)*0.00895781211794;\n}\n"

.field static final FRAGMENT_SHADER_BLEND:Ljava/lang/String; = "precision mediump float;\nvarying vec2 vTexCoords;\n\nuniform sampler2D SGTexture;\nuniform sampler2D textureBlured;\n\nuniform float uBlend;\n\nvoid main(){\n   vec4 bluredColor = texture2D(textureBlured, vTexCoords);\n   vec4 color = texture2D(SGTexture, vTexCoords);\n   gl_FragColor = bluredColor * uBlend + color * (1.0-uBlend);\n}"

.field static final VERTEX_SHADER_BLEND:Ljava/lang/String; = "attribute vec3 SGPositions;\nattribute vec2 SGTextureCoords;\n\nvarying vec2 vTexCoords;\n\nuniform mat4 SGWorldViewProjection;\nuniform vec4 SGTextureRect;\n\nvoid main()\n{\n    vTexCoords = SGTextureRect.xy + SGTextureCoords * (SGTextureRect.zw - SGTextureRect.xy);\n    gl_Position = SGWorldViewProjection * vec4(SGPositions, 1.0);\n}\n"

.field static final VERTEX_SHADER_H:Ljava/lang/String; = "precision mediump float;\nattribute highp vec3 SGPositions;\nattribute vec2 SGTextureCoords;\nvarying vec2 vTexCoord;\nvarying vec2 vNeighborTexCoord0;\nvarying vec2 vNeighborTexCoord1;\nvarying vec2 vNeighborTexCoord2;\nvarying vec2 vNeighborTexCoord3;\nvarying vec2 vNeighborTexCoord4;\nvarying vec2 vNeighborTexCoord5;\nvarying vec2 vNeighborTexCoord6;\nvarying vec2 vNeighborTexCoord7;\nvarying vec2 vNeighborTexCoord8;\nvarying vec2 vNeighborTexCoord9;\nvarying vec2 vNeighborTexCoord10;\nvarying vec2 vNeighborTexCoord11;\nuniform mat4 SGWorldViewProjection;\nuniform vec4 SGTextureRect;\nuniform float radius;\nvoid main() {\n    vTexCoord = SGTextureRect.xy + SGTextureCoords * (SGTextureRect.zw - SGTextureRect.xy);\n    gl_Position = SGWorldViewProjection * vec4(SGPositions, 1.0);\n    vNeighborTexCoord0 = vTexCoord + vec2(-6.0*radius, 0.0);\n    vNeighborTexCoord1 = vTexCoord + vec2(-5.0*radius, 0.0);\n    vNeighborTexCoord2 = vTexCoord + vec2(-4.0*radius, 0.0);\n    vNeighborTexCoord3 = vTexCoord + vec2(-3.0*radius, 0.0);\n    vNeighborTexCoord4 = vTexCoord + vec2(-2.0*radius, 0.0);\n    vNeighborTexCoord5 = vTexCoord + vec2(-1.0*radius, 0.0);\n    vNeighborTexCoord6 = vTexCoord + vec2(1.0*radius, 0.0);\n    vNeighborTexCoord7 = vTexCoord + vec2(2.0*radius, 0.0);\n    vNeighborTexCoord8 = vTexCoord + vec2(3.0*radius, 0.0);\n    vNeighborTexCoord9 = vTexCoord + vec2(4.0*radius, 0.0);\n    vNeighborTexCoord10 = vTexCoord + vec2(5.0*radius, 0.0);\n    vNeighborTexCoord11 = vTexCoord + vec2(6.0*radius, 0.0);\n}"

.field static final VERTEX_SHADER_V:Ljava/lang/String; = "precision mediump float;\nattribute highp vec3 SGPositions;\nattribute vec2 SGTextureCoords;\nvarying vec2 vTexCoord;\nvarying vec2 vNeighborTexCoord0;\nvarying vec2 vNeighborTexCoord1;\nvarying vec2 vNeighborTexCoord2;\nvarying vec2 vNeighborTexCoord3;\nvarying vec2 vNeighborTexCoord4;\nvarying vec2 vNeighborTexCoord5;\nvarying vec2 vNeighborTexCoord6;\nvarying vec2 vNeighborTexCoord7;\nvarying vec2 vNeighborTexCoord8;\nvarying vec2 vNeighborTexCoord9;\nvarying vec2 vNeighborTexCoord10;\nvarying vec2 vNeighborTexCoord11;\nuniform mat4 SGWorldViewProjection;\nuniform vec4 SGTextureRect;\nuniform float radius;\nvoid main() {\n    vTexCoord = SGTextureRect.xy + SGTextureCoords * (SGTextureRect.zw - SGTextureRect.xy);\n    gl_Position = SGWorldViewProjection * vec4(SGPositions, 1.0);\n    vNeighborTexCoord0 = vTexCoord + vec2(0.0, -6.0*radius);\n    vNeighborTexCoord1 = vTexCoord + vec2(0.0, -5.0*radius);\n    vNeighborTexCoord2 = vTexCoord + vec2(0.0, -4.0*radius);\n    vNeighborTexCoord3 = vTexCoord + vec2(0.0, -3.0*radius);\n    vNeighborTexCoord4 = vTexCoord + vec2(0.0, -2.0*radius);\n    vNeighborTexCoord5 = vTexCoord + vec2(0.0, -1.0*radius);\n    vNeighborTexCoord6 = vTexCoord + vec2(0.0,  1.0*radius);\n    vNeighborTexCoord7 = vTexCoord + vec2(0.0,  2.0*radius);\n    vNeighborTexCoord8 = vTexCoord + vec2(0.0,  3.0*radius);\n    vNeighborTexCoord9 = vTexCoord + vec2(0.0,  4.0*radius);\n    vNeighborTexCoord10 = vTexCoord + vec2(0.0,  5.0*radius);\n    vNeighborTexCoord11 = vTexCoord + vec2(0.0,  6.0*radius);\n}"


# instance fields
.field private mBitmapHeight:I

.field private mBitmapWidth:I

.field private mBlendProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

.field private mBlendProperty:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

.field private mContentRect:Landroid/graphics/RectF;

.field private mHProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

.field private mHeight:F

.field protected mInputFBOA:Lcom/samsung/android/sdk/sgi/render/SGProperty;

.field private mIsBlured:Z

.field private mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

.field private mRadius:F

.field private mScreen_shot_x_blured:Lcom/samsung/android/sdk/sgi/render/SGProperty;

.field private mScreen_shot_y_blured:Lcom/samsung/android/sdk/sgi/render/SGProperty;

.field private mVProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

.field private mWidth:F

.field test:Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;


# direct methods
.method public constructor <init>(F)V
    .locals 5
    .param p1, "radius"    # F

    .prologue
    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 181
    invoke-direct {p0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    .line 170
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mInputFBOA:Lcom/samsung/android/sdk/sgi/render/SGProperty;

    .line 178
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mIsBlured:Z

    .line 179
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v3, v3, v4, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mContentRect:Landroid/graphics/RectF;

    .line 320
    new-instance v2, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;

    invoke-direct {v2, p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer$1;-><init>(Lcom/sec/android/mimage/blureffect/GaussianLayer;)V

    iput-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->test:Lcom/samsung/android/sdk/sgi/vi/SGPropertyScreenshotListener;

    .line 182
    iput p1, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mRadius:F

    .line 185
    new-instance v1, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    .line 186
    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->VERTEX:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v3, "precision mediump float;\nattribute highp vec3 SGPositions;\nattribute vec2 SGTextureCoords;\nvarying vec2 vTexCoord;\nvarying vec2 vNeighborTexCoord0;\nvarying vec2 vNeighborTexCoord1;\nvarying vec2 vNeighborTexCoord2;\nvarying vec2 vNeighborTexCoord3;\nvarying vec2 vNeighborTexCoord4;\nvarying vec2 vNeighborTexCoord5;\nvarying vec2 vNeighborTexCoord6;\nvarying vec2 vNeighborTexCoord7;\nvarying vec2 vNeighborTexCoord8;\nvarying vec2 vNeighborTexCoord9;\nvarying vec2 vNeighborTexCoord10;\nvarying vec2 vNeighborTexCoord11;\nuniform mat4 SGWorldViewProjection;\nuniform vec4 SGTextureRect;\nuniform float radius;\nvoid main() {\n    vTexCoord = SGTextureRect.xy + SGTextureCoords * (SGTextureRect.zw - SGTextureRect.xy);\n    gl_Position = SGWorldViewProjection * vec4(SGPositions, 1.0);\n    vNeighborTexCoord0 = vTexCoord + vec2(-6.0*radius, 0.0);\n    vNeighborTexCoord1 = vTexCoord + vec2(-5.0*radius, 0.0);\n    vNeighborTexCoord2 = vTexCoord + vec2(-4.0*radius, 0.0);\n    vNeighborTexCoord3 = vTexCoord + vec2(-3.0*radius, 0.0);\n    vNeighborTexCoord4 = vTexCoord + vec2(-2.0*radius, 0.0);\n    vNeighborTexCoord5 = vTexCoord + vec2(-1.0*radius, 0.0);\n    vNeighborTexCoord6 = vTexCoord + vec2(1.0*radius, 0.0);\n    vNeighborTexCoord7 = vTexCoord + vec2(2.0*radius, 0.0);\n    vNeighborTexCoord8 = vTexCoord + vec2(3.0*radius, 0.0);\n    vNeighborTexCoord9 = vTexCoord + vec2(4.0*radius, 0.0);\n    vNeighborTexCoord10 = vTexCoord + vec2(5.0*radius, 0.0);\n    vNeighborTexCoord11 = vTexCoord + vec2(6.0*radius, 0.0);\n}"

    .line 185
    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 187
    .local v1, "vertex":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    .line 188
    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->FRAGMENT:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v3, "precision mediump float;\nuniform sampler2D SGTexture;\nvarying vec2 vTexCoord;\nvarying vec2 vNeighborTexCoord0;\nvarying vec2 vNeighborTexCoord1;\nvarying vec2 vNeighborTexCoord2;\nvarying vec2 vNeighborTexCoord3;\nvarying vec2 vNeighborTexCoord4;\nvarying vec2 vNeighborTexCoord5;\nvarying vec2 vNeighborTexCoord6;\nvarying vec2 vNeighborTexCoord7;\nvarying vec2 vNeighborTexCoord8;\nvarying vec2 vNeighborTexCoord9;\nvarying vec2 vNeighborTexCoord10;\nvarying vec2 vNeighborTexCoord11;\nvoid main()\n{\n     gl_FragColor = vec4(0.0);\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord0)*0.00895781211794;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord1)*0.0215963866053;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord2)*0.0443683338718;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord3)*0.0776744219933;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord4)*0.115876621105;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord5)*0.147308056121;\n     gl_FragColor += texture2D(SGTexture, vTexCoord            )*0.159576912161;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord6)*0.147308056121;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord7)*0.115876621105;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord8)*0.0776744219933;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord9)*0.0443683338718;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord10)*0.0215963866053;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord11)*0.00895781211794;\n}\n"

    .line 187
    invoke-direct {v0, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 189
    .local v0, "fragment":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-direct {v2, v1, v0}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;)V

    iput-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mHProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    .line 193
    new-instance v1, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    .line 194
    .end local v1    # "vertex":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->VERTEX:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v3, "precision mediump float;\nattribute highp vec3 SGPositions;\nattribute vec2 SGTextureCoords;\nvarying vec2 vTexCoord;\nvarying vec2 vNeighborTexCoord0;\nvarying vec2 vNeighborTexCoord1;\nvarying vec2 vNeighborTexCoord2;\nvarying vec2 vNeighborTexCoord3;\nvarying vec2 vNeighborTexCoord4;\nvarying vec2 vNeighborTexCoord5;\nvarying vec2 vNeighborTexCoord6;\nvarying vec2 vNeighborTexCoord7;\nvarying vec2 vNeighborTexCoord8;\nvarying vec2 vNeighborTexCoord9;\nvarying vec2 vNeighborTexCoord10;\nvarying vec2 vNeighborTexCoord11;\nuniform mat4 SGWorldViewProjection;\nuniform vec4 SGTextureRect;\nuniform float radius;\nvoid main() {\n    vTexCoord = SGTextureRect.xy + SGTextureCoords * (SGTextureRect.zw - SGTextureRect.xy);\n    gl_Position = SGWorldViewProjection * vec4(SGPositions, 1.0);\n    vNeighborTexCoord0 = vTexCoord + vec2(0.0, -6.0*radius);\n    vNeighborTexCoord1 = vTexCoord + vec2(0.0, -5.0*radius);\n    vNeighborTexCoord2 = vTexCoord + vec2(0.0, -4.0*radius);\n    vNeighborTexCoord3 = vTexCoord + vec2(0.0, -3.0*radius);\n    vNeighborTexCoord4 = vTexCoord + vec2(0.0, -2.0*radius);\n    vNeighborTexCoord5 = vTexCoord + vec2(0.0, -1.0*radius);\n    vNeighborTexCoord6 = vTexCoord + vec2(0.0,  1.0*radius);\n    vNeighborTexCoord7 = vTexCoord + vec2(0.0,  2.0*radius);\n    vNeighborTexCoord8 = vTexCoord + vec2(0.0,  3.0*radius);\n    vNeighborTexCoord9 = vTexCoord + vec2(0.0,  4.0*radius);\n    vNeighborTexCoord10 = vTexCoord + vec2(0.0,  5.0*radius);\n    vNeighborTexCoord11 = vTexCoord + vec2(0.0,  6.0*radius);\n}"

    .line 193
    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 195
    .restart local v1    # "vertex":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    .line 196
    .end local v0    # "fragment":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->FRAGMENT:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v3, "precision mediump float;\nuniform sampler2D SGTexture;\nvarying vec2 vTexCoord;\nvarying vec2 vNeighborTexCoord0;\nvarying vec2 vNeighborTexCoord1;\nvarying vec2 vNeighborTexCoord2;\nvarying vec2 vNeighborTexCoord3;\nvarying vec2 vNeighborTexCoord4;\nvarying vec2 vNeighborTexCoord5;\nvarying vec2 vNeighborTexCoord6;\nvarying vec2 vNeighborTexCoord7;\nvarying vec2 vNeighborTexCoord8;\nvarying vec2 vNeighborTexCoord9;\nvarying vec2 vNeighborTexCoord10;\nvarying vec2 vNeighborTexCoord11;\nvoid main()\n{\n     gl_FragColor = vec4(0.0);\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord0)*0.00895781211794;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord1)*0.0215963866053;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord2)*0.0443683338718;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord3)*0.0776744219933;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord4)*0.115876621105;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord5)*0.147308056121;\n     gl_FragColor += texture2D(SGTexture, vTexCoord            )*0.159576912161;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord6)*0.147308056121;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord7)*0.115876621105;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord8)*0.0776744219933;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord9)*0.0443683338718;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord10)*0.0215963866053;\n     gl_FragColor += texture2D(SGTexture, vNeighborTexCoord11)*0.00895781211794;\n}\n"

    .line 195
    invoke-direct {v0, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 197
    .restart local v0    # "fragment":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-direct {v2, v1, v0}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;)V

    iput-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mVProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    .line 201
    new-instance v1, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    .line 202
    .end local v1    # "vertex":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->VERTEX:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v3, "attribute vec3 SGPositions;\nattribute vec2 SGTextureCoords;\n\nvarying vec2 vTexCoords;\n\nuniform mat4 SGWorldViewProjection;\nuniform vec4 SGTextureRect;\n\nvoid main()\n{\n    vTexCoords = SGTextureRect.xy + SGTextureCoords * (SGTextureRect.zw - SGTextureRect.xy);\n    gl_Position = SGWorldViewProjection * vec4(SGPositions, 1.0);\n}\n"

    .line 201
    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 203
    .restart local v1    # "vertex":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;

    .line 204
    .end local v0    # "fragment":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    sget-object v2, Lcom/samsung/android/sdk/sgi/render/SGShaderType;->FRAGMENT:Lcom/samsung/android/sdk/sgi/render/SGShaderType;

    const-string v3, "precision mediump float;\nvarying vec2 vTexCoords;\n\nuniform sampler2D SGTexture;\nuniform sampler2D textureBlured;\n\nuniform float uBlend;\n\nvoid main(){\n   vec4 bluredColor = texture2D(textureBlured, vTexCoords);\n   vec4 color = texture2D(SGTexture, vTexCoords);\n   gl_FragColor = bluredColor * uBlend + color * (1.0-uBlend);\n}"

    .line 203
    invoke-direct {v0, v2, v3}, Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderType;Ljava/lang/String;)V

    .line 205
    .restart local v0    # "fragment":Lcom/samsung/android/sdk/sgi/render/SGStringShaderProperty;
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-direct {v2, v1, v0}, Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;-><init>(Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;Lcom/samsung/android/sdk/sgi/render/SGShaderProperty;)V

    iput-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBlendProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    .line 207
    new-instance v2, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    invoke-direct {v2, v4}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;-><init>(F)V

    iput-object v2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBlendProperty:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    .line 208
    return-void
.end method

.method static synthetic access$0(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/mimage/blureffect/GaussianLayer;Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mScreen_shot_y_blured:Lcom/samsung/android/sdk/sgi/render/SGProperty;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBlendProperty:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBlendProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mContentRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/mimage/blureffect/GaussianLayer;)F
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mWidth:F

    return v0
.end method

.method static synthetic access$3(Lcom/sec/android/mimage/blureffect/GaussianLayer;)F
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mHeight:F

    return v0
.end method

.method static synthetic access$4(Lcom/sec/android/mimage/blureffect/GaussianLayer;)F
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mRadius:F

    return v0
.end method

.method static synthetic access$5(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mHProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    return-object v0
.end method

.method static synthetic access$6(Lcom/sec/android/mimage/blureffect/GaussianLayer;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mScreen_shot_x_blured:Lcom/samsung/android/sdk/sgi/render/SGProperty;

    return-void
.end method

.method static synthetic access$7(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/render/SGProperty;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mScreen_shot_x_blured:Lcom/samsung/android/sdk/sgi/render/SGProperty;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/mimage/blureffect/GaussianLayer;)Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mVProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    return-object v0
.end method

.method static synthetic access$9(Lcom/sec/android/mimage/blureffect/GaussianLayer;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mScreen_shot_y_blured:Lcom/samsung/android/sdk/sgi/render/SGProperty;

    return-void
.end method


# virtual methods
.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 266
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mIsBlured:Z

    .line 268
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapWidth:I

    .line 269
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapHeight:I

    .line 271
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->setFitBitmapRatio()V

    .line 273
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 276
    return-void
.end method

.method public setContentRect(Landroid/graphics/RectF;)V
    .locals 1
    .param p1, "arg0"    # Landroid/graphics/RectF;

    .prologue
    .line 258
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setContentRect(Landroid/graphics/RectF;)V

    .line 259
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setContentRect(Landroid/graphics/RectF;)V

    .line 262
    :cond_0
    return-void
.end method

.method public setFitBitmapRatio()V
    .locals 13

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const/high16 v11, 0x3f000000    # 0.5f

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    .line 279
    iget v7, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapWidth:I

    int-to-float v7, v7

    cmpl-float v7, v7, v9

    if-eqz v7, :cond_0

    iget v7, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapHeight:I

    int-to-float v7, v7

    cmpl-float v7, v7, v9

    if-nez v7, :cond_1

    .line 318
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v7

    cmpl-float v7, v7, v9

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v7

    cmpl-float v7, v7, v9

    if-eqz v7, :cond_0

    .line 287
    const-string v7, "setFitBitmapRatio"

    invoke-static {v7}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 289
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v9, v9, v10, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 291
    .local v4, "rect":Landroid/graphics/RectF;
    iget v7, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapHeight:I

    int-to-float v7, v7

    iget v8, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapWidth:I

    int-to-float v8, v8

    div-float v0, v7, v8

    .line 292
    .local v0, "bitmap_ratio":F
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v7

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v8

    div-float v2, v7, v8

    .line 294
    .local v2, "layerImage_ratio":F
    cmpg-float v7, v0, v2

    if-gez v7, :cond_3

    .line 295
    iget v7, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapHeight:I

    int-to-float v7, v7

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v8

    mul-float/2addr v7, v8

    div-float/2addr v7, v12

    iget v8, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapWidth:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v8

    div-float/2addr v7, v8

    sub-float v3, v11, v7

    .line 296
    .local v3, "left":F
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v7

    iget v8, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapHeight:I

    int-to-float v8, v8

    mul-float/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v8

    div-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapWidth:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    add-float v5, v3, v7

    .line 298
    .local v5, "right":F
    invoke-virtual {v4, v3, v9, v5, v10}, Landroid/graphics/RectF;->set(FFFF)V

    .line 307
    .end local v3    # "left":F
    .end local v5    # "right":F
    :cond_2
    :goto_1
    iput-object v4, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mContentRect:Landroid/graphics/RectF;

    .line 309
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "view Ratio = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 310
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "bitmap Ratio = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 311
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "view = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 312
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "bitmap = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapWidth:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapHeight:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 313
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "rect = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, v4, Landroid/graphics/RectF;->left:F

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v4, Landroid/graphics/RectF;->right:F

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v4, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/mimage/blureffect/Define;->addLog(Ljava/lang/String;)V

    .line 315
    iget-object v7, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    if-eqz v7, :cond_0

    .line 316
    iget-object v7, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    iget-object v8, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mContentRect:Landroid/graphics/RectF;

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setContentRect(Landroid/graphics/RectF;)V

    goto/16 :goto_0

    .line 300
    :cond_3
    cmpl-float v7, v0, v2

    if-lez v7, :cond_2

    .line 301
    iget v7, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapWidth:I

    int-to-float v7, v7

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v8

    mul-float/2addr v7, v8

    div-float/2addr v7, v12

    iget v8, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapHeight:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v8

    div-float/2addr v7, v8

    sub-float v6, v11, v7

    .line 302
    .local v6, "top":F
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v7

    iget v8, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapWidth:I

    int-to-float v8, v8

    mul-float/2addr v7, v8

    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v8

    div-float/2addr v7, v8

    iget v8, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBitmapHeight:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    add-float v1, v6, v7

    .line 304
    .local v1, "bottom":F
    invoke-virtual {v4, v9, v6, v10, v1}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_1
.end method

.method public setSize(FF)V
    .locals 1
    .param p1, "arg0"    # F
    .param p2, "arg1"    # F

    .prologue
    .line 212
    iput p1, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mWidth:F

    .line 213
    iput p2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mHeight:F

    .line 214
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 215
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 218
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->setFitBitmapRatio()V

    .line 219
    return-void
.end method

.method public setSize(FFZ)V
    .locals 1
    .param p1, "arg0"    # F
    .param p2, "arg1"    # F
    .param p3, "recalculate"    # Z

    .prologue
    .line 233
    iput p1, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mWidth:F

    .line 234
    iput p2, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mHeight:F

    .line 235
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(FF)V

    .line 239
    :cond_0
    if-eqz p3, :cond_1

    .line 240
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->setFitBitmapRatio()V

    .line 242
    :cond_1
    return-void
.end method

.method public setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V
    .locals 1
    .param p1, "arg0"    # Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    .prologue
    .line 223
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mWidth:F

    .line 224
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mHeight:F

    .line 225
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 226
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 229
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->setFitBitmapRatio()V

    .line 230
    return-void
.end method

.method public setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;Z)V
    .locals 1
    .param p1, "arg0"    # Lcom/samsung/android/sdk/sgi/base/SGVector2f;
    .param p2, "recalculate"    # Z

    .prologue
    .line 245
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mWidth:F

    .line 246
    invoke-virtual {p1}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mHeight:F

    .line 247
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 248
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 251
    :cond_0
    if-eqz p2, :cond_1

    .line 252
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->setFitBitmapRatio()V

    .line 254
    :cond_1
    return-void
.end method

.method public updateBlurredImage()V
    .locals 9

    .prologue
    const/high16 v8, 0x41700000    # 15.0f

    const v7, 0x3e4ccccd    # 0.2f

    .line 365
    iget-boolean v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mIsBlured:Z

    if-eqz v3, :cond_0

    .line 410
    :goto_0
    return-void

    .line 369
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->makeScreenShot()Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mInputFBOA:Lcom/samsung/android/sdk/sgi/render/SGProperty;

    .line 372
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    if-nez v3, :cond_1

    .line 373
    new-instance v3, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-direct {v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;-><init>()V

    iput-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    .line 374
    move-object v2, p0

    .line 375
    .local v2, "parent":Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayer;->addLayer(Lcom/samsung/android/sdk/sgi/vi/SGLayer;)I

    .line 378
    .end local v2    # "parent":Lcom/samsung/android/sdk/sgi/vi/SGLayer;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    new-instance v4, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget v5, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mWidth:F

    mul-float/2addr v5, v7

    iget v6, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mHeight:F

    mul-float/2addr v6, v7

    invoke-direct {v4, v5, v6}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 379
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->setOpacity(F)V

    .line 383
    new-instance v0, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    iget v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mRadius:F

    .line 384
    iget-object v4, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getX()F

    move-result v4

    div-float/2addr v3, v4

    div-float/2addr v3, v8

    .line 383
    invoke-direct {v0, v3}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;-><init>(F)V

    .line 385
    .local v0, "deltaX":Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    const-string v4, "radius"

    invoke-virtual {v3, v4, v0}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 386
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    const-string v4, "SGTexture"

    iget-object v5, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mInputFBOA:Lcom/samsung/android/sdk/sgi/render/SGProperty;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 387
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    iget-object v4, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mHProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V

    .line 388
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->makeScreenShot()Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mScreen_shot_x_blured:Lcom/samsung/android/sdk/sgi/render/SGProperty;

    .line 393
    new-instance v1, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    iget v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mRadius:F

    .line 394
    iget-object v4, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->getSize()Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;->getY()F

    move-result v4

    div-float/2addr v3, v4

    div-float/2addr v3, v8

    .line 393
    invoke-direct {v1, v3}, Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;-><init>(F)V

    .line 395
    .local v1, "deltaY":Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    const-string v4, "radius"

    invoke-virtual {v3, v4, v1}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 396
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    const-string v4, "SGTexture"

    iget-object v5, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mScreen_shot_x_blured:Lcom/samsung/android/sdk/sgi/render/SGProperty;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 397
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    iget-object v4, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mVProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V

    .line 398
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->makeScreenShot()Lcom/samsung/android/sdk/sgi/render/SGProperty;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mScreen_shot_y_blured:Lcom/samsung/android/sdk/sgi/render/SGProperty;

    .line 401
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    new-instance v4, Lcom/samsung/android/sdk/sgi/base/SGVector2f;

    iget v5, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mWidth:F

    iget v6, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mHeight:F

    invoke-direct {v4, v5, v6}, Lcom/samsung/android/sdk/sgi/base/SGVector2f;-><init>(FF)V

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setSize(Lcom/samsung/android/sdk/sgi/base/SGVector2f;)V

    .line 403
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    const-string v4, "textureBlured"

    iget-object v5, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mScreen_shot_y_blured:Lcom/samsung/android/sdk/sgi/render/SGProperty;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 404
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    const-string v4, "SGTexture"

    iget-object v5, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mInputFBOA:Lcom/samsung/android/sdk/sgi/render/SGProperty;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 405
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    const-string v4, "uBlend"

    iget-object v5, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBlendProperty:Lcom/samsung/android/sdk/sgi/render/SGFloatProperty;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProperty(Ljava/lang/String;Lcom/samsung/android/sdk/sgi/render/SGProperty;)V

    .line 406
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    iget-object v4, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mBlendProgram:Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setProgramProperty(Lcom/samsung/android/sdk/sgi/render/SGShaderProgramProperty;)V

    .line 408
    invoke-virtual {p0}, Lcom/sec/android/mimage/blureffect/GaussianLayer;->setFitBitmapRatio()V

    .line 409
    iget-object v3, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mLayer:Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;

    iget-object v4, p0, Lcom/sec/android/mimage/blureffect/GaussianLayer;->mContentRect:Landroid/graphics/RectF;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/sgi/vi/SGLayerImage;->setContentRect(Landroid/graphics/RectF;)V

    goto/16 :goto_0
.end method
