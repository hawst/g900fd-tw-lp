.class public Lcom/sec/android/sgieffect/SGIEffect;
.super Landroid/app/ListActivity;
.source "SGIEffect.java"


# instance fields
.field mIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/sgieffect/SGIEffect;->mIntent:Landroid/content/Intent;

    .line 23
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 35
    const/4 v2, 0x4

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 36
    const-string v3, "Calculator Demo"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 37
    const-string v3, "GroupPlay Demo"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 38
    const-string v3, "PhotoEditor Demo - 1"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 39
    const-string v3, "PhotoEditor Demo - 2"

    aput-object v3, v1, v2

    .line 44
    .local v1, "mListCont":[Ljava/lang/String;
    new-instance v2, Landroid/widget/ArrayAdapter;

    const v3, 0x1090003

    invoke-direct {v2, p0, v3, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {p0, v2}, Lcom/sec/android/sgieffect/SGIEffect;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 46
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/SGIEffect;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 48
    .local v0, "list":Landroid/widget/ListView;
    new-instance v2, Lcom/sec/android/sgieffect/SGIEffect$1;

    invoke-direct {v2, p0}, Lcom/sec/android/sgieffect/SGIEffect$1;-><init>(Lcom/sec/android/sgieffect/SGIEffect;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 87
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/sgieffect/SGIEffect;->mIntent:Landroid/content/Intent;

    .line 96
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 97
    return-void
.end method
