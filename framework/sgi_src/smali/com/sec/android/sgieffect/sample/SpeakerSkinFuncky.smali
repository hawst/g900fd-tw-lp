.class public Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;
.super Lcom/sec/android/sgieffect/sample/SpeakerSkin;
.source "SpeakerSkinFuncky.java"


# instance fields
.field public mAllBase:Landroid/graphics/Bitmap;

.field public mBaseFX:Landroid/graphics/Bitmap;

.field public mBottomBase:Landroid/graphics/Bitmap;

.field private mCurrentMaskType:I

.field public mHighFX:Landroid/graphics/Bitmap;

.field public mLowFX:Landroid/graphics/Bitmap;

.field public mMask:Landroid/graphics/Bitmap;

.field public mMaskA:Landroid/graphics/Bitmap;

.field public mMaskB:Landroid/graphics/Bitmap;

.field public mMaskC:Landroid/graphics/Bitmap;

.field public mMaskD:Landroid/graphics/Bitmap;

.field public mMaskE:Landroid/graphics/Bitmap;

.field public mMidFX:Landroid/graphics/Bitmap;

.field public mOtherFX_A:Landroid/graphics/Bitmap;

.field public mOtherFX_B:Landroid/graphics/Bitmap;

.field public mOtherFX_C:Landroid/graphics/Bitmap;

.field public mOtherFX_D:Landroid/graphics/Bitmap;

.field public mOtherFX_Top:Landroid/graphics/Bitmap;

.field public mOtherTopBase:Landroid/graphics/Bitmap;

.field public mTopBase:Landroid/graphics/Bitmap;

.field public mTopFX:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 12
    invoke-direct {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSkin;-><init>()V

    .line 13
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mBottomBase:Landroid/graphics/Bitmap;

    .line 15
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mTopFX:Landroid/graphics/Bitmap;

    .line 16
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mHighFX:Landroid/graphics/Bitmap;

    .line 17
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMidFX:Landroid/graphics/Bitmap;

    .line 18
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mLowFX:Landroid/graphics/Bitmap;

    .line 19
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mBaseFX:Landroid/graphics/Bitmap;

    .line 21
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mTopBase:Landroid/graphics/Bitmap;

    .line 22
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mAllBase:Landroid/graphics/Bitmap;

    .line 24
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskA:Landroid/graphics/Bitmap;

    .line 25
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskB:Landroid/graphics/Bitmap;

    .line 26
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskC:Landroid/graphics/Bitmap;

    .line 27
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskD:Landroid/graphics/Bitmap;

    .line 28
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskE:Landroid/graphics/Bitmap;

    .line 32
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_A:Landroid/graphics/Bitmap;

    .line 33
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_B:Landroid/graphics/Bitmap;

    .line 34
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_C:Landroid/graphics/Bitmap;

    .line 35
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_D:Landroid/graphics/Bitmap;

    .line 36
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_Top:Landroid/graphics/Bitmap;

    .line 37
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherTopBase:Landroid/graphics/Bitmap;

    .line 41
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMask:Landroid/graphics/Bitmap;

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mCurrentMaskType:I

    .line 12
    return-void
.end method


# virtual methods
.method public changeBgImage(Landroid/content/res/Resources;I)V
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "maskType"    # I

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mCurrentMaskType:I

    if-eq v0, p2, :cond_0

    const/4 v0, 0x5

    if-ge p2, v0, :cond_0

    .line 84
    packed-switch p2, :pswitch_data_0

    .line 102
    :goto_0
    iput p2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mCurrentMaskType:I

    .line 104
    :cond_0
    return-void

    .line 86
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskA:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMask:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 89
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskB:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMask:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 92
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskC:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMask:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 95
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskD:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMask:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 98
    :pswitch_4
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskE:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMask:Landroid/graphics/Bitmap;

    goto :goto_0

    .line 84
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public finalize()V
    .locals 0

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmaps()V

    .line 189
    return-void
.end method

.method public loadBgImages(Landroid/content/res/Resources;)V
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/sec/android/sgieffect/sample/SpeakerSkin;->loadSpeakerImages(Landroid/content/res/Resources;)V

    .line 49
    const/4 v0, 0x0

    .line 51
    .local v0, "is":Ljava/io/InputStream;
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskA:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 52
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->mask_a:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 53
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskA:Landroid/graphics/Bitmap;

    .line 56
    :cond_0
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskB:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    .line 57
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->mask_b:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 58
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskB:Landroid/graphics/Bitmap;

    .line 61
    :cond_1
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskC:Landroid/graphics/Bitmap;

    if-nez v1, :cond_2

    .line 62
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->mask_c:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 63
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskC:Landroid/graphics/Bitmap;

    .line 66
    :cond_2
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskD:Landroid/graphics/Bitmap;

    if-nez v1, :cond_3

    .line 67
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->mask_d:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 68
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskD:Landroid/graphics/Bitmap;

    .line 71
    :cond_3
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskE:Landroid/graphics/Bitmap;

    if-nez v1, :cond_4

    .line 72
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->mask_e:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 73
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskE:Landroid/graphics/Bitmap;

    .line 76
    :cond_4
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskA:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMask:Landroid/graphics/Bitmap;

    .line 77
    return-void
.end method

.method public loadSpeakerImages(Landroid/content/res/Resources;)V
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 108
    invoke-super {p0, p1}, Lcom/sec/android/sgieffect/sample/SpeakerSkin;->loadSpeakerImages(Landroid/content/res/Resources;)V

    .line 110
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mBottomBase:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 152
    :goto_0
    return-void

    .line 114
    :cond_0
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->bottom_base:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 115
    .local v0, "is":Ljava/io/InputStream;
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mBottomBase:Landroid/graphics/Bitmap;

    .line 117
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->fx_top:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 118
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mTopFX:Landroid/graphics/Bitmap;

    .line 120
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->fx_high:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 121
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mHighFX:Landroid/graphics/Bitmap;

    .line 123
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->fx_mid:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 124
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMidFX:Landroid/graphics/Bitmap;

    .line 126
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->fx_low:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 127
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mLowFX:Landroid/graphics/Bitmap;

    .line 129
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->fx_base:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 130
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mBaseFX:Landroid/graphics/Bitmap;

    .line 132
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->top_base:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 133
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mTopBase:Landroid/graphics/Bitmap;

    .line 135
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->fx_s_a:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 136
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_A:Landroid/graphics/Bitmap;

    .line 138
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->fx_s_b:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 139
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_B:Landroid/graphics/Bitmap;

    .line 141
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->fx_s_c:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 142
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_C:Landroid/graphics/Bitmap;

    .line 144
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->fx_s_d:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 145
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_D:Landroid/graphics/Bitmap;

    .line 147
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->fx_s_top:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 148
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_Top:Landroid/graphics/Bitmap;

    .line 150
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->top_base:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 151
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherTopBase:Landroid/graphics/Bitmap;

    goto/16 :goto_0
.end method

.method public releaseBitmaps()V
    .locals 1

    .prologue
    .line 157
    invoke-super {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSkin;->releaseBitmaps()V

    .line 159
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mBottomBase:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 160
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mTopFX:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 161
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mHighFX:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 162
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMidFX:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 163
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mLowFX:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mBaseFX:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 166
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mTopBase:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mAllBase:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMask:Landroid/graphics/Bitmap;

    .line 170
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskA:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 171
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskB:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskC:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 173
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskD:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 174
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMaskE:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_A:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 177
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_B:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 178
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_C:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 179
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_D:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_Top:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherTopBase:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 182
    return-void
.end method
