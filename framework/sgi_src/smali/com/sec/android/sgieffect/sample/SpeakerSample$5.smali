.class Lcom/sec/android/sgieffect/sample/SpeakerSample$5;
.super Ljava/lang/Object;
.source "SpeakerSample.java"

# interfaces
.implements Landroid/media/audiofx/Visualizer$OnDataCaptureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/sgieffect/sample/SpeakerSample;->createAudioModules()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;


# direct methods
.method constructor <init>(Lcom/sec/android/sgieffect/sample/SpeakerSample;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample$5;->this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;

    .line 358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFftDataCapture(Landroid/media/audiofx/Visualizer;[BI)V
    .locals 1
    .param p1, "visualizer"    # Landroid/media/audiofx/Visualizer;
    .param p2, "bytes"    # [B
    .param p3, "samplingRate"    # I

    .prologue
    .line 370
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample$5;->this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;

    invoke-virtual {v0, p2}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->updateVisualizerFFT([B)V

    .line 371
    return-void
.end method

.method public onWaveFormDataCapture(Landroid/media/audiofx/Visualizer;[BI)V
    .locals 1
    .param p1, "visualizer"    # Landroid/media/audiofx/Visualizer;
    .param p2, "bytes"    # [B
    .param p3, "samplingRate"    # I

    .prologue
    .line 361
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample$5;->this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;

    invoke-virtual {v0, p2}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->updateVisualizer([B)V

    .line 362
    return-void
.end method
