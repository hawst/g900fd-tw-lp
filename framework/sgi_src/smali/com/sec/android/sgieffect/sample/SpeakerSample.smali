.class public Lcom/sec/android/sgieffect/sample/SpeakerSample;
.super Landroid/app/Activity;
.source "SpeakerSample.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Lcom/samsung/sgieffect/main/SGIEffectListener;


# instance fields
.field private FREQUENCY_MILLIHERTZ:I

.field private final KEEP_MASK_TYPE:J

.field private final TAG:Ljava/lang/String;

.field private accRMS:D

.field private count:I

.field private gravitymeter:Landroid/hardware/Sensor;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mChangedTime:J

.field private mCollectColor:Lcom/samsung/sgieffect/speaker/findColor/CollectColor;

.field private mColors:[I

.field private mCoverView:Landroid/widget/ImageView;

.field private mCurrentMask:I

.field private mCurrentSkin:I

.field private mDbValues:[F

.field private mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

.field private mEqualizer:Landroid/media/audiofx/Equalizer;

.field private mFindColors:[I

.field private mGravity:[F

.field private mHeight:F

.field private mLayout:Landroid/widget/FrameLayout;

.field private mLayoutClassic:Landroid/widget/AbsoluteLayout;

.field private mLayoutFuncky:Landroid/widget/AbsoluteLayout;

.field private mLayoutWave:Landroid/widget/AbsoluteLayout;

.field private mListenerClassic:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private mListenerFuncky:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private mListenerWave:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private mMovementScale:F

.field private mMusicPlayer:Landroid/media/MediaPlayer;

.field private mPrevDbValues:[F

.field private mRMS:D

.field private mRemoveSplashHandler:Landroid/os/Handler;

.field private mSGSurfaceChangesDrwawnListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

.field private mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

.field private mSkinWave:Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;

.field private mSplashView:Landroid/widget/ImageView;

.field private mUseSound:Z

.field private mVisualizer:Landroid/media/audiofx/Visualizer;

.field private mWaveHighPos:Landroid/graphics/Point;

.field private mWaveLowPos:Landroid/graphics/Point;

.field private mWaveMidPos:Landroid/graphics/Point;

.field private mWidth:F

.field mode:I

.field private prevTime:Ljava/util/Calendar;

.field public prevTimeFFT:J


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 56
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 57
    const-string v0, "SGIGroupPlay"

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->TAG:Ljava/lang/String;

    .line 59
    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    .line 60
    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->gravitymeter:Landroid/hardware/Sensor;

    .line 61
    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSensorManager:Landroid/hardware/SensorManager;

    .line 62
    new-array v0, v6, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mGravity:[F

    .line 66
    new-instance v0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    invoke-direct {v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    .line 67
    new-instance v0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    invoke-direct {v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    .line 69
    new-instance v0, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;

    invoke-direct {v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;-><init>()V

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinWave:Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;

    .line 72
    const/high16 v0, 0x44340000    # 720.0f

    iput v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWidth:F

    .line 73
    const/high16 v0, 0x44a00000    # 1280.0f

    iput v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mHeight:F

    .line 74
    const v0, 0x3f333333    # 0.7f

    iput v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMovementScale:F

    .line 76
    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    .line 79
    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutClassic:Landroid/widget/AbsoluteLayout;

    .line 80
    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutFuncky:Landroid/widget/AbsoluteLayout;

    .line 82
    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutWave:Landroid/widget/AbsoluteLayout;

    .line 85
    iput v3, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCurrentSkin:I

    .line 86
    iput v3, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCurrentMask:I

    .line 91
    const-wide/16 v0, 0x1f40

    iput-wide v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->KEEP_MASK_TYPE:J

    .line 98
    iput-boolean v4, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mUseSound:Z

    .line 101
    const v0, 0xf4240

    iput v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->FREQUENCY_MILLIHERTZ:I

    .line 102
    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCoverView:Landroid/widget/ImageView;

    .line 106
    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSplashView:Landroid/widget/ImageView;

    .line 109
    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mDbValues:[F

    .line 113
    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mPrevDbValues:[F

    .line 124
    const/16 v0, 0x27

    new-array v0, v0, [I

    .line 126
    const/16 v1, 0xf2

    aput v1, v0, v6

    const/4 v1, 0x4

    const/16 v2, 0x40

    aput v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0xc5

    aput v2, v0, v1

    const/4 v1, 0x6

    .line 127
    const/16 v2, 0xa6

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x31

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0xea

    aput v2, v0, v1

    const/16 v1, 0x9

    .line 128
    const/16 v2, 0xea

    aput v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0x38

    aput v2, v0, v1

    const/16 v1, 0xb

    const/16 v2, 0xbc

    aput v2, v0, v1

    const/16 v1, 0xc

    .line 129
    const/16 v2, 0xc3

    aput v2, v0, v1

    const/16 v1, 0xd

    const/16 v2, 0x2c

    aput v2, v0, v1

    const/16 v1, 0xe

    const/16 v2, 0xdd

    aput v2, v0, v1

    const/16 v1, 0xf

    .line 130
    aput v4, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x3d

    aput v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0xf4

    aput v2, v0, v1

    const/16 v1, 0x12

    .line 132
    const/16 v2, 0xdf

    aput v2, v0, v1

    const/16 v1, 0x13

    const/16 v2, 0x39

    aput v2, v0, v1

    const/16 v1, 0x14

    const/16 v2, 0xb0

    aput v2, v0, v1

    const/16 v1, 0x15

    .line 133
    const/16 v2, 0xcb

    aput v2, v0, v1

    const/16 v1, 0x16

    aput v4, v0, v1

    const/16 v1, 0x17

    const/16 v2, 0x56

    aput v2, v0, v1

    const/16 v1, 0x18

    .line 134
    const/16 v2, 0xb9

    aput v2, v0, v1

    const/16 v1, 0x19

    aput v4, v0, v1

    const/16 v1, 0x1a

    const/16 v2, 0xd2

    aput v2, v0, v1

    const/16 v1, 0x1b

    .line 135
    const/16 v2, 0xbc

    aput v2, v0, v1

    const/16 v1, 0x1c

    aput v5, v0, v1

    const/16 v1, 0x1d

    const/16 v2, 0x52

    aput v2, v0, v1

    const/16 v1, 0x1e

    .line 136
    const/16 v2, 0x9f

    aput v2, v0, v1

    const/16 v1, 0x1f

    const/16 v2, 0x11

    aput v2, v0, v1

    const/16 v1, 0x20

    const/16 v2, 0xd8

    aput v2, v0, v1

    const/16 v1, 0x21

    .line 138
    const/16 v2, 0xf1

    aput v2, v0, v1

    const/16 v1, 0x22

    const/4 v2, 0x5

    aput v2, v0, v1

    const/16 v1, 0x23

    const/16 v2, 0x7d

    aput v2, v0, v1

    const/16 v1, 0x24

    .line 139
    aput v5, v0, v1

    const/16 v1, 0x25

    const/16 v2, 0x2f

    aput v2, v0, v1

    const/16 v1, 0x26

    const/16 v2, 0xb3

    aput v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mColors:[I

    .line 174
    iput v3, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->count:I

    .line 175
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->accRMS:D

    .line 176
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mChangedTime:J

    .line 256
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->prevTimeFFT:J

    .line 941
    new-instance v0, Lcom/sec/android/sgieffect/sample/SpeakerSample$1;

    invoke-direct {v0, p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample$1;-><init>(Lcom/sec/android/sgieffect/sample/SpeakerSample;)V

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mListenerClassic:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 953
    new-instance v0, Lcom/sec/android/sgieffect/sample/SpeakerSample$2;

    invoke-direct {v0, p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample$2;-><init>(Lcom/sec/android/sgieffect/sample/SpeakerSample;)V

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mListenerFuncky:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 964
    new-instance v0, Lcom/sec/android/sgieffect/sample/SpeakerSample$3;

    invoke-direct {v0, p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample$3;-><init>(Lcom/sec/android/sgieffect/sample/SpeakerSample;)V

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mListenerWave:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1154
    iput v3, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mode:I

    .line 1232
    new-instance v0, Lcom/sec/android/sgieffect/sample/SpeakerSample$4;

    invoke-direct {v0, p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample$4;-><init>(Lcom/sec/android/sgieffect/sample/SpeakerSample;)V

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mRemoveSplashHandler:Landroid/os/Handler;

    .line 56
    return-void

    .line 62
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method static synthetic access$0(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Landroid/widget/AbsoluteLayout;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutClassic:Landroid/widget/AbsoluteLayout;

    return-object v0
.end method

.method static synthetic access$1(Lcom/sec/android/sgieffect/sample/SpeakerSample;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 553
    invoke-direct {p0, p1}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildClassicSkin(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$10(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSplashView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$11(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    return-object v0
.end method

.method static synthetic access$12(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    return-object v0
.end method

.method static synthetic access$13(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinWave:Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;

    return-object v0
.end method

.method static synthetic access$2(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .locals 1

    .prologue
    .line 941
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mListenerClassic:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    return-object v0
.end method

.method static synthetic access$4(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Landroid/widget/AbsoluteLayout;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutFuncky:Landroid/widget/AbsoluteLayout;

    return-object v0
.end method

.method static synthetic access$5(Lcom/sec/android/sgieffect/sample/SpeakerSample;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 600
    invoke-direct {p0, p1}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildFunckySkin(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$6(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .locals 1

    .prologue
    .line 953
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mListenerFuncky:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    return-object v0
.end method

.method static synthetic access$7(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Landroid/widget/AbsoluteLayout;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutWave:Landroid/widget/AbsoluteLayout;

    return-object v0
.end method

.method static synthetic access$8(Lcom/sec/android/sgieffect/sample/SpeakerSample;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 842
    invoke-direct {p0, p1}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildWaveSkin(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$9(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .locals 1

    .prologue
    .line 964
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mListenerWave:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    return-object v0
.end method

.method private buildClassicSkin(Landroid/view/View;)V
    .locals 14
    .param p1, "layout"    # Landroid/view/View;

    .prologue
    .line 554
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    if-nez v0, :cond_0

    .line 596
    :goto_0
    return-void

    .line 556
    :cond_0
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->clearNodes()V

    .line 558
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setSkinMode(I)V

    .line 560
    sget v0, Lcom/sec/android/sgieffect/R$id;->base_outside_port:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 561
    .local v9, "baseViewFromXML":Landroid/view/View;
    sget v0, Lcom/sec/android/sgieffect/R$id;->rubber_outside_port:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .line 562
    .local v13, "rubberOutsideViewFromXML":Landroid/view/View;
    sget v0, Lcom/sec/android/sgieffect/R$id;->rubber_inside_port:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 563
    .local v12, "rubberInsideViewFromXML":Landroid/view/View;
    sget v0, Lcom/sec/android/sgieffect/R$id;->center_circle_port:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 567
    .local v10, "centerViewFromXML":Landroid/view/View;
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSplashView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/FrameLayout$LayoutParams;

    .line 568
    .local v11, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, v11, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 569
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, v11, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 570
    invoke-virtual {v9}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    iput v0, v11, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 571
    invoke-virtual {v9}, Landroid/view/View;->getPaddingTop()I

    move-result v0

    iput v0, v11, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 572
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSplashView:Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 574
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    .line 575
    invoke-virtual {v9}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v9}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    .line 576
    iget-object v5, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    iget-object v5, v5, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBaseDiffuse:Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    iget-object v6, v6, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBaseNormal:Landroid/graphics/Bitmap;

    iget-object v7, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    iget-object v7, v7, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBaseWeight:Landroid/graphics/Bitmap;

    .line 577
    const/high16 v8, -0x40800000    # -1.0f

    .line 574
    invoke-virtual/range {v0 .. v8}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addPannel(FFFFLandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;F)V

    .line 579
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    .line 580
    invoke-virtual {v13}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v13}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v13}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v13}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    .line 581
    iget-object v5, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    iget-object v5, v5, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberOutsideDiffuse:Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    iget-object v6, v6, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberOutsideNormal:Landroid/graphics/Bitmap;

    iget-object v7, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    iget-object v7, v7, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberOutsideWeight:Landroid/graphics/Bitmap;

    .line 582
    const/high16 v8, -0x40800000    # -1.0f

    .line 579
    invoke-virtual/range {v0 .. v8}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addPannel(FFFFLandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;F)V

    .line 584
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    .line 585
    invoke-virtual {v12}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v12}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v12}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v12}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    .line 586
    iget-object v5, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    iget-object v5, v5, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberInsideDiffuse:Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    iget-object v6, v6, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberInsideNormal:Landroid/graphics/Bitmap;

    iget-object v7, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    iget-object v7, v7, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberInsideWeight:Landroid/graphics/Bitmap;

    .line 587
    const/high16 v8, -0x40800000    # -1.0f

    .line 584
    invoke-virtual/range {v0 .. v8}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addPannel(FFFFLandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;F)V

    .line 589
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    .line 590
    invoke-virtual {v10}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v10}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v10}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    .line 591
    iget-object v5, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    iget-object v5, v5, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mCenterDiffuse:Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    iget-object v6, v6, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mCenterNormal:Landroid/graphics/Bitmap;

    iget-object v7, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    iget-object v7, v7, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mCenterWeight:Landroid/graphics/Bitmap;

    .line 592
    const/high16 v8, -0x40800000    # -1.0f

    .line 589
    invoke-virtual/range {v0 .. v8}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addPannel(FFFFLandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;F)V

    .line 594
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/4 v1, 0x3

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->validateOnSensorChange([FI)Z

    goto/16 :goto_0

    nop

    :array_0
    .array-data 4
        -0x3fc00000    # -3.0f
        0x40400000    # 3.0f
        0x40e00000    # 7.0f
    .end array-data
.end method

.method private buildFunckySkin(Landroid/view/View;)V
    .locals 42
    .param p1, "layout"    # Landroid/view/View;

    .prologue
    .line 601
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    if-nez v1, :cond_0

    .line 839
    :goto_0
    return-void

    .line 603
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->clearNodes()V

    .line 607
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setSkinMode(I)V

    .line 611
    sget v1, Lcom/sec/android/sgieffect/R$id;->fx_size:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    .line 612
    .local v19, "fxView":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->bottom_base:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    .line 613
    .local v17, "bottomBaseView":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->top_base:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v41

    .line 614
    .local v41, "topBaseView":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->all_base:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 617
    .local v16, "allBaseView":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_a_first_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    .line 618
    .local v22, "maskA_one_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_a_second_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v24

    .line 619
    .local v24, "maskA_two_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_a_third_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    .line 620
    .local v23, "maskA_three_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_a_fourth_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    .line 623
    .local v21, "maskA_four_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_b_first_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    .line 624
    .local v26, "maskB_one_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_b_second_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v28

    .line 625
    .local v28, "maskB_two_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_b_third_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    .line 626
    .local v27, "maskB_three_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_b_fourth_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v25

    .line 629
    .local v25, "maskB_four_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_c_first_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v30

    .line 630
    .local v30, "maskC_one_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_c_second_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v32

    .line 631
    .local v32, "maskC_two_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_c_third_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    .line 632
    .local v31, "maskC_three_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_c_fourth_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v29

    .line 635
    .local v29, "maskC_four_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_d_first_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v34

    .line 636
    .local v34, "maskD_one_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_d_second_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v36

    .line 637
    .local v36, "maskD_two_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_d_third_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v35

    .line 638
    .local v35, "maskD_three_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_d_fourth_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v33

    .line 641
    .local v33, "maskD_four_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_e_first_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v38

    .line 642
    .local v38, "maskE_one_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_e_second_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v40

    .line 643
    .local v40, "maskE_two_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_e_third_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v39

    .line 644
    .local v39, "maskE_three_center":Landroid/view/View;
    sget v1, Lcom/sec/android/sgieffect/R$id;->mask_e_fourth_center:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v37

    .line 649
    .local v37, "maskE_four_center":Landroid/view/View;
    const v6, 0x3d0f5c29    # 0.035f

    .line 650
    .local v6, "color":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    .line 651
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    .line 652
    const/high16 v9, 0x3f800000    # 1.0f

    move v7, v6

    move v8, v6

    .line 650
    invoke-virtual/range {v1 .. v9}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addPannel(FFFFFFFF)V

    .line 655
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    .line 656
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    int-to-float v8, v1

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    int-to-float v9, v1

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v10, v1

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v11, v1

    .line 657
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v12, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mBaseFX:Landroid/graphics/Bitmap;

    const/4 v13, 0x0

    .line 655
    invoke-virtual/range {v7 .. v13}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addPannel(FFFFLandroid/graphics/Bitmap;Z)V

    .line 660
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    .line 661
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    int-to-float v8, v1

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    int-to-float v9, v1

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v10, v1

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v11, v1

    .line 662
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v12, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mLowFX:Landroid/graphics/Bitmap;

    const/4 v13, 0x0

    .line 660
    invoke-virtual/range {v7 .. v13}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addPannel(FFFFLandroid/graphics/Bitmap;Z)V

    .line 667
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->createOthersCircleFX()V

    .line 670
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    .line 671
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    int-to-float v8, v1

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    int-to-float v9, v1

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v10, v1

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v11, v1

    .line 672
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v12, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMidFX:Landroid/graphics/Bitmap;

    const/4 v13, 0x0

    .line 670
    invoke-virtual/range {v7 .. v13}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addPannel(FFFFLandroid/graphics/Bitmap;Z)V

    .line 675
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    .line 676
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    int-to-float v8, v1

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    int-to-float v9, v1

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v10, v1

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v11, v1

    .line 677
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v12, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mHighFX:Landroid/graphics/Bitmap;

    const/4 v13, 0x0

    .line 675
    invoke-virtual/range {v7 .. v13}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addPannel(FFFFLandroid/graphics/Bitmap;Z)V

    .line 680
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    .line 681
    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    int-to-float v8, v1

    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    int-to-float v9, v1

    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v10, v1

    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v11, v1

    .line 682
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v12, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mTopBase:Landroid/graphics/Bitmap;

    const/4 v13, 0x0

    .line 680
    invoke-virtual/range {v7 .. v13}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addPannel(FFFFLandroid/graphics/Bitmap;Z)V

    .line 685
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    .line 686
    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    int-to-float v8, v1

    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    int-to-float v9, v1

    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v10, v1

    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v11, v1

    .line 687
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v12, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mTopFX:Landroid/graphics/Bitmap;

    const/4 v13, 0x0

    .line 685
    invoke-virtual/range {v7 .. v13}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addPannel(FFFFLandroid/graphics/Bitmap;Z)V

    .line 689
    new-instance v20, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object/from16 v0, v20

    invoke-direct {v0, v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;-><init>(Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;)V

    .line 691
    .local v20, "info":Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomWidth:F

    .line 692
    invoke-virtual/range {v17 .. v17}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomHeight:F

    .line 693
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxWidth:F

    .line 694
    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFxHeight:F

    .line 695
    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopWidth:F

    .line 696
    invoke-virtual/range {v41 .. v41}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopHeight:F

    .line 697
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mBottomBase:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mBottomBase:Landroid/graphics/Bitmap;

    .line 698
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherTopBase:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mTopBase:Landroid/graphics/Bitmap;

    .line 699
    const v1, 0x3f2147ae    # 0.63f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    .line 700
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    .line 701
    const v1, 0x3f47ae14    # 0.78f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    .line 702
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    .line 703
    const v1, 0x3f23d70a    # 0.64f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    .line 704
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    .line 705
    const v1, 0x3e99999a    # 0.3f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    .line 706
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    .line 708
    const/high16 v1, 0x43110000    # 145.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    .line 709
    const/high16 v1, 0x41f00000    # 30.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    .line 710
    const/high16 v1, -0x3d7e0000    # -65.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    .line 711
    const/4 v1, 0x0

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    .line 713
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_B:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    .line 714
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_D:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    .line 715
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_A:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    .line 716
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_C:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    .line 719
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/4 v2, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v1, v2, v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setMaskInfomation(ILcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;)V

    .line 721
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_B:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    .line 722
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_D:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    .line 723
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_A:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    .line 724
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_C:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    .line 726
    const v1, 0x3f5eb852    # 0.87f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    .line 727
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    .line 728
    const v1, 0x3f051eb8    # 0.52f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    .line 729
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v28 .. v28}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v28 .. v28}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    .line 730
    const v1, 0x3e947ae1    # 0.29f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    .line 731
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v27 .. v27}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    .line 732
    const/high16 v1, 0x3f400000    # 0.75f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    .line 733
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    .line 735
    const/high16 v1, -0x3cef0000    # -145.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    .line 736
    const/high16 v1, 0x43520000    # 210.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    .line 737
    const/high16 v1, -0x3d6a0000    # -75.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    .line 738
    const/high16 v1, -0x3e380000    # -25.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    .line 740
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_B:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    .line 741
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_D:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    .line 742
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_C:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    .line 743
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_A:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    .line 746
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/4 v2, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v1, v2, v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setMaskInfomation(ILcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;)V

    .line 748
    const v1, 0x3f19999a    # 0.6f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    .line 749
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v30 .. v30}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v30 .. v30}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    .line 750
    const v1, 0x3ec7ae14    # 0.39f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    .line 751
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v32 .. v32}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v32 .. v32}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    .line 752
    const v1, 0x3f28f5c3    # 0.66f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    .line 753
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v31 .. v31}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v31 .. v31}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    .line 754
    const/high16 v1, 0x3f800000    # 1.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    .line 755
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v29 .. v29}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v29 .. v29}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    .line 757
    const/high16 v1, -0x3ccc0000    # -180.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    .line 758
    const/high16 v1, 0x42f00000    # 120.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    .line 759
    const/high16 v1, 0x428c0000    # 70.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    .line 760
    const/high16 v1, -0x3df40000    # -35.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    .line 762
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_B:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    .line 763
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_D:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    .line 764
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_A:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    .line 765
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_C:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    .line 768
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/4 v2, 0x2

    move-object/from16 v0, v20

    invoke-virtual {v1, v2, v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setMaskInfomation(ILcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;)V

    .line 770
    const v1, 0x3f547ae1    # 0.83f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    .line 771
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v34 .. v34}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v34 .. v34}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    .line 772
    const/high16 v1, 0x3f800000    # 1.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    .line 773
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v36 .. v36}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v36 .. v36}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    .line 774
    const v1, 0x3ee66666    # 0.45f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    .line 775
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v35 .. v35}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v35 .. v35}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    .line 776
    const v1, 0x3e851eb8    # 0.26f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    .line 777
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v33 .. v33}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v33 .. v33}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    .line 779
    const/high16 v1, -0x3cef0000    # -145.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    .line 780
    const/4 v1, 0x0

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    .line 781
    const/high16 v1, -0x3d560000    # -85.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    .line 782
    const/high16 v1, -0x3e100000    # -30.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    .line 784
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_B:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    .line 785
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_A:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    .line 786
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_C:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    .line 787
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_D:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    .line 790
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/4 v2, 0x3

    move-object/from16 v0, v20

    invoke-virtual {v1, v2, v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setMaskInfomation(ILcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;)V

    .line 792
    const v1, 0x3ee147ae    # 0.44f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstScale:F

    .line 793
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v38 .. v38}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v38 .. v38}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFirstCenter:Landroid/graphics/Point;

    .line 794
    const v1, 0x3e570a3d    # 0.21f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondScale:F

    .line 795
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v40 .. v40}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v40 .. v40}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mSecondCenter:Landroid/graphics/Point;

    .line 796
    const v1, 0x3f051eb8    # 0.52f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdScale:F

    .line 797
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v39 .. v39}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v39 .. v39}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mThirdCenter:Landroid/graphics/Point;

    .line 798
    const v1, 0x3f70a3d7    # 0.94f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthScale:F

    .line 799
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual/range {v37 .. v37}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFourthCenter:Landroid/graphics/Point;

    .line 801
    const/high16 v1, 0x430c0000    # 140.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFirstRotate:F

    .line 802
    const/high16 v1, 0x42dc0000    # 110.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherSecondRotate:F

    .line 803
    const/high16 v1, 0x41c80000    # 25.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherThirdRotate:F

    .line 804
    const/high16 v1, -0x3e380000    # -25.0f

    move-object/from16 v0, v20

    iput v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mOtherFourthRotate:F

    .line 806
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_D:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_First:Landroid/graphics/Bitmap;

    .line 807
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_C:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Second:Landroid/graphics/Bitmap;

    .line 808
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_B:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Third:Landroid/graphics/Bitmap;

    .line 809
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mOtherFX_A:Landroid/graphics/Bitmap;

    move-object/from16 v0, v20

    iput-object v1, v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;->mFX_Fourth:Landroid/graphics/Bitmap;

    .line 812
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/4 v2, 0x4

    move-object/from16 v0, v20

    invoke-virtual {v1, v2, v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setMaskInfomation(ILcom/samsung/sgieffect/speaker/SGIEffectSpeaker$MaskInfo;)V

    .line 817
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->createOthersCircleBase()V

    .line 820
    new-instance v18, Landroid/util/DisplayMetrics;

    invoke-direct/range {v18 .. v18}, Landroid/util/DisplayMetrics;-><init>()V

    .line 821
    .local v18, "dm":Landroid/util/DisplayMetrics;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 822
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v18

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v10, v1

    move-object/from16 v0, v18

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v11, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v12, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMask:Landroid/graphics/Bitmap;

    const/4 v13, 0x1

    invoke-virtual/range {v7 .. v13}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addPannel(FFFFLandroid/graphics/Bitmap;Z)V

    .line 828
    const/4 v12, 0x0

    .line 829
    .local v12, "red":F
    const v13, 0x3f46c6c7

    .line 830
    .local v13, "green":F
    const/high16 v14, 0x3f800000    # 1.0f

    .line 831
    .local v14, "blue":F
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    .line 832
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    int-to-float v8, v1

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    int-to-float v9, v1

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-float v10, v1

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v11, v1

    .line 833
    const/high16 v15, 0x3f800000    # 1.0f

    .line 831
    invoke-virtual/range {v7 .. v15}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addPannel(FFFFFFFF)V

    .line 838
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    iget-object v3, v3, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMask:Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v4}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setMaskType(ILandroid/graphics/Bitmap;Z)V

    goto/16 :goto_0
.end method

.method private buildWaveSkin(Landroid/view/View;)V
    .locals 11
    .param p1, "layout"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 843
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    if-nez v0, :cond_0

    .line 889
    :goto_0
    return-void

    .line 845
    :cond_0
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->clearNodes()V

    .line 850
    const/high16 v3, 0x44870000    # 1080.0f

    .line 851
    .local v3, "width":F
    const/high16 v4, 0x44f00000    # 1920.0f

    .line 852
    .local v4, "height":F
    const/4 v5, 0x0

    .line 853
    .local v5, "color":F
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/high16 v8, 0x3f800000    # 1.0f

    move v2, v1

    move v6, v5

    move v7, v5

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addPannel(FFFFFFFF)V

    .line 856
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinWave:Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;->mBitmap:[Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setWaveTextures([Landroid/graphics/Bitmap;)V

    .line 858
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->createWaveTextureAtlas()V

    .line 868
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/16 v1, 0x172

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setWaveMaxSlide(I)V

    .line 869
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setSkinMode(I)V

    .line 880
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/sec/android/sgieffect/R$drawable;->default_sample_color_new200x200:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v10

    .line 881
    .local v10, "is":Ljava/io/InputStream;
    invoke-static {v10}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 883
    .local v9, "bitmap":Landroid/graphics/Bitmap;
    const-string v0, "teja.kim"

    const-string v1, "start_make albumjacket array"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 884
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCollectColor:Lcom/samsung/sgieffect/speaker/findColor/CollectColor;

    const/16 v1, 0xd

    invoke-virtual {v0, v9, v1}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->setImage(Landroid/graphics/Bitmap;I)[I

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mFindColors:[I

    .line 885
    const-string v0, "teja.kim"

    const-string v1, "end_make albumjacket array"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mFindColors:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    invoke-virtual {v1, v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setWaveColors([I)V

    goto :goto_0
.end method


# virtual methods
.method public buildLayoutForClassic()V
    .locals 2

    .prologue
    .line 894
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->loadBgImages(Landroid/content/res/Resources;)V

    .line 895
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->loadSpeakerImages(Landroid/content/res/Resources;)V

    .line 898
    invoke-virtual {p0, p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildViews_Classic(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsoluteLayout;

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutClassic:Landroid/widget/AbsoluteLayout;

    .line 901
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutClassic:Landroid/widget/AbsoluteLayout;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildViews(Landroid/widget/AbsoluteLayout;Z)V

    .line 904
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildViewBasedOnXML_Classic()V

    .line 905
    return-void
.end method

.method public buildLayoutForFuncky()V
    .locals 2

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->loadBgImages(Landroid/content/res/Resources;)V

    .line 911
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->loadSpeakerImages(Landroid/content/res/Resources;)V

    .line 914
    invoke-virtual {p0, p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildViews_Funcky(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsoluteLayout;

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutFuncky:Landroid/widget/AbsoluteLayout;

    .line 917
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutFuncky:Landroid/widget/AbsoluteLayout;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildViews(Landroid/widget/AbsoluteLayout;Z)V

    .line 920
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildViewBasedOnXML_Funcky()V

    .line 921
    return-void
.end method

.method public buildLayoutForWave()V
    .locals 2

    .prologue
    .line 926
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinWave:Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;

    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;->loadBgImages(Landroid/content/res/Resources;)V

    .line 927
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinWave:Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;

    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;->loadSpeakerImages(Landroid/content/res/Resources;)V

    .line 930
    invoke-virtual {p0, p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildViews_Wave(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsoluteLayout;

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutWave:Landroid/widget/AbsoluteLayout;

    .line 933
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutWave:Landroid/widget/AbsoluteLayout;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildViews(Landroid/widget/AbsoluteLayout;Z)V

    .line 936
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildViewBasedOnXML_Wave()V

    .line 937
    return-void
.end method

.method public buildViewBasedOnXML_Classic()V
    .locals 2

    .prologue
    .line 976
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutClassic:Landroid/widget/AbsoluteLayout;

    if-nez v1, :cond_0

    .line 983
    :goto_0
    return-void

    .line 981
    :cond_0
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 982
    .local v0, "vto":Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mListenerClassic:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method public buildViewBasedOnXML_Funcky()V
    .locals 2

    .prologue
    .line 987
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutFuncky:Landroid/widget/AbsoluteLayout;

    if-nez v1, :cond_0

    .line 994
    :goto_0
    return-void

    .line 992
    :cond_0
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 993
    .local v0, "vto":Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mListenerFuncky:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method public buildViewBasedOnXML_Wave()V
    .locals 2

    .prologue
    .line 998
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutWave:Landroid/widget/AbsoluteLayout;

    if-nez v1, :cond_0

    .line 1005
    :goto_0
    return-void

    .line 1003
    :cond_0
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1004
    .local v0, "vto":Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mListenerWave:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method public buildViews(Landroid/widget/AbsoluteLayout;Z)V
    .locals 3
    .param p1, "layout"    # Landroid/widget/AbsoluteLayout;
    .param p2, "isPortraits"    # Z

    .prologue
    const/4 v2, 0x0

    .line 515
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 517
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViewsInLayout()V

    .line 520
    :cond_0
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    .line 523
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCoverView:Landroid/widget/ImageView;

    .line 525
    iget v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCurrentSkin:I

    if-nez v0, :cond_2

    .line 526
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCoverView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBgDiffuse:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 527
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCoverView:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 530
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSplashView:Landroid/widget/ImageView;

    .line 531
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSplashView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    iget-object v1, v1, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mSplashBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 532
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSplashView:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 534
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSplashView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 545
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->createEffectView()Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 546
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCoverView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 548
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 549
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->setContentView(Landroid/view/View;)V

    .line 550
    return-void

    .line 536
    :cond_2
    iget v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCurrentSkin:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 539
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCoverView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 541
    :cond_3
    iget v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCurrentSkin:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 542
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCoverView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public buildViews_Classic(Landroid/content/Context;)Landroid/view/View;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 150
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 151
    .local v0, "factory":Landroid/view/LayoutInflater;
    sget v2, Lcom/sec/android/sgieffect/R$layout;->speaker_layout_classic:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 152
    .local v1, "layoutView":Landroid/view/View;
    return-object v1
.end method

.method public buildViews_Funcky(Landroid/content/Context;)Landroid/view/View;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 160
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 161
    .local v0, "factory":Landroid/view/LayoutInflater;
    sget v2, Lcom/sec/android/sgieffect/R$layout;->speaker_layout_funcky:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 162
    .local v1, "layoutView":Landroid/view/View;
    return-object v1
.end method

.method public buildViews_Wave(Landroid/content/Context;)Landroid/view/View;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 167
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 168
    .local v0, "factory":Landroid/view/LayoutInflater;
    sget v2, Lcom/sec/android/sgieffect/R$layout;->speaker_layout_wave:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 169
    .local v1, "layoutView":Landroid/view/View;
    return-object v1
.end method

.method protected createAudioModules()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 343
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mAudioManager:Landroid/media/AudioManager;

    .line 347
    iget-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMusicPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMusicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v1

    .line 348
    .local v1, "sessionId":I
    :goto_0
    new-instance v2, Landroid/media/audiofx/Equalizer;

    invoke-direct {v2, v3, v1}, Landroid/media/audiofx/Equalizer;-><init>(II)V

    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEqualizer:Landroid/media/audiofx/Equalizer;

    .line 349
    iget-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEqualizer:Landroid/media/audiofx/Equalizer;

    invoke-virtual {v2, v4}, Landroid/media/audiofx/Equalizer;->setEnabled(Z)I

    .line 351
    new-instance v2, Landroid/media/audiofx/Visualizer;

    invoke-direct {v2, v3}, Landroid/media/audiofx/Visualizer;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mVisualizer:Landroid/media/audiofx/Visualizer;

    .line 352
    iget-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mVisualizer:Landroid/media/audiofx/Visualizer;

    invoke-virtual {v2}, Landroid/media/audiofx/Visualizer;->getEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 353
    iget-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mVisualizer:Landroid/media/audiofx/Visualizer;

    invoke-virtual {v2, v3}, Landroid/media/audiofx/Visualizer;->setEnabled(Z)I

    .line 354
    :cond_0
    iget-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mVisualizer:Landroid/media/audiofx/Visualizer;

    invoke-static {}, Landroid/media/audiofx/Visualizer;->getCaptureSizeRange()[I

    move-result-object v3

    aget v3, v3, v4

    div-int/lit8 v3, v3, 0x8

    invoke-virtual {v2, v3}, Landroid/media/audiofx/Visualizer;->setCaptureSize(I)I

    .line 358
    new-instance v0, Lcom/sec/android/sgieffect/sample/SpeakerSample$5;

    invoke-direct {v0, p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample$5;-><init>(Lcom/sec/android/sgieffect/sample/SpeakerSample;)V

    .line 374
    .local v0, "captureListener":Landroid/media/audiofx/Visualizer$OnDataCaptureListener;
    iget-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mVisualizer:Landroid/media/audiofx/Visualizer;

    invoke-static {}, Landroid/media/audiofx/Visualizer;->getMaxCaptureRate()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v0, v3, v4, v4}, Landroid/media/audiofx/Visualizer;->setDataCaptureListener(Landroid/media/audiofx/Visualizer$OnDataCaptureListener;IZZ)I

    .line 375
    iget-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mVisualizer:Landroid/media/audiofx/Visualizer;

    invoke-virtual {v2, v4}, Landroid/media/audiofx/Visualizer;->setEnabled(Z)I

    .line 376
    return-void

    .end local v0    # "captureListener":Landroid/media/audiofx/Visualizer$OnDataCaptureListener;
    .end local v1    # "sessionId":I
    :cond_1
    move v1, v3

    .line 347
    goto :goto_0
.end method

.method public createEffectView()Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;
    .locals 2

    .prologue
    .line 1060
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    if-nez v0, :cond_0

    .line 1061
    new-instance v0, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-direct {v0, p0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    .line 1066
    :goto_0
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v0, p0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setListener(Lcom/samsung/sgieffect/main/SGIEffectListener;)V

    .line 1067
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->initialize()V

    .line 1068
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setZOrderOnTop(Z)V

    .line 1073
    new-instance v0, Lcom/sec/android/sgieffect/sample/SpeakerSample$6;

    invoke-direct {v0, p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample$6;-><init>(Lcom/sec/android/sgieffect/sample/SpeakerSample;)V

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSGSurfaceChangesDrwawnListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;

    .line 1087
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->getSurface()Lcom/samsung/android/sdk/sgi/vi/SGSurface;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSGSurfaceChangesDrwawnListener:Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/sgi/vi/SGSurface;->addChangesDrawnListener(Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;)V

    .line 1089
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    return-object v0

    .line 1063
    :cond_0
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->destroy()V

    goto :goto_0
.end method

.method protected loadBGM()V
    .locals 2

    .prologue
    .line 332
    :try_start_0
    sget v0, Lcom/sec/android/sgieffect/R$raw;->bgm:I

    invoke-static {p0, v0}, Landroid/media/MediaPlayer;->create(Landroid/content/Context;I)Landroid/media/MediaPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMusicPlayer:Landroid/media/MediaPlayer;

    .line 333
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMusicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepare()V

    .line 334
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMusicPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 335
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMusicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 340
    :goto_0
    return-void

    .line 337
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/hardware/Sensor;
    .param p2, "arg1"    # I

    .prologue
    .line 1259
    return-void
.end method

.method public onAnimateHideFinished()V
    .locals 0

    .prologue
    .line 1249
    return-void
.end method

.method public onAnimateShowFinished()V
    .locals 0

    .prologue
    .line 1254
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 1094
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 1095
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 1096
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 1097
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViewsInLayout()V

    .line 1100
    :cond_0
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMusicPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mUseSound:Z

    if-eqz v0, :cond_1

    .line 1101
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMusicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 1103
    :cond_1
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->destroy()V

    .line 1104
    :cond_2
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1011
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1012
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v2, 0x400

    const/4 v3, 0x4

    .line 381
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 386
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->requestWindowFeature(I)Z

    .line 387
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 389
    iget-boolean v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mUseSound:Z

    if-eqz v1, :cond_0

    .line 392
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->loadBGM()V

    .line 393
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->createAudioModules()V

    .line 396
    :cond_0
    const-string v1, "sensor"

    invoke-virtual {p0, v1}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSensorManager:Landroid/hardware/SensorManager;

    .line 397
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSensorManager:Landroid/hardware/SensorManager;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->gravitymeter:Landroid/hardware/Sensor;

    .line 399
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWidth:F

    .line 400
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mHeight:F

    .line 405
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->setSkinMode(I)V

    .line 409
    new-array v1, v3, [F

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mDbValues:[F

    .line 413
    new-array v1, v3, [F

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mPrevDbValues:[F

    .line 414
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-lt v0, v3, :cond_1

    .line 420
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWaveLowPos:Landroid/graphics/Point;

    .line 421
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWaveLowPos:Landroid/graphics/Point;

    const/16 v2, 0x21c

    iput v2, v1, Landroid/graphics/Point;->x:I

    .line 422
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWaveLowPos:Landroid/graphics/Point;

    const/16 v2, 0x5a0

    iput v2, v1, Landroid/graphics/Point;->y:I

    .line 424
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWaveMidPos:Landroid/graphics/Point;

    .line 425
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWaveMidPos:Landroid/graphics/Point;

    const/16 v2, 0x10e

    iput v2, v1, Landroid/graphics/Point;->x:I

    .line 426
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWaveMidPos:Landroid/graphics/Point;

    const/16 v2, 0x3c0

    iput v2, v1, Landroid/graphics/Point;->y:I

    .line 428
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWaveHighPos:Landroid/graphics/Point;

    .line 429
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWaveHighPos:Landroid/graphics/Point;

    const/16 v2, 0x438

    iput v2, v1, Landroid/graphics/Point;->x:I

    .line 430
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWaveHighPos:Landroid/graphics/Point;

    const/4 v2, 0x0

    iput v2, v1, Landroid/graphics/Point;->y:I

    .line 434
    new-instance v1, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;

    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;-><init>(Landroid/content/res/Resources;)V

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCollectColor:Lcom/samsung/sgieffect/speaker/findColor/CollectColor;

    .line 438
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCollectColor:Lcom/samsung/sgieffect/speaker/findColor/CollectColor;

    iget-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mColors:[I

    invoke-virtual {v1, v2}, Lcom/samsung/sgieffect/speaker/findColor/CollectColor;->setDefaultColor([I)V

    .line 439
    return-void

    .line 415
    :cond_1
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mPrevDbValues:[F

    const/4 v2, 0x0

    aput v2, v1, v0

    .line 414
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1108
    const-string v0, "MODE Funcky"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1109
    const-string v0, "MODE Classic"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1110
    const-string v0, "MODE Wave"

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1150
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 472
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    if-eqz v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->finalize()V

    .line 474
    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    .line 477
    :cond_0
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMusicPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 478
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMusicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 479
    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMusicPlayer:Landroid/media/MediaPlayer;

    .line 483
    :cond_1
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    invoke-virtual {v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmaps()V

    .line 484
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    invoke-virtual {v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmaps()V

    .line 485
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinWave:Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;

    invoke-virtual {v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;->releaseBitmaps()V

    .line 488
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mVisualizer:Landroid/media/audiofx/Visualizer;

    if-eqz v0, :cond_2

    .line 489
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mVisualizer:Landroid/media/audiofx/Visualizer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/audiofx/Visualizer;->setEnabled(Z)I

    .line 490
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mVisualizer:Landroid/media/audiofx/Visualizer;

    invoke-virtual {v0}, Landroid/media/audiofx/Visualizer;->release()V

    .line 491
    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mVisualizer:Landroid/media/audiofx/Visualizer;

    .line 495
    :cond_2
    iput-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mDbValues:[F

    .line 497
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 499
    return-void
.end method

.method public onFirstRenderFinished()V
    .locals 2

    .prologue
    .line 1243
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mRemoveSplashHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1244
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1157
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "MODE Funcky"

    if-ne v0, v1, :cond_1

    .line 1158
    iput v3, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mode:I

    .line 1159
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->gravitymeter:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 1160
    invoke-virtual {p0, v2}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->setSkinMode(I)V

    .line 1226
    :cond_0
    :goto_0
    return v2

    .line 1161
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "MODE Classic"

    if-ne v0, v1, :cond_2

    .line 1162
    iput v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mode:I

    .line 1163
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->gravitymeter:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 1164
    invoke-virtual {p0, v3}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->setSkinMode(I)V

    goto :goto_0

    .line 1165
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "MODE Wave"

    if-ne v0, v1, :cond_0

    .line 1166
    iput v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mode:I

    .line 1167
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->gravitymeter:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 1168
    invoke-virtual {p0, v4}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->setSkinMode(I)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 1017
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1018
    iget v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1019
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->gravitymeter:Landroid/hardware/Sensor;

    invoke-virtual {v0, p0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;)V

    .line 1024
    :cond_0
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    if-eqz v0, :cond_1

    .line 1025
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->onPause()V

    .line 1027
    :cond_1
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMusicPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mUseSound:Z

    if-eqz v0, :cond_2

    .line 1028
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMusicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 1029
    :cond_2
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 1035
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 1038
    iget v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1039
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->gravitymeter:Landroid/hardware/Sensor;

    const/4 v2, 0x2

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 1045
    :cond_0
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    if-eqz v0, :cond_1

    .line 1046
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v0}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->onResume()V

    .line 1049
    :cond_1
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMusicPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mUseSound:Z

    if-eqz v0, :cond_2

    .line 1050
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMusicPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 1054
    :cond_2
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mChangedTime:J

    .line 1055
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 14
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1263
    const/4 v4, 0x0

    .line 1265
    .local v4, "isGravityChanged":Z
    iget-object v5, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v5}, Landroid/hardware/Sensor;->getType()I

    move-result v5

    const/16 v6, 0x9

    if-ne v5, v6, :cond_0

    .line 1267
    iget-object v5, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v5, v5, v10

    iget-object v6, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mGravity:[F

    aget v6, v6, v10

    sub-float/2addr v5, v6

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v6, v6, v10

    iget-object v7, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mGravity:[F

    aget v7, v7, v10

    sub-float/2addr v6, v7

    mul-float/2addr v5, v6

    .line 1268
    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v6, v6, v11

    iget-object v7, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mGravity:[F

    aget v7, v7, v11

    sub-float/2addr v6, v7

    iget-object v7, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v7, v7, v11

    iget-object v8, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mGravity:[F

    aget v8, v8, v11

    sub-float/2addr v7, v8

    mul-float/2addr v6, v7

    .line 1267
    add-float/2addr v5, v6

    .line 1269
    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v6, v6, v12

    iget-object v7, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mGravity:[F

    aget v7, v7, v12

    sub-float/2addr v6, v7

    iget-object v7, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v7, v7, v12

    iget-object v8, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mGravity:[F

    aget v8, v8, v12

    sub-float/2addr v7, v8

    mul-float/2addr v6, v7

    .line 1267
    add-float v0, v5, v6

    .line 1271
    .local v0, "fDistance":F
    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    cmpl-double v5, v6, v8

    if-lez v5, :cond_0

    .line 1272
    iget-object v5, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mGravity:[F

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v6, v6, v10

    aput v6, v5, v10

    .line 1273
    iget-object v5, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mGravity:[F

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v6, v6, v11

    aput v6, v5, v11

    .line 1274
    iget-object v5, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mGravity:[F

    iget-object v6, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v6, v6, v12

    aput v6, v5, v12

    .line 1276
    const/4 v4, 0x1

    .line 1281
    .end local v0    # "fDistance":F
    :cond_0
    if-eqz v4, :cond_1

    .line 1283
    iget-object v5, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    if-eqz v5, :cond_1

    .line 1285
    iget-object v5, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mGravity:[F

    aget v5, v5, v10

    sub-float v5, v13, v5

    const/high16 v6, 0x40000000    # 2.0f

    add-float v1, v5, v6

    .line 1286
    .local v1, "initialx":F
    iget-object v5, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mGravity:[F

    aget v5, v5, v11

    add-float v2, v13, v5

    .line 1287
    .local v2, "initialy":F
    iget-object v5, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mGravity:[F

    aget v5, v5, v12

    add-float v3, v13, v5

    .line 1293
    .local v3, "initialz":F
    iget-object v5, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/4 v6, 0x6

    new-array v6, v6, [F

    aput v1, v6, v10

    aput v2, v6, v11

    aput v3, v6, v12

    const/4 v7, 0x3

    aput v13, v6, v7

    const/4 v7, 0x4

    aput v13, v6, v7

    const/4 v7, 0x5

    aput v13, v6, v7

    invoke-virtual {v5, v6, v10}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->validateOnSensorChange([FI)Z

    .line 1297
    .end local v1    # "initialx":F
    .end local v2    # "initialy":F
    .end local v3    # "initialz":F
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 505
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWaveHighPos:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 506
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWaveHighPos:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 508
    invoke-super {p0, p1}, Landroid/app/Activity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setSkinMode(I)V
    .locals 5
    .param p1, "type"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 446
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mChangedTime:J

    .line 447
    if-nez p1, :cond_1

    .line 448
    iput v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCurrentSkin:I

    .line 449
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildLayoutForClassic()V

    .line 463
    :cond_0
    :goto_0
    return-void

    .line 451
    :cond_1
    if-ne p1, v3, :cond_3

    .line 452
    iput v3, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCurrentSkin:I

    .line 455
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    invoke-virtual {v0, v2}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->enableAddBlend(Z)V

    .line 456
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildLayoutForFuncky()V

    goto :goto_0

    .line 458
    :cond_3
    if-ne p1, v4, :cond_0

    .line 459
    iput v4, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCurrentSkin:I

    .line 461
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildLayoutForWave()V

    goto :goto_0
.end method

.method public updateVisualizer([B)V
    .locals 22
    .param p1, "bytes"    # [B

    .prologue
    .line 180
    const-wide/16 v12, 0x0

    .line 181
    .local v12, "max":D
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    if-lt v9, v0, :cond_1

    .line 185
    const-wide/16 v18, 0x0

    cmpl-double v18, v12, v18

    if-lez v18, :cond_0

    .line 186
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v18, v0

    div-double v4, v12, v18

    .line 188
    .local v4, "amplitude":D
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v16

    .line 189
    .local v16, "rms":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->accRMS:D

    move-wide/from16 v18, v0

    add-double v18, v18, v16

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/sgieffect/sample/SpeakerSample;->accRMS:D

    .line 190
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->count:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/sgieffect/sample/SpeakerSample;->count:I

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->prevTime:Ljava/util/Calendar;

    move-object/from16 v18, v0

    if-nez v18, :cond_2

    .line 193
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/sgieffect/sample/SpeakerSample;->prevTime:Ljava/util/Calendar;

    .line 254
    .end local v4    # "amplitude":D
    .end local v16    # "rms":D
    :cond_0
    :goto_1
    return-void

    .line 182
    :cond_1
    aget-byte v18, p1, v9

    aget-byte v19, p1, v9

    mul-int v18, v18, v19

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v18, v0

    add-double v12, v12, v18

    .line 181
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 196
    .restart local v4    # "amplitude":D
    .restart local v16    # "rms":D
    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 197
    .local v8, "curTime":Ljava/util/Calendar;
    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->prevTime:Ljava/util/Calendar;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v20

    sub-long v10, v18, v20

    .line 198
    .local v10, "deltaTime":J
    const-wide/16 v18, 0x0

    cmp-long v18, v10, v18

    if-eqz v18, :cond_0

    .line 201
    const-wide/16 v18, 0x3e8

    div-long v18, v18, v10

    move-wide/from16 v0, v18

    long-to-float v6, v0

    .line 202
    .local v6, "aproxFps":F
    const/high16 v18, 0x41a00000    # 20.0f

    cmpl-float v18, v6, v18

    if-gtz v18, :cond_0

    .line 205
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->prevTime:Ljava/util/Calendar;

    .line 208
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->accRMS:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->count:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-double v0, v0

    move-wide/from16 v20, v0

    div-double v18, v18, v20

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mRMS:D

    .line 209
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/sgieffect/sample/SpeakerSample;->accRMS:D

    .line 210
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/sgieffect/sample/SpeakerSample;->count:I

    .line 212
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mRMS:D

    move-wide/from16 v18, v0

    const-wide/high16 v20, 0x4060000000000000L    # 128.0

    sub-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->abs(D)D

    move-result-wide v18

    const-wide/high16 v20, 0x4050000000000000L    # 64.0

    div-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-float v15, v0

    .line 215
    .local v15, "ratio":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    move-object/from16 v18, v0

    if-eqz v18, :cond_0

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setDecibelRatio(F)V

    .line 227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mMovementScale:F

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setMovementScale(F)V

    .line 231
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setTimeDelta(J)V

    .line 235
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mChangedTime:J

    move-wide/from16 v18, v0

    add-long v18, v18, v10

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mChangedTime:J

    .line 236
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCurrentSkin:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mChangedTime:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x1f40

    cmp-long v18, v18, v20

    if-lez v18, :cond_0

    const v18, 0x3f19999a    # 0.6f

    cmpl-float v18, v15, v18

    if-lez v18, :cond_0

    .line 238
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCurrentMask:I

    .line 239
    .local v7, "curMask":I
    new-instance v14, Ljava/util/Random;

    invoke-direct {v14}, Ljava/util/Random;-><init>()V

    .line 241
    .local v14, "random":Ljava/util/Random;
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCurrentMask:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-eq v7, v0, :cond_3

    .line 245
    move-object/from16 v0, p0

    iput v7, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mCurrentMask:I

    .line 247
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    move-object/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v7}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->changeBgImage(Landroid/content/res/Resources;I)V

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->mMask:Landroid/graphics/Bitmap;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v7, v1, v2}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setMaskType(ILandroid/graphics/Bitmap;Z)V

    .line 251
    const-wide/16 v18, 0x0

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mChangedTime:J

    goto/16 :goto_1

    .line 242
    :cond_3
    const/16 v18, 0x5

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v7

    goto :goto_2
.end method

.method public updateVisualizerFFT([B)V
    .locals 14
    .param p1, "bytes"    # [B

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v11, 0x1

    const/high16 v2, 0x42c80000    # 100.0f

    .line 259
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mDbValues:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    if-nez v0, :cond_1

    .line 326
    :cond_0
    return-void

    .line 264
    :cond_1
    const/4 v9, 0x0

    .line 265
    .local v9, "magnitude":F
    const/4 v6, 0x0

    .line 267
    .local v6, "dbValue":I
    array-length v0, p1

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    .line 271
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-lt v7, v13, :cond_5

    .line 287
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mDbValues:[F

    const/4 v3, 0x0

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setMusiBase(F)V

    .line 289
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mDbValues:[F

    aget v1, v1, v11

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setMusiLow(F)V

    .line 291
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mDbValues:[F

    const/4 v3, 0x2

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setMusiMid(F)V

    .line 293
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mDbValues:[F

    aget v1, v1, v12

    invoke-virtual {v0, v1}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->setMusiHigh(F)V

    .line 299
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->prevTimeFFT:J

    sub-long/2addr v0, v4

    const-wide/16 v4, 0x12c

    cmp-long v0, v0, v4

    if-lez v0, :cond_4

    .line 301
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mDbValues:[F

    aget v0, v0, v11

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mPrevDbValues:[F

    aget v1, v1, v11

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 303
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/high16 v1, 0x437a0000    # 250.0f

    iget-object v3, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWaveLowPos:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWaveLowPos:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    const/high16 v5, 0x42600000    # 56.0f

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addLowWave(FFFFF)V

    .line 315
    :cond_2
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mDbValues:[F

    aget v0, v0, v12

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mPrevDbValues:[F

    aget v1, v1, v12

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 317
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mEffectView:Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;

    const/high16 v1, 0x42f00000    # 120.0f

    iget-object v3, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWaveHighPos:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mWaveHighPos:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    const/high16 v5, 0x42d20000    # 105.0f

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;->addHighWave(FFFFF)V

    .line 320
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->prevTimeFFT:J

    .line 323
    :cond_4
    const/4 v7, 0x0

    :goto_1
    if-ge v7, v13, :cond_0

    .line 324
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mPrevDbValues:[F

    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mDbValues:[F

    aget v1, v1, v7

    aput v1, v0, v7

    .line 323
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 272
    :cond_5
    mul-int/lit8 v0, v7, 0x2

    add-int/lit8 v0, v0, 0x2

    aget-byte v10, p1, v0

    .line 273
    .local v10, "rfk":B
    mul-int/lit8 v0, v7, 0x2

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x2

    aget-byte v8, p1, v0

    .line 274
    .local v8, "ifk":B
    mul-int v0, v10, v10

    mul-int v1, v8, v8

    add-int/2addr v0, v1

    int-to-float v9, v0

    .line 276
    const/4 v6, 0x0

    .line 278
    const/4 v0, 0x0

    cmpl-float v0, v9, v0

    if-eqz v0, :cond_6

    .line 279
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    float-to-double v4, v9

    invoke-static {v4, v5}, Ljava/lang/Math;->log10(D)D

    move-result-wide v4

    mul-double/2addr v0, v4

    double-to-int v6, v0

    .line 282
    :cond_6
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample;->mDbValues:[F

    int-to-float v1, v6

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v1, v3

    div-float/2addr v1, v2

    aput v1, v0, v7

    .line 271
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0
.end method
