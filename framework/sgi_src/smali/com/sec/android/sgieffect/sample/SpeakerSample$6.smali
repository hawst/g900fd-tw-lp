.class Lcom/sec/android/sgieffect/sample/SpeakerSample$6;
.super Ljava/lang/Object;
.source "SpeakerSample.java"

# interfaces
.implements Lcom/samsung/android/sdk/sgi/vi/SGSurfaceChangesDrawnListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/sgieffect/sample/SpeakerSample;->createEffectView()Lcom/samsung/sgieffect/speaker/SGIEffectSpeaker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;


# direct methods
.method constructor <init>(Lcom/sec/android/sgieffect/sample/SpeakerSample;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample$6;->this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;

    .line 1073
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChangesDrawn()V
    .locals 2

    .prologue
    .line 1078
    const/4 v0, 0x0

    .line 1079
    .local v0, "a":I
    add-int/lit8 v0, v0, 0x1

    .line 1081
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample$6;->this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;

    # getter for: Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinClassic:Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;
    invoke-static {v1}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->access$11(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmaps()V

    .line 1082
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample$6;->this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;

    # getter for: Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinFuncky:Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;
    invoke-static {v1}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->access$12(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/sgieffect/sample/SpeakerSkinFuncky;->releaseBitmaps()V

    .line 1083
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample$6;->this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;

    # getter for: Lcom/sec/android/sgieffect/sample/SpeakerSample;->mSkinWave:Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;
    invoke-static {v1}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->access$13(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;->releaseBitmaps()V

    .line 1084
    return-void
.end method
