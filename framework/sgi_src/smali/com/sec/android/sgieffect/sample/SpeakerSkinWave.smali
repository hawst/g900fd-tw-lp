.class public Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;
.super Lcom/sec/android/sgieffect/sample/SpeakerSkin;
.source "SpeakerSkinWave.java"


# instance fields
.field public mBitmap:[Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSkin;-><init>()V

    .line 12
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;->mBitmap:[Landroid/graphics/Bitmap;

    .line 10
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 0

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;->releaseBitmaps()V

    .line 59
    return-void
.end method

.method public loadBgImages(Landroid/content/res/Resources;)V
    .locals 5
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 16
    invoke-super {p0, p1}, Lcom/sec/android/sgieffect/sample/SpeakerSkin;->loadSpeakerImages(Landroid/content/res/Resources;)V

    .line 17
    const/4 v1, 0x0

    .line 18
    .local v1, "is":Ljava/io/InputStream;
    const/16 v3, 0xa

    new-array v2, v3, [I

    const/4 v3, 0x0

    .line 19
    sget v4, Lcom/sec/android/sgieffect/R$drawable;->particle_png_000:I

    aput v4, v2, v3

    const/4 v3, 0x1

    .line 20
    sget v4, Lcom/sec/android/sgieffect/R$drawable;->particle_png_001:I

    aput v4, v2, v3

    const/4 v3, 0x2

    .line 21
    sget v4, Lcom/sec/android/sgieffect/R$drawable;->particle_png_002:I

    aput v4, v2, v3

    const/4 v3, 0x3

    .line 22
    sget v4, Lcom/sec/android/sgieffect/R$drawable;->particle_png_003:I

    aput v4, v2, v3

    const/4 v3, 0x4

    .line 23
    sget v4, Lcom/sec/android/sgieffect/R$drawable;->particle_png_004:I

    aput v4, v2, v3

    const/4 v3, 0x5

    .line 24
    sget v4, Lcom/sec/android/sgieffect/R$drawable;->particle_png_005:I

    aput v4, v2, v3

    const/4 v3, 0x6

    .line 25
    sget v4, Lcom/sec/android/sgieffect/R$drawable;->particle_png_006:I

    aput v4, v2, v3

    const/4 v3, 0x7

    .line 26
    sget v4, Lcom/sec/android/sgieffect/R$drawable;->particle_png_007:I

    aput v4, v2, v3

    const/16 v3, 0x8

    .line 27
    sget v4, Lcom/sec/android/sgieffect/R$drawable;->particle_png_008:I

    aput v4, v2, v3

    const/16 v3, 0x9

    .line 28
    sget v4, Lcom/sec/android/sgieffect/R$drawable;->particle_png_009:I

    aput v4, v2, v3

    .line 30
    .local v2, "testImage":[I
    array-length v3, v2

    new-array v3, v3, [Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;->mBitmap:[Landroid/graphics/Bitmap;

    .line 31
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-lt v0, v3, :cond_0

    .line 35
    return-void

    .line 32
    :cond_0
    aget v3, v2, v0

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 33
    iget-object v3, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;->mBitmap:[Landroid/graphics/Bitmap;

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v4

    aput-object v4, v3, v0

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public loadSpeakerImages(Landroid/content/res/Resources;)V
    .locals 0
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/sec/android/sgieffect/sample/SpeakerSkin;->loadSpeakerImages(Landroid/content/res/Resources;)V

    .line 40
    return-void
.end method

.method public releaseBitmaps()V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSkin;->releaseBitmaps()V

    .line 46
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;->mBitmap:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 47
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;->mBitmap:[Landroid/graphics/Bitmap;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    .line 50
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;->mBitmap:[Landroid/graphics/Bitmap;

    .line 52
    .end local v0    # "i":I
    :cond_0
    return-void

    .line 48
    .restart local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;->mBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {p0, v1}, Lcom/sec/android/sgieffect/sample/SpeakerSkinWave;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
