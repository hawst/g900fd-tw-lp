.class Lcom/sec/android/sgieffect/sample/SpeakerSample$3;
.super Ljava/lang/Object;
.source "SpeakerSample.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/sgieffect/sample/SpeakerSample;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;


# direct methods
.method constructor <init>(Lcom/sec/android/sgieffect/sample/SpeakerSample;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample$3;->this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;

    .line 964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .prologue
    .line 966
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample$3;->this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;

    iget-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample$3;->this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;

    # getter for: Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutWave:Landroid/widget/AbsoluteLayout;
    invoke-static {v2}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->access$7(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Landroid/widget/AbsoluteLayout;

    move-result-object v2

    # invokes: Lcom/sec/android/sgieffect/sample/SpeakerSample;->buildWaveSkin(Landroid/view/View;)V
    invoke-static {v1, v2}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->access$8(Lcom/sec/android/sgieffect/sample/SpeakerSample;Landroid/view/View;)V

    .line 967
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample$3;->this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;

    # getter for: Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->access$2(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 968
    .local v0, "vto":Landroid/view/ViewTreeObserver;
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample$3;->this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;

    # getter for: Lcom/sec/android/sgieffect/sample/SpeakerSample;->mListenerWave:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    invoke-static {v1}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->access$9(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 969
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample$3;->this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;

    # getter for: Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayout:Landroid/widget/FrameLayout;
    invoke-static {v1}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->access$2(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Landroid/widget/FrameLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/sgieffect/sample/SpeakerSample$3;->this$0:Lcom/sec/android/sgieffect/sample/SpeakerSample;

    # getter for: Lcom/sec/android/sgieffect/sample/SpeakerSample;->mLayoutWave:Landroid/widget/AbsoluteLayout;
    invoke-static {v2}, Lcom/sec/android/sgieffect/sample/SpeakerSample;->access$7(Lcom/sec/android/sgieffect/sample/SpeakerSample;)Landroid/widget/AbsoluteLayout;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 970
    return-void
.end method
