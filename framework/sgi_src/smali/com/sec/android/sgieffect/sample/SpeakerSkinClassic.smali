.class public Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;
.super Lcom/sec/android/sgieffect/sample/SpeakerSkin;
.source "SpeakerSkinClassic.java"


# instance fields
.field public mBaseDiffuse:Landroid/graphics/Bitmap;

.field public mBaseNormal:Landroid/graphics/Bitmap;

.field public mBaseWeight:Landroid/graphics/Bitmap;

.field public mBgDiffuse:Landroid/graphics/Bitmap;

.field public mCenterDiffuse:Landroid/graphics/Bitmap;

.field public mCenterNormal:Landroid/graphics/Bitmap;

.field public mCenterWeight:Landroid/graphics/Bitmap;

.field public mRubberInsideDiffuse:Landroid/graphics/Bitmap;

.field public mRubberInsideNormal:Landroid/graphics/Bitmap;

.field public mRubberInsideWeight:Landroid/graphics/Bitmap;

.field public mRubberOutsideDiffuse:Landroid/graphics/Bitmap;

.field public mRubberOutsideNormal:Landroid/graphics/Bitmap;

.field public mRubberOutsideWeight:Landroid/graphics/Bitmap;

.field public mSplashBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSkin;-><init>()V

    .line 12
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBgDiffuse:Landroid/graphics/Bitmap;

    .line 14
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBaseDiffuse:Landroid/graphics/Bitmap;

    .line 15
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBaseNormal:Landroid/graphics/Bitmap;

    .line 16
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBaseWeight:Landroid/graphics/Bitmap;

    .line 18
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mCenterDiffuse:Landroid/graphics/Bitmap;

    .line 19
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mCenterNormal:Landroid/graphics/Bitmap;

    .line 20
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mCenterWeight:Landroid/graphics/Bitmap;

    .line 22
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberInsideDiffuse:Landroid/graphics/Bitmap;

    .line 23
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberInsideNormal:Landroid/graphics/Bitmap;

    .line 24
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberInsideWeight:Landroid/graphics/Bitmap;

    .line 26
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberOutsideDiffuse:Landroid/graphics/Bitmap;

    .line 27
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberOutsideNormal:Landroid/graphics/Bitmap;

    .line 28
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberOutsideWeight:Landroid/graphics/Bitmap;

    .line 30
    iput-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mSplashBitmap:Landroid/graphics/Bitmap;

    .line 11
    return-void
.end method


# virtual methods
.method public finalize()V
    .locals 0

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmaps()V

    .line 111
    return-void
.end method

.method public loadBgImages(Landroid/content/res/Resources;)V
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 34
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBgDiffuse:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 43
    :goto_0
    return-void

    .line 36
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/sgieffect/sample/SpeakerSkin;->loadSpeakerImages(Landroid/content/res/Resources;)V

    .line 38
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->speaker_bg_diffuse:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 39
    .local v0, "is":Ljava/io/InputStream;
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBgDiffuse:Landroid/graphics/Bitmap;

    .line 41
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->speaker_splash:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 42
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mSplashBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public loadSpeakerImages(Landroid/content/res/Resources;)V
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 48
    iget-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBaseDiffuse:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 81
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/sgieffect/sample/SpeakerSkin;->loadSpeakerImages(Landroid/content/res/Resources;)V

    .line 54
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->speaker_base_diffuse:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 55
    .local v0, "is":Ljava/io/InputStream;
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBaseDiffuse:Landroid/graphics/Bitmap;

    .line 56
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->speaker_base_normal:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 57
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBaseNormal:Landroid/graphics/Bitmap;

    .line 58
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->speaker_base_weight:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 59
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBaseWeight:Landroid/graphics/Bitmap;

    .line 61
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->speaker_center_diffuse:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 62
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mCenterDiffuse:Landroid/graphics/Bitmap;

    .line 63
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->speaker_center_normal:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 64
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mCenterNormal:Landroid/graphics/Bitmap;

    .line 65
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->speaker_center_weight:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 66
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mCenterWeight:Landroid/graphics/Bitmap;

    .line 68
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->speaker_rubber_outside_diffuse:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 69
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberOutsideDiffuse:Landroid/graphics/Bitmap;

    .line 70
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->speaker_rubber_outside_normal:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 71
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberOutsideNormal:Landroid/graphics/Bitmap;

    .line 72
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->speaker_rubber_outside_weight:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 73
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberOutsideWeight:Landroid/graphics/Bitmap;

    .line 75
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->speaker_rubber_inside_diffuse:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 76
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberInsideDiffuse:Landroid/graphics/Bitmap;

    .line 77
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->speaker_rubber_inside_normal:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 78
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberInsideNormal:Landroid/graphics/Bitmap;

    .line 79
    sget v1, Lcom/sec/android/sgieffect/R$drawable;->speaker_rubber_inside_weight:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    .line 80
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberInsideWeight:Landroid/graphics/Bitmap;

    goto/16 :goto_0
.end method

.method public releaseBitmaps()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Lcom/sec/android/sgieffect/sample/SpeakerSkin;->releaseBitmaps()V

    .line 89
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mSplashBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBgDiffuse:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBaseDiffuse:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 93
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBaseNormal:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mBaseWeight:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mCenterDiffuse:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mCenterNormal:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 97
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mCenterWeight:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 98
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberOutsideDiffuse:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 99
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberOutsideNormal:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 100
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberOutsideWeight:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 101
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberInsideDiffuse:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 102
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberInsideNormal:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 103
    iget-object v0, p0, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->mRubberInsideWeight:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/sec/android/sgieffect/sample/SpeakerSkinClassic;->releaseBitmap(Landroid/graphics/Bitmap;)V

    .line 104
    return-void
.end method
