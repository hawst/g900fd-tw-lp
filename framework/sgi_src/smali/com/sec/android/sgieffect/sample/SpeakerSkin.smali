.class public Lcom/sec/android/sgieffect/sample/SpeakerSkin;
.super Ljava/lang/Object;
.source "SpeakerSkin.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public loadBgImages(Landroid/content/res/Resources;)V
    .locals 0
    .param p1, "aRes"    # Landroid/content/res/Resources;

    .prologue
    .line 14
    return-void
.end method

.method public loadSpeakerImages(Landroid/content/res/Resources;)V
    .locals 0
    .param p1, "aRes"    # Landroid/content/res/Resources;

    .prologue
    .line 10
    return-void
.end method

.method protected releaseBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "aBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 21
    if-eqz p1, :cond_0

    .line 22
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 23
    const/4 p1, 0x0

    .line 25
    :cond_0
    return-void
.end method

.method public releaseBitmaps()V
    .locals 0

    .prologue
    .line 18
    return-void
.end method
