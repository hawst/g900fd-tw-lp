#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

varying vec3 lightDir;
varying vec3 eyeVec;
varying vec3 eyeSpaceNormal;
varying vec2 vTexCoords;
varying vec4 vColour;

uniform sampler2D SGTexture;
uniform vec4 SGTextureRect;
uniform vec4 SGColor;

uniform float SGShininess;

void main() {
//    vec3 norm = eyeSpaceNormal;
//    norm.z = abs(eyeSpaceNormal.z);

    vec2 scaledCoord = SGTextureRect.xy + (vTexCoords * SGTextureRect.zw);
    vec4 texcolor = texture2D(SGTexture, scaledCoord);
    vec3 precolor = SGColor.rgb * (texcolor.a * vColour.a);

//    float lightPower = max(dot(norm, vec3(0.0, 0.0, 1.0)), SGShininess);
    float lightPower = max(abs(eyeSpaceNormal.z), SGShininess);
    texcolor.rgb = texcolor.rgb * lightPower;

    vec3 resultColor = texcolor.rgb * vColour.rgb * precolor.rgb;
    gl_FragColor = vec4(resultColor, texcolor.a * vColour.a) * SGColor.a;
}