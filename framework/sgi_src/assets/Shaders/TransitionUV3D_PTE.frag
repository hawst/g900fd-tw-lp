#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

varying vec3 lightDir;
varying vec3 eyeVec;
varying vec3 eyeSpaceNormal;
varying vec2 vTexCoords;

uniform sampler2D SGTexture;
uniform vec4 SGColor;
uniform float SGShininess;

void main() {
    vec2 scaledCoord = vTexCoords;
    vec4 texcolor = texture2D(SGTexture, scaledCoord);
    vec3 precolor = SGColor.rgb * texcolor.a;

    float lightPower = max(abs(eyeSpaceNormal.z), SGShininess);
    texcolor.rgb = texcolor.rgb * lightPower;

    gl_FragColor = vec4(texcolor.rgb * precolor.rgb, texcolor.a) * SGColor.a;
}