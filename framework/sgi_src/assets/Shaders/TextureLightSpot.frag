#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

varying vec2 vTexCoords;

uniform sampler2D SGTexture;
uniform vec4 SGColor;
uniform vec4 SGLightColor;
uniform vec4 SGLightFactor;

void main() {
    vec2 lightOffset = SGLightFactor.xy;
    vec2 lightRadius = SGLightFactor.zw;
    vec4 texColor = texture2D(SGTexture, vTexCoords);

    float dist = distance(vTexCoords, lightOffset);
    texColor.rgb = texColor.rgb + (SGLightColor.rgb * smoothstep(lightRadius.x, lightRadius.y, dist)* SGLightColor.a) * texColor.a;

    gl_FragColor = vec4((texColor.rgb * SGColor.rgb) * SGColor.a, texColor.a * SGColor.a);
}