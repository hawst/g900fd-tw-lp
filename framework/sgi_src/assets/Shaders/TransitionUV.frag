#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

varying vec2 vTexCoords;

uniform vec4 SGColor;
uniform vec4 SGTextureRect;
uniform sampler2D SGTexture;

void main() {
    vec2 scaledCoord = SGTextureRect.xy + (vTexCoords * SGTextureRect.zw);
    vec4 texColor = texture2D(SGTexture, scaledCoord);
    gl_FragColor = vec4(texColor.rgb * SGColor.rgb, texColor.a) * SGColor.a;
}