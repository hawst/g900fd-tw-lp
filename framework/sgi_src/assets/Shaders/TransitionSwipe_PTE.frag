#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

varying vec3 lightDir;
varying vec3 eyeVec;
varying vec3 eyeSpaceNormal;
varying vec2 vTexCoords;
varying float vTexCoords_y;

uniform vec4 SGColor;
uniform vec3 SGSwipe;
uniform sampler2D SGTexture;

float uShininess = 1.0;

void main() {
    vec2 scaledCoord = vTexCoords;
    vec4 texcolor = texture2D(SGTexture, scaledCoord);

    if (uShininess <= 0.4) texcolor = max(texcolor, 0.5);

    float lightPower = max(abs(eyeSpaceNormal.z), uShininess);
    texcolor.rgb = texcolor.rgb * lightPower;
    float salpha = abs(SGSwipe.z - smoothstep(SGSwipe.x,SGSwipe.x+SGSwipe.y,vTexCoords_y));
    vec3 precolor = SGColor.rgb * (texcolor.a * salpha); 
    gl_FragColor = vec4(texcolor.rgb * precolor, texcolor.a * salpha) * SGColor.a;
}