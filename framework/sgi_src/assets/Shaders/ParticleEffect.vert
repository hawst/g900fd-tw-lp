#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

uniform mat4 SGWorldViewProjection;
uniform mat4 SGWorldInverseTranspose;

attribute vec3 SGPositions;
attribute vec4 SGColors;
attribute vec2 SGTextureCoords;

varying vec4 v_color;
varying vec2 v_texCoord;

void main() {
    v_texCoord = SGTextureCoords;
    v_color = SGColors;
    gl_Position = SGWorldViewProjection * vec4(SGPositions, 1.0);
}
