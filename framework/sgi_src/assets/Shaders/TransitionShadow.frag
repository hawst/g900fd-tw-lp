#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

varying vec2 vTexCoords;
varying vec3 lightDir;
varying vec3 eyeVec;
varying vec3 eyeSpaceNormal;
uniform vec4 SGColor;
uniform vec4 SGTextureRect;
uniform sampler2D SGTexture;
float shadowWidth = 0.02;

void main() {
    //vec2 scaledCoord = SGTextureRect.xy + (vTexCoords * SGTextureRect.zw);
    vec4 texcolor = texture2D(SGTexture, vec2(0.01,0.01));

    float d = min(min(vTexCoords.x, 1.0 - vTexCoords.x), min(vTexCoords.y, 1.0 - vTexCoords.y)) / shadowWidth;
    float shadowAlpha = SGColor.a * (1.0-step(1.0,d)) * d;

    vec3 precolor = SGColor.rgb * (texcolor.a * shadowAlpha);    
    gl_FragColor = vec4(texcolor.rgb * precolor, shadowAlpha * texcolor.a) * SGColor.a;
}
