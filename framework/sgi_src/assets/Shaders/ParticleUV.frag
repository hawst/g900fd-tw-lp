#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

uniform sampler2D SGTexture;

varying vec2 v_texCoord;
varying vec4 v_color;

void main() {
    gl_FragColor = texture2D(SGTexture, v_texCoord) * v_color;
}