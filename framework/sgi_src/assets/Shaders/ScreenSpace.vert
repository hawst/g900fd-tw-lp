#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

attribute vec2 SGPositions;
attribute vec2 SGTextureCoords;

varying vec2 vTexCoords;

void main() {
    vTexCoords = SGTextureCoords;
    gl_Position = vec4(SGPositions, 0.0, 1.0);
}