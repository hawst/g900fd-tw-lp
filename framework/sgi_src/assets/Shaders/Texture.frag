#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

varying vec2 vTexCoords;

uniform sampler2D SGTexture;

void main() {
    gl_FragColor = texture2D(SGTexture, vTexCoords);
}