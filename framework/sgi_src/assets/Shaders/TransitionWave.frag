#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

varying vec3 lightDir;
varying vec3 eyeVec;
varying vec3 eyeSpaceNormal;
varying vec2 vTexCoords;

uniform vec4 SGColor;
uniform vec4 SGTextureRect;
uniform vec3 SGSwipe;
uniform float SGTime;
uniform float SGRatio;
uniform sampler2D SGTexture;
uniform vec2 SGCenter;
uniform vec2 SGWidgetRatio;

float uShininess = 1.0;

void main() {
    vec2 tc = vTexCoords.xy;
    vec2 p = tc - SGCenter;

    float len = max(0.05,length(p));
    vec2 filter = (p/len)*cos(len*12.0-SGTime*4.0)*0.03*SGRatio;
    filter *= SGWidgetRatio;
    vec2 uv = tc + filter;

    vec4 texcolor = texture2D(SGTexture,uv);
    float a = step(0.0, uv.x) * step(0.0, uv.y) * step(-1.0, -uv.x) * step(-1.0, -uv.y);

    gl_FragColor = vec4(texcolor.rgb * SGColor.rgb, texcolor.a) * SGColor.a * a;
}