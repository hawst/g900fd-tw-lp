#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

uniform float SGOpacity;

void main() {
    gl_FragColor = vec4(0.5, 0.5, 0.5, 1.0);
    gl_FragColor.a *= SGOpacity;
}