#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif


varying vec3 lightDir;
varying vec3 eyeVec;
varying vec3 eyeSpaceNormal;
varying vec2 vTexCoords;

uniform vec4 SGColor;
uniform vec4 SGTextureRect;
uniform vec3 SGSwipe;
uniform sampler2D SGTexture;
uniform vec4 SGLightColor;
uniform vec4 SGLightFactor;

float uShininess = 1.0;

void main() {
    float dist  = 0.0;
    float dist2 = 0.0;
    float dist3 = 0.0;

    if (SGLightFactor.z != 0.0 || SGLightFactor.w != 0.0) {
        vec2 lightOffset = SGLightFactor.xy;
        lightOffset.y -= 1.0;
        vec2 lightRadius = SGLightFactor.zw;
        dist = smoothstep(lightRadius.x, lightRadius.y, distance(vTexCoords, lightOffset));

        vec2 lightOffset2 = SGLightFactor.xy;
        lightOffset2.y += 1.0;
        vec2 lightRadius2 = SGLightFactor.zw;
        dist2 = smoothstep(lightRadius2.x, lightRadius2.y, distance(vTexCoords, lightOffset2));

        float t1 = abs(vTexCoords.x - lightOffset.x);
        float t2 = abs(vTexCoords.x - lightOffset2.x);
        dist3 = smoothstep(lightRadius.x * 0.12, lightRadius.y * 0.12, t1);
    }

//    vec3 norm = eyeSpaceNormal;
//    norm.z = abs(eyeSpaceNormal.z);
    vec2 scaledCoord = SGTextureRect.xy + (vTexCoords * SGTextureRect.zw);
    vec4 texcolor = texture2D(SGTexture, scaledCoord);
    texcolor.rgb = texcolor.rgb + (SGLightColor.rgb * dist * SGLightColor.a) * texcolor.a;
    texcolor.rgb += (SGLightColor.rgb * dist2 * SGLightColor.a) * texcolor.a;
    texcolor.rgb += SGLightColor.rgb * dist3 * SGLightColor.a * texcolor.a * 0.2;
    
    if (uShininess <= 0.4) texcolor = max(texcolor, 0.5);
//    float lightPower = max(dot(norm, vec3(0.0, 0.0, 1.0)), uShininess);
    float lightPower = max(abs(eyeSpaceNormal.z), uShininess);
    texcolor.rgb = texcolor.rgb * lightPower;
    float salpha = abs(SGSwipe.z - smoothstep(SGSwipe.x,SGSwipe.x+SGSwipe.y,vTexCoords.x));
    
    vec3 precolor = SGColor.rgb * (texcolor.a * salpha);
    gl_FragColor = vec4(texcolor.rgb * precolor, salpha * texcolor.a) * SGColor.a;
}