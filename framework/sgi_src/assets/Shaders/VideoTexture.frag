#extension GL_OES_EGL_image_external : require

#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

uniform samplerExternalOES SGTexture;
uniform float SGOpacity;

varying vec2 vTexCoords;

void main() {
    gl_FragColor = texture2D(SGTexture, vTexCoords);
    gl_FragColor.a *= SGOpacity;
}