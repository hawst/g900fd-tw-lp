#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

varying vec3 lightDir;
varying vec3 eyeVec;
varying vec3 eyeSpaceNormal;
varying vec2 vTexCoords;
uniform vec4 SGColor;
uniform vec4 SGTextureRect;
uniform vec3 SGSwipe;
uniform sampler2D SGTexture;
float uShininess = 1.0;

void main() {
//    vec3 norm = eyeSpaceNormal;
//    norm.z = abs(eyeSpaceNormal.z);
    vec2 scaledCoord = SGTextureRect.xy + (vTexCoords * SGTextureRect.zw);
    vec4 texcolor = texture2D(SGTexture, scaledCoord);

    if (uShininess <= 0.4) texcolor = max(texcolor, 0.5);
//    float lightPower = max(dot(norm, vec3(0.0, 0.0, 1.0)), uShininess);
    float lightPower = max(abs(eyeSpaceNormal.z), uShininess);
    texcolor.rgb = texcolor.rgb * lightPower;
    float salpha = abs(SGSwipe.z - smoothstep(SGSwipe.x,SGSwipe.x+SGSwipe.y,vTexCoords.y));
    gl_FragColor = vec4(texcolor.rgb * SGColor.rgb * salpha, texcolor.a * salpha) * SGColor.a;
}