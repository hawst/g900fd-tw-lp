#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

varying vec2 vTexCoords;

uniform sampler2D SGTexture;
uniform vec4 SGColor;
uniform vec4 SGLightColor;
uniform vec4 SGLightFactor;

void main() {
    vec2 lightOffset = SGLightFactor.xy;
    vec2 lightTarget = SGLightFactor.zw;

    float magnitude = distance(lightTarget, lightOffset);
    vec4 texColor = texture2D(SGTexture, vTexCoords);

    float distance = (vTexCoords.x * (lightTarget.x - lightOffset.x) - (lightOffset.x * lightTarget.x) + (lightOffset.x * lightOffset.x) +
                      vTexCoords.y * (lightTarget.y - lightOffset.y) - (lightOffset.y * lightTarget.y) + (lightOffset.x * lightOffset.y)) / magnitude;

    float ratio = abs((2.0 * distance - distance*distance) / (2.0 * magnitude - magnitude*magnitude));

    texColor.rgb = texColor.rgb + (SGLightColor.rgb * (1.0 - min(1.0, ratio)) * SGLightColor.a) * texColor.a;

    gl_FragColor = vec4((texColor.rgb * SGColor.rgb) * SGColor.a, texColor.a * SGColor.a);
}