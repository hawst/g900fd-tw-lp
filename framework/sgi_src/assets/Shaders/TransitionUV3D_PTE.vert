#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

uniform mat4 SGWorldViewProjection;
uniform mat4 SGWorldInverseTranspose;
uniform vec4 SGTextureRect;

attribute vec3 SGPositions;
attribute vec3 SGNormals;
attribute vec2 SGTextureCoords;

varying vec2 vTexCoords;
varying vec3 lightDir;
varying vec3 eyeVec;
varying vec3 eyeSpaceNormal;

varying float vTexCoords_y;

void main() {
    lightDir = vec3(0.025, 0.025, 0.9994);
    eyeVec = vec3(-0.7071, 0.0, -0.7071);
    eyeSpaceNormal = SGNormals;

    vTexCoords_y = SGTextureCoords.y;
    vTexCoords = SGTextureRect.xy + (SGTextureCoords * SGTextureRect.zw);
    gl_Position = SGWorldViewProjection * vec4(SGPositions, 1.0);
}