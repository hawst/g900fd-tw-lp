#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

attribute vec3 SGPositions;
attribute vec2 SGTextureCoords;

varying vec2 vTexCoords;

uniform mat4 SGWorldViewProjection;
uniform vec4 SGTextureRect;

void main() {
    vTexCoords = SGTextureRect.xy + SGTextureCoords * (SGTextureRect.zw - SGTextureRect.xy);
    gl_Position = SGWorldViewProjection * vec4(SGPositions, 1.0);
}