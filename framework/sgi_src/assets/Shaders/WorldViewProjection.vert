#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

attribute vec3 SGPositions;

uniform mat4 SGWorldViewProjection;

void main() {
    gl_Position = SGWorldViewProjection * vec4(SGPositions, 1.0);
}