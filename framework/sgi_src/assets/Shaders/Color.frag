#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

uniform vec4 SGColor;
uniform float SGOpacity;

void main() {
    gl_FragColor = SGColor;
    gl_FragColor.a *= SGOpacity;
}