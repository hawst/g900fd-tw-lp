#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

varying vec2 vTexCoords;

uniform sampler2D SGTexture;
uniform vec4 SGColor;
uniform float SGOpacity;

void main() {
    gl_FragColor = SGColor + texture2D(SGTexture, vTexCoords);
    gl_FragColor.a *= SGOpacity;
}