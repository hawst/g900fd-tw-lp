#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

uniform mat4 SGWorldViewProjection;

attribute vec3 SGPositions;
attribute vec2 SGTextureCoords;

varying vec2 vTexCoords;

void main() {
    vTexCoords = SGTextureCoords;
    gl_Position = SGWorldViewProjection * vec4(SGPositions, 1.0);
}