#ifdef GL_ES
#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif
#endif

uniform vec4 SGColor;

void main() {
    vec4 precolor = vec4(SGColor.rgb * SGColor.a, SGColor.a);
    gl_FragColor = precolor;
}