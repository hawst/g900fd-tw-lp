.class public final enum Lcom/qti/snapdragon/digitalpen/ConfigManager$State;
.super Ljava/lang/Enum;
.source "ConfigManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/ConfigManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qti/snapdragon/digitalpen/ConfigManager$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

.field public static final enum APP:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

.field public static final enum LEGACY:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

.field public static final enum LEGACY_WITH_SIDE_CHANNEL:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    new-instance v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    const-string v1, "LEGACY"

    invoke-direct {v0, v1, v2}, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->LEGACY:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    .line 43
    new-instance v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    const-string v1, "LEGACY_WITH_SIDE_CHANNEL"

    invoke-direct {v0, v1, v3}, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->LEGACY_WITH_SIDE_CHANNEL:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    .line 46
    new-instance v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    const-string v1, "APP"

    invoke-direct {v0, v1, v4}, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->APP:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    .line 36
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    sget-object v1, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->LEGACY:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->LEGACY_WITH_SIDE_CHANNEL:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->APP:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    aput-object v1, v0, v4

    sput-object v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->$VALUES:[Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qti/snapdragon/digitalpen/ConfigManager$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 36
    const-class v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    return-object v0
.end method

.method public static values()[Lcom/qti/snapdragon/digitalpen/ConfigManager$State;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->$VALUES:[Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    invoke-virtual {v0}, [Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    return-object v0
.end method
