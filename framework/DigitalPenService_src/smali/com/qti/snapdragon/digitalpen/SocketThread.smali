.class Lcom/qti/snapdragon/digitalpen/SocketThread;
.super Ljava/lang/Thread;
.source "SocketThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/SocketThread$ReceiveWorker;
    }
.end annotation


# static fields
.field private static final CONNECTION_SLEEP_TIME:I = 0x1f4

.field private static final MAX_CONNECTION_RETRIES:I = 0x32

.field private static final TAG:Ljava/lang/String; = "SocketThread"


# instance fields
.field private mConnected:Z

.field private mSocket:Landroid/net/LocalSocket;

.field private final mSocketPath:Ljava/lang/String;

.field private mWorker:Lcom/qti/snapdragon/digitalpen/SocketThread$ReceiveWorker;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/qti/snapdragon/digitalpen/SocketThread$ReceiveWorker;)V
    .locals 1
    .param p1, "socketPath"    # Ljava/lang/String;
    .param p2, "worker"    # Lcom/qti/snapdragon/digitalpen/SocketThread$ReceiveWorker;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mSocketPath:Ljava/lang/String;

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mConnected:Z

    .line 50
    iput-object p2, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mWorker:Lcom/qti/snapdragon/digitalpen/SocketThread$ReceiveWorker;

    .line 51
    return-void
.end method


# virtual methods
.method public getSocket()Landroid/net/LocalSocket;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mSocket:Landroid/net/LocalSocket;

    return-object v0
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mConnected:Z

    return v0
.end method

.method public run()V
    .locals 8

    .prologue
    .line 73
    const/4 v1, 0x1

    .line 75
    .local v1, "recover":Z
    :goto_0
    if-eqz v1, :cond_2

    .line 77
    const/16 v2, 0x32

    .line 78
    .local v2, "retriesLeft":I
    :goto_1
    add-int/lit8 v2, v2, -0x1

    if-lez v2, :cond_0

    .line 81
    :try_start_0
    const-string v4, "SocketThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "connecting to : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mSocketPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", retriesLeft: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    new-instance v4, Landroid/net/LocalSocket;

    invoke-direct {v4}, Landroid/net/LocalSocket;-><init>()V

    iput-object v4, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mSocket:Landroid/net/LocalSocket;

    .line 84
    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mSocket:Landroid/net/LocalSocket;

    new-instance v5, Landroid/net/LocalSocketAddress;

    iget-object v6, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mSocketPath:Ljava/lang/String;

    sget-object v7, Landroid/net/LocalSocketAddress$Namespace;->FILESYSTEM:Landroid/net/LocalSocketAddress$Namespace;

    invoke-direct {v5, v6, v7}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    invoke-virtual {v4, v5}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 86
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mConnected:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 98
    :cond_0
    if-nez v2, :cond_1

    .line 99
    :try_start_1
    const-string v4, "SocketThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "no more retries: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mSocketPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    const/4 v1, 0x0

    .line 104
    :cond_1
    iget-boolean v4, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mConnected:Z

    if-nez v4, :cond_4

    .line 105
    const-string v4, "SocketThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Connection to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mSocketPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " unsuccessful"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    .end local v2    # "retriesLeft":I
    :goto_2
    return-void

    .line 88
    .restart local v2    # "retriesLeft":I
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "SocketThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "socket not connecting to: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mSocketPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    const-wide/16 v4, 0x1f4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 124
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v4

    .line 127
    .end local v2    # "retriesLeft":I
    :cond_2
    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mSocket:Landroid/net/LocalSocket;

    if-eqz v4, :cond_3

    .line 129
    :try_start_2
    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v4}, Landroid/net/LocalSocket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    .line 134
    :cond_3
    :goto_3
    const-string v4, "SocketThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Thread closing: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 109
    .restart local v2    # "retriesLeft":I
    :cond_4
    const/4 v3, 0x0

    .line 114
    .local v3, "socketInput":Ljava/io/InputStream;
    :try_start_3
    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v4}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    .line 115
    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/SocketThread;->mWorker:Lcom/qti/snapdragon/digitalpen/SocketThread$ReceiveWorker;

    invoke-interface {v4, v3}, Lcom/qti/snapdragon/digitalpen/SocketThread$ReceiveWorker;->receiveLoop(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    goto/16 :goto_0

    .line 116
    :catch_2
    move-exception v0

    .line 117
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_4
    const-string v4, "SocketThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 118
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 119
    .local v0, "e":Ljava/lang/InterruptedException;
    throw v0

    .line 120
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_4
    move-exception v0

    .line 121
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 130
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "retriesLeft":I
    .end local v3    # "socketInput":Ljava/io/InputStream;
    :catch_5
    move-exception v0

    .line 131
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "SocketThread"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException while closing socket "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method
