.class Lcom/qti/snapdragon/digitalpen/DigitalPenService$6$1;
.super Ljava/lang/Object;
.source "DigitalPenService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;


# direct methods
.method constructor <init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;)V
    .locals 0

    .prologue
    .line 540
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$6$1;->this$1:Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 542
    const-string v0, "DigitalPenService"

    const-string v1, "Restarting daemon"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$6$1;->this$1:Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # invokes: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->enableDaemon()Z
    invoke-static {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$900(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 551
    const-string v0, "DigitalPenService"

    const-string v1, "Daemon could not be restarted. Disabling feature."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$6$1;->this$1:Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->disable()Z

    .line 554
    :cond_0
    return-void
.end method
