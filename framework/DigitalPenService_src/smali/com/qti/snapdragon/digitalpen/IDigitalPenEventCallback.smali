.class public interface abstract Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;
.super Ljava/lang/Object;
.source "IDigitalPenEventCallback.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback$Stub;
    }
.end annotation


# virtual methods
.method public abstract onDigitalPenPropEvent(Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
