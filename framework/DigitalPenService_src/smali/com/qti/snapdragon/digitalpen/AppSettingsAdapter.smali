.class public Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter;
.super Ljava/lang/Object;
.source "AppSettingsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AppSettingsAdapter"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public static applyAppSettings(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Landroid/os/Bundle;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 14
    .param p0, "globalConfig"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .param p1, "appSettings"    # Landroid/os/Bundle;

    .prologue
    const/4 v13, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 47
    invoke-static {p0}, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter;->copyOfConfig(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v2

    .line 51
    .local v2, "currentConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    const-string v9, "OffScreenMode"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 52
    const-string v9, "OffScreenMode"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    .line 55
    .local v6, "mode":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;
    sget-object v9, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$OffScreenMode:[I

    invoke-virtual {v6}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->ordinal()I

    move-result v12

    aget v9, v9, v12

    packed-switch v9, :pswitch_data_0

    .line 66
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unknown mode: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 57
    :pswitch_0
    const/4 v0, 0x0

    .line 68
    .local v0, "configMode":B
    :goto_0
    invoke-virtual {v2, v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setOffScreenMode(B)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 71
    .end local v0    # "configMode":B
    .end local v6    # "mode":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;
    :cond_0
    const-string v9, "OnScreenHoverEnabled"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 72
    const-string v9, "OnScreenHoverEnabled"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    invoke-virtual {v2, v9}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setOnScreenHoverEnable(Z)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 76
    :cond_1
    const-string v9, "OnScreenHoverMaxDistance"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 77
    const-string v9, "OnScreenHoverMaxDistance"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v2, v9}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setOnScreenHoverMaxRange(I)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 81
    :cond_2
    const-string v9, "OffScreenHoverEnabled"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 82
    const-string v9, "OffScreenHoverEnabled"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    invoke-virtual {v2, v9}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setOffScreenHoverEnable(Z)V

    .line 86
    :cond_3
    const-string v9, "OffScreenHoverMaxDistance"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 87
    const-string v9, "OffScreenHoverMaxDistance"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v2, v9}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setOffScreenHoverMaxRange(I)V

    .line 91
    :cond_4
    const-string v9, "EraserBypass"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 92
    const-string v9, "EraserBypass"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 93
    const/4 v9, -0x1

    invoke-virtual {v2, v9}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setEraseButtonIndex(I)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 97
    :cond_5
    const-string v9, "InputType"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 98
    const-string v9, "InputType"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    .line 101
    .local v4, "input":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;
    sget-object v9, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$InputType:[I

    invoke-virtual {v4}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;->ordinal()I

    move-result v12

    aget v9, v9, v12

    packed-switch v9, :pswitch_data_1

    .line 115
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unknown input type: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 60
    .end local v4    # "input":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;
    .restart local v6    # "mode":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;
    :pswitch_1
    const/4 v0, 0x2

    .line 61
    .restart local v0    # "configMode":B
    goto/16 :goto_0

    .line 63
    .end local v0    # "configMode":B
    :pswitch_2
    const/4 v0, 0x1

    .line 64
    .restart local v0    # "configMode":B
    goto/16 :goto_0

    .line 103
    .end local v0    # "configMode":B
    .end local v6    # "mode":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;
    .restart local v4    # "input":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;
    :pswitch_3
    const/4 v1, 0x4

    .line 104
    .local v1, "configSetting":B
    const-string v9, "OffScreenMode"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    check-cast v6, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    .line 106
    .restart local v6    # "mode":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;
    sget-object v9, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->DUPLICATE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    if-eq v6, v9, :cond_6

    .line 107
    const-string v9, "AppSettingsAdapter"

    const-string v12, "Mouse will only work when offscreen mode is OffscreenMode.DUPLICATE"

    invoke-static {v9, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    .end local v6    # "mode":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;
    :cond_6
    :goto_1
    invoke-virtual {v2, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setInputType(B)V

    .line 120
    .end local v1    # "configSetting":B
    .end local v4    # "input":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;
    :cond_7
    const-string v9, "MouseSensitivity"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_8

    .line 121
    const-string v9, "MouseSensitivity"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    .line 124
    .local v7, "mouseSensitivity":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;
    sget-object v9, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$MouseSensitivity:[I

    invoke-virtual {v7}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;->ordinal()I

    move-result v12

    aget v9, v9, v12

    packed-switch v9, :pswitch_data_2

    .line 135
    new-instance v9, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Unknown mouse sensitivity value: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 112
    .end local v7    # "mouseSensitivity":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;
    .restart local v4    # "input":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;
    :pswitch_4
    const/4 v1, 0x1

    .line 113
    .restart local v1    # "configSetting":B
    goto :goto_1

    .line 126
    .end local v1    # "configSetting":B
    .end local v4    # "input":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;
    .restart local v7    # "mouseSensitivity":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;
    :pswitch_5
    const/4 v1, 0x1

    .line 138
    .restart local v1    # "configSetting":B
    :goto_2
    invoke-virtual {v2, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setMouseSensitivity(B)V

    .line 142
    .end local v1    # "configSetting":B
    .end local v7    # "mouseSensitivity":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;
    :cond_8
    const-string v9, "OnScreenMapping"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 143
    sget-object v12, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;->SCALED:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    const-string v9, "OnScreenMapping"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v9

    check-cast v9, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    if-ne v12, v9, :cond_c

    move v5, v10

    .line 146
    .local v5, "isMapped":Z
    :goto_3
    invoke-virtual {v2, v13, v5}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setOnScreenCoordReporting(BZ)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 151
    .end local v5    # "isMapped":Z
    :cond_9
    const-string v9, "AllMapping"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 152
    invoke-virtual {v2, v10}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setSendAllDataEventsToSideChannel(Z)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 156
    :cond_a
    const-string v9, "OffScreenMapping"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 157
    sget-object v12, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;->SCALED:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    const-string v9, "OffScreenMapping"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v9

    check-cast v9, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    if-ne v12, v9, :cond_d

    move v5, v10

    .line 161
    .restart local v5    # "isMapped":Z
    :goto_4
    const-string v9, "OffScreenBackgroundListener"

    invoke-virtual {p1, v9, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 163
    const/4 v3, 0x1

    .line 164
    .local v3, "destination":B
    invoke-virtual {v2, v10}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setOffScreenMode(B)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 174
    :goto_5
    invoke-virtual {v2, v3, v5}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setOffScreenCoordReporting(BZ)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 176
    .end local v3    # "destination":B
    .end local v5    # "isMapped":Z
    :cond_b
    return-object v2

    .line 129
    .restart local v7    # "mouseSensitivity":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;
    :pswitch_6
    const/4 v1, 0x2

    .line 130
    .restart local v1    # "configSetting":B
    goto :goto_2

    .line 132
    .end local v1    # "configSetting":B
    :pswitch_7
    const/4 v1, 0x4

    .line 133
    .restart local v1    # "configSetting":B
    goto :goto_2

    .end local v1    # "configSetting":B
    .end local v7    # "mouseSensitivity":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;
    :cond_c
    move v5, v11

    .line 143
    goto :goto_3

    :cond_d
    move v5, v11

    .line 157
    goto :goto_4

    .line 166
    .restart local v5    # "isMapped":Z
    :cond_e
    invoke-virtual {v2}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOffScreenMode()B

    move-result v8

    .line 167
    .local v8, "offScreenMode":B
    if-eq v8, v10, :cond_f

    if-ne v8, v13, :cond_10

    .line 169
    :cond_f
    const/4 v3, 0x2

    .restart local v3    # "destination":B
    goto :goto_5

    .line 171
    .end local v3    # "destination":B
    :cond_10
    const/4 v3, 0x1

    .restart local v3    # "destination":B
    goto :goto_5

    .line 55
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 101
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 124
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static copyForBackgroundOffScreenListener(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 3
    .param p0, "baseSettings"    # Landroid/os/Bundle;

    .prologue
    .line 197
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 198
    .local v0, "settings":Landroid/os/Bundle;
    const-string v1, "OffScreenBackgroundListener"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 199
    const-string v1, "OffScreenMode"

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->EXTEND:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 200
    const-string v1, "OffScreenMapping"

    const-string v2, "OffScreenMapping"

    invoke-virtual {p0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 202
    return-object v0
.end method

.method public static copyOfConfig(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 3
    .param p0, "globalConfig"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .prologue
    const/4 v2, 0x0

    .line 186
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 187
    .local v1, "parcel":Landroid/os/Parcel;
    invoke-virtual {p0, v1, v2}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 188
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 189
    sget-object v2, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v2, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 190
    .local v0, "currentConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 191
    return-object v0
.end method
