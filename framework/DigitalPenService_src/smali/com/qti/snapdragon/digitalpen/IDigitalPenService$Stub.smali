.class public abstract Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;
.super Landroid/os/Binder;
.source "IDigitalPenService.java"

# interfaces
.implements Lcom/qti/snapdragon/digitalpen/IDigitalPenService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qti.snapdragon.digitalpen.IDigitalPenService"

.field static final TRANSACTION_applyAppSettings:I = 0xb

.field static final TRANSACTION_disable:I = 0x3

.field static final TRANSACTION_enable:I = 0x2

.field static final TRANSACTION_getConfig:I = 0x6

.field static final TRANSACTION_isEnabled:I = 0x4

.field static final TRANSACTION_registerDataCallback:I = 0x7

.field static final TRANSACTION_registerEventCallback:I = 0x8

.field static final TRANSACTION_registerOffScreenDimensionsCallback:I = 0x1

.field static final TRANSACTION_releaseActivity:I = 0xc

.field static final TRANSACTION_setGlobalConfig:I = 0x5

.field static final TRANSACTION_unregisterDataCallback:I = 0x9

.field static final TRANSACTION_unregisterEventCallback:I = 0xa


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 22
    const-string v0, "com.qti.snapdragon.digitalpen.IDigitalPenService"

    invoke-virtual {p0, p0, v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 30
    if-nez p0, :cond_0

    .line 31
    const/4 v0, 0x0

    .line 37
    :goto_0
    return-object v0

    .line 33
    :cond_0
    const-string v1, "com.qti.snapdragon.digitalpen.IDigitalPenService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 34
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    if-eqz v1, :cond_1

    .line 35
    check-cast v0, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    goto :goto_0

    .line 37
    :cond_1
    new-instance v0, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 41
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 45
    sparse-switch p1, :sswitch_data_0

    .line 178
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v3

    :goto_0
    return v3

    .line 49
    :sswitch_0
    const-string v2, "com.qti.snapdragon.digitalpen.IDigitalPenService"

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 54
    :sswitch_1
    const-string v2, "com.qti.snapdragon.digitalpen.IDigitalPenService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcodeaurora/ultrasound/IDigitalPenDimensionsCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcodeaurora/ultrasound/IDigitalPenDimensionsCallback;

    move-result-object v0

    .line 57
    .local v0, "_arg0":Lcodeaurora/ultrasound/IDigitalPenDimensionsCallback;
    invoke-virtual {p0, v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;->registerOffScreenDimensionsCallback(Lcodeaurora/ultrasound/IDigitalPenDimensionsCallback;)V

    .line 58
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 63
    .end local v0    # "_arg0":Lcodeaurora/ultrasound/IDigitalPenDimensionsCallback;
    :sswitch_2
    const-string v4, "com.qti.snapdragon.digitalpen.IDigitalPenService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;->enable()Z

    move-result v1

    .line 65
    .local v1, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 66
    if-eqz v1, :cond_0

    move v2, v3

    :cond_0
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 71
    .end local v1    # "_result":Z
    :sswitch_3
    const-string v4, "com.qti.snapdragon.digitalpen.IDigitalPenService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;->disable()Z

    move-result v1

    .line 73
    .restart local v1    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 74
    if-eqz v1, :cond_1

    move v2, v3

    :cond_1
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 79
    .end local v1    # "_result":Z
    :sswitch_4
    const-string v4, "com.qti.snapdragon.digitalpen.IDigitalPenService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;->isEnabled()Z

    move-result v1

    .line 81
    .restart local v1    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 82
    if-eqz v1, :cond_2

    move v2, v3

    :cond_2
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 87
    .end local v1    # "_result":Z
    :sswitch_5
    const-string v4, "com.qti.snapdragon.digitalpen.IDigitalPenService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 89
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_4

    .line 90
    sget-object v4, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 95
    .local v0, "_arg0":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;->setGlobalConfig(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)Z

    move-result v1

    .line 96
    .restart local v1    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 97
    if-eqz v1, :cond_3

    move v2, v3

    :cond_3
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 93
    .end local v0    # "_arg0":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .end local v1    # "_result":Z
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    goto :goto_1

    .line 102
    .end local v0    # "_arg0":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    :sswitch_6
    const-string v4, "com.qti.snapdragon.digitalpen.IDigitalPenService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;->getConfig()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v1

    .line 104
    .local v1, "_result":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 105
    if-eqz v1, :cond_5

    .line 106
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 107
    invoke-virtual {v1, p3, v3}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 110
    :cond_5
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 116
    .end local v1    # "_result":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    :sswitch_7
    const-string v4, "com.qti.snapdragon.digitalpen.IDigitalPenService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;

    move-result-object v0

    .line 119
    .local v0, "_arg0":Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;
    invoke-virtual {p0, v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;->registerDataCallback(Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;)Z

    move-result v1

    .line 120
    .local v1, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 121
    if-eqz v1, :cond_6

    move v2, v3

    :cond_6
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 126
    .end local v0    # "_arg0":Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;
    .end local v1    # "_result":Z
    :sswitch_8
    const-string v4, "com.qti.snapdragon.digitalpen.IDigitalPenService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 128
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;

    move-result-object v0

    .line 129
    .local v0, "_arg0":Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;
    invoke-virtual {p0, v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;->registerEventCallback(Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;)Z

    move-result v1

    .line 130
    .restart local v1    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 131
    if-eqz v1, :cond_7

    move v2, v3

    :cond_7
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 136
    .end local v0    # "_arg0":Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;
    .end local v1    # "_result":Z
    :sswitch_9
    const-string v4, "com.qti.snapdragon.digitalpen.IDigitalPenService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;

    move-result-object v0

    .line 139
    .local v0, "_arg0":Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;
    invoke-virtual {p0, v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;->unregisterDataCallback(Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;)Z

    move-result v1

    .line 140
    .restart local v1    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 141
    if-eqz v1, :cond_8

    move v2, v3

    :cond_8
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 146
    .end local v0    # "_arg0":Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;
    .end local v1    # "_result":Z
    :sswitch_a
    const-string v4, "com.qti.snapdragon.digitalpen.IDigitalPenService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 148
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;

    move-result-object v0

    .line 149
    .local v0, "_arg0":Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;
    invoke-virtual {p0, v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;->unregisterEventCallback(Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;)Z

    move-result v1

    .line 150
    .restart local v1    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 151
    if-eqz v1, :cond_9

    move v2, v3

    :cond_9
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 156
    .end local v0    # "_arg0":Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;
    .end local v1    # "_result":Z
    :sswitch_b
    const-string v4, "com.qti.snapdragon.digitalpen.IDigitalPenService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 158
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_b

    .line 159
    sget-object v4, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 164
    .local v0, "_arg0":Landroid/os/Bundle;
    :goto_2
    invoke-virtual {p0, v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;->applyAppSettings(Landroid/os/Bundle;)Z

    move-result v1

    .line 165
    .restart local v1    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 166
    if-eqz v1, :cond_a

    move v2, v3

    :cond_a
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 162
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v1    # "_result":Z
    :cond_b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_2

    .line 171
    .end local v0    # "_arg0":Landroid/os/Bundle;
    :sswitch_c
    const-string v4, "com.qti.snapdragon.digitalpen.IDigitalPenService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 172
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;->releaseActivity()Z

    move-result v1

    .line 173
    .restart local v1    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 174
    if-eqz v1, :cond_c

    move v2, v3

    :cond_c
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 45
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
