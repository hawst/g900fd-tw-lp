.class Lcom/qti/snapdragon/digitalpen/DigitalPenService$2;
.super Ljava/lang/Object;
.source "DigitalPenService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenService;->sendCachedStateEvents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;


# direct methods
.method constructor <init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$2;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 178
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$2;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    new-array v1, v3, [I

    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$2;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedCurrentPowerState:I
    invoke-static {v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$100(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)I

    move-result v2

    aput v2, v1, v4

    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$2;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPreviousPowerState:I
    invoke-static {v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$200(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)I

    move-result v2

    aput v2, v1, v5

    invoke-virtual {v0, v4, v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->sendEvent(I[I)V

    .line 181
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$2;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    new-array v1, v5, [I

    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$2;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedMicsBlocked:I
    invoke-static {v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$300(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)I

    move-result v2

    aput v2, v1, v4

    invoke-virtual {v0, v3, v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->sendEvent(I[I)V

    .line 184
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$2;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    const/4 v1, 0x4

    new-array v2, v3, [I

    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$2;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPenBatteryState:I
    invoke-static {v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$400(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)I

    move-result v3

    aput v3, v2, v4

    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$2;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPenBatteryLevel:I
    invoke-static {v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$500(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)I

    move-result v3

    aput v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->sendEvent(I[I)V

    .line 187
    return-void
.end method
