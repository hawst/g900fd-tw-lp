.class public Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;
.super Ljava/lang/Object;
.source "DigitalPenConfigMirror.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$1;,
        Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$EraseButton;,
        Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$Hover;,
        Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$SmarterStand;,
        Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$OffScreen;,
        Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$CopyDirection;
    }
.end annotation

.annotation runtime Lorg/simpleframework/xml/Root;
    name = "GlobalSettings"
.end annotation


# instance fields
.field private mEraseButton:Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$EraseButton;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "EraseButton"
    .end annotation
.end field

.field private mInputType:B
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "InputType"
    .end annotation
.end field

.field private mMouseSensitivity:B
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "MouseSensitivity"
    .end annotation
.end field

.field private mOffScreen:Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$OffScreen;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "OffScreen"
    .end annotation
.end field

.field private mOffScreenHover:Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$Hover;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "OffScreenHover"
    .end annotation
.end field

.field private mOnScreenHover:Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$Hover;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "OnScreenHover"
    .end annotation
.end field

.field private mPowerSaveMode:B
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "PowerSaveMode"
    .end annotation
.end field

.field private mStartPenOnBoot:Z
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "StartPenOnBoot"
    .end annotation
.end field

.field private mStopPenOnScreenOff:Z
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "StopPenOnScreenOff"
    .end annotation
.end field

.field private mTouchRange:I
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "TouchRange"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$OffScreen;

    invoke-direct {v0, v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$OffScreen;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;->mOffScreen:Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$OffScreen;

    .line 93
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$Hover;

    invoke-direct {v0, v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$Hover;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$Hover;

    .line 96
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$Hover;

    invoke-direct {v0, v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$Hover;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$Hover;

    .line 99
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$EraseButton;

    invoke-direct {v0, v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$EraseButton;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;->mEraseButton:Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$EraseButton;

    return-void
.end method

.method private static doCopyPersistedFields(Ljava/lang/Object;Ljava/lang/Object;Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$CopyDirection;)V
    .locals 10
    .param p0, "mirrorObj"    # Ljava/lang/Object;
    .param p1, "trueObj"    # Ljava/lang/Object;
    .param p2, "dir"    # Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$CopyDirection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NoSuchFieldException;,
            Ljava/lang/IllegalAccessException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 124
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v5

    .line 125
    .local v5, "mirrorFields":[Ljava/lang/reflect/Field;
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/reflect/Field;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_4

    aget-object v4, v0, v2

    .line 126
    .local v4, "mirrorField":Ljava/lang/reflect/Field;
    const-class v7, Lorg/simpleframework/xml/Element;

    invoke-virtual {v4, v7}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 127
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v1

    .line 128
    .local v1, "fieldName":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v6

    .line 129
    .local v6, "trueField":Ljava/lang/reflect/Field;
    invoke-virtual {v4, v9}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 130
    invoke-virtual {v6, v9}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 131
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v7

    array-length v7, v7

    if-nez v7, :cond_3

    .line 133
    invoke-virtual {v6}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v8

    if-eq v7, v8, :cond_0

    .line 134
    new-instance v7, Ljava/lang/IllegalArgumentException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Type of field \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' differs"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 138
    :cond_0
    sget-object v7, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$CopyDirection;->MIRROR_TO_TRUE:Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$CopyDirection;

    if-ne p2, v7, :cond_2

    .line 139
    invoke-virtual {v4, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, p1, v7}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 125
    .end local v1    # "fieldName":Ljava/lang/String;
    .end local v6    # "trueField":Ljava/lang/reflect/Field;
    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 141
    .restart local v1    # "fieldName":Ljava/lang/String;
    .restart local v6    # "trueField":Ljava/lang/reflect/Field;
    :cond_2
    invoke-virtual {v6, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v4, p0, v7}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 145
    :cond_3
    invoke-virtual {v4, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-static {v7, v8, p2}, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;->doCopyPersistedFields(Ljava/lang/Object;Ljava/lang/Object;Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$CopyDirection;)V

    goto :goto_1

    .line 149
    .end local v1    # "fieldName":Ljava/lang/String;
    .end local v4    # "mirrorField":Ljava/lang/reflect/Field;
    .end local v6    # "trueField":Ljava/lang/reflect/Field;
    :cond_4
    return-void
.end method


# virtual methods
.method public copyPersistedFields(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$CopyDirection;)V
    .locals 4
    .param p1, "trueObj"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .param p2, "dir"    # Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$CopyDirection;

    .prologue
    .line 116
    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;->doCopyPersistedFields(Ljava/lang/Object;Ljava/lang/Object;Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$CopyDirection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    return-void

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Problem persisting fields in direction: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
