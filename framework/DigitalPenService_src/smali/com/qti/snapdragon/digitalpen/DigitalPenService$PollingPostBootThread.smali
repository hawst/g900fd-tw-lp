.class Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;
.super Ljava/lang/Thread;
.source "DigitalPenService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PollingPostBootThread"
.end annotation


# instance fields
.field final SHORT_SLEEP_TIME:I

.field final synthetic this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;


# direct methods
.method constructor <init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V
    .locals 1

    .prologue
    .line 861
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 862
    const/16 v0, 0x12c

    iput v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->SHORT_SLEEP_TIME:I

    return-void
.end method

.method static synthetic access$1600(Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;
    .param p1, "x1"    # Z

    .prologue
    .line 861
    invoke-direct {p0, p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->parseAndDeleteCommandFile(Z)V

    return-void
.end method

.method private attachCommandFileObserver()V
    .locals 4

    .prologue
    .line 892
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mEposDirObserver:Lcom/qti/snapdragon/digitalpen/DirectoryObserver;
    invoke-static {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$1700(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Lcom/qti/snapdragon/digitalpen/DirectoryObserver;

    move-result-object v0

    const-string v1, "command.txt"

    const/16 v2, 0x8

    new-instance v3, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread$1;

    invoke-direct {v3, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread$1;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/qti/snapdragon/digitalpen/DirectoryObserver;->registerObserver(Ljava/lang/String;ILcom/qti/snapdragon/digitalpen/DirectoryObserver$FileChangeListener;)V

    .line 901
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->parseAndDeleteCommandFile(Z)V

    .line 902
    return-void
.end method

.method private attachSettingsFileObserver()V
    .locals 4

    .prologue
    .line 942
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mEposDirObserver:Lcom/qti/snapdragon/digitalpen/DirectoryObserver;
    invoke-static {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$1700(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Lcom/qti/snapdragon/digitalpen/DirectoryObserver;

    move-result-object v0

    const-string v1, "service_settings.xml"

    const/16 v2, 0x100

    new-instance v3, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread$2;

    invoke-direct {v3, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread$2;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/qti/snapdragon/digitalpen/DirectoryObserver;->registerObserver(Ljava/lang/String;ILcom/qti/snapdragon/digitalpen/DirectoryObserver$FileChangeListener;)V

    .line 965
    return-void
.end method

.method private parseAndDeleteCommandFile(Z)V
    .locals 7
    .param p1, "mustBeFound"    # Z

    .prologue
    .line 906
    :try_start_0
    new-instance v0, Ljava/io/File;

    const-string v4, "/data/usf/epos/command.txt"

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 907
    .local v0, "commandFile":Ljava/io/File;
    new-instance v3, Ljava/util/Scanner;

    invoke-direct {v3, v0}, Ljava/util/Scanner;-><init>(Ljava/io/File;)V

    .line 908
    .local v3, "scan":Ljava/util/Scanner;
    invoke-virtual {v3}, Ljava/util/Scanner;->nextLine()Ljava/lang/String;

    move-result-object v2

    .line 909
    .local v2, "line":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/util/Scanner;->close()V

    .line 916
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 919
    const-string v4, "DigitalPenService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Got command from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ": "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 920
    const-string v4, "enable"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 921
    iget-object v5, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    monitor-enter v5
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 922
    :try_start_1
    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-virtual {v4}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->enable()Z

    .line 923
    monitor-exit v5

    .line 936
    .end local v0    # "commandFile":Ljava/io/File;
    .end local v2    # "line":Ljava/lang/String;
    .end local v3    # "scan":Ljava/util/Scanner;
    :cond_0
    :goto_0
    return-void

    .line 923
    .restart local v0    # "commandFile":Ljava/io/File;
    .restart local v2    # "line":Ljava/lang/String;
    .restart local v3    # "scan":Ljava/util/Scanner;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    .line 931
    .end local v0    # "commandFile":Ljava/io/File;
    .end local v2    # "line":Ljava/lang/String;
    .end local v3    # "scan":Ljava/util/Scanner;
    :catch_0
    move-exception v1

    .line 932
    .local v1, "e":Ljava/io/FileNotFoundException;
    if-eqz p1, :cond_0

    .line 933
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 924
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .restart local v0    # "commandFile":Ljava/io/File;
    .restart local v2    # "line":Ljava/lang/String;
    .restart local v3    # "scan":Ljava/util/Scanner;
    :cond_1
    :try_start_3
    const-string v4, "disable"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 925
    iget-object v5, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    monitor-enter v5
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0

    .line 926
    :try_start_4
    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-virtual {v4}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->disable()Z

    .line 927
    monitor-exit v5

    goto :goto_0

    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v4

    .line 929
    :cond_2
    const-string v4, "DigitalPenService"

    const-string v5, "Unknown command"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 866
    :goto_0
    const-string v2, "init.svc.usf-post-boot"

    const-string v3, "running"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "running"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 868
    const-wide/16 v2, 0x12c

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 869
    :catch_0
    move-exception v0

    .line 870
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v2, "DigitalPenService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Interrupted while waiting for post boot "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 874
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    monitor-enter v3

    .line 875
    :try_start_1
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->attachSettingsFileObserver()V

    .line 876
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # invokes: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->setGlobalConfigFromPersister()V
    invoke-static {v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$1500(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    .line 877
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigManager:Lcom/qti/snapdragon/digitalpen/ConfigManager;
    invoke-static {v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$600(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Lcom/qti/snapdragon/digitalpen/ConfigManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->getGlobalConfig()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isStartPenOnBootEnabled()Z

    move-result v1

    .line 879
    .local v1, "enableDigitalPenOnBoot":Z
    if-eqz v1, :cond_1

    .line 880
    const-string v2, "DigitalPenService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "usf-post-boot is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "init.svc.usf-post-boot"

    const-string v6, "running"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", enabling daemon"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 884
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-virtual {v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->enable()Z

    .line 886
    :cond_1
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->attachCommandFileObserver()V

    .line 887
    monitor-exit v3

    .line 888
    return-void

    .line 887
    .end local v1    # "enableDigitalPenOnBoot":Z
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method
