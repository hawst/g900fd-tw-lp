.class public Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;
.super Ljava/lang/Object;
.source "SmarterStandSensorListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$AccelerometerChangeCallback;
    }
.end annotation


# static fields
.field private static final REPORT_ANGLE_THRESHOLD:D = 1.5

.field private static final TAG:Ljava/lang/String; = "SmarterStandSensorListener"


# instance fields
.field private final callback:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$AccelerometerChangeCallback;

.field private prevReportedAngle:D

.field private final sensorEventListener:Landroid/hardware/SensorEventListener;

.field private final sensorManager:Landroid/hardware/SensorManager;

.field private final supportedOrientation:I


# direct methods
.method public constructor <init>(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$AccelerometerChangeCallback;Landroid/hardware/SensorManager;I)V
    .locals 2
    .param p1, "callback"    # Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$AccelerometerChangeCallback;
    .param p2, "sensorManager"    # Landroid/hardware/SensorManager;
    .param p3, "supportedOrientation"    # I

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->prevReportedAngle:D

    .line 39
    new-instance v0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$1;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$1;-><init>(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->sensorEventListener:Landroid/hardware/SensorEventListener;

    .line 96
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->callback:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$AccelerometerChangeCallback;

    .line 97
    iput-object p2, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->sensorManager:Landroid/hardware/SensorManager;

    .line 98
    iput p3, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->supportedOrientation:I

    .line 99
    return-void
.end method

.method static synthetic access$000(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;)D
    .locals 2
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    .prologue
    .line 20
    iget-wide v0, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->prevReportedAngle:D

    return-wide v0
.end method

.method static synthetic access$002(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;D)D
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;
    .param p1, "x1"    # D

    .prologue
    .line 20
    iput-wide p1, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->prevReportedAngle:D

    return-wide p1
.end method

.method static synthetic access$100(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;DD)D
    .locals 3
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;
    .param p1, "x1"    # D
    .param p3, "x2"    # D

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->diff(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$200(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;)Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$AccelerometerChangeCallback;
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->callback:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$AccelerometerChangeCallback;

    return-object v0
.end method

.method static synthetic access$300(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    .prologue
    .line 20
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->supportedOrientation:I

    return v0
.end method

.method private diff(DD)D
    .locals 3
    .param p1, "d1"    # D
    .param p3, "d2"    # D

    .prologue
    .line 122
    sub-double v0, p1, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public start()V
    .locals 5

    .prologue
    .line 104
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->sensorManager:Landroid/hardware/SensorManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getSensorList(I)Ljava/util/List;

    move-result-object v1

    .line 105
    .local v1, "sensors":Ljava/util/List;, "Ljava/util/List<Landroid/hardware/Sensor;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 106
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Sensor;

    .line 107
    .local v0, "sensor":Landroid/hardware/Sensor;
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->sensorEventListener:Landroid/hardware/SensorEventListener;

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v0, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 113
    .end local v0    # "sensor":Landroid/hardware/Sensor;
    :goto_0
    return-void

    .line 111
    :cond_0
    const-string v2, "SmarterStandSensorListener"

    const-string v3, "There\'s no accelerometer sensor, cannot track angle changes"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->sensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->sensorEventListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 119
    :cond_0
    return-void
.end method
