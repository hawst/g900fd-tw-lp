.class Lcom/qti/snapdragon/digitalpen/DigitalPenService$7;
.super Ljava/lang/Object;
.source "DigitalPenService.java"

# interfaces
.implements Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker$OnDataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;


# direct methods
.method constructor <init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V
    .locals 0

    .prologue
    .line 571
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$7;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onData(Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;)V
    .locals 2
    .param p1, "data"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;

    .prologue
    .line 576
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$7;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mIsFilteringData:Z
    invoke-static {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$1000(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->getRegion()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 580
    :goto_0
    return-void

    .line 579
    :cond_0
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$7;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->invokeDataCallbacks(Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;)V

    goto :goto_0
.end method
