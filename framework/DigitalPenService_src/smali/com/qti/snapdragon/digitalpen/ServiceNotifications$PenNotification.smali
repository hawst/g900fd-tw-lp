.class final enum Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;
.super Ljava/lang/Enum;
.source "ServiceNotifications.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/ServiceNotifications;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "PenNotification"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

.field public static final enum ACTIVE:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

.field public static final enum BACKGROUND_LISTENER:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

.field public static final enum BLOCKED_2_MICS:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

.field public static final enum BLOCKED_3_OR_MORE_MICS:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

.field public static final enum LOW_BATTERY:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

.field public static final enum POSITIONING_PROBLEM:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;


# instance fields
.field builder:Landroid/app/Notification$Builder;

.field final icon:I

.field final id:I

.field final title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 24
    new-instance v0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    const-string v1, "ACTIVE"

    const/16 v3, 0x10

    const-string v4, "Digital pen is active"

    const v5, 0x10803f2

    invoke-direct/range {v0 .. v5}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;-><init>(Ljava/lang/String;IILjava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->ACTIVE:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    .line 25
    new-instance v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    const-string v4, "BLOCKED_2_MICS"

    const/16 v6, 0x20

    const-string v7, "2 digital pen microphones are blocked"

    const v8, 0x10803f4

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;-><init>(Ljava/lang/String;IILjava/lang/String;I)V

    sput-object v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->BLOCKED_2_MICS:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    .line 27
    new-instance v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    const-string v4, "BLOCKED_3_OR_MORE_MICS"

    const/16 v6, 0x20

    const-string v7, "3 or more digital pen microphones are blocked"

    const v8, 0x10803f3

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;-><init>(Ljava/lang/String;IILjava/lang/String;I)V

    sput-object v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->BLOCKED_3_OR_MORE_MICS:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    .line 29
    new-instance v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    const-string v4, "LOW_BATTERY"

    const/16 v6, 0x30

    const-string v7, "Digital pen battery is low"

    const v8, 0x10803f5

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;-><init>(Ljava/lang/String;IILjava/lang/String;I)V

    sput-object v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->LOW_BATTERY:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    .line 30
    new-instance v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    const-string v4, "POSITIONING_PROBLEM"

    const/16 v6, 0x40

    const-string v7, "There is a problem determining the location of the pen. Environmental issues or blocked microphones might cause this"

    const v8, 0x10803f6

    move v5, v12

    invoke-direct/range {v3 .. v8}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;-><init>(Ljava/lang/String;IILjava/lang/String;I)V

    sput-object v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->POSITIONING_PROBLEM:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    .line 34
    new-instance v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    const-string v4, "BACKGROUND_LISTENER"

    const/4 v5, 0x5

    const/16 v6, 0x50

    const-string v7, "Background application is recording off-screen pen strokes"

    const v8, 0x10803f7

    invoke-direct/range {v3 .. v8}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;-><init>(Ljava/lang/String;IILjava/lang/String;I)V

    sput-object v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->BACKGROUND_LISTENER:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    .line 23
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    sget-object v1, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->ACTIVE:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->BLOCKED_2_MICS:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    aput-object v1, v0, v9

    sget-object v1, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->BLOCKED_3_OR_MORE_MICS:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    aput-object v1, v0, v10

    sget-object v1, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->LOW_BATTERY:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    aput-object v1, v0, v11

    sget-object v1, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->POSITIONING_PROBLEM:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->BACKGROUND_LISTENER:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->$VALUES:[Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;I)V
    .locals 0
    .param p3, "id"    # I
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "icon"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 44
    iput p3, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->id:I

    .line 45
    iput-object p4, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->title:Ljava/lang/String;

    .line 46
    iput p5, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->icon:I

    .line 47
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 23
    const-class v0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    return-object v0
.end method

.method public static values()[Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->$VALUES:[Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-virtual {v0}, [Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    return-object v0
.end method
