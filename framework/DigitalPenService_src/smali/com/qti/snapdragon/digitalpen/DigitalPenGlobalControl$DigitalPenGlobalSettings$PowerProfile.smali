.class public final enum Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;
.super Ljava/lang/Enum;
.source "DigitalPenGlobalControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PowerProfile"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

.field public static final enum OPTIMIZE_ACCURACY:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

.field public static final enum OPTIMIZE_POWER:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;


# instance fields
.field private final code:B


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 68
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    const-string v1, "OPTIMIZE_ACCURACY"

    invoke-direct {v0, v1, v2, v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;->OPTIMIZE_ACCURACY:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    .line 69
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    const-string v1, "OPTIMIZE_POWER"

    invoke-direct {v0, v1, v3, v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;->OPTIMIZE_POWER:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    .line 67
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    sget-object v1, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;->OPTIMIZE_ACCURACY:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;->OPTIMIZE_POWER:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    aput-object v1, v0, v3

    sput-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;->$VALUES:[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IB)V
    .locals 0
    .param p3, "code"    # B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B)V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 74
    iput-byte p3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;->code:B

    .line 75
    return-void
.end method

.method static synthetic access$400(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;)B
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    .prologue
    .line 67
    iget-byte v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;->code:B

    return v0
.end method

.method public static fromCode(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;
    .locals 5
    .param p0, "code"    # B

    .prologue
    .line 78
    invoke-static {}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;->values()[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    move-result-object v0

    .local v0, "arr$":[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 79
    .local v3, "profile":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;
    iget-byte v4, v3, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;->code:B

    if-ne p0, v4, :cond_0

    .line 83
    .end local v3    # "profile":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;
    :goto_1
    return-object v3

    .line 78
    .restart local v3    # "profile":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 83
    .end local v3    # "profile":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 67
    const-class v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    return-object v0
.end method

.method public static values()[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;->$VALUES:[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    invoke-virtual {v0}, [Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    return-object v0
.end method
