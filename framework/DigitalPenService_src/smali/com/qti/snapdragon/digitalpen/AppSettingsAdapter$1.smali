.class synthetic Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;
.super Ljava/lang/Object;
.source "AppSettingsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$InputType:[I

.field static final synthetic $SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$MouseSensitivity:[I

.field static final synthetic $SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$OffScreenMode:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 124
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;->values()[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$MouseSensitivity:[I

    :try_start_0
    sget-object v0, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$MouseSensitivity:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;->SLOW:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_0
    :try_start_1
    sget-object v0, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$MouseSensitivity:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;->MEDIUM:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_1
    :try_start_2
    sget-object v0, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$MouseSensitivity:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;->FAST:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    .line 101
    :goto_2
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;->values()[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$InputType:[I

    :try_start_3
    sget-object v0, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$InputType:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;->MOUSE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_3
    :try_start_4
    sget-object v0, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$InputType:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;->STYLUS:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    .line 55
    :goto_4
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->values()[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$OffScreenMode:[I

    :try_start_5
    sget-object v0, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$OffScreenMode:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->DISABLED:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_5
    :try_start_6
    sget-object v0, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$OffScreenMode:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->DUPLICATE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_6
    :try_start_7
    sget-object v0, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter$1;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$OffScreenMode:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->EXTEND:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_7
    return-void

    :catch_0
    move-exception v0

    goto :goto_7

    :catch_1
    move-exception v0

    goto :goto_6

    :catch_2
    move-exception v0

    goto :goto_5

    .line 101
    :catch_3
    move-exception v0

    goto :goto_4

    :catch_4
    move-exception v0

    goto :goto_3

    .line 124
    :catch_5
    move-exception v0

    goto :goto_2

    :catch_6
    move-exception v0

    goto :goto_1

    :catch_7
    move-exception v0

    goto :goto_0
.end method
