.class public Lcom/qti/snapdragon/digitalpen/ConfigManager;
.super Ljava/lang/Object;
.source "ConfigManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/ConfigManager$1;,
        Lcom/qti/snapdragon/digitalpen/ConfigManager$State;,
        Lcom/qti/snapdragon/digitalpen/ConfigManager$ConfigChangedListener;
    }
.end annotation


# instance fields
.field private appSettings:Landroid/os/Bundle;

.field private backgroundSettings:Landroid/os/Bundle;

.field private globalConfig:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

.field private listener:Lcom/qti/snapdragon/digitalpen/ConfigManager$ConfigChangedListener;

.field private state:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;


# direct methods
.method public constructor <init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/ConfigManager$ConfigChangedListener;)V
    .locals 1
    .param p1, "globalConfig"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .param p2, "listener"    # Lcom/qti/snapdragon/digitalpen/ConfigManager$ConfigChangedListener;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->appSettings:Landroid/os/Bundle;

    .line 57
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->globalConfig:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 58
    iput-object p2, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->listener:Lcom/qti/snapdragon/digitalpen/ConfigManager$ConfigChangedListener;

    .line 59
    sget-object v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->LEGACY:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->state:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    .line 60
    return-void
.end method

.method private appHasBackgroundListener()Z
    .locals 3

    .prologue
    .line 116
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->appSettings:Landroid/os/Bundle;

    const-string v1, "OffScreenBackgroundListener"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private overlaySettingsOnGlobalConfig(Landroid/os/Bundle;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 1
    .param p1, "settings"    # Landroid/os/Bundle;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->globalConfig:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-static {v0, p1}, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter;->applyAppSettings(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Landroid/os/Bundle;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v0

    return-object v0
.end method

.method private updateListener(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/ConfigManager$State;)V
    .locals 3
    .param p1, "config"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .param p2, "newState"    # Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    .prologue
    .line 124
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->state:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    if-eq v1, p2, :cond_0

    const/4 v0, 0x1

    .line 125
    .local v0, "stateChanged":Z
    :goto_0
    iput-object p2, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->state:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    .line 126
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->listener:Lcom/qti/snapdragon/digitalpen/ConfigManager$ConfigChangedListener;

    invoke-static {p1}, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter;->copyOfConfig(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v2

    invoke-interface {v1, v2, p2, v0}, Lcom/qti/snapdragon/digitalpen/ConfigManager$ConfigChangedListener;->onConfigChanged(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/ConfigManager$State;Z)V

    .line 127
    return-void

    .line 124
    .end local v0    # "stateChanged":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public appNowBackground()V
    .locals 0

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->releaseApp()V

    .line 82
    return-void
.end method

.method public applyAppSettings(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "settings"    # Landroid/os/Bundle;

    .prologue
    .line 63
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->appSettings:Landroid/os/Bundle;

    invoke-virtual {v2}, Landroid/os/Bundle;->clear()V

    .line 64
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->appSettings:Landroid/os/Bundle;

    invoke-virtual {v2, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 68
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->appHasBackgroundListener()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 70
    invoke-static {p1}, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter;->copyForBackgroundOffScreenListener(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    iput-object v2, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->backgroundSettings:Landroid/os/Bundle;

    .line 71
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->backgroundSettings:Landroid/os/Bundle;

    .line 72
    .local v1, "overlay":Landroid/os/Bundle;
    sget-object v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->LEGACY_WITH_SIDE_CHANNEL:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    .line 77
    .local v0, "newState":Lcom/qti/snapdragon/digitalpen/ConfigManager$State;
    :goto_0
    invoke-direct {p0, v1}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->overlaySettingsOnGlobalConfig(Landroid/os/Bundle;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v2

    invoke-direct {p0, v2, v0}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->updateListener(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/ConfigManager$State;)V

    .line 78
    return-void

    .line 74
    .end local v0    # "newState":Lcom/qti/snapdragon/digitalpen/ConfigManager$State;
    .end local v1    # "overlay":Landroid/os/Bundle;
    :cond_0
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->appSettings:Landroid/os/Bundle;

    .line 75
    .restart local v1    # "overlay":Landroid/os/Bundle;
    sget-object v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->APP:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    .restart local v0    # "newState":Lcom/qti/snapdragon/digitalpen/ConfigManager$State;
    goto :goto_0
.end method

.method public getCurrentConfig()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->appSettings:Landroid/os/Bundle;

    invoke-direct {p0, v0}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->overlaySettingsOnGlobalConfig(Landroid/os/Bundle;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v0

    return-object v0
.end method

.method public getGlobalConfig()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->globalConfig:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-static {v0}, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter;->copyOfConfig(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v0

    return-object v0
.end method

.method public releaseApp()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->globalConfig:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    sget-object v1, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->LEGACY:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    invoke-direct {p0, v0, v1}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->updateListener(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/ConfigManager$State;)V

    .line 94
    return-void
.end method

.method public setGlobalConfig(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V
    .locals 3
    .param p1, "newGlobalConfig"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .prologue
    .line 97
    invoke-static {p1}, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter;->copyOfConfig(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v1

    iput-object v1, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->globalConfig:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 99
    sget-object v1, Lcom/qti/snapdragon/digitalpen/ConfigManager$1;->$SwitchMap$com$qti$snapdragon$digitalpen$ConfigManager$State:[I

    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->state:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    invoke-virtual {v2}, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 110
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Programming error: state missing"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 101
    :pswitch_0
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->globalConfig:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->appSettings:Landroid/os/Bundle;

    invoke-static {v1, v2}, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter;->applyAppSettings(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Landroid/os/Bundle;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v0

    .line 112
    .local v0, "newConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    :goto_0
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->state:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    invoke-direct {p0, v0, v1}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->updateListener(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/ConfigManager$State;)V

    .line 113
    return-void

    .line 104
    .end local v0    # "newConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    :pswitch_1
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->globalConfig:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 105
    .restart local v0    # "newConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    goto :goto_0

    .line 107
    .end local v0    # "newConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    :pswitch_2
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->globalConfig:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/ConfigManager;->backgroundSettings:Landroid/os/Bundle;

    invoke-static {v1, v2}, Lcom/qti/snapdragon/digitalpen/AppSettingsAdapter;->applyAppSettings(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Landroid/os/Bundle;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v0

    .line 108
    .restart local v0    # "newConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    goto :goto_0

    .line 99
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
