.class public Lcom/qti/snapdragon/digitalpen/ServiceNotifications;
.super Ljava/lang/Object;
.source "ServiceNotifications.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCurrentPowerState:I

.field private mIsBadScenario:Z

.field private mIsSpur:Z

.field private mNM:Landroid/app/NotificationManager;

.field private mPenEnabled:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-boolean v8, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mIsSpur:Z

    .line 52
    iput-boolean v8, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mIsBadScenario:Z

    .line 55
    const/4 v6, 0x3

    iput v6, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mCurrentPowerState:I

    .line 58
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mContext:Landroid/content/Context;

    .line 59
    iget-object v6, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mContext:Landroid/content/Context;

    const-string v7, "notification"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/NotificationManager;

    iput-object v6, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mNM:Landroid/app/NotificationManager;

    .line 63
    new-instance v3, Landroid/content/Intent;

    iget-object v6, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mContext:Landroid/content/Context;

    const-class v7, Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-direct {v3, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 64
    .local v3, "intent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mContext:Landroid/content/Context;

    invoke-static {v6, v8, v3, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 66
    .local v1, "contentIntent":Landroid/app/PendingIntent;
    invoke-static {}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->values()[Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    move-result-object v0

    .local v0, "arr$":[Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v0, v2

    .line 68
    .local v5, "pn":Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;
    new-instance v6, Landroid/app/Notification$Builder;

    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    iget v7, v5, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->icon:I

    invoke-virtual {v6, v7}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v6

    iget-object v7, v5, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->title:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v6

    iput-object v6, v5, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->builder:Landroid/app/Notification$Builder;

    .line 66
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 73
    .end local v5    # "pn":Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;
    :cond_0
    return-void
.end method

.method private cancelAllNotifications()V
    .locals 6

    .prologue
    .line 114
    invoke-static {}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->values()[Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    move-result-object v0

    .local v0, "arr$":[Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 115
    .local v3, "notification":Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;
    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mNM:Landroid/app/NotificationManager;

    iget v5, v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->id:I

    invoke-virtual {v4, v5}, Landroid/app/NotificationManager;->cancel(I)V

    .line 114
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 117
    .end local v3    # "notification":Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;
    :cond_0
    return-void
.end method

.method private removeBatteryLevelInformation()V
    .locals 2

    .prologue
    .line 224
    sget-object v0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->ACTIVE:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->builder:Landroid/app/Notification$Builder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 225
    iget-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mPenEnabled:Z

    if-eqz v0, :cond_0

    .line 226
    sget-object v0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->ACTIVE:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-virtual {p0, v0}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->showNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V

    .line 228
    :cond_0
    return-void
.end method

.method private removeNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V
    .locals 4
    .param p1, "notification"    # Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    .prologue
    .line 124
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 125
    .local v0, "oldId":J
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mNM:Landroid/app/NotificationManager;

    iget v3, p1, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->id:I

    invoke-virtual {v2, v3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 126
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 127
    return-void
.end method

.method private updateBatteryLevelInformation(I)V
    .locals 3
    .param p1, "batteryLevel"    # I

    .prologue
    .line 216
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Battery level: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 217
    .local v0, "batteryString":Ljava/lang/String;
    sget-object v1, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->ACTIVE:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    iget-object v1, v1, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->builder:Landroid/app/Notification$Builder;

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 218
    iget-boolean v1, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mPenEnabled:Z

    if-eqz v1, :cond_0

    .line 219
    sget-object v1, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->ACTIVE:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-virtual {p0, v1}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->showNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V

    .line 221
    :cond_0
    return-void
.end method


# virtual methods
.method public backgroundListenerEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 231
    if-eqz p1, :cond_0

    .line 232
    sget-object v0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->BACKGROUND_LISTENER:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-virtual {p0, v0}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->showNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V

    .line 236
    :goto_0
    return-void

    .line 234
    :cond_0
    sget-object v0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->BACKGROUND_LISTENER:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-direct {p0, v0}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->removeNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V

    goto :goto_0
.end method

.method protected doNotify(ILandroid/app/Notification;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "notification"    # Landroid/app/Notification;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mNM:Landroid/app/NotificationManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 86
    return-void
.end method

.method public notifyPenDisabled()V
    .locals 3

    .prologue
    .line 101
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mPenEnabled:Z

    .line 102
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 103
    .local v0, "oldId":J
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->cancelAllNotifications()V

    .line 104
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 105
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->removeBatteryLevelInformation()V

    .line 106
    return-void
.end method

.method public notifyPenEnabled()V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mPenEnabled:Z

    .line 93
    sget-object v0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->ACTIVE:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-virtual {p0, v0}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->showNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V

    .line 94
    return-void
.end method

.method public sendEvent(I[I)V
    .locals 6
    .param p1, "eventType"    # I
    .param p2, "params"    # [I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 131
    const/4 v3, 0x2

    if-ne v3, p1, :cond_0

    .line 132
    aget v1, p2, v4

    .line 133
    .local v1, "numMicsBlocked":I
    packed-switch v1, :pswitch_data_0

    .line 147
    sget-object v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->BLOCKED_3_OR_MORE_MICS:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-virtual {p0, v3}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->showNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V

    .line 153
    .end local v1    # "numMicsBlocked":I
    :cond_0
    :goto_0
    const/4 v3, 0x4

    if-ne v3, p1, :cond_2

    .line 154
    aget v3, p2, v4

    packed-switch v3, :pswitch_data_1

    .line 162
    :goto_1
    aget v0, p2, v5

    .line 163
    .local v0, "batteryLevel":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    iget v3, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mCurrentPowerState:I

    if-eqz v3, :cond_6

    .line 165
    :cond_1
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->removeBatteryLevelInformation()V

    .line 173
    .end local v0    # "batteryLevel":I
    :cond_2
    :goto_2
    if-nez p1, :cond_3

    .line 174
    aget v3, p2, v4

    iput v3, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mCurrentPowerState:I

    .line 175
    aget v3, p2, v4

    if-eqz v3, :cond_3

    .line 176
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->removeBatteryLevelInformation()V

    .line 181
    :cond_3
    const/16 v3, 0x8

    if-ne v3, p1, :cond_4

    .line 182
    aget v3, p2, v4

    packed-switch v3, :pswitch_data_2

    .line 197
    :cond_4
    :goto_3
    const/16 v3, 0x10

    if-ne v3, p1, :cond_5

    .line 198
    aget v2, p2, v4

    .line 199
    .local v2, "scenario":I
    packed-switch v2, :pswitch_data_3

    .line 213
    .end local v2    # "scenario":I
    :cond_5
    :goto_4
    return-void

    .line 140
    .restart local v1    # "numMicsBlocked":I
    :pswitch_0
    sget-object v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->BLOCKED_2_MICS:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-direct {p0, v3}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->removeNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V

    goto :goto_0

    .line 144
    :pswitch_1
    sget-object v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->BLOCKED_2_MICS:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-virtual {p0, v3}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->showNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V

    goto :goto_0

    .line 156
    .end local v1    # "numMicsBlocked":I
    :pswitch_2
    sget-object v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->LOW_BATTERY:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-direct {p0, v3}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->removeNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V

    goto :goto_1

    .line 159
    :pswitch_3
    sget-object v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->LOW_BATTERY:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-virtual {p0, v3}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->showNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V

    goto :goto_1

    .line 167
    .restart local v0    # "batteryLevel":I
    :cond_6
    invoke-direct {p0, v0}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->updateBatteryLevelInformation(I)V

    goto :goto_2

    .line 184
    .end local v0    # "batteryLevel":I
    :pswitch_4
    iput-boolean v4, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mIsSpur:Z

    .line 185
    iget-boolean v3, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mIsBadScenario:Z

    if-nez v3, :cond_4

    .line 186
    sget-object v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->POSITIONING_PROBLEM:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-direct {p0, v3}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->removeNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V

    goto :goto_3

    .line 190
    :pswitch_5
    iput-boolean v5, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mIsSpur:Z

    .line 191
    sget-object v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->POSITIONING_PROBLEM:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-virtual {p0, v3}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->showNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V

    goto :goto_3

    .line 201
    .restart local v2    # "scenario":I
    :pswitch_6
    iput-boolean v4, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mIsBadScenario:Z

    .line 202
    iget-boolean v3, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mIsSpur:Z

    if-nez v3, :cond_5

    .line 203
    sget-object v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->POSITIONING_PROBLEM:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-direct {p0, v3}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->removeNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V

    goto :goto_4

    .line 208
    :pswitch_7
    iput-boolean v5, p0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->mIsBadScenario:Z

    .line 209
    sget-object v3, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->POSITIONING_PROBLEM:Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    invoke-virtual {p0, v3}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->showNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V

    goto :goto_4

    .line 133
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 154
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 182
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 199
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method protected showNotification(Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;)V
    .locals 4
    .param p1, "penNotification"    # Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;

    .prologue
    .line 79
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 80
    .local v0, "oldId":J
    iget v2, p1, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->id:I

    iget-object v3, p1, Lcom/qti/snapdragon/digitalpen/ServiceNotifications$PenNotification;->builder:Landroid/app/Notification$Builder;

    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->doNotify(ILandroid/app/Notification;)V

    .line 81
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 82
    return-void
.end method
