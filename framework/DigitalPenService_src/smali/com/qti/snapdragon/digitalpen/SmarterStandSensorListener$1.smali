.class Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$1;
.super Ljava/lang/Object;
.source "SmarterStandSensorListener.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;


# direct methods
.method constructor <init>(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$1;->this$0:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getRotationAngleOfSupportedOrientation(FFF)D
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 72
    const-wide/16 v0, 0x0

    .line 73
    .local v0, "atanArgument":D
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$1;->this$0:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    # getter for: Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->supportedOrientation:I
    invoke-static {v2}, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->access$300(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 87
    const-string v2, "SmarterStandSensorListener"

    const-string v3, "Invalid orientation for accelerometer angle"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    return-wide v2

    .line 75
    :pswitch_0
    div-float v2, p2, p3

    float-to-double v0, v2

    .line 76
    goto :goto_0

    .line 78
    :pswitch_1
    div-float v2, p1, p3

    float-to-double v0, v2

    .line 79
    goto :goto_0

    .line 81
    :pswitch_2
    neg-float v2, p2

    div-float/2addr v2, p3

    float-to-double v0, v2

    .line 82
    goto :goto_0

    .line 84
    :pswitch_3
    neg-float v2, p1

    div-float/2addr v2, p3

    float-to-double v0, v2

    .line 85
    goto :goto_0

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method getRotationAngle(FFF)D
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 59
    const/4 v2, 0x0

    cmpl-float v2, v2, p3

    if-nez v2, :cond_1

    .line 60
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 67
    :cond_0
    :goto_0
    return-wide v0

    .line 63
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$1;->getRotationAngleOfSupportedOrientation(FFF)D

    move-result-wide v0

    .line 64
    .local v0, "rotationAngle":D
    const-wide/16 v2, 0x0

    cmpl-double v2, v2, v0

    if-lez v2, :cond_0

    .line 65
    const-wide v2, 0x4076800000000000L    # 360.0

    add-double/2addr v0, v2

    goto :goto_0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 56
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 8
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 42
    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    invoke-virtual {p0, v2, v3, v4}, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$1;->getRotationAngle(FFF)D

    move-result-wide v0

    .line 44
    .local v0, "rotationAngle":D
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpl-double v2, v2, v0

    if-eqz v2, :cond_0

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$1;->this$0:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    iget-object v5, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$1;->this$0:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    # getter for: Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->prevReportedAngle:D
    invoke-static {v5}, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->access$000(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;)D

    move-result-wide v6

    # invokes: Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->diff(DD)D
    invoke-static {v4, v0, v1, v6, v7}, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->access$100(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;DD)D

    move-result-wide v4

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 46
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$1;->this$0:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    # getter for: Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->callback:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$AccelerometerChangeCallback;
    invoke-static {v2}, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->access$200(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;)Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$AccelerometerChangeCallback;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$AccelerometerChangeCallback;->onNewPosition(D)V

    .line 47
    const-string v2, "SmarterStandSensorListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Rotation angle: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$1;->this$0:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    # setter for: Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->prevReportedAngle:D
    invoke-static {v2, v0, v1}, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->access$002(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;D)D

    .line 51
    :cond_0
    return-void
.end method
