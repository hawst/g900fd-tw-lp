.class Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;
.super Ljava/lang/Object;
.source "SidebandConfigChanger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ConfigField"
.end annotation


# instance fields
.field private doCommand:Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigCommand;

.field private fieldName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigCommand;)V
    .locals 0
    .param p1, "fieldName"    # Ljava/lang/String;
    .param p2, "doCommand"    # Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigCommand;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;->fieldName:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;->doCommand:Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigCommand;

    .line 39
    return-void
.end method


# virtual methods
.method public checkAndDoCommand(Landroid/os/Bundle;Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V
    .locals 3
    .param p1, "b"    # Landroid/os/Bundle;
    .param p2, "config"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .prologue
    .line 42
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "com.qti.snapdragon.digitalpen."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;->fieldName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "key":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;->doCommand:Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigCommand;

    invoke-interface {v1, p1, v0, p2}, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigCommand;->execute(Landroid/os/Bundle;Ljava/lang/String;Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V

    .line 46
    :cond_0
    return-void
.end method
