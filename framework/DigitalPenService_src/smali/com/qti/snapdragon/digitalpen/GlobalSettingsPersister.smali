.class public Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;
.super Ljava/lang/Object;
.source "GlobalSettingsPersister.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private final settingsFilename:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "settingsFilename"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, "GlobalSettingsPersister"

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    .line 54
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->settingsFilename:Ljava/lang/String;

    .line 55
    return-void
.end method

.method private getFileFromSymlink(Ljava/lang/String;)Landroid/util/AtomicFile;
    .locals 3
    .param p1, "settingsFilename"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->getFileLocationFromLinkFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 59
    .local v1, "settingsFile":Ljava/lang/String;
    new-instance v0, Landroid/util/AtomicFile;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v2}, Landroid/util/AtomicFile;-><init>(Ljava/io/File;)V

    .line 60
    .local v0, "atomicSettingsFile":Landroid/util/AtomicFile;
    return-object v0
.end method

.method private getFileLocationFromLinkFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "linkFile"    # Ljava/lang/String;

    .prologue
    .line 31
    :try_start_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 34
    .local v4, "targetFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_0

    .line 35
    move-object v0, v4

    .line 40
    .local v0, "canon":Ljava/io/File;
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v3, 0x1

    .line 42
    .local v3, "isSymbolicLink":Z
    :goto_1
    if-nez v3, :cond_2

    .line 43
    iget-object v5, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Link File: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is not a symbolic link"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    .end local v0    # "canon":Ljava/io/File;
    .end local v3    # "isSymbolicLink":Z
    .end local v4    # "targetFile":Ljava/io/File;
    .end local p1    # "linkFile":Ljava/lang/String;
    :goto_2
    return-object p1

    .line 37
    .restart local v4    # "targetFile":Ljava/io/File;
    .restart local p1    # "linkFile":Ljava/lang/String;
    :cond_0
    invoke-virtual {v4}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getCanonicalFile()Ljava/io/File;

    move-result-object v1

    .line 38
    .local v1, "canonDir":Ljava/io/File;
    new-instance v0, Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v1, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .restart local v0    # "canon":Ljava/io/File;
    goto :goto_0

    .line 40
    .end local v1    # "canonDir":Ljava/io/File;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 46
    .restart local v3    # "isSymbolicLink":Z
    :cond_2
    invoke-virtual {v4}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto :goto_2

    .line 47
    .end local v0    # "canon":Ljava/io/File;
    .end local v3    # "isSymbolicLink":Z
    .end local v4    # "targetFile":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 48
    .local v2, "e":Ljava/io/IOException;
    iget-object v5, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Error: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method public deserialize()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 10

    .prologue
    .line 64
    new-instance v6, Lorg/simpleframework/xml/core/Persister;

    invoke-direct {v6}, Lorg/simpleframework/xml/core/Persister;-><init>()V

    .line 65
    .local v6, "serializer":Lorg/simpleframework/xml/Serializer;
    const/4 v0, 0x0

    .line 66
    .local v0, "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    const/4 v4, 0x0

    .line 67
    .local v4, "is":Ljava/io/InputStream;
    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->settingsFilename:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->getFileFromSymlink(Ljava/lang/String;)Landroid/util/AtomicFile;

    move-result-object v3

    .line 69
    .local v3, "globalSettingsFile":Landroid/util/AtomicFile;
    :try_start_0
    invoke-virtual {v3}, Landroid/util/AtomicFile;->openRead()Ljava/io/FileInputStream;

    move-result-object v4

    .line 70
    const-class v7, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;

    invoke-interface {v6, v7, v4}, Lorg/simpleframework/xml/Serializer;->read(Ljava/lang/Class;Ljava/io/InputStream;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;

    .line 71
    .local v5, "mirror":Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;
    if-eqz v5, :cond_0

    .line 72
    new-instance v1, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-direct {v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;-><init>()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    .end local v0    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .local v1, "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    :try_start_1
    sget-object v7, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$CopyDirection;->MIRROR_TO_TRUE:Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$CopyDirection;

    invoke-virtual {v5, v1, v7}, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;->copyPersistedFields(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$CopyDirection;)V

    .line 74
    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    const-string v8, "Successfully deserialized xml"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v0, v1

    .line 84
    .end local v1    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .restart local v0    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    :cond_0
    if-eqz v4, :cond_1

    .line 85
    :try_start_2
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 92
    .end local v5    # "mirror":Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;
    :cond_1
    :goto_0
    if-nez v0, :cond_2

    .line 93
    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error deserializing XML: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->settingsFilename:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " getting default DigitalPenConfig"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .end local v0    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    invoke-direct {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;-><init>()V

    .line 97
    .restart local v0    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    :cond_2
    return-object v0

    .line 87
    .restart local v5    # "mirror":Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;
    :catch_0
    move-exception v2

    .line 88
    .local v2, "e":Ljava/io/IOException;
    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    const-string v8, "IOException while closing xml"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 76
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "mirror":Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;
    :catch_1
    move-exception v2

    .line 77
    .local v2, "e":Ljava/io/FileNotFoundException;
    :goto_1
    :try_start_3
    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    const-string v8, "FileNotFoundException while opening xml"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 84
    if-eqz v4, :cond_1

    .line 85
    :try_start_4
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 87
    :catch_2
    move-exception v2

    .line 88
    .local v2, "e":Ljava/io/IOException;
    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    const-string v8, "IOException while closing xml"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 79
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 80
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    const-string v8, "Exception while deserializing xml"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 84
    if-eqz v4, :cond_1

    .line 85
    :try_start_6
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 87
    :catch_4
    move-exception v2

    .line 88
    .local v2, "e":Ljava/io/IOException;
    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    const-string v8, "IOException while closing xml"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 83
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    .line 84
    :goto_3
    if-eqz v4, :cond_3

    .line 85
    :try_start_7
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 90
    :cond_3
    :goto_4
    throw v7

    .line 87
    :catch_5
    move-exception v2

    .line 88
    .restart local v2    # "e":Ljava/io/IOException;
    iget-object v8, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    const-string v9, "IOException while closing xml"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 83
    .end local v0    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .restart local v5    # "mirror":Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;
    :catchall_1
    move-exception v7

    move-object v0, v1

    .end local v1    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .restart local v0    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    goto :goto_3

    .line 79
    .end local v0    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .restart local v1    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    :catch_6
    move-exception v2

    move-object v0, v1

    .end local v1    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .restart local v0    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    goto :goto_2

    .line 76
    .end local v0    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .restart local v1    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    :catch_7
    move-exception v2

    move-object v0, v1

    .end local v1    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .restart local v0    # "digitalPenConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    goto :goto_1
.end method

.method public serialize(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V
    .locals 10
    .param p1, "config"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .prologue
    .line 102
    new-instance v5, Lorg/simpleframework/xml/core/Persister;

    invoke-direct {v5}, Lorg/simpleframework/xml/core/Persister;-><init>()V

    .line 103
    .local v5, "serializer":Lorg/simpleframework/xml/Serializer;
    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->settingsFilename:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->getFileFromSymlink(Ljava/lang/String;)Landroid/util/AtomicFile;

    move-result-object v2

    .line 105
    .local v2, "globalSettingsFile":Landroid/util/AtomicFile;
    :try_start_0
    invoke-virtual {v2}, Landroid/util/AtomicFile;->startWrite()Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 106
    .local v4, "os":Ljava/io/FileOutputStream;
    const/4 v6, 0x0

    .line 108
    .local v6, "success":Z
    :try_start_1
    new-instance v3, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;

    invoke-direct {v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;-><init>()V

    .line 109
    .local v3, "mirror":Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;
    sget-object v7, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$CopyDirection;->TRUE_TO_MIRROR:Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$CopyDirection;

    invoke-virtual {v3, p1, v7}, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;->copyPersistedFields(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$CopyDirection;)V

    .line 110
    invoke-interface {v5, v3, v4}, Lorg/simpleframework/xml/Serializer;->write(Ljava/lang/Object;Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    const/4 v6, 0x1

    .line 116
    if-eqz v6, :cond_0

    .line 117
    :try_start_2
    invoke-virtual {v2, v4}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V

    .line 118
    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    const-string v8, "Successfully serialized xml"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    .end local v3    # "mirror":Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;
    .end local v4    # "os":Ljava/io/FileOutputStream;
    .end local v6    # "success":Z
    :goto_0
    return-void

    .line 120
    .restart local v3    # "mirror":Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;
    .restart local v4    # "os":Ljava/io/FileOutputStream;
    .restart local v6    # "success":Z
    :cond_0
    invoke-virtual {v2, v4}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 123
    .end local v3    # "mirror":Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;
    .end local v4    # "os":Ljava/io/FileOutputStream;
    .end local v6    # "success":Z
    :catch_0
    move-exception v1

    .line 124
    .local v1, "ex":Ljava/io/IOException;
    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    const-string v8, "Failed to save xml."

    invoke-static {v7, v8, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 112
    .end local v1    # "ex":Ljava/io/IOException;
    .restart local v4    # "os":Ljava/io/FileOutputStream;
    .restart local v6    # "success":Z
    :catch_1
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    const-string v8, "Exception while serializing xml"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 116
    if-eqz v6, :cond_1

    .line 117
    :try_start_4
    invoke-virtual {v2, v4}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V

    .line 118
    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    const-string v8, "Successfully serialized xml"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 120
    :cond_1
    invoke-virtual {v2, v4}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V

    goto :goto_0

    .line 116
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    if-eqz v6, :cond_2

    .line 117
    invoke-virtual {v2, v4}, Landroid/util/AtomicFile;->finishWrite(Ljava/io/FileOutputStream;)V

    .line 118
    iget-object v8, p0, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->TAG:Ljava/lang/String;

    const-string v9, "Successfully serialized xml"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :goto_1
    throw v7

    :cond_2
    invoke-virtual {v2, v4}, Landroid/util/AtomicFile;->failWrite(Ljava/io/FileOutputStream;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1
.end method
