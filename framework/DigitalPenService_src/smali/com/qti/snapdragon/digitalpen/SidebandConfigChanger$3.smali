.class final Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$3;
.super Ljava/lang/Object;
.source "SidebandConfigChanger.java"

# interfaces
.implements Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Landroid/os/Bundle;Ljava/lang/String;Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V
    .locals 8
    .param p1, "b"    # Landroid/os/Bundle;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "config"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .prologue
    const/16 v7, 0x9

    const/4 v6, 0x6

    const/4 v5, 0x3

    .line 76
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v0

    .line 77
    .local v0, "coords":[F
    array-length v4, v0

    if-eq v4, v7, :cond_0

    .line 78
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "For key "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", expected "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "array of length 9, received length "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 81
    :cond_0
    const/4 v4, 0x0

    invoke-static {v0, v4, v5}, Ljava/util/Arrays;->copyOfRange([FII)[F

    move-result-object v3

    .line 82
    .local v3, "origin":[F
    invoke-static {v0, v5, v6}, Ljava/util/Arrays;->copyOfRange([FII)[F

    move-result-object v1

    .line 83
    .local v1, "endX":[F
    invoke-static {v0, v6, v7}, Ljava/util/Arrays;->copyOfRange([FII)[F

    move-result-object v2

    .line 84
    .local v2, "endY":[F
    invoke-virtual {p3, v3, v1, v2}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setOffScreenPlane([F[F[F)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 85
    return-void
.end method
