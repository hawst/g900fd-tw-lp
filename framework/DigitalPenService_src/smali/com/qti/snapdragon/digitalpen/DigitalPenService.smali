.class public Lcom/qti/snapdragon/digitalpen/DigitalPenService;
.super Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;
.source "DigitalPenService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/DigitalPenService$13;,
        Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;,
        Lcom/qti/snapdragon/digitalpen/DigitalPenService$CheckApplicationStillForeground;,
        Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;,
        Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;
    }
.end annotation


# static fields
.field private static final CAPABILITIES_TABLE:[Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;

.field private static final COMMAND_FILENAME:Ljava/lang/String; = "command.txt"

.field private static final CONTROL_SOCKET_PATH:Ljava/lang/String; = "/data/usf/epos/control_socket"

.field private static final DAEMON_STOPPED:I = 0x0

.field private static final DATA_SOCKET_PATH:Ljava/lang/String; = "/data/usf/epos/data_socket"

.field private static final EPOS_PATH:Ljava/lang/String; = "/data/usf/epos/"

.field private static final GENERATE_ON_SCREEN_DATA_ACTION:Ljava/lang/String; = "com.qti.snapdragon.digitalpen.GenerateOnScreenData"

.field private static final LOAD_CONFIG_ACTION:Ljava/lang/String; = "com.qti.snapdragon.digitalpen.LOAD_CONFIG"

.field private static final MSG_ID_ACCEL_UPDATE:I = 0x2

.field private static final MSG_ID_LOAD_CONFIG:I = 0x1

.field private static final NO_APP_PID:I = 0x0

.field private static final SERVICE_SETTINGS_SYMLINK_FILENAME:Ljava/lang/String; = "service_settings.xml"

.field private static final SIDE_CHANNEL_CAPABLE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "DigitalPenService"

.field private static final VERSION_MAJOR:I = 0x87

.field private static final VERSION_MINOR:I = 0x0

.field private static mDaemonEnabled:Z = false

.field private static mFeatureEnabled:Z = false

.field public static mRefs:I = 0x0

.field private static final pidDirectory:Ljava/lang/String; = "/data/usf/"

.field private static final pidFileExtention:Ljava/lang/String; = ".pid"


# instance fields
.field final DAEMON_START_FAILURE_MAX_TIME:I

.field final LONG_SLEEP_TIME:I

.field final SHORT_SLEEP_TIME:I

.field private cachedCurrentPowerState:I

.field private cachedMicsBlocked:I

.field private cachedPenBatteryLevel:I

.field private cachedPenBatteryState:I

.field private cachedPreviousPowerState:I

.field private mActivityManager:Landroid/app/ActivityManager;

.field private mAppPid:I

.field private mAppPidPoller:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field private mAppPidPollerTask:Ljava/util/concurrent/ScheduledFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ScheduledFuture",
            "<*>;"
        }
    .end annotation
.end field

.field private mConfigChangeListener:Lcom/qti/snapdragon/digitalpen/ConfigManager$ConfigChangedListener;

.field private mConfigManager:Lcom/qti/snapdragon/digitalpen/ConfigManager;

.field private mContext:Landroid/content/Context;

.field private mControlSocketListener:Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker$OnEventListener;

.field private mControlSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

.field private mCurrentState:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

.field private final mDaemonName:Ljava/lang/String;

.field private mDaemonPollingThread:Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;

.field private final mDataCallback:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final mDataSocketListener:Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker$OnDataListener;

.field private mDataSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

.field private mDimensionsCallback:Lcodeaurora/ultrasound/IDigitalPenDimensionsCallback;

.field private mEposDirObserver:Lcom/qti/snapdragon/digitalpen/DirectoryObserver;

.field private final mEventCallback:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mGlobalSettingsPersister:Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;

.field private final mHandler:Landroid/os/Handler;

.field private mIsFilteringData:Z

.field mScreenState:Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;

.field private mServiceNotifications:Lcom/qti/snapdragon/digitalpen/ServiceNotifications;

.field private mSmarterStandSensorListener:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

.field private mStatusBarOverlayView:Landroid/view/View;

.field private mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 83
    new-array v0, v3, [Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;

    new-instance v1, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;

    const/16 v2, 0x20

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-direct {v1, v2, v3}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;-><init>(I[I)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;

    const/16 v2, 0x40

    new-array v3, v5, [I

    aput v5, v3, v4

    invoke-direct {v1, v2, v3}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;-><init>(I[I)V

    aput-object v1, v0, v5

    sput-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->CAPABILITIES_TABLE:[Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;

    .line 235
    sput-boolean v4, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDaemonEnabled:Z

    .line 318
    const-string v0, "DigitalPenService"

    const-string v1, "Loaded DigitalPenService library"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    sget v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mRefs:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mRefs:I

    .line 325
    return-void

    .line 83
    :array_0
    .array-data 4
        0x87
        0x0
    .end array-data
.end method

.method protected constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0x5dc

    const/4 v1, 0x3

    const/4 v3, 0x0

    .line 376
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;-><init>()V

    .line 107
    const-string v0, "usf_epos"

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDaemonName:Ljava/lang/String;

    .line 110
    iput v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedCurrentPowerState:I

    .line 111
    iput v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPreviousPowerState:I

    .line 112
    iput v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedMicsBlocked:I

    .line 113
    iput v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPenBatteryState:I

    .line 114
    const/4 v0, -0x1

    iput v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPenBatteryLevel:I

    .line 117
    sget-object v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->LEGACY:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mCurrentState:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    .line 119
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$1;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$1;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigChangeListener:Lcom/qti/snapdragon/digitalpen/ConfigManager$ConfigChangedListener;

    .line 209
    new-instance v0, Lcom/qti/snapdragon/digitalpen/ConfigManager;

    new-instance v1, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-direct {v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;-><init>()V

    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigChangeListener:Lcom/qti/snapdragon/digitalpen/ConfigManager$ConfigChangedListener;

    invoke-direct {v0, v1, v2}, Lcom/qti/snapdragon/digitalpen/ConfigManager;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/ConfigManager$ConfigChangedListener;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigManager:Lcom/qti/snapdragon/digitalpen/ConfigManager;

    .line 224
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataCallback:Landroid/os/RemoteCallbackList;

    .line 225
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mEventCallback:Landroid/os/RemoteCallbackList;

    .line 315
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;Lcom/qti/snapdragon/digitalpen/DigitalPenService$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mScreenState:Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;

    .line 518
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->SHORT_SLEEP_TIME:I

    .line 519
    iput v4, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->LONG_SLEEP_TIME:I

    .line 522
    iput v4, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->DAEMON_START_FAILURE_MAX_TIME:I

    .line 525
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mHandler:Landroid/os/Handler;

    .line 563
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPidPoller:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 567
    iput v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPid:I

    .line 571
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$7;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$7;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataSocketListener:Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker$OnDataListener;

    .line 586
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$8;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$8;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mControlSocketListener:Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker$OnEventListener;

    .line 378
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v4, 0x5dc

    const/4 v1, 0x3

    const/4 v3, 0x0

    .line 372
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;-><init>()V

    .line 107
    const-string v0, "usf_epos"

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDaemonName:Ljava/lang/String;

    .line 110
    iput v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedCurrentPowerState:I

    .line 111
    iput v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPreviousPowerState:I

    .line 112
    iput v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedMicsBlocked:I

    .line 113
    iput v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPenBatteryState:I

    .line 114
    const/4 v0, -0x1

    iput v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPenBatteryLevel:I

    .line 117
    sget-object v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->LEGACY:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mCurrentState:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    .line 119
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$1;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$1;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigChangeListener:Lcom/qti/snapdragon/digitalpen/ConfigManager$ConfigChangedListener;

    .line 209
    new-instance v0, Lcom/qti/snapdragon/digitalpen/ConfigManager;

    new-instance v1, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-direct {v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;-><init>()V

    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigChangeListener:Lcom/qti/snapdragon/digitalpen/ConfigManager$ConfigChangedListener;

    invoke-direct {v0, v1, v2}, Lcom/qti/snapdragon/digitalpen/ConfigManager;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/ConfigManager$ConfigChangedListener;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigManager:Lcom/qti/snapdragon/digitalpen/ConfigManager;

    .line 224
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataCallback:Landroid/os/RemoteCallbackList;

    .line 225
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mEventCallback:Landroid/os/RemoteCallbackList;

    .line 315
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;Lcom/qti/snapdragon/digitalpen/DigitalPenService$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mScreenState:Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;

    .line 518
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->SHORT_SLEEP_TIME:I

    .line 519
    iput v4, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->LONG_SLEEP_TIME:I

    .line 522
    iput v4, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->DAEMON_START_FAILURE_MAX_TIME:I

    .line 525
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mHandler:Landroid/os/Handler;

    .line 563
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPidPoller:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 567
    iput v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPid:I

    .line 571
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$7;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$7;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataSocketListener:Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker$OnDataListener;

    .line 586
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$8;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$8;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mControlSocketListener:Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker$OnEventListener;

    .line 373
    invoke-virtual {p0, p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->init(Landroid/content/Context;)V

    .line 374
    return-void
.end method

.method static synthetic access$000(Lcom/qti/snapdragon/digitalpen/DigitalPenService;Lcom/qti/snapdragon/digitalpen/ConfigManager$State;)V
    .locals 0
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;
    .param p1, "x1"    # Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->handleNewState(Lcom/qti/snapdragon/digitalpen/ConfigManager$State;)V

    return-void
.end method

.method static synthetic access$100(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)I
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    .prologue
    .line 67
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedCurrentPowerState:I

    return v0
.end method

.method static synthetic access$1000(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mIsFilteringData:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/qti/snapdragon/digitalpen/DigitalPenService;I[I)V
    .locals 0
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;
    .param p1, "x1"    # I
    .param p2, "x2"    # [I

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cacheEvent(I[I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Lcom/qti/snapdragon/digitalpen/ConfigManager$State;
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mCurrentState:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Landroid/app/ActivityManager;
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mActivityManager:Landroid/app/ActivityManager;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)I
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    .prologue
    .line 67
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPid:I

    return v0
.end method

.method static synthetic access$1500(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V
    .locals 0
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->setGlobalConfigFromPersister()V

    return-void
.end method

.method static synthetic access$1700(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Lcom/qti/snapdragon/digitalpen/DirectoryObserver;
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mEposDirObserver:Lcom/qti/snapdragon/digitalpen/DirectoryObserver;

    return-object v0
.end method

.method static synthetic access$200(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)I
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    .prologue
    .line 67
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPreviousPowerState:I

    return v0
.end method

.method static synthetic access$300(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)I
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    .prologue
    .line 67
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedMicsBlocked:I

    return v0
.end method

.method static synthetic access$400(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)I
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    .prologue
    .line 67
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPenBatteryState:I

    return v0
.end method

.method static synthetic access$500(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)I
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    .prologue
    .line 67
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPenBatteryLevel:I

    return v0
.end method

.method static synthetic access$600(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Lcom/qti/snapdragon/digitalpen/ConfigManager;
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigManager:Lcom/qti/snapdragon/digitalpen/ConfigManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->disableDaemon()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->enableDaemon()Z

    move-result v0

    return v0
.end method

.method private cacheEvent(I[I)V
    .locals 2
    .param p1, "eventType"    # I
    .param p2, "params"    # [I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 472
    packed-switch p1, :pswitch_data_0

    .line 488
    :goto_0
    :pswitch_0
    return-void

    .line 474
    :pswitch_1
    aget v0, p2, v0

    iput v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedCurrentPowerState:I

    .line 475
    aget v0, p2, v1

    iput v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPreviousPowerState:I

    goto :goto_0

    .line 478
    :pswitch_2
    aget v0, p2, v0

    iput v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedMicsBlocked:I

    goto :goto_0

    .line 481
    :pswitch_3
    aget v0, p2, v0

    iput v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPenBatteryState:I

    .line 482
    aget v0, p2, v1

    iput v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPenBatteryLevel:I

    goto :goto_0

    .line 472
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private checkSmarterStandChange(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V
    .locals 2
    .param p1, "config"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .prologue
    .line 776
    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isSmarterStandEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mSmarterStandSensorListener:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    if-nez v0, :cond_1

    .line 777
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$9;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$9;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getSmarterStandSupportedOrientation()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->createSmarterStandSensorListener(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$AccelerometerChangeCallback;I)Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    move-result-object v0

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mSmarterStandSensorListener:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    .line 785
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mSmarterStandSensorListener:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->start()V

    .line 790
    :cond_0
    :goto_0
    return-void

    .line 786
    :cond_1
    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isSmarterStandEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mSmarterStandSensorListener:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    if-eqz v0, :cond_0

    .line 787
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mSmarterStandSensorListener:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->stop()V

    .line 788
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mSmarterStandSensorListener:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    goto :goto_0
.end method

.method private createGlobalSettingsPersister()V
    .locals 3

    .prologue
    .line 449
    const-string v0, "/data/usf/epos/service_settings.xml"

    .line 450
    .local v0, "settingsSymlinkPath":Ljava/lang/String;
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->isRunningInTestEnvironment()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 452
    new-instance v1, Lcom/qti/snapdragon/digitalpen/DigitalPenService$5;

    const-string v2, "/data/usf/epos/service_settings.xml"

    invoke-direct {v1, p0, v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$5;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mGlobalSettingsPersister:Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;

    .line 461
    :goto_0
    return-void

    .line 459
    :cond_0
    new-instance v1, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;

    const-string v2, "/data/usf/epos/service_settings.xml"

    invoke-direct {v1, v2}, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mGlobalSettingsPersister:Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;

    goto :goto_0
.end method

.method private disableBackgroundSideChannelIndicators()V
    .locals 2

    .prologue
    .line 1395
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mServiceNotifications:Lcom/qti/snapdragon/digitalpen/ServiceNotifications;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->backgroundListenerEnabled(Z)V

    .line 1398
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/qti/snapdragon/digitalpen/DigitalPenService$12;

    invoke-direct {v1, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$12;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1403
    return-void
.end method

.method private declared-synchronized disableDaemon()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1111
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logFunctionCalled()V

    .line 1113
    sget-boolean v2, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDaemonEnabled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    .line 1170
    :goto_0
    monitor-exit p0

    return v1

    .line 1117
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDaemonPollingThread:Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;

    if-eqz v1, :cond_1

    .line 1118
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDaemonPollingThread:Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;

    invoke-virtual {v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;->interrupt()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1122
    :try_start_2
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDaemonPollingThread:Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;

    invoke-virtual {v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;->join()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1125
    :goto_1
    const/4 v1, 0x0

    :try_start_3
    iput-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDaemonPollingThread:Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;

    .line 1129
    :cond_1
    :goto_2
    const-string v1, "init.svc.usf_epos"

    const-string v2, "stopped"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "running"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1130
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->stopDaemon()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1133
    const-wide/16 v2, 0x5dc

    :try_start_4
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 1134
    :catch_0
    move-exception v1

    goto :goto_2

    .line 1139
    :cond_2
    :try_start_5
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mControlSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    if-eqz v1, :cond_3

    .line 1140
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mControlSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    invoke-virtual {v1}, Lcom/qti/snapdragon/digitalpen/SocketThread;->interrupt()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1142
    :try_start_6
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mControlSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    invoke-virtual {v1}, Lcom/qti/snapdragon/digitalpen/SocketThread;->join()V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1145
    :goto_3
    const/4 v1, 0x0

    :try_start_7
    iput-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mControlSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    .line 1147
    :cond_3
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    if-eqz v1, :cond_4

    .line 1148
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    invoke-virtual {v1}, Lcom/qti/snapdragon/digitalpen/SocketThread;->interrupt()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1150
    :try_start_8
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    invoke-virtual {v1}, Lcom/qti/snapdragon/digitalpen/SocketThread;->join()V
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1153
    :goto_4
    const/4 v1, 0x0

    :try_start_9
    iput-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    .line 1156
    :cond_4
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPidPollerTask:Ljava/util/concurrent/ScheduledFuture;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    if-eqz v1, :cond_5

    .line 1158
    :try_start_a
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPidPollerTask:Ljava/util/concurrent/ScheduledFuture;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z

    .line 1159
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPidPollerTask:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v1}, Ljava/util/concurrent/ScheduledFuture;->get()Ljava/lang/Object;
    :try_end_a
    .catch Ljava/util/concurrent/CancellationException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1165
    :goto_5
    const/4 v1, 0x0

    :try_start_b
    iput-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPidPollerTask:Ljava/util/concurrent/ScheduledFuture;

    .line 1168
    :cond_5
    const/4 v1, 0x0

    sput-boolean v1, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDaemonEnabled:Z

    .line 1170
    const/4 v1, 0x1

    goto :goto_0

    .line 1162
    :catch_1
    move-exception v0

    .line 1163
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_5

    .line 1111
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 1160
    :catch_2
    move-exception v1

    goto :goto_5

    .line 1151
    :catch_3
    move-exception v1

    goto :goto_4

    .line 1143
    :catch_4
    move-exception v1

    goto :goto_3

    .line 1123
    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method private enableBackgroundSideChannelIndicators()V
    .locals 2

    .prologue
    .line 1386
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mServiceNotifications:Lcom/qti/snapdragon/digitalpen/ServiceNotifications;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->backgroundListenerEnabled(Z)V

    .line 1387
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/qti/snapdragon/digitalpen/DigitalPenService$11;

    invoke-direct {v1, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$11;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1392
    return-void
.end method

.method private declared-synchronized enableDaemon()Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 1056
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logFunctionCalled()V

    .line 1058
    const-string v1, "init.svc.usf_epos"

    const-string v2, "stopped"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "running"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1061
    const-string v1, "usf_epos"

    invoke-static {v1}, Landroid/os/SystemService;->start(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1065
    const-wide/16 v2, 0x5dc

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1071
    :cond_0
    :try_start_2
    const-string v1, "init.svc.usf_epos"

    const-string v2, "stopped"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "running"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1076
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Couldn\'t start the daemon"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1104
    :goto_0
    monitor-exit p0

    return v0

    .line 1066
    :catch_0
    move-exception v7

    .line 1067
    .local v7, "e":Ljava/lang/InterruptedException;
    goto :goto_0

    .line 1080
    .end local v7    # "e":Ljava/lang/InterruptedException;
    :cond_1
    const/4 v0, 0x1

    :try_start_3
    sput-boolean v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDaemonEnabled:Z

    .line 1084
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mControlSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mControlSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/SocketThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1085
    :cond_2
    new-instance v0, Lcom/qti/snapdragon/digitalpen/SocketThread;

    const-string v1, "/data/usf/epos/control_socket"

    new-instance v2, Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker;

    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mControlSocketListener:Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker$OnEventListener;

    invoke-direct {v2, v3}, Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker;-><init>(Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker$OnEventListener;)V

    invoke-direct {v0, v1, v2}, Lcom/qti/snapdragon/digitalpen/SocketThread;-><init>(Ljava/lang/String;Lcom/qti/snapdragon/digitalpen/SocketThread$ReceiveWorker;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mControlSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    .line 1087
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mControlSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/SocketThread;->start()V

    .line 1089
    :cond_3
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/SocketThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1090
    :cond_4
    new-instance v0, Lcom/qti/snapdragon/digitalpen/SocketThread;

    const-string v1, "/data/usf/epos/data_socket"

    new-instance v2, Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker;

    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataSocketListener:Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker$OnDataListener;

    invoke-direct {v2, v3}, Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker;-><init>(Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker$OnDataListener;)V

    invoke-direct {v0, v1, v2}, Lcom/qti/snapdragon/digitalpen/SocketThread;-><init>(Ljava/lang/String;Lcom/qti/snapdragon/digitalpen/SocketThread$ReceiveWorker;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    .line 1092
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/SocketThread;->start()V

    .line 1094
    :cond_5
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDaemonPollingThread:Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDaemonPollingThread:Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_7

    .line 1095
    :cond_6
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;

    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDaemonPollingThread:Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;

    .line 1096
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDaemonPollingThread:Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;->start()V

    .line 1098
    :cond_7
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPidPollerTask:Ljava/util/concurrent/ScheduledFuture;

    if-nez v0, :cond_8

    .line 1099
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPidPoller:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    new-instance v1, Lcom/qti/snapdragon/digitalpen/DigitalPenService$CheckApplicationStillForeground;

    invoke-direct {v1, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$CheckApplicationStillForeground;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1f4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v0 .. v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPidPollerTask:Ljava/util/concurrent/ScheduledFuture;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_8
    move v0, v8

    .line 1104
    goto :goto_0

    .line 1056
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private getStatusBarHeight(Landroid/content/res/Resources;Ljava/lang/String;)I
    .locals 4
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 328
    const/4 v1, 0x0

    .line 330
    .local v1, "result":I
    const-string v2, "dimen"

    const-string v3, "android"

    invoke-virtual {p1, p2, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 331
    .local v0, "resourceId":I
    if-lez v0, :cond_0

    .line 332
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 334
    :cond_0
    return v1
.end method

.method private handleNewState(Lcom/qti/snapdragon/digitalpen/ConfigManager$State;)V
    .locals 6
    .param p1, "newState"    # Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    .prologue
    const/4 v5, 0x0

    .line 135
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mCurrentState:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    .line 136
    .local v1, "oldState":Lcom/qti/snapdragon/digitalpen/ConfigManager$State;
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mCurrentState:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    .line 139
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->getCallingPidOverridable()I

    move-result v2

    .line 140
    .local v2, "pid":I
    const/4 v0, 0x0

    .line 141
    .local v0, "isNewApp":Z
    iget v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPid:I

    if-eq v2, v3, :cond_0

    .line 142
    invoke-direct {p0, v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logNewAppProcess(I)V

    .line 143
    iget v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPid:I

    invoke-direct {p0, v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->removeOldAppDataCallbacks(I)V

    .line 144
    iput v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPid:I

    .line 145
    const/4 v0, 0x1

    .line 149
    :cond_0
    sget-object v3, Lcom/qti/snapdragon/digitalpen/DigitalPenService$13;->$SwitchMap$com$qti$snapdragon$digitalpen$ConfigManager$State:[I

    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 170
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Unknown state"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 152
    :pswitch_0
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->enableBackgroundSideChannelIndicators()V

    .line 153
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mIsFilteringData:Z

    .line 172
    :goto_0
    return-void

    .line 157
    :pswitch_1
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->disableBackgroundSideChannelIndicators()V

    .line 158
    iput-boolean v5, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mIsFilteringData:Z

    .line 159
    iget v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPid:I

    invoke-direct {p0, v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->removeOldAppDataCallbacks(I)V

    .line 160
    iput v5, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPid:I

    goto :goto_0

    .line 164
    :pswitch_2
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->disableBackgroundSideChannelIndicators()V

    .line 165
    iput-boolean v5, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mIsFilteringData:Z

    .line 166
    invoke-direct {p0, v1, v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->ifBackgroundCanceledNotifyApp(Lcom/qti/snapdragon/digitalpen/ConfigManager$State;Z)V

    .line 167
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->sendCachedStateEvents()V

    goto :goto_0

    .line 149
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private ifBackgroundCanceledNotifyApp(Lcom/qti/snapdragon/digitalpen/ConfigManager$State;Z)V
    .locals 1
    .param p1, "oldState"    # Lcom/qti/snapdragon/digitalpen/ConfigManager$State;
    .param p2, "isNewApp"    # Z

    .prologue
    .line 195
    sget-object v0, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->LEGACY_WITH_SIDE_CHANNEL:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 196
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$3;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$3;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    invoke-virtual {p0, v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->postToHandler(Ljava/lang/Runnable;)V

    .line 207
    :cond_0
    return-void
.end method

.method private isRunningInTestEnvironment()Z
    .locals 2

    .prologue
    .line 467
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    const-string v1, "/system/framework"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private logFunctionCalled()V
    .locals 5

    .prologue
    .line 1270
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 1271
    .local v0, "currentThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    .line 1277
    .local v1, "stackTrace":[Ljava/lang/StackTraceElement;
    const-string v2, "DigitalPenService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x3

    aget-object v4, v1, v4

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " called"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1278
    return-void
.end method

.method private logNewAppProcess(I)V
    .locals 6
    .param p1, "pid"    # I

    .prologue
    .line 1366
    const-string v2, "<unknown>"

    .line 1367
    .local v2, "processName":Ljava/lang/String;
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mActivityManager:Landroid/app/ActivityManager;

    invoke-virtual {v3}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 1368
    .local v1, "process":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v3, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v3, p1, :cond_0

    .line 1369
    iget-object v2, v1, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    .line 1373
    .end local v1    # "process":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    const-string v3, "DigitalPenService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Detected new application: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "(pid: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1374
    return-void
.end method

.method private notifyOffScreenDimensionsChange()V
    .locals 14

    .prologue
    const/4 v11, 0x1

    const/4 v13, 0x0

    const/high16 v12, 0x44e10000    # 1800.0f

    const/4 v10, 0x3

    .line 975
    new-array v7, v10, [F

    .line 976
    .local v7, "origin":[F
    new-array v4, v10, [F

    .line 977
    .local v4, "endX":[F
    new-array v5, v10, [F

    .line 978
    .local v5, "endY":[F
    const/4 v1, 0x0

    .line 979
    .local v1, "X":I
    const/4 v2, 0x1

    .line 980
    .local v2, "Y":I
    const/16 v0, 0x708

    .line 982
    .local v0, "OFF_SCREEN_MAX_AXIS_RESOLUTION":I
    iget-object v10, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigManager:Lcom/qti/snapdragon/digitalpen/ConfigManager;

    invoke-virtual {v10}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->getGlobalConfig()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v10

    invoke-virtual {v10, v7, v4, v5}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOffScreenPlane([F[F[F)V

    .line 984
    aget v10, v7, v11

    aget v11, v5, v11

    sub-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v6

    .line 985
    .local v6, "height":F
    aget v10, v4, v13

    aget v11, v7, v13

    sub-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v9

    .line 986
    .local v9, "width":F
    div-float v8, v9, v6

    .line 988
    .local v8, "resolutionProportion":F
    cmpl-float v10, v6, v9

    if-lez v10, :cond_0

    .line 989
    const/high16 v6, 0x44e10000    # 1800.0f

    .line 990
    mul-float v9, v12, v8

    .line 997
    :goto_0
    :try_start_0
    iget-object v10, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDimensionsCallback:Lcodeaurora/ultrasound/IDigitalPenDimensionsCallback;

    if-eqz v10, :cond_1

    .line 998
    iget-object v10, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDimensionsCallback:Lcodeaurora/ultrasound/IDigitalPenDimensionsCallback;

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v11

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v12

    invoke-interface {v10, v11, v12}, Lcodeaurora/ultrasound/IDigitalPenDimensionsCallback;->onDimensionsChange(II)V

    .line 1006
    :goto_1
    return-void

    .line 992
    :cond_0
    const/high16 v9, 0x44e10000    # 1800.0f

    .line 993
    const/high16 v10, 0x3f800000    # 1.0f

    div-float/2addr v10, v8

    mul-float v6, v12, v10

    goto :goto_0

    .line 1000
    :cond_1
    const-string v10, "DigitalPenService"

    const-string v11, "Could not set off screen display dimensions since callback is null"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1002
    :catch_0
    move-exception v3

    .line 1003
    .local v3, "e":Landroid/os/RemoteException;
    const-string v10, "DigitalPenService"

    const-string v11, "onDimensionsChange callback threw RemoteException"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1004
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method private removeOldAppDataCallbacks(I)V
    .locals 5
    .param p1, "pidToRemove"    # I

    .prologue
    .line 1406
    if-nez p1, :cond_0

    .line 1417
    :goto_0
    return-void

    .line 1409
    :cond_0
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 1410
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 1411
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 1412
    .local v2, "pid":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne p1, v3, :cond_1

    .line 1413
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataCallback:Landroid/os/RemoteCallbackList;

    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 1410
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1416
    .end local v2    # "pid":Ljava/lang/Integer;
    :cond_2
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto :goto_0
.end method

.method private sendCachedStateEvents()V
    .locals 1

    .prologue
    .line 175
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$2;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$2;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    invoke-virtual {p0, v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->postToHandler(Ljava/lang/Runnable;)V

    .line 189
    return-void
.end method

.method private setGlobalConfigFromPersister()V
    .locals 2

    .prologue
    .line 970
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigManager:Lcom/qti/snapdragon/digitalpen/ConfigManager;

    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mGlobalSettingsPersister:Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;

    invoke-virtual {v1}, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->deserialize()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->setGlobalConfig(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V

    .line 971
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->notifyOffScreenDimensionsChange()V

    .line 972
    return-void
.end method

.method private stopDaemon()V
    .locals 6

    .prologue
    .line 731
    const/4 v2, -0x1

    .line 732
    .local v2, "pid":I
    const/16 v1, 0xa

    .line 733
    .local v1, "numTries":I
    :goto_0
    add-int/lit8 v1, v1, -0x1

    if-lez v1, :cond_1

    .line 734
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->getPid()I

    move-result v2

    .line 735
    const/4 v3, -0x2

    if-ne v3, v2, :cond_2

    .line 737
    const-string v3, "usf_epos"

    invoke-static {v3}, Landroid/os/SystemService;->stop(Ljava/lang/String;)V

    .line 754
    :cond_0
    :goto_1
    const-wide/16 v4, 0x1f4

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 755
    :catch_0
    move-exception v0

    .line 759
    :cond_1
    :goto_2
    return-void

    .line 738
    :cond_2
    const/4 v3, -0x1

    if-eq v3, v2, :cond_0

    .line 741
    :try_start_1
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "kill -15 "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    .line 743
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mSmarterStandSensorListener:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    if-eqz v3, :cond_1

    .line 744
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mSmarterStandSensorListener:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    invoke-virtual {v3}, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;->stop()V

    .line 745
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mSmarterStandSensorListener:Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 748
    :catch_1
    move-exception v0

    .line 749
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public declared-synchronized applyAppSettings(Landroid/os/Bundle;)Z
    .locals 1
    .param p1, "settings"    # Landroid/os/Bundle;

    .prologue
    .line 1355
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logFunctionCalled()V

    .line 1356
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigManager:Lcom/qti/snapdragon/digitalpen/ConfigManager;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->applyAppSettings(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1357
    const/4 v0, 0x1

    monitor-exit p0

    return v0

    .line 1355
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected changeConfig(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)Z
    .locals 2
    .param p1, "config"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .prologue
    .line 766
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 767
    const-string v0, "DigitalPenService"

    const-string v1, "Ignoring changeConfig when service not enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    const/4 v0, 0x0

    .line 772
    :goto_0
    return v0

    .line 770
    :cond_0
    invoke-direct {p0, p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->checkSmarterStandChange(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V

    .line 771
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mScreenState:Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;

    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isStopPenOnScreenOffEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->setStopPenOnScreenOff(Z)V

    .line 772
    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->marshalForDaemon()[B

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->sendControlMessage(I[B)Z

    move-result v0

    goto :goto_0
.end method

.method protected checkCallerAccess()Z
    .locals 4

    .prologue
    const/16 v3, 0x3e8

    .line 1018
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    if-eq v3, v0, :cond_0

    .line 1019
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Access denied, caller UID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", required UID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1021
    const/4 v0, 0x0

    .line 1023
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected createServiceNotifications()Lcom/qti/snapdragon/digitalpen/ServiceNotifications;
    .locals 2

    .prologue
    .line 445
    new-instance v0, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;

    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected createSmarterStandSensorListener(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$AccelerometerChangeCallback;I)Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;
    .locals 3
    .param p1, "callback"    # Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$AccelerometerChangeCallback;
    .param p2, "supportedOrientation"    # I

    .prologue
    .line 794
    new-instance v1, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;

    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mContext:Landroid/content/Context;

    const-string v2, "sensor"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    invoke-direct {v1, p1, v0, p2}, Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener;-><init>(Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$AccelerometerChangeCallback;Landroid/hardware/SensorManager;I)V

    return-object v1
.end method

.method public declared-synchronized disable()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1209
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logFunctionCalled()V

    .line 1211
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->checkCallerAccess()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 1238
    :goto_0
    monitor-exit p0

    return v0

    .line 1215
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->disableDaemon()Z

    .line 1217
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mScreenState:Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->resetPenState()V

    .line 1219
    const/4 v0, 0x0

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {p0, v0, v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->sendEvent(I[I)V

    .line 1226
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mServiceNotifications:Lcom/qti/snapdragon/digitalpen/ServiceNotifications;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->notifyPenDisabled()V

    .line 1227
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/qti/snapdragon/digitalpen/DigitalPenService$10;

    invoke-direct {v1, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$10;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1234
    const/4 v0, -0x1

    iput v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->cachedPenBatteryLevel:I

    .line 1236
    const/4 v0, 0x0

    sput-boolean v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mFeatureEnabled:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1238
    const/4 v0, 0x1

    goto :goto_0

    .line 1209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1219
    nop

    :array_0
    .array-data 4
        0x3
        0x0
    .end array-data
.end method

.method public declared-synchronized enable()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1182
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logFunctionCalled()V

    .line 1184
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->checkCallerAccess()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 1201
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 1188
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->enableDaemon()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1192
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->setGlobalConfigFromPersister()V

    .line 1193
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mScreenState:Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->resetPenState()V

    .line 1195
    const/4 v0, 0x1

    sput-boolean v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mFeatureEnabled:Z

    .line 1198
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigManager:Lcom/qti/snapdragon/digitalpen/ConfigManager;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->getGlobalConfig()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->changeConfig(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)Z

    .line 1199
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mServiceNotifications:Lcom/qti/snapdragon/digitalpen/ServiceNotifications;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->notifyPenEnabled()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 1201
    goto :goto_0

    .line 1182
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected getCallingPidOverridable()I
    .locals 1

    .prologue
    .line 1362
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    return v0
.end method

.method public getConfig()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 1

    .prologue
    .line 1285
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logFunctionCalled()V

    .line 1286
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigManager:Lcom/qti/snapdragon/digitalpen/ConfigManager;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->getGlobalConfig()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v0

    return-object v0
.end method

.method protected getControlStream()Ljava/io/OutputStream;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 825
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mControlSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/SocketThread;->getSocket()Landroid/net/LocalSocket;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method getPid()I
    .locals 9

    .prologue
    const/4 v5, -0x1

    .line 687
    const-string v6, ""

    .line 688
    .local v6, "str":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 690
    .local v0, "buf":Ljava/lang/StringBuffer;
    const/4 v3, 0x0

    .line 695
    .local v3, "reader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    const-string v7, "/data/usf/usf_epos.pid"

    invoke-direct {v2, v7}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 698
    .local v2, "fStream":Ljava/io/FileInputStream;
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/InputStreamReader;

    invoke-direct {v7, v2}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 699
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .local v4, "reader":Ljava/io/BufferedReader;
    :goto_0
    :try_start_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 700
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 702
    :catch_0
    move-exception v1

    move-object v3, v4

    .line 705
    .end local v2    # "fStream":Ljava/io/FileInputStream;
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .local v1, "e":Ljava/io/IOException;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    :goto_1
    if-eqz v3, :cond_0

    .line 707
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 720
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    :goto_2
    return v5

    .line 705
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "fStream":Ljava/io/FileInputStream;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :cond_1
    if-eqz v4, :cond_2

    .line 707
    :try_start_3
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 715
    :cond_2
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_4

    move-result v5

    .local v5, "retPid":I
    move-object v3, v4

    .line 720
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 708
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .end local v5    # "retPid":I
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_1
    move-exception v1

    .restart local v1    # "e":Ljava/io/IOException;
    move-object v3, v4

    .line 709
    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 708
    .end local v2    # "fStream":Ljava/io/FileInputStream;
    :catch_2
    move-exception v1

    .line 709
    goto :goto_2

    .line 705
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_3
    if-eqz v3, :cond_3

    .line 707
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 709
    :cond_3
    throw v7

    .line 708
    :catch_3
    move-exception v1

    .line 709
    .restart local v1    # "e":Ljava/io/IOException;
    goto :goto_2

    .line 716
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "fStream":Ljava/io/FileInputStream;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v1

    .line 717
    .local v1, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "Daemon pid file does not contain an integer"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    const/4 v5, -0x2

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 705
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    .end local v3    # "reader":Ljava/io/BufferedReader;
    .restart local v4    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v7

    move-object v3, v4

    .end local v4    # "reader":Ljava/io/BufferedReader;
    .restart local v3    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 702
    .end local v2    # "fStream":Ljava/io/FileInputStream;
    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method protected init(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 381
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mContext:Landroid/content/Context;

    .line 382
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mContext:Landroid/content/Context;

    if-nez v1, :cond_0

    .line 383
    const-string v1, "DigitalPenService"

    const-string v2, "mContext is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    :cond_0
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->createServiceNotifications()Lcom/qti/snapdragon/digitalpen/ServiceNotifications;

    move-result-object v1

    iput-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mServiceNotifications:Lcom/qti/snapdragon/digitalpen/ServiceNotifications;

    .line 388
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->createGlobalSettingsPersister()V

    .line 390
    const-string v1, "activity"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    iput-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mActivityManager:Landroid/app/ActivityManager;

    .line 392
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 393
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.qti.snapdragon.digitalpen.LOAD_CONFIG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 394
    const-string v1, "com.qti.snapdragon.digitalpen.GenerateOnScreenData"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 395
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 396
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 398
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/qti/snapdragon/digitalpen/DigitalPenService$4;

    invoke-direct {v2, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$4;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 408
    new-instance v1, Lcom/qti/snapdragon/digitalpen/DirectoryObserver;

    const-string v2, "/data/usf/epos/"

    invoke-direct {v1, v2}, Lcom/qti/snapdragon/digitalpen/DirectoryObserver;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mEposDirObserver:Lcom/qti/snapdragon/digitalpen/DirectoryObserver;

    .line 410
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->startPostBootThread()V

    .line 411
    return-void
.end method

.method protected declared-synchronized invokeDataCallbacks(Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;)V
    .locals 3
    .param p1, "dataToSend"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;

    .prologue
    .line 1421
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .local v0, "i":I
    move v1, v0

    .line 1422
    .end local v0    # "i":I
    .local v1, "i":I
    :goto_0
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    if-lez v1, :cond_0

    .line 1424
    :try_start_1
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2, v0}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v2

    check-cast v2, Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;

    invoke-interface {v2, p1}, Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;->onDigitalPenPropData(Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v1, v0

    .line 1428
    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 1425
    .end local v1    # "i":I
    .restart local v0    # "i":I
    :catch_0
    move-exception v2

    move v1, v0

    .line 1428
    .end local v0    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 1430
    .end local v1    # "i":I
    .restart local v0    # "i":I
    :cond_0
    :try_start_2
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1431
    monitor-exit p0

    return-void

    .line 1421
    .end local v0    # "i":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method protected isControlSocketConnected()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 829
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mControlSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    if-nez v2, :cond_0

    .line 842
    :goto_0
    return v1

    .line 833
    :cond_0
    const/4 v0, 0x4

    .line 834
    .local v0, "num_tries":I
    :cond_1
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mControlSocketThread:Lcom/qti/snapdragon/digitalpen/SocketThread;

    invoke-virtual {v2}, Lcom/qti/snapdragon/digitalpen/SocketThread;->isConnected()Z

    move-result v2

    if-nez v2, :cond_2

    .line 836
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 839
    :goto_1
    add-int/lit8 v0, v0, -0x1

    if-nez v0, :cond_1

    goto :goto_0

    .line 842
    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    .line 837
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 1248
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logFunctionCalled()V

    .line 1250
    sget-boolean v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mFeatureEnabled:Z

    return v0
.end method

.method protected declared-synchronized onReceiveIntent(Landroid/content/Intent;)V
    .locals 13
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 414
    monitor-enter p0

    :try_start_0
    const-string v1, "DigitalPenService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received intent: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.qti.snapdragon.digitalpen.LOAD_CONFIG"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v2, :cond_1

    .line 418
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->onSidebandLoadConfig(Landroid/content/Intent;)Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 442
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 419
    :catch_0
    move-exception v11

    .line 420
    .local v11, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    invoke-virtual {v11}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 414
    .end local v11    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 423
    :cond_1
    :try_start_3
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.qti.snapdragon.digitalpen.GenerateOnScreenData"

    if-ne v1, v2, :cond_2

    .line 424
    new-instance v12, Ljava/util/Scanner;

    const-string v1, "GenerateOnScreenData"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v12, v1}, Ljava/util/Scanner;-><init>(Ljava/lang/String;)V

    .line 426
    .local v12, "scanner":Ljava/util/Scanner;
    const-string v1, "x="

    invoke-virtual {v12, v1}, Ljava/util/Scanner;->skip(Ljava/lang/String;)Ljava/util/Scanner;

    .line 427
    const/4 v1, 0x3

    new-array v9, v1, [I

    fill-array-data v9, :array_0

    .line 430
    .local v9, "sideButtonsState":[I
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;

    invoke-virtual {v12}, Ljava/util/Scanner;->nextInt()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;-><init>(IIIIIIIZ[II)V

    .line 434
    .local v0, "data":Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;
    invoke-virtual {v12}, Ljava/util/Scanner;->close()V

    .line 435
    invoke-virtual {p0, v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->invokeDataCallbacks(Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;)V

    goto :goto_0

    .line 437
    .end local v0    # "data":Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;
    .end local v9    # "sideButtonsState":[I
    .end local v12    # "scanner":Ljava/util/Scanner;
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.SCREEN_ON"

    if-eq v1, v2, :cond_3

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.SCREEN_OFF"

    if-ne v1, v2, :cond_0

    .line 439
    :cond_3
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mScreenState:Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->changePenStateAccordingToScreenState(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 427
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public onSidebandLoadConfig(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 846
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 847
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->enable()Z

    .line 849
    :cond_0
    new-instance v0, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;

    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigManager:Lcom/qti/snapdragon/digitalpen/ConfigManager;

    invoke-virtual {v1}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->getGlobalConfig()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V

    .line 850
    .local v0, "changer":Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigManager:Lcom/qti/snapdragon/digitalpen/ConfigManager;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;->processIntent(Landroid/content/Intent;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->setGlobalConfig(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V

    .line 851
    const/4 v1, 0x1

    return v1
.end method

.method protected postToHandler(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 338
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 339
    return-void
.end method

.method protected processAccelerometerData(D)V
    .locals 3
    .param p1, "d"    # D

    .prologue
    .line 855
    const/16 v1, 0x8

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 858
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    const/4 v1, 0x2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->sendControlMessage(I[B)Z

    .line 859
    return-void
.end method

.method public declared-synchronized registerDataCallback(Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;)Z
    .locals 3
    .param p1, "cb"    # Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;

    .prologue
    .line 1294
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logFunctionCalled()V

    .line 1295
    if-eqz p1, :cond_0

    .line 1296
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataCallback:Landroid/os/RemoteCallbackList;

    new-instance v1, Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->getCallingPidOverridable()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, p1, v1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1297
    const/4 v0, 0x1

    .line 1299
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1294
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public registerEventCallback(Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;)Z
    .locals 6
    .param p1, "cb"    # Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;

    .prologue
    const/4 v4, 0x0

    .line 1307
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logFunctionCalled()V

    .line 1308
    if-eqz p1, :cond_1

    .line 1309
    iget-object v5, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mEventCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v5, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 1311
    :try_start_0
    sget-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->CAPABILITIES_TABLE:[Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;

    .local v0, "arr$":[Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 1312
    .local v1, "e":Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;
    invoke-interface {p1, v1}, Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;->onDigitalPenPropEvent(Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1311
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1314
    .end local v1    # "e":Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;
    :cond_0
    const/4 v4, 0x1

    .line 1320
    .end local v0    # "arr$":[Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    :cond_1
    :goto_1
    return v4

    .line 1315
    :catch_0
    move-exception v1

    .line 1316
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public registerOffScreenDimensionsCallback(Lcodeaurora/ultrasound/IDigitalPenDimensionsCallback;)V
    .locals 1
    .param p1, "callback"    # Lcodeaurora/ultrasound/IDigitalPenDimensionsCallback;

    .prologue
    .line 1035
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logFunctionCalled()V

    .line 1037
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->checkCallerAccess()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1042
    :goto_0
    return-void

    .line 1041
    :cond_0
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDimensionsCallback:Lcodeaurora/ultrasound/IDigitalPenDimensionsCallback;

    goto :goto_0
.end method

.method public declared-synchronized releaseActivity()Z
    .locals 1

    .prologue
    .line 1380
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logFunctionCalled()V

    .line 1381
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigManager:Lcom/qti/snapdragon/digitalpen/ConfigManager;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->releaseApp()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1382
    const/4 v0, 0x0

    monitor-exit p0

    return v0

    .line 1380
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized sendControlMessage(I[B)Z
    .locals 7
    .param p1, "msgId"    # I
    .param p2, "msgBuf"    # [B

    .prologue
    const/4 v3, 0x0

    .line 800
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->isControlSocketConnected()Z

    move-result v4

    if-nez v4, :cond_0

    .line 801
    const-string v4, "DigitalPenService"

    const-string v5, "Control socket not connected"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 821
    :goto_0
    monitor-exit p0

    return v3

    .line 805
    :cond_0
    :try_start_1
    array-length v4, p2

    add-int/lit8 v4, v4, 0x8

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    sget-object v5, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    array-length v5, p2

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 814
    .local v1, "msg":[B
    :try_start_2
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->getControlStream()Ljava/io/OutputStream;

    move-result-object v2

    .line 815
    .local v2, "out":Ljava/io/OutputStream;
    const/4 v4, 0x0

    array-length v5, v1

    invoke-virtual {v2, v1, v4, v5}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 821
    const/4 v3, 0x1

    goto :goto_0

    .line 816
    .end local v2    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 817
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    const-string v4, "DigitalPenService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException while trying to write to daemon through socket: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 800
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "msg":[B
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method protected sendEvent(I[I)V
    .locals 6
    .param p1, "eventType"    # I
    .param p2, "params"    # [I

    .prologue
    .line 494
    invoke-static {p1, p2}, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->makeEventIntent(I[I)Landroid/content/Intent;

    move-result-object v0

    .line 495
    .local v0, "eventIntent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mContext:Landroid/content/Context;

    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 497
    new-instance v1, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;

    invoke-direct {v1, p1, p2}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;-><init>(I[I)V

    .line 500
    .local v1, "eventToSend":Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;
    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mEventCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v2

    .local v2, "i":I
    move v3, v2

    .line 501
    .end local v2    # "i":I
    .local v3, "i":I
    :goto_0
    add-int/lit8 v2, v3, -0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    if-lez v3, :cond_0

    .line 503
    :try_start_0
    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mEventCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v4

    check-cast v4, Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;

    invoke-interface {v4, v1}, Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;->onDigitalPenPropEvent(Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move v3, v2

    .line 508
    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 505
    .end local v3    # "i":I
    .restart local v2    # "i":I
    :catch_0
    move-exception v4

    move v3, v2

    .line 508
    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 510
    .end local v3    # "i":I
    .restart local v2    # "i":I
    :cond_0
    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mEventCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 512
    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mServiceNotifications:Lcom/qti/snapdragon/digitalpen/ServiceNotifications;

    invoke-virtual {v4, p1, p2}, Lcom/qti/snapdragon/digitalpen/ServiceNotifications;->sendEvent(I[I)V

    .line 513
    return-void
.end method

.method public declared-synchronized setGlobalConfig(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)Z
    .locals 1
    .param p1, "config"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .prologue
    .line 1258
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logFunctionCalled()V

    .line 1260
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->checkCallerAccess()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1261
    const/4 v0, 0x0

    .line 1266
    :goto_0
    monitor-exit p0

    return v0

    .line 1264
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mGlobalSettingsPersister:Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/GlobalSettingsPersister;->serialize(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V

    .line 1265
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigManager:Lcom/qti/snapdragon/digitalpen/ConfigManager;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->setGlobalConfig(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1266
    const/4 v0, 0x1

    goto :goto_0

    .line 1258
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected setStatusBarOverlayVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    .line 365
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mStatusBarOverlayView:Landroid/view/View;

    if-nez v0, :cond_0

    .line 366
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->setupStatusBarOverlay()V

    .line 368
    :cond_0
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mStatusBarOverlayView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 369
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mStatusBarOverlayView:Landroid/view/View;

    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 370
    return-void
.end method

.method protected setupStatusBarOverlay()V
    .locals 9

    .prologue
    .line 342
    const-string v6, "status_bar_height"

    .line 343
    .local v6, "STATUS_BAR_HEIGHT_RES_NAME":Ljava/lang/String;
    const/high16 v7, -0x7f010000

    .line 345
    .local v7, "STATUS_BAR_OVERLAY_SIDE_CHANNEL_COLOR":I
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 346
    .local v8, "res":Landroid/content/res/Resources;
    const-string v0, "status_bar_height"

    invoke-direct {p0, v8, v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->getStatusBarHeight(Landroid/content/res/Resources;Ljava/lang/String;)I

    move-result v2

    .line 347
    .local v2, "StatusBarOverlayHeight":I
    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mStatusBarOverlayView:Landroid/view/View;

    .line 348
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x1

    const/16 v3, 0x7d6

    const v4, 0x10100

    const/4 v5, -0x3

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 353
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x30

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 354
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mStatusBarOverlayView:Landroid/view/View;

    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 356
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mStatusBarOverlayView:Landroid/view/View;

    const/high16 v1, -0x7f010000

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 357
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mStatusBarOverlayView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 359
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mWindowManager:Landroid/view/WindowManager;

    .line 361
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mStatusBarOverlayView:Landroid/view/View;

    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mWindowLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v3}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 362
    return-void
.end method

.method public startPostBootThread()V
    .locals 1

    .prologue
    .line 1012
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logFunctionCalled()V

    .line 1013
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    .line 1014
    .local v0, "pollingPostBootThread":Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;
    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->start()V

    .line 1015
    return-void
.end method

.method public declared-synchronized unregisterDataCallback(Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;)Z
    .locals 1
    .param p1, "cb"    # Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;

    .prologue
    .line 1328
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logFunctionCalled()V

    .line 1329
    if-eqz p1, :cond_0

    .line 1330
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mDataCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1331
    const/4 v0, 0x1

    .line 1333
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1328
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public unregisterEventCallback(Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;)Z
    .locals 1
    .param p1, "cb"    # Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;

    .prologue
    .line 1341
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->logFunctionCalled()V

    .line 1342
    if-eqz p1, :cond_0

    .line 1343
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mEventCallback:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 1344
    const/4 v0, 0x1

    .line 1346
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
