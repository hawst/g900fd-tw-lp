.class public Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
.super Ljava/lang/Object;
.source "DigitalPenGlobalControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DigitalPenGlobalSettings"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;,
        Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;,
        Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$Side;,
        Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;,
        Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$EraseMode;,
        Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;
    }
.end annotation


# instance fields
.field private final config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;


# direct methods
.method private constructor <init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V
    .locals 0
    .param p1, "config"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 155
    return-void
.end method

.method synthetic constructor <init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .param p2, "x1"    # Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$1;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->getConfig()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v0

    return-object v0
.end method

.method private getConfig()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    return-object v0
.end method


# virtual methods
.method public disableErase()Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setEraseButtonIndex(I)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 218
    return-object p0
.end method

.method public enableErase(ILcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$EraseMode;)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 2
    .param p1, "index"    # I
    .param p2, "mode"    # Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$EraseMode;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$EraseMode;->code:B
    invoke-static {p2}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$EraseMode;->access$300(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$EraseMode;)B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setEraseButtonBehavior(B)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 212
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setEraseButtonIndex(I)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 213
    return-object p0
.end method

.method public enableOffScreenHover(Z)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 198
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setOffScreenHoverEnable(Z)V

    .line 199
    return-object p0
.end method

.method public enableOnScreenHover(Z)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 193
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setOnScreenHoverEnable(Z)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 194
    return-object p0
.end method

.method public enableSmarterStand(Z)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 171
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setSmarterStand(Z)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 172
    return-object p0
.end method

.method public enableStartPenOnBoot(Z)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 262
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setStartPenOnBoot(Z)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 263
    return-object p0
.end method

.method public enableStopPenOnScreenOff(Z)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 253
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setStopPenOnScreenOff(Z)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 254
    return-object p0
.end method

.method public getEraseButtonIndex()I
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getEraseButtonIndex()I

    move-result v0

    return v0
.end method

.method public getEraseButtonMode()Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$EraseMode;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getEraseButtonMode()B

    move-result v0

    # invokes: Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$EraseMode;->fromCode(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$EraseMode;
    invoke-static {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$EraseMode;->access$200(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$EraseMode;

    move-result-object v0

    return-object v0
.end method

.method public getInRangeDistance()I
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getTouchRange()I

    move-result v0

    return v0
.end method

.method public getInputType()Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getInputType()B

    move-result v0

    # invokes: Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->fromCode(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;
    invoke-static {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->access$800(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    move-result-object v0

    return-object v0
.end method

.method public getMouseSensitivity()Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getMouseSensitivity()B

    move-result v0

    # invokes: Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->fromCode(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;
    invoke-static {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->access$1000(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    move-result-object v0

    return-object v0
.end method

.method public getOffScreenHoverMaxRange()I
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOffScreenHoverMaxRange()I

    move-result v0

    return v0
.end method

.method public getOffScreenMode()Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOffScreenMode()B

    move-result v0

    # invokes: Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->fromCode(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;
    invoke-static {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->access$100(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    move-result-object v0

    return-object v0
.end method

.method public getOffScreenPortraitSide()Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$Side;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOffSceenPortraitSide()B

    move-result v0

    # invokes: Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$Side;->fromCode(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$Side;
    invoke-static {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$Side;->access$500(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$Side;

    move-result-object v0

    return-object v0
.end method

.method public getOnScreenHoverMaxRange()I
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOnScreenHoverMaxRange()I

    move-result v0

    return v0
.end method

.method public getPowerProfile()Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getPowerSave()B

    move-result v0

    invoke-static {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;->fromCode(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    move-result-object v0

    return-object v0
.end method

.method public isOffScreenHoverEnabled()Z
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isOffScreenHoverEnabled()Z

    move-result v0

    return v0
.end method

.method public isOnScreenHoverEnabled()Z
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isOnScreenHoverEnabled()Z

    move-result v0

    return v0
.end method

.method public isShowingOffScreenHoverIcon()Z
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isShowingOffScreenHoverIcon()Z

    move-result v0

    return v0
.end method

.method public isShowingOnScreenHoverIcon()Z
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isShowingOnScreenHoverIcon()Z

    move-result v0

    return v0
.end method

.method public isSmarterStandEnabled()Z
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isSmarterStandEnabled()Z

    move-result v0

    return v0
.end method

.method public isStartPenOnBootEnabled()Z
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isStartPenOnBootEnabled()Z

    move-result v0

    return v0
.end method

.method public isStopPenOnScreenOffEnabled()Z
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isStopPenOnScreenOffEnabled()Z

    move-result v0

    return v0
.end method

.method public setDefaultInputType(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 2
    .param p1, "inputType"    # Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->code:B
    invoke-static {p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->access$700(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;)B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setInputType(B)V

    .line 241
    return-object p0
.end method

.method public setDefaultOffScreenMode(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 2
    .param p1, "mode"    # Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->code:B
    invoke-static {p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->access$000(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;)B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setOffScreenMode(B)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 163
    return-object p0
.end method

.method public setInRangeDistance(I)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 1
    .param p1, "distance"    # I

    .prologue
    .line 184
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setTouchRange(I)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 185
    return-object p0
.end method

.method public setMouseSensitivity(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;)V
    .locals 2
    .param p1, "mouseSensitivity"    # Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    .prologue
    .line 307
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->code:B
    invoke-static {p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->access$900(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;)B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setMouseSensitivity(B)V

    .line 308
    return-void
.end method

.method public setOffScreenHoverMaxRange(I)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 1
    .param p1, "maxRange"    # I

    .prologue
    .line 292
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setOffScreenHoverMaxRange(I)V

    .line 293
    return-object p0
.end method

.method public setOffScreenPortraitSide(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$Side;)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 2
    .param p1, "side"    # Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$Side;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$Side;->code:B
    invoke-static {p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$Side;->access$600(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$Side;)B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setOffScreenPortraitSide(B)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 236
    return-object p0
.end method

.method public setOnScreenHoverMaxRange(I)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 1
    .param p1, "maxRange"    # I

    .prologue
    .line 287
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setOnScreenHoverMaxRange(I)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 288
    return-object p0
.end method

.method public setPowerProfile(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 2
    .param p1, "profile"    # Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;->code:B
    invoke-static {p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;->access$400(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$PowerProfile;)B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setPowerSave(B)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 223
    return-object p0
.end method

.method public showOffScreenHoverIcon(Z)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 302
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setShowOffScreenHoverIcon(Z)V

    .line 303
    return-object p0
.end method

.method public showOnScreenHoverIcon(Z)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 297
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setShowOnScreenHoverIcon(Z)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 298
    return-object p0
.end method
