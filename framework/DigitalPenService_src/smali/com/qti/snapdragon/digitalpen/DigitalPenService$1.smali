.class Lcom/qti/snapdragon/digitalpen/DigitalPenService$1;
.super Ljava/lang/Object;
.source "DigitalPenService.java"

# interfaces
.implements Lcom/qti/snapdragon/digitalpen/ConfigManager$ConfigChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;


# direct methods
.method constructor <init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$1;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigChanged(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/ConfigManager$State;Z)V
    .locals 2
    .param p1, "newConfig"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .param p2, "newState"    # Lcom/qti/snapdragon/digitalpen/ConfigManager$State;
    .param p3, "stateChanged"    # Z

    .prologue
    .line 123
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$1;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    monitor-enter v1

    .line 124
    if-eqz p3, :cond_0

    .line 125
    :try_start_0
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$1;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # invokes: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->handleNewState(Lcom/qti/snapdragon/digitalpen/ConfigManager$State;)V
    invoke-static {v0, p2}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$000(Lcom/qti/snapdragon/digitalpen/DigitalPenService;Lcom/qti/snapdragon/digitalpen/ConfigManager$State;)V

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$1;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-virtual {v0, p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->changeConfig(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)Z

    .line 128
    monitor-exit v1

    .line 129
    return-void

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
