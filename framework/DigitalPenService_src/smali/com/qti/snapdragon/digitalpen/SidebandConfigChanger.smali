.class public Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;
.super Ljava/lang/Object;
.source "SidebandConfigChanger.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;,
        Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigCommand;
    }
.end annotation


# static fields
.field protected static final TAG:Ljava/lang/String; = "LoadConfigIntentHandler"

.field static configFieldHandlers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;->configFieldHandlers:Ljava/util/List;

    .line 54
    sget-object v0, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;->configFieldHandlers:Ljava/util/List;

    new-instance v1, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;

    const-string v2, "OffScreenMode"

    new-instance v3, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$1;

    invoke-direct {v3}, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$1;-><init>()V

    invoke-direct {v1, v2, v3}, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;-><init>(Ljava/lang/String;Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigCommand;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    sget-object v0, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;->configFieldHandlers:Ljava/util/List;

    new-instance v1, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;

    const-string v2, "UseSmarterStand"

    new-instance v3, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$2;

    invoke-direct {v3}, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$2;-><init>()V

    invoke-direct {v1, v2, v3}, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;-><init>(Ljava/lang/String;Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigCommand;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    sget-object v0, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;->configFieldHandlers:Ljava/util/List;

    new-instance v1, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;

    const-string v2, "OffScreenPlane"

    new-instance v3, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$3;

    invoke-direct {v3}, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$3;-><init>()V

    invoke-direct {v1, v2, v3}, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;-><init>(Ljava/lang/String;Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigCommand;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    sget-object v0, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;->configFieldHandlers:Ljava/util/List;

    new-instance v1, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;

    const-string v2, "EraserMode"

    new-instance v3, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$4;

    invoke-direct {v3}, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$4;-><init>()V

    invoke-direct {v1, v2, v3}, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;-><init>(Ljava/lang/String;Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigCommand;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    sget-object v0, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;->configFieldHandlers:Ljava/util/List;

    new-instance v1, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;

    const-string v2, "EraserButtonIndex"

    new-instance v3, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$5;

    invoke-direct {v3}, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$5;-><init>()V

    invoke-direct {v1, v2, v3}, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;-><init>(Ljava/lang/String;Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigCommand;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    return-void
.end method

.method public constructor <init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V
    .locals 0
    .param p1, "config"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 24
    return-void
.end method


# virtual methods
.method public processIntent(Landroid/content/Intent;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 108
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.qti.snapdragon.digitalpen.LOAD_CONFIG"

    if-eq v3, v4, :cond_0

    .line 109
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected action: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 111
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 112
    .local v0, "bundle":Landroid/os/Bundle;
    sget-object v3, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;->configFieldHandlers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;

    .line 113
    .local v1, "field":Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    invoke-virtual {v1, v0, v3}, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;->checkAndDoCommand(Landroid/os/Bundle;Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V

    goto :goto_0

    .line 115
    .end local v1    # "field":Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigField;
    :cond_1
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    return-object v3
.end method
