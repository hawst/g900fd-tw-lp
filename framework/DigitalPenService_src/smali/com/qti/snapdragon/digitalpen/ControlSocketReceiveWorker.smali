.class Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker;
.super Ljava/lang/Object;
.source "ControlSocketReceiveWorker.java"

# interfaces
.implements Lcom/qti/snapdragon/digitalpen/SocketThread$ReceiveWorker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker$OnEventListener;
    }
.end annotation


# static fields
.field static final HEADER_WORDS:I = 0x1

.field static final NUM_PARAMS:I = 0x2

.field static final RAW_PACKET_SIZE:I = 0xc


# instance fields
.field private listener:Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker$OnEventListener;


# direct methods
.method public constructor <init>(Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker$OnEventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker$OnEventListener;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker;->listener:Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker$OnEventListener;

    .line 30
    return-void
.end method


# virtual methods
.method public receiveLoop(Ljava/io/InputStream;)V
    .locals 10
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/16 v7, 0xc

    const/4 v6, 0x0

    .line 41
    if-nez p1, :cond_0

    .line 42
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "null not a valid InputStream"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 46
    :cond_0
    new-array v1, v7, [B

    .line 47
    .local v1, "data":[B
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    sget-object v5, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v0

    .line 50
    .local v0, "buf":Ljava/nio/IntBuffer;
    :goto_0
    invoke-virtual {p1, v1, v6, v7}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    if-ne v4, v7, :cond_1

    .line 51
    invoke-virtual {v0, v6}, Ljava/nio/IntBuffer;->get(I)I

    move-result v2

    .line 52
    .local v2, "eventType":I
    new-array v3, v9, [I

    invoke-virtual {v0, v8}, Ljava/nio/IntBuffer;->get(I)I

    move-result v4

    aput v4, v3, v6

    invoke-virtual {v0, v9}, Ljava/nio/IntBuffer;->get(I)I

    move-result v4

    aput v4, v3, v8

    .line 56
    .local v3, "params":[I
    iget-object v4, p0, Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker;->listener:Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker$OnEventListener;

    invoke-interface {v4, v2, v3}, Lcom/qti/snapdragon/digitalpen/ControlSocketReceiveWorker$OnEventListener;->onEvent(I[I)V

    goto :goto_0

    .line 58
    .end local v2    # "eventType":I
    .end local v3    # "params":[I
    :cond_1
    return-void
.end method
