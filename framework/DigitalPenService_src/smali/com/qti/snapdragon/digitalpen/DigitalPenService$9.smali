.class Lcom/qti/snapdragon/digitalpen/DigitalPenService$9;
.super Ljava/lang/Object;
.source "DigitalPenService.java"

# interfaces
.implements Lcom/qti/snapdragon/digitalpen/SmarterStandSensorListener$AccelerometerChangeCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenService;->checkSmarterStandChange(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;


# direct methods
.method constructor <init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V
    .locals 0

    .prologue
    .line 778
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$9;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNewPosition(D)V
    .locals 1
    .param p1, "accelerometerAngleReading"    # D

    .prologue
    .line 782
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$9;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-virtual {v0, p1, p2}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->processAccelerometerData(D)V

    .line 783
    return-void
.end method
