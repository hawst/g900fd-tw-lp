.class Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;
.super Landroid/os/Handler;
.source "DigitalPenService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final DAEMON_RESTART_DELAY:I

.field final synthetic this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;


# direct methods
.method constructor <init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V
    .locals 1

    .prologue
    .line 525
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 529
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;->DAEMON_RESTART_DELAY:I

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 533
    iget v0, p1, Landroid/os/Message;->arg1:I

    packed-switch v0, :pswitch_data_0

    .line 560
    :goto_0
    return-void

    .line 535
    :pswitch_0
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # invokes: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->disableDaemon()Z
    invoke-static {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$700(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Z

    .line 537
    const-string v0, "DigitalPenService"

    const-string v1, "Scheduling daemon restart in 1000 ms"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$6$1;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$6$1;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2, v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$6;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 533
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
