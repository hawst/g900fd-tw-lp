.class Lcom/qti/snapdragon/digitalpen/DigitalPenService$CheckApplicationStillForeground;
.super Ljava/lang/Object;
.source "DigitalPenService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CheckApplicationStillForeground"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;


# direct methods
.method constructor <init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V
    .locals 0

    .prologue
    .line 637
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$CheckApplicationStillForeground;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 641
    iget-object v6, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$CheckApplicationStillForeground;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    monitor-enter v6

    .line 643
    :try_start_0
    sget-object v5, Lcom/qti/snapdragon/digitalpen/DigitalPenService$13;->$SwitchMap$com$qti$snapdragon$digitalpen$ConfigManager$State:[I

    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$CheckApplicationStillForeground;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mCurrentState:Lcom/qti/snapdragon/digitalpen/ConfigManager$State;
    invoke-static {v7}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$1200(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Lcom/qti/snapdragon/digitalpen/ConfigManager$State;

    move-result-object v7

    invoke-virtual {v7}, Lcom/qti/snapdragon/digitalpen/ConfigManager$State;->ordinal()I

    move-result v7

    aget v5, v5, v7

    packed-switch v5, :pswitch_data_0

    .line 654
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v7, "Unknown state"

    invoke-direct {v5, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 676
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 645
    :pswitch_0
    :try_start_1
    monitor-exit v6

    .line 677
    :goto_0
    return-void

    .line 647
    :pswitch_1
    const/4 v0, 0x1

    .line 656
    .local v0, "expectedForeground":Z
    :goto_1
    const/4 v3, 0x0

    .line 657
    .local v3, "processExists":Z
    const/4 v4, 0x0

    .line 658
    .local v4, "processIsForeground":Z
    iget-object v5, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$CheckApplicationStillForeground;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mActivityManager:Landroid/app/ActivityManager;
    invoke-static {v5}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$1300(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Landroid/app/ActivityManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 659
    .local v2, "process":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v5, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    iget-object v7, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$CheckApplicationStillForeground;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPid:I
    invoke-static {v7}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$1400(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)I

    move-result v7

    if-ne v5, v7, :cond_0

    .line 660
    const/4 v3, 0x1

    .line 661
    iget v5, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v7, 0x64

    if-ne v5, v7, :cond_4

    const/4 v4, 0x1

    .line 666
    .end local v2    # "process":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    :goto_2
    if-nez v3, :cond_2

    .line 667
    const-string v5, "DigitalPenService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Detected last app (pid "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$CheckApplicationStillForeground;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mAppPid:I
    invoke-static {v8}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$1400(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") has crashed, releasing settings"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 669
    iget-object v5, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$CheckApplicationStillForeground;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-virtual {v5}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->releaseActivity()Z

    .line 672
    :cond_2
    if-eqz v0, :cond_3

    if-nez v4, :cond_3

    .line 673
    const-string v5, "DigitalPenService"

    const-string v7, "App not in expected foreground state, releasing."

    invoke-static {v5, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    iget-object v5, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$CheckApplicationStillForeground;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-virtual {v5}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->releaseActivity()Z

    .line 676
    :cond_3
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 650
    .end local v0    # "expectedForeground":Z
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "processExists":Z
    .end local v4    # "processIsForeground":Z
    :pswitch_2
    const/4 v0, 0x0

    .line 651
    .restart local v0    # "expectedForeground":Z
    goto :goto_1

    .line 661
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "process":Landroid/app/ActivityManager$RunningAppProcessInfo;
    .restart local v3    # "processExists":Z
    .restart local v4    # "processIsForeground":Z
    :cond_4
    const/4 v4, 0x0

    goto :goto_2

    .line 643
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
