.class final Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$4;
.super Ljava/lang/Object;
.source "SidebandConfigChanger.java"

# interfaces
.implements Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger$ConfigCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/SidebandConfigChanger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Landroid/os/Bundle;Ljava/lang/String;Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)V
    .locals 2
    .param p1, "b"    # Landroid/os/Bundle;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "config"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .prologue
    .line 92
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "newMode":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;)B

    move-result v1

    invoke-virtual {p3, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->setEraseButtonBehavior(B)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 94
    return-void
.end method
