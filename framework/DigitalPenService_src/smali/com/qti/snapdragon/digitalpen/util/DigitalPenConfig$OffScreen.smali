.class Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;
.super Ljava/lang/Object;
.source "DigitalPenConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OffScreen"
.end annotation


# instance fields
.field public endX:[F

.field public endY:[F

.field public mode:B

.field public origin:[F

.field public portraitSide:B

.field public smarterStand:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->smarterStand:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;

    .line 107
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->origin:[F

    .line 109
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->endX:[F

    .line 111
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->endY:[F

    return-void
.end method

.method synthetic constructor <init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;-><init>()V

    return-void
.end method
