.class public Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
.super Ljava/lang/Object;
.source "DigitalPenConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;,
        Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;,
        Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;,
        Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;,
        Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;",
            ">;"
        }
    .end annotation
.end field

.field public static final DP_COORD_DESTINATION_BOTH:B = 0x2t

.field public static final DP_COORD_DESTINATION_MOTION_EVENT:B = 0x0t

.field public static final DP_COORD_DESTINATION_SOCKET:B = 0x1t

.field public static final DP_ERASE_MODE_HOLD:B = 0x0t

.field public static final DP_ERASE_MODE_TOGGLE:B = 0x1t

.field public static final DP_INPUT_TYPE_MOUSE:B = 0x4t

.field public static final DP_INPUT_TYPE_STYLUS:B = 0x1t

.field public static final DP_MOUSE_SENSITIVITY_FAST:B = 0x4t

.field public static final DP_MOUSE_SENSITIVITY_MEDIUM:B = 0x2t

.field public static final DP_MOUSE_SENSITIVITY_SLOW:B = 0x1t

.field public static final DP_OFF_SCREEN_MODE_DISABLED:B = 0x0t

.field public static final DP_OFF_SCREEN_MODE_DUPLICATE:B = 0x2t

.field public static final DP_OFF_SCREEN_MODE_EXTEND:B = 0x1t

.field public static final DP_PORTRAIT_SIDE_LEFT:B = 0x0t

.field public static final DP_PORTRAIT_SIDE_RIGHT:B = 0x1t

.field public static final DP_POWER_PROFILE_OPTIMIZE_ACCURACY:B = 0x0t

.field public static final DP_POWER_PROFILE_OPTIMIZE_POWER:B = 0x1t

.field public static final DP_SMARTER_STAND_ORIENTATION_LANDSCAPE:B = 0x0t

.field public static final DP_SMARTER_STAND_ORIENTATION_LANDSCAPE_REVERSED:B = 0x2t

.field public static final DP_SMARTER_STAND_ORIENTATION_PORTRAIT:B = 0x1t

.field public static final DP_SMARTER_STAND_ORIENTATION_PORTRAIT_REVERSED:B = 0x3t

.field private static final MAX_CONFIG_MSG_SIZE:I = 0x400


# instance fields
.field private mEraseButton:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;

.field private mInputType:B

.field private mMouseSensitivity:B

.field private mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

.field private final mOffScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

.field private mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

.field private final mOnScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

.field private mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

.field private mPowerSaveMode:B

.field private mSendAllDataEventsToSideChannel:Z

.field private mStartPenOnBoot:Z

.field private mStopPenOnScreenOff:Z

.field private mTouchRange:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 296
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;

    invoke-direct {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;-><init>()V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x3

    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    invoke-direct {v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    .line 150
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    invoke-direct {v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    .line 152
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    invoke-direct {v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    .line 154
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    invoke-direct {v0, p0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    .line 156
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    invoke-direct {v0, p0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    .line 158
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;

    invoke-direct {v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mEraseButton:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;

    .line 227
    const/16 v0, 0x23

    iput v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mTouchRange:I

    .line 228
    iput-byte v2, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mPowerSaveMode:B

    .line 229
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    const/16 v1, 0x190

    iput v1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->maxRange:I

    .line 230
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iput-boolean v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->enable:Z

    .line 231
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iput-boolean v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->showIcon:Z

    .line 232
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    const/16 v1, 0x190

    iput v1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->maxRange:I

    .line 233
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iput-boolean v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->enable:Z

    .line 234
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iput-boolean v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->showIcon:Z

    .line 235
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iput-byte v5, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->mode:B

    .line 236
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->smarterStand:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;

    iput-boolean v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;->enable:Z

    .line 237
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->smarterStand:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;

    iput v2, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;->supportedOffScreenOrientation:I

    .line 238
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    new-array v1, v4, [F

    fill-array-data v1, :array_0

    iput-object v1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->origin:[F

    .line 241
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    new-array v1, v4, [F

    fill-array-data v1, :array_1

    iput-object v1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->endX:[F

    .line 244
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    new-array v1, v4, [F

    fill-array-data v1, :array_2

    iput-object v1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->endY:[F

    .line 247
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iput-byte v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->portraitSide:B

    .line 248
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iput-byte v2, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->destination:B

    .line 249
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iput-boolean v2, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->isMapped:Z

    .line 250
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iput-byte v2, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->destination:B

    .line 251
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iput-boolean v2, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->isMapped:Z

    .line 252
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mEraseButton:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;

    iput v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;->index:I

    .line 253
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mEraseButton:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;

    iput-byte v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;->mode:B

    .line 254
    iput-boolean v2, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mSendAllDataEventsToSideChannel:Z

    .line 255
    iput-boolean v2, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mStopPenOnScreenOff:Z

    .line 256
    iput-boolean v2, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mStartPenOnBoot:Z

    .line 257
    iput-byte v3, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mInputType:B

    .line 258
    iput-byte v5, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mMouseSensitivity:B

    .line 259
    return-void

    .line 238
    :array_0
    .array-data 4
        -0x3eb9999a    # -12.4f
        0x43a66148    # 332.76f
        -0x3ee00000    # -10.0f
    .end array-data

    .line 241
    :array_1
    .array-data 4
        0x4373eb85    # 243.92f
        0x43a66148    # 332.76f
        -0x3ee00000    # -10.0f
    .end array-data

    .line 244
    :array_2
    .array-data 4
        -0x3eb9999a    # -12.4f
        0x433c947b    # 188.58f
        -0x3ee00000    # -10.0f
    .end array-data
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 308
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    invoke-direct {v0, v3}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    .line 150
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    invoke-direct {v0, v3}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    .line 152
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    invoke-direct {v0, v3}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    .line 154
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    invoke-direct {v0, p0, v3}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    .line 156
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    invoke-direct {v0, p0, v3}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    .line 158
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;

    invoke-direct {v0, v3}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mEraseButton:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;

    .line 309
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    iput-byte v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->mode:B

    .line 310
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mTouchRange:I

    .line 311
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mPowerSaveMode:B

    .line 312
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->maxRange:I

    .line 313
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->enable:Z

    .line 314
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, v3, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->showIcon:Z

    .line 315
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->maxRange:I

    .line 316
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, v3, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->enable:Z

    .line 317
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, v3, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->showIcon:Z

    .line 318
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->smarterStand:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, v3, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;->enable:Z

    .line 319
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->smarterStand:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;->supportedOffScreenOrientation:I

    .line 320
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->origin:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readFloatArray([F)V

    .line 321
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->endX:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readFloatArray([F)V

    .line 322
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->endY:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readFloatArray([F)V

    .line 323
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    iput-byte v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->destination:B

    .line 324
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, v3, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->isMapped:Z

    .line 325
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    iput-byte v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->destination:B

    .line 326
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, v3, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->isMapped:Z

    .line 327
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mEraseButton:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;->index:I

    .line 328
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mEraseButton:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    iput-byte v3, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;->mode:B

    .line 329
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mSendAllDataEventsToSideChannel:Z

    .line 330
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mStopPenOnScreenOff:Z

    .line 331
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_9

    :goto_9
    iput-boolean v1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mStartPenOnBoot:Z

    .line 332
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mInputType:B

    .line 333
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mMouseSensitivity:B

    .line 334
    return-void

    :cond_0
    move v0, v2

    .line 313
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 314
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 316
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 317
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 318
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 324
    goto :goto_5

    :cond_6
    move v0, v2

    .line 326
    goto :goto_6

    :cond_7
    move v0, v2

    .line 329
    goto :goto_7

    :cond_8
    move v0, v2

    .line 330
    goto :goto_8

    :cond_9
    move v1, v2

    .line 331
    goto :goto_9
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$1;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 264
    const/4 v0, 0x0

    return v0
.end method

.method public getEraseButtonIndex()I
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mEraseButton:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;

    iget v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;->index:I

    return v0
.end method

.method public getEraseButtonMode()B
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mEraseButton:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;

    iget-byte v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;->mode:B

    return v0
.end method

.method public getInputType()B
    .locals 1

    .prologue
    .line 545
    iget-byte v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mInputType:B

    return v0
.end method

.method public getMouseSensitivity()B
    .locals 1

    .prologue
    .line 549
    iget-byte v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mMouseSensitivity:B

    return v0
.end method

.method public getOffSceenPortraitSide()B
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-byte v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->portraitSide:B

    return v0
.end method

.method public getOffScreenCoordReportDestination()B
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iget-byte v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->destination:B

    return v0
.end method

.method public getOffScreenCoordReportIsMapped()Z
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iget-boolean v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->isMapped:Z

    return v0
.end method

.method public getOffScreenHoverMaxRange()I
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iget v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->maxRange:I

    return v0
.end method

.method public getOffScreenMode()B
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-byte v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->mode:B

    return v0
.end method

.method public getOffScreenPlane([F[F[F)V
    .locals 2
    .param p1, "origin"    # [F
    .param p2, "endX"    # [F
    .param p3, "endY"    # [F

    .prologue
    .line 412
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 413
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v1, v1, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->origin:[F

    aget v1, v1, v0

    aput v1, p1, v0

    .line 414
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v1, v1, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->endX:[F

    aget v1, v1, v0

    aput v1, p2, v0

    .line 415
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v1, v1, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->endY:[F

    aget v1, v1, v0

    aput v1, p3, v0

    .line 412
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 417
    :cond_0
    return-void
.end method

.method public getOnScreenCoordReportDestination()B
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iget-byte v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->destination:B

    return v0
.end method

.method public getOnScreenCoordReportIsMapped()Z
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iget-boolean v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->isMapped:Z

    return v0
.end method

.method public getOnScreenHoverMaxRange()I
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iget v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->maxRange:I

    return v0
.end method

.method public getPowerSave()B
    .locals 1

    .prologue
    .line 211
    iget-byte v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mPowerSaveMode:B

    return v0
.end method

.method public getSendAllDataEventsToSideChannel()Z
    .locals 1

    .prologue
    .line 454
    iget-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mSendAllDataEventsToSideChannel:Z

    return v0
.end method

.method public getSmarterStandSupportedOrientation()I
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->smarterStand:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;

    iget v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;->supportedOffScreenOrientation:I

    return v0
.end method

.method public getTouchRange()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mTouchRange:I

    return v0
.end method

.method public isOffScreenHoverEnabled()Z
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iget-boolean v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->enable:Z

    return v0
.end method

.method public isOnScreenHoverEnabled()Z
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iget-boolean v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->enable:Z

    return v0
.end method

.method public isShowingOffScreenHoverIcon()Z
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iget-boolean v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->showIcon:Z

    return v0
.end method

.method public isShowingOnScreenHoverIcon()Z
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iget-boolean v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->showIcon:Z

    return v0
.end method

.method public isSmarterStandEnabled()Z
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->smarterStand:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;

    iget-boolean v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;->enable:Z

    return v0
.end method

.method public isStartPenOnBootEnabled()Z
    .locals 1

    .prologue
    .line 472
    iget-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mStartPenOnBoot:Z

    return v0
.end method

.method public isStopPenOnScreenOffEnabled()Z
    .locals 1

    .prologue
    .line 463
    iget-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mStopPenOnScreenOff:Z

    return v0
.end method

.method public marshalForDaemon()[B
    .locals 9

    .prologue
    const/4 v7, 0x3

    const/4 v8, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 512
    const/16 v4, 0x400

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 513
    .local v2, "msg":Ljava/nio/ByteBuffer;
    sget-object v4, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 515
    new-array v3, v7, [F

    .line 516
    .local v3, "origin":[F
    new-array v0, v7, [F

    .line 517
    .local v0, "endX":[F
    new-array v1, v7, [F

    .line 518
    .local v1, "endY":[F
    invoke-virtual {p0, v3, v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOffScreenPlane([F[F[F)V

    .line 519
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOffScreenMode()B

    move-result v4

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getTouchRange()I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getPowerSave()B

    move-result v7

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOnScreenHoverMaxRange()I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isOnScreenHoverEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v5

    :goto_0
    int-to-byte v4, v4

    invoke-virtual {v7, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isShowingOnScreenHoverIcon()Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v5

    :goto_1
    int-to-byte v4, v4

    invoke-virtual {v7, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOffScreenHoverMaxRange()I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isOffScreenHoverEnabled()Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v5

    :goto_2
    int-to-byte v4, v4

    invoke-virtual {v7, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isShowingOffScreenHoverIcon()Z

    move-result v4

    if-eqz v4, :cond_3

    move v4, v5

    :goto_3
    int-to-byte v4, v4

    invoke-virtual {v7, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->isSmarterStandEnabled()Z

    move-result v4

    if-eqz v4, :cond_4

    move v4, v5

    :goto_4
    int-to-byte v4, v4

    invoke-virtual {v7, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v4

    aget v7, v3, v6

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v4

    aget v7, v3, v5

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v4

    aget v7, v3, v8

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v4

    aget v7, v0, v6

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v4

    aget v7, v0, v5

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v4

    aget v7, v0, v8

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v4

    aget v7, v1, v6

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v4

    aget v7, v1, v5

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v4

    aget v7, v1, v8

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOnScreenCoordReportDestination()B

    move-result v7

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOnScreenCoordReportIsMapped()Z

    move-result v4

    if-eqz v4, :cond_5

    move v4, v5

    :goto_5
    int-to-byte v4, v4

    invoke-virtual {v7, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOffScreenCoordReportDestination()B

    move-result v7

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v7

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOffScreenCoordReportIsMapped()Z

    move-result v4

    if-eqz v4, :cond_6

    move v4, v5

    :goto_6
    int-to-byte v4, v4

    invoke-virtual {v7, v4}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getEraseButtonIndex()I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getEraseButtonMode()B

    move-result v7

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getSendAllDataEventsToSideChannel()Z

    move-result v7

    if-eqz v7, :cond_7

    :goto_7
    int-to-byte v5, v5

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getInputType()B

    move-result v5

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getMouseSensitivity()B

    move-result v5

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 537
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    invoke-static {v4, v5}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v4

    return-object v4

    :cond_0
    move v4, v6

    .line 519
    goto/16 :goto_0

    :cond_1
    move v4, v6

    goto/16 :goto_1

    :cond_2
    move v4, v6

    goto/16 :goto_2

    :cond_3
    move v4, v6

    goto/16 :goto_3

    :cond_4
    move v4, v6

    goto/16 :goto_4

    :cond_5
    move v4, v6

    goto :goto_5

    :cond_6
    move v4, v6

    goto :goto_6

    :cond_7
    move v5, v6

    goto :goto_7
.end method

.method public setEraseButtonBehavior(B)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 3
    .param p1, "mode"    # B

    .prologue
    .line 485
    packed-switch p1, :pswitch_data_0

    .line 491
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad eraser-button mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 488
    :pswitch_0
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mEraseButton:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;

    iput-byte p1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;->mode:B

    .line 493
    return-object p0

    .line 485
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setEraseButtonIndex(I)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 476
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mEraseButton:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;

    iput p1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;->index:I

    .line 477
    return-object p0
.end method

.method public setInputType(B)V
    .locals 0
    .param p1, "inputType"    # B

    .prologue
    .line 541
    iput-byte p1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mInputType:B

    .line 542
    return-void
.end method

.method public setMouseSensitivity(B)V
    .locals 0
    .param p1, "mouseSensitivity"    # B

    .prologue
    .line 553
    iput-byte p1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mMouseSensitivity:B

    .line 554
    return-void
.end method

.method public setOffScreenCoordReporting(BZ)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 1
    .param p1, "destination"    # B
    .param p2, "isMapped"    # Z

    .prologue
    .line 426
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iput-byte p1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->destination:B

    .line 427
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iput-boolean p2, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->isMapped:Z

    .line 428
    return-object p0
.end method

.method public setOffScreenHoverEnable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 372
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iput-boolean p1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->enable:Z

    .line 373
    return-void
.end method

.method public setOffScreenHoverMaxRange(I)V
    .locals 1
    .param p1, "maxRange"    # I

    .prologue
    .line 364
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iput p1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->maxRange:I

    .line 365
    return-void
.end method

.method public setOffScreenMode(B)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 3
    .param p1, "mode"    # B

    .prologue
    .line 179
    packed-switch p1, :pswitch_data_0

    .line 186
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad off-screen mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 183
    :pswitch_0
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iput-byte p1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->mode:B

    .line 188
    return-object p0

    .line 179
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setOffScreenPlane([F[F[F)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 3
    .param p1, "origin"    # [F
    .param p2, "endX"    # [F
    .param p3, "endY"    # [F

    .prologue
    const/4 v2, 0x3

    .line 405
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    invoke-static {p1, v2}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v1

    iput-object v1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->origin:[F

    .line 406
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    invoke-static {p2, v2}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v1

    iput-object v1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->endX:[F

    .line 407
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    invoke-static {p3, v2}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v1

    iput-object v1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->endY:[F

    .line 408
    return-object p0
.end method

.method public setOffScreenPortraitSide(B)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 1
    .param p1, "code"    # B

    .prologue
    .line 502
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iput-byte p1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->portraitSide:B

    .line 503
    return-object p0
.end method

.method public setOnScreenCoordReporting(BZ)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 1
    .param p1, "destination"    # B
    .param p2, "isMapped"    # Z

    .prologue
    .line 420
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iput-byte p1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->destination:B

    .line 421
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iput-boolean p2, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->isMapped:Z

    .line 422
    return-object p0
.end method

.method public setOnScreenHoverEnable(Z)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 346
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iput-boolean p1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->enable:Z

    .line 347
    return-object p0
.end method

.method public setOnScreenHoverMaxRange(I)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 1
    .param p1, "maxRange"    # I

    .prologue
    .line 337
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iput p1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->maxRange:I

    .line 338
    return-object p0
.end method

.method public setPowerSave(B)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 0
    .param p1, "mode"    # B

    .prologue
    .line 216
    iput-byte p1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mPowerSaveMode:B

    .line 217
    return-object p0
.end method

.method public setSendAllDataEventsToSideChannel(Z)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 449
    iput-boolean p1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mSendAllDataEventsToSideChannel:Z

    .line 450
    return-object p0
.end method

.method public setShowOffScreenHoverIcon(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 380
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iput-boolean p1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->showIcon:Z

    .line 381
    return-void
.end method

.method public setShowOnScreenHoverIcon(Z)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 355
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iput-boolean p1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->showIcon:Z

    .line 356
    return-object p0
.end method

.method public setSmarterStand(Z)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 388
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->smarterStand:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;

    iput-boolean p1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;->enable:Z

    .line 389
    return-object p0
.end method

.method public setSmarterStandSupportedOrientation(I)V
    .locals 1
    .param p1, "supportedOrientation"    # I

    .prologue
    .line 397
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->smarterStand:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;

    iput p1, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;->supportedOffScreenOrientation:I

    .line 398
    return-void
.end method

.method public setStartPenOnBoot(Z)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 467
    iput-boolean p1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mStartPenOnBoot:Z

    .line 468
    return-object p0
.end method

.method public setStopPenOnScreenOff(Z)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 458
    iput-boolean p1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mStopPenOnScreenOff:Z

    .line 459
    return-object p0
.end method

.method public setTouchRange(I)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 206
    iput p1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mTouchRange:I

    .line 207
    return-object p0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 269
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-byte v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->mode:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 270
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mTouchRange:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 271
    iget-byte v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mPowerSaveMode:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 272
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iget v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->maxRange:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 273
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iget-boolean v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->enable:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 274
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iget-boolean v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->showIcon:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 275
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iget v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->maxRange:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 276
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iget-boolean v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->enable:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 277
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenHover:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;

    iget-boolean v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$Hover;->showIcon:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 278
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->smarterStand:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;

    iget-boolean v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;->enable:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 279
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->smarterStand:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;

    iget v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$SmarterStand;->supportedOffScreenOrientation:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 280
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->origin:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloatArray([F)V

    .line 281
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->endX:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloatArray([F)V

    .line 282
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreen:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$OffScreen;->endY:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloatArray([F)V

    .line 283
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iget-byte v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->destination:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 284
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOnScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iget-boolean v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->isMapped:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 285
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iget-byte v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->destination:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 286
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mOffScreenCoordReport:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;

    iget-boolean v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$CoordReport;->isMapped:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 287
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mEraseButton:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;

    iget v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;->index:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 288
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mEraseButton:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;

    iget-byte v0, v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig$EraseButton;->mode:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 289
    iget-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mSendAllDataEventsToSideChannel:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 290
    iget-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mStopPenOnScreenOff:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 291
    iget-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mStartPenOnBoot:Z

    if-eqz v0, :cond_9

    :goto_9
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 292
    iget-byte v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mInputType:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 293
    iget-byte v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->mMouseSensitivity:B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 294
    return-void

    :cond_0
    move v0, v2

    .line 273
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 274
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 276
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 277
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 278
    goto :goto_4

    :cond_5
    move v0, v2

    .line 284
    goto :goto_5

    :cond_6
    move v0, v2

    .line 286
    goto :goto_6

    :cond_7
    move v0, v2

    .line 289
    goto :goto_7

    :cond_8
    move v0, v2

    .line 290
    goto :goto_8

    :cond_9
    move v1, v2

    .line 291
    goto :goto_9
.end method
