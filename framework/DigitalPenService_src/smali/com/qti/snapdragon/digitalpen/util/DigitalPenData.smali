.class public Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;
.super Ljava/lang/Object;
.source "DigitalPenData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;",
            ">;"
        }
    .end annotation
.end field

.field private static final NUM_OF_SIDE_BUTTONS:I = 0x3

.field public static final REGION_ALL:I = 0x0

.field public static final REGION_OFF_SCREEN:I = 0x2

.field public static final REGION_ON_SCREEN:I = 0x1


# instance fields
.field private mPenState:Z

.field private mPressure:I

.field private mRegion:I

.field private mSideButtonsState:[I

.field private mX:I

.field private mXTilt:I

.field private mY:I

.field private mYTilt:I

.field private mZ:I

.field private mZTilt:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 134
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData$1;

    invoke-direct {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData$1;-><init>()V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method public constructor <init>(IIIIIIIZ[II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "z"    # I
    .param p4, "xTilt"    # I
    .param p5, "yTilt"    # I
    .param p6, "zTilt"    # I
    .param p7, "pressure"    # I
    .param p8, "penState"    # Z
    .param p9, "sideButtonsState"    # [I
    .param p10, "region"    # I

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput p1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mX:I

    .line 65
    iput p2, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mY:I

    .line 66
    iput p3, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mZ:I

    .line 67
    iput p4, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mXTilt:I

    .line 68
    iput p5, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mYTilt:I

    .line 69
    iput p6, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mZTilt:I

    .line 70
    iput p7, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mPressure:I

    .line 71
    iput-boolean p8, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mPenState:Z

    .line 72
    const/4 v0, 0x3

    invoke-static {p9, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mSideButtonsState:[I

    .line 73
    iput p10, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mRegion:I

    .line 74
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x1

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mX:I

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mY:I

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mZ:I

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mXTilt:I

    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mYTilt:I

    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mZTilt:I

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mPressure:I

    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mPenState:Z

    .line 154
    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mSideButtonsState:[I

    .line 155
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mSideButtonsState:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readIntArray([I)V

    .line 156
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mRegion:I

    .line 157
    return-void

    .line 153
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qti/snapdragon/digitalpen/util/DigitalPenData$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenData$1;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public getPenState()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mPenState:Z

    return v0
.end method

.method public getPressure()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mPressure:I

    return v0
.end method

.method public getRegion()I
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mRegion:I

    return v0
.end method

.method public getSideButtonsState()[I
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mSideButtonsState:[I

    return-object v0
.end method

.method public getX()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mX:I

    return v0
.end method

.method public getXTilt()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mXTilt:I

    return v0
.end method

.method public getY()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mY:I

    return v0
.end method

.method public getYTilt()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mYTilt:I

    return v0
.end method

.method public getZ()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mZ:I

    return v0
.end method

.method public getZTilt()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mZTilt:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 122
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mX:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 123
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mY:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 124
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mZ:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 125
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mXTilt:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 126
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mYTilt:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 127
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mZTilt:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 128
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mPressure:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 129
    iget-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mPenState:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mSideButtonsState:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 131
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->mRegion:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 132
    return-void

    .line 129
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
