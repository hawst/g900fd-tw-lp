.class public Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;
.super Ljava/lang/Object;
.source "DigitalPenEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final BAD_SCENARIO_BETWEEN_TWO_MICS:I = 0x2

.field public static final BAD_SCENARIO_MULTILATERAION:I = 0x1

.field public static final BAD_SCENARIO_OK:I = 0x0

.field public static final BATTERY_LEVEL_UNAVAILABLE:I = -0x1

.field public static final BATTERY_LOW:I = 0x1

.field public static final BATTERY_OK:I = 0x0

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final PARAM_BAD_SCENARIO:I = 0x3c

.field public static final PARAM_CURRENT_PEN_DRAW_SCREEN:I = 0x1e

.field public static final PARAM_CURRENT_POWER_STATE:I = 0x0

.field public static final PARAM_INTERNAL_ERROR_NUMBER:I = 0xa

.field public static final PARAM_MIC_BLOCKED:I = 0x14

.field public static final PARAM_PEN_BATTERY_LEVEL:I = 0x29

.field public static final PARAM_PEN_BATTERY_STATE:I = 0x28

.field public static final PARAM_PREVIOUS_PEN_DRAW_SCREEN:I = 0x1f

.field public static final PARAM_PREVIOUS_POWER_STATE:I = 0x1

.field public static final PARAM_SIDE_CHANNEL_CAPABLE:I = 0x50

.field public static final PARAM_SPUR_STATE:I = 0x32

.field public static final PARAM_VERSION_MAJOR:I = 0x46

.field public static final PARAM_VERSION_MINOR:I = 0x47

.field public static final POWER_STATE_ACTIVE:I = 0x0

.field public static final POWER_STATE_IDLE:I = 0x2

.field public static final POWER_STATE_OFF:I = 0x3

.field public static final POWER_STATE_STANDBY:I = 0x1

.field public static final SCREEN_NON_SUPPORTED:I = 0x0

.field public static final SCREEN_OFF:I = 0x2

.field public static final SCREEN_ON:I = 0x1

.field public static final SPURS_EXIST:I = 0x1

.field public static final SPURS_OK:I = 0x0

.field public static final TYPE_BAD_SCENARIO:I = 0x10

.field public static final TYPE_INTERNAL_DAEMON_ERROR:I = 0x1

.field public static final TYPE_MIC_BLOCKED:I = 0x2

.field public static final TYPE_PEN_BATTERY_STATE:I = 0x4

.field public static final TYPE_POWER_STATE_CHANGED:I = 0x0

.field public static final TYPE_SIDE_CHANNEL_CAPABLE:I = 0x40

.field public static final TYPE_SPUR_STATE:I = 0x8

.field public static final TYPE_VERSION:I = 0x20


# instance fields
.field private mEventType:I

.field private mParams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 343
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent$1;

    invoke-direct {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent$1;-><init>()V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->mParams:Ljava/util/Map;

    .line 246
    return-void
.end method

.method public constructor <init>(I[I)V
    .locals 4
    .param p1, "eventType"    # I
    .param p2, "params"    # [I

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->mParams:Ljava/util/Map;

    .line 256
    iput p1, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->mEventType:I

    .line 258
    sparse-switch p1, :sswitch_data_0

    .line 295
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown event type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 260
    :sswitch_0
    invoke-direct {p0, p2, v3}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->checkNumParams([II)V

    .line 261
    aget v0, p2, v1

    invoke-direct {p0, v1, v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->setParameter(II)V

    .line 262
    aget v0, p2, v2

    invoke-direct {p0, v2, v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->setParameter(II)V

    .line 297
    :goto_0
    return-void

    .line 265
    :sswitch_1
    invoke-direct {p0, p2, v2}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->checkNumParams([II)V

    .line 266
    const/16 v0, 0xa

    aget v1, p2, v1

    invoke-direct {p0, v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->setParameter(II)V

    goto :goto_0

    .line 269
    :sswitch_2
    invoke-direct {p0, p2, v2}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->checkNumParams([II)V

    .line 270
    const/16 v0, 0x14

    aget v1, p2, v1

    invoke-direct {p0, v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->setParameter(II)V

    goto :goto_0

    .line 273
    :sswitch_3
    invoke-direct {p0, p2, v2}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->checkNumParams([II)V

    .line 274
    const/16 v0, 0x28

    aget v1, p2, v1

    invoke-direct {p0, v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->setParameter(II)V

    .line 275
    const/16 v0, 0x29

    aget v1, p2, v2

    invoke-direct {p0, v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->setParameter(II)V

    goto :goto_0

    .line 278
    :sswitch_4
    invoke-direct {p0, p2, v2}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->checkNumParams([II)V

    .line 279
    const/16 v0, 0x32

    aget v1, p2, v1

    invoke-direct {p0, v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->setParameter(II)V

    goto :goto_0

    .line 282
    :sswitch_5
    invoke-direct {p0, p2, v2}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->checkNumParams([II)V

    .line 283
    const/16 v0, 0x3c

    aget v1, p2, v1

    invoke-direct {p0, v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->setParameter(II)V

    goto :goto_0

    .line 286
    :sswitch_6
    invoke-direct {p0, p2, v3}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->checkNumParams([II)V

    .line 287
    const/16 v0, 0x46

    aget v1, p2, v1

    invoke-direct {p0, v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->setParameter(II)V

    .line 288
    const/16 v0, 0x47

    aget v1, p2, v2

    invoke-direct {p0, v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->setParameter(II)V

    goto :goto_0

    .line 291
    :sswitch_7
    invoke-direct {p0, p2, v2}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->checkNumParams([II)V

    .line 292
    const/16 v0, 0x50

    aget v1, p2, v1

    invoke-direct {p0, v0, v1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->setParameter(II)V

    goto :goto_0

    .line 258
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x4 -> :sswitch_3
        0x8 -> :sswitch_4
        0x10 -> :sswitch_5
        0x20 -> :sswitch_6
        0x40 -> :sswitch_7
    .end sparse-switch
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->mParams:Ljava/util/Map;

    .line 355
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    iput v4, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->mEventType:I

    .line 357
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .local v1, "n":I
    move v2, v1

    .line 358
    .end local v1    # "n":I
    .local v2, "n":I
    :goto_0
    add-int/lit8 v1, v2, -0x1

    .end local v2    # "n":I
    .restart local v1    # "n":I
    if-lez v2, :cond_0

    .line 360
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 361
    .local v0, "key":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 362
    .local v3, "value":I
    invoke-direct {p0, v0, v3}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->setParameter(II)V

    move v2, v1

    .line 363
    .end local v1    # "n":I
    .restart local v2    # "n":I
    goto :goto_0

    .line 364
    .end local v0    # "key":I
    .end local v2    # "n":I
    .end local v3    # "value":I
    .restart local v1    # "n":I
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent$1;

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private checkNumParams([II)V
    .locals 2
    .param p1, "params"    # [I
    .param p2, "minParams"    # I

    .prologue
    .line 300
    array-length v0, p1

    if-ge v0, p2, :cond_0

    .line 301
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parameter array size is incorrect"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 303
    :cond_0
    return-void
.end method

.method private setParameter(II)V
    .locals 3
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 316
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->mParams:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 329
    const/4 v0, 0x0

    return v0
.end method

.method public getEventType()I
    .locals 1

    .prologue
    .line 307
    iget v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->mEventType:I

    return v0
.end method

.method public getParameterValue(I)I
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 321
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->mParams:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 322
    const/4 v0, -0x1

    .line 324
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->mParams:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 333
    iget v2, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->mEventType:I

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 335
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->mParams:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 337
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->mParams:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 338
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 339
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 341
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_0
    return-void
.end method
