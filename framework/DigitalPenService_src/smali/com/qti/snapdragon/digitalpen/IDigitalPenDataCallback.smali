.class public interface abstract Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;
.super Ljava/lang/Object;
.source "IDigitalPenDataCallback.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback$Stub;
    }
.end annotation


# virtual methods
.method public abstract onDigitalPenPropData(Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
