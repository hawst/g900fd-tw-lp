.class Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread$2;
.super Ljava/lang/Object;
.source "DigitalPenService.java"

# interfaces
.implements Lcom/qti/snapdragon/digitalpen/DirectoryObserver$FileChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->attachSettingsFileObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;


# direct methods
.method constructor <init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;)V
    .locals 0

    .prologue
    .line 943
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread$2;->this$1:Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEvent(ILjava/lang/String;)V
    .locals 3
    .param p1, "event"    # I
    .param p2, "file"    # Ljava/lang/String;

    .prologue
    .line 947
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread$2;->this$1:Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;

    iget-object v1, v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    monitor-enter v1

    .line 948
    :try_start_0
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread$2;->this$1:Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 949
    monitor-exit v1

    .line 963
    :goto_0
    return-void

    .line 951
    :cond_0
    const-string v0, "DigitalPenService"

    const-string v2, "Detected change to service_settings.xml, toggling service"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 954
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread$2;->this$1:Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->disable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 955
    const-string v0, "DigitalPenService"

    const-string v2, "Couldn\'t disable service after global settings change"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 958
    :cond_1
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread$2;->this$1:Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;

    iget-object v0, v0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$PollingPostBootThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->enable()Z

    move-result v0

    if-nez v0, :cond_2

    .line 959
    const-string v0, "DigitalPenService"

    const-string v2, "Couldn\'t enable service after global settings change"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 962
    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
