.class public final enum Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;
.super Ljava/lang/Enum;
.source "DigitalPenGlobalControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MouseSensitivity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

.field public static final enum FAST:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

.field public static final enum MEDIUM:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

.field public static final enum SLOW:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;


# instance fields
.field private final code:B


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 130
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    const-string v1, "SLOW"

    invoke-direct {v0, v1, v5, v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->SLOW:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    .line 131
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3, v4}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->MEDIUM:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    .line 132
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    const-string v1, "FAST"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v4, v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->FAST:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    .line 129
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    sget-object v1, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->SLOW:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->MEDIUM:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->FAST:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    aput-object v1, v0, v4

    sput-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->$VALUES:[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IB)V
    .locals 0
    .param p3, "code"    # B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B)V"
        }
    .end annotation

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 137
    iput-byte p3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->code:B

    .line 138
    return-void
.end method

.method static synthetic access$1000(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;
    .locals 1
    .param p0, "x0"    # B

    .prologue
    .line 129
    invoke-static {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->fromCode(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;)B
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    .prologue
    .line 129
    iget-byte v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->code:B

    return v0
.end method

.method private static fromCode(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;
    .locals 5
    .param p0, "code"    # B

    .prologue
    .line 141
    invoke-static {}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->values()[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    move-result-object v0

    .local v0, "arr$":[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 142
    .local v3, "mode":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;
    iget-byte v4, v3, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->code:B

    if-ne v4, p0, :cond_0

    .line 146
    .end local v3    # "mode":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;
    :goto_1
    return-object v3

    .line 141
    .restart local v3    # "mode":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 146
    .end local v3    # "mode":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 129
    const-class v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    return-object v0
.end method

.method public static values()[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;
    .locals 1

    .prologue
    .line 129
    sget-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->$VALUES:[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    invoke-virtual {v0}, [Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$MouseSensitivity;

    return-object v0
.end method
