.class public Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl;
.super Ljava/lang/Object;
.source "DigitalPenGlobalControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;,
        Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenEventListener;
    }
.end annotation


# instance fields
.field private attachedService:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

.field private config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 335
    return-void
.end method


# virtual methods
.method protected attachService()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl;->attachedService:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    if-nez v0, :cond_0

    .line 339
    const-string v0, "DigitalPen"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    move-result-object v0

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl;->attachedService:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl;->attachedService:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    if-nez v0, :cond_1

    .line 343
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not connect to Digital Pen service"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :cond_1
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl;->attachedService:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    return-object v0
.end method

.method public commitSettings(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;)V
    .locals 3
    .param p1, "settings"    # Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 328
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl;->attachService()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    move-result-object v0

    .line 329
    .local v0, "service":Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    # invokes: Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->getConfig()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    invoke-static {p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;->access$1200(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;)Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->setGlobalConfig(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 330
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Couldn\'t set default config in service"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 332
    :cond_0
    return-void
.end method

.method public disablePenFeature()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 349
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl;->attachService()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    move-result-object v0

    .line 350
    .local v0, "service":Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    invoke-interface {v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 357
    :cond_0
    return-void

    .line 353
    :cond_1
    invoke-interface {v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->disable()Z

    move-result v1

    .line 354
    .local v1, "success":Z
    if-nez v1, :cond_0

    .line 355
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Problem disabling pen feature"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public enablePenFeature()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl;->attachService()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    move-result-object v0

    .line 361
    .local v0, "service":Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    invoke-interface {v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 368
    :cond_0
    return-void

    .line 364
    :cond_1
    invoke-interface {v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->enable()Z

    move-result v1

    .line 365
    .local v1, "success":Z
    if-nez v1, :cond_0

    .line 366
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Problem enabling pen feature"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getCurrentSettings()Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 321
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl;->attachService()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    move-result-object v0

    .line 322
    .local v0, "service":Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    invoke-interface {v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->getConfig()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v2

    iput-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    .line 323
    new-instance v1, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;

    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl;->config:Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;-><init>(Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$1;)V

    .line 324
    .local v1, "settings":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
    return-object v1
.end method

.method public isPenFeatureEnabled()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 371
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl;->attachService()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    move-result-object v0

    .line 372
    .local v0, "service":Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    invoke-interface {v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->isEnabled()Z

    move-result v1

    return v1
.end method

.method public registerEventListener(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenEventListener;)V
    .locals 4
    .param p1, "eventCb"    # Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenEventListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 376
    invoke-virtual {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl;->attachService()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    move-result-object v0

    .line 377
    .local v0, "service":Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    new-instance v2, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$1;

    invoke-direct {v2, p0, p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$1;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl;Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenEventListener;)V

    invoke-interface {v0, v2}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->registerEventCallback(Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;)Z

    move-result v1

    .line 385
    .local v1, "success":Z
    if-nez v1, :cond_0

    .line 386
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Problem registering event listener"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 388
    :cond_0
    return-void
.end method
