.class Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$OffScreen;
.super Ljava/lang/Object;
.source "DigitalPenConfigMirror.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "OffScreen"
.end annotation


# instance fields
.field public endX:[F
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "EndX"
    .end annotation
.end field

.field public endY:[F
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "EndY"
    .end annotation
.end field

.field public mode:B
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Mode"
    .end annotation
.end field

.field public origin:[F
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "Origin"
    .end annotation
.end field

.field public portraitSide:B
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "PortraitSide"
    .end annotation
.end field

.field public smarterStand:Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$SmarterStand;
    .annotation runtime Lorg/simpleframework/xml/Element;
        name = "SmarterStand"
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$SmarterStand;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$SmarterStand;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$1;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$OffScreen;->smarterStand:Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$SmarterStand;

    .line 44
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$OffScreen;->origin:[F

    .line 47
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$OffScreen;->endX:[F

    .line 50
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$OffScreen;->endY:[F

    return-void
.end method

.method synthetic constructor <init>(Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$1;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenConfigMirror$OffScreen;-><init>()V

    return-void
.end method
