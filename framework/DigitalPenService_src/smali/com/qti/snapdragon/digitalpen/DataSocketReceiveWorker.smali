.class Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker;
.super Ljava/lang/Object;
.source "DataSocketReceiveWorker.java"

# interfaces
.implements Lcom/qti/snapdragon/digitalpen/SocketThread$ReceiveWorker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker$OnDataListener;
    }
.end annotation


# static fields
.field static final NUM_PARAMS:I = 0xc

.field static final RAW_PACKET_SIZE:I = 0x30


# instance fields
.field private listener:Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker$OnDataListener;


# direct methods
.method public constructor <init>(Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker$OnDataListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker$OnDataListener;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker;->listener:Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker$OnDataListener;

    .line 33
    return-void
.end method


# virtual methods
.method public receiveLoop(Ljava/io/InputStream;)V
    .locals 13
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 42
    if-nez p1, :cond_0

    .line 43
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "null not a valid InputStream"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 47
    :cond_0
    const/16 v1, 0x30

    new-array v12, v1, [B

    .line 48
    .local v12, "data":[B
    invoke-static {v12}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    sget-object v2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v11

    .line 51
    .local v11, "buf":Ljava/nio/IntBuffer;
    :goto_0
    const/4 v1, 0x0

    const/16 v2, 0x30

    invoke-virtual {p1, v12, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    const/16 v2, 0x30

    if-ne v1, v2, :cond_2

    .line 54
    const/4 v1, 0x3

    new-array v9, v1, [I

    const/4 v1, 0x0

    const/16 v2, 0x8

    invoke-virtual {v11, v2}, Ljava/nio/IntBuffer;->get(I)I

    move-result v2

    aput v2, v9, v1

    const/4 v1, 0x1

    const/16 v2, 0x9

    invoke-virtual {v11, v2}, Ljava/nio/IntBuffer;->get(I)I

    move-result v2

    aput v2, v9, v1

    const/4 v1, 0x2

    const/16 v2, 0xa

    invoke-virtual {v11, v2}, Ljava/nio/IntBuffer;->get(I)I

    move-result v2

    aput v2, v9, v1

    .line 60
    .local v9, "sideButtonsState":[I
    new-instance v0, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;

    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Ljava/nio/IntBuffer;->get(I)I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v11, v2}, Ljava/nio/IntBuffer;->get(I)I

    move-result v2

    const/4 v3, 0x2

    invoke-virtual {v11, v3}, Ljava/nio/IntBuffer;->get(I)I

    move-result v3

    const/4 v4, 0x3

    invoke-virtual {v11, v4}, Ljava/nio/IntBuffer;->get(I)I

    move-result v4

    const/4 v5, 0x4

    invoke-virtual {v11, v5}, Ljava/nio/IntBuffer;->get(I)I

    move-result v5

    const/4 v6, 0x5

    invoke-virtual {v11, v6}, Ljava/nio/IntBuffer;->get(I)I

    move-result v6

    const/4 v7, 0x6

    invoke-virtual {v11, v7}, Ljava/nio/IntBuffer;->get(I)I

    move-result v7

    const/4 v8, 0x7

    invoke-virtual {v11, v8}, Ljava/nio/IntBuffer;->get(I)I

    move-result v8

    const/4 v10, 0x1

    if-ne v8, v10, :cond_1

    const/4 v8, 0x1

    :goto_1
    const/16 v10, 0xb

    invoke-virtual {v11, v10}, Ljava/nio/IntBuffer;->get(I)I

    move-result v10

    invoke-direct/range {v0 .. v10}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;-><init>(IIIIIIIZ[II)V

    .line 74
    .local v0, "dataToSend":Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker;->listener:Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker$OnDataListener;

    invoke-interface {v1, v0}, Lcom/qti/snapdragon/digitalpen/DataSocketReceiveWorker$OnDataListener;->onData(Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;)V

    goto :goto_0

    .line 60
    .end local v0    # "dataToSend":Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;
    :cond_1
    const/4 v8, 0x0

    goto :goto_1

    .line 76
    .end local v9    # "sideButtonsState":[I
    :cond_2
    return-void
.end method
