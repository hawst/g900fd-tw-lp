.class Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;
.super Ljava/lang/Object;
.source "DigitalPenService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScreenState"
.end annotation


# instance fields
.field private mDaemonDisabledAfterScreenOffAction:Z

.field private mEnableStopPenOnScreenOff:Z

.field final synthetic this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;


# direct methods
.method private constructor <init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 243
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    iput-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->mEnableStopPenOnScreenOff:Z

    .line 245
    iput-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->mDaemonDisabledAfterScreenOffAction:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;Lcom/qti/snapdragon/digitalpen/DigitalPenService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService;
    .param p2, "x1"    # Lcom/qti/snapdragon/digitalpen/DigitalPenService$1;

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;-><init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)V

    return-void
.end method

.method private reportDestinationHasSocket(B)Z
    .locals 2
    .param p1, "reportDestination"    # B

    .prologue
    const/4 v0, 0x1

    .line 251
    if-eq v0, p1, :cond_0

    const/4 v1, 0x2

    if-ne v1, p1, :cond_1

    .line 255
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public changePenStateAccordingToScreenState(Ljava/lang/String;)V
    .locals 5
    .param p1, "screenState"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 276
    const-string v3, "android.intent.action.SCREEN_ON"

    if-ne p1, v3, :cond_1

    .line 279
    iget-boolean v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->mDaemonDisabledAfterScreenOffAction:Z

    if-eqz v2, :cond_0

    .line 280
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-virtual {v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->enable()Z

    .line 304
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    const-string v3, "android.intent.action.SCREEN_OFF"

    if-ne p1, v3, :cond_4

    .line 284
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # getter for: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->mConfigManager:Lcom/qti/snapdragon/digitalpen/ConfigManager;
    invoke-static {v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$600(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Lcom/qti/snapdragon/digitalpen/ConfigManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/qti/snapdragon/digitalpen/ConfigManager;->getCurrentConfig()Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;

    move-result-object v0

    .line 285
    .local v0, "currentConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOnScreenCoordReportDestination()B

    move-result v3

    invoke-direct {p0, v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->reportDestinationHasSocket(B)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getOffScreenCoordReportDestination()B

    move-result v3

    invoke-direct {p0, v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->reportDestinationHasSocket(B)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;->getSendAllDataEventsToSideChannel()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    move v1, v2

    .line 293
    .local v1, "sideChannelDestination":Z
    :goto_1
    iget-boolean v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->mEnableStopPenOnScreenOff:Z

    if-eqz v3, :cond_0

    if-nez v1, :cond_0

    .line 294
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-virtual {v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 295
    iget-object v3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    # invokes: Lcom/qti/snapdragon/digitalpen/DigitalPenService;->disableDaemon()Z
    invoke-static {v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->access$700(Lcom/qti/snapdragon/digitalpen/DigitalPenService;)Z

    .line 298
    iput-boolean v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->mDaemonDisabledAfterScreenOffAction:Z

    goto :goto_0

    .line 285
    .end local v1    # "sideChannelDestination":Z
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 302
    .end local v0    # "currentConfig":Lcom/qti/snapdragon/digitalpen/util/DigitalPenConfig;
    :cond_4
    const-string v2, "DigitalPenService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid screen action "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public resetPenState()V
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->mDaemonDisabledAfterScreenOffAction:Z

    .line 312
    return-void
.end method

.method public setStopPenOnScreenOff(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 262
    iput-boolean p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->mEnableStopPenOnScreenOff:Z

    .line 263
    iget-boolean v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$ScreenState;->mEnableStopPenOnScreenOff:Z

    if-eqz v0, :cond_0

    .line 264
    const-string v0, "DigitalPenService"

    const-string v1, "Enabling stopping the pen when main display is off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :goto_0
    return-void

    .line 266
    :cond_0
    const-string v0, "DigitalPenService"

    const-string v1, "Disabling stopping the pen when main display is off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
