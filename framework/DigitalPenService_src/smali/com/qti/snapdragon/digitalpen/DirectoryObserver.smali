.class public Lcom/qti/snapdragon/digitalpen/DirectoryObserver;
.super Ljava/lang/Object;
.source "DirectoryObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/digitalpen/DirectoryObserver$ListenerEntry;,
        Lcom/qti/snapdragon/digitalpen/DirectoryObserver$FileChangeListener;
    }
.end annotation


# instance fields
.field private fileObserver:Landroid/os/FileObserver;

.field private listenerMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/qti/snapdragon/digitalpen/DirectoryObserver$ListenerEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DirectoryObserver;->listenerMap:Ljava/util/HashMap;

    .line 44
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DirectoryObserver$1;

    invoke-direct {v0, p0, p1}, Lcom/qti/snapdragon/digitalpen/DirectoryObserver$1;-><init>(Lcom/qti/snapdragon/digitalpen/DirectoryObserver;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/qti/snapdragon/digitalpen/DirectoryObserver;->fileObserver:Landroid/os/FileObserver;

    .line 51
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DirectoryObserver;->fileObserver:Landroid/os/FileObserver;

    invoke-virtual {v0}, Landroid/os/FileObserver;->startWatching()V

    .line 52
    return-void
.end method


# virtual methods
.method protected handleEvent(ILjava/lang/String;)V
    .locals 2
    .param p1, "event"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 59
    iget-object v1, p0, Lcom/qti/snapdragon/digitalpen/DirectoryObserver;->listenerMap:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/digitalpen/DirectoryObserver$ListenerEntry;

    .line 60
    .local v0, "entry":Lcom/qti/snapdragon/digitalpen/DirectoryObserver$ListenerEntry;
    if-nez v0, :cond_1

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    iget v1, v0, Lcom/qti/snapdragon/digitalpen/DirectoryObserver$ListenerEntry;->mask:I

    and-int/2addr v1, p1

    if-eqz v1, :cond_0

    .line 64
    iget-object v1, v0, Lcom/qti/snapdragon/digitalpen/DirectoryObserver$ListenerEntry;->listener:Lcom/qti/snapdragon/digitalpen/DirectoryObserver$FileChangeListener;

    invoke-interface {v1, p1, p2}, Lcom/qti/snapdragon/digitalpen/DirectoryObserver$FileChangeListener;->onEvent(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public registerObserver(Ljava/lang/String;ILcom/qti/snapdragon/digitalpen/DirectoryObserver$FileChangeListener;)V
    .locals 2
    .param p1, "string"    # Ljava/lang/String;
    .param p2, "mask"    # I
    .param p3, "listener"    # Lcom/qti/snapdragon/digitalpen/DirectoryObserver$FileChangeListener;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/qti/snapdragon/digitalpen/DirectoryObserver;->listenerMap:Ljava/util/HashMap;

    new-instance v1, Lcom/qti/snapdragon/digitalpen/DirectoryObserver$ListenerEntry;

    invoke-direct {v1, p0, p3, p2}, Lcom/qti/snapdragon/digitalpen/DirectoryObserver$ListenerEntry;-><init>(Lcom/qti/snapdragon/digitalpen/DirectoryObserver;Lcom/qti/snapdragon/digitalpen/DirectoryObserver$FileChangeListener;I)V

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    return-void
.end method
