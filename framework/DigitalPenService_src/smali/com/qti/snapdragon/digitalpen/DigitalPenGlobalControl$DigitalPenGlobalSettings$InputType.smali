.class public final enum Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;
.super Ljava/lang/Enum;
.source "DigitalPenGlobalControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InputType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

.field public static final enum MOUSE:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

.field public static final enum STYLUS:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;


# instance fields
.field private final code:B


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 110
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    const-string v1, "STYLUS"

    invoke-direct {v0, v1, v4, v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->STYLUS:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    .line 111
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    const-string v1, "MOUSE"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v3, v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->MOUSE:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    .line 109
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    sget-object v1, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->STYLUS:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->MOUSE:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->$VALUES:[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IB)V
    .locals 0
    .param p3, "code"    # B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B)V"
        }
    .end annotation

    .prologue
    .line 115
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 116
    iput-byte p3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->code:B

    .line 117
    return-void
.end method

.method static synthetic access$700(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;)B
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    .prologue
    .line 109
    iget-byte v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->code:B

    return v0
.end method

.method static synthetic access$800(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;
    .locals 1
    .param p0, "x0"    # B

    .prologue
    .line 109
    invoke-static {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->fromCode(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    move-result-object v0

    return-object v0
.end method

.method private static fromCode(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;
    .locals 5
    .param p0, "code"    # B

    .prologue
    .line 120
    invoke-static {}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->values()[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    move-result-object v0

    .local v0, "arr$":[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 121
    .local v3, "mode":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;
    iget-byte v4, v3, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->code:B

    if-ne v4, p0, :cond_0

    .line 125
    .end local v3    # "mode":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;
    :goto_1
    return-object v3

    .line 120
    .restart local v3    # "mode":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 125
    .end local v3    # "mode":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 109
    const-class v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    return-object v0
.end method

.method public static values()[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->$VALUES:[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    invoke-virtual {v0}, [Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$InputType;

    return-object v0
.end method
