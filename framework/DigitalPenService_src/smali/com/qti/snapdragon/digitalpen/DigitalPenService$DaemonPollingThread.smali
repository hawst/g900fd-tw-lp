.class Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;
.super Ljava/lang/Thread;
.source "DigitalPenService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DaemonPollingThread"
.end annotation


# instance fields
.field private final mServiceHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;


# direct methods
.method constructor <init>(Lcom/qti/snapdragon/digitalpen/DigitalPenService;Landroid/os/Handler;)V
    .locals 0
    .param p2, "serviceHandler"    # Landroid/os/Handler;

    .prologue
    .line 599
    iput-object p1, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 600
    iput-object p2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;->mServiceHandler:Landroid/os/Handler;

    .line 601
    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 606
    const-wide/16 v2, 0x5dc

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 611
    :goto_0
    const-string v2, "init.svc.usf_epos"

    const-string v3, "stopped"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "stopped"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 613
    const-string v2, "DigitalPenService"

    const-string v3, "Digital pen daemon has stopped"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;->this$0:Lcom/qti/snapdragon/digitalpen/DigitalPenService;

    new-array v3, v5, [I

    aput v4, v3, v4

    invoke-virtual {v2, v5, v3}, Lcom/qti/snapdragon/digitalpen/DigitalPenService;->sendEvent(I[I)V

    .line 622
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 623
    .local v1, "msg":Landroid/os/Message;
    iput v4, v1, Landroid/os/Message;->arg1:I

    .line 624
    iget-object v2, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenService$DaemonPollingThread;->mServiceHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 634
    .end local v1    # "msg":Landroid/os/Message;
    :goto_1
    return-void

    .line 607
    :catch_0
    move-exception v0

    .line 608
    .local v0, "e":Ljava/lang/InterruptedException;
    goto :goto_1

    .line 629
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    const-wide/16 v2, 0x1f4

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 630
    :catch_1
    move-exception v0

    .line 631
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    goto :goto_1
.end method
