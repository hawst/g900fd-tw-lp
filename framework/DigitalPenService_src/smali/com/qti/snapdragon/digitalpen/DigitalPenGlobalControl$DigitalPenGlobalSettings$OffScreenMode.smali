.class public final enum Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;
.super Ljava/lang/Enum;
.source "DigitalPenGlobalControl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OffScreenMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

.field public static final enum DISABLED:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

.field public static final enum DUPLICATE:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;


# instance fields
.field private final code:B


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 28
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v2, v2}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->DISABLED:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    .line 29
    new-instance v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    const-string v1, "DUPLICATE"

    invoke-direct {v0, v1, v3, v4}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;-><init>(Ljava/lang/String;IB)V

    sput-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->DUPLICATE:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    .line 27
    new-array v0, v4, [Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    sget-object v1, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->DISABLED:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->DUPLICATE:Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->$VALUES:[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IB)V
    .locals 0
    .param p3, "code"    # B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput-byte p3, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->code:B

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;)B
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    .prologue
    .line 27
    iget-byte v0, p0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->code:B

    return v0
.end method

.method static synthetic access$100(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;
    .locals 1
    .param p0, "x0"    # B

    .prologue
    .line 27
    invoke-static {p0}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->fromCode(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    move-result-object v0

    return-object v0
.end method

.method private static fromCode(B)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;
    .locals 5
    .param p0, "code"    # B

    .prologue
    .line 38
    invoke-static {}, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->values()[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    move-result-object v0

    .local v0, "arr$":[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 39
    .local v3, "mode":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;
    iget-byte v4, v3, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->code:B

    if-ne v4, p0, :cond_0

    .line 43
    .end local v3    # "mode":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;
    :goto_1
    return-object v3

    .line 38
    .restart local v3    # "mode":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 43
    .end local v3    # "mode":Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 27
    const-class v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    return-object v0
.end method

.method public static values()[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->$VALUES:[Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    invoke-virtual {v0}, [Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qti/snapdragon/digitalpen/DigitalPenGlobalControl$DigitalPenGlobalSettings$OffScreenMode;

    return-object v0
.end method
