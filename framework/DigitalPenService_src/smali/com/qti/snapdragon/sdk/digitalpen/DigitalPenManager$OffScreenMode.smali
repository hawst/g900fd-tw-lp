.class public final enum Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;
.super Ljava/lang/Enum;
.source "DigitalPenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "OffScreenMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

.field public static final enum DISABLED:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

.field public static final enum DUPLICATE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

.field public static final enum EXTEND:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 764
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    const-string v1, "EXTEND"

    invoke-direct {v0, v1, v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->EXTEND:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    .line 774
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    const-string v1, "DUPLICATE"

    invoke-direct {v0, v1, v3}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->DUPLICATE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    .line 780
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v4}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->DISABLED:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    .line 752
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->EXTEND:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->DUPLICATE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->DISABLED:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->$VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 752
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 752
    const-class v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    return-object v0
.end method

.method public static values()[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;
    .locals 1

    .prologue
    .line 752
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->$VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    invoke-virtual {v0}, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    return-object v0
.end method
