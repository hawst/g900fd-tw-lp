.class public Lcom/qti/snapdragon/sdk/digitalpen/PenEnabledChecker;
.super Ljava/lang/Object;
.source "PenEnabledChecker.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkEnabledAndLaunchSettings(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->DIGITAL_PEN_ENABLED:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    invoke-static {v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isFeatureSupported(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 32
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 33
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const-string v2, "Snapdragon Digital Pen Not Enabled"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 34
    const-string v2, "This application requires the digital pen to be enabled.\n\nPress OK to launch Snapdragon Digital Pen Settings."

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 36
    move-object v1, p0

    .line 37
    .local v1, "launchingContext":Landroid/content/Context;
    const-string v2, "OK"

    new-instance v3, Lcom/qti/snapdragon/sdk/digitalpen/PenEnabledChecker$1;

    invoke-direct {v3, v1}, Lcom/qti/snapdragon/sdk/digitalpen/PenEnabledChecker$1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 45
    const-string v2, "Cancel"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 46
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 48
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v1    # "launchingContext":Landroid/content/Context;
    :cond_0
    return-void
.end method
