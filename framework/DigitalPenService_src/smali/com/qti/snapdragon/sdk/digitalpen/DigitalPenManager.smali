.class public Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;
.super Ljava/lang/Object;
.source "DigitalPenManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;,
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;,
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;,
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;,
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;,
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;,
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;,
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;,
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelData;,
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;,
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;,
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;,
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;,
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;,
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;,
        Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException;
    }
.end annotation


# static fields
.field public static final ACTION_DIGITAL_PEN_SETTINGS:Ljava/lang/String; = "com.qti.snapdragon.sdk.digitalpen.SETTINGS"

.field private static final DEFAULT_APP_CONFIG:Landroid/os/Bundle;

.field public static final DEFAULT_MAX_HOVER_DISTANCE:I = 0x190

.field private static final DEFAULT_SIDE_CHANNEL_MAPPING:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

.field private static final DIGITAL_PEN_OFF_SCREEN_DISPLAY_NAME:Ljava/lang/String; = "Digital Pen off-screen display"

.field private static final DIGITAL_PEN_SYSTEM_SERVICE_NAME:Ljava/lang/String; = "DigitalPen"

.field private static final NULL_BACKGROUND_SIDE_CHANNEL_CANCELED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;

.field private static final NULL_BATTERY_STATE_CHANGED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;

.field private static final NULL_DATA_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

.field private static NULL_MIC_BLOCKED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener; = null

.field private static NULL_POWER_STATE_CHANGED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener; = null

.field private static final TAG:Ljava/lang/String; = "DigitalPenManager"

.field private static final VERSION:Ljava/lang/String; = "135.0"

.field private static capabilitySet:Z

.field private static isSideChannelSupported:Z

.field private static service:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

.field private static serviceVersion:Ljava/lang/String;


# instance fields
.field private activityLifecycleHandler:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;

.field private allAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

.field private appContext:Landroid/content/Context;

.field private final appSettings:Landroid/os/Bundle;

.field private backgroundSideChannelCanceledListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;

.field private batteryStateChangedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;

.field private cachedAppSettings:Landroid/os/Bundle;

.field private cachedBatteryState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;

.field private cachedCurrentPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

.field private cachedLastPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

.field private cachedMicsBlocked:I

.field private isBackgroundListenerRegistered:Z

.field private micBlockedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;

.field private offScreenAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

.field private onScreenAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

.field private penEventReceiver:Landroid/content/BroadcastReceiver;

.field private powerStateChangedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;

.field private registeredApplication:Landroid/app/Application;

.field private released:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x190

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 108
    sput-boolean v3, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isSideChannelSupported:Z

    .line 109
    sput-boolean v3, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->capabilitySet:Z

    .line 110
    const-string v0, "<unknown>"

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->serviceVersion:Ljava/lang/String;

    .line 571
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;->RAW:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_SIDE_CHANNEL_MAPPING:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    .line 846
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$2;

    invoke-direct {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$2;-><init>()V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_DATA_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    .line 854
    const/4 v0, 0x0

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->service:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    .line 891
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_APP_CONFIG:Landroid/os/Bundle;

    .line 892
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_APP_CONFIG:Landroid/os/Bundle;

    const-string v1, "OnScreenHoverEnabled"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 893
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_APP_CONFIG:Landroid/os/Bundle;

    const-string v1, "OffScreenHoverEnabled"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 894
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_APP_CONFIG:Landroid/os/Bundle;

    const-string v1, "OnScreenHoverMaxDistance"

    invoke-virtual {v0, v1, v4}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 896
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_APP_CONFIG:Landroid/os/Bundle;

    const-string v1, "OffScreenHoverMaxDistance"

    invoke-virtual {v0, v1, v4}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 898
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_APP_CONFIG:Landroid/os/Bundle;

    const-string v1, "OffScreenMode"

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;->DISABLED:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 900
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_APP_CONFIG:Landroid/os/Bundle;

    const-string v1, "EraserBypass"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 901
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_APP_CONFIG:Landroid/os/Bundle;

    const-string v1, "InputType"

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;->STYLUS:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 902
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_APP_CONFIG:Landroid/os/Bundle;

    const-string v1, "MouseSensitivity"

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;->MEDIUM:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 949
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$4;

    invoke-direct {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$4;-><init>()V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_MIC_BLOCKED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;

    .line 957
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$5;

    invoke-direct {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$5;-><init>()V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_POWER_STATE_CHANGED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;

    .line 970
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$6;

    invoke-direct {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$6;-><init>()V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_BATTERY_STATE_CHANGED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;

    .line 983
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$7;

    invoke-direct {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$7;-><init>()V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_BACKGROUND_SIDE_CHANNEL_CANCELED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;

    return-void
.end method

.method protected constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1014
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 855
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_DATA_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->allAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    .line 856
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_DATA_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->onScreenAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    .line 857
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_DATA_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->offScreenAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    .line 906
    new-instance v0, Landroid/os/Bundle;

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_APP_CONFIG:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;

    .line 908
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedAppSettings:Landroid/os/Bundle;

    .line 912
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;-><init>(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)V

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->penEventReceiver:Landroid/content/BroadcastReceiver;

    .line 947
    iput v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedMicsBlocked:I

    .line 955
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_MIC_BLOCKED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->micBlockedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;

    .line 964
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_POWER_STATE_CHANGED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->powerStateChangedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;

    .line 966
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->OFF:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedCurrentPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    .line 968
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->OFF:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedLastPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    .line 979
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;->OK:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedBatteryState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;

    .line 981
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_BATTERY_STATE_CHANGED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->batteryStateChangedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;

    .line 991
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_BACKGROUND_SIDE_CHANNEL_CANCELED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->backgroundSideChannelCanceledListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;

    .line 997
    iput-boolean v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->released:Z

    .line 1016
    return-void
.end method

.method public constructor <init>(Landroid/app/Application;)V
    .locals 3
    .param p1, "application"    # Landroid/app/Application;

    .prologue
    const/4 v2, 0x0

    .line 1004
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 855
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_DATA_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->allAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    .line 856
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_DATA_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->onScreenAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    .line 857
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_DATA_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->offScreenAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    .line 906
    new-instance v0, Landroid/os/Bundle;

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_APP_CONFIG:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;

    .line 908
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedAppSettings:Landroid/os/Bundle;

    .line 912
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;-><init>(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)V

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->penEventReceiver:Landroid/content/BroadcastReceiver;

    .line 947
    iput v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedMicsBlocked:I

    .line 955
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_MIC_BLOCKED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->micBlockedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;

    .line 964
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_POWER_STATE_CHANGED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->powerStateChangedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;

    .line 966
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->OFF:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedCurrentPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    .line 968
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->OFF:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedLastPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    .line 979
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;->OK:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedBatteryState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;

    .line 981
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_BATTERY_STATE_CHANGED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->batteryStateChangedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;

    .line 991
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_BACKGROUND_SIDE_CHANNEL_CANCELED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->backgroundSideChannelCanceledListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;

    .line 997
    iput-boolean v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->released:Z

    .line 1005
    if-nez p1, :cond_0

    .line 1006
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Application shouldn\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1008
    :cond_0
    invoke-virtual {p0, p1}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->init(Landroid/app/Application;)V

    .line 1009
    return-void
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 90
    sput-boolean p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->capabilitySet:Z

    return p0
.end method

.method static synthetic access$1000(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->micBlockedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;

    return-object v0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 90
    sput-boolean p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isSideChannelSupported:Z

    return p0
.end method

.method static synthetic access$1100(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedCurrentPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;
    .locals 0
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;
    .param p1, "x1"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedCurrentPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedLastPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;
    .locals 0
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;
    .param p1, "x1"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedLastPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->powerStateChangedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->backgroundSideChannelCanceledListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->applyCachedAppSettings()V

    return-void
.end method

.method static synthetic access$1600(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->attachCallbacksToService()V

    return-void
.end method

.method static synthetic access$1700(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isBackgroundListenerRegistered:Z

    return v0
.end method

.method static synthetic access$1800()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->service:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->releaseAppFromService()Z

    move-result v0

    return v0
.end method

.method static synthetic access$202(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 90
    sput-object p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->serviceVersion:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->applySettings()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$500()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_APP_CONFIG:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$600()Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_SIDE_CHANNEL_MAPPING:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    return-object v0
.end method

.method static synthetic access$700(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedBatteryState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;

    return-object v0
.end method

.method static synthetic access$702(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;
    .locals 0
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;
    .param p1, "x1"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedBatteryState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;

    return-object p1
.end method

.method static synthetic access$800(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;
    .locals 1
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->batteryStateChangedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;

    return-object v0
.end method

.method static synthetic access$902(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;
    .param p1, "x1"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedMicsBlocked:I

    return p1
.end method

.method private applyCachedAppSettings()V
    .locals 3

    .prologue
    .line 1221
    :try_start_0
    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->service:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedAppSettings:Landroid/os/Bundle;

    invoke-interface {v1, v2}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->applyAppSettings(Landroid/os/Bundle;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    .line 1227
    :cond_0
    :goto_0
    return-void

    .line 1223
    :catch_0
    move-exception v0

    .line 1224
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DigitalPenManager"

    const-string v2, "applyCachedAppSettings failed!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1225
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private applySettings()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1206
    :try_start_0
    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->service:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    iget-object v3, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;

    invoke-interface {v2, v3}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->applyAppSettings(Landroid/os/Bundle;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1215
    :goto_0
    return v1

    .line 1209
    :cond_0
    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedAppSettings:Landroid/os/Bundle;

    invoke-virtual {v2}, Landroid/os/Bundle;->clear()V

    .line 1210
    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedAppSettings:Landroid/os/Bundle;

    iget-object v3, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1211
    const/4 v1, 0x1

    goto :goto_0

    .line 1212
    :catch_0
    move-exception v0

    .line 1213
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "DigitalPenManager"

    const-string v3, "applyAppSettings failed!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1214
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private attachCallbacksToService()V
    .locals 3

    .prologue
    .line 1257
    :try_start_0
    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->service:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    new-instance v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$8;

    invoke-direct {v2, p0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$8;-><init>(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)V

    invoke-interface {v1, v2}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->registerDataCallback(Lcom/qti/snapdragon/digitalpen/IDigitalPenDataCallback;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1268
    :goto_0
    return-void

    .line 1265
    :catch_0
    move-exception v0

    .line 1266
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private static bindService()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1240
    const-string v0, "DigitalPen"

    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->bindService(Ljava/lang/String;)Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    move-result-object v0

    return-object v0
.end method

.method private static bindService(Ljava/lang/String;)Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    .locals 3
    .param p0, "serviceName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1245
    invoke-static {p0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    move-result-object v0

    .line 1247
    .local v0, "boundService":Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    if-nez v0, :cond_0

    .line 1248
    new-instance v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException;

    const-string v2, "Could not connect to Digital Pen service"

    invoke-direct {v1, v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1250
    :cond_0
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->queryServiceProperties(Lcom/qti/snapdragon/digitalpen/IDigitalPenService;)V

    .line 1252
    return-object v0
.end method

.method private static checkPenEnabled()Z
    .locals 2

    .prologue
    .line 1065
    :try_start_0
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->bindService()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    move-result-object v1

    invoke-interface {v1}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->isEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 1068
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return v1

    .line 1066
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 1067
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1068
    const/4 v1, 0x0

    goto :goto_0

    .line 1069
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 1070
    .local v0, "e":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static checkSideChannelSupported()Z
    .locals 2

    .prologue
    .line 1076
    :try_start_0
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->bindService()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    :try_end_0
    .catch Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1080
    sget-boolean v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isSideChannelSupported:Z

    return v1

    .line 1077
    :catch_0
    move-exception v0

    .line 1078
    .local v0, "e":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static isBasicServiceSupported()Z
    .locals 1

    .prologue
    .line 1041
    const-string v0, "DigitalPen"

    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isBasicServiceSupported(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static isBasicServiceSupported(Ljava/lang/String;)Z
    .locals 5
    .param p0, "serviceName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1045
    sget-object v3, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    const-string v4, "generic"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1046
    const-string v3, "DigitalPenManager"

    const-string v4, "Detected emulator due to \"generic\" as part of build fingerprint"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1060
    :goto_0
    return v2

    .line 1049
    :cond_0
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    .line 1050
    .local v1, "systemResources":Landroid/content/res/Resources;
    const v3, 0x1120082

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1051
    const-string v3, "DigitalPenManager"

    const-string v4, "System not configured to be digital pen capable"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1055
    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->bindService(Ljava/lang/String;)Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    :try_end_0
    .catch Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1060
    const/4 v2, 0x1

    goto :goto_0

    .line 1056
    :catch_0
    move-exception v0

    .line 1057
    .local v0, "e":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException;
    const-string v3, "DigitalPenManager"

    const-string v4, "Couldn\'t connect to digital pen service"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isFeatureSupported(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;)Z
    .locals 2
    .param p0, "feature"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    .prologue
    .line 1025
    if-nez p0, :cond_0

    .line 1026
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Feature shouldn\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1028
    :cond_0
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$Feature:[I

    invoke-virtual {p0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1036
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1030
    :pswitch_0
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isBasicServiceSupported()Z

    move-result v0

    goto :goto_0

    .line 1032
    :pswitch_1
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->checkPenEnabled()Z

    move-result v0

    goto :goto_0

    .line 1034
    :pswitch_2
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->checkSideChannelSupported()Z

    move-result v0

    goto :goto_0

    .line 1028
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static logVersions()V
    .locals 4

    .prologue
    .line 1447
    :try_start_0
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->bindService()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    :try_end_0
    .catch Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1454
    .local v0, "e":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException;
    :goto_0
    const-string v1, "DigitalPenManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "versions: DigitalPenManager v135.0, Service v"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->serviceVersion:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1458
    return-void

    .line 1448
    .end local v0    # "e":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException;
    :catch_0
    move-exception v0

    .line 1449
    .restart local v0    # "e":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException;
    const-string v1, "DigitalPenManager"

    const-string v2, "Cannot connect to digital pen service"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static queryServiceProperties(Lcom/qti/snapdragon/digitalpen/IDigitalPenService;)V
    .locals 4
    .param p0, "service"    # Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    .prologue
    .line 113
    sget-boolean v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->capabilitySet:Z

    if-nez v2, :cond_1

    .line 114
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$1;

    invoke-direct {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$1;-><init>()V

    .line 132
    .local v0, "cb":Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;
    :try_start_0
    invoke-interface {p0, v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->registerEventCallback(Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 133
    const-string v2, "DigitalPenManager"

    const-string v3, "Error registering event callback"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    :cond_0
    invoke-interface {p0, v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->unregisterEventCallback(Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 136
    const-string v2, "DigitalPenManager"

    const-string v3, "Error unregistering event callback"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    .end local v0    # "cb":Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;
    :cond_1
    :goto_0
    return-void

    .line 138
    .restart local v0    # "cb":Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback;
    :catch_0
    move-exception v1

    .line 139
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private releaseAppFromService()Z
    .locals 2

    .prologue
    .line 1186
    :try_start_0
    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->service:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    invoke-interface {v1}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->releaseActivity()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1187
    const/4 v1, 0x1

    .line 1190
    :goto_0
    return v1

    .line 1188
    :catch_0
    move-exception v0

    .line 1189
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 1190
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getOffScreenDisplay()Landroid/view/Display;
    .locals 10

    .prologue
    .line 1359
    iget-object v8, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appContext:Landroid/content/Context;

    const-string v9, "display"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/display/DisplayManager;

    .line 1361
    .local v2, "displayMgr":Landroid/hardware/display/DisplayManager;
    invoke-virtual {v2}, Landroid/hardware/display/DisplayManager;->getDisplays()[Landroid/view/Display;

    move-result-object v4

    .line 1362
    .local v4, "displays":[Landroid/view/Display;
    const/4 v7, 0x0

    .line 1363
    .local v7, "offscreenDisplay":Landroid/view/Display;
    move-object v0, v4

    .local v0, "arr$":[Landroid/view/Display;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v1, v0, v5

    .line 1364
    .local v1, "display":Landroid/view/Display;
    invoke-virtual {v1}, Landroid/view/Display;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1365
    .local v3, "displayName":Ljava/lang/String;
    const-string v8, "Digital Pen off-screen display"

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1366
    move-object v7, v1

    .line 1371
    .end local v1    # "display":Landroid/view/Display;
    .end local v3    # "displayName":Ljava/lang/String;
    :cond_0
    if-nez v7, :cond_1

    .line 1372
    const-string v8, "DigitalPenManager"

    const-string v9, "No off-screen display found"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1374
    :cond_1
    return-object v7

    .line 1363
    .restart local v1    # "display":Landroid/view/Display;
    .restart local v3    # "displayName":Ljava/lang/String;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method protected getServiceInterface()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException;
        }
    .end annotation

    .prologue
    .line 1230
    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->service:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    if-eqz v1, :cond_0

    .line 1231
    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->service:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    .line 1236
    :goto_0
    return-object v1

    .line 1233
    :cond_0
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->bindService()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    move-result-object v0

    .line 1235
    .local v0, "boundService":Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->service:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    .line 1236
    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->service:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    goto :goto_0
.end method

.method public getSettings()Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;
    .locals 1

    .prologue
    .line 1201
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;

    invoke-direct {v0, p0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;-><init>(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)V

    return-object v0
.end method

.method protected declared-synchronized handleDataCallback(Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;)V
    .locals 5
    .param p1, "data"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;

    .prologue
    .line 860
    monitor-enter p0

    :try_start_0
    new-instance v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelData;

    invoke-direct {v1}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelData;-><init>()V

    .line 861
    .local v1, "returnedData":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelData;
    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->getX()I

    move-result v2

    iput v2, v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelData;->xPos:I

    .line 862
    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->getY()I

    move-result v2

    iput v2, v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelData;->yPos:I

    .line 863
    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->getZ()I

    move-result v2

    iput v2, v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelData;->zPos:I

    .line 864
    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->getXTilt()I

    move-result v2

    iput v2, v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelData;->xTilt:I

    .line 865
    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->getYTilt()I

    move-result v2

    iput v2, v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelData;->yTilt:I

    .line 866
    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->getZTilt()I

    move-result v2

    iput v2, v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelData;->zTilt:I

    .line 867
    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->getPenState()Z

    move-result v2

    iput-boolean v2, v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelData;->isDown:Z

    .line 868
    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->getSideButtonsState()[I

    move-result-object v2

    iput-object v2, v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelData;->sideButtonsState:[I

    .line 869
    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->getPressure()I

    move-result v2

    iput v2, v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelData;->pressure:I

    .line 870
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_DATA_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    .line 871
    .local v0, "listener":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;
    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->getRegion()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 882
    const-string v2, "DigitalPenManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Got data point with unknown region: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenData;->getRegion()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    :goto_0
    invoke-interface {v0, v1}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;->onDigitalPenData(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelData;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 886
    monitor-exit p0

    return-void

    .line 873
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->allAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    .line 874
    goto :goto_0

    .line 876
    :pswitch_1
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->onScreenAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    .line 877
    goto :goto_0

    .line 879
    :pswitch_2
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->offScreenAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 880
    goto :goto_0

    .line 860
    .end local v0    # "listener":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;
    .end local v1    # "returnedData":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelData;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 871
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected init(Landroid/app/Application;)V
    .locals 1
    .param p1, "application"    # Landroid/app/Application;

    .prologue
    .line 1132
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->init(Landroid/app/Application;Ljava/lang/String;)V

    .line 1133
    return-void
.end method

.method protected init(Landroid/app/Application;Ljava/lang/String;)V
    .locals 6
    .param p1, "application"    # Landroid/app/Application;
    .param p2, "serviceName"    # Ljava/lang/String;

    .prologue
    .line 1139
    if-nez p2, :cond_0

    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isBasicServiceSupported()Z

    move-result v1

    .line 1141
    .local v1, "isSupported":Z
    :goto_0
    if-nez v1, :cond_1

    .line 1142
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Basic digital pen services not supported"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1139
    .end local v1    # "isSupported":Z
    :cond_0
    invoke-static {p2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isBasicServiceSupported(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    .line 1145
    .restart local v1    # "isSupported":Z
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->getServiceInterface()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    move-result-object v2

    sput-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->service:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    :try_end_0
    .catch Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1153
    new-instance v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;-><init>(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$1;)V

    iput-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->activityLifecycleHandler:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;

    .line 1154
    iput-object p1, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->registeredApplication:Landroid/app/Application;

    .line 1155
    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->registeredApplication:Landroid/app/Application;

    iget-object v3, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->activityLifecycleHandler:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;

    invoke-virtual {v2, v3}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 1156
    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->registeredApplication:Landroid/app/Application;

    invoke-virtual {v2}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appContext:Landroid/content/Context;

    .line 1158
    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->registeredApplication:Landroid/app/Application;

    iget-object v3, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->penEventReceiver:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "com.qti.snapdragon.digitalpen.ACTION_EVENT"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/Application;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1161
    invoke-direct {p0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->attachCallbacksToService()V

    .line 1165
    invoke-direct {p0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->applySettings()Z

    .line 1166
    return-void

    .line 1146
    :catch_0
    move-exception v0

    .line 1147
    .local v0, "e":Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PenNotSupportedException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public registerSideChannelEventListener(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;)V
    .locals 2
    .param p1, "area"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;
    .param p2, "listener"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    .prologue
    .line 1280
    if-nez p1, :cond_0

    .line 1281
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Area shouldn\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1284
    :cond_0
    if-nez p2, :cond_1

    .line 1285
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "OnSideChannelDataListener shouldn\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1288
    :cond_1
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$Area:[I

    invoke-virtual {p1}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1306
    :goto_0
    return-void

    .line 1290
    :pswitch_0
    iput-object p2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->allAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    goto :goto_0

    .line 1293
    :pswitch_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isBackgroundListenerRegistered:Z

    .line 1294
    iput-object p2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->offScreenAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    goto :goto_0

    .line 1297
    :pswitch_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isBackgroundListenerRegistered:Z

    .line 1298
    iput-object p2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->offScreenAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    goto :goto_0

    .line 1301
    :pswitch_3
    iput-object p2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->onScreenAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    goto :goto_0

    .line 1288
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public releaseApplication()Z
    .locals 2

    .prologue
    .line 1173
    iget-boolean v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->released:Z

    if-nez v0, :cond_0

    .line 1174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->released:Z

    .line 1175
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->registeredApplication:Landroid/app/Application;

    iget-object v1, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->activityLifecycleHandler:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;

    invoke-virtual {v0, v1}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 1176
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->registeredApplication:Landroid/app/Application;

    iget-object v1, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->penEventReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Application;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1177
    invoke-direct {p0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->releaseAppFromService()Z

    move-result v0

    .line 1180
    :goto_0
    return v0

    .line 1179
    :cond_0
    const-string v0, "DigitalPenManager"

    const-string v1, "Application is already released"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1180
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBackgroundSideChannelCanceledListener(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;

    .prologue
    .line 1433
    if-nez p1, :cond_0

    .line 1434
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_BACKGROUND_SIDE_CHANNEL_CANCELED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->backgroundSideChannelCanceledListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;

    .line 1438
    :goto_0
    return-void

    .line 1436
    :cond_0
    iput-object p1, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->backgroundSideChannelCanceledListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;

    goto :goto_0
.end method

.method public setBatteryStateChangedListener(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;

    .prologue
    .line 1415
    if-nez p1, :cond_0

    .line 1416
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_BATTERY_STATE_CHANGED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->batteryStateChangedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;

    .line 1421
    :goto_0
    return-void

    .line 1418
    :cond_0
    iput-object p1, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->batteryStateChangedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;

    .line 1419
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedBatteryState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;

    invoke-interface {p1, v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;->onBatteryStateChanged(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;)V

    goto :goto_0
.end method

.method public setMicBlockedListener(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;

    .prologue
    .line 1384
    if-nez p1, :cond_0

    .line 1385
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_MIC_BLOCKED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->micBlockedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;

    .line 1390
    :goto_0
    return-void

    .line 1387
    :cond_0
    iput-object p1, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->micBlockedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;

    .line 1388
    iget v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedMicsBlocked:I

    invoke-interface {p1, v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;->onMicBlocked(I)V

    goto :goto_0
.end method

.method public setPowerStateChangedListener(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;

    .prologue
    .line 1399
    if-nez p1, :cond_0

    .line 1400
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_POWER_STATE_CHANGED_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->powerStateChangedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;

    .line 1405
    :goto_0
    return-void

    .line 1402
    :cond_0
    iput-object p1, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->powerStateChangedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;

    .line 1403
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedCurrentPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    iget-object v1, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedLastPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    invoke-interface {p1, v0, v1}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;->onPowerStateChanged(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;)V

    goto :goto_0
.end method

.method public unregisterSideChannelEventListener(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;)V
    .locals 3
    .param p1, "area"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    .prologue
    .line 1318
    if-nez p1, :cond_0

    .line 1319
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Area shouldn\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1322
    :cond_0
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$Area:[I

    invoke-virtual {p1}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1349
    :goto_0
    return-void

    .line 1324
    :pswitch_0
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_DATA_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->allAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    goto :goto_0

    .line 1327
    :pswitch_1
    iget-boolean v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isBackgroundListenerRegistered:Z

    if-nez v0, :cond_1

    .line 1328
    const-string v0, "DigitalPenManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ignoring request to unregister "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->OFF_SCREEN_BACKGROUND:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " because it is not registered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1332
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isBackgroundListenerRegistered:Z

    .line 1333
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_DATA_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->offScreenAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    goto :goto_0

    .line 1336
    :pswitch_2
    iget-boolean v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isBackgroundListenerRegistered:Z

    if-eqz v0, :cond_2

    .line 1337
    const-string v0, "DigitalPenManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Ignoring request to unregister "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->OFF_SCREEN:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " because "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->OFF_SCREEN_BACKGROUND:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is registered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1341
    :cond_2
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_DATA_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->offScreenAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    goto :goto_0

    .line 1344
    :pswitch_3
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->NULL_DATA_LISTENER:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    iput-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->onScreenAreaDataListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OnSideChannelDataListener;

    goto :goto_0

    .line 1322
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
