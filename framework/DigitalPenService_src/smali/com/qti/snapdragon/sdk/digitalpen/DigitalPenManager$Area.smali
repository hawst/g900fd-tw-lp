.class public final enum Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;
.super Ljava/lang/Enum;
.source "DigitalPenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Area"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

.field public static final enum ALL:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

.field public static final enum OFF_SCREEN:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

.field public static final enum OFF_SCREEN_BACKGROUND:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

.field public static final enum ON_SCREEN:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;


# instance fields
.field private final mappingKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 794
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    const-string v1, "OFF_SCREEN"

    const-string v2, "OffScreenMapping"

    invoke-direct {v0, v1, v3, v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->OFF_SCREEN:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    .line 797
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    const-string v1, "ON_SCREEN"

    const-string v2, "OnScreenMapping"

    invoke-direct {v0, v1, v4, v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->ON_SCREEN:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    .line 800
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    const-string v1, "ALL"

    const-string v2, "AllMapping"

    invoke-direct {v0, v1, v5, v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->ALL:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    .line 813
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    const-string v1, "OFF_SCREEN_BACKGROUND"

    const-string v2, "OffScreenMapping"

    invoke-direct {v0, v1, v6, v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->OFF_SCREEN_BACKGROUND:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    .line 792
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->OFF_SCREEN:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->ON_SCREEN:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->ALL:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->OFF_SCREEN_BACKGROUND:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    aput-object v1, v0, v6

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->$VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "mappingKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 817
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 818
    iput-object p3, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->mappingKey:Ljava/lang/String;

    .line 819
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 792
    const-class v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    return-object v0
.end method

.method public static values()[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;
    .locals 1

    .prologue
    .line 792
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->$VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    invoke-virtual {v0}, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    return-object v0
.end method


# virtual methods
.method getMappingKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 822
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->mappingKey:Ljava/lang/String;

    return-object v0
.end method
