.class public Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;
.super Ljava/lang/Object;
.source "DigitalPenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Settings"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;


# direct methods
.method constructor <init>(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    return-void
.end method


# virtual methods
.method public apply()Z
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # invokes: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->applySettings()Z
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$300(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Z

    move-result v0

    return v0
.end method

.method public clear()Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;
    .locals 2

    .prologue
    .line 342
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 343
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_APP_CONFIG:Landroid/os/Bundle;
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$500()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 344
    return-object p0
.end method

.method public getInputType()Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;
    .locals 2

    .prologue
    .line 432
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "InputType"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    return-object v0
.end method

.method public getMouseSensitivity()Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;
    .locals 2

    .prologue
    .line 451
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "MouseSensitivity"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    return-object v0
.end method

.method public getOffScreenHoverMaxDistance()I
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OffScreenHoverMaxDistance"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getOffScreenMode()Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OffScreenMode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    return-object v0
.end method

.method public getOnScreenHoverMaxDistance()I
    .locals 3

    .prologue
    .line 240
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OnScreenHoverMaxDistance"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getSideChannelMapping(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;
    .locals 3
    .param p1, "area"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    .prologue
    .line 398
    if-nez p1, :cond_0

    .line 399
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Area shouldn\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 402
    :cond_0
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->SIDE_CHANNEL:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isFeatureSupported(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 403
    const-string v0, "DigitalPenManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->SIDE_CHANNEL:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported; ignoring"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    const/4 v0, 0x0

    .line 410
    :goto_0
    return-object v0

    .line 406
    :cond_1
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->getMappingKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 407
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->getMappingKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    goto :goto_0

    .line 410
    :cond_2
    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->DEFAULT_SIDE_CHANNEL_MAPPING:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$600()Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    move-result-object v0

    goto :goto_0
.end method

.method public isEraserBypassed()Z
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "EraserBypass"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isOffScreenHoverEnabled()Z
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OffScreenHoverEnabled"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isOnScreenHoverEnabled()Z
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OnScreenHoverEnabled"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setEraserBypass()Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;
    .locals 3

    .prologue
    .line 248
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "EraserBypass"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 249
    return-object p0
.end method

.method public setEraserBypassDisabled()Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;
    .locals 3

    .prologue
    .line 258
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "EraserBypass"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 259
    return-object p0
.end method

.method public setInputType(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;
    .locals 2
    .param p1, "input"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    .prologue
    .line 421
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "InputType"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 422
    return-object p0
.end method

.method public setMouseSensitivity(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;
    .locals 2
    .param p1, "ms"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    .prologue
    .line 441
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "MouseSensitivity"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 442
    return-object p0
.end method

.method public setOffScreenHoverDisabled()Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;
    .locals 3

    .prologue
    .line 308
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OffScreenHoverEnabled"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 309
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OffScreenHoverMaxDistance"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 310
    return-object p0
.end method

.method public setOffScreenHoverEnabled()Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;
    .locals 3

    .prologue
    .line 297
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OffScreenHoverEnabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 298
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OffScreenHoverMaxDistance"

    const/16 v2, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 300
    return-object p0
.end method

.method public setOffScreenHoverEnabled(I)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;
    .locals 3
    .param p1, "maxDistance"    # I

    .prologue
    .line 320
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OffScreenHoverEnabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 321
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OffScreenHoverMaxDistance"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 322
    return-object p0
.end method

.method public setOffScreenMode(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;
    .locals 2
    .param p1, "mode"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$OffScreenMode;

    .prologue
    .line 185
    if-nez p1, :cond_0

    .line 186
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "OffScreenMode shouldn\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OffScreenMode"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 189
    return-object p0
.end method

.method public setOnScreenHoverDisabled()Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 221
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OnScreenHoverEnabled"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 222
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OnScreenHoverMaxDistance"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 223
    return-object p0
.end method

.method public setOnScreenHoverEnabled()Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;
    .locals 3

    .prologue
    .line 198
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OnScreenHoverEnabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 199
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OnScreenHoverMaxDistance"

    const/16 v2, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 201
    return-object p0
.end method

.method public setOnScreenHoverEnabled(I)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;
    .locals 3
    .param p1, "maxDistance"    # I

    .prologue
    .line 211
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OnScreenHoverMaxDistance"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 212
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "OnScreenHoverEnabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 213
    return-object p0
.end method

.method public setSideChannelMapping(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;
    .locals 3
    .param p1, "area"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;
    .param p2, "mapping"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    .prologue
    .line 358
    if-nez p1, :cond_0

    .line 359
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Area shouldn\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 362
    :cond_0
    if-nez p2, :cond_1

    .line 363
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "SideChannelMapping shouldn\'t be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 366
    :cond_1
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->SIDE_CHANNEL:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isFeatureSupported(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 367
    const-string v0, "DigitalPenManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->SIDE_CHANNEL:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported; ignoring"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    :goto_0
    return-object p0

    .line 370
    :cond_2
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$SideChannelMapping:[I

    invoke-virtual {p2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 372
    :pswitch_0
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->getMappingKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_0

    .line 375
    :pswitch_1
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->getMappingKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_0

    .line 378
    :pswitch_2
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->ALL:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    if-ne p1, v0, :cond_3

    .line 379
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Only "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;->RAW:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " or "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;->DISABLED:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " allowed for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->ALL:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 382
    :cond_3
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Settings;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->getMappingKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_0

    .line 370
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
