.class public final enum Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;
.super Ljava/lang/Enum;
.source "DigitalPenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InputType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

.field public static final enum MOUSE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

.field public static final enum STYLUS:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 741
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    const-string v1, "STYLUS"

    invoke-direct {v0, v1, v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;->STYLUS:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    .line 748
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    const-string v1, "MOUSE"

    invoke-direct {v0, v1, v3}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;->MOUSE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    .line 737
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;->STYLUS:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;->MOUSE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;->$VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 737
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 737
    const-class v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    return-object v0
.end method

.method public static values()[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;
    .locals 1

    .prologue
    .line 737
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;->$VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    invoke-virtual {v0}, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$InputType;

    return-object v0
.end method
