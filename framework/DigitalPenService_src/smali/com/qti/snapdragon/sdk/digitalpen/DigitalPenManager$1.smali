.class final Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$1;
.super Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback$Stub;
.source "DigitalPenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->queryServiceProperties(Lcom/qti/snapdragon/digitalpen/IDigitalPenService;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenEventCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onDigitalPenPropEvent(Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 117
    # setter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->capabilitySet:Z
    invoke-static {v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$002(Z)Z

    .line 118
    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->getEventType()I

    move-result v3

    const/16 v4, 0x40

    if-ne v3, v4, :cond_0

    .line 119
    const/16 v3, 0x50

    invoke-virtual {p1, v3}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->getParameterValue(I)I

    move-result v3

    if-ne v3, v2, :cond_2

    :goto_0
    # setter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isSideChannelSupported:Z
    invoke-static {v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$102(Z)Z

    .line 122
    :cond_0
    invoke-virtual {p1}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->getEventType()I

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_1

    .line 123
    const/16 v2, 0x46

    invoke-virtual {p1, v2}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->getParameterValue(I)I

    move-result v0

    .line 124
    .local v0, "major":I
    const/16 v2, 0x47

    invoke-virtual {p1, v2}, Lcom/qti/snapdragon/digitalpen/util/DigitalPenEvent;->getParameterValue(I)I

    move-result v1

    .line 125
    .local v1, "minor":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->serviceVersion:Ljava/lang/String;
    invoke-static {v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$202(Ljava/lang/String;)Ljava/lang/String;

    .line 127
    .end local v0    # "major":I
    .end local v1    # "minor":I
    :cond_1
    return-void

    .line 119
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
