.class Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;
.super Ljava/lang/Object;
.source "DigitalPenManager.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ActivityLifecycleHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;


# direct methods
.method private constructor <init>(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)V
    .locals 0

    .prologue
    .line 1083
    iput-object p1, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;
    .param p2, "x1"    # Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$1;

    .prologue
    .line 1083
    invoke-direct {p0, p1}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;-><init>(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 1087
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1127
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    invoke-virtual {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->releaseApplication()Z

    .line 1128
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1101
    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->isBackgroundListenerRegistered:Z
    invoke-static {v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$1700(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1102
    new-instance v0, Landroid/os/Bundle;

    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->appSettings:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 1103
    .local v0, "backgroundSettings":Landroid/os/Bundle;
    const-string v2, "OffScreenBackgroundListener"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1106
    :try_start_0
    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->service:Lcom/qti/snapdragon/digitalpen/IDigitalPenService;
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$1800()Lcom/qti/snapdragon/digitalpen/IDigitalPenService;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/qti/snapdragon/digitalpen/IDigitalPenService;->applyAppSettings(Landroid/os/Bundle;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1115
    .end local v0    # "backgroundSettings":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 1107
    .restart local v0    # "backgroundSettings":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 1108
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 1111
    .end local v0    # "backgroundSettings":Landroid/os/Bundle;
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # invokes: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->releaseAppFromService()Z
    invoke-static {v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$1900(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1112
    const-string v2, "DigitalPenManager"

    const-string v3, "onActivityPaused: Problem releasing the application from service "

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1095
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # invokes: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->applyCachedAppSettings()V
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$1500(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)V

    .line 1096
    iget-object v0, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$ActivityLifecycleHandler;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # invokes: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->attachCallbacksToService()V
    invoke-static {v0}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$1600(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)V

    .line 1097
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 1123
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1091
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 1119
    return-void
.end method
