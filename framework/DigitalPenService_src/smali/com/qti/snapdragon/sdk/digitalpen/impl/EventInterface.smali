.class public Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;
.super Ljava/lang/Object;
.source "EventInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;
    }
.end annotation


# static fields
.field public static EVENT_TYPE_BACKGROUND_SIDE_CHANNEL_CANCELED:I = 0x0

.field public static final INTENT_ACTION:Ljava/lang/String; = "com.qti.snapdragon.digitalpen.ACTION_EVENT"

.field private static final INTENT_EXTRA_KEY_PARAMS:Ljava/lang/String; = "params"

.field public static final INTENT_EXTRA_KEY_TYPE:Ljava/lang/String; = "type"

.field private static final TAG:Ljava/lang/String; = "DigitalPenEventInterface"

.field private static eventMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    const/16 v0, 0x3e8

    sput v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->EVENT_TYPE_BACKGROUND_SIDE_CHANNEL_CANCELED:I

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->eventMap:Ljava/util/HashMap;

    .line 45
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->eventMap:Ljava/util/HashMap;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->POWER_STATE_CHANGED:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->eventMap:Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->MIC_BLOCKED:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->eventMap:Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->BATTERY_STATE:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->eventMap:Ljava/util/HashMap;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->SPUR_STATE:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->eventMap:Ljava/util/HashMap;

    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->BAD_SCENARIO:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->eventMap:Ljava/util/HashMap;

    sget v1, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->EVENT_TYPE_BACKGROUND_SIDE_CHANNEL_CANCELED:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->BACKGROUND_SIDE_CHANNEL_CANCELED:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method public static batteryStateFromParam(I)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;
    .locals 1
    .param p0, "param"    # I

    .prologue
    .line 93
    if-nez p0, :cond_0

    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;->OK:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;->LOW:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;

    goto :goto_0
.end method

.method public static getEventParams(Landroid/content/Intent;)[I
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 64
    const-string v1, "params"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v0

    .line 65
    .local v0, "params":[I
    return-object v0
.end method

.method public static getEventType(Landroid/content/Intent;)Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;
    .locals 4
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 55
    const-string v2, "type"

    const/4 v3, -0x1

    invoke-virtual {p0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 56
    .local v0, "eventCode":I
    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->eventMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    .line 57
    .local v1, "type":Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;
    if-nez v1, :cond_0

    .line 58
    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->UNKNOWN:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    .line 60
    .end local v1    # "type":Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;
    :cond_0
    return-object v1
.end method

.method public static makeEventIntent(I[I)Landroid/content/Intent;
    .locals 2
    .param p0, "eventType"    # I
    .param p1, "params"    # [I

    .prologue
    .line 69
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.qti.snapdragon.digitalpen.ACTION_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 70
    .local v0, "eventIntent":Landroid/content/Intent;
    const-string v1, "type"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 71
    const-string v1, "params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 72
    return-object v0
.end method

.method public static powerStateFromParam(I)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;
    .locals 3
    .param p0, "param"    # I

    .prologue
    .line 77
    packed-switch p0, :pswitch_data_0

    .line 87
    const-string v0, "DigitalPenEventInterface"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected power state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 79
    :pswitch_0
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->ACTIVE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    goto :goto_0

    .line 81
    :pswitch_1
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->STANDBY:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    goto :goto_0

    .line 83
    :pswitch_2
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->IDLE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    goto :goto_0

    .line 85
    :pswitch_3
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->OFF:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    goto :goto_0

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
