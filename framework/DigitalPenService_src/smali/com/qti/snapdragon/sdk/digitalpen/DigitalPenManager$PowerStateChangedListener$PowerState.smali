.class public final enum Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;
.super Ljava/lang/Enum;
.source "DigitalPenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PowerState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

.field public static final enum ACTIVE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

.field public static final enum IDLE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

.field public static final enum OFF:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

.field public static final enum STANDBY:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 503
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->ACTIVE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    .line 508
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    const-string v1, "STANDBY"

    invoke-direct {v0, v1, v3}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->STANDBY:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    .line 513
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v4}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->IDLE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    .line 517
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v5}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->OFF:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    .line 499
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->ACTIVE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->STANDBY:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->IDLE:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->OFF:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    aput-object v1, v0, v5

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->$VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 499
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 499
    const-class v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    return-object v0
.end method

.method public static values()[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;
    .locals 1

    .prologue
    .line 499
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->$VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    invoke-virtual {v0}, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    return-object v0
.end method
