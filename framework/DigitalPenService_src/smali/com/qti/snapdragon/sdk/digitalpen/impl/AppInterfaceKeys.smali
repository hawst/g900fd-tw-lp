.class public Lcom/qti/snapdragon/sdk/digitalpen/impl/AppInterfaceKeys;
.super Ljava/lang/Object;
.source "AppInterfaceKeys.java"


# static fields
.field public static final ALL_MAPPING:Ljava/lang/String; = "AllMapping"

.field public static final ERASER_BYPASS:Ljava/lang/String; = "EraserBypass"

.field public static final INPUT_TYPE:Ljava/lang/String; = "InputType"

.field public static final MOUSE_SENSITIVITY:Ljava/lang/String; = "MouseSensitivity"

.field public static final OFF_SCREEN_BACKGROUND_LISTENER:Ljava/lang/String; = "OffScreenBackgroundListener"

.field public static final OFF_SCREEN_HOVER_ENABLED:Ljava/lang/String; = "OffScreenHoverEnabled"

.field public static final OFF_SCREEN_HOVER_MAX_DISTANCE:Ljava/lang/String; = "OffScreenHoverMaxDistance"

.field public static final OFF_SCREEN_MAPPING:Ljava/lang/String; = "OffScreenMapping"

.field public static final OFF_SCREEN_MODE:Ljava/lang/String; = "OffScreenMode"

.field public static final ON_SCREEN_HOVER_ENABLED:Ljava/lang/String; = "OnScreenHoverEnabled"

.field public static final ON_SCREEN_HOVER_MAX_DISTANCE:Ljava/lang/String; = "OnScreenHoverMaxDistance"

.field public static final ON_SCREEN_MAPPING:Ljava/lang/String; = "OnScreenMapping"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
