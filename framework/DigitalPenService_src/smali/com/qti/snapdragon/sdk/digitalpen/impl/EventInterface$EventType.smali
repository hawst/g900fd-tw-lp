.class public final enum Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;
.super Ljava/lang/Enum;
.source "EventInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EventType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

.field public static final enum BACKGROUND_SIDE_CHANNEL_CANCELED:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

.field public static final enum BAD_SCENARIO:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

.field public static final enum BATTERY_STATE:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

.field public static final enum MIC_BLOCKED:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

.field public static final enum POWER_STATE_CHANGED:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

.field public static final enum SPUR_STATE:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

.field public static final enum UNKNOWN:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->UNKNOWN:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    .line 32
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    const-string v1, "POWER_STATE_CHANGED"

    invoke-direct {v0, v1, v4}, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->POWER_STATE_CHANGED:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    .line 33
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    const-string v1, "MIC_BLOCKED"

    invoke-direct {v0, v1, v5}, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->MIC_BLOCKED:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    .line 34
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    const-string v1, "BATTERY_STATE"

    invoke-direct {v0, v1, v6}, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->BATTERY_STATE:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    .line 35
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    const-string v1, "SPUR_STATE"

    invoke-direct {v0, v1, v7}, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->SPUR_STATE:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    .line 36
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    const-string v1, "BAD_SCENARIO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->BAD_SCENARIO:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    .line 37
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    const-string v1, "BACKGROUND_SIDE_CHANNEL_CANCELED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->BACKGROUND_SIDE_CHANNEL_CANCELED:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    .line 30
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->UNKNOWN:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->POWER_STATE_CHANGED:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->MIC_BLOCKED:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->BATTERY_STATE:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->SPUR_STATE:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->BAD_SCENARIO:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->BACKGROUND_SIDE_CHANNEL_CANCELED:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->$VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    return-object v0
.end method

.method public static values()[Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->$VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    invoke-virtual {v0}, [Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    return-object v0
.end method
