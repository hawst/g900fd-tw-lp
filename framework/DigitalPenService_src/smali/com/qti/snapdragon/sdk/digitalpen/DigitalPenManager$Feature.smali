.class public final enum Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;
.super Ljava/lang/Enum;
.source "DigitalPenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Feature"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

.field public static final enum BASIC_DIGITAL_PEN_SERVICES:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

.field public static final enum DIGITAL_PEN_ENABLED:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

.field public static final enum SIDE_CHANNEL:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 580
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    const-string v1, "BASIC_DIGITAL_PEN_SERVICES"

    invoke-direct {v0, v1, v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->BASIC_DIGITAL_PEN_SERVICES:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    .line 589
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    const-string v1, "DIGITAL_PEN_ENABLED"

    invoke-direct {v0, v1, v3}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->DIGITAL_PEN_ENABLED:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    .line 601
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    const-string v1, "SIDE_CHANNEL"

    invoke-direct {v0, v1, v4}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->SIDE_CHANNEL:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    .line 577
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->BASIC_DIGITAL_PEN_SERVICES:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->DIGITAL_PEN_ENABLED:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->SIDE_CHANNEL:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    aput-object v1, v0, v4

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->$VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 577
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 577
    const-class v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    return-object v0
.end method

.method public static values()[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;
    .locals 1

    .prologue
    .line 577
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->$VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    invoke-virtual {v0}, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    return-object v0
.end method
