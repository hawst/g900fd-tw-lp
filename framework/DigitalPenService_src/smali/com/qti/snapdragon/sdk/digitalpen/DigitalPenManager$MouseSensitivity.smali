.class public final enum Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;
.super Ljava/lang/Enum;
.source "DigitalPenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MouseSensitivity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

.field public static final enum FAST:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

.field public static final enum MEDIUM:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

.field public static final enum SLOW:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 727
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    const-string v1, "SLOW"

    invoke-direct {v0, v1, v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;->SLOW:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    .line 728
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;->MEDIUM:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    .line 729
    new-instance v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    const-string v1, "FAST"

    invoke-direct {v0, v1, v4}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;->FAST:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    .line 726
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;->SLOW:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;->MEDIUM:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;->FAST:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    aput-object v1, v0, v4

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;->$VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 726
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 726
    const-class v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    return-object v0
.end method

.method public static values()[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;
    .locals 1

    .prologue
    .line 726
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;->$VALUES:[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    invoke-virtual {v0}, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MouseSensitivity;

    return-object v0
.end method
