.class synthetic Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;
.super Ljava/lang/Object;
.source "DigitalPenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$Area:[I

.field static final synthetic $SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$Feature:[I

.field static final synthetic $SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$SideChannelMapping:[I

.field static final synthetic $SwitchMap$com$qti$snapdragon$sdk$digitalpen$impl$EventInterface$EventType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1288
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->values()[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$Area:[I

    :try_start_0
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$Area:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->ALL:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_e

    :goto_0
    :try_start_1
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$Area:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->OFF_SCREEN_BACKGROUND:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_d

    :goto_1
    :try_start_2
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$Area:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->OFF_SCREEN:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_c

    :goto_2
    :try_start_3
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$Area:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->ON_SCREEN:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_b

    .line 1028
    :goto_3
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->values()[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$Feature:[I

    :try_start_4
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$Feature:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->BASIC_DIGITAL_PEN_SERVICES:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_a

    :goto_4
    :try_start_5
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$Feature:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->DIGITAL_PEN_ENABLED:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_9

    :goto_5
    :try_start_6
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$Feature:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;->SIDE_CHANNEL:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Feature;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_8

    .line 918
    :goto_6
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->values()[Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$impl$EventInterface$EventType:[I

    :try_start_7
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$impl$EventInterface$EventType:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->BATTERY_STATE:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :goto_7
    :try_start_8
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$impl$EventInterface$EventType:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->MIC_BLOCKED:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_6

    :goto_8
    :try_start_9
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$impl$EventInterface$EventType:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->POWER_STATE_CHANGED:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_5

    :goto_9
    :try_start_a
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$impl$EventInterface$EventType:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->BACKGROUND_SIDE_CHANNEL_CANCELED:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_4

    :goto_a
    :try_start_b
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$impl$EventInterface$EventType:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->UNKNOWN:Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_3

    .line 370
    :goto_b
    invoke-static {}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;->values()[Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$SideChannelMapping:[I

    :try_start_c
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$SideChannelMapping:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;->DISABLED:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_2

    :goto_c
    :try_start_d
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$SideChannelMapping:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;->RAW:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1

    :goto_d
    :try_start_e
    sget-object v0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$DigitalPenManager$SideChannelMapping:[I

    sget-object v1, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;->SCALED:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$SideChannelMapping;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_0

    :goto_e
    return-void

    :catch_0
    move-exception v0

    goto :goto_e

    :catch_1
    move-exception v0

    goto :goto_d

    :catch_2
    move-exception v0

    goto :goto_c

    .line 918
    :catch_3
    move-exception v0

    goto :goto_b

    :catch_4
    move-exception v0

    goto :goto_a

    :catch_5
    move-exception v0

    goto :goto_9

    :catch_6
    move-exception v0

    goto :goto_8

    :catch_7
    move-exception v0

    goto :goto_7

    .line 1028
    :catch_8
    move-exception v0

    goto :goto_6

    :catch_9
    move-exception v0

    goto/16 :goto_5

    :catch_a
    move-exception v0

    goto/16 :goto_4

    .line 1288
    :catch_b
    move-exception v0

    goto/16 :goto_3

    :catch_c
    move-exception v0

    goto/16 :goto_2

    :catch_d
    move-exception v0

    goto/16 :goto_1

    :catch_e
    move-exception v0

    goto/16 :goto_0
.end method
