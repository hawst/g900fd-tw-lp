.class Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;
.super Landroid/content/BroadcastReceiver;
.source "DigitalPenManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;


# direct methods
.method constructor <init>(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)V
    .locals 0

    .prologue
    .line 912
    iput-object p1, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 916
    invoke-static {p2}, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->getEventType(Landroid/content/Intent;)Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;

    move-result-object v1

    .line 917
    .local v1, "type":Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;
    invoke-static {p2}, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->getEventParams(Landroid/content/Intent;)[I

    move-result-object v0

    .line 918
    .local v0, "params":[I
    sget-object v2, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$9;->$SwitchMap$com$qti$snapdragon$sdk$digitalpen$impl$EventInterface$EventType:[I

    invoke-virtual {v1}, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface$EventType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 944
    :goto_0
    :pswitch_0
    return-void

    .line 920
    :pswitch_1
    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    aget v3, v0, v4

    invoke-static {v3}, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->batteryStateFromParam(I)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;

    move-result-object v3

    # setter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedBatteryState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;
    invoke-static {v2, v3}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$702(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;

    .line 921
    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->batteryStateChangedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;
    invoke-static {v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$800(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;

    move-result-object v2

    iget-object v3, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedBatteryState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;
    invoke-static {v3}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$700(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener;->onBatteryStateChanged(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BatteryStateChangedListener$BatteryState;)V

    goto :goto_0

    .line 924
    :pswitch_2
    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    aget v3, v0, v4

    # setter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedMicsBlocked:I
    invoke-static {v2, v3}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$902(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;I)I

    .line 925
    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->micBlockedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;
    invoke-static {v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$1000(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;

    move-result-object v2

    aget v3, v0, v4

    invoke-interface {v2, v3}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$MicBlockedListener;->onMicBlocked(I)V

    goto :goto_0

    .line 928
    :pswitch_3
    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    aget v3, v0, v4

    invoke-static {v3}, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->powerStateFromParam(I)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    move-result-object v3

    # setter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedCurrentPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;
    invoke-static {v2, v3}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$1102(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    .line 929
    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    const/4 v3, 0x1

    aget v3, v0, v3

    invoke-static {v3}, Lcom/qti/snapdragon/sdk/digitalpen/impl/EventInterface;->powerStateFromParam(I)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    move-result-object v3

    # setter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedLastPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;
    invoke-static {v2, v3}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$1202(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    .line 930
    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->powerStateChangedListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;
    invoke-static {v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$1300(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;

    move-result-object v2

    iget-object v3, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedCurrentPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;
    invoke-static {v3}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$1100(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    move-result-object v3

    iget-object v4, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->cachedLastPowerState:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;
    invoke-static {v4}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$1200(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener;->onPowerStateChanged(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$PowerStateChangedListener$PowerState;)V

    goto :goto_0

    .line 936
    :pswitch_4
    iget-object v2, p0, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$3;->this$0:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;

    # getter for: Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->backgroundSideChannelCanceledListener:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;
    invoke-static {v2}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;->access$1400(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager;)Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;

    move-result-object v2

    sget-object v3, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;->OFF_SCREEN_BACKGROUND:Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;

    invoke-interface {v2, v3}, Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$BackgroundSideChannelCanceledListener;->onBackgroundSideChannelCanceled(Lcom/qti/snapdragon/sdk/digitalpen/DigitalPenManager$Area;)V

    goto :goto_0

    .line 918
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method
