.class public abstract Lcom/qualcomm/qti/presence/IQPresListener$Stub;
.super Landroid/os/Binder;
.source "IQPresListener.java"

# interfaces
.implements Lcom/qualcomm/qti/presence/IQPresListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/presence/IQPresListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/presence/IQPresListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.presence.IQPresListener"

.field static final TRANSACTION_IQPresListener_CMDStatus:I = 0x5

.field static final TRANSACTION_IQPresListener_CapInfoReceived:I = 0x7

.field static final TRANSACTION_IQPresListener_GetVersion:I = 0x1

.field static final TRANSACTION_IQPresListener_ListCapInfoReceived:I = 0x8

.field static final TRANSACTION_IQPresListener_PublishTriggering:I = 0x4

.field static final TRANSACTION_IQPresListener_ServiceAvailable:I = 0x2

.field static final TRANSACTION_IQPresListener_ServiceUnAvailable:I = 0x3

.field static final TRANSACTION_IQPresListener_SipResponseReceived:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.presence.IQPresListener"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/presence/IQPresListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/presence/IQPresListener;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.presence.IQPresListener"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/presence/IQPresListener;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/presence/IQPresListener;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/presence/IQPresListener$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/presence/IQPresListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 7
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 201
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    .line 44
    :sswitch_0
    const-string v5, "com.qualcomm.qti.presence.IQPresListener"

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v5, "com.qualcomm.qti.presence.IQPresListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 53
    .local v0, "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_0

    .line 54
    sget-object v5, Lcom/qualcomm/qti/rcsservice/QRCSString;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/QRCSString;

    .line 60
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_1

    .line 61
    sget-object v5, Lcom/qualcomm/qti/rcsservice/QRCSInt;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/qualcomm/qti/rcsservice/QRCSInt;

    .line 66
    .local v3, "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    :goto_2
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/qualcomm/qti/presence/IQPresListener$Stub;->IQPresListener_GetVersion(JLcom/qualcomm/qti/rcsservice/QRCSString;Lcom/qualcomm/qti/rcsservice/QRCSInt;)V

    .line 67
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 68
    if-eqz v2, :cond_2

    .line 69
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    invoke-virtual {v2, p3, v4}, Lcom/qualcomm/qti/rcsservice/QRCSString;->writeToParcel(Landroid/os/Parcel;I)V

    .line 75
    :goto_3
    if-eqz v3, :cond_3

    .line 76
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 77
    invoke-virtual {v3, p3, v4}, Lcom/qualcomm/qti/rcsservice/QRCSInt;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 57
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    .end local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    goto :goto_1

    .line 64
    :cond_1
    const/4 v3, 0x0

    .restart local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    goto :goto_2

    .line 73
    :cond_2
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3

    .line 80
    :cond_3
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 86
    .end local v0    # "_arg0":J
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    .end local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    :sswitch_2
    const-string v5, "com.qualcomm.qti.presence.IQPresListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 88
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 90
    .restart local v0    # "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_4

    .line 91
    sget-object v5, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 96
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_4
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/presence/IQPresListener$Stub;->IQPresListener_ServiceAvailable(JLcom/qualcomm/qti/rcsservice/StatusCode;)V

    .line 97
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 94
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_4

    .line 102
    .end local v0    # "_arg0":J
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_3
    const-string v5, "com.qualcomm.qti.presence.IQPresListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 104
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 106
    .restart local v0    # "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_5

    .line 107
    sget-object v5, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 112
    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_5
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/presence/IQPresListener$Stub;->IQPresListener_ServiceUnAvailable(JLcom/qualcomm/qti/rcsservice/StatusCode;)V

    .line 113
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 110
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_5
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_5

    .line 118
    .end local v0    # "_arg0":J
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_4
    const-string v5, "com.qualcomm.qti.presence.IQPresListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 122
    .restart local v0    # "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_6

    .line 123
    sget-object v5, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/presence/PresPublishTriggerType;

    .line 128
    .local v2, "_arg1":Lcom/qualcomm/qti/presence/PresPublishTriggerType;
    :goto_6
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/presence/IQPresListener$Stub;->IQPresListener_PublishTriggering(JLcom/qualcomm/qti/presence/PresPublishTriggerType;)V

    .line 129
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 126
    .end local v2    # "_arg1":Lcom/qualcomm/qti/presence/PresPublishTriggerType;
    :cond_6
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/presence/PresPublishTriggerType;
    goto :goto_6

    .line 134
    .end local v0    # "_arg0":J
    .end local v2    # "_arg1":Lcom/qualcomm/qti/presence/PresPublishTriggerType;
    :sswitch_5
    const-string v5, "com.qualcomm.qti.presence.IQPresListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 136
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 138
    .restart local v0    # "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_7

    .line 139
    sget-object v5, Lcom/qualcomm/qti/presence/PresCmdStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/presence/PresCmdStatus;

    .line 144
    .local v2, "_arg1":Lcom/qualcomm/qti/presence/PresCmdStatus;
    :goto_7
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/presence/IQPresListener$Stub;->IQPresListener_CMDStatus(JLcom/qualcomm/qti/presence/PresCmdStatus;)V

    .line 145
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 142
    .end local v2    # "_arg1":Lcom/qualcomm/qti/presence/PresCmdStatus;
    :cond_7
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/presence/PresCmdStatus;
    goto :goto_7

    .line 150
    .end local v0    # "_arg0":J
    .end local v2    # "_arg1":Lcom/qualcomm/qti/presence/PresCmdStatus;
    :sswitch_6
    const-string v5, "com.qualcomm.qti.presence.IQPresListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 152
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 154
    .restart local v0    # "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_8

    .line 155
    sget-object v5, Lcom/qualcomm/qti/presence/PresSipResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/presence/PresSipResponse;

    .line 160
    .local v2, "_arg1":Lcom/qualcomm/qti/presence/PresSipResponse;
    :goto_8
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/presence/IQPresListener$Stub;->IQPresListener_SipResponseReceived(JLcom/qualcomm/qti/presence/PresSipResponse;)V

    .line 161
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 158
    .end local v2    # "_arg1":Lcom/qualcomm/qti/presence/PresSipResponse;
    :cond_8
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/presence/PresSipResponse;
    goto :goto_8

    .line 166
    .end local v0    # "_arg0":J
    .end local v2    # "_arg1":Lcom/qualcomm/qti/presence/PresSipResponse;
    :sswitch_7
    const-string v5, "com.qualcomm.qti.presence.IQPresListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 168
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 170
    .restart local v0    # "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_9

    .line 171
    sget-object v5, Lcom/qualcomm/qti/rcsservice/QRCSString;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/QRCSString;

    .line 177
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    :goto_9
    sget-object v5, Lcom/qualcomm/qti/presence/PresTupleInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/qualcomm/qti/presence/PresTupleInfo;

    .line 178
    .local v3, "_arg2":[Lcom/qualcomm/qti/presence/PresTupleInfo;
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/qualcomm/qti/presence/IQPresListener$Stub;->IQPresListener_CapInfoReceived(JLcom/qualcomm/qti/rcsservice/QRCSString;[Lcom/qualcomm/qti/presence/PresTupleInfo;)V

    .line 179
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 174
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    .end local v3    # "_arg2":[Lcom/qualcomm/qti/presence/PresTupleInfo;
    :cond_9
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    goto :goto_9

    .line 184
    .end local v0    # "_arg0":J
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    :sswitch_8
    const-string v5, "com.qualcomm.qti.presence.IQPresListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 186
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 188
    .restart local v0    # "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_a

    .line 189
    sget-object v5, Lcom/qualcomm/qti/presence/PresRlmiInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/presence/PresRlmiInfo;

    .line 195
    .local v2, "_arg1":Lcom/qualcomm/qti/presence/PresRlmiInfo;
    :goto_a
    sget-object v5, Lcom/qualcomm/qti/presence/PresResInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/qualcomm/qti/presence/PresResInfo;

    .line 196
    .local v3, "_arg2":[Lcom/qualcomm/qti/presence/PresResInfo;
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/qualcomm/qti/presence/IQPresListener$Stub;->IQPresListener_ListCapInfoReceived(JLcom/qualcomm/qti/presence/PresRlmiInfo;[Lcom/qualcomm/qti/presence/PresResInfo;)V

    .line 197
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 192
    .end local v2    # "_arg1":Lcom/qualcomm/qti/presence/PresRlmiInfo;
    .end local v3    # "_arg2":[Lcom/qualcomm/qti/presence/PresResInfo;
    :cond_a
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/presence/PresRlmiInfo;
    goto :goto_a

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
