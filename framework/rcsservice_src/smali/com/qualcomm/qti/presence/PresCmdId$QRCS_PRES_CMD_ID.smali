.class public final enum Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;
.super Ljava/lang/Enum;
.source "PresCmdId.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/presence/PresCmdId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QRCS_PRES_CMD_ID"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

.field public static final enum QRCS_PRES_CMD_GETCONTACTCAP:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

.field public static final enum QRCS_PRES_CMD_GETCONTACTLISTCAP:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

.field public static final enum QRCS_PRES_CMD_MAX32:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

.field public static final enum QRCS_PRES_CMD_PUBLISHMYCAP:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

.field public static final enum QRCS_PRES_CMD_SETNEWFEATURETAG:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

.field public static final enum QRCS_PRES_CMD_UNKNOWN:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    const-string v1, "QRCS_PRES_CMD_PUBLISHMYCAP"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_PUBLISHMYCAP:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    .line 22
    new-instance v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    const-string v1, "QRCS_PRES_CMD_GETCONTACTCAP"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_GETCONTACTCAP:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    .line 24
    new-instance v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    const-string v1, "QRCS_PRES_CMD_GETCONTACTLISTCAP"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_GETCONTACTLISTCAP:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    .line 26
    new-instance v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    const-string v1, "QRCS_PRES_CMD_SETNEWFEATURETAG"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_SETNEWFEATURETAG:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    .line 29
    new-instance v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    const-string v1, "QRCS_PRES_CMD_MAX32"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_MAX32:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    .line 31
    new-instance v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    const-string v1, "QRCS_PRES_CMD_UNKNOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_UNKNOWN:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    .line 18
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    sget-object v1, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_PUBLISHMYCAP:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_GETCONTACTCAP:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_GETCONTACTLISTCAP:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_SETNEWFEATURETAG:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_MAX32:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_UNKNOWN:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->$VALUES:[Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->$VALUES:[Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    return-object v0
.end method
