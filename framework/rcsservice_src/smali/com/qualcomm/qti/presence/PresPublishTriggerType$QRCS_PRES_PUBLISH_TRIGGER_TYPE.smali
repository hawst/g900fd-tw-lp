.class public final enum Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;
.super Ljava/lang/Enum;
.source "PresPublishTriggerType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/presence/PresPublishTriggerType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QRCS_PRES_PUBLISH_TRIGGER_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

.field public static final enum QRCS_PRES_PUBLISH_TRIGGER_ETAG_EXPIRED:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

.field public static final enum QRCS_PRES_PUBLISH_TRIGGER_MAX32:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

.field public static final enum QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_2G:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

.field public static final enum QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_3G:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

.field public static final enum QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_EHRPD:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

.field public static final enum QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_HSPAPLUS:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

.field public static final enum QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_LTE_VOPS_DISABLED:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

.field public static final enum QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_LTE_VOPS_ENABLED:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

.field public static final enum QRCS_PRES_PUBLISH_TRIGGER_UNKNOWN:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 20
    new-instance v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    const-string v1, "QRCS_PRES_PUBLISH_TRIGGER_ETAG_EXPIRED"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_ETAG_EXPIRED:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    .line 22
    new-instance v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    const-string v1, "QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_LTE_VOPS_DISABLED"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_LTE_VOPS_DISABLED:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    .line 24
    new-instance v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    const-string v1, "QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_LTE_VOPS_ENABLED"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_LTE_VOPS_ENABLED:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    .line 26
    new-instance v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    const-string v1, "QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_EHRPD"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_EHRPD:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    .line 28
    new-instance v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    const-string v1, "QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_HSPAPLUS"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_HSPAPLUS:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    .line 30
    new-instance v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    const-string v1, "QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_3G"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_3G:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    .line 32
    new-instance v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    const-string v1, "QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_2G"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_2G:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    .line 35
    new-instance v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    const-string v1, "QRCS_PRES_PUBLISH_TRIGGER_MAX32"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MAX32:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    .line 37
    new-instance v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    const-string v1, "QRCS_PRES_PUBLISH_TRIGGER_UNKNOWN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_UNKNOWN:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    .line 18
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    sget-object v1, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_ETAG_EXPIRED:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_LTE_VOPS_DISABLED:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_LTE_VOPS_ENABLED:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_EHRPD:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_HSPAPLUS:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_3G:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_2G:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MAX32:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_UNKNOWN:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->$VALUES:[Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->$VALUES:[Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    return-object v0
.end method
