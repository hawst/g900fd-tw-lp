.class public interface abstract Lcom/qualcomm/qti/presence/IQPresListener;
.super Ljava/lang/Object;
.source "IQPresListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/presence/IQPresListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract IQPresListener_CMDStatus(JLcom/qualcomm/qti/presence/PresCmdStatus;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQPresListener_CapInfoReceived(JLcom/qualcomm/qti/rcsservice/QRCSString;[Lcom/qualcomm/qti/presence/PresTupleInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQPresListener_GetVersion(JLcom/qualcomm/qti/rcsservice/QRCSString;Lcom/qualcomm/qti/rcsservice/QRCSInt;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQPresListener_ListCapInfoReceived(JLcom/qualcomm/qti/presence/PresRlmiInfo;[Lcom/qualcomm/qti/presence/PresResInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQPresListener_PublishTriggering(JLcom/qualcomm/qti/presence/PresPublishTriggerType;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQPresListener_ServiceAvailable(JLcom/qualcomm/qti/rcsservice/StatusCode;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQPresListener_ServiceUnAvailable(JLcom/qualcomm/qti/rcsservice/StatusCode;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQPresListener_SipResponseReceived(JLcom/qualcomm/qti/presence/PresSipResponse;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
