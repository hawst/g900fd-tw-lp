.class public abstract Lcom/qualcomm/qti/presence/IQPresService$Stub;
.super Landroid/os/Binder;
.source "IQPresService.java"

# interfaces
.implements Lcom/qualcomm/qti/presence/IQPresService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/presence/IQPresService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/presence/IQPresService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.presence.IQPresService"

.field static final TRANSACTION_QPresServic_SetNewFeatureTag:I = 0x8

.field static final TRANSACTION_QPresService_AddListener:I = 0x2

.field static final TRANSACTION_QPresService_GetContactCap:I = 0x6

.field static final TRANSACTION_QPresService_GetContactListCap:I = 0x7

.field static final TRANSACTION_QPresService_GetVersion:I = 0x1

.field static final TRANSACTION_QPresService_PublishMyCap:I = 0x5

.field static final TRANSACTION_QPresService_ReenableService:I = 0x4

.field static final TRANSACTION_QPresService_RemoveListener:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.presence.IQPresService"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/presence/IQPresService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/presence/IQPresService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.presence.IQPresService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/presence/IQPresService;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/presence/IQPresService;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/presence/IQPresService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/presence/IQPresService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 262
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v5

    :goto_0
    return v5

    .line 44
    :sswitch_0
    const-string v6, "com.qualcomm.qti.presence.IQPresService"

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v6, "com.qualcomm.qti.presence.IQPresService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 53
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_0

    .line 54
    sget-object v6, Lcom/qualcomm/qti/rcsservice/QRCSString;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/QRCSString;

    .line 60
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_1

    .line 61
    sget-object v6, Lcom/qualcomm/qti/rcsservice/QRCSInt;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/QRCSInt;

    .line 66
    .local v2, "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    :goto_2
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/presence/IQPresService$Stub;->QPresService_GetVersion(ILcom/qualcomm/qti/rcsservice/QRCSString;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 67
    .local v4, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 68
    if-eqz v4, :cond_2

    .line 69
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 75
    :goto_3
    if-eqz v1, :cond_3

    .line 76
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 77
    invoke-virtual {v1, p3, v5}, Lcom/qualcomm/qti/rcsservice/QRCSString;->writeToParcel(Landroid/os/Parcel;I)V

    .line 82
    :goto_4
    if-eqz v2, :cond_4

    .line 83
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 84
    invoke-virtual {v2, p3, v5}, Lcom/qualcomm/qti/rcsservice/QRCSInt;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 57
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    goto :goto_1

    .line 64
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    goto :goto_2

    .line 73
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_2
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3

    .line 80
    :cond_3
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_4

    .line 87
    :cond_4
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 93
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_2
    const-string v6, "com.qualcomm.qti.presence.IQPresService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 97
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v6

    invoke-static {v6}, Lcom/qualcomm/qti/presence/IQPresListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/presence/IQPresListener;

    move-result-object v1

    .line 99
    .local v1, "_arg1":Lcom/qualcomm/qti/presence/IQPresListener;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_5

    .line 100
    sget-object v6, Lcom/qualcomm/qti/rcsservice/QRCSLong;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/QRCSLong;

    .line 105
    .local v2, "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    :goto_5
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/presence/IQPresService$Stub;->QPresService_AddListener(ILcom/qualcomm/qti/presence/IQPresListener;Lcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 106
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 107
    if-eqz v4, :cond_6

    .line 108
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 114
    :goto_6
    if-eqz v2, :cond_7

    .line 115
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 116
    invoke-virtual {v2, p3, v5}, Lcom/qualcomm/qti/rcsservice/QRCSLong;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 103
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_5
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    goto :goto_5

    .line 112
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_6
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_6

    .line 119
    :cond_7
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 125
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/presence/IQPresListener;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_3
    const-string v6, "com.qualcomm.qti.presence.IQPresService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 127
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 129
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_8

    .line 130
    sget-object v6, Lcom/qualcomm/qti/rcsservice/QRCSLong;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/QRCSLong;

    .line 135
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    :goto_7
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/presence/IQPresService$Stub;->QPresService_RemoveListener(ILcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 136
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 137
    if-eqz v4, :cond_9

    .line 138
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 139
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 133
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_8
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    goto :goto_7

    .line 142
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_9
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 148
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_4
    const-string v6, "com.qualcomm.qti.presence.IQPresService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 151
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/presence/IQPresService$Stub;->QPresService_ReenableService(I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 152
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 153
    if-eqz v4, :cond_a

    .line 154
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 155
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 158
    :cond_a
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 164
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_5
    const-string v6, "com.qualcomm.qti.presence.IQPresService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 166
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 168
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_b

    .line 169
    sget-object v6, Lcom/qualcomm/qti/presence/PresCapInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/presence/PresCapInfo;

    .line 175
    .local v1, "_arg1":Lcom/qualcomm/qti/presence/PresCapInfo;
    :goto_8
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 176
    .local v2, "_arg2":I
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/presence/IQPresService$Stub;->QPresService_PublishMyCap(ILcom/qualcomm/qti/presence/PresCapInfo;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 177
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 178
    if-eqz v4, :cond_c

    .line 179
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 180
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 172
    .end local v1    # "_arg1":Lcom/qualcomm/qti/presence/PresCapInfo;
    .end local v2    # "_arg2":I
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_b
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/presence/PresCapInfo;
    goto :goto_8

    .line 183
    .restart local v2    # "_arg2":I
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 189
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/presence/PresCapInfo;
    .end local v2    # "_arg2":I
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_6
    const-string v6, "com.qualcomm.qti.presence.IQPresService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 191
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 193
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 195
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 196
    .restart local v2    # "_arg2":I
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/presence/IQPresService$Stub;->QPresService_GetContactCap(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 197
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 198
    if-eqz v4, :cond_d

    .line 199
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 200
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 203
    :cond_d
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 209
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v2    # "_arg2":I
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_7
    const-string v6, "com.qualcomm.qti.presence.IQPresService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 211
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 213
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    .line 215
    .local v1, "_arg1":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 216
    .restart local v2    # "_arg2":I
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/presence/IQPresService$Stub;->QPresService_GetContactListCap(I[Ljava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 217
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 218
    if-eqz v4, :cond_e

    .line 219
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 220
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 223
    :cond_e
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 229
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":[Ljava/lang/String;
    .end local v2    # "_arg2":I
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_8
    const-string v6, "com.qualcomm.qti.presence.IQPresService"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 231
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 233
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 235
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_f

    .line 236
    sget-object v6, Lcom/qualcomm/qti/presence/PresServiceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/presence/PresServiceInfo;

    .line 242
    .local v2, "_arg2":Lcom/qualcomm/qti/presence/PresServiceInfo;
    :goto_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 243
    .local v3, "_arg3":I
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/qualcomm/qti/presence/IQPresService$Stub;->QPresServic_SetNewFeatureTag(ILjava/lang/String;Lcom/qualcomm/qti/presence/PresServiceInfo;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 244
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 245
    if-eqz v4, :cond_10

    .line 246
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 247
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 252
    :goto_a
    if-eqz v2, :cond_11

    .line 253
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 254
    invoke-virtual {v2, p3, v5}, Lcom/qualcomm/qti/presence/PresServiceInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 239
    .end local v2    # "_arg2":Lcom/qualcomm/qti/presence/PresServiceInfo;
    .end local v3    # "_arg3":I
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_f
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/qualcomm/qti/presence/PresServiceInfo;
    goto :goto_9

    .line 250
    .restart local v3    # "_arg3":I
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_10
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_a

    .line 257
    :cond_11
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
