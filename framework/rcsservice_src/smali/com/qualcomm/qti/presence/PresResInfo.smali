.class public Lcom/qualcomm/qti/presence/PresResInfo;
.super Ljava/lang/Object;
.source "PresResInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/presence/PresResInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private displayName:Ljava/lang/String;

.field private instanceInfo:Lcom/qualcomm/qti/presence/PresResInstanceInfo;

.field private resUri:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 157
    new-instance v0, Lcom/qualcomm/qti/presence/PresResInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresResInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/presence/PresResInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInfo;->resUri:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInfo;->displayName:Ljava/lang/String;

    .line 123
    new-instance v0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresResInstanceInfo;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInfo;->instanceInfo:Lcom/qualcomm/qti/presence/PresResInstanceInfo;

    .line 124
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInfo;->resUri:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInfo;->displayName:Ljava/lang/String;

    .line 171
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/presence/PresResInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 172
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/presence/PresResInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/presence/PresResInfo$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresResInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getPresResInfoInstance()Lcom/qualcomm/qti/presence/PresResInfo;
    .locals 1

    .prologue
    .line 137
    new-instance v0, Lcom/qualcomm/qti/presence/PresResInfo;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresResInfo;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInfo;->resUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInfo;->displayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInfo;->instanceInfo:Lcom/qualcomm/qti/presence/PresResInstanceInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 155
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    return v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInfo;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getInstanceInfo()Lcom/qualcomm/qti/presence/PresResInstanceInfo;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInfo;->instanceInfo:Lcom/qualcomm/qti/presence/PresResInstanceInfo;

    return-object v0
.end method

.method public getResUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInfo;->resUri:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 175
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInfo;->resUri:Ljava/lang/String;

    .line 176
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInfo;->displayName:Ljava/lang/String;

    .line 177
    const-class v0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInfo;->instanceInfo:Lcom/qualcomm/qti/presence/PresResInstanceInfo;

    .line 179
    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 3
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresResInfo;->displayName:Ljava/lang/String;

    .line 112
    const-string v0, "AIDL2"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "aks2 displayName  =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/qualcomm/qti/presence/PresResInfo;->displayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    return-void
.end method

.method public setInstanceInfo(Lcom/qualcomm/qti/presence/PresResInstanceInfo;)V
    .locals 0
    .param p1, "instanceInfo"    # Lcom/qualcomm/qti/presence/PresResInstanceInfo;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresResInfo;->instanceInfo:Lcom/qualcomm/qti/presence/PresResInstanceInfo;

    .line 55
    return-void
.end method

.method public setResUri(Ljava/lang/String;)V
    .locals 3
    .param p1, "resUri"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresResInfo;->resUri:Ljava/lang/String;

    .line 83
    const-string v0, "AIDL2"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "aks2 resUri  =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/qualcomm/qti/presence/PresResInfo;->resUri:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 146
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresResInfo;->writeToParcel(Landroid/os/Parcel;)V

    .line 148
    return-void
.end method
