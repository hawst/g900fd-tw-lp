.class public Lcom/qualcomm/qti/presence/PresSipResponse;
.super Ljava/lang/Object;
.source "PresSipResponse.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/presence/PresSipResponse;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private cmdId:Lcom/qualcomm/qti/presence/PresCmdId;

.field private reasonPhrase:Ljava/lang/String;

.field private requestID:I

.field private sipResponseCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 181
    new-instance v0, Lcom/qualcomm/qti/presence/PresSipResponse$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresSipResponse$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/presence/PresSipResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->reasonPhrase:Ljava/lang/String;

    .line 147
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->reasonPhrase:Ljava/lang/String;

    .line 195
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/presence/PresSipResponse;->readFromParcel(Landroid/os/Parcel;)V

    .line 196
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/presence/PresSipResponse$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/presence/PresSipResponse$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresSipResponse;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getPresSipResponseInstance()Lcom/qualcomm/qti/presence/PresSipResponse;
    .locals 1

    .prologue
    .line 160
    new-instance v0, Lcom/qualcomm/qti/presence/PresSipResponse;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresSipResponse;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 175
    iget v0, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->requestID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 176
    iget v0, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->sipResponseCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 177
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->reasonPhrase:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->cmdId:Lcom/qualcomm/qti/presence/PresCmdId;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 179
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    return v0
.end method

.method public getCmdId()Lcom/qualcomm/qti/presence/PresCmdId;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->cmdId:Lcom/qualcomm/qti/presence/PresCmdId;

    return-object v0
.end method

.method public getReasonPhrase()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->reasonPhrase:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestID()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->requestID:I

    return v0
.end method

.method public getSipResponseCode()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->sipResponseCode:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 199
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->requestID:I

    .line 200
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->sipResponseCode:I

    .line 201
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->reasonPhrase:Ljava/lang/String;

    .line 202
    const-class v0, Lcom/qualcomm/qti/presence/PresCmdId;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/presence/PresCmdId;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->cmdId:Lcom/qualcomm/qti/presence/PresCmdId;

    .line 203
    return-void
.end method

.method public setCmdId(Lcom/qualcomm/qti/presence/PresCmdId;)V
    .locals 0
    .param p1, "cmdId"    # Lcom/qualcomm/qti/presence/PresCmdId;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->cmdId:Lcom/qualcomm/qti/presence/PresCmdId;

    .line 53
    return-void
.end method

.method public setReasonPhrase(Ljava/lang/String;)V
    .locals 0
    .param p1, "reasonPhrase"    # Ljava/lang/String;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->reasonPhrase:Ljava/lang/String;

    .line 139
    return-void
.end method

.method public setRequestID(I)V
    .locals 0
    .param p1, "requestID"    # I

    .prologue
    .line 81
    iput p1, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->requestID:I

    .line 82
    return-void
.end method

.method public setSipResponseCode(I)V
    .locals 0
    .param p1, "sipResponseCode"    # I

    .prologue
    .line 109
    iput p1, p0, Lcom/qualcomm/qti/presence/PresSipResponse;->sipResponseCode:I

    .line 110
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 169
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresSipResponse;->writeToParcel(Landroid/os/Parcel;)V

    .line 171
    return-void
.end method
