.class public Lcom/qualcomm/qti/presence/PresCapInfo;
.super Ljava/lang/Object;
.source "PresCapInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/presence/PresCapInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private cdInfo:Lcom/qualcomm/qti/rcsservice/CDInfo;

.field private contactURI:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lcom/qualcomm/qti/presence/PresCapInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresCapInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/presence/PresCapInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCapInfo;->contactURI:Ljava/lang/String;

    .line 92
    new-instance v0, Lcom/qualcomm/qti/rcsservice/CDInfo;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/CDInfo;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCapInfo;->cdInfo:Lcom/qualcomm/qti/rcsservice/CDInfo;

    .line 93
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCapInfo;->contactURI:Ljava/lang/String;

    .line 135
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/presence/PresCapInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 136
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/presence/PresCapInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/presence/PresCapInfo$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresCapInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getPresCapInfoInstance()Lcom/qualcomm/qti/presence/PresCapInfo;
    .locals 1

    .prologue
    .line 106
    new-instance v0, Lcom/qualcomm/qti/presence/PresCapInfo;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresCapInfo;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresCapInfo;->contactURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresCapInfo;->cdInfo:Lcom/qualcomm/qti/rcsservice/CDInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 121
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public getCdInfo()Lcom/qualcomm/qti/rcsservice/CDInfo;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresCapInfo;->cdInfo:Lcom/qualcomm/qti/rcsservice/CDInfo;

    return-object v0
.end method

.method public getContactURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresCapInfo;->contactURI:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 139
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCapInfo;->contactURI:Ljava/lang/String;

    .line 140
    const-class v0, Lcom/qualcomm/qti/rcsservice/CDInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/CDInfo;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCapInfo;->cdInfo:Lcom/qualcomm/qti/rcsservice/CDInfo;

    .line 141
    return-void
.end method

.method public setCdInfo(Lcom/qualcomm/qti/rcsservice/CDInfo;)V
    .locals 0
    .param p1, "cdInfo"    # Lcom/qualcomm/qti/rcsservice/CDInfo;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresCapInfo;->cdInfo:Lcom/qualcomm/qti/rcsservice/CDInfo;

    .line 54
    return-void
.end method

.method public setContactURI(Ljava/lang/String;)V
    .locals 0
    .param p1, "contactURI"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresCapInfo;->contactURI:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresCapInfo;->writeToParcel(Landroid/os/Parcel;)V

    .line 115
    return-void
.end method
