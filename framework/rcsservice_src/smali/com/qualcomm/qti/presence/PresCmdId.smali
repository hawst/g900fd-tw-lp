.class public Lcom/qualcomm/qti/presence/PresCmdId;
.super Ljava/lang/Object;
.source "PresCmdId.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/presence/PresCmdId;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private eCmdId:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 131
    new-instance v0, Lcom/qualcomm/qti/presence/PresCmdId$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresCmdId$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/presence/PresCmdId;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/presence/PresCmdId;->readFromParcel(Landroid/os/Parcel;)V

    .line 146
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/presence/PresCmdId$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/presence/PresCmdId$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresCmdId;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getPresCmdIdInstance()Lcom/qualcomm/qti/presence/PresCmdId;
    .locals 1

    .prologue
    .line 113
    new-instance v0, Lcom/qualcomm/qti/presence/PresCmdId;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresCmdId;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdId;->eCmdId:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 129
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdId;->eCmdId:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    invoke-virtual {v0}, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public getCmdId()Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdId;->eCmdId:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 152
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    move-result-object v2

    iput-object v2, p0, Lcom/qualcomm/qti/presence/PresCmdId;->eCmdId:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 159
    :goto_0
    return-void

    .line 153
    :catch_0
    move-exception v1

    .line 154
    .local v1, "x":Ljava/lang/IllegalArgumentException;
    const-string v2, "AIDL"

    const-string v3, "Cmd ID IllegalArgumentException"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    sget-object v2, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_UNKNOWN:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    iput-object v2, p0, Lcom/qualcomm/qti/presence/PresCmdId;->eCmdId:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    goto :goto_0

    .line 156
    .end local v1    # "x":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 157
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "AIDL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cmd ID Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setCmdId(I)V
    .locals 3
    .param p1, "eCmdId"    # I

    .prologue
    .line 68
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "eCmdId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    sparse-switch p1, :sswitch_data_0

    .line 87
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default eCmdId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    sget-object v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_UNKNOWN:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdId;->eCmdId:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    .line 91
    :goto_0
    return-void

    .line 72
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_PUBLISHMYCAP:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdId;->eCmdId:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    goto :goto_0

    .line 75
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_GETCONTACTCAP:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdId;->eCmdId:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    goto :goto_0

    .line 78
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_GETCONTACTLISTCAP:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdId;->eCmdId:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    goto :goto_0

    .line 81
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_SETNEWFEATURETAG:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdId;->eCmdId:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    goto :goto_0

    .line 84
    :sswitch_4
    sget-object v0, Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;->QRCS_PRES_CMD_MAX32:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdId;->eCmdId:Lcom/qualcomm/qti/presence/PresCmdId$QRCS_PRES_CMD_ID;

    goto :goto_0

    .line 69
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x10000000 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresCmdId;->writeToParcel(Landroid/os/Parcel;)V

    .line 124
    return-void
.end method
