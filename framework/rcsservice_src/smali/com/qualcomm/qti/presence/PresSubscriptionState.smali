.class public Lcom/qualcomm/qti/presence/PresSubscriptionState;
.super Ljava/lang/Object;
.source "PresSubscriptionState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/presence/PresSubscriptionState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private ePresSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/qualcomm/qti/presence/PresSubscriptionState$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresSubscriptionState$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/presence/PresSubscriptionState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/presence/PresSubscriptionState;->readFromParcel(Landroid/os/Parcel;)V

    .line 67
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/presence/PresSubscriptionState$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/presence/PresSubscriptionState$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresSubscriptionState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getPresSubscriptionStateInstance()Lcom/qualcomm/qti/presence/PresSubscriptionState;
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/qualcomm/qti/presence/PresSubscriptionState;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresSubscriptionState;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresSubscriptionState;->ePresSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 50
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresSubscriptionState;->ePresSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method

.method public getPresSubscriptionStateIntValue()I
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresSubscriptionState;->ePresSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;->ordinal()I

    move-result v0

    return v0
.end method

.method public getPresSubscriptionStateValue()Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresSubscriptionState;->ePresSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 73
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    move-result-object v2

    iput-object v2, p0, Lcom/qualcomm/qti/presence/PresSubscriptionState;->ePresSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 82
    :goto_0
    return-void

    .line 74
    :catch_0
    move-exception v1

    .line 75
    .local v1, "x":Ljava/lang/IllegalArgumentException;
    const-string v2, "AIDL"

    const-string v3, "PresSubscriptionState IllegalArgumentException"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    sget-object v2, Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;->QRCS_PRES_SUBSCRIPTION_STATE_UNKNOWN:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    iput-object v2, p0, Lcom/qualcomm/qti/presence/PresSubscriptionState;->ePresSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    goto :goto_0

    .line 78
    .end local v1    # "x":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 79
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "AIDL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PresSubscriptionState Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    sget-object v2, Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;->QRCS_PRES_SUBSCRIPTION_STATE_UNKNOWN:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    iput-object v2, p0, Lcom/qualcomm/qti/presence/PresSubscriptionState;->ePresSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    goto :goto_0
.end method

.method public setPresSubscriptionState(I)V
    .locals 1
    .param p1, "ePresSubscriptionState"    # I

    .prologue
    .line 140
    packed-switch p1, :pswitch_data_0

    .line 158
    sget-object v0, Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;->QRCS_PRES_SUBSCRIPTION_STATE_UNKNOWN:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresSubscriptionState;->ePresSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    .line 161
    :goto_0
    return-void

    .line 143
    :pswitch_0
    sget-object v0, Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;->QRCS_PRES_SUBSCRIPTION_STATE_ACTIVE:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresSubscriptionState;->ePresSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    goto :goto_0

    .line 146
    :pswitch_1
    sget-object v0, Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;->QRCS_PRES_SUBSCRIPTION_STATE_PENDING:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresSubscriptionState;->ePresSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    goto :goto_0

    .line 149
    :pswitch_2
    sget-object v0, Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;->QRCS_PRES_SUBSCRIPTION_STATE_TERMINATED:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresSubscriptionState;->ePresSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    goto :goto_0

    .line 152
    :pswitch_3
    sget-object v0, Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;->QRCS_PRES_SUBSCRIPTION_STATE_UNKNOWN:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresSubscriptionState;->ePresSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    goto :goto_0

    .line 155
    :pswitch_4
    sget-object v0, Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;->QRCS_PRES_SUBSCRIPTION__STATE_MAX32:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresSubscriptionState;->ePresSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState$QRCS_PRES_SUBSCRIPTION_STATE;

    goto :goto_0

    .line 140
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresSubscriptionState;->writeToParcel(Landroid/os/Parcel;)V

    .line 45
    return-void
.end method
