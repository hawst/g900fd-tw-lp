.class public Lcom/qualcomm/qti/presence/PresPublishTriggerType;
.super Ljava/lang/Object;
.source "PresPublishTriggerType.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/presence/PresPublishTriggerType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148
    new-instance v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresPublishTriggerType$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 161
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 162
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->readFromParcel(Landroid/os/Parcel;)V

    .line 163
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/presence/PresPublishTriggerType$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/presence/PresPublishTriggerType$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresPublishTriggerType;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getPresPublishTriggerTypeInstance()Lcom/qualcomm/qti/presence/PresPublishTriggerType;
    .locals 1

    .prologue
    .line 130
    new-instance v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresPublishTriggerType;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 145
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 146
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method

.method public getPublishTrigeerType()Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 169
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    move-result-object v2

    iput-object v2, p0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 177
    :goto_0
    return-void

    .line 170
    :catch_0
    move-exception v1

    .line 171
    .local v1, "x":Ljava/lang/IllegalArgumentException;
    const-string v2, "AIDL"

    const-string v3, "PublishTriggerType IllegalArgumentException"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    sget-object v2, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_UNKNOWN:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    iput-object v2, p0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    goto :goto_0

    .line 173
    .end local v1    # "x":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 174
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "AIDL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PublishTriggerType Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    sget-object v2, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_UNKNOWN:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    iput-object v2, p0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    goto :goto_0
.end method

.method public setPublishTrigeerType(I)V
    .locals 3
    .param p1, "ePublishTriggerType"    # I

    .prologue
    .line 75
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Publish Trigeer Type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    sparse-switch p1, :sswitch_data_0

    .line 104
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default ePublishTriggerType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    sget-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_UNKNOWN:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    .line 108
    :goto_0
    return-void

    .line 80
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_ETAG_EXPIRED:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    goto :goto_0

    .line 83
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_LTE_VOPS_DISABLED:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    goto :goto_0

    .line 86
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_LTE_VOPS_ENABLED:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    goto :goto_0

    .line 89
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_EHRPD:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    goto :goto_0

    .line 92
    :sswitch_4
    sget-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_HSPAPLUS:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    goto :goto_0

    .line 95
    :sswitch_5
    sget-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_3G:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    goto :goto_0

    .line 98
    :sswitch_6
    sget-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MOVE_TO_2G:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    goto :goto_0

    .line 101
    :sswitch_7
    sget-object v0, Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;->QRCS_PRES_PUBLISH_TRIGGER_MAX32:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->ePublishTriggerType:Lcom/qualcomm/qti/presence/PresPublishTriggerType$QRCS_PRES_PUBLISH_TRIGGER_TYPE;

    goto :goto_0

    .line 77
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x10000000 -> :sswitch_7
    .end sparse-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 139
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresPublishTriggerType;->writeToParcel(Landroid/os/Parcel;)V

    .line 141
    return-void
.end method
