.class public Lcom/qualcomm/qti/presence/PresTupleInfo;
.super Ljava/lang/Object;
.source "PresTupleInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/presence/PresTupleInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private contactURI:Ljava/lang/String;

.field private featureTag:Ljava/lang/String;

.field private timestamp:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 153
    new-instance v0, Lcom/qualcomm/qti/presence/PresTupleInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresTupleInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/presence/PresTupleInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->featureTag:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->contactURI:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->timestamp:Ljava/lang/String;

    .line 120
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->featureTag:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->contactURI:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->timestamp:Ljava/lang/String;

    .line 167
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/presence/PresTupleInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 168
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/presence/PresTupleInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/presence/PresTupleInfo$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresTupleInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getPresTupleInfoInstance()Lcom/qualcomm/qti/presence/PresTupleInfo;
    .locals 1

    .prologue
    .line 133
    new-instance v0, Lcom/qualcomm/qti/presence/PresTupleInfo;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresTupleInfo;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->featureTag:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->contactURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->timestamp:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 151
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    return v0
.end method

.method public getContactURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->contactURI:Ljava/lang/String;

    return-object v0
.end method

.method public getFeatureTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->featureTag:Ljava/lang/String;

    return-object v0
.end method

.method public getTimestamp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->timestamp:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 171
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->featureTag:Ljava/lang/String;

    .line 172
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->contactURI:Ljava/lang/String;

    .line 173
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->timestamp:Ljava/lang/String;

    .line 174
    return-void
.end method

.method public setContactURI(Ljava/lang/String;)V
    .locals 0
    .param p1, "contactURI"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->contactURI:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public setFeatureTag(Ljava/lang/String;)V
    .locals 0
    .param p1, "featureTag"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->featureTag:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public setTimestamp(Ljava/lang/String;)V
    .locals 0
    .param p1, "timestamp"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresTupleInfo;->timestamp:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresTupleInfo;->writeToParcel(Landroid/os/Parcel;)V

    .line 144
    return-void
.end method
