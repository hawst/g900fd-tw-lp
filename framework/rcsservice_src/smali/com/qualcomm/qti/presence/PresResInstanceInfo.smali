.class public Lcom/qualcomm/qti/presence/PresResInstanceInfo;
.super Ljava/lang/Object;
.source "PresResInstanceInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/presence/PresResInstanceInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private eResInstanceState:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

.field private id:Ljava/lang/String;

.field private presentityUri:Ljava/lang/String;

.field private reason:Ljava/lang/String;

.field private tupleInfoArray:[Lcom/qualcomm/qti/presence/PresTupleInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 263
    new-instance v0, Lcom/qualcomm/qti/presence/PresResInstanceInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresResInstanceInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->id:Ljava/lang/String;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->reason:Ljava/lang/String;

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->presentityUri:Ljava/lang/String;

    .line 220
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->id:Ljava/lang/String;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->reason:Ljava/lang/String;

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->presentityUri:Ljava/lang/String;

    .line 277
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 278
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/presence/PresResInstanceInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/presence/PresResInstanceInfo$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresResInstanceInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getPresResInstanceInfoInstance()Lcom/qualcomm/qti/presence/PresResInstanceInfo;
    .locals 1

    .prologue
    .line 233
    new-instance v0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresResInstanceInfo;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 248
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->reason:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 250
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->eResInstanceState:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 251
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->presentityUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->tupleInfoArray:[Lcom/qualcomm/qti/presence/PresTupleInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->tupleInfoArray:[Lcom/qualcomm/qti/presence/PresTupleInfo;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 254
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->tupleInfoArray:[Lcom/qualcomm/qti/presence/PresTupleInfo;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 255
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->tupleInfoArray:[Lcom/qualcomm/qti/presence/PresTupleInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 261
    :goto_1
    return-void

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->eResInstanceState:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 259
    :cond_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 238
    const/4 v0, 0x0

    return v0
.end method

.method public getPresentityUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->presentityUri:Ljava/lang/String;

    return-object v0
.end method

.method public getReason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->reason:Ljava/lang/String;

    return-object v0
.end method

.method public getResId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getResInstanceState()Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->eResInstanceState:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    return-object v0
.end method

.method public getTupleInfo()[Lcom/qualcomm/qti/presence/PresTupleInfo;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->tupleInfoArray:[Lcom/qualcomm/qti/presence/PresTupleInfo;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 281
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->id:Ljava/lang/String;

    .line 282
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->reason:Ljava/lang/String;

    .line 285
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    move-result-object v3

    iput-object v3, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->eResInstanceState:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 292
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->presentityUri:Ljava/lang/String;

    .line 294
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 295
    .local v1, "length":I
    if-lez v1, :cond_0

    .line 297
    new-array v3, v1, [Lcom/qualcomm/qti/presence/PresTupleInfo;

    iput-object v3, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->tupleInfoArray:[Lcom/qualcomm/qti/presence/PresTupleInfo;

    .line 298
    iget-object v3, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->tupleInfoArray:[Lcom/qualcomm/qti/presence/PresTupleInfo;

    sget-object v4, Lcom/qualcomm/qti/presence/PresTupleInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    .line 300
    :cond_0
    return-void

    .line 286
    .end local v1    # "length":I
    :catch_0
    move-exception v2

    .line 287
    .local v2, "x":Ljava/lang/IllegalArgumentException;
    const-string v3, "AIDL"

    const-string v4, "PRES RES INSTANCE STATE IllegalArgumentException "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    sget-object v3, Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;->QRCS_PRES_RES_INSTANCE_UNKNOWN:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    iput-object v3, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->eResInstanceState:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    goto :goto_0

    .line 289
    .end local v2    # "x":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 290
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "AIDL"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PRES RES INSTANCE STATE Exception "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setPresentityUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "presentityUri"    # Ljava/lang/String;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->presentityUri:Ljava/lang/String;

    .line 179
    return-void
.end method

.method public setReason(Ljava/lang/String;)V
    .locals 0
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->reason:Ljava/lang/String;

    .line 151
    return-void
.end method

.method public setResId(Ljava/lang/String;)V
    .locals 0
    .param p1, "sId"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->id:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public setResInstanceState(I)V
    .locals 3
    .param p1, "eResInstanceState"    # I

    .prologue
    .line 72
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ResInstanceState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    sparse-switch p1, :sswitch_data_0

    .line 91
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default ResInstanceState = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    sget-object v0, Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;->QRCS_PRES_RES_INSTANCE_UNKNOWN:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->eResInstanceState:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    .line 95
    :goto_0
    return-void

    .line 76
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;->QRCS_PRES_RES_INSTANCE_STATE_ACTIVE:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->eResInstanceState:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    goto :goto_0

    .line 79
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;->QRCS_PRES_RES_INSTANCE_STATE_PENDING:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->eResInstanceState:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    goto :goto_0

    .line 82
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;->QRCS_PRES_RES_INSTANCE_STATE_TERMINATED:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->eResInstanceState:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    goto :goto_0

    .line 85
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;->QRCS_PRES_RES_INSTANCE_STATE_UNKNOWN:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->eResInstanceState:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    goto :goto_0

    .line 88
    :sswitch_4
    sget-object v0, Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;->QRCS_PRES_RES_INSTANCE_STATE_MAX32:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->eResInstanceState:Lcom/qualcomm/qti/presence/PresResInstanceInfo$QRCS_PRES_RES_INSTANCE_STATE;

    goto :goto_0

    .line 73
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x10000000 -> :sswitch_4
    .end sparse-switch
.end method

.method public setTupleInfo([Lcom/qualcomm/qti/presence/PresTupleInfo;)V
    .locals 3
    .param p1, "tupleInfo"    # [Lcom/qualcomm/qti/presence/PresTupleInfo;

    .prologue
    .line 206
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "aks2 tupleInfo.length "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    array-length v0, p1

    new-array v0, v0, [Lcom/qualcomm/qti/presence/PresTupleInfo;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->tupleInfoArray:[Lcom/qualcomm/qti/presence/PresTupleInfo;

    .line 208
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->tupleInfoArray:[Lcom/qualcomm/qti/presence/PresTupleInfo;

    .line 209
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 242
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresResInstanceInfo;->writeToParcel(Landroid/os/Parcel;)V

    .line 244
    return-void
.end method
