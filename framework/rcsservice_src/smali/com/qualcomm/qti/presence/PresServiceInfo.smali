.class public Lcom/qualcomm/qti/presence/PresServiceInfo;
.super Ljava/lang/Object;
.source "PresServiceInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/presence/PresServiceInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private eMediaCap:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

.field private serviceDesc:Ljava/lang/String;

.field private serviceID:Ljava/lang/String;

.field private serviceVer:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 217
    new-instance v0, Lcom/qualcomm/qti/presence/PresServiceInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresServiceInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/presence/PresServiceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceID:Ljava/lang/String;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceDesc:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceVer:Ljava/lang/String;

    .line 183
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceID:Ljava/lang/String;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceDesc:Ljava/lang/String;

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceVer:Ljava/lang/String;

    .line 231
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/presence/PresServiceInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 232
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/presence/PresServiceInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/presence/PresServiceInfo$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresServiceInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getPresServiceInfoInstance()Lcom/qualcomm/qti/presence/PresServiceInfo;
    .locals 1

    .prologue
    .line 196
    new-instance v0, Lcom/qualcomm/qti/presence/PresServiceInfo;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresServiceInfo;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 212
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceDesc:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceVer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->eMediaCap:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 215
    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->eMediaCap:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    invoke-virtual {v0}, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    return v0
.end method

.method public getMediaType()Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->eMediaCap:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    return-object v0
.end method

.method public getServiceDesc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceDesc:Ljava/lang/String;

    return-object v0
.end method

.method public getServiceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceID:Ljava/lang/String;

    return-object v0
.end method

.method public getServiceVer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceVer:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 235
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceID:Ljava/lang/String;

    .line 236
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceDesc:Ljava/lang/String;

    .line 237
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceVer:Ljava/lang/String;

    .line 241
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    move-result-object v2

    iput-object v2, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->eMediaCap:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 248
    :goto_0
    return-void

    .line 242
    :catch_0
    move-exception v1

    .line 243
    .local v1, "x":Ljava/lang/IllegalArgumentException;
    const-string v2, "AIDL"

    const-string v3, "PRES MEDIA CAP IllegalArgumentException "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    sget-object v2, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_UNKNOWN:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    iput-object v2, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->eMediaCap:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    goto :goto_0

    .line 245
    .end local v1    # "x":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 246
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "AIDL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PRES MEDIA CAP Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setMediaType(I)V
    .locals 3
    .param p1, "eMediaCap"    # I

    .prologue
    .line 69
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "eMediaCap = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    sparse-switch p1, :sswitch_data_0

    .line 86
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default eMediaCap= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    sget-object v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_UNKNOWN:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->eMediaCap:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    .line 90
    :goto_0
    return-void

    .line 74
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_NONE:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->eMediaCap:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    goto :goto_0

    .line 77
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_FULL_AUDIO_ONLY:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->eMediaCap:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    goto :goto_0

    .line 80
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_FULL_AUDIO_AND_VIDEO:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->eMediaCap:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    goto :goto_0

    .line 83
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_MAX32:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->eMediaCap:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    goto :goto_0

    .line 71
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x10000000 -> :sswitch_3
    .end sparse-switch
.end method

.method public setServiceDesc(Ljava/lang/String;)V
    .locals 0
    .param p1, "serviceDesc"    # Ljava/lang/String;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceDesc:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public setServiceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "serviceID"    # Ljava/lang/String;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceID:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public setServiceVer(Ljava/lang/String;)V
    .locals 0
    .param p1, "serviceVer"    # Ljava/lang/String;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresServiceInfo;->serviceVer:Ljava/lang/String;

    .line 174
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 205
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresServiceInfo;->writeToParcel(Landroid/os/Parcel;)V

    .line 207
    return-void
.end method
