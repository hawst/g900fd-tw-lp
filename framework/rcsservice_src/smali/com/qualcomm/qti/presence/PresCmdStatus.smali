.class public Lcom/qualcomm/qti/presence/PresCmdStatus;
.super Ljava/lang/Object;
.source "PresCmdStatus.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/presence/PresCmdStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private cmdId:Lcom/qualcomm/qti/presence/PresCmdId;

.field private requestID:I

.field private status:Lcom/qualcomm/qti/rcsservice/StatusCode;

.field private userData:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 182
    new-instance v0, Lcom/qualcomm/qti/presence/PresCmdStatus$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresCmdStatus$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/presence/PresCmdStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->status:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 147
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->status:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 148
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->status:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 196
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/presence/PresCmdStatus;->readFromParcel(Landroid/os/Parcel;)V

    .line 197
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/presence/PresCmdStatus$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/presence/PresCmdStatus$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresCmdStatus;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getPresCmdStatusInstance()Lcom/qualcomm/qti/presence/PresCmdStatus;
    .locals 1

    .prologue
    .line 161
    new-instance v0, Lcom/qualcomm/qti/presence/PresCmdStatus;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresCmdStatus;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 176
    iget v0, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->userData:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 177
    iget v0, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->requestID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 178
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->cmdId:Lcom/qualcomm/qti/presence/PresCmdId;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 179
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->status:Lcom/qualcomm/qti/rcsservice/StatusCode;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 180
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    return v0
.end method

.method public getCmdId()Lcom/qualcomm/qti/presence/PresCmdId;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->cmdId:Lcom/qualcomm/qti/presence/PresCmdId;

    return-object v0
.end method

.method public getRequestID()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->requestID:I

    return v0
.end method

.method public getStatus()Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->status:Lcom/qualcomm/qti/rcsservice/StatusCode;

    return-object v0
.end method

.method public getUserData()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->userData:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 200
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->userData:I

    .line 201
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->requestID:I

    .line 202
    const-class v0, Lcom/qualcomm/qti/presence/PresCmdId;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/presence/PresCmdId;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->cmdId:Lcom/qualcomm/qti/presence/PresCmdId;

    .line 203
    const-class v0, Lcom/qualcomm/qti/rcsservice/StatusCode;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/StatusCode;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->status:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 204
    return-void
.end method

.method public setCmdId(Lcom/qualcomm/qti/presence/PresCmdId;)V
    .locals 0
    .param p1, "cmdId"    # Lcom/qualcomm/qti/presence/PresCmdId;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->cmdId:Lcom/qualcomm/qti/presence/PresCmdId;

    .line 54
    return-void
.end method

.method public setRequestID(I)V
    .locals 0
    .param p1, "requestID"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->requestID:I

    .line 137
    return-void
.end method

.method public setStatus(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .locals 0
    .param p1, "status"    # Lcom/qualcomm/qti/rcsservice/StatusCode;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->status:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 109
    return-void
.end method

.method public setUserData(I)V
    .locals 0
    .param p1, "userData"    # I

    .prologue
    .line 81
    iput p1, p0, Lcom/qualcomm/qti/presence/PresCmdStatus;->userData:I

    .line 82
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 170
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresCmdStatus;->writeToParcel(Landroid/os/Parcel;)V

    .line 172
    return-void
.end method
