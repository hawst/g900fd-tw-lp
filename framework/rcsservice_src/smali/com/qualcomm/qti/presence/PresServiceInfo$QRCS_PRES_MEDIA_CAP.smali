.class public final enum Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;
.super Ljava/lang/Enum;
.source "PresServiceInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/presence/PresServiceInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QRCS_PRES_MEDIA_CAP"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

.field public static final enum QRCS_PRES_MEDIA_CAP_FULL_AUDIO_AND_VIDEO:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

.field public static final enum QRCS_PRES_MEDIA_CAP_FULL_AUDIO_ONLY:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

.field public static final enum QRCS_PRES_MEDIA_CAP_MAX32:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

.field public static final enum QRCS_PRES_MEDIA_CAP_NONE:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

.field public static final enum QRCS_PRES_MEDIA_CAP_UNKNOWN:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    const-string v1, "QRCS_PRES_MEDIA_CAP_NONE"

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_NONE:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    .line 22
    new-instance v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    const-string v1, "QRCS_PRES_MEDIA_CAP_FULL_AUDIO_ONLY"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_FULL_AUDIO_ONLY:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    .line 24
    new-instance v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    const-string v1, "QRCS_PRES_MEDIA_CAP_FULL_AUDIO_AND_VIDEO"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_FULL_AUDIO_AND_VIDEO:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    .line 27
    new-instance v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    const-string v1, "QRCS_PRES_MEDIA_CAP_MAX32"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_MAX32:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    .line 29
    new-instance v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    const-string v1, "QRCS_PRES_MEDIA_CAP_UNKNOWN"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_UNKNOWN:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    .line 18
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    sget-object v1, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_NONE:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_FULL_AUDIO_ONLY:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_FULL_AUDIO_AND_VIDEO:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_MAX32:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->QRCS_PRES_MEDIA_CAP_UNKNOWN:Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    aput-object v1, v0, v6

    sput-object v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->$VALUES:[Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->$VALUES:[Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/presence/PresServiceInfo$QRCS_PRES_MEDIA_CAP;

    return-object v0
.end method
