.class public Lcom/qualcomm/qti/presence/PresRlmiInfo;
.super Ljava/lang/Object;
.source "PresRlmiInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/presence/PresRlmiInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private fullState:Z

.field private listName:Ljava/lang/String;

.field private presSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState;

.field private requestID:I

.field private subscriptionExpireTime:I

.field private subscriptionTerminatedReason:Ljava/lang/String;

.field private uri:Ljava/lang/String;

.field private version:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 308
    new-instance v0, Lcom/qualcomm/qti/presence/PresRlmiInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresRlmiInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 270
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->uri:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->listName:Ljava/lang/String;

    .line 270
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->uri:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->listName:Ljava/lang/String;

    .line 322
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/presence/PresRlmiInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 323
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/presence/PresRlmiInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/presence/PresRlmiInfo$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresRlmiInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getPresRlmiInfoInstance()Lcom/qualcomm/qti/presence/PresRlmiInfo;
    .locals 1

    .prologue
    .line 283
    new-instance v0, Lcom/qualcomm/qti/presence/PresRlmiInfo;

    invoke-direct {v0}, Lcom/qualcomm/qti/presence/PresRlmiInfo;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 298
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->uri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 299
    iget v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->version:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 300
    iget-boolean v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->fullState:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 301
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->listName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 302
    iget v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->requestID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 303
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->presSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 304
    iget v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->subscriptionExpireTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 305
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->subscriptionTerminatedReason:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 306
    return-void

    .line 300
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 288
    const/4 v0, 0x0

    return v0
.end method

.method public getListName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->listName:Ljava/lang/String;

    return-object v0
.end method

.method public getPresSubscriptionState()Lcom/qualcomm/qti/presence/PresSubscriptionState;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->presSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState;

    return-object v0
.end method

.method public getRequestID()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->requestID:I

    return v0
.end method

.method public getSubscriptionExpireTime()I
    .locals 1

    .prologue
    .line 218
    iget v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->subscriptionExpireTime:I

    return v0
.end method

.method public getSubscriptionTerminatedReason()Ljava/lang/String;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->subscriptionTerminatedReason:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->uri:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->version:I

    return v0
.end method

.method public isFullState()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->fullState:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 326
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->uri:Ljava/lang/String;

    .line 327
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->version:I

    .line 328
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->fullState:Z

    .line 329
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->listName:Ljava/lang/String;

    .line 330
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->requestID:I

    .line 331
    const-class v0, Lcom/qualcomm/qti/presence/PresSubscriptionState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/presence/PresSubscriptionState;

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->presSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState;

    .line 332
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->subscriptionExpireTime:I

    .line 333
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->subscriptionTerminatedReason:Ljava/lang/String;

    .line 334
    return-void

    .line 328
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setFullState(Z)V
    .locals 0
    .param p1, "fullState"    # Z

    .prologue
    .line 121
    iput-boolean p1, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->fullState:Z

    .line 122
    return-void
.end method

.method public setListName(Ljava/lang/String;)V
    .locals 0
    .param p1, "listName"    # Ljava/lang/String;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->listName:Ljava/lang/String;

    .line 150
    return-void
.end method

.method public setPresSubscriptionState(Lcom/qualcomm/qti/presence/PresSubscriptionState;)V
    .locals 0
    .param p1, "presSubscriptionState"    # Lcom/qualcomm/qti/presence/PresSubscriptionState;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->presSubscriptionState:Lcom/qualcomm/qti/presence/PresSubscriptionState;

    .line 206
    return-void
.end method

.method public setRequestID(I)V
    .locals 0
    .param p1, "requestID"    # I

    .prologue
    .line 177
    iput p1, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->requestID:I

    .line 178
    return-void
.end method

.method public setSubscriptionExpireTime(I)V
    .locals 0
    .param p1, "subscriptionExpireTime"    # I

    .prologue
    .line 233
    iput p1, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->subscriptionExpireTime:I

    .line 234
    return-void
.end method

.method public setSubscriptionTerminatedReason(Ljava/lang/String;)V
    .locals 0
    .param p1, "subscriptionTerminatedReason"    # Ljava/lang/String;

    .prologue
    .line 261
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->subscriptionTerminatedReason:Ljava/lang/String;

    .line 262
    return-void
.end method

.method public setUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->uri:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setVersion(I)V
    .locals 0
    .param p1, "version"    # I

    .prologue
    .line 93
    iput p1, p0, Lcom/qualcomm/qti/presence/PresRlmiInfo;->version:I

    .line 94
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 292
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/presence/PresRlmiInfo;->writeToParcel(Landroid/os/Parcel;)V

    .line 294
    return-void
.end method
