.class public Lcom/qualcomm/qti/rcsservice/DNReq;
.super Ljava/lang/Object;
.source "DNReq.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/DNReq;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field eDNReq:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/qualcomm/qti/rcsservice/DNReq$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/DNReq$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/DNReq;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/DNReq;->readFromParcel(Landroid/os/Parcel;)V

    .line 68
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/DNReq$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/DNReq$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/DNReq;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public getDNReqValue()Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DNReq;->eDNReq:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    return-object v0
.end method

.method public getEnumValueAsInt()I
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DNReq;->eDNReq:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->ordinal()I

    move-result v0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 74
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/DNReq;->eDNReq:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    return-void

    .line 76
    :catch_0
    move-exception v0

    .line 77
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_UNKNOWN:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/DNReq;->eDNReq:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    goto :goto_0
.end method

.method public setDNReqValue(I)V
    .locals 3
    .param p1, "eDNReq"    # I

    .prologue
    .line 111
    packed-switch p1, :pswitch_data_0

    .line 132
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_UNKNOWN:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DNReq;->eDNReq:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    .line 136
    :goto_0
    return-void

    .line 114
    :pswitch_0
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_UNKNOWN:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DNReq;->eDNReq:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    goto :goto_0

    .line 117
    :pswitch_1
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_NONE:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DNReq;->eDNReq:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    goto :goto_0

    .line 120
    :pswitch_2
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_DELIVERY:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DNReq;->eDNReq:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    goto :goto_0

    .line 123
    :pswitch_3
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_DISPLAY:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DNReq;->eDNReq:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    goto :goto_0

    .line 126
    :pswitch_4
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_DELIVERY_AND_DISPLAY:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DNReq;->eDNReq:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    goto :goto_0

    .line 129
    :pswitch_5
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_MAX32:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DNReq;->eDNReq:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    goto :goto_0

    .line 111
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 50
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DNReq;->eDNReq:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 52
    return-void

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DNReq;->eDNReq:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
