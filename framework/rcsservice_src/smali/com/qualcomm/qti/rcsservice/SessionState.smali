.class public Lcom/qualcomm/qti/rcsservice/SessionState;
.super Ljava/lang/Object;
.source "SessionState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/SessionState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private eSessionState:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 118
    new-instance v0, Lcom/qualcomm/qti/rcsservice/SessionState$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/SessionState$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/SessionState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/SessionState;->readFromParcel(Landroid/os/Parcel;)V

    .line 133
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/SessionState$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/SessionState$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/SessionState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionState;->eSessionState:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 116
    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionState;->eSessionState:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public getEnumValueAsInt()I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionState;->eSessionState:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;->ordinal()I

    move-result v0

    return v0
.end method

.method public getSessionStateValue()Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionState;->eSessionState:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 138
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionState;->eSessionState:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :goto_0
    return-void

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;->QRCS_SESSION_UNKNOWN:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionState;->eSessionState:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    goto :goto_0
.end method

.method public setSessionStateValue(I)V
    .locals 3
    .param p1, "inSessionState"    # I

    .prologue
    .line 66
    packed-switch p1, :pswitch_data_0

    .line 88
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/qualcomm/qti/rcsservice/SessionState;->eSessionState:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;->QRCS_SESSION_UNKNOWN:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionState;->eSessionState:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    .line 92
    :goto_0
    return-void

    .line 69
    :pswitch_0
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;->QRCS_SESSION_STATE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionState;->eSessionState:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    goto :goto_0

    .line 73
    :pswitch_1
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;->QRCS_SESSION_STATE_INIT:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionState;->eSessionState:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    goto :goto_0

    .line 77
    :pswitch_2
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;->QRCS_SESSION_STATE_PENDING:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionState;->eSessionState:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    goto :goto_0

    .line 81
    :pswitch_3
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;->QRCS_SESSION_STATE_ESTABLISHED:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionState;->eSessionState:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    goto :goto_0

    .line 85
    :pswitch_4
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;->QRCS_SESSION_STATE_TERMINATED:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionState;->eSessionState:Lcom/qualcomm/qti/rcsservice/SessionState$QRCS_SESSION_STATE;

    goto :goto_0

    .line 66
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/SessionState;->writeToParcel(Landroid/os/Parcel;)V

    .line 111
    return-void
.end method
