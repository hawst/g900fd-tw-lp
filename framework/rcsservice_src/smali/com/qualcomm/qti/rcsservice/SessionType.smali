.class public Lcom/qualcomm/qti/rcsservice/SessionType;
.super Ljava/lang/Object;
.source "SessionType.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/SessionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/qualcomm/qti/rcsservice/SessionType$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/SessionType$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/SessionType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/SessionType;->readFromParcel(Landroid/os/Parcel;)V

    .line 66
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/SessionType$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/SessionType$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/SessionType;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method

.method public getEnumValueAsInt()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;->ordinal()I

    move-result v0

    return v0
.end method

.method public getSessionTypeValue()Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 72
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :goto_0
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 75
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;->QRCS_SESSION_TYPE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    goto :goto_0
.end method

.method public setSessionTypeValue(I)V
    .locals 3
    .param p1, "eSessionType"    # I

    .prologue
    .line 109
    packed-switch p1, :pswitch_data_0

    .line 139
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;->QRCS_SESSION_TYPE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    .line 143
    :goto_0
    return-void

    .line 112
    :pswitch_0
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;->QRCS_SESSION_TYPE_INVALID:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    goto :goto_0

    .line 115
    :pswitch_1
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;->QRCS_SESSION_TYPE_ONETOONE_CHAT:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    goto :goto_0

    .line 118
    :pswitch_2
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;->QRCS_SESSION_TYPE_GROUP_CHAT:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    goto :goto_0

    .line 121
    :pswitch_3
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;->QRCS_SESSION_TYPE_CLOSED_GROUP_CHAT:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    goto :goto_0

    .line 124
    :pswitch_4
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;->QRCS_SESSION_TYPE_REJOIN_GROUP_CHAT:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    goto :goto_0

    .line 127
    :pswitch_5
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;->QRCS_SESSION_TYPE_ONETOONE_FT_SENDONLY:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    goto :goto_0

    .line 130
    :pswitch_6
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;->QRCS_SESSION_TYPE_ONETOONE_FT_RECVONLY:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    goto :goto_0

    .line 133
    :pswitch_7
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;->QRCS_SESSION_TYPE_GROUP_FT_SENDONLY:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    goto :goto_0

    .line 136
    :pswitch_8
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;->QRCS_SESSION_TYPE_GROUP_FT_RECVONLY:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    goto :goto_0

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 48
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 50
    return-void

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionType;->eSessionType:Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/SessionType$QRCS_SESSION_TYPE;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
