.class public Lcom/qualcomm/qti/rcsservice/QRCSString;
.super Ljava/lang/Object;
.source "QRCSString.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/QRCSString;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private sQRCSString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lcom/qualcomm/qti/rcsservice/QRCSString$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/QRCSString$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/QRCSString;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSString;->sQRCSString:Ljava/lang/String;

    .line 66
    const-string v0, "AIDL"

    const-string v1, "QRCSString     ctor 71"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSString;->sQRCSString:Ljava/lang/String;

    .line 115
    const-string v0, "AIDL"

    const-string v1, "QRCSString     QRCSString ctor param   121"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/QRCSString;->readFromParcel(Landroid/os/Parcel;)V

    .line 117
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/QRCSString$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/QRCSString$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/QRCSString;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getQRCSStringInstance()Lcom/qualcomm/qti/rcsservice/QRCSString;
    .locals 2

    .prologue
    .line 78
    const-string v0, "AIDL"

    const-string v1, "QRCSString     getQRCSStringInstance  83"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    new-instance v0, Lcom/qualcomm/qti/rcsservice/QRCSString;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/QRCSString;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 97
    const-string v0, "AIDL"

    const-string v1, "QRCSString     writeToParcel out   102"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSString;->sQRCSString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 99
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 2

    .prologue
    .line 84
    const-string v0, "AIDL"

    const-string v1, "QRCSString     describeContents  89"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const/4 v0, 0x0

    return v0
.end method

.method public getQRCSString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 35
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QRCSString     getQRCSString  sQRCSString =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/qualcomm/qti/rcsservice/QRCSString;->sQRCSString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSString;->sQRCSString:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 121
    const-string v0, "AIDL"

    const-string v1, "QRCSString     readFromParcel   source 127"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSString;->sQRCSString:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public setQRCSString(Ljava/lang/String;)V
    .locals 3
    .param p1, "sQRCSString"    # Ljava/lang/String;

    .prologue
    .line 52
    const-string v0, "AIDL"

    const-string v1, "QRCSString     setQRCSString  61 "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/QRCSString;->sQRCSString:Ljava/lang/String;

    .line 54
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QRCSString     setQRCSString  this  =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "QRCSString     setQRCSString  sQRCSString =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/qualcomm/qti/rcsservice/QRCSString;->sQRCSString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 90
    const-string v0, "AIDL"

    const-string v1, "QRCSString     writeToParcel  95"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/QRCSString;->writeToParcel(Landroid/os/Parcel;)V

    .line 93
    return-void
.end method
