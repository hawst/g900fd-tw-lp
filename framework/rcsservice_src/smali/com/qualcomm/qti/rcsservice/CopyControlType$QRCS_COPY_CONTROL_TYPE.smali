.class public final enum Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;
.super Ljava/lang/Enum;
.source "CopyControlType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsservice/CopyControlType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QRCS_COPY_CONTROL_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

.field public static final enum QRCS_COPY_CONTROL_BCC:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

.field public static final enum QRCS_COPY_CONTROL_CC:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

.field public static final enum QRCS_COPY_CONTROL_MAX32:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

.field public static final enum QRCS_COPY_CONTROL_TO:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

.field public static final enum QRCS_COPY_CONTROL_TYPE_INVALID:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

.field public static final enum QRCS_COPY_CONTROL_UNKNOWN:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    new-instance v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    const-string v1, "QRCS_COPY_CONTROL_TYPE_INVALID"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_TYPE_INVALID:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    .line 26
    new-instance v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    const-string v1, "QRCS_COPY_CONTROL_TO"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_TO:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    .line 28
    new-instance v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    const-string v1, "QRCS_COPY_CONTROL_CC"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_CC:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    .line 30
    new-instance v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    const-string v1, "QRCS_COPY_CONTROL_BCC"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_BCC:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    .line 32
    new-instance v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    const-string v1, "QRCS_COPY_CONTROL_MAX32"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_MAX32:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    .line 33
    new-instance v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    const-string v1, "QRCS_COPY_CONTROL_UNKNOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_UNKNOWN:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    .line 22
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    sget-object v1, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_TYPE_INVALID:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_TO:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_CC:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_BCC:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_MAX32:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_UNKNOWN:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->$VALUES:[Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->$VALUES:[Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    return-object v0
.end method
