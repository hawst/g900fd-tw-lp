.class public Lcom/qualcomm/qti/rcsservice/StatusCode;
.super Ljava/lang/Object;
.source "StatusCode.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/StatusCode;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 186
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/StatusCode$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    const-string v0, "AIDL"

    const-string v1, "StatusCode     ctor   start 69 "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    const-string v0, "AIDL"

    const-string v1, "StatusCode     ctor   start 206"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/StatusCode;->readFromParcel(Landroid/os/Parcel;)V

    .line 204
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/StatusCode$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/StatusCode$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/StatusCode;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getStatusCodeInstance()Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 2

    .prologue
    .line 73
    const-string v0, "AIDL"

    const-string v1, "StatusCode     getStatusCodeInstance   start 81 "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 74
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 182
    const-string v0, "AIDL"

    const-string v1, "StatusCode     writeToParcel  184 "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 184
    return-void

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 2

    .prologue
    .line 169
    const-string v0, "AIDL"

    const-string v1, "StatusCode     describeContents   start  "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    const/4 v0, 0x0

    return v0
.end method

.method public getEnumValueAsInt()I
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->ordinal()I

    move-result v0

    return v0
.end method

.method public getStatusCode()Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;
    .locals 3

    .prologue
    .line 88
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StatusCode     getStatusCode   96   eStatusCode =  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 209
    :try_start_0
    const-string v2, "AIDL"

    const-string v3, "StatusCode     readFromParcel   start 213 "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    move-result-object v2

    iput-object v2, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 217
    :goto_0
    return-void

    .line 211
    :catch_0
    move-exception v1

    .line 212
    .local v1, "x":Ljava/lang/IllegalArgumentException;
    const-string v2, "AIDL"

    const-string v3, "Status code IllegalArgumentException "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    sget-object v2, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_SERVICE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    iput-object v2, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    goto :goto_0

    .line 214
    .end local v1    # "x":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 215
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "AIDL"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Status code Exception "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setStatusCode(I)V
    .locals 3
    .param p1, "nStatusCode"    # I

    .prologue
    .line 104
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StatusCode     ctor   setStatusCode 112 nStatusCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    packed-switch p1, :pswitch_data_0

    .line 159
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default nStatusCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_SERVICE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    .line 163
    :goto_0
    return-void

    .line 108
    :pswitch_0
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_SUCCESS:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    goto :goto_0

    .line 112
    :pswitch_1
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_FAILURE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    goto :goto_0

    .line 116
    :pswitch_2
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_SUCCESS_ASYC_UPDATE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    goto :goto_0

    .line 120
    :pswitch_3
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_INVALID_SERVICE_HANDLE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    goto :goto_0

    .line 124
    :pswitch_4
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_INVALID_LISTENER_HANDLE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    goto :goto_0

    .line 128
    :pswitch_5
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_INVALID_PARAM:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    goto :goto_0

    .line 132
    :pswitch_6
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_FETCH_ERROR:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    goto :goto_0

    .line 136
    :pswitch_7
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_REQUEST_TIMEOUT:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    goto :goto_0

    .line 140
    :pswitch_8
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_INSUFFICIENT_MEMORY:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    goto :goto_0

    .line 144
    :pswitch_9
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_LOST_NET:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    goto :goto_0

    .line 148
    :pswitch_a
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_NOT_SUPPORTED:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    goto :goto_0

    .line 152
    :pswitch_b
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_NOT_FOUND:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    goto :goto_0

    .line 156
    :pswitch_c
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_SERVICE_UNAVAILABLE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/StatusCode;->eStatusCode:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    goto :goto_0

    .line 105
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 175
    const-string v0, "AIDL"

    const-string v1, "StatusCode     ctor   writeToParcel 179"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;)V

    .line 178
    return-void
.end method
