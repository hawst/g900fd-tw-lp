.class public Lcom/qualcomm/qti/rcsservice/Content;
.super Ljava/lang/Object;
.source "Content.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/Content;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private byteArrayBuffer:[B

.field private contentDescription:Ljava/lang/String;

.field private contentType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Content$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/Content$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Content;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->contentDescription:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->contentType:Ljava/lang/String;

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->byteArrayBuffer:[B

    .line 31
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->contentDescription:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->contentType:Ljava/lang/String;

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->byteArrayBuffer:[B

    .line 65
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/Content;->readFromParcel(Landroid/os/Parcel;)V

    .line 66
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/Content$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/Content$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/Content;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method public getByteArrayBuffer()[B
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->byteArrayBuffer:[B

    return-object v0
.end method

.method public getContentDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->contentDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->contentType:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/Content;->contentDescription:Ljava/lang/String;

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/Content;->contentType:Ljava/lang/String;

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 72
    .local v0, "length":I
    if-eqz v0, :cond_0

    .line 74
    new-array v1, v0, [B

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/Content;->byteArrayBuffer:[B

    .line 75
    iget-object v1, p0, Lcom/qualcomm/qti/rcsservice/Content;->byteArrayBuffer:[B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readByteArray([B)V

    .line 77
    :cond_0
    return-void
.end method

.method public setByteArrayBuffer([B)V
    .locals 1
    .param p1, "byteArrayBuffer"    # [B

    .prologue
    .line 141
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    .line 143
    array-length v0, p1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->byteArrayBuffer:[B

    .line 144
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/Content;->byteArrayBuffer:[B

    .line 147
    :cond_0
    return-void
.end method

.method public setContentDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "contentDescription"    # Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/Content;->contentDescription:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public setContentType(Ljava/lang/String;)V
    .locals 0
    .param p1, "contentType"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/Content;->contentType:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 38
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->contentDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->contentType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->byteArrayBuffer:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->byteArrayBuffer:[B

    array-length v0, v0

    if-lez v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->byteArrayBuffer:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 43
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/Content;->byteArrayBuffer:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 50
    :goto_0
    return-void

    .line 47
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
