.class public Lcom/qualcomm/qti/rcsservice/CDInfo;
.super Ljava/lang/Object;
.source "CDInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/CDInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private bCDViaPresenceSupported:Z

.field private bFtSupported:Z

.field private bFullSnFGroupChatSupported:Z

.field private bGeoPullSupported:Z

.field private bGeoPushSupported:Z

.field private bIPVideoSupported:Z

.field private bIPVoiceSupported:Z

.field private bImSupported:Z

.field private bIsSupported:Z

.field private bRcsCapable:Z

.field private bRcsRegistered:Z

.field private bSMSupported:Z

.field private bSpSupported:Z

.field private bVsDuringCSSupported:Z

.field private bVsSupported:Z

.field private dwCapTimestamp:J

.field private noOfExts:I

.field private pExts:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 611
    new-instance v0, Lcom/qualcomm/qti/rcsservice/CDInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/CDInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/CDInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bRcsCapable:Z

    .line 22
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bRcsRegistered:Z

    .line 25
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bImSupported:Z

    .line 26
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bFtSupported:Z

    .line 27
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIsSupported:Z

    .line 28
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bVsDuringCSSupported:Z

    .line 29
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bVsSupported:Z

    .line 30
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bSpSupported:Z

    .line 31
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bCDViaPresenceSupported:Z

    .line 32
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIPVoiceSupported:Z

    .line 33
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIPVideoSupported:Z

    .line 34
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bGeoPullSupported:Z

    .line 35
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bGeoPushSupported:Z

    .line 36
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bSMSupported:Z

    .line 37
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bFullSnFGroupChatSupported:Z

    .line 38
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->pExts:[Ljava/lang/String;

    .line 39
    iput v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->noOfExts:I

    .line 41
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->dwCapTimestamp:J

    .line 48
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CDInfo     57   bRcsCapable = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bRcsCapable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bRcsCapable:Z

    .line 22
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bRcsRegistered:Z

    .line 25
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bImSupported:Z

    .line 26
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bFtSupported:Z

    .line 27
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIsSupported:Z

    .line 28
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bVsDuringCSSupported:Z

    .line 29
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bVsSupported:Z

    .line 30
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bSpSupported:Z

    .line 31
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bCDViaPresenceSupported:Z

    .line 32
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIPVoiceSupported:Z

    .line 33
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIPVideoSupported:Z

    .line 34
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bGeoPullSupported:Z

    .line 35
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bGeoPushSupported:Z

    .line 36
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bSMSupported:Z

    .line 37
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bFullSnFGroupChatSupported:Z

    .line 38
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->pExts:[Ljava/lang/String;

    .line 39
    iput v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->noOfExts:I

    .line 41
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->dwCapTimestamp:J

    .line 626
    const-string v0, "AIDL"

    const-string v1, "CDInfo     CDInfo ctor amit    606 "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/CDInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 628
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/CDInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/CDInfo$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/CDInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getCDInfoInstance()Lcom/qualcomm/qti/rcsservice/CDInfo;
    .locals 2

    .prologue
    .line 62
    const-string v0, "AIDL"

    const-string v1, "CDInfo     getCDInfoInstance  start amit "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    new-instance v0, Lcom/qualcomm/qti/rcsservice/CDInfo;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/CDInfo;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 589
    const-string v0, "AIDL"

    const-string v3, "CDInfo     writeToParcel parcle out  amit   start 570 "

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bRcsCapable:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 591
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bRcsRegistered:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 592
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bImSupported:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 593
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bFtSupported:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 594
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIsSupported:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 595
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bVsDuringCSSupported:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 596
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bVsSupported:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 597
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bSpSupported:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 598
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bCDViaPresenceSupported:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 599
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIPVoiceSupported:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 600
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIPVideoSupported:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 601
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bGeoPullSupported:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 602
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bGeoPushSupported:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 603
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bSMSupported:Z

    if-eqz v0, :cond_d

    move v0, v1

    :goto_d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 604
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bFullSnFGroupChatSupported:Z

    if-eqz v0, :cond_e

    :goto_e
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 605
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->pExts:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 606
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->noOfExts:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 607
    iget-wide v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->dwCapTimestamp:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 608
    const-string v0, "AIDL"

    const-string v1, "CDInfo     writeToParcel parcle out  amit   end 588 "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 609
    return-void

    :cond_0
    move v0, v2

    .line 590
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 591
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 592
    goto :goto_2

    :cond_3
    move v0, v2

    .line 593
    goto :goto_3

    :cond_4
    move v0, v2

    .line 594
    goto :goto_4

    :cond_5
    move v0, v2

    .line 595
    goto :goto_5

    :cond_6
    move v0, v2

    .line 596
    goto :goto_6

    :cond_7
    move v0, v2

    .line 597
    goto :goto_7

    :cond_8
    move v0, v2

    .line 598
    goto :goto_8

    :cond_9
    move v0, v2

    .line 599
    goto :goto_9

    :cond_a
    move v0, v2

    .line 600
    goto :goto_a

    :cond_b
    move v0, v2

    .line 601
    goto :goto_b

    :cond_c
    move v0, v2

    .line 602
    goto :goto_c

    :cond_d
    move v0, v2

    .line 603
    goto :goto_d

    :cond_e
    move v1, v2

    .line 604
    goto :goto_e
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 577
    const/4 v0, 0x0

    return v0
.end method

.method public getCapTimestamp()J
    .locals 2

    .prologue
    .line 554
    iget-wide v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->dwCapTimestamp:J

    return-wide v0
.end method

.method public getExts()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->pExts:[Ljava/lang/String;

    return-object v0
.end method

.method public getNoOfExts()I
    .locals 1

    .prologue
    .line 526
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->noOfExts:I

    return v0
.end method

.method public isCDViaPresenceSupported()Z
    .locals 1

    .prologue
    .line 302
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bCDViaPresenceSupported:Z

    return v0
.end method

.method public isFtSupported()Z
    .locals 1

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bFtSupported:Z

    return v0
.end method

.method public isGeoPullSupported()Z
    .locals 1

    .prologue
    .line 386
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bGeoPullSupported:Z

    return v0
.end method

.method public isGeoPushSupported()Z
    .locals 1

    .prologue
    .line 414
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bGeoPushSupported:Z

    return v0
.end method

.method public isIPVideoSupported()Z
    .locals 1

    .prologue
    .line 358
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIPVideoSupported:Z

    return v0
.end method

.method public isIPVoiceSupported()Z
    .locals 1

    .prologue
    .line 330
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIPVoiceSupported:Z

    return v0
.end method

.method public isImSupported()Z
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bImSupported:Z

    return v0
.end method

.method public isIsSupported()Z
    .locals 1

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIsSupported:Z

    return v0
.end method

.method public isRcsCapable()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bRcsCapable:Z

    return v0
.end method

.method public isRcsRegistered()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bRcsRegistered:Z

    return v0
.end method

.method public isSMSupported()Z
    .locals 1

    .prologue
    .line 442
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bSMSupported:Z

    return v0
.end method

.method public isSpSupported()Z
    .locals 1

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bSpSupported:Z

    return v0
.end method

.method public isVsDuringCSSupported()Z
    .locals 1

    .prologue
    .line 218
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bVsDuringCSSupported:Z

    return v0
.end method

.method public isVsSupported()Z
    .locals 1

    .prologue
    .line 246
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bVsSupported:Z

    return v0
.end method

.method public isbFullSnFGroupChatSupported()Z
    .locals 1

    .prologue
    .line 470
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bFullSnFGroupChatSupported:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 632
    const-string v0, "AIDL"

    const-string v3, "CDInfo     readFromParcel amit    start "

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    if-eqz p1, :cond_f

    .line 635
    const-string v0, "AIDL"

    const-string v3, "CDInfo     readFromParcel inside if "

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bRcsCapable:Z

    .line 637
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bRcsRegistered:Z

    .line 638
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bImSupported:Z

    .line 639
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bFtSupported:Z

    .line 640
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIsSupported:Z

    .line 641
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bVsDuringCSSupported:Z

    .line 642
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bVsSupported:Z

    .line 643
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bSpSupported:Z

    .line 644
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bCDViaPresenceSupported:Z

    .line 645
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIPVoiceSupported:Z

    .line 646
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIPVideoSupported:Z

    .line 647
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bGeoPullSupported:Z

    .line 648
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bGeoPushSupported:Z

    .line 649
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bSMSupported:Z

    .line 650
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_e

    :goto_e
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bFullSnFGroupChatSupported:Z

    .line 652
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->pExts:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 653
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->noOfExts:I

    .line 654
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->dwCapTimestamp:J

    .line 660
    :goto_f
    const-string v0, "AIDL"

    const-string v1, "CDInfo     readFromParcel amit    end "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    return-void

    :cond_0
    move v0, v2

    .line 636
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 637
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 638
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 639
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 640
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 641
    goto :goto_5

    :cond_6
    move v0, v2

    .line 642
    goto :goto_6

    :cond_7
    move v0, v2

    .line 643
    goto :goto_7

    :cond_8
    move v0, v2

    .line 644
    goto :goto_8

    :cond_9
    move v0, v2

    .line 645
    goto :goto_9

    :cond_a
    move v0, v2

    .line 646
    goto :goto_a

    :cond_b
    move v0, v2

    .line 647
    goto :goto_b

    :cond_c
    move v0, v2

    .line 648
    goto :goto_c

    :cond_d
    move v0, v2

    .line 649
    goto :goto_d

    :cond_e
    move v1, v2

    .line 650
    goto :goto_e

    .line 658
    :cond_f
    const-string v0, "AIDL"

    const-string v1, "CDInfo     readFromParcel source = null "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_f
.end method

.method public setCDViaPresenceSupported(Z)V
    .locals 0
    .param p1, "bCDViaPresenceSupported"    # Z

    .prologue
    .line 317
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bCDViaPresenceSupported:Z

    .line 318
    return-void
.end method

.method public setCapTimestamp(J)V
    .locals 1
    .param p1, "dwCapTimestamp"    # J

    .prologue
    .line 569
    iput-wide p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->dwCapTimestamp:J

    .line 570
    return-void
.end method

.method public setExts([Ljava/lang/Object;)V
    .locals 0
    .param p1, "pExts"    # [Ljava/lang/Object;

    .prologue
    .line 513
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "pExts":[Ljava/lang/Object;
    check-cast p1, [Ljava/lang/String;

    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->pExts:[Ljava/lang/String;

    .line 514
    return-void
.end method

.method public setFtSupported(Z)V
    .locals 0
    .param p1, "bFtSupported"    # Z

    .prologue
    .line 177
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bFtSupported:Z

    .line 178
    return-void
.end method

.method public setGeoPullSupported(Z)V
    .locals 0
    .param p1, "bGeoPullSupported"    # Z

    .prologue
    .line 401
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bGeoPullSupported:Z

    .line 402
    return-void
.end method

.method public setGeoPushSupported(Z)V
    .locals 0
    .param p1, "bGeoPushSupported"    # Z

    .prologue
    .line 429
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bGeoPushSupported:Z

    .line 430
    return-void
.end method

.method public setIPVideoSupported(Z)V
    .locals 0
    .param p1, "bIPVideoSupported"    # Z

    .prologue
    .line 373
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIPVideoSupported:Z

    .line 374
    return-void
.end method

.method public setIPVoiceSupported(Z)V
    .locals 0
    .param p1, "bIPVoiceSupported"    # Z

    .prologue
    .line 345
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIPVoiceSupported:Z

    .line 346
    return-void
.end method

.method public setImSupported(Z)V
    .locals 0
    .param p1, "bImSupported"    # Z

    .prologue
    .line 149
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bImSupported:Z

    .line 150
    return-void
.end method

.method public setIsSupported(Z)V
    .locals 0
    .param p1, "bIsSupported"    # Z

    .prologue
    .line 205
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bIsSupported:Z

    .line 206
    return-void
.end method

.method public setNoOfExts(I)V
    .locals 0
    .param p1, "noOfExts"    # I

    .prologue
    .line 541
    iput p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->noOfExts:I

    .line 542
    return-void
.end method

.method public setRcsCapable(Z)V
    .locals 0
    .param p1, "bRcsCapable"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bRcsCapable:Z

    .line 94
    return-void
.end method

.method public setRcsRegistered(Z)V
    .locals 0
    .param p1, "bRcsRegistered"    # Z

    .prologue
    .line 121
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bRcsRegistered:Z

    .line 122
    return-void
.end method

.method public setSMSupported(Z)V
    .locals 0
    .param p1, "bSMSupported"    # Z

    .prologue
    .line 457
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bSMSupported:Z

    .line 458
    return-void
.end method

.method public setSpSupported(Z)V
    .locals 0
    .param p1, "bSpSupported"    # Z

    .prologue
    .line 289
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bSpSupported:Z

    .line 290
    return-void
.end method

.method public setVsDuringCSSupported(Z)V
    .locals 0
    .param p1, "bVsDuringCSSupported"    # Z

    .prologue
    .line 233
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bVsDuringCSSupported:Z

    .line 234
    return-void
.end method

.method public setVsSupported(Z)V
    .locals 0
    .param p1, "bVsSupported"    # Z

    .prologue
    .line 261
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bVsSupported:Z

    .line 262
    return-void
.end method

.method public setbFullSnFGroupChatSupported(Z)V
    .locals 0
    .param p1, "bFullSnFGroupChatSupported"    # Z

    .prologue
    .line 485
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/CDInfo;->bFullSnFGroupChatSupported:Z

    .line 486
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 582
    const-string v0, "AIDL"

    const-string v1, "CDInfo     writeToParcel dest amit   563"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/CDInfo;->writeToParcel(Landroid/os/Parcel;)V

    .line 585
    return-void
.end method
