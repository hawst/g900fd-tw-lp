.class public Lcom/qualcomm/qti/rcsservice/QRCSInt;
.super Ljava/lang/Object;
.source "QRCSInt.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/QRCSInt;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private nClientID:I

.field private nQRCSInt:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lcom/qualcomm/qti/rcsservice/QRCSInt$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/QRCSInt$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/QRCSInt;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/16 v0, 0x3e9

    iput v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSInt;->nClientID:I

    .line 26
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/16 v0, 0x3e9

    iput v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSInt;->nClientID:I

    .line 127
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/QRCSInt;->readFromParcel(Landroid/os/Parcel;)V

    .line 128
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/QRCSInt$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/QRCSInt$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/QRCSInt;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getQRCSIntInstance()Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/qualcomm/qti/rcsservice/QRCSInt;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/QRCSInt;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 111
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSInt;->nQRCSInt:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSInt;->nClientID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 113
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    return v0
.end method

.method public getClientID()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSInt;->nClientID:I

    return v0
.end method

.method public getQRCSInt()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSInt;->nQRCSInt:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 131
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSInt;->nQRCSInt:I

    .line 132
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSInt;->nClientID:I

    .line 133
    return-void
.end method

.method public setClientID(I)V
    .locals 0
    .param p1, "nClientID"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/qualcomm/qti/rcsservice/QRCSInt;->nClientID:I

    .line 84
    return-void
.end method

.method public setQRCSInt(I)V
    .locals 0
    .param p1, "nRCSInt"    # I

    .prologue
    .line 55
    iput p1, p0, Lcom/qualcomm/qti/rcsservice/QRCSInt;->nQRCSInt:I

    .line 56
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/QRCSInt;->writeToParcel(Landroid/os/Parcel;)V

    .line 107
    return-void
.end method
