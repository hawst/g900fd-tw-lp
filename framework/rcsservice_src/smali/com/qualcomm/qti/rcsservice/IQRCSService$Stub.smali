.class public abstract Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;
.super Landroid/os/Binder;
.source "IQRCSService.java"

# interfaces
.implements Lcom/qualcomm/qti/rcsservice/IQRCSService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsservice/IQRCSService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.rcsservice.IQRCSService"

.field static final TRANSACTION_QRCSCreateCDService:I = 0x4

.field static final TRANSACTION_QRCSCreateConfigService:I = 0xc

.field static final TRANSACTION_QRCSCreateFTService:I = 0xa

.field static final TRANSACTION_QRCSCreateIMService:I = 0x6

.field static final TRANSACTION_QRCSCreateISService:I = 0x8

.field static final TRANSACTION_QRCSCreatePresService:I = 0xf

.field static final TRANSACTION_QRCSCreateSMService:I = 0x11

.field static final TRANSACTION_QRCSDestroyCDService:I = 0x5

.field static final TRANSACTION_QRCSDestroyConfigService:I = 0xd

.field static final TRANSACTION_QRCSDestroyFTService:I = 0xb

.field static final TRANSACTION_QRCSDestroyIMService:I = 0x7

.field static final TRANSACTION_QRCSDestroyISService:I = 0x9

.field static final TRANSACTION_QRCSDestroyPresService:I = 0x10

.field static final TRANSACTION_QRCSDestroySMService:I = 0x12

.field static final TRANSACTION_QRCSIsRCSServiceStarted:I = 0x3

.field static final TRANSACTION_QRCSStartRCSService:I = 0x1

.field static final TRANSACTION_QRCSStopRCSService:I = 0x2

.field static final TRANSACTION_getCDService:I = 0x15

.field static final TRANSACTION_getConfigService:I = 0x14

.field static final TRANSACTION_getConfigServiceHandle:I = 0x19

.field static final TRANSACTION_getFTService:I = 0x17

.field static final TRANSACTION_getIMService:I = 0x16

.field static final TRANSACTION_getISService:I = 0x18

.field static final TRANSACTION_getPresenceService:I = 0x13

.field static final TRANSACTION_getSMService:I = 0x1a

.field static final TRANSACTION_getServiceStatus:I = 0xe


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/rcsservice/IQRCSService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/rcsservice/IQRCSService;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/rcsservice/IQRCSService;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 345
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    .line 44
    :sswitch_0
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v5, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/qualcomm/qti/rcsservice/IQRCSListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/rcsservice/IQRCSListener;

    move-result-object v0

    .line 52
    .local v0, "_arg0":Lcom/qualcomm/qti/rcsservice/IQRCSListener;
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSStartRCSService(Lcom/qualcomm/qti/rcsservice/IQRCSListener;)Z

    move-result v2

    .line 53
    .local v2, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 54
    if-eqz v2, :cond_0

    move v3, v4

    :cond_0
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 59
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsservice/IQRCSListener;
    .end local v2    # "_result":Z
    :sswitch_2
    const-string v5, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSStopRCSService()Z

    move-result v2

    .line 61
    .restart local v2    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 62
    if-eqz v2, :cond_1

    move v3, v4

    :cond_1
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 67
    .end local v2    # "_result":Z
    :sswitch_3
    const-string v5, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSIsRCSServiceStarted()Z

    move-result v2

    .line 69
    .restart local v2    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 70
    if-eqz v2, :cond_2

    move v3, v4

    :cond_2
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 75
    .end local v2    # "_result":Z
    :sswitch_4
    const-string v5, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/qualcomm/qti/cd/IQCDServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/cd/IQCDServiceListener;

    move-result-object v0

    .line 78
    .local v0, "_arg0":Lcom/qualcomm/qti/cd/IQCDServiceListener;
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSCreateCDService(Lcom/qualcomm/qti/cd/IQCDServiceListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v2

    .line 79
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 80
    if-eqz v2, :cond_3

    .line 81
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 82
    invoke-virtual {v2, p3, v4}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 85
    :cond_3
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 91
    .end local v0    # "_arg0":Lcom/qualcomm/qti/cd/IQCDServiceListener;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_5
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 94
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSDestroyCDService(I)V

    .line 95
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 100
    .end local v0    # "_arg0":I
    :sswitch_6
    const-string v5, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 102
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/qualcomm/qti/im/IQIMServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/im/IQIMServiceListener;

    move-result-object v1

    .line 105
    .local v1, "_arg1":Lcom/qualcomm/qti/im/IQIMServiceListener;
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSCreateIMService(Ljava/lang/String;Lcom/qualcomm/qti/im/IQIMServiceListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v2

    .line 106
    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 107
    if-eqz v2, :cond_4

    .line 108
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    invoke-virtual {v2, p3, v4}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 112
    :cond_4
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 118
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/im/IQIMServiceListener;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_7
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 121
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSDestroyIMService(I)V

    .line 122
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 127
    .end local v0    # "_arg0":I
    :sswitch_8
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 129
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 131
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/qualcomm/qti/is/IQISServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/is/IQISServiceListener;

    move-result-object v1

    .line 132
    .local v1, "_arg1":Lcom/qualcomm/qti/is/IQISServiceListener;
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSCreateISService(Ljava/lang/String;Lcom/qualcomm/qti/is/IQISServiceListener;)I

    move-result v2

    .line 133
    .local v2, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 134
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 139
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/is/IQISServiceListener;
    .end local v2    # "_result":I
    :sswitch_9
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 141
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 142
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSDestroyISService(I)V

    .line 143
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 148
    .end local v0    # "_arg0":I
    :sswitch_a
    const-string v5, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/qualcomm/qti/ft/IQFTServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/ft/IQFTServiceListener;

    move-result-object v1

    .line 153
    .local v1, "_arg1":Lcom/qualcomm/qti/ft/IQFTServiceListener;
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSCreateFTService(Ljava/lang/String;Lcom/qualcomm/qti/ft/IQFTServiceListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v2

    .line 154
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 155
    if-eqz v2, :cond_5

    .line 156
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 157
    invoke-virtual {v2, p3, v4}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 160
    :cond_5
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 166
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/ft/IQFTServiceListener;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_b
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 168
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 169
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSDestroyFTService(I)V

    .line 170
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 175
    .end local v0    # "_arg0":I
    :sswitch_c
    const-string v5, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 177
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/qualcomm/qti/config/IQConfigServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/config/IQConfigServiceListener;

    move-result-object v0

    .line 179
    .local v0, "_arg0":Lcom/qualcomm/qti/config/IQConfigServiceListener;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_6

    .line 180
    sget-object v5, Lcom/qualcomm/qti/rcsservice/QRCSLong;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/QRCSLong;

    .line 185
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSCreateConfigService(Lcom/qualcomm/qti/config/IQConfigServiceListener;Lcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v2

    .line 186
    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 187
    if-eqz v2, :cond_7

    .line 188
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 189
    invoke-virtual {v2, p3, v4}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 194
    :goto_2
    if-eqz v1, :cond_8

    .line 195
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 196
    invoke-virtual {v1, p3, v4}, Lcom/qualcomm/qti/rcsservice/QRCSLong;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 183
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_6
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    goto :goto_1

    .line 192
    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_7
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    .line 199
    :cond_8
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 205
    .end local v0    # "_arg0":Lcom/qualcomm/qti/config/IQConfigServiceListener;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_d
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 207
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 208
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSDestroyConfigService(I)V

    .line 209
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 214
    .end local v0    # "_arg0":I
    :sswitch_e
    const-string v5, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->getServiceStatus()Z

    move-result v2

    .line 216
    .local v2, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 217
    if-eqz v2, :cond_9

    move v3, v4

    :cond_9
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 222
    .end local v2    # "_result":Z
    :sswitch_f
    const-string v5, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/qualcomm/qti/presence/IQPresListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/presence/IQPresListener;

    move-result-object v0

    .line 226
    .local v0, "_arg0":Lcom/qualcomm/qti/presence/IQPresListener;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_a

    .line 227
    sget-object v5, Lcom/qualcomm/qti/rcsservice/QRCSLong;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/QRCSLong;

    .line 232
    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    :goto_3
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSCreatePresService(Lcom/qualcomm/qti/presence/IQPresListener;Lcom/qualcomm/qti/rcsservice/QRCSLong;)I

    move-result v2

    .line 233
    .local v2, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 234
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 235
    if-eqz v1, :cond_b

    .line 236
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 237
    invoke-virtual {v1, p3, v4}, Lcom/qualcomm/qti/rcsservice/QRCSLong;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 230
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v2    # "_result":I
    :cond_a
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    goto :goto_3

    .line 240
    .restart local v2    # "_result":I
    :cond_b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 246
    .end local v0    # "_arg0":Lcom/qualcomm/qti/presence/IQPresListener;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v2    # "_result":I
    :sswitch_10
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 248
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 249
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSDestroyPresService(I)V

    .line 250
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 255
    .end local v0    # "_arg0":I
    :sswitch_11
    const-string v5, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 257
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 259
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/standalone/IQSMServiceListener;

    move-result-object v1

    .line 260
    .local v1, "_arg1":Lcom/qualcomm/qti/standalone/IQSMServiceListener;
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSCreateSMService(Ljava/lang/String;Lcom/qualcomm/qti/standalone/IQSMServiceListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v2

    .line 261
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 262
    if-eqz v2, :cond_c

    .line 263
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 264
    invoke-virtual {v2, p3, v4}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 267
    :cond_c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 273
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/standalone/IQSMServiceListener;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_12
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 275
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 276
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->QRCSDestroySMService(I)V

    .line 277
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 282
    .end local v0    # "_arg0":I
    :sswitch_13
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 283
    invoke-virtual {p0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->getPresenceService()Lcom/qualcomm/qti/presence/IQPresService;

    move-result-object v2

    .line 284
    .local v2, "_result":Lcom/qualcomm/qti/presence/IQPresService;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 285
    if-eqz v2, :cond_d

    invoke-interface {v2}, Lcom/qualcomm/qti/presence/IQPresService;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    :goto_4
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    goto/16 :goto_0

    :cond_d
    move-object v3, v5

    goto :goto_4

    .line 290
    .end local v2    # "_result":Lcom/qualcomm/qti/presence/IQPresService;
    :sswitch_14
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 291
    invoke-virtual {p0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->getConfigService()Lcom/qualcomm/qti/config/IQConfigService;

    move-result-object v2

    .line 292
    .local v2, "_result":Lcom/qualcomm/qti/config/IQConfigService;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 293
    if-eqz v2, :cond_e

    invoke-interface {v2}, Lcom/qualcomm/qti/config/IQConfigService;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    :cond_e
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    goto/16 :goto_0

    .line 298
    .end local v2    # "_result":Lcom/qualcomm/qti/config/IQConfigService;
    :sswitch_15
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 299
    invoke-virtual {p0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->getCDService()Lcom/qualcomm/qti/cd/IQCDService;

    move-result-object v2

    .line 300
    .local v2, "_result":Lcom/qualcomm/qti/cd/IQCDService;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 301
    if-eqz v2, :cond_f

    invoke-interface {v2}, Lcom/qualcomm/qti/cd/IQCDService;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    :cond_f
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    goto/16 :goto_0

    .line 306
    .end local v2    # "_result":Lcom/qualcomm/qti/cd/IQCDService;
    :sswitch_16
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 307
    invoke-virtual {p0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->getIMService()Lcom/qualcomm/qti/im/IQIMService;

    move-result-object v2

    .line 308
    .local v2, "_result":Lcom/qualcomm/qti/im/IQIMService;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 309
    if-eqz v2, :cond_10

    invoke-interface {v2}, Lcom/qualcomm/qti/im/IQIMService;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    :cond_10
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    goto/16 :goto_0

    .line 314
    .end local v2    # "_result":Lcom/qualcomm/qti/im/IQIMService;
    :sswitch_17
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 315
    invoke-virtual {p0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->getFTService()Lcom/qualcomm/qti/ft/IQFTService;

    move-result-object v2

    .line 316
    .local v2, "_result":Lcom/qualcomm/qti/ft/IQFTService;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 317
    if-eqz v2, :cond_11

    invoke-interface {v2}, Lcom/qualcomm/qti/ft/IQFTService;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    :cond_11
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    goto/16 :goto_0

    .line 322
    .end local v2    # "_result":Lcom/qualcomm/qti/ft/IQFTService;
    :sswitch_18
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 323
    invoke-virtual {p0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->getISService()Lcom/qualcomm/qti/is/IQISService;

    move-result-object v2

    .line 324
    .local v2, "_result":Lcom/qualcomm/qti/is/IQISService;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 325
    if-eqz v2, :cond_12

    invoke-interface {v2}, Lcom/qualcomm/qti/is/IQISService;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    :cond_12
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    goto/16 :goto_0

    .line 330
    .end local v2    # "_result":Lcom/qualcomm/qti/is/IQISService;
    :sswitch_19
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 331
    invoke-virtual {p0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->getConfigServiceHandle()I

    move-result v2

    .line 332
    .local v2, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 333
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 338
    .end local v2    # "_result":I
    :sswitch_1a
    const-string v3, "com.qualcomm.qti.rcsservice.IQRCSService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 339
    invoke-virtual {p0}, Lcom/qualcomm/qti/rcsservice/IQRCSService$Stub;->getSMService()Lcom/qualcomm/qti/standalone/IQSMService;

    move-result-object v2

    .line 340
    .local v2, "_result":Lcom/qualcomm/qti/standalone/IQSMService;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 341
    if-eqz v2, :cond_13

    invoke-interface {v2}, Lcom/qualcomm/qti/standalone/IQSMService;->asBinder()Landroid/os/IBinder;

    move-result-object v5

    :cond_13
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    goto/16 :goto_0

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
