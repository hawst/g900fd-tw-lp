.class public Lcom/qualcomm/qti/rcsservice/QRCSLong;
.super Ljava/lang/Object;
.source "QRCSLong.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/QRCSLong;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private bQRCSLong:J

.field private nClientID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 113
    new-instance v0, Lcom/qualcomm/qti/rcsservice/QRCSLong$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/QRCSLong$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/QRCSLong;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/16 v0, 0x3e9

    iput v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSLong;->nClientID:I

    .line 29
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/16 v0, 0x3e9

    iput v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSLong;->nClientID:I

    .line 125
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/QRCSLong;->readFromParcel(Landroid/os/Parcel;)V

    .line 126
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/QRCSLong$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/QRCSLong$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/QRCSLong;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getQRCSLongInstance()Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .locals 2

    .prologue
    .line 94
    const-string v0, "aks"

    const-string v1, "AIDL     QRCSLong   getQRCSLongInstance   78     "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    new-instance v0, Lcom/qualcomm/qti/rcsservice/QRCSLong;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/QRCSLong;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 109
    iget-wide v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSLong;->bQRCSLong:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 110
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSLong;->nClientID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 111
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return v0
.end method

.method public getClientID()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSLong;->nClientID:I

    return v0
.end method

.method public getQRCSLong()J
    .locals 2

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSLong;->bQRCSLong:J

    return-wide v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSLong;->bQRCSLong:J

    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsservice/QRCSLong;->nClientID:I

    .line 131
    return-void
.end method

.method public setClientID(I)V
    .locals 0
    .param p1, "nClientID"    # I

    .prologue
    .line 80
    iput p1, p0, Lcom/qualcomm/qti/rcsservice/QRCSLong;->nClientID:I

    .line 81
    return-void
.end method

.method public setQRCSLong(J)V
    .locals 1
    .param p1, "rcsLong"    # J

    .prologue
    .line 52
    iput-wide p1, p0, Lcom/qualcomm/qti/rcsservice/QRCSLong;->bQRCSLong:J

    .line 53
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/QRCSLong;->writeToParcel(Landroid/os/Parcel;)V

    .line 105
    return-void
.end method
