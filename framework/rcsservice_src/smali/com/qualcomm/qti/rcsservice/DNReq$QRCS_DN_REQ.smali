.class public final enum Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;
.super Ljava/lang/Enum;
.source "DNReq.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsservice/DNReq;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QRCS_DN_REQ"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

.field public static final enum QRCS_DN_REQ_DELIVERY:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

.field public static final enum QRCS_DN_REQ_DELIVERY_AND_DISPLAY:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

.field public static final enum QRCS_DN_REQ_DISPLAY:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

.field public static final enum QRCS_DN_REQ_MAX32:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

.field public static final enum QRCS_DN_REQ_NONE:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

.field public static final enum QRCS_DN_REQ_UNKNOWN:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

.field public static final enum QRCS_DN_UNKNOWN:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    new-instance v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    const-string v1, "QRCS_DN_REQ_UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_UNKNOWN:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    .line 25
    new-instance v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    const-string v1, "QRCS_DN_REQ_NONE"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_NONE:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    .line 27
    new-instance v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    const-string v1, "QRCS_DN_REQ_DELIVERY"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_DELIVERY:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    .line 29
    new-instance v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    const-string v1, "QRCS_DN_REQ_DISPLAY"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_DISPLAY:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    .line 31
    new-instance v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    const-string v1, "QRCS_DN_REQ_DELIVERY_AND_DISPLAY"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_DELIVERY_AND_DISPLAY:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    .line 33
    new-instance v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    const-string v1, "QRCS_DN_REQ_MAX32"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_MAX32:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    .line 34
    new-instance v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    const-string v1, "QRCS_DN_UNKNOWN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_UNKNOWN:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    .line 22
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    sget-object v1, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_UNKNOWN:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_NONE:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_DELIVERY:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_DISPLAY:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_DELIVERY_AND_DISPLAY:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_REQ_MAX32:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->QRCS_DN_UNKNOWN:Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->$VALUES:[Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->$VALUES:[Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    return-object v0
.end method
