.class public Lcom/qualcomm/qti/rcsservice/RecipientInfo;
.super Ljava/lang/Object;
.source "RecipientInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/RecipientInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private cctype:Lcom/qualcomm/qti/rcsservice/CopyControlType;

.field private contactUri:Ljava/lang/String;

.field private displayName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/qualcomm/qti/rcsservice/RecipientInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/RecipientInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->contactUri:Ljava/lang/String;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->displayName:Ljava/lang/String;

    .line 94
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->contactUri:Ljava/lang/String;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->displayName:Ljava/lang/String;

    .line 124
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 125
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/RecipientInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/RecipientInfo$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/RecipientInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getParticipantClassObject()Lcom/qualcomm/qti/rcsservice/RecipientInfo;
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/RecipientInfo;-><init>()V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public getContactUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->contactUri:Ljava/lang/String;

    return-object v0
.end method

.method public getCopyControlType()Lcom/qualcomm/qti/rcsservice/CopyControlType;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->cctype:Lcom/qualcomm/qti/rcsservice/CopyControlType;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->contactUri:Ljava/lang/String;

    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->displayName:Ljava/lang/String;

    .line 130
    const-class v0, Lcom/qualcomm/qti/rcsservice/CopyControlType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/CopyControlType;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->cctype:Lcom/qualcomm/qti/rcsservice/CopyControlType;

    .line 131
    return-void
.end method

.method public setContactUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "contactUri"    # Ljava/lang/String;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->contactUri:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public setCopyControlType(Lcom/qualcomm/qti/rcsservice/CopyControlType;)V
    .locals 0
    .param p1, "cctype"    # Lcom/qualcomm/qti/rcsservice/CopyControlType;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->cctype:Lcom/qualcomm/qti/rcsservice/CopyControlType;

    .line 87
    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0
    .param p1, "displayName"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->displayName:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 107
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->contactUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->displayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->cctype:Lcom/qualcomm/qti/rcsservice/CopyControlType;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 110
    return-void
.end method
