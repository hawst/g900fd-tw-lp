.class public Lcom/qualcomm/qti/rcsservice/IncomingMessage;
.super Ljava/lang/Object;
.source "IncomingMessage.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/IncomingMessage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

.field private imdnReqInfo:Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

.field private participant:Lcom/qualcomm/qti/rcsservice/Participant;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/qualcomm/qti/rcsservice/IncomingMessage$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/IncomingMessage$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/IncomingMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/IncomingMessage;->readFromParcel(Landroid/os/Parcel;)V

    .line 58
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/IncomingMessage$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/IncomingMessage$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/IncomingMessage;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method public getContentInfo()Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingMessage;->contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    return-object v0
.end method

.method public getImdnReqInfo()Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingMessage;->imdnReqInfo:Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    return-object v0
.end method

.method public getParticipant()Lcom/qualcomm/qti/rcsservice/Participant;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingMessage;->participant:Lcom/qualcomm/qti/rcsservice/Participant;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 61
    const-class v0, Lcom/qualcomm/qti/rcsservice/Participant;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/Participant;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingMessage;->participant:Lcom/qualcomm/qti/rcsservice/Participant;

    .line 62
    const-class v0, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingMessage;->contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 63
    const-class v0, Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingMessage;->imdnReqInfo:Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    .line 65
    return-void
.end method

.method public setContentInfo(Lcom/qualcomm/qti/rcsservice/ContentInfo;)V
    .locals 0
    .param p1, "contentInfo"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/IncomingMessage;->contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 109
    return-void
.end method

.method public setImdnReqInfo(Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;)V
    .locals 0
    .param p1, "imdnReqInfo"    # Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/IncomingMessage;->imdnReqInfo:Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    .line 130
    return-void
.end method

.method public setParticipant(Lcom/qualcomm/qti/rcsservice/Participant;)V
    .locals 0
    .param p1, "participant"    # Lcom/qualcomm/qti/rcsservice/Participant;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/IncomingMessage;->participant:Lcom/qualcomm/qti/rcsservice/Participant;

    .line 88
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 38
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingMessage;->participant:Lcom/qualcomm/qti/rcsservice/Participant;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 39
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingMessage;->contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 40
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingMessage;->imdnReqInfo:Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 41
    return-void
.end method
