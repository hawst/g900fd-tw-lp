.class public Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;
.super Ljava/lang/Object;
.source "SessionCreationInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private conferenceFocusURI:Ljava/lang/String;

.field private groupName:Ljava/lang/String;

.field private idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

.field private participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

.field private sessionType:Lcom/qualcomm/qti/rcsservice/SessionType;

.field private subject:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->subject:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->groupName:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->conferenceFocusURI:Ljava/lang/String;

    .line 32
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->subject:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->groupName:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->conferenceFocusURI:Ljava/lang/String;

    .line 71
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 72
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/SessionCreationInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/SessionCreationInfo$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public getConferenceFocusURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->conferenceFocusURI:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->groupName:Ljava/lang/String;

    return-object v0
.end method

.method public getIdHeaderInfo()Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    return-object v0
.end method

.method public getParticipantArray()[Lcom/qualcomm/qti/rcsservice/Participant;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

    return-object v0
.end method

.method public getSessionType()Lcom/qualcomm/qti/rcsservice/SessionType;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->sessionType:Lcom/qualcomm/qti/rcsservice/SessionType;

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->subject:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 75
    const-class v1, Lcom/qualcomm/qti/rcsservice/SessionType;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/SessionType;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->sessionType:Lcom/qualcomm/qti/rcsservice/SessionType;

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 77
    .local v0, "length":I
    if-lez v0, :cond_0

    .line 79
    new-array v1, v0, [Lcom/qualcomm/qti/rcsservice/Participant;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

    .line 80
    iget-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

    sget-object v2, Lcom/qualcomm/qti/rcsservice/Participant;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    .line 82
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->subject:Ljava/lang/String;

    .line 83
    const-class v1, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->groupName:Ljava/lang/String;

    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->conferenceFocusURI:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public setConferenceFocusURI(Ljava/lang/String;)V
    .locals 0
    .param p1, "conferenceFocusURI"    # Ljava/lang/String;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->conferenceFocusURI:Ljava/lang/String;

    .line 193
    return-void
.end method

.method public setGroupName(Ljava/lang/String;)V
    .locals 0
    .param p1, "groupName"    # Ljava/lang/String;

    .prologue
    .line 171
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->groupName:Ljava/lang/String;

    .line 172
    return-void
.end method

.method public setIdHeaderInfo(Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;)V
    .locals 0
    .param p1, "idHeaderInfo"    # Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .line 214
    return-void
.end method

.method public setParticipantArray([Lcom/qualcomm/qti/rcsservice/Participant;)V
    .locals 0
    .param p1, "participantArray"    # [Lcom/qualcomm/qti/rcsservice/Participant;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

    .line 151
    return-void
.end method

.method public setSessionType(Lcom/qualcomm/qti/rcsservice/SessionType;)V
    .locals 0
    .param p1, "sessionType"    # Lcom/qualcomm/qti/rcsservice/SessionType;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->sessionType:Lcom/qualcomm/qti/rcsservice/SessionType;

    .line 130
    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 0
    .param p1, "subject"    # Ljava/lang/String;

    .prologue
    .line 108
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->subject:Ljava/lang/String;

    .line 109
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x0

    .line 40
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->sessionType:Lcom/qualcomm/qti/rcsservice/SessionType;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 41
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 44
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 50
    :goto_0
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->subject:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 52
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->groupName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->conferenceFocusURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 55
    return-void

    .line 48
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
