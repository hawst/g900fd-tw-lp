.class public Lcom/qualcomm/qti/rcsservice/SessionList;
.super Ljava/lang/Object;
.source "SessionList.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/SessionList;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private sessionHandleList:[I

.field private sessionHandleListLen:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    new-instance v0, Lcom/qualcomm/qti/rcsservice/SessionList$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/SessionList$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/SessionList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionList;->sessionHandleList:[I

    .line 56
    iput v1, p0, Lcom/qualcomm/qti/rcsservice/SessionList;->sessionHandleListLen:I

    .line 57
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/SessionList;->readFromParcel(Landroid/os/Parcel;)V

    .line 88
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/SessionList$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/SessionList$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/SessionList;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getSessionListClassObject()Lcom/qualcomm/qti/rcsservice/SessionList;
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/qualcomm/qti/rcsservice/SessionList;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/SessionList;-><init>()V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method public getSessionHandleList()[I
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionList;->sessionHandleList:[I

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionList;->sessionHandleList:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readIntArray([I)V

    .line 92
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsservice/SessionList;->sessionHandleListLen:I

    .line 93
    return-void
.end method

.method public setSessionHandleList([I)V
    .locals 1
    .param p1, "sessionHandleList"    # [I

    .prologue
    .line 43
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    .line 45
    array-length v0, p1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionList;->sessionHandleList:[I

    .line 46
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/SessionList;->sessionHandleList:[I

    .line 47
    array-length v0, p1

    iput v0, p0, Lcom/qualcomm/qti/rcsservice/SessionList;->sessionHandleListLen:I

    .line 49
    :cond_0
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionList;->sessionHandleList:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 71
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/SessionList;->sessionHandleListLen:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 73
    return-void
.end method
