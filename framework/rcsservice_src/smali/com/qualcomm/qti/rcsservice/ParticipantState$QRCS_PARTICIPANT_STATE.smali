.class public final enum Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;
.super Ljava/lang/Enum;
.source "ParticipantState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsservice/ParticipantState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QRCS_PARTICIPANT_STATE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

.field public static final enum QRCS_PARTICIPANT_STATE_CONNECTED:Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

.field public static final enum QRCS_PARTICIPANT_STATE_DISCONNECTED:Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

.field public static final enum QRCS_PARTICIPANT_STATE_PENDING:Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

.field public static final enum QRCS_PARTICIPANT_STATE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

.field public static final enum QRCS_PARTICIPANT_UNKNOWN:Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    const-string v1, "QRCS_PARTICIPANT_STATE_UNKNOWN"

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;->QRCS_PARTICIPANT_STATE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    .line 22
    new-instance v0, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    const-string v1, "QRCS_PARTICIPANT_STATE_PENDING"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;->QRCS_PARTICIPANT_STATE_PENDING:Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    .line 23
    new-instance v0, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    const-string v1, "QRCS_PARTICIPANT_STATE_CONNECTED"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;->QRCS_PARTICIPANT_STATE_CONNECTED:Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    .line 24
    new-instance v0, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    const-string v1, "QRCS_PARTICIPANT_STATE_DISCONNECTED"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;->QRCS_PARTICIPANT_STATE_DISCONNECTED:Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    .line 25
    new-instance v0, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    const-string v1, "QRCS_PARTICIPANT_UNKNOWN"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;->QRCS_PARTICIPANT_UNKNOWN:Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    .line 20
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    sget-object v1, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;->QRCS_PARTICIPANT_STATE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;->QRCS_PARTICIPANT_STATE_PENDING:Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;->QRCS_PARTICIPANT_STATE_CONNECTED:Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;->QRCS_PARTICIPANT_STATE_DISCONNECTED:Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;->QRCS_PARTICIPANT_UNKNOWN:Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    aput-object v1, v0, v6

    sput-object v0, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;->$VALUES:[Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;->$VALUES:[Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/rcsservice/ParticipantState$QRCS_PARTICIPANT_STATE;

    return-object v0
.end method
