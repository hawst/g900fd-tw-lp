.class public Lcom/qualcomm/qti/rcsservice/SessionInfo;
.super Ljava/lang/Object;
.source "SessionInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/SessionInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private conferenceFocusURI:Ljava/lang/String;

.field private groupName:Ljava/lang/String;

.field private idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

.field private isDeliveryReportEnabled:Z

.field private isFtHttpSupported:Z

.field private isFtStAndFwSupported:Z

.field private isFtSupported:Z

.field private isFtThumbSupported:Z

.field private isGeoPushSupported:Z

.field private isOutgoingSession:Z

.field private isSessionStoreAndForward:Z

.field private participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

.field private sessionHandle:I

.field private sessionState:Lcom/qualcomm/qti/rcsservice/SessionState;

.field private sessionType:Lcom/qualcomm/qti/rcsservice/SessionType;

.field private subject:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/qualcomm/qti/rcsservice/SessionInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/SessionInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->subject:Ljava/lang/String;

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->groupName:Ljava/lang/String;

    .line 43
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->subject:Ljava/lang/String;

    .line 35
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->groupName:Ljava/lang/String;

    .line 92
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/SessionInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 93
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/SessionInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/SessionInfo$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/SessionInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public getConferenceFocusURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->conferenceFocusURI:Ljava/lang/String;

    return-object v0
.end method

.method public getGroupName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->groupName:Ljava/lang/String;

    return-object v0
.end method

.method public getIdHeaderInfo()Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    return-object v0
.end method

.method public getParticipantArray()[Lcom/qualcomm/qti/rcsservice/Participant;
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

    return-object v0
.end method

.method public getSessionHandle()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->sessionHandle:I

    return v0
.end method

.method public getSessionState()Lcom/qualcomm/qti/rcsservice/SessionState;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->sessionState:Lcom/qualcomm/qti/rcsservice/SessionState;

    return-object v0
.end method

.method public getSessionType()Lcom/qualcomm/qti/rcsservice/SessionType;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->sessionType:Lcom/qualcomm/qti/rcsservice/SessionType;

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->subject:Ljava/lang/String;

    return-object v0
.end method

.method public isDeliveryReportEnabled()Z
    .locals 1

    .prologue
    .line 296
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isDeliveryReportEnabled:Z

    return v0
.end method

.method public isFtHttpSupported()Z
    .locals 1

    .prologue
    .line 426
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtHttpSupported:Z

    return v0
.end method

.method public isFtStAndFwSupported()Z
    .locals 1

    .prologue
    .line 404
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtStAndFwSupported:Z

    return v0
.end method

.method public isFtSupported()Z
    .locals 1

    .prologue
    .line 360
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtSupported:Z

    return v0
.end method

.method public isFtThumbSupported()Z
    .locals 1

    .prologue
    .line 382
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtThumbSupported:Z

    return v0
.end method

.method public isGeoPushSupported()Z
    .locals 1

    .prologue
    .line 448
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isGeoPushSupported:Z

    return v0
.end method

.method public isOutgoingSession()Z
    .locals 1

    .prologue
    .line 191
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isOutgoingSession:Z

    return v0
.end method

.method public isSessionStoreAndForward()Z
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isSessionStoreAndForward:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->sessionHandle:I

    .line 97
    const-class v1, Lcom/qualcomm/qti/rcsservice/SessionState;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/SessionState;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->sessionState:Lcom/qualcomm/qti/rcsservice/SessionState;

    .line 98
    const-class v1, Lcom/qualcomm/qti/rcsservice/SessionState;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/SessionType;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->sessionType:Lcom/qualcomm/qti/rcsservice/SessionType;

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isOutgoingSession:Z

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isSessionStoreAndForward:Z

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isDeliveryReportEnabled:Z

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_4

    move v1, v2

    :goto_3
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtSupported:Z

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    :goto_4
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtThumbSupported:Z

    .line 104
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_6

    move v1, v2

    :goto_5
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtStAndFwSupported:Z

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_7

    move v1, v2

    :goto_6
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtHttpSupported:Z

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_8

    :goto_7
    iput-boolean v2, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isGeoPushSupported:Z

    .line 107
    const-class v1, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->subject:Ljava/lang/String;

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->groupName:Ljava/lang/String;

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->conferenceFocusURI:Ljava/lang/String;

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 112
    .local v0, "length":I
    if-lez v0, :cond_0

    .line 114
    new-array v1, v0, [Lcom/qualcomm/qti/rcsservice/Participant;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

    .line 115
    iget-object v1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

    sget-object v2, Lcom/qualcomm/qti/rcsservice/Participant;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    .line 117
    :cond_0
    return-void

    .end local v0    # "length":I
    :cond_1
    move v1, v3

    .line 99
    goto :goto_0

    :cond_2
    move v1, v3

    .line 100
    goto :goto_1

    :cond_3
    move v1, v3

    .line 101
    goto :goto_2

    :cond_4
    move v1, v3

    .line 102
    goto :goto_3

    :cond_5
    move v1, v3

    .line 103
    goto :goto_4

    :cond_6
    move v1, v3

    .line 104
    goto :goto_5

    :cond_7
    move v1, v3

    .line 105
    goto :goto_6

    :cond_8
    move v2, v3

    .line 106
    goto :goto_7
.end method

.method public setConferenceFocusURI(Ljava/lang/String;)V
    .locals 0
    .param p1, "conferenceFocusURI"    # Ljava/lang/String;

    .prologue
    .line 286
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->conferenceFocusURI:Ljava/lang/String;

    .line 287
    return-void
.end method

.method public setDeliveryReportEnabled(Z)V
    .locals 0
    .param p1, "isDeliveryReportEnabled"    # Z

    .prologue
    .line 307
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isDeliveryReportEnabled:Z

    .line 308
    return-void
.end method

.method public setFtHttpSupported(Z)V
    .locals 0
    .param p1, "isFtHttpSupported"    # Z

    .prologue
    .line 437
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtHttpSupported:Z

    .line 438
    return-void
.end method

.method public setFtStAndFwSupported(Z)V
    .locals 0
    .param p1, "isFtStAndFwSupported"    # Z

    .prologue
    .line 415
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtStAndFwSupported:Z

    .line 416
    return-void
.end method

.method public setFtSupported(Z)V
    .locals 0
    .param p1, "isFtSupported"    # Z

    .prologue
    .line 371
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtSupported:Z

    .line 372
    return-void
.end method

.method public setFtThumbSupported(Z)V
    .locals 0
    .param p1, "isFtThumbSupported"    # Z

    .prologue
    .line 393
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtThumbSupported:Z

    .line 394
    return-void
.end method

.method public setGeoPushSupported(Z)V
    .locals 0
    .param p1, "isGeoPushSupported"    # Z

    .prologue
    .line 459
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isGeoPushSupported:Z

    .line 460
    return-void
.end method

.method public setGroupName(Ljava/lang/String;)V
    .locals 0
    .param p1, "groupName"    # Ljava/lang/String;

    .prologue
    .line 328
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->groupName:Ljava/lang/String;

    .line 329
    return-void
.end method

.method public setIdHeaderInfo(Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;)V
    .locals 0
    .param p1, "idHeaderInfo"    # Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .prologue
    .line 244
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .line 245
    return-void
.end method

.method public setOutgoingSession(Z)V
    .locals 0
    .param p1, "isOutgoingSession"    # Z

    .prologue
    .line 202
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isOutgoingSession:Z

    .line 203
    return-void
.end method

.method public setParticipantArray([Lcom/qualcomm/qti/rcsservice/Participant;)V
    .locals 0
    .param p1, "participantArray"    # [Lcom/qualcomm/qti/rcsservice/Participant;

    .prologue
    .line 349
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

    .line 350
    return-void
.end method

.method public setSessionHandle(I)V
    .locals 0
    .param p1, "sessionHandle"    # I

    .prologue
    .line 139
    iput p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->sessionHandle:I

    .line 140
    return-void
.end method

.method public setSessionState(Lcom/qualcomm/qti/rcsservice/SessionState;)V
    .locals 0
    .param p1, "sessionState"    # Lcom/qualcomm/qti/rcsservice/SessionState;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->sessionState:Lcom/qualcomm/qti/rcsservice/SessionState;

    .line 161
    return-void
.end method

.method public setSessionStoreAndForward(Z)V
    .locals 0
    .param p1, "isSessionStoreAndForward"    # Z

    .prologue
    .line 223
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isSessionStoreAndForward:Z

    .line 224
    return-void
.end method

.method public setSessionType(Lcom/qualcomm/qti/rcsservice/SessionType;)V
    .locals 0
    .param p1, "sessionType"    # Lcom/qualcomm/qti/rcsservice/SessionType;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->sessionType:Lcom/qualcomm/qti/rcsservice/SessionType;

    .line 182
    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 0
    .param p1, "subject"    # Ljava/lang/String;

    .prologue
    .line 265
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->subject:Ljava/lang/String;

    .line 266
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->sessionHandle:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 52
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->sessionState:Lcom/qualcomm/qti/rcsservice/SessionState;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 53
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->sessionType:Lcom/qualcomm/qti/rcsservice/SessionType;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 54
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isOutgoingSession:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 55
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isSessionStoreAndForward:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 56
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isDeliveryReportEnabled:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 57
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtSupported:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 58
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtThumbSupported:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtStAndFwSupported:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 60
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isFtHttpSupported:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 61
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->isGeoPushSupported:Z

    if-eqz v0, :cond_7

    :goto_7
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 62
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 63
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->subject:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->groupName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 65
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->conferenceFocusURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

    if-eqz v0, :cond_8

    .line 69
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->participantArray:[Lcom/qualcomm/qti/rcsservice/Participant;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 76
    :goto_8
    return-void

    :cond_0
    move v0, v2

    .line 54
    goto :goto_0

    :cond_1
    move v0, v2

    .line 55
    goto :goto_1

    :cond_2
    move v0, v2

    .line 56
    goto :goto_2

    :cond_3
    move v0, v2

    .line 57
    goto :goto_3

    :cond_4
    move v0, v2

    .line 58
    goto :goto_4

    :cond_5
    move v0, v2

    .line 59
    goto :goto_5

    :cond_6
    move v0, v2

    .line 60
    goto :goto_6

    :cond_7
    move v1, v2

    .line 61
    goto :goto_7

    .line 74
    :cond_8
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_8
.end method
