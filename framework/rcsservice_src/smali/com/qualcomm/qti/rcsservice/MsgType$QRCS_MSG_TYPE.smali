.class public final enum Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;
.super Ljava/lang/Enum;
.source "MsgType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsservice/MsgType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QRCS_MSG_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

.field public static final enum QRCS_MSG_TYPE_SESSION_CHAT:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

.field public static final enum QRCS_MSG_TYPE_SESSION_FT:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

.field public static final enum QRCS_MSG_TYPE_SM_LARGE_MSG_MODE:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

.field public static final enum QRCS_MSG_TYPE_SM_PAGER_MODE:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

.field public static final enum QRCS_SM_MSG_MAX32:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

.field public static final enum QRCS_SM_TYPE_NONE:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

.field public static final enum QRCS_SM_TYPE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    new-instance v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    const-string v1, "QRCS_SM_TYPE_NONE"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_SM_TYPE_NONE:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    .line 25
    new-instance v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    const-string v1, "QRCS_MSG_TYPE_SM_PAGER_MODE"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_MSG_TYPE_SM_PAGER_MODE:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    .line 27
    new-instance v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    const-string v1, "QRCS_MSG_TYPE_SM_LARGE_MSG_MODE"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_MSG_TYPE_SM_LARGE_MSG_MODE:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    .line 29
    new-instance v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    const-string v1, "QRCS_MSG_TYPE_SESSION_CHAT"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_MSG_TYPE_SESSION_CHAT:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    .line 30
    new-instance v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    const-string v1, "QRCS_MSG_TYPE_SESSION_FT"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_MSG_TYPE_SESSION_FT:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    .line 32
    new-instance v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    const-string v1, "QRCS_SM_MSG_MAX32"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_SM_MSG_MAX32:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    .line 33
    new-instance v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    const-string v1, "QRCS_SM_TYPE_UNKNOWN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_SM_TYPE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    .line 22
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    sget-object v1, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_SM_TYPE_NONE:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_MSG_TYPE_SM_PAGER_MODE:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_MSG_TYPE_SM_LARGE_MSG_MODE:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_MSG_TYPE_SESSION_CHAT:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_MSG_TYPE_SESSION_FT:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_SM_MSG_MAX32:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_SM_TYPE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->$VALUES:[Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->$VALUES:[Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    return-object v0
.end method
