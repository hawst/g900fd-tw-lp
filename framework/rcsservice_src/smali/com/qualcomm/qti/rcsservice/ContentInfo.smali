.class public Lcom/qualcomm/qti/rcsservice/ContentInfo;
.super Ljava/lang/Object;
.source "ContentInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/ContentInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private content:Lcom/qualcomm/qti/rcsservice/Content;

.field private contentType:Lcom/qualcomm/qti/rcsservice/ContentType;

.field private fileInfo:Lcom/qualcomm/qti/rcsservice/File;

.field private textMsg:Ljava/lang/String;

.field private totalSize:I

.field private totalSizeRemaining:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/qualcomm/qti/rcsservice/ContentInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/ContentInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->textMsg:Ljava/lang/String;

    .line 33
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->textMsg:Ljava/lang/String;

    .line 62
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/ContentInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 63
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/ContentInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/ContentInfo$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/ContentInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public getContent()Lcom/qualcomm/qti/rcsservice/Content;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->content:Lcom/qualcomm/qti/rcsservice/Content;

    return-object v0
.end method

.method public getContentType()Lcom/qualcomm/qti/rcsservice/ContentType;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->contentType:Lcom/qualcomm/qti/rcsservice/ContentType;

    return-object v0
.end method

.method public getFileInfo()Lcom/qualcomm/qti/rcsservice/File;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->fileInfo:Lcom/qualcomm/qti/rcsservice/File;

    return-object v0
.end method

.method public getTextMsg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->textMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalSize()I
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->totalSize:I

    return v0
.end method

.method public getTotalSizeRemaining()I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->totalSizeRemaining:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 66
    const-class v0, Lcom/qualcomm/qti/rcsservice/ContentType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/ContentType;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->contentType:Lcom/qualcomm/qti/rcsservice/ContentType;

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->textMsg:Ljava/lang/String;

    .line 68
    const-class v0, Lcom/qualcomm/qti/rcsservice/Content;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/Content;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->content:Lcom/qualcomm/qti/rcsservice/Content;

    .line 69
    const-class v0, Lcom/qualcomm/qti/rcsservice/File;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/File;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->fileInfo:Lcom/qualcomm/qti/rcsservice/File;

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->totalSize:I

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->totalSizeRemaining:I

    .line 72
    return-void
.end method

.method public setContent(Lcom/qualcomm/qti/rcsservice/Content;)V
    .locals 0
    .param p1, "content"    # Lcom/qualcomm/qti/rcsservice/Content;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->content:Lcom/qualcomm/qti/rcsservice/Content;

    .line 137
    return-void
.end method

.method public setContentType(Lcom/qualcomm/qti/rcsservice/ContentType;)V
    .locals 0
    .param p1, "contentType"    # Lcom/qualcomm/qti/rcsservice/ContentType;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->contentType:Lcom/qualcomm/qti/rcsservice/ContentType;

    .line 95
    return-void
.end method

.method public setFileInfo(Lcom/qualcomm/qti/rcsservice/File;)V
    .locals 0
    .param p1, "fileInfo"    # Lcom/qualcomm/qti/rcsservice/File;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->fileInfo:Lcom/qualcomm/qti/rcsservice/File;

    .line 158
    return-void
.end method

.method public setTextMsg(Ljava/lang/String;)V
    .locals 0
    .param p1, "textMsg"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->textMsg:Ljava/lang/String;

    .line 116
    return-void
.end method

.method public setTotalSize(I)V
    .locals 0
    .param p1, "totalSize"    # I

    .prologue
    .line 178
    iput p1, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->totalSize:I

    .line 179
    return-void
.end method

.method public setTotalSizeRemaining(I)V
    .locals 0
    .param p1, "totalSizeRemaining"    # I

    .prologue
    .line 199
    iput p1, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->totalSizeRemaining:I

    .line 200
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 40
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->contentType:Lcom/qualcomm/qti/rcsservice/ContentType;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 41
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->textMsg:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->content:Lcom/qualcomm/qti/rcsservice/Content;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->fileInfo:Lcom/qualcomm/qti/rcsservice/File;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 44
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->totalSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 45
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->totalSizeRemaining:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 47
    return-void
.end method
