.class public Lcom/qualcomm/qti/rcsservice/NativeRCS;
.super Ljava/lang/Object;
.source "NativeRCS.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native QRCSCreateCDService(Lcom/qualcomm/qti/cd/IQCDServiceListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QRCSCreateConfigService(Lcom/qualcomm/qti/config/IQConfigServiceListener;Lcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QRCSCreateFTService(Ljava/lang/String;Lcom/qualcomm/qti/ft/IQFTServiceListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QRCSCreateIMService(Ljava/lang/String;Lcom/qualcomm/qti/im/IQIMServiceListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QRCSCreateISService(Ljava/lang/String;Lcom/qualcomm/qti/is/IQISServiceListener;)I
.end method

.method public native QRCSCreatePresService(Lcom/qualcomm/qti/presence/IQPresListener;Lcom/qualcomm/qti/rcsservice/QRCSLong;)I
.end method

.method public native QRCSCreateSMService(Ljava/lang/String;Lcom/qualcomm/qti/standalone/IQSMServiceListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QRCSDestroyCDService(I)V
.end method

.method public native QRCSDestroyConfigService(I)V
.end method

.method public native QRCSDestroyFTService(I)V
.end method

.method public native QRCSDestroyIMService(I)V
.end method

.method public native QRCSDestroyISService(I)V
.end method

.method public native QRCSDestroyPresService(I)V
.end method

.method public native QRCSDestroySMService(I)V
.end method

.method public native QRCSIsRCSServiceStarted()Z
.end method

.method public native QRCSStartRCSService(Lcom/qualcomm/qti/rcsservice/IQRCSListener;)Z
.end method

.method public native QRCSStopRCSService()Z
.end method

.method public native QRcsAMTimerExpired(I)V
.end method
