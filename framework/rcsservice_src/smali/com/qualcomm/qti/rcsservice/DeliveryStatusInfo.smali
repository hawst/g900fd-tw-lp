.class public Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;
.super Ljava/lang/Object;
.source "DeliveryStatusInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private dateTime:Ljava/lang/String;

.field private deliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus;

.field private msgID:Ljava/lang/String;

.field private recipientURI:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->msgID:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->recipientURI:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->dateTime:Ljava/lang/String;

    .line 31
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->msgID:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->recipientURI:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->dateTime:Ljava/lang/String;

    .line 58
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 59
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method public getDateTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->dateTime:Ljava/lang/String;

    return-object v0
.end method

.method public getDeliveryStatus()Lcom/qualcomm/qti/rcsservice/DeliveryStatus;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->deliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus;

    return-object v0
.end method

.method public getMsgID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->msgID:Ljava/lang/String;

    return-object v0
.end method

.method public getRecipientURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->recipientURI:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->msgID:Ljava/lang/String;

    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->recipientURI:Ljava/lang/String;

    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->dateTime:Ljava/lang/String;

    .line 65
    const-class v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->deliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus;

    .line 67
    return-void
.end method

.method public setDateTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "dateTime"    # Ljava/lang/String;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->dateTime:Ljava/lang/String;

    .line 132
    return-void
.end method

.method public setDeliveryStatus(Lcom/qualcomm/qti/rcsservice/DeliveryStatus;)V
    .locals 0
    .param p1, "deliveryStatus"    # Lcom/qualcomm/qti/rcsservice/DeliveryStatus;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->deliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus;

    .line 153
    return-void
.end method

.method public setMsgID(Ljava/lang/String;)V
    .locals 0
    .param p1, "msgID"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->msgID:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public setRecipientURI(Ljava/lang/String;)V
    .locals 0
    .param p1, "recipientURI"    # Ljava/lang/String;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->recipientURI:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 38
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->msgID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->recipientURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->dateTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->deliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 43
    return-void
.end method
