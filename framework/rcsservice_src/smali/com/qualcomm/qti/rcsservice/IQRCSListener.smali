.class public interface abstract Lcom/qualcomm/qti/rcsservice/IQRCSListener;
.super Ljava/lang/Object;
.source "IQRCSListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsservice/IQRCSListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract IQRCSListener_RcsStartTimer(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQRCSListener_RcsStopTimer(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQRCSListener_SetStatus(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
