.class public final enum Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;
.super Ljava/lang/Enum;
.source "Error.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsservice/Error;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QRCS_ERROR"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

.field public static final enum QRCS_ERROR_EXCEED_MAX_SIZE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

.field public static final enum QRCS_ERROR_FILE_NOT_PRESENT:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

.field public static final enum QRCS_ERROR_ICON_EXCEED_MAX_SIZE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

.field public static final enum QRCS_ERROR_INTERRUPTION:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

.field public static final enum QRCS_ERROR_MAX32:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

.field public static final enum QRCS_ERROR_MISSING_PARAM:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

.field public static final enum QRCS_ERROR_MSRP:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

.field public static final enum QRCS_ERROR_NETWORK_LOST:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

.field public static final enum QRCS_ERROR_SIP_CANCEL:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

.field public static final enum QRCS_ERROR_SIP_INVITE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

.field public static final enum QRCS_ERROR_SIP_MESSAGE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

.field public static final enum QRCS_ERROR_SIP_REFER:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

.field public static final enum QRCS_ERROR_UNKNOWN:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

.field public static final enum QRCS_ERROR_USER_CANCEL:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

.field public static final enum QRCS_UNKNOWN:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    const-string v1, "QRCS_ERROR_UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_UNKNOWN:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 26
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    const-string v1, "QRCS_ERROR_USER_CANCEL"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_USER_CANCEL:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 28
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    const-string v1, "QRCS_ERROR_NETWORK_LOST"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_NETWORK_LOST:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 30
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    const-string v1, "QRCS_ERROR_FILE_NOT_PRESENT"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_FILE_NOT_PRESENT:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 32
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    const-string v1, "QRCS_ERROR_EXCEED_MAX_SIZE"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_EXCEED_MAX_SIZE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 34
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    const-string v1, "QRCS_ERROR_ICON_EXCEED_MAX_SIZE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_ICON_EXCEED_MAX_SIZE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 39
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    const-string v1, "QRCS_ERROR_INTERRUPTION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_INTERRUPTION:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 41
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    const-string v1, "QRCS_ERROR_MISSING_PARAM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_MISSING_PARAM:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 43
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    const-string v1, "QRCS_ERROR_SIP_INVITE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_SIP_INVITE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 45
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    const-string v1, "QRCS_ERROR_SIP_MESSAGE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_SIP_MESSAGE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 47
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    const-string v1, "QRCS_ERROR_SIP_REFER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_SIP_REFER:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 49
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    const-string v1, "QRCS_ERROR_SIP_CANCEL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_SIP_CANCEL:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 51
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    const-string v1, "QRCS_ERROR_MSRP"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_MSRP:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 53
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    const-string v1, "QRCS_ERROR_MAX32"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_MAX32:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 54
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    const-string v1, "QRCS_UNKNOWN"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_UNKNOWN:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 22
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    sget-object v1, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_UNKNOWN:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_USER_CANCEL:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_NETWORK_LOST:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_FILE_NOT_PRESENT:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_EXCEED_MAX_SIZE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_ICON_EXCEED_MAX_SIZE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_INTERRUPTION:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_MISSING_PARAM:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_SIP_INVITE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_SIP_MESSAGE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_SIP_REFER:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_SIP_CANCEL:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_MSRP:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_MAX32:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_UNKNOWN:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->$VALUES:[Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->$VALUES:[Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    return-object v0
.end method
