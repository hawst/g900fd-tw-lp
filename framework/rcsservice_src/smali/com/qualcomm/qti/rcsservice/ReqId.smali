.class public Lcom/qualcomm/qti/rcsservice/ReqId;
.super Ljava/lang/Object;
.source "ReqId.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/ReqId;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 156
    new-instance v0, Lcom/qualcomm/qti/rcsservice/ReqId$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/ReqId$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/ReqId;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/ReqId;->readFromParcel(Landroid/os/Parcel;)V

    .line 171
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/ReqId$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/ReqId$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/ReqId;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 154
    return-void

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    return v0
.end method

.method public getEnumValueAsInt()I
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->ordinal()I

    move-result v0

    return v0
.end method

.method public getReqIdValue()Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 176
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :goto_0
    return-void

    .line 177
    :catch_0
    move-exception v0

    .line 178
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_SESSION_UNKNOWN:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0
.end method

.method public setReqIdValue(I)V
    .locals 3
    .param p1, "eReqId"    # I

    .prologue
    .line 68
    packed-switch p1, :pswitch_data_0

    .line 125
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_SESSION_UNKNOWN:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    .line 129
    :goto_0
    return-void

    .line 71
    :pswitch_0
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_IMSESSION_ACCEPT:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 74
    :pswitch_1
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_IMSESSION_REJECT:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 77
    :pswitch_2
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_IMSESSION_CANCEL:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 80
    :pswitch_3
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_IMSESSION_ADDPATICIPANTS:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 83
    :pswitch_4
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_IMSESSION_SENDTEXTMESSAGE:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 86
    :pswitch_5
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_IMSESSION_SENDMMMESSAGE:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 89
    :pswitch_6
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_IMSESSION_SETISCOMPOSINGSTATUS:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 92
    :pswitch_7
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_IMSESSION_SENDDISPLAYSTATUS:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 95
    :pswitch_8
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_IMSESSION_CLOSESESSION:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 98
    :pswitch_9
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_FTSESSION_ACCEPT:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 101
    :pswitch_a
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_FTSESSION_REJECT:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 104
    :pswitch_b
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_FTSESSION_CANCEL:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 107
    :pswitch_c
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_SMSERVICE_SENDSTANDALONEMESSAGE:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 110
    :pswitch_d
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_SMSERVICE_SENDDISPLAYSTATUS:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 113
    :pswitch_e
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_SMSERVICE_ACCEPTINVITATION:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 116
    :pswitch_f
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_SMSERVICE_REJECTNVITATION:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 119
    :pswitch_10
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_SMSERVICE_CANCELNVITATION:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 122
    :pswitch_11
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;->QRCS_SESSION_MAX32:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqId;->eReqId:Lcom/qualcomm/qti/rcsservice/ReqId$QRCS_REQ_ID;

    goto :goto_0

    .line 68
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/ReqId;->writeToParcel(Landroid/os/Parcel;)V

    .line 149
    return-void
.end method
