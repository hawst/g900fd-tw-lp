.class public Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;
.super Ljava/lang/Object;
.source "IncomingContactInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private assertedIdentity:Ljava/lang/String;

.field private deviceID:Ljava/lang/String;

.field private from:Ljava/lang/String;

.field private imdnRecordRoute:Ljava/lang/String;

.field private originalTo:Ljava/lang/String;

.field private referredBy:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->referredBy:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->assertedIdentity:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->from:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->imdnRecordRoute:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->deviceID:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->originalTo:Ljava/lang/String;

    .line 33
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->referredBy:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->assertedIdentity:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->from:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->imdnRecordRoute:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->deviceID:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->originalTo:Ljava/lang/String;

    .line 62
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 63
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/IncomingContactInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/IncomingContactInfo$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public getAssertedIdentity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->assertedIdentity:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->deviceID:Ljava/lang/String;

    return-object v0
.end method

.method public getFrom()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->from:Ljava/lang/String;

    return-object v0
.end method

.method public getImdnRecordRoute()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->imdnRecordRoute:Ljava/lang/String;

    return-object v0
.end method

.method public getOriginalTo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->originalTo:Ljava/lang/String;

    return-object v0
.end method

.method public getReferredBy()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->referredBy:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->referredBy:Ljava/lang/String;

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->assertedIdentity:Ljava/lang/String;

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->from:Ljava/lang/String;

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->imdnRecordRoute:Ljava/lang/String;

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->deviceID:Ljava/lang/String;

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->originalTo:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setAssertedIdentity(Ljava/lang/String;)V
    .locals 0
    .param p1, "assertedIdentity"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->assertedIdentity:Ljava/lang/String;

    .line 116
    return-void
.end method

.method public setDeviceID(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceID"    # Ljava/lang/String;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->deviceID:Ljava/lang/String;

    .line 179
    return-void
.end method

.method public setFrom(Ljava/lang/String;)V
    .locals 0
    .param p1, "from"    # Ljava/lang/String;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->from:Ljava/lang/String;

    .line 137
    return-void
.end method

.method public setImdnRecordRoute(Ljava/lang/String;)V
    .locals 0
    .param p1, "imdnRecordRoute"    # Ljava/lang/String;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->imdnRecordRoute:Ljava/lang/String;

    .line 158
    return-void
.end method

.method public setOriginalTo(Ljava/lang/String;)V
    .locals 0
    .param p1, "originalTo"    # Ljava/lang/String;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->originalTo:Ljava/lang/String;

    .line 200
    return-void
.end method

.method public setReferredBy(Ljava/lang/String;)V
    .locals 0
    .param p1, "referredBy"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->referredBy:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 40
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->referredBy:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->assertedIdentity:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->from:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->imdnRecordRoute:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->deviceID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;->originalTo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    return-void
.end method
