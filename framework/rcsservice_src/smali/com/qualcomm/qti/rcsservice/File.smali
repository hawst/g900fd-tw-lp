.class public Lcom/qualcomm/qti/rcsservice/File;
.super Ljava/lang/Object;
.source "File.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/File;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private contentType:Ljava/lang/String;

.field private dateTime:Ljava/lang/String;

.field private fileHash:Ljava/lang/String;

.field private fileName:Ljava/lang/String;

.field private filePath:Ljava/lang/String;

.field private startOffset:I

.field private transferID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/qualcomm/qti/rcsservice/File$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/File$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/File;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->fileName:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->filePath:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->contentType:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->dateTime:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->transferID:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->fileHash:Ljava/lang/String;

    .line 34
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->fileName:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->filePath:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->contentType:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->dateTime:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->transferID:Ljava/lang/String;

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->fileHash:Ljava/lang/String;

    .line 63
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/File;->readFromParcel(Landroid/os/Parcel;)V

    .line 64
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/File$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/File$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/File;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->contentType:Ljava/lang/String;

    return-object v0
.end method

.method public getDateTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->dateTime:Ljava/lang/String;

    return-object v0
.end method

.method public getFileHash()Ljava/lang/String;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->fileHash:Ljava/lang/String;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->fileName:Ljava/lang/String;

    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->filePath:Ljava/lang/String;

    return-object v0
.end method

.method public getStartOffset()I
    .locals 1

    .prologue
    .line 211
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/File;->startOffset:I

    return v0
.end method

.method public getTransferID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->transferID:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->fileName:Ljava/lang/String;

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->filePath:Ljava/lang/String;

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->contentType:Ljava/lang/String;

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->dateTime:Ljava/lang/String;

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->transferID:Ljava/lang/String;

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->fileHash:Ljava/lang/String;

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsservice/File;->startOffset:I

    .line 74
    return-void
.end method

.method public setContentType(Ljava/lang/String;)V
    .locals 0
    .param p1, "contentType"    # Ljava/lang/String;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/File;->contentType:Ljava/lang/String;

    .line 139
    return-void
.end method

.method public setDateTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "dateTime"    # Ljava/lang/String;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/File;->dateTime:Ljava/lang/String;

    .line 160
    return-void
.end method

.method public setFileHash(Ljava/lang/String;)V
    .locals 0
    .param p1, "fileHash"    # Ljava/lang/String;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/File;->fileHash:Ljava/lang/String;

    .line 202
    return-void
.end method

.method public setFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/File;->fileName:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public setFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/File;->filePath:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public setStartOffset(I)V
    .locals 0
    .param p1, "startOffset"    # I

    .prologue
    .line 222
    iput p1, p0, Lcom/qualcomm/qti/rcsservice/File;->startOffset:I

    .line 223
    return-void
.end method

.method public setTransferID(Ljava/lang/String;)V
    .locals 0
    .param p1, "transferID"    # Ljava/lang/String;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/File;->transferID:Ljava/lang/String;

    .line 181
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 41
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->fileName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->filePath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->contentType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->dateTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->transferID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/File;->fileHash:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/File;->startOffset:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 48
    return-void
.end method
