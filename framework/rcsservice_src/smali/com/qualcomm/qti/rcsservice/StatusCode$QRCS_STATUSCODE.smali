.class public final enum Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;
.super Ljava/lang/Enum;
.source "StatusCode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsservice/StatusCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QRCS_STATUSCODE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

.field public static final enum QRCS_FAILURE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

.field public static final enum QRCS_FETCH_ERROR:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

.field public static final enum QRCS_INSUFFICIENT_MEMORY:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

.field public static final enum QRCS_INVALID_LISTENER_HANDLE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

.field public static final enum QRCS_INVALID_PARAM:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

.field public static final enum QRCS_INVALID_SERVICE_HANDLE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

.field public static final enum QRCS_LOST_NET:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

.field public static final enum QRCS_NOT_FOUND:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

.field public static final enum QRCS_NOT_SUPPORTED:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

.field public static final enum QRCS_REQUEST_TIMEOUT:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

.field public static final enum QRCS_SERVICE_UNAVAILABLE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

.field public static final enum QRCS_SERVICE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

.field public static final enum QRCS_SUCCESS:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

.field public static final enum QRCS_SUCCESS_ASYC_UPDATE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    const-string v1, "QRCS_SUCCESS"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_SUCCESS:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    .line 23
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    const-string v1, "QRCS_FAILURE"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_FAILURE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    .line 25
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    const-string v1, "QRCS_SUCCESS_ASYC_UPDATE"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_SUCCESS_ASYC_UPDATE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    .line 27
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    const-string v1, "QRCS_INVALID_SERVICE_HANDLE"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_INVALID_SERVICE_HANDLE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    .line 29
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    const-string v1, "QRCS_INVALID_LISTENER_HANDLE"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_INVALID_LISTENER_HANDLE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    .line 31
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    const-string v1, "QRCS_INVALID_PARAM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_INVALID_PARAM:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    .line 33
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    const-string v1, "QRCS_FETCH_ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_FETCH_ERROR:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    .line 35
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    const-string v1, "QRCS_REQUEST_TIMEOUT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_REQUEST_TIMEOUT:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    .line 37
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    const-string v1, "QRCS_INSUFFICIENT_MEMORY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_INSUFFICIENT_MEMORY:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    .line 39
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    const-string v1, "QRCS_LOST_NET"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_LOST_NET:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    .line 41
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    const-string v1, "QRCS_NOT_SUPPORTED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_NOT_SUPPORTED:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    .line 43
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    const-string v1, "QRCS_NOT_FOUND"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_NOT_FOUND:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    .line 45
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    const-string v1, "QRCS_SERVICE_UNAVAILABLE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_SERVICE_UNAVAILABLE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    .line 47
    new-instance v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    const-string v1, "QRCS_SERVICE_UNKNOWN"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_SERVICE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    .line 19
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    sget-object v1, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_SUCCESS:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_FAILURE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_SUCCESS_ASYC_UPDATE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_INVALID_SERVICE_HANDLE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_INVALID_LISTENER_HANDLE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_INVALID_PARAM:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_FETCH_ERROR:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_REQUEST_TIMEOUT:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_INSUFFICIENT_MEMORY:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_LOST_NET:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_NOT_SUPPORTED:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_NOT_FOUND:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_SERVICE_UNAVAILABLE:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->QRCS_SERVICE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->$VALUES:[Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->$VALUES:[Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/rcsservice/StatusCode$QRCS_STATUSCODE;

    return-object v0
.end method
