.class public Lcom/qualcomm/qti/rcsservice/CopyControlType;
.super Ljava/lang/Object;
.source "CopyControlType.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/CopyControlType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private eCopyControlType:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    new-instance v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/CopyControlType$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/CopyControlType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/CopyControlType;->readFromParcel(Landroid/os/Parcel;)V

    .line 68
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/CopyControlType$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/CopyControlType$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/CopyControlType;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public getCopyControlTypeValue()Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/CopyControlType;->eCopyControlType:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    return-object v0
.end method

.method public getEnumValueAsInt()I
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/CopyControlType;->eCopyControlType:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->ordinal()I

    move-result v0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 74
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/CopyControlType;->eCopyControlType:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    return-void

    .line 76
    :catch_0
    move-exception v0

    .line 77
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_UNKNOWN:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/CopyControlType;->eCopyControlType:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    goto :goto_0
.end method

.method public setCopyControlTypeValue(I)V
    .locals 3
    .param p1, "eCopyControlType"    # I

    .prologue
    .line 111
    packed-switch p1, :pswitch_data_0

    .line 129
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    sget-object v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_UNKNOWN:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/CopyControlType;->eCopyControlType:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    .line 133
    :goto_0
    return-void

    .line 114
    :pswitch_0
    sget-object v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_TYPE_INVALID:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/CopyControlType;->eCopyControlType:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    goto :goto_0

    .line 117
    :pswitch_1
    sget-object v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_TO:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/CopyControlType;->eCopyControlType:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    goto :goto_0

    .line 120
    :pswitch_2
    sget-object v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_CC:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/CopyControlType;->eCopyControlType:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    goto :goto_0

    .line 123
    :pswitch_3
    sget-object v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_BCC:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/CopyControlType;->eCopyControlType:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    goto :goto_0

    .line 126
    :pswitch_4
    sget-object v0, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->QRCS_COPY_CONTROL_MAX32:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/CopyControlType;->eCopyControlType:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    goto :goto_0

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 50
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/CopyControlType;->eCopyControlType:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 52
    return-void

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/CopyControlType;->eCopyControlType:Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/CopyControlType$QRCS_COPY_CONTROL_TYPE;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
