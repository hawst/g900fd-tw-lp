.class public Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;
.super Ljava/lang/Object;
.source "IdHeaderInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private contributionID:Ljava/lang/String;

.field private conversationID:Ljava/lang/String;

.field private inReplyToContributionID:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->conversationID:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->contributionID:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->inReplyToContributionID:Ljava/lang/String;

    .line 30
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->conversationID:Ljava/lang/String;

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->contributionID:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->inReplyToContributionID:Ljava/lang/String;

    .line 55
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 56
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/IdHeaderInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/IdHeaderInfo$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    return v0
.end method

.method public getContributionID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->contributionID:Ljava/lang/String;

    return-object v0
.end method

.method public getConversationID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->conversationID:Ljava/lang/String;

    return-object v0
.end method

.method public getInReplyToContributionID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->inReplyToContributionID:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->conversationID:Ljava/lang/String;

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->contributionID:Ljava/lang/String;

    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->inReplyToContributionID:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public setContributionID(Ljava/lang/String;)V
    .locals 0
    .param p1, "contributionID"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->contributionID:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public setConversationID(Ljava/lang/String;)V
    .locals 0
    .param p1, "conversationID"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->conversationID:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public setInReplyToContributionID(Ljava/lang/String;)V
    .locals 0
    .param p1, "inReplyToContributionID"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->inReplyToContributionID:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 37
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->conversationID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->contributionID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;->inReplyToContributionID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 40
    return-void
.end method
