.class public Lcom/qualcomm/qti/rcsservice/ReqStatus;
.super Ljava/lang/Object;
.source "ReqStatus.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/ReqStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private errorDesc:Ljava/lang/String;

.field private idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

.field private imdnReqInfo:Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

.field private msgType:Lcom/qualcomm/qti/rcsservice/MsgType;

.field private reqId:Lcom/qualcomm/qti/rcsservice/ReqId;

.field private reqUserData:I

.field private statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/qualcomm/qti/rcsservice/ReqStatus$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/ReqStatus$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->errorDesc:Ljava/lang/String;

    .line 34
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->errorDesc:Ljava/lang/String;

    .line 65
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/ReqStatus;->readFromParcel(Landroid/os/Parcel;)V

    .line 66
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/ReqStatus$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/ReqStatus$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/ReqStatus;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method

.method public getErrorDesc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->errorDesc:Ljava/lang/String;

    return-object v0
.end method

.method public getIdHeaderInfo()Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    return-object v0
.end method

.method public getImdnReqInfo()Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->imdnReqInfo:Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    return-object v0
.end method

.method public getMsgType()Lcom/qualcomm/qti/rcsservice/MsgType;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->msgType:Lcom/qualcomm/qti/rcsservice/MsgType;

    return-object v0
.end method

.method public getReqId()Lcom/qualcomm/qti/rcsservice/ReqId;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->reqId:Lcom/qualcomm/qti/rcsservice/ReqId;

    return-object v0
.end method

.method public getReqUserData()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->reqUserData:I

    return v0
.end method

.method public getStatusCode()Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 69
    const-class v0, Lcom/qualcomm/qti/rcsservice/ReqId;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/ReqId;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->reqId:Lcom/qualcomm/qti/rcsservice/ReqId;

    .line 70
    const-class v0, Lcom/qualcomm/qti/rcsservice/StatusCode;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/StatusCode;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 71
    const-class v0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .line 72
    const-class v0, Lcom/qualcomm/qti/rcsservice/MsgType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/MsgType;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->msgType:Lcom/qualcomm/qti/rcsservice/MsgType;

    .line 73
    const-class v0, Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->imdnReqInfo:Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->errorDesc:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->reqUserData:I

    .line 76
    return-void
.end method

.method public setErrorDesc(Ljava/lang/String;)V
    .locals 0
    .param p1, "errorDesc"    # Ljava/lang/String;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->errorDesc:Ljava/lang/String;

    .line 162
    return-void
.end method

.method public setIdHeaderInfo(Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;)V
    .locals 0
    .param p1, "idHeaderInfo"    # Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .line 141
    return-void
.end method

.method public setImdnReqInfo(Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;)V
    .locals 0
    .param p1, "imdnReqInfo"    # Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    .prologue
    .line 224
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->imdnReqInfo:Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    .line 225
    return-void
.end method

.method public setMsgType(Lcom/qualcomm/qti/rcsservice/MsgType;)V
    .locals 0
    .param p1, "msgType"    # Lcom/qualcomm/qti/rcsservice/MsgType;

    .prologue
    .line 203
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->msgType:Lcom/qualcomm/qti/rcsservice/MsgType;

    .line 204
    return-void
.end method

.method public setReqId(Lcom/qualcomm/qti/rcsservice/ReqId;)V
    .locals 0
    .param p1, "reqId"    # Lcom/qualcomm/qti/rcsservice/ReqId;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->reqId:Lcom/qualcomm/qti/rcsservice/ReqId;

    .line 99
    return-void
.end method

.method public setReqUserData(I)V
    .locals 0
    .param p1, "reqUserData"    # I

    .prologue
    .line 182
    iput p1, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->reqUserData:I

    .line 183
    return-void
.end method

.method public setStatusCode(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .locals 0
    .param p1, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 120
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 42
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->reqId:Lcom/qualcomm/qti/rcsservice/ReqId;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 44
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 45
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->msgType:Lcom/qualcomm/qti/rcsservice/MsgType;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 46
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->imdnReqInfo:Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 47
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->errorDesc:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 48
    iget v0, p0, Lcom/qualcomm/qti/rcsservice/ReqStatus;->reqUserData:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 49
    return-void
.end method
