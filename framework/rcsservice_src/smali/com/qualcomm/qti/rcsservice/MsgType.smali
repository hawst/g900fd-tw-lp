.class public Lcom/qualcomm/qti/rcsservice/MsgType;
.super Ljava/lang/Object;
.source "MsgType.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/MsgType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field eMsgType:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/qualcomm/qti/rcsservice/MsgType$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/MsgType$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/MsgType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/MsgType;->readFromParcel(Landroid/os/Parcel;)V

    .line 67
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/MsgType$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/MsgType$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/MsgType;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public getEnumValueAsInt()I
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/MsgType;->eMsgType:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->ordinal()I

    move-result v0

    return v0
.end method

.method public getMsgTypeValue()Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/MsgType;->eMsgType:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 73
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/MsgType;->eMsgType:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :goto_0
    return-void

    .line 75
    :catch_0
    move-exception v0

    .line 76
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_SM_TYPE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/MsgType;->eMsgType:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    goto :goto_0
.end method

.method public setMsgTypeValue(I)V
    .locals 3
    .param p1, "eMsgType"    # I

    .prologue
    .line 110
    packed-switch p1, :pswitch_data_0

    .line 131
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    sget-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_SM_TYPE_UNKNOWN:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/MsgType;->eMsgType:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    .line 135
    :goto_0
    return-void

    .line 113
    :pswitch_0
    sget-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_SM_TYPE_NONE:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/MsgType;->eMsgType:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    goto :goto_0

    .line 116
    :pswitch_1
    sget-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_MSG_TYPE_SM_PAGER_MODE:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/MsgType;->eMsgType:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    goto :goto_0

    .line 119
    :pswitch_2
    sget-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_MSG_TYPE_SM_LARGE_MSG_MODE:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/MsgType;->eMsgType:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    goto :goto_0

    .line 122
    :pswitch_3
    sget-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_MSG_TYPE_SESSION_CHAT:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/MsgType;->eMsgType:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    goto :goto_0

    .line 125
    :pswitch_4
    sget-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_MSG_TYPE_SESSION_FT:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/MsgType;->eMsgType:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    goto :goto_0

    .line 128
    :pswitch_5
    sget-object v0, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->QRCS_SM_MSG_MAX32:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/MsgType;->eMsgType:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    goto :goto_0

    .line 110
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 49
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/MsgType;->eMsgType:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 51
    return-void

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/MsgType;->eMsgType:Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/MsgType$QRCS_MSG_TYPE;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
