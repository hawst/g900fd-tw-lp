.class public Lcom/qualcomm/qti/rcsservice/Error;
.super Ljava/lang/Object;
.source "Error.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/Error;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    new-instance v0, Lcom/qualcomm/qti/rcsservice/Error$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/Error$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/Error;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/Error;->readFromParcel(Landroid/os/Parcel;)V

    .line 89
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/Error$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/Error$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/Error;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public getEnumValueAsInt()I
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->ordinal()I

    move-result v0

    return v0
.end method

.method public getErrorValue()Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 95
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_UNKNOWN:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    goto :goto_0
.end method

.method public setErrorValue(I)V
    .locals 3
    .param p1, "eError"    # I

    .prologue
    .line 132
    packed-switch p1, :pswitch_data_0

    .line 177
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_UNKNOWN:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    .line 181
    :goto_0
    return-void

    .line 135
    :pswitch_0
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_UNKNOWN:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    goto :goto_0

    .line 138
    :pswitch_1
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_USER_CANCEL:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    goto :goto_0

    .line 141
    :pswitch_2
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_NETWORK_LOST:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    goto :goto_0

    .line 144
    :pswitch_3
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_FILE_NOT_PRESENT:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    goto :goto_0

    .line 147
    :pswitch_4
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_EXCEED_MAX_SIZE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    goto :goto_0

    .line 150
    :pswitch_5
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_ICON_EXCEED_MAX_SIZE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    goto :goto_0

    .line 153
    :pswitch_6
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_INTERRUPTION:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    goto :goto_0

    .line 156
    :pswitch_7
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_MISSING_PARAM:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    goto :goto_0

    .line 159
    :pswitch_8
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_SIP_INVITE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    goto :goto_0

    .line 162
    :pswitch_9
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_SIP_MESSAGE:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    goto :goto_0

    .line 165
    :pswitch_a
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_SIP_REFER:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    goto :goto_0

    .line 168
    :pswitch_b
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_SIP_CANCEL:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    goto :goto_0

    .line 171
    :pswitch_c
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_MSRP:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    goto :goto_0

    .line 174
    :pswitch_d
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->QRCS_ERROR_MAX32:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    goto :goto_0

    .line 132
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 71
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 73
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/Error;->eError:Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/Error$QRCS_ERROR;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
