.class public Lcom/qualcomm/qti/rcsservice/DeliveryStatus;
.super Ljava/lang/Object;
.source "DeliveryStatus.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsservice/DeliveryStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->readFromParcel(Landroid/os/Parcel;)V

    .line 77
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsservice/DeliveryStatus$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsservice/DeliveryStatus$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method public getDeliveryStatusValue()Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    return-object v0
.end method

.method public getEnumValueAsInt()I
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;->ordinal()I

    move-result v0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 83
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :goto_0
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;->QRCS_DS_UNKNOWN:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    goto :goto_0
.end method

.method public setDeliveryStatusValue(I)V
    .locals 3
    .param p1, "eDeliveryStatus"    # I

    .prologue
    .line 120
    packed-switch p1, :pswitch_data_0

    .line 153
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;->QRCS_DS_UNKNOWN:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    .line 157
    :goto_0
    return-void

    .line 123
    :pswitch_0
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;->QRCS_DS_NONE:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    goto :goto_0

    .line 126
    :pswitch_1
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;->QRCS_DS_DELIVERED:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    goto :goto_0

    .line 129
    :pswitch_2
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;->QRCS_DS_DELIVERY_FAILED:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    goto :goto_0

    .line 132
    :pswitch_3
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;->QRCS_DS_DELIVERY_FORBIDDEN:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    goto :goto_0

    .line 135
    :pswitch_4
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;->QRCS_DS_DELIVERY_ERROR:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    goto :goto_0

    .line 138
    :pswitch_5
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;->QRCS_DS_DISPLAYED:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    goto :goto_0

    .line 141
    :pswitch_6
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;->QRCS_DS_DISPLAY_FORBIDDEN:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    goto :goto_0

    .line 144
    :pswitch_7
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;->QRCS_DS_DISPLAY_ERROR:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    goto :goto_0

    .line 147
    :pswitch_8
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;->QRCS_DS_SENT:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    goto :goto_0

    .line 150
    :pswitch_9
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;->QRCS_DS_MAX32:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    goto :goto_0

    .line 120
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 61
    return-void

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/rcsservice/DeliveryStatus;->eDeliveryStatus:Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsservice/DeliveryStatus$QRCS_DELIVERY_STATUS;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
