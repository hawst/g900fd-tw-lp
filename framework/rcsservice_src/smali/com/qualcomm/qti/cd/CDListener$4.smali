.class Lcom/qualcomm/qti/cd/CDListener$4;
.super Ljava/lang/Thread;
.source "CDListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/qti/cd/CDListener;->QCDServiceListener_CDUpdate(Lcom/qualcomm/qti/rcsservice/StatusCode;Ljava/lang/String;Lcom/qualcomm/qti/rcsservice/CDInfo;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/cd/CDListener;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/cd/CDListener;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/qualcomm/qti/cd/CDListener$4;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 132
    :try_start_0
    const-string v0, "AIDL"

    const-string v1, "  QCDServiceListener_CDUpdate start inside thread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-object v0, p0, Lcom/qualcomm/qti/cd/CDListener$4;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget-object v0, v0, Lcom/qualcomm/qti/cd/CDListener;->m_QCDServiceListener:Lcom/qualcomm/qti/cd/IQCDServiceListener;

    iget-object v1, p0, Lcom/qualcomm/qti/cd/CDListener$4;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget-object v1, v1, Lcom/qualcomm/qti/cd/CDListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    iget-object v2, p0, Lcom/qualcomm/qti/cd/CDListener$4;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget-object v2, v2, Lcom/qualcomm/qti/cd/CDListener;->m_URI:Ljava/lang/String;

    iget-object v3, p0, Lcom/qualcomm/qti/cd/CDListener$4;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget-object v3, v3, Lcom/qualcomm/qti/cd/CDListener;->m_cdInfo:Lcom/qualcomm/qti/rcsservice/CDInfo;

    iget-object v4, p0, Lcom/qualcomm/qti/cd/CDListener$4;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget v4, v4, Lcom/qualcomm/qti/cd/CDListener;->m_responseCode:I

    iget-object v5, p0, Lcom/qualcomm/qti/cd/CDListener$4;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget v5, v5, Lcom/qualcomm/qti/cd/CDListener;->m_userData:I

    invoke-interface/range {v0 .. v5}, Lcom/qualcomm/qti/cd/IQCDServiceListener;->QCDServiceListener_CDUpdate(Lcom/qualcomm/qti/rcsservice/StatusCode;Ljava/lang/String;Lcom/qualcomm/qti/rcsservice/CDInfo;II)V

    .line 135
    const-string v0, "AIDL"

    const-string v1, "  QCDServiceListener_CDUpdate end inside thread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :goto_0
    return-void

    .line 136
    :catch_0
    move-exception v6

    .line 138
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
