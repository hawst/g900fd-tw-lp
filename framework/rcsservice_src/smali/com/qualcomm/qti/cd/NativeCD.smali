.class public Lcom/qualcomm/qti/cd/NativeCD;
.super Ljava/lang/Object;
.source "NativeCD.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native QCDServiceAddListener(ILcom/qualcomm/qti/cd/IQCDServiceListener;)J
.end method

.method public native QCDServiceGetMyCdInfo(ILcom/qualcomm/qti/rcsservice/CDInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QCDServiceGetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QCDServiceRemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QCDServiceRequestCdInfo(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QCDServiceRequestGivenContactsCDInfo(I[Ljava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QCDServiceRequestStatusInfo(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QCDServiceSetMyCdInfo(ILcom/qualcomm/qti/rcsservice/CDInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QCDServiceSetMyCdStatusInfo(ILjava/lang/String;Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method
