.class Lcom/qualcomm/qti/cd/CDListener$1;
.super Ljava/lang/Thread;
.source "CDListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/qti/cd/CDListener;->QCDServiceListener_ServiceCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/cd/CDListener;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/cd/CDListener;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/qualcomm/qti/cd/CDListener$1;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 52
    :try_start_0
    const-string v1, "AIDL"

    const-string v2, "   QCDServiceListener_ServiceCreated  start inside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    iget-object v1, p0, Lcom/qualcomm/qti/cd/CDListener$1;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget-object v1, v1, Lcom/qualcomm/qti/cd/CDListener;->m_QCDServiceListener:Lcom/qualcomm/qti/cd/IQCDServiceListener;

    iget-object v2, p0, Lcom/qualcomm/qti/cd/CDListener$1;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget-wide v2, v2, Lcom/qualcomm/qti/cd/CDListener;->m_cdServiceListenerUserData:J

    iget-object v4, p0, Lcom/qualcomm/qti/cd/CDListener$1;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget-object v4, v4, Lcom/qualcomm/qti/cd/CDListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    iget-object v5, p0, Lcom/qualcomm/qti/cd/CDListener$1;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget v5, v5, Lcom/qualcomm/qti/cd/CDListener;->m_cdServiceHandle:I

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/qualcomm/qti/cd/IQCDServiceListener;->QCDServiceListener_ServiceCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;I)V

    .line 55
    const-string v1, "AIDL"

    const-string v2, "   QCDServiceListener_ServiceCreated  end inside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :goto_0
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
