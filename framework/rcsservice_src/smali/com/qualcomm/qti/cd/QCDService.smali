.class public Lcom/qualcomm/qti/cd/QCDService;
.super Lcom/qualcomm/qti/cd/IQCDService$Stub;
.source "QCDService.java"


# instance fields
.field private objNativeCD:Lcom/qualcomm/qti/cd/NativeCD;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/qualcomm/qti/cd/IQCDService$Stub;-><init>()V

    .line 21
    new-instance v0, Lcom/qualcomm/qti/cd/NativeCD;

    invoke-direct {v0}, Lcom/qualcomm/qti/cd/NativeCD;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/cd/QCDService;->objNativeCD:Lcom/qualcomm/qti/cd/NativeCD;

    return-void
.end method


# virtual methods
.method public QCDService_AddListener(ILcom/qualcomm/qti/cd/IQCDServiceListener;)J
    .locals 2
    .param p1, "cdServiceHandle"    # I
    .param p2, "cdServiceListener"    # Lcom/qualcomm/qti/cd/IQCDServiceListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/qualcomm/qti/cd/QCDService;->objNativeCD:Lcom/qualcomm/qti/cd/NativeCD;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/cd/NativeCD;->QCDServiceAddListener(ILcom/qualcomm/qti/cd/IQCDServiceListener;)J

    move-result-wide v0

    return-wide v0
.end method

.method public QCDService_GetMyCdInfo(ILcom/qualcomm/qti/rcsservice/CDInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "cdServiceHandle"    # I
    .param p2, "myCdInfo"    # Lcom/qualcomm/qti/rcsservice/CDInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/qualcomm/qti/cd/QCDService;->objNativeCD:Lcom/qualcomm/qti/cd/NativeCD;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/cd/NativeCD;->QCDServiceGetMyCdInfo(ILcom/qualcomm/qti/rcsservice/CDInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QCDService_GetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "cdServiceHandle"    # I
    .param p2, "versionInfo"    # Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/qualcomm/qti/cd/QCDService;->objNativeCD:Lcom/qualcomm/qti/cd/NativeCD;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/cd/NativeCD;->QCDServiceGetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QCDService_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 2
    .param p1, "cdServiceHandle"    # I
    .param p2, "cdServiceUserData"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/qualcomm/qti/cd/QCDService;->objNativeCD:Lcom/qualcomm/qti/cd/NativeCD;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/cd/NativeCD;->QCDServiceRemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QCDService_RequestCdInfo(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "cdServiceHandle"    # I
    .param p2, "remoteURI"    # Ljava/lang/String;
    .param p3, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/qualcomm/qti/cd/QCDService;->objNativeCD:Lcom/qualcomm/qti/cd/NativeCD;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/cd/NativeCD;->QCDServiceRequestCdInfo(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QCDService_RequestGivenContactsCDInfo(I[Ljava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "cdServiceHandle"    # I
    .param p2, "remoteURIList"    # [Ljava/lang/String;
    .param p3, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/qualcomm/qti/cd/QCDService;->objNativeCD:Lcom/qualcomm/qti/cd/NativeCD;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/cd/NativeCD;->QCDServiceRequestGivenContactsCDInfo(I[Ljava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QCDService_RequestStatusInfo(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "cdServiceHandle"    # I
    .param p2, "remoteURI"    # Ljava/lang/String;
    .param p3, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/qualcomm/qti/cd/QCDService;->objNativeCD:Lcom/qualcomm/qti/cd/NativeCD;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/cd/NativeCD;->QCDServiceRequestStatusInfo(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QCDService_SetMyCdInfo(ILcom/qualcomm/qti/rcsservice/CDInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "cdServiceHandle"    # I
    .param p2, "myCdInfo"    # Lcom/qualcomm/qti/rcsservice/CDInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/qualcomm/qti/cd/QCDService;->objNativeCD:Lcom/qualcomm/qti/cd/NativeCD;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/cd/NativeCD;->QCDServiceSetMyCdInfo(ILcom/qualcomm/qti/rcsservice/CDInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QCDService_SetMyCdStatusInfo(ILjava/lang/String;Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "cdServiceHandle"    # I
    .param p2, "statusString"    # Ljava/lang/String;
    .param p3, "customString"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/qualcomm/qti/cd/QCDService;->objNativeCD:Lcom/qualcomm/qti/cd/NativeCD;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/cd/NativeCD;->QCDServiceSetMyCdStatusInfo(ILjava/lang/String;Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public getCDServiceImpl()Lcom/qualcomm/qti/cd/IQCDService$Stub;
    .locals 0

    .prologue
    .line 24
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 87
    :try_start_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/qualcomm/qti/cd/IQCDService$Stub;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 88
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v1, "AIDL_QCDService"

    const-string v2, "Unexpected remote exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 91
    throw v0
.end method
