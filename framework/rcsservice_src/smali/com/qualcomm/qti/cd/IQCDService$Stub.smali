.class public abstract Lcom/qualcomm/qti/cd/IQCDService$Stub;
.super Landroid/os/Binder;
.source "IQCDService.java"

# interfaces
.implements Lcom/qualcomm/qti/cd/IQCDService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/cd/IQCDService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/cd/IQCDService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.cd.IQCDService"

.field static final TRANSACTION_QCDService_AddListener:I = 0x2

.field static final TRANSACTION_QCDService_GetMyCdInfo:I = 0x7

.field static final TRANSACTION_QCDService_GetVersion:I = 0x1

.field static final TRANSACTION_QCDService_RemoveListener:I = 0x3

.field static final TRANSACTION_QCDService_RequestCdInfo:I = 0x8

.field static final TRANSACTION_QCDService_RequestGivenContactsCDInfo:I = 0x9

.field static final TRANSACTION_QCDService_RequestStatusInfo:I = 0x6

.field static final TRANSACTION_QCDService_SetMyCdInfo:I = 0x4

.field static final TRANSACTION_QCDService_SetMyCdStatusInfo:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.cd.IQCDService"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/cd/IQCDService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/cd/IQCDService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.cd.IQCDService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/cd/IQCDService;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/cd/IQCDService;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/cd/IQCDService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/cd/IQCDService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 241
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 44
    :sswitch_0
    const-string v7, "com.qualcomm.qti.cd.IQCDService"

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v7, "com.qualcomm.qti.cd.IQCDService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 53
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_0

    .line 54
    sget-object v7, Lcom/qualcomm/qti/rcsservice/VersionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/VersionInfo;

    .line 59
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    :goto_1
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/qti/cd/IQCDService$Stub;->QCDService_GetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 60
    .local v4, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 61
    if-eqz v4, :cond_1

    .line 62
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 63
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 68
    :goto_2
    if-eqz v2, :cond_2

    .line 69
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    invoke-virtual {v2, p3, v6}, Lcom/qualcomm/qti/rcsservice/VersionInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 57
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    goto :goto_1

    .line 66
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    .line 73
    :cond_2
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 79
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_2
    const-string v7, "com.qualcomm.qti.cd.IQCDService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 83
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Lcom/qualcomm/qti/cd/IQCDServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/cd/IQCDServiceListener;

    move-result-object v2

    .line 84
    .local v2, "_arg1":Lcom/qualcomm/qti/cd/IQCDServiceListener;
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/qti/cd/IQCDService$Stub;->QCDService_AddListener(ILcom/qualcomm/qti/cd/IQCDServiceListener;)J

    move-result-wide v4

    .line 85
    .local v4, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 86
    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    goto :goto_0

    .line 91
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/cd/IQCDServiceListener;
    .end local v4    # "_result":J
    :sswitch_3
    const-string v7, "com.qualcomm.qti.cd.IQCDService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 95
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 96
    .local v2, "_arg1":J
    invoke-virtual {p0, v0, v2, v3}, Lcom/qualcomm/qti/cd/IQCDService$Stub;->QCDService_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 97
    .local v4, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 98
    if-eqz v4, :cond_3

    .line 99
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 100
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 103
    :cond_3
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 109
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":J
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_4
    const-string v7, "com.qualcomm.qti.cd.IQCDService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 111
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 113
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_4

    .line 114
    sget-object v7, Lcom/qualcomm/qti/rcsservice/CDInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/CDInfo;

    .line 119
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/CDInfo;
    :goto_3
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/qti/cd/IQCDService$Stub;->QCDService_SetMyCdInfo(ILcom/qualcomm/qti/rcsservice/CDInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 120
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 121
    if-eqz v4, :cond_5

    .line 122
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 123
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 117
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/CDInfo;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/CDInfo;
    goto :goto_3

    .line 126
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_5
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 132
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/CDInfo;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_5
    const-string v7, "com.qualcomm.qti.cd.IQCDService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 134
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 136
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 138
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 139
    .local v1, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/cd/IQCDService$Stub;->QCDService_SetMyCdStatusInfo(ILjava/lang/String;Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 140
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 141
    if-eqz v4, :cond_6

    .line 142
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 143
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 146
    :cond_6
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 152
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_6
    const-string v7, "com.qualcomm.qti.cd.IQCDService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 154
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 156
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 158
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 159
    .local v1, "_arg2":I
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/cd/IQCDService$Stub;->QCDService_RequestStatusInfo(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 160
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 161
    if-eqz v4, :cond_7

    .line 162
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 163
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 166
    :cond_7
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 172
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_7
    const-string v7, "com.qualcomm.qti.cd.IQCDService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 176
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_8

    .line 177
    sget-object v7, Lcom/qualcomm/qti/rcsservice/CDInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/CDInfo;

    .line 182
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/CDInfo;
    :goto_4
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/qti/cd/IQCDService$Stub;->QCDService_GetMyCdInfo(ILcom/qualcomm/qti/rcsservice/CDInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 183
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 184
    if-eqz v4, :cond_9

    .line 185
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 186
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 191
    :goto_5
    if-eqz v2, :cond_a

    .line 192
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 193
    invoke-virtual {v2, p3, v6}, Lcom/qualcomm/qti/rcsservice/CDInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 180
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/CDInfo;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_8
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/CDInfo;
    goto :goto_4

    .line 189
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_9
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_5

    .line 196
    :cond_a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 202
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/CDInfo;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_8
    const-string v7, "com.qualcomm.qti.cd.IQCDService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 204
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 206
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 208
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 209
    .restart local v1    # "_arg2":I
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/cd/IQCDService$Stub;->QCDService_RequestCdInfo(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 210
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 211
    if-eqz v4, :cond_b

    .line 212
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 213
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 216
    :cond_b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 222
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_9
    const-string v7, "com.qualcomm.qti.cd.IQCDService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 226
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v2

    .line 228
    .local v2, "_arg1":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 229
    .restart local v1    # "_arg2":I
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/cd/IQCDService$Stub;->QCDService_RequestGivenContactsCDInfo(I[Ljava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 230
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 231
    if-eqz v4, :cond_c

    .line 232
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 233
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 236
    :cond_c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
