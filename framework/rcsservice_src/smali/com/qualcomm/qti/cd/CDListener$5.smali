.class Lcom/qualcomm/qti/cd/CDListener$5;
.super Ljava/lang/Thread;
.source "CDListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/qti/cd/CDListener;->QCDServiceListener_CDStatusUpdate(Lcom/qualcomm/qti/rcsservice/StatusCode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/cd/CDListener;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/cd/CDListener;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/qualcomm/qti/cd/CDListener$5;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 165
    :try_start_0
    const-string v0, "AIDL"

    const-string v1, "  QCDServiceListener_CDStatusUpdate start inside thread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v0, p0, Lcom/qualcomm/qti/cd/CDListener$5;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget-object v0, v0, Lcom/qualcomm/qti/cd/CDListener;->m_QCDServiceListener:Lcom/qualcomm/qti/cd/IQCDServiceListener;

    iget-object v1, p0, Lcom/qualcomm/qti/cd/CDListener$5;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget-object v1, v1, Lcom/qualcomm/qti/cd/CDListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    iget-object v2, p0, Lcom/qualcomm/qti/cd/CDListener$5;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget-object v2, v2, Lcom/qualcomm/qti/cd/CDListener;->m_URI:Ljava/lang/String;

    iget-object v3, p0, Lcom/qualcomm/qti/cd/CDListener$5;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget-object v3, v3, Lcom/qualcomm/qti/cd/CDListener;->m_statusString:Ljava/lang/String;

    iget-object v4, p0, Lcom/qualcomm/qti/cd/CDListener$5;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget-object v4, v4, Lcom/qualcomm/qti/cd/CDListener;->m_customString:Ljava/lang/String;

    iget-object v5, p0, Lcom/qualcomm/qti/cd/CDListener$5;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget v5, v5, Lcom/qualcomm/qti/cd/CDListener;->m_responseCode:I

    iget-object v6, p0, Lcom/qualcomm/qti/cd/CDListener$5;->this$0:Lcom/qualcomm/qti/cd/CDListener;

    iget v6, v6, Lcom/qualcomm/qti/cd/CDListener;->m_userData:I

    invoke-interface/range {v0 .. v6}, Lcom/qualcomm/qti/cd/IQCDServiceListener;->QCDServiceListener_CDStatusUpdate(Lcom/qualcomm/qti/rcsservice/StatusCode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 168
    const-string v0, "AIDL"

    const-string v1, "  QCDServiceListener_CDStatusUpdate end inside thread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :goto_0
    return-void

    .line 169
    :catch_0
    move-exception v7

    .line 171
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
