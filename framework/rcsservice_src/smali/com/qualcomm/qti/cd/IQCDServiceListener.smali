.class public interface abstract Lcom/qualcomm/qti/cd/IQCDServiceListener;
.super Ljava/lang/Object;
.source "IQCDServiceListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/cd/IQCDServiceListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract QCDServiceListener_CDStatusUpdate(Lcom/qualcomm/qti/rcsservice/StatusCode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QCDServiceListener_CDUpdate(Lcom/qualcomm/qti/rcsservice/StatusCode;Ljava/lang/String;Lcom/qualcomm/qti/rcsservice/CDInfo;II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QCDServiceListener_ServiceAvailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QCDServiceListener_ServiceCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QCDServiceListener_ServiceUnavailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
