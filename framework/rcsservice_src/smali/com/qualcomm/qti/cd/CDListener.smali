.class public Lcom/qualcomm/qti/cd/CDListener;
.super Ljava/lang/Object;
.source "CDListener.java"


# instance fields
.field m_QCDServiceListener:Lcom/qualcomm/qti/cd/IQCDServiceListener;

.field m_URI:Ljava/lang/String;

.field m_cdInfo:Lcom/qualcomm/qti/rcsservice/CDInfo;

.field m_cdServiceHandle:I

.field m_cdServiceListenerUserData:J

.field m_customString:Ljava/lang/String;

.field m_responseCode:I

.field m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

.field m_statusString:Ljava/lang/String;

.field m_userData:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method


# virtual methods
.method public QCDServiceListener_CDStatusUpdate(Lcom/qualcomm/qti/rcsservice/StatusCode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 3
    .param p1, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .param p2, "URI"    # Ljava/lang/String;
    .param p3, "statusString"    # Ljava/lang/String;
    .param p4, "customString"    # Ljava/lang/String;
    .param p5, "responseCode"    # I
    .param p6, "userData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 150
    const-string v1, "AIDL"

    const-string v2, "  QCDServiceListener_CDStatusUpdate start outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iput-object p1, p0, Lcom/qualcomm/qti/cd/CDListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 153
    iput-object p2, p0, Lcom/qualcomm/qti/cd/CDListener;->m_URI:Ljava/lang/String;

    .line 154
    iput-object p3, p0, Lcom/qualcomm/qti/cd/CDListener;->m_statusString:Ljava/lang/String;

    .line 155
    iput-object p4, p0, Lcom/qualcomm/qti/cd/CDListener;->m_customString:Ljava/lang/String;

    .line 156
    iput p5, p0, Lcom/qualcomm/qti/cd/CDListener;->m_responseCode:I

    .line 157
    iput p6, p0, Lcom/qualcomm/qti/cd/CDListener;->m_userData:I

    .line 159
    new-instance v0, Lcom/qualcomm/qti/cd/CDListener$5;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/cd/CDListener$5;-><init>(Lcom/qualcomm/qti/cd/CDListener;)V

    .line 175
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 176
    const-string v1, "AIDL"

    const-string v2, "  QCDServiceListener_CDStatusUpdate end outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    return-void
.end method

.method public QCDServiceListener_CDUpdate(Lcom/qualcomm/qti/rcsservice/StatusCode;Ljava/lang/String;Lcom/qualcomm/qti/rcsservice/CDInfo;II)V
    .locals 3
    .param p1, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .param p2, "URI"    # Ljava/lang/String;
    .param p3, "cdInfo"    # Lcom/qualcomm/qti/rcsservice/CDInfo;
    .param p4, "responseCode"    # I
    .param p5, "userData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 118
    const-string v1, "AIDL"

    const-string v2, "  QCDServiceListener_CDUpdate start outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iput-object p1, p0, Lcom/qualcomm/qti/cd/CDListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 121
    iput-object p2, p0, Lcom/qualcomm/qti/cd/CDListener;->m_URI:Ljava/lang/String;

    .line 122
    iput-object p3, p0, Lcom/qualcomm/qti/cd/CDListener;->m_cdInfo:Lcom/qualcomm/qti/rcsservice/CDInfo;

    .line 123
    iput p4, p0, Lcom/qualcomm/qti/cd/CDListener;->m_responseCode:I

    .line 124
    iput p5, p0, Lcom/qualcomm/qti/cd/CDListener;->m_userData:I

    .line 126
    new-instance v0, Lcom/qualcomm/qti/cd/CDListener$4;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/cd/CDListener$4;-><init>(Lcom/qualcomm/qti/cd/CDListener;)V

    .line 142
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 143
    const-string v1, "AIDL"

    const-string v2, "  QCDServiceListener_CDUpdate end outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    return-void
.end method

.method public QCDServiceListener_ServiceAvailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .locals 3
    .param p1, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 68
    const-string v1, "AIDL "

    const-string v2, "  QCDServiceListener_ServiceAvailable start outside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iput-object p1, p0, Lcom/qualcomm/qti/cd/CDListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 71
    new-instance v0, Lcom/qualcomm/qti/cd/CDListener$2;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/cd/CDListener$2;-><init>(Lcom/qualcomm/qti/cd/CDListener;)V

    .line 86
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 87
    const-string v1, "AIDL "

    const-string v2, "  QCDServiceListener_ServiceAvailable end outside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    return-void
.end method

.method public QCDServiceListener_ServiceCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;I)V
    .locals 3
    .param p1, "cdServiceListenerUserData"    # J
    .param p3, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .param p4, "cdServiceHandle"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 42
    const-string v1, "AIDL"

    const-string v2, "   QCDServiceListener_ServiceCreated  start outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    iput-wide p1, p0, Lcom/qualcomm/qti/cd/CDListener;->m_cdServiceListenerUserData:J

    .line 44
    iput-object p3, p0, Lcom/qualcomm/qti/cd/CDListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 45
    iput p4, p0, Lcom/qualcomm/qti/cd/CDListener;->m_cdServiceHandle:I

    .line 46
    new-instance v0, Lcom/qualcomm/qti/cd/CDListener$1;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/cd/CDListener$1;-><init>(Lcom/qualcomm/qti/cd/CDListener;)V

    .line 62
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 63
    const-string v1, "AIDL"

    const-string v2, "   QCDServiceListener_ServiceCreated  end outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    return-void
.end method

.method public QCDServiceListener_ServiceUnavailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .locals 3
    .param p1, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 92
    const-string v1, "AIDL"

    const-string v2, "  QCDServiceListener_ServiceUnavailable start outside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iput-object p1, p0, Lcom/qualcomm/qti/cd/CDListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 95
    new-instance v0, Lcom/qualcomm/qti/cd/CDListener$3;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/cd/CDListener$3;-><init>(Lcom/qualcomm/qti/cd/CDListener;)V

    .line 110
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 111
    const-string v1, "AIDL"

    const-string v2, "  QCDServiceListener_ServiceUnavailable end outside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    return-void
.end method

.method public setIQCDServiceListener(Lcom/qualcomm/qti/cd/IQCDServiceListener;)V
    .locals 2
    .param p1, "objIQCDServiceListener"    # Lcom/qualcomm/qti/cd/IQCDServiceListener;

    .prologue
    .line 32
    const-string v0, "AIDL"

    const-string v1, "  setIQCDServiceListener start "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    iput-object p1, p0, Lcom/qualcomm/qti/cd/CDListener;->m_QCDServiceListener:Lcom/qualcomm/qti/cd/IQCDServiceListener;

    .line 34
    const-string v0, "AIDL"

    const-string v1, "  setIQCDServiceListener end "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    return-void
.end method
