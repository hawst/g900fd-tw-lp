.class public abstract Lcom/qualcomm/qti/cd/IQCDServiceListener$Stub;
.super Landroid/os/Binder;
.source "IQCDServiceListener.java"

# interfaces
.implements Lcom/qualcomm/qti/cd/IQCDServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/cd/IQCDServiceListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/cd/IQCDServiceListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.cd.IQCDServiceListener"

.field static final TRANSACTION_QCDServiceListener_CDStatusUpdate:I = 0x5

.field static final TRANSACTION_QCDServiceListener_CDUpdate:I = 0x4

.field static final TRANSACTION_QCDServiceListener_ServiceAvailable:I = 0x2

.field static final TRANSACTION_QCDServiceListener_ServiceCreated:I = 0x1

.field static final TRANSACTION_QCDServiceListener_ServiceUnavailable:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.cd.IQCDServiceListener"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/cd/IQCDServiceListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/cd/IQCDServiceListener;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.cd.IQCDServiceListener"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/cd/IQCDServiceListener;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/cd/IQCDServiceListener;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/cd/IQCDServiceListener$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/cd/IQCDServiceListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 145
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 44
    :sswitch_0
    const-string v0, "com.qualcomm.qti.cd.IQCDServiceListener"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v7

    .line 45
    goto :goto_0

    .line 49
    :sswitch_1
    const-string v0, "com.qualcomm.qti.cd.IQCDServiceListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v8

    .line 53
    .local v8, "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 60
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 61
    .local v3, "_arg2":I
    invoke-virtual {p0, v8, v9, v2, v3}, Lcom/qualcomm/qti/cd/IQCDServiceListener$Stub;->QCDServiceListener_ServiceCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;I)V

    .line 62
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 63
    goto :goto_0

    .line 57
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    .end local v3    # "_arg2":I
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_1

    .line 67
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    .end local v8    # "_arg0":J
    :sswitch_2
    const-string v0, "com.qualcomm.qti.cd.IQCDServiceListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 75
    .local v1, "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_2
    invoke-virtual {p0, v1}, Lcom/qualcomm/qti/cd/IQCDServiceListener$Stub;->QCDServiceListener_ServiceAvailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V

    .line 76
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 77
    goto :goto_0

    .line 73
    .end local v1    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_2

    .line 81
    .end local v1    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_3
    const-string v0, "com.qualcomm.qti.cd.IQCDServiceListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 84
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 89
    .restart local v1    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_3
    invoke-virtual {p0, v1}, Lcom/qualcomm/qti/cd/IQCDServiceListener$Stub;->QCDServiceListener_ServiceUnavailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V

    .line 90
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 91
    goto :goto_0

    .line 87
    .end local v1    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_3

    .line 95
    .end local v1    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_4
    const-string v0, "com.qualcomm.qti.cd.IQCDServiceListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 97
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    .line 98
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 104
    .restart local v1    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 106
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    .line 107
    sget-object v0, Lcom/qualcomm/qti/rcsservice/CDInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/qualcomm/qti/rcsservice/CDInfo;

    .line 113
    .local v3, "_arg2":Lcom/qualcomm/qti/rcsservice/CDInfo;
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 115
    .local v4, "_arg3":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .local v5, "_arg4":I
    move-object v0, p0

    .line 116
    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qti/cd/IQCDServiceListener$Stub;->QCDServiceListener_CDUpdate(Lcom/qualcomm/qti/rcsservice/StatusCode;Ljava/lang/String;Lcom/qualcomm/qti/rcsservice/CDInfo;II)V

    .line 117
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 118
    goto/16 :goto_0

    .line 101
    .end local v1    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/CDInfo;
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":I
    :cond_3
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_4

    .line 110
    .restart local v2    # "_arg1":Ljava/lang/String;
    :cond_4
    const/4 v3, 0x0

    .restart local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/CDInfo;
    goto :goto_5

    .line 122
    .end local v1    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/CDInfo;
    :sswitch_5
    const-string v0, "com.qualcomm.qti.cd.IQCDServiceListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 124
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    .line 125
    sget-object v0, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 131
    .restart local v1    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 133
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 135
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 137
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 139
    .restart local v5    # "_arg4":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .local v6, "_arg5":I
    move-object v0, p0

    .line 140
    invoke-virtual/range {v0 .. v6}, Lcom/qualcomm/qti/cd/IQCDServiceListener$Stub;->QCDServiceListener_CDStatusUpdate(Lcom/qualcomm/qti/rcsservice/StatusCode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 141
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 142
    goto/16 :goto_0

    .line 128
    .end local v1    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v5    # "_arg4":I
    .end local v6    # "_arg5":I
    :cond_5
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_6

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
