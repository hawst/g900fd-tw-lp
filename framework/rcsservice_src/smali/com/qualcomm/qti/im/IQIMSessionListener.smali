.class public interface abstract Lcom/qualcomm/qti/im/IQIMSessionListener;
.super Ljava/lang/Object;
.source "IQIMSessionListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract QIMSessionListener_HandleConferenceEvent(I[Lcom/qualcomm/qti/rcsservice/Participant;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QIMSessionListener_HandleIMError(ILjava/lang/String;Lcom/qualcomm/qti/rcsservice/Error;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QIMSessionListener_HandleInteruption(ILjava/lang/String;Lcom/qualcomm/qti/rcsservice/ContentInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QIMSessionListener_HandleIscomposingEvent(ILjava/lang/String;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QIMSessionListener_HandleMessageDeliveryStatus(ILcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QIMSessionListener_HandleMessageReceived(ILcom/qualcomm/qti/rcsservice/IncomingMessage;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QIMSessionListener_HandleSessionCanceled(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QIMSessionListener_HandleSessionClosed(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QIMSessionListener_HandleSessionEstablished(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QIMSessionListener_HandleSessionInfoUpdate(ILcom/qualcomm/qti/rcsservice/SessionInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QIMSessionListener_HandleSessionReqStatus(ILcom/qualcomm/qti/rcsservice/ReqStatus;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QIMSessionListener_HandleTransferProgress(ILjava/lang/String;Lcom/qualcomm/qti/rcsservice/ContentInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
