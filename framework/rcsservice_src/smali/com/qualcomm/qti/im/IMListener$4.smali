.class Lcom/qualcomm/qti/im/IMListener$4;
.super Ljava/lang/Thread;
.source "IMListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/qti/im/IMListener;->QIMServiceListener_HandleIncomingSession(Lcom/qualcomm/qti/rcsservice/SessionInfo;Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;Ljava/lang/String;Lcom/qualcomm/qti/rcsservice/ContentInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/im/IMListener;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/im/IMListener;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/qualcomm/qti/im/IMListener$4;->this$0:Lcom/qualcomm/qti/im/IMListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 148
    :try_start_0
    iget-object v1, p0, Lcom/qualcomm/qti/im/IMListener$4;->this$0:Lcom/qualcomm/qti/im/IMListener;

    iget-object v1, v1, Lcom/qualcomm/qti/im/IMListener;->m_QIMServiceListener:Lcom/qualcomm/qti/im/IQIMServiceListener;

    iget-object v2, p0, Lcom/qualcomm/qti/im/IMListener$4;->this$0:Lcom/qualcomm/qti/im/IMListener;

    # getter for: Lcom/qualcomm/qti/im/IMListener;->m_sessionInfo:Lcom/qualcomm/qti/rcsservice/SessionInfo;
    invoke-static {v2}, Lcom/qualcomm/qti/im/IMListener;->access$400(Lcom/qualcomm/qti/im/IMListener;)Lcom/qualcomm/qti/rcsservice/SessionInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/qualcomm/qti/im/IMListener$4;->this$0:Lcom/qualcomm/qti/im/IMListener;

    # getter for: Lcom/qualcomm/qti/im/IMListener;->m_incomingContactInfo:Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;
    invoke-static {v3}, Lcom/qualcomm/qti/im/IMListener;->access$500(Lcom/qualcomm/qti/im/IMListener;)Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;

    move-result-object v3

    iget-object v4, p0, Lcom/qualcomm/qti/im/IMListener$4;->this$0:Lcom/qualcomm/qti/im/IMListener;

    # getter for: Lcom/qualcomm/qti/im/IMListener;->m_msgID:Ljava/lang/String;
    invoke-static {v4}, Lcom/qualcomm/qti/im/IMListener;->access$600(Lcom/qualcomm/qti/im/IMListener;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/qualcomm/qti/im/IMListener$4;->this$0:Lcom/qualcomm/qti/im/IMListener;

    # getter for: Lcom/qualcomm/qti/im/IMListener;->m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;
    invoke-static {v5}, Lcom/qualcomm/qti/im/IMListener;->access$700(Lcom/qualcomm/qti/im/IMListener;)Lcom/qualcomm/qti/rcsservice/ContentInfo;

    move-result-object v5

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/qualcomm/qti/im/IQIMServiceListener;->QIMServiceListener_HandleIncomingSession(Lcom/qualcomm/qti/rcsservice/SessionInfo;Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;Ljava/lang/String;Lcom/qualcomm/qti/rcsservice/ContentInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :goto_0
    return-void

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
