.class public Lcom/qualcomm/qti/im/NativeIM;
.super Ljava/lang/Object;
.source "NativeIM.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native QIMServiceAddListener(ILcom/qualcomm/qti/im/IQIMServiceListener;)J
.end method

.method public native QIMServiceCreateSession(ILcom/qualcomm/qti/im/IQIMSessionListener;Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;Lcom/qualcomm/qti/rcsservice/ContentInfo;I)I
.end method

.method public native QIMServiceGetActiveSessions(ILcom/qualcomm/qti/rcsservice/SessionList;Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QIMServiceGetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QIMServiceRemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QIMSessionAccept(II)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QIMSessionAddListener(ILcom/qualcomm/qti/im/IQIMSessionListener;)J
.end method

.method public native QIMSessionAddParticipants(I[Lcom/qualcomm/qti/rcsservice/Participant;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QIMSessionCancel(II)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QIMSessionCloseSession(II)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QIMSessionGetSessionInfo(ILcom/qualcomm/qti/rcsservice/SessionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QIMSessionReject(II)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QIMSessionRemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QIMSessionSendDisplayStatus(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QIMSessionSendMMMessage(ILcom/qualcomm/qti/rcsservice/ContentInfo;II)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QIMSessionSendTextMessage(ILjava/lang/String;II)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QIMSessionSetIsComposingStatus(IZI)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method
