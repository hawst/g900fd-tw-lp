.class public abstract Lcom/qualcomm/qti/im/IQIMService$Stub;
.super Landroid/os/Binder;
.source "IQIMService.java"

# interfaces
.implements Lcom/qualcomm/qti/im/IQIMService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/im/IQIMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/im/IQIMService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.im.IQIMService"

.field static final TRANSACTION_QIMService_AddListener:I = 0x2

.field static final TRANSACTION_QIMService_CreateSession:I = 0x4

.field static final TRANSACTION_QIMService_GetActiveSessions:I = 0x5

.field static final TRANSACTION_QIMService_GetVersion:I = 0x1

.field static final TRANSACTION_QIMService_RemoveListener:I = 0x3

.field static final TRANSACTION_QIMSession_Accept:I = 0x8

.field static final TRANSACTION_QIMSession_AddListener:I = 0x6

.field static final TRANSACTION_QIMSession_AddParticipants:I = 0xc

.field static final TRANSACTION_QIMSession_Cancel:I = 0xa

.field static final TRANSACTION_QIMSession_CloseSession:I = 0xb

.field static final TRANSACTION_QIMSession_GetSessionInfo:I = 0x11

.field static final TRANSACTION_QIMSession_Reject:I = 0x9

.field static final TRANSACTION_QIMSession_RemoveListener:I = 0x7

.field static final TRANSACTION_QIMSession_SendDisplayStatus:I = 0x10

.field static final TRANSACTION_QIMSession_SendMMMessage:I = 0xe

.field static final TRANSACTION_QIMSession_SendTextMessage:I = 0xd

.field static final TRANSACTION_QIMSession_SetIsComposingStatus:I = 0xf


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/im/IQIMService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/im/IQIMService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.im.IQIMService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/im/IQIMService;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/im/IQIMService;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/im/IQIMService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IQIMService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 419
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 44
    :sswitch_0
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 45
    const/4 v0, 0x1

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 53
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    sget-object v0, Lcom/qualcomm/qti/rcsservice/VersionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/VersionInfo;

    .line 59
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    :goto_1
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMService_GetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 60
    .local v8, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 61
    if-eqz v8, :cond_1

    .line 62
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 63
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 68
    :goto_2
    if-eqz v2, :cond_2

    .line 69
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    const/4 v0, 0x1

    invoke-virtual {v2, p3, v0}, Lcom/qualcomm/qti/rcsservice/VersionInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 75
    :goto_3
    const/4 v0, 0x1

    goto :goto_0

    .line 57
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    goto :goto_1

    .line 66
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    .line 73
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3

    .line 79
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_2
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 83
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/qualcomm/qti/im/IQIMServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/im/IQIMServiceListener;

    move-result-object v2

    .line 84
    .local v2, "_arg1":Lcom/qualcomm/qti/im/IQIMServiceListener;
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMService_AddListener(ILcom/qualcomm/qti/im/IQIMServiceListener;)J

    move-result-wide v8

    .line 85
    .local v8, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 86
    invoke-virtual {p3, v8, v9}, Landroid/os/Parcel;->writeLong(J)V

    .line 87
    const/4 v0, 0x1

    goto :goto_0

    .line 91
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/im/IQIMServiceListener;
    .end local v8    # "_result":J
    :sswitch_3
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 95
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 96
    .local v6, "_arg1":J
    invoke-virtual {p0, v1, v6, v7}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMService_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 97
    .local v8, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 98
    if-eqz v8, :cond_3

    .line 99
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 100
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 105
    :goto_4
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 103
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_4

    .line 109
    .end local v1    # "_arg0":I
    .end local v6    # "_arg1":J
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_4
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 111
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 113
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/im/IQIMSessionListener;

    move-result-object v2

    .line 115
    .local v2, "_arg1":Lcom/qualcomm/qti/im/IQIMSessionListener;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    .line 116
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;

    .line 122
    .local v3, "_arg2":Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    .line 123
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 129
    .local v4, "_arg3":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .local v5, "_arg4":I
    move-object v0, p0

    .line 130
    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMService_CreateSession(ILcom/qualcomm/qti/im/IQIMSessionListener;Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;Lcom/qualcomm/qti/rcsservice/ContentInfo;I)I

    move-result v8

    .line 131
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 132
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 133
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 119
    .end local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;
    .end local v4    # "_arg3":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .end local v5    # "_arg4":I
    .end local v8    # "_result":I
    :cond_4
    const/4 v3, 0x0

    .restart local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;
    goto :goto_5

    .line 126
    :cond_5
    const/4 v4, 0x0

    .restart local v4    # "_arg3":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    goto :goto_6

    .line 137
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/im/IQIMSessionListener;
    .end local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;
    .end local v4    # "_arg3":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :sswitch_5
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 141
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    .line 142
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionList;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/SessionList;

    .line 148
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/SessionList;
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 149
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMService_GetActiveSessions(ILcom/qualcomm/qti/rcsservice/SessionList;Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 150
    .local v8, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 151
    if-eqz v8, :cond_7

    .line 152
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 153
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 158
    :goto_8
    if-eqz v2, :cond_8

    .line 159
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 160
    const/4 v0, 0x1

    invoke-virtual {v2, p3, v0}, Lcom/qualcomm/qti/rcsservice/SessionList;->writeToParcel(Landroid/os/Parcel;I)V

    .line 165
    :goto_9
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 145
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionList;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_6
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionList;
    goto :goto_7

    .line 156
    .restart local v3    # "_arg2":Ljava/lang/String;
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_8

    .line 163
    :cond_8
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_9

    .line 169
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionList;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_6
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 171
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 173
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/im/IQIMSessionListener;

    move-result-object v2

    .line 174
    .local v2, "_arg1":Lcom/qualcomm/qti/im/IQIMSessionListener;
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMSession_AddListener(ILcom/qualcomm/qti/im/IQIMSessionListener;)J

    move-result-wide v8

    .line 175
    .local v8, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 176
    invoke-virtual {p3, v8, v9}, Landroid/os/Parcel;->writeLong(J)V

    .line 177
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 181
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/im/IQIMSessionListener;
    .end local v8    # "_result":J
    :sswitch_7
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 183
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 185
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 186
    .restart local v6    # "_arg1":J
    invoke-virtual {p0, v1, v6, v7}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMSession_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 187
    .local v8, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 188
    if-eqz v8, :cond_9

    .line 189
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 190
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 195
    :goto_a
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 193
    :cond_9
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_a

    .line 199
    .end local v1    # "_arg0":I
    .end local v6    # "_arg1":J
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_8
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 201
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 203
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 204
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMSession_Accept(II)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 205
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 206
    if-eqz v8, :cond_a

    .line 207
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 208
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 213
    :goto_b
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 211
    :cond_a
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_b

    .line 217
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_9
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 219
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 221
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 222
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMSession_Reject(II)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 223
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 224
    if-eqz v8, :cond_b

    .line 225
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 226
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 231
    :goto_c
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 229
    :cond_b
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_c

    .line 235
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_a
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 237
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 239
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 240
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMSession_Cancel(II)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 241
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 242
    if-eqz v8, :cond_c

    .line 243
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 244
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 249
    :goto_d
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 247
    :cond_c
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_d

    .line 253
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_b
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 255
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 257
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 258
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMSession_CloseSession(II)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 259
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 260
    if-eqz v8, :cond_d

    .line 261
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 262
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 267
    :goto_e
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 265
    :cond_d
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_e

    .line 271
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_c
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 273
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 275
    .restart local v1    # "_arg0":I
    sget-object v0, Lcom/qualcomm/qti/rcsservice/Participant;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/qualcomm/qti/rcsservice/Participant;

    .line 277
    .local v2, "_arg1":[Lcom/qualcomm/qti/rcsservice/Participant;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 278
    .local v3, "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMSession_AddParticipants(I[Lcom/qualcomm/qti/rcsservice/Participant;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 279
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 280
    if-eqz v8, :cond_e

    .line 281
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 282
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 287
    :goto_f
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 285
    :cond_e
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_f

    .line 291
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":[Lcom/qualcomm/qti/rcsservice/Participant;
    .end local v3    # "_arg2":I
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_d
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 293
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 295
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 297
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_f

    .line 298
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DNReq;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/qualcomm/qti/rcsservice/DNReq;

    .line 304
    .local v3, "_arg2":Lcom/qualcomm/qti/rcsservice/DNReq;
    :goto_10
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 305
    .local v4, "_arg3":I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMSession_SendTextMessage(ILjava/lang/String;Lcom/qualcomm/qti/rcsservice/DNReq;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 306
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 307
    if-eqz v8, :cond_10

    .line 308
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 309
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 314
    :goto_11
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 301
    .end local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/DNReq;
    .end local v4    # "_arg3":I
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_f
    const/4 v3, 0x0

    .restart local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/DNReq;
    goto :goto_10

    .line 312
    .restart local v4    # "_arg3":I
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_10
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_11

    .line 318
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/DNReq;
    .end local v4    # "_arg3":I
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_e
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 320
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 322
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_11

    .line 323
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 329
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :goto_12
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_12

    .line 330
    sget-object v0, Lcom/qualcomm/qti/rcsservice/DNReq;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/qualcomm/qti/rcsservice/DNReq;

    .line 336
    .restart local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/DNReq;
    :goto_13
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 337
    .restart local v4    # "_arg3":I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMSession_SendMMMessage(ILcom/qualcomm/qti/rcsservice/ContentInfo;Lcom/qualcomm/qti/rcsservice/DNReq;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 338
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 339
    if-eqz v8, :cond_13

    .line 340
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 341
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 346
    :goto_14
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 326
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .end local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/DNReq;
    .end local v4    # "_arg3":I
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_11
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    goto :goto_12

    .line 333
    :cond_12
    const/4 v3, 0x0

    .restart local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/DNReq;
    goto :goto_13

    .line 344
    .restart local v4    # "_arg3":I
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_13
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_14

    .line 350
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .end local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/DNReq;
    .end local v4    # "_arg3":I
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_f
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 352
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 354
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_14

    const/4 v2, 0x1

    .line 356
    .local v2, "_arg1":Z
    :goto_15
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 357
    .local v3, "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMSession_SetIsComposingStatus(IZI)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 358
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 359
    if-eqz v8, :cond_15

    .line 360
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 361
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 366
    :goto_16
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 354
    .end local v2    # "_arg1":Z
    .end local v3    # "_arg2":I
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_14
    const/4 v2, 0x0

    goto :goto_15

    .line 364
    .restart local v2    # "_arg1":Z
    .restart local v3    # "_arg2":I
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_15
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_16

    .line 370
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Z
    .end local v3    # "_arg2":I
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_10
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 372
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 374
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 376
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 377
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMSession_SendDisplayStatus(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 378
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 379
    if-eqz v8, :cond_16

    .line 380
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 381
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 386
    :goto_17
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 384
    :cond_16
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_17

    .line 390
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_11
    const-string v0, "com.qualcomm.qti.im.IQIMService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 392
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 394
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_17

    .line 395
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/SessionInfo;

    .line 400
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    :goto_18
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/im/IQIMService$Stub;->QIMSession_GetSessionInfo(ILcom/qualcomm/qti/rcsservice/SessionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 401
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 402
    if-eqz v8, :cond_18

    .line 403
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 404
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 409
    :goto_19
    if-eqz v2, :cond_19

    .line 410
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 411
    const/4 v0, 0x1

    invoke-virtual {v2, p3, v0}, Lcom/qualcomm/qti/rcsservice/SessionInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 416
    :goto_1a
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 398
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_17
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    goto :goto_18

    .line 407
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_18
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_19

    .line 414
    :cond_19
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1a

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
