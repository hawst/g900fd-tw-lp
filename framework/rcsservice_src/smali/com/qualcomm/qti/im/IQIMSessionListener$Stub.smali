.class public abstract Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;
.super Landroid/os/Binder;
.source "IQIMSessionListener.java"

# interfaces
.implements Lcom/qualcomm/qti/im/IQIMSessionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/im/IQIMSessionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/im/IQIMSessionListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.im.IQIMSessionListener"

.field static final TRANSACTION_QIMSessionListener_HandleConferenceEvent:I = 0x7

.field static final TRANSACTION_QIMSessionListener_HandleIMError:I = 0x6

.field static final TRANSACTION_QIMSessionListener_HandleInteruption:I = 0x9

.field static final TRANSACTION_QIMSessionListener_HandleIscomposingEvent:I = 0xa

.field static final TRANSACTION_QIMSessionListener_HandleMessageDeliveryStatus:I = 0x8

.field static final TRANSACTION_QIMSessionListener_HandleMessageReceived:I = 0x5

.field static final TRANSACTION_QIMSessionListener_HandleSessionCanceled:I = 0x2

.field static final TRANSACTION_QIMSessionListener_HandleSessionClosed:I = 0x3

.field static final TRANSACTION_QIMSessionListener_HandleSessionEstablished:I = 0x1

.field static final TRANSACTION_QIMSessionListener_HandleSessionInfoUpdate:I = 0xb

.field static final TRANSACTION_QIMSessionListener_HandleSessionReqStatus:I = 0xc

.field static final TRANSACTION_QIMSessionListener_HandleTransferProgress:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.qualcomm.qti.im.IQIMSessionListener"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/im/IQIMSessionListener;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.qualcomm.qti.im.IQIMSessionListener"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/im/IQIMSessionListener;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/qualcomm/qti/im/IQIMSessionListener;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 217
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    .line 42
    :sswitch_0
    const-string v5, "com.qualcomm.qti.im.IQIMSessionListener"

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v5, "com.qualcomm.qti.im.IQIMSessionListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 50
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;->QIMSessionListener_HandleSessionEstablished(I)V

    .line 51
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 56
    .end local v0    # "_arg0":I
    :sswitch_2
    const-string v5, "com.qualcomm.qti.im.IQIMSessionListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 58
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 59
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;->QIMSessionListener_HandleSessionCanceled(I)V

    .line 60
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 65
    .end local v0    # "_arg0":I
    :sswitch_3
    const-string v5, "com.qualcomm.qti.im.IQIMSessionListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 68
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;->QIMSessionListener_HandleSessionClosed(I)V

    .line 69
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 74
    .end local v0    # "_arg0":I
    :sswitch_4
    const-string v5, "com.qualcomm.qti.im.IQIMSessionListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 78
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 80
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_0

    .line 81
    sget-object v5, Lcom/qualcomm/qti/rcsservice/ContentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 86
    .local v2, "_arg2":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :goto_1
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;->QIMSessionListener_HandleTransferProgress(ILjava/lang/String;Lcom/qualcomm/qti/rcsservice/ContentInfo;)V

    .line 87
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 84
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    goto :goto_1

    .line 92
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :sswitch_5
    const-string v5, "com.qualcomm.qti.im.IQIMSessionListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 96
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_1

    .line 97
    sget-object v5, Lcom/qualcomm/qti/rcsservice/IncomingMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/IncomingMessage;

    .line 102
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/IncomingMessage;
    :goto_2
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;->QIMSessionListener_HandleMessageReceived(ILcom/qualcomm/qti/rcsservice/IncomingMessage;)V

    .line 103
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 100
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/IncomingMessage;
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/IncomingMessage;
    goto :goto_2

    .line 108
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/IncomingMessage;
    :sswitch_6
    const-string v5, "com.qualcomm.qti.im.IQIMSessionListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 110
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 112
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 114
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_2

    .line 115
    sget-object v5, Lcom/qualcomm/qti/rcsservice/Error;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/Error;

    .line 121
    .local v2, "_arg2":Lcom/qualcomm/qti/rcsservice/Error;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 122
    .local v3, "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;->QIMSessionListener_HandleIMError(ILjava/lang/String;Lcom/qualcomm/qti/rcsservice/Error;Ljava/lang/String;)V

    .line 123
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 118
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/Error;
    .end local v3    # "_arg3":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/Error;
    goto :goto_3

    .line 128
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/Error;
    :sswitch_7
    const-string v5, "com.qualcomm.qti.im.IQIMSessionListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 130
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 132
    .restart local v0    # "_arg0":I
    sget-object v5, Lcom/qualcomm/qti/rcsservice/Participant;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/qualcomm/qti/rcsservice/Participant;

    .line 133
    .local v1, "_arg1":[Lcom/qualcomm/qti/rcsservice/Participant;
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;->QIMSessionListener_HandleConferenceEvent(I[Lcom/qualcomm/qti/rcsservice/Participant;)V

    .line 134
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 139
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":[Lcom/qualcomm/qti/rcsservice/Participant;
    :sswitch_8
    const-string v5, "com.qualcomm.qti.im.IQIMSessionListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 141
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 143
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_3

    .line 144
    sget-object v5, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;

    .line 149
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;
    :goto_4
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;->QIMSessionListener_HandleMessageDeliveryStatus(ILcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;)V

    .line 150
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 147
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;
    :cond_3
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;
    goto :goto_4

    .line 155
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;
    :sswitch_9
    const-string v5, "com.qualcomm.qti.im.IQIMSessionListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 159
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 161
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_4

    .line 162
    sget-object v5, Lcom/qualcomm/qti/rcsservice/ContentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 167
    .local v2, "_arg2":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :goto_5
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;->QIMSessionListener_HandleInteruption(ILjava/lang/String;Lcom/qualcomm/qti/rcsservice/ContentInfo;)V

    .line 168
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 165
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    goto :goto_5

    .line 173
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :sswitch_a
    const-string v5, "com.qualcomm.qti.im.IQIMSessionListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 175
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 177
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 179
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_5

    move v2, v4

    .line 180
    .local v2, "_arg2":Z
    :goto_6
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;->QIMSessionListener_HandleIscomposingEvent(ILjava/lang/String;Z)V

    .line 181
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 179
    .end local v2    # "_arg2":Z
    :cond_5
    const/4 v2, 0x0

    goto :goto_6

    .line 186
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Ljava/lang/String;
    :sswitch_b
    const-string v5, "com.qualcomm.qti.im.IQIMSessionListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 188
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 190
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_6

    .line 191
    sget-object v5, Lcom/qualcomm/qti/rcsservice/SessionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/SessionInfo;

    .line 196
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    :goto_7
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;->QIMSessionListener_HandleSessionInfoUpdate(ILcom/qualcomm/qti/rcsservice/SessionInfo;)V

    .line 197
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 194
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    :cond_6
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    goto :goto_7

    .line 202
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    :sswitch_c
    const-string v5, "com.qualcomm.qti.im.IQIMSessionListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 204
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 206
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_7

    .line 207
    sget-object v5, Lcom/qualcomm/qti/rcsservice/ReqStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/ReqStatus;

    .line 212
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/ReqStatus;
    :goto_8
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/im/IQIMSessionListener$Stub;->QIMSessionListener_HandleSessionReqStatus(ILcom/qualcomm/qti/rcsservice/ReqStatus;)V

    .line 213
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 210
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/ReqStatus;
    :cond_7
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/ReqStatus;
    goto :goto_8

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
