.class public Lcom/qualcomm/qti/im/QIMService;
.super Lcom/qualcomm/qti/im/IQIMService$Stub;
.source "QIMService.java"


# instance fields
.field private objNativeIM:Lcom/qualcomm/qti/im/NativeIM;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/qualcomm/qti/im/IQIMService$Stub;-><init>()V

    .line 27
    new-instance v0, Lcom/qualcomm/qti/im/NativeIM;

    invoke-direct {v0}, Lcom/qualcomm/qti/im/NativeIM;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    return-void
.end method


# virtual methods
.method public QIMService_AddListener(ILcom/qualcomm/qti/im/IQIMServiceListener;)J
    .locals 2
    .param p1, "imServiceHandle"    # I
    .param p2, "imServiceListener"    # Lcom/qualcomm/qti/im/IQIMServiceListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/im/NativeIM;->QIMServiceAddListener(ILcom/qualcomm/qti/im/IQIMServiceListener;)J

    move-result-wide v0

    return-wide v0
.end method

.method public QIMService_CreateSession(ILcom/qualcomm/qti/im/IQIMSessionListener;Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;Lcom/qualcomm/qti/rcsservice/ContentInfo;I)I
    .locals 6
    .param p1, "imServiceHandle"    # I
    .param p2, "imSessionListener"    # Lcom/qualcomm/qti/im/IQIMSessionListener;
    .param p3, "sessionCreationInfo"    # Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;
    .param p4, "contentInfo"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .param p5, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 54
    const-string v0, "AIDL"

    const-string v1, "QIMService_CreateSession api native call"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qti/im/NativeIM;->QIMServiceCreateSession(ILcom/qualcomm/qti/im/IQIMSessionListener;Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;Lcom/qualcomm/qti/rcsservice/ContentInfo;I)I

    move-result v0

    return v0
.end method

.method public QIMService_GetActiveSessions(ILcom/qualcomm/qti/rcsservice/SessionList;Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "imServiceHandle"    # I
    .param p2, "sessionList"    # Lcom/qualcomm/qti/rcsservice/SessionList;
    .param p3, "remoteContactURI"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/im/NativeIM;->QIMServiceGetActiveSessions(ILcom/qualcomm/qti/rcsservice/SessionList;Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QIMService_GetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "imServiceHandle"    # I
    .param p2, "versionInfo"    # Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/im/NativeIM;->QIMServiceGetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QIMService_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 2
    .param p1, "imServiceHandle"    # I
    .param p2, "imServiceUserData"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/im/NativeIM;->QIMServiceRemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QIMSession_Accept(II)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "imSessionHandle"    # I
    .param p2, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/im/NativeIM;->QIMSessionAccept(II)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QIMSession_AddListener(ILcom/qualcomm/qti/im/IQIMSessionListener;)J
    .locals 2
    .param p1, "imSessionHandle"    # I
    .param p2, "imSessionHandleObject"    # Lcom/qualcomm/qti/im/IQIMSessionListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/im/NativeIM;->QIMSessionAddListener(ILcom/qualcomm/qti/im/IQIMSessionListener;)J

    move-result-wide v0

    return-wide v0
.end method

.method public QIMSession_AddParticipants(I[Lcom/qualcomm/qti/rcsservice/Participant;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "imSessionHandle"    # I
    .param p2, "participantList"    # [Lcom/qualcomm/qti/rcsservice/Participant;
    .param p3, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/im/NativeIM;->QIMSessionAddParticipants(I[Lcom/qualcomm/qti/rcsservice/Participant;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QIMSession_Cancel(II)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "imSessionHandle"    # I
    .param p2, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/im/NativeIM;->QIMSessionCancel(II)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QIMSession_CloseSession(II)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "imSessionHandle"    # I
    .param p2, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/im/NativeIM;->QIMSessionCloseSession(II)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QIMSession_GetSessionInfo(ILcom/qualcomm/qti/rcsservice/SessionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "imSessionHandle"    # I
    .param p2, "sessionInfo"    # Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 138
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/im/NativeIM;->QIMSessionGetSessionInfo(ILcom/qualcomm/qti/rcsservice/SessionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QIMSession_Reject(II)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "imSessionHandle"    # I
    .param p2, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/im/NativeIM;->QIMSessionReject(II)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QIMSession_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 2
    .param p1, "imSessionHandle"    # I
    .param p2, "imSessionUserData"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/im/NativeIM;->QIMSessionRemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QIMSession_SendDisplayStatus(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "imSessionHandle"    # I
    .param p2, "pMsgID"    # Ljava/lang/String;
    .param p3, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/im/NativeIM;->QIMSessionSendDisplayStatus(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QIMSession_SendMMMessage(ILcom/qualcomm/qti/rcsservice/ContentInfo;Lcom/qualcomm/qti/rcsservice/DNReq;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 2
    .param p1, "imSessionHandle"    # I
    .param p2, "multimediaMsg"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .param p3, "dnReq"    # Lcom/qualcomm/qti/rcsservice/DNReq;
    .param p4, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {p3}, Lcom/qualcomm/qti/rcsservice/DNReq;->getDNReqValue()Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->ordinal()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1, p4}, Lcom/qualcomm/qti/im/NativeIM;->QIMSessionSendMMMessage(ILcom/qualcomm/qti/rcsservice/ContentInfo;II)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QIMSession_SendTextMessage(ILjava/lang/String;Lcom/qualcomm/qti/rcsservice/DNReq;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 2
    .param p1, "imSessionHandle"    # I
    .param p2, "textMessage"    # Ljava/lang/String;
    .param p3, "dnReq"    # Lcom/qualcomm/qti/rcsservice/DNReq;
    .param p4, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {p3}, Lcom/qualcomm/qti/rcsservice/DNReq;->getDNReqValue()Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;

    move-result-object v1

    invoke-virtual {v1}, Lcom/qualcomm/qti/rcsservice/DNReq$QRCS_DN_REQ;->ordinal()I

    move-result v1

    invoke-virtual {v0, p1, p2, v1, p4}, Lcom/qualcomm/qti/im/NativeIM;->QIMSessionSendTextMessage(ILjava/lang/String;II)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QIMSession_SetIsComposingStatus(IZI)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "imSessionHandle"    # I
    .param p2, "status"    # Z
    .param p3, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 126
    iget-object v0, p0, Lcom/qualcomm/qti/im/QIMService;->objNativeIM:Lcom/qualcomm/qti/im/NativeIM;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/im/NativeIM;->QIMSessionSetIsComposingStatus(IZI)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public getIMServiceImpl()Lcom/qualcomm/qti/im/IQIMService$Stub;
    .locals 0

    .prologue
    .line 30
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 146
    :try_start_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/qualcomm/qti/im/IQIMService$Stub;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 147
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v1, "AIDL_QIMService"

    const-string v2, "Unexpected remote exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 150
    throw v0
.end method
