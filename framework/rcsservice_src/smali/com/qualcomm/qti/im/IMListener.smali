.class public Lcom/qualcomm/qti/im/IMListener;
.super Ljava/lang/Object;
.source "IMListener.java"


# instance fields
.field m_QIMServiceListener:Lcom/qualcomm/qti/im/IQIMServiceListener;

.field m_QIMSessionListener:Lcom/qualcomm/qti/im/IQIMSessionListener;

.field private m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

.field private m_error:Lcom/qualcomm/qti/rcsservice/Error;

.field private m_errorDesc:Ljava/lang/String;

.field private m_featureTag:Ljava/lang/String;

.field private m_imServiceHandle:I

.field private m_imServiceListenerUserData:J

.field private m_imSessionHandle:I

.field private m_imSessionHandleOld:I

.field private m_imSessionHdl:I

.field private m_incomingContactInfo:Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;

.field private m_incomingMessage:Lcom/qualcomm/qti/rcsservice/IncomingMessage;

.field private m_msgID:Ljava/lang/String;

.field private m_participant:[Lcom/qualcomm/qti/rcsservice/Participant;

.field private m_participantListLen:I

.field private m_remoteContact:Ljava/lang/String;

.field private m_reqStatus:Lcom/qualcomm/qti/rcsservice/ReqStatus;

.field private m_reqUserData:I

.field private m_sessionInfo:Lcom/qualcomm/qti/rcsservice/SessionInfo;

.field private m_status:Z

.field private m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

.field private m_statusInfo:Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/qualcomm/qti/im/IMListener;)J
    .locals 2
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget-wide v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_imServiceListenerUserData:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/qualcomm/qti/im/IMListener;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_featureTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/qualcomm/qti/im/IMListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_reqUserData:I

    return v0
.end method

.method static synthetic access$1100(Lcom/qualcomm/qti/im/IMListener;)Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_statusInfo:Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/qualcomm/qti/im/IMListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHdl:I

    return v0
.end method

.method static synthetic access$1300(Lcom/qualcomm/qti/im/IMListener;)Lcom/qualcomm/qti/rcsservice/IncomingMessage;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_incomingMessage:Lcom/qualcomm/qti/rcsservice/IncomingMessage;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/qualcomm/qti/im/IMListener;)Lcom/qualcomm/qti/rcsservice/Error;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_error:Lcom/qualcomm/qti/rcsservice/Error;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/qualcomm/qti/im/IMListener;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_errorDesc:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/qualcomm/qti/im/IMListener;)[Lcom/qualcomm/qti/rcsservice/Participant;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_participant:[Lcom/qualcomm/qti/rcsservice/Participant;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/qualcomm/qti/im/IMListener;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_remoteContact:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/qualcomm/qti/im/IMListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_status:Z

    return v0
.end method

.method static synthetic access$1900(Lcom/qualcomm/qti/im/IMListener;)Lcom/qualcomm/qti/rcsservice/ReqStatus;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_reqStatus:Lcom/qualcomm/qti/rcsservice/ReqStatus;

    return-object v0
.end method

.method static synthetic access$200(Lcom/qualcomm/qti/im/IMListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    return-object v0
.end method

.method static synthetic access$300(Lcom/qualcomm/qti/im/IMListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_imServiceHandle:I

    return v0
.end method

.method static synthetic access$400(Lcom/qualcomm/qti/im/IMListener;)Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_sessionInfo:Lcom/qualcomm/qti/rcsservice/SessionInfo;

    return-object v0
.end method

.method static synthetic access$500(Lcom/qualcomm/qti/im/IMListener;)Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_incomingContactInfo:Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;

    return-object v0
.end method

.method static synthetic access$600(Lcom/qualcomm/qti/im/IMListener;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_msgID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/qualcomm/qti/im/IMListener;)Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    return-object v0
.end method

.method static synthetic access$800(Lcom/qualcomm/qti/im/IMListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHandleOld:I

    return v0
.end method

.method static synthetic access$900(Lcom/qualcomm/qti/im/IMListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/im/IMListener;

    .prologue
    .line 23
    iget v0, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHandle:I

    return v0
.end method


# virtual methods
.method public QIMServiceListener_HandleIncomingSession(Lcom/qualcomm/qti/rcsservice/SessionInfo;Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;Ljava/lang/String;Lcom/qualcomm/qti/rcsservice/ContentInfo;)V
    .locals 1
    .param p1, "sessionInfo"    # Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .param p2, "incomingContactInfo"    # Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;
    .param p3, "msgID"    # Ljava/lang/String;
    .param p4, "contentInfo"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 137
    iput-object p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_sessionInfo:Lcom/qualcomm/qti/rcsservice/SessionInfo;

    .line 138
    iput-object p2, p0, Lcom/qualcomm/qti/im/IMListener;->m_incomingContactInfo:Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;

    .line 139
    iput-object p3, p0, Lcom/qualcomm/qti/im/IMListener;->m_msgID:Ljava/lang/String;

    .line 140
    iput-object p4, p0, Lcom/qualcomm/qti/im/IMListener;->m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 142
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$4;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$4;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 155
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 157
    return-void
.end method

.method public QIMServiceListener_HandleMessageDeliveryStatus(Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;)V
    .locals 1
    .param p1, "statusInfo"    # Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 213
    iput-object p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_statusInfo:Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;

    .line 214
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$7;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$7;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 227
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 229
    return-void
.end method

.method public QIMServiceListener_ReplaceSession(ILcom/qualcomm/qti/rcsservice/SessionInfo;)V
    .locals 1
    .param p1, "imSessionHandleOld"    # I
    .param p2, "newSessionInfo"    # Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 164
    iput p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHandleOld:I

    .line 165
    iput-object p2, p0, Lcom/qualcomm/qti/im/IMListener;->m_sessionInfo:Lcom/qualcomm/qti/rcsservice/SessionInfo;

    .line 166
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$5;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$5;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 179
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 181
    return-void
.end method

.method public QIMServiceListener_ServiceAvailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .locals 1
    .param p1, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 94
    iput-object p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 96
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$2;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$2;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 108
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 110
    return-void
.end method

.method public QIMServiceListener_ServiceCreated(JLjava/lang/String;Lcom/qualcomm/qti/rcsservice/StatusCode;I)V
    .locals 1
    .param p1, "imServiceListenerUserData"    # J
    .param p3, "featureTag"    # Ljava/lang/String;
    .param p4, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .param p5, "imServiceHandle"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 69
    iput-wide p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_imServiceListenerUserData:J

    .line 70
    iput-object p3, p0, Lcom/qualcomm/qti/im/IMListener;->m_featureTag:Ljava/lang/String;

    .line 71
    iput-object p4, p0, Lcom/qualcomm/qti/im/IMListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 72
    iput p5, p0, Lcom/qualcomm/qti/im/IMListener;->m_imServiceHandle:I

    .line 74
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$1;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$1;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 88
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 90
    return-void
.end method

.method public QIMServiceListener_ServiceUnavailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .locals 1
    .param p1, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 114
    iput-object p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 116
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$3;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$3;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 128
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 130
    return-void
.end method

.method public QIMServiceListener_SessionCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;ILjava/lang/String;I)V
    .locals 1
    .param p1, "imServiceListenerUserData"    # J
    .param p3, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .param p4, "imSessionHandle"    # I
    .param p5, "msgID"    # Ljava/lang/String;
    .param p6, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 187
    iput-wide p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_imServiceListenerUserData:J

    .line 188
    iput-object p3, p0, Lcom/qualcomm/qti/im/IMListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 189
    iput p4, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHandle:I

    .line 190
    iput-object p5, p0, Lcom/qualcomm/qti/im/IMListener;->m_msgID:Ljava/lang/String;

    .line 191
    iput p6, p0, Lcom/qualcomm/qti/im/IMListener;->m_reqUserData:I

    .line 192
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$6;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$6;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 206
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 208
    return-void
.end method

.method public QIMSessionListener_HandleConferenceEvent(I[Lcom/qualcomm/qti/rcsservice/Participant;I)V
    .locals 1
    .param p1, "iMSessionHandle"    # I
    .param p2, "participantList"    # [Lcom/qualcomm/qti/rcsservice/Participant;
    .param p3, "participantListLen"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 370
    iput p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHdl:I

    .line 371
    iput-object p2, p0, Lcom/qualcomm/qti/im/IMListener;->m_participant:[Lcom/qualcomm/qti/rcsservice/Participant;

    .line 372
    iput p3, p0, Lcom/qualcomm/qti/im/IMListener;->m_participantListLen:I

    .line 374
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$14;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$14;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 387
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 389
    return-void
.end method

.method public QIMSessionListener_HandleIMError(ILjava/lang/String;Lcom/qualcomm/qti/rcsservice/Error;Ljava/lang/String;)V
    .locals 1
    .param p1, "iMSessionHandle"    # I
    .param p2, "msgID"    # Ljava/lang/String;
    .param p3, "errorCode"    # Lcom/qualcomm/qti/rcsservice/Error;
    .param p4, "errorDesc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 345
    iput p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHdl:I

    .line 346
    iput-object p2, p0, Lcom/qualcomm/qti/im/IMListener;->m_msgID:Ljava/lang/String;

    .line 347
    iput-object p3, p0, Lcom/qualcomm/qti/im/IMListener;->m_error:Lcom/qualcomm/qti/rcsservice/Error;

    .line 348
    iput-object p4, p0, Lcom/qualcomm/qti/im/IMListener;->m_errorDesc:Ljava/lang/String;

    .line 350
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$13;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$13;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 363
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 365
    return-void
.end method

.method public QIMSessionListener_HandleInteruption(ILjava/lang/String;Lcom/qualcomm/qti/rcsservice/ContentInfo;)V
    .locals 1
    .param p1, "iMSessionHandle"    # I
    .param p2, "msgID"    # Ljava/lang/String;
    .param p3, "contentInfo"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 419
    iput p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHdl:I

    .line 420
    iput-object p2, p0, Lcom/qualcomm/qti/im/IMListener;->m_msgID:Ljava/lang/String;

    .line 421
    iput-object p3, p0, Lcom/qualcomm/qti/im/IMListener;->m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 423
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$16;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$16;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 436
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 438
    return-void
.end method

.method public QIMSessionListener_HandleIscomposingEvent(ILjava/lang/String;Z)V
    .locals 1
    .param p1, "iMSessionHandle"    # I
    .param p2, "remoteContactURI"    # Ljava/lang/String;
    .param p3, "status"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 444
    iput p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHdl:I

    .line 445
    iput-object p2, p0, Lcom/qualcomm/qti/im/IMListener;->m_remoteContact:Ljava/lang/String;

    .line 446
    iput-boolean p3, p0, Lcom/qualcomm/qti/im/IMListener;->m_status:Z

    .line 448
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$17;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$17;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 461
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 463
    return-void
.end method

.method public QIMSessionListener_HandleMessageDeliveryStatus(ILcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;)V
    .locals 1
    .param p1, "iMSessionHandle"    # I
    .param p2, "statusInfo"    # Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 395
    iput p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHdl:I

    .line 396
    iput-object p2, p0, Lcom/qualcomm/qti/im/IMListener;->m_statusInfo:Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;

    .line 398
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$15;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$15;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 411
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 413
    return-void
.end method

.method public QIMSessionListener_HandleMessageReceived(ILcom/qualcomm/qti/rcsservice/IncomingMessage;)V
    .locals 1
    .param p1, "iMSessionHandle"    # I
    .param p2, "incomingMessage"    # Lcom/qualcomm/qti/rcsservice/IncomingMessage;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 322
    iput p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHdl:I

    .line 323
    iput-object p2, p0, Lcom/qualcomm/qti/im/IMListener;->m_incomingMessage:Lcom/qualcomm/qti/rcsservice/IncomingMessage;

    .line 325
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$12;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$12;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 338
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 340
    return-void
.end method

.method public QIMSessionListener_HandleSessionCanceled(I)V
    .locals 1
    .param p1, "iMSessionHandle"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 256
    iput p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHdl:I

    .line 258
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$9;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$9;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 270
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 272
    return-void
.end method

.method public QIMSessionListener_HandleSessionClosed(I)V
    .locals 1
    .param p1, "iMSessionHandle"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 276
    iput p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHdl:I

    .line 278
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$10;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$10;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 290
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 292
    return-void
.end method

.method public QIMSessionListener_HandleSessionEstablished(I)V
    .locals 1
    .param p1, "iMSessionHandle"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 234
    iput p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHdl:I

    .line 236
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$8;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$8;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 249
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 251
    return-void
.end method

.method public QIMSessionListener_HandleSessionInfoUpdate(ILcom/qualcomm/qti/rcsservice/SessionInfo;)V
    .locals 1
    .param p1, "iMSessionHandle"    # I
    .param p2, "sessionInfo"    # Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 469
    iput p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHdl:I

    .line 470
    iput-object p2, p0, Lcom/qualcomm/qti/im/IMListener;->m_sessionInfo:Lcom/qualcomm/qti/rcsservice/SessionInfo;

    .line 472
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$18;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$18;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 485
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 487
    return-void
.end method

.method public QIMSessionListener_HandleSessionReqStatus(ILcom/qualcomm/qti/rcsservice/ReqStatus;)V
    .locals 1
    .param p1, "iMSessionHandle"    # I
    .param p2, "reqStatus"    # Lcom/qualcomm/qti/rcsservice/ReqStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 492
    iput p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHdl:I

    .line 493
    iput-object p2, p0, Lcom/qualcomm/qti/im/IMListener;->m_reqStatus:Lcom/qualcomm/qti/rcsservice/ReqStatus;

    .line 495
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$19;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$19;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 508
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 510
    return-void
.end method

.method public QIMSessionListener_HandleTransferProgress(ILjava/lang/String;Lcom/qualcomm/qti/rcsservice/ContentInfo;)V
    .locals 1
    .param p1, "iMSessionHandle"    # I
    .param p2, "msgID"    # Ljava/lang/String;
    .param p3, "contentInfo"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 297
    iput p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_imSessionHdl:I

    .line 298
    iput-object p2, p0, Lcom/qualcomm/qti/im/IMListener;->m_msgID:Ljava/lang/String;

    .line 299
    iput-object p3, p0, Lcom/qualcomm/qti/im/IMListener;->m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 301
    new-instance v0, Lcom/qualcomm/qti/im/IMListener$11;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/im/IMListener$11;-><init>(Lcom/qualcomm/qti/im/IMListener;)V

    .line 314
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 316
    return-void
.end method

.method public setIQIMServiceListener(Lcom/qualcomm/qti/im/IQIMServiceListener;)V
    .locals 2
    .param p1, "objIQIMServiceListener"    # Lcom/qualcomm/qti/im/IQIMServiceListener;

    .prologue
    .line 55
    const-string v0, "AIDL"

    const-string v1, "  setIQIMServiceListener start "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    iput-object p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_QIMServiceListener:Lcom/qualcomm/qti/im/IQIMServiceListener;

    .line 57
    return-void
.end method

.method public setIQIMSessionListener(Lcom/qualcomm/qti/im/IQIMSessionListener;)V
    .locals 2
    .param p1, "objIQIMSessionListener"    # Lcom/qualcomm/qti/im/IQIMSessionListener;

    .prologue
    .line 61
    const-string v0, "AIDL"

    const-string v1, "  setIQIMSessionListener start "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    iput-object p1, p0, Lcom/qualcomm/qti/im/IMListener;->m_QIMSessionListener:Lcom/qualcomm/qti/im/IQIMSessionListener;

    .line 63
    return-void
.end method
