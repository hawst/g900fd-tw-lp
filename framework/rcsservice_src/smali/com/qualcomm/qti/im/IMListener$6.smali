.class Lcom/qualcomm/qti/im/IMListener$6;
.super Ljava/lang/Thread;
.source "IMListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/qti/im/IMListener;->QIMServiceListener_SessionCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;ILjava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/im/IMListener;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/im/IMListener;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/qualcomm/qti/im/IMListener$6;->this$0:Lcom/qualcomm/qti/im/IMListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 198
    :try_start_0
    iget-object v1, p0, Lcom/qualcomm/qti/im/IMListener$6;->this$0:Lcom/qualcomm/qti/im/IMListener;

    iget-object v1, v1, Lcom/qualcomm/qti/im/IMListener;->m_QIMServiceListener:Lcom/qualcomm/qti/im/IQIMServiceListener;

    iget-object v2, p0, Lcom/qualcomm/qti/im/IMListener$6;->this$0:Lcom/qualcomm/qti/im/IMListener;

    # getter for: Lcom/qualcomm/qti/im/IMListener;->m_imServiceListenerUserData:J
    invoke-static {v2}, Lcom/qualcomm/qti/im/IMListener;->access$000(Lcom/qualcomm/qti/im/IMListener;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/qualcomm/qti/im/IMListener$6;->this$0:Lcom/qualcomm/qti/im/IMListener;

    # getter for: Lcom/qualcomm/qti/im/IMListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-static {v4}, Lcom/qualcomm/qti/im/IMListener;->access$200(Lcom/qualcomm/qti/im/IMListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    iget-object v5, p0, Lcom/qualcomm/qti/im/IMListener$6;->this$0:Lcom/qualcomm/qti/im/IMListener;

    # getter for: Lcom/qualcomm/qti/im/IMListener;->m_imSessionHandle:I
    invoke-static {v5}, Lcom/qualcomm/qti/im/IMListener;->access$900(Lcom/qualcomm/qti/im/IMListener;)I

    move-result v5

    iget-object v6, p0, Lcom/qualcomm/qti/im/IMListener$6;->this$0:Lcom/qualcomm/qti/im/IMListener;

    # getter for: Lcom/qualcomm/qti/im/IMListener;->m_msgID:Ljava/lang/String;
    invoke-static {v6}, Lcom/qualcomm/qti/im/IMListener;->access$600(Lcom/qualcomm/qti/im/IMListener;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/qualcomm/qti/im/IMListener$6;->this$0:Lcom/qualcomm/qti/im/IMListener;

    # getter for: Lcom/qualcomm/qti/im/IMListener;->m_reqUserData:I
    invoke-static {v7}, Lcom/qualcomm/qti/im/IMListener;->access$1000(Lcom/qualcomm/qti/im/IMListener;)I

    move-result v7

    invoke-interface/range {v1 .. v7}, Lcom/qualcomm/qti/im/IQIMServiceListener;->QIMServiceListener_SessionCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;ILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    :goto_0
    return-void

    .line 201
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
