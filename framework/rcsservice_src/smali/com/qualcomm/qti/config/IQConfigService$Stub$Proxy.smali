.class Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;
.super Ljava/lang/Object;
.source "IQConfigService.java"

# interfaces
.implements Lcom/qualcomm/qti/config/IQConfigService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/config/IQConfigService$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 0
    .param p1, "remote"    # Landroid/os/IBinder;

    .prologue
    .line 605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 606
    iput-object p1, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    .line 607
    return-void
.end method


# virtual methods
.method public QConfigService_AcceptEvent(IZ)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "accepted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1221
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 1222
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 1225
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v4, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 1226
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1227
    if-eqz p2, :cond_0

    const/4 v3, 0x1

    :cond_0
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1228
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/16 v4, 0x11

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 1229
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 1230
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    .line 1231
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1238
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1239
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 1241
    return-object v2

    .line 1234
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_0

    .line 1238
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1239
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method

.method public QConfigService_AddListener(ILcom/qualcomm/qti/config/IQConfigServiceListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "configServiceListener"    # Lcom/qualcomm/qti/config/IQConfigServiceListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 663
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 664
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 667
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 668
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 669
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/qualcomm/qti/config/IQConfigServiceListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    :goto_0
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 670
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 671
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 672
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    .line 673
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 680
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 681
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 683
    return-object v2

    .line 669
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 676
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_1

    .line 680
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 681
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method

.method public QConfigService_GetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "versionInfo"    # Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 630
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 631
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 634
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 635
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 636
    if-eqz p2, :cond_1

    .line 637
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 638
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/rcsservice/VersionInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 643
    :goto_0
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 644
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 645
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_2

    .line 646
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 651
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 652
    invoke-virtual {p2, v1}, Lcom/qualcomm/qti/rcsservice/VersionInfo;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 656
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 657
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 659
    return-object v2

    .line 641
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 656
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 657
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 649
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_1
.end method

.method public QConfigService_GetXMLConfigApnValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/APNConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "apnConfig"    # Lcom/qualcomm/qti/config/APNConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1111
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 1112
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 1115
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 1116
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1117
    if-eqz p2, :cond_1

    .line 1118
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1119
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1124
    :goto_0
    if-eqz p3, :cond_2

    .line 1125
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1126
    const/4 v3, 0x0

    invoke-virtual {p3, v0, v3}, Lcom/qualcomm/qti/config/APNConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1131
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/16 v4, 0xe

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 1132
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 1133
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    .line 1134
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 1139
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 1140
    invoke-virtual {p3, v1}, Lcom/qualcomm/qti/config/APNConfig;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1144
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1145
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 1147
    return-object v2

    .line 1122
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1144
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1145
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 1129
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1137
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_2
.end method

.method public QConfigService_GetXMLConfigCdValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/CDConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "cdConfig"    # Lcom/qualcomm/qti/config/CDConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1071
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 1072
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 1075
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 1076
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1077
    if-eqz p2, :cond_1

    .line 1078
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1079
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1084
    :goto_0
    if-eqz p3, :cond_2

    .line 1085
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1086
    const/4 v3, 0x0

    invoke-virtual {p3, v0, v3}, Lcom/qualcomm/qti/config/CDConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1091
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/16 v4, 0xd

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 1092
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 1093
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    .line 1094
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 1099
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 1100
    invoke-virtual {p3, v1}, Lcom/qualcomm/qti/config/CDConfig;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1104
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1105
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 1107
    return-object v2

    .line 1082
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1104
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1105
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 1089
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1097
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_2
.end method

.method public QConfigService_GetXMLConfigCpmValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/CPMConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "cpmConfig"    # Lcom/qualcomm/qti/config/CPMConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1031
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 1032
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 1035
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 1036
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1037
    if-eqz p2, :cond_1

    .line 1038
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1039
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1044
    :goto_0
    if-eqz p3, :cond_2

    .line 1045
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1046
    const/4 v3, 0x0

    invoke-virtual {p3, v0, v3}, Lcom/qualcomm/qti/config/CPMConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1051
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/16 v4, 0xc

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 1052
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 1053
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    .line 1054
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 1059
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 1060
    invoke-virtual {p3, v1}, Lcom/qualcomm/qti/config/CPMConfig;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1064
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1065
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 1067
    return-object v2

    .line 1042
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1064
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1065
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 1049
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1057
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_2
.end method

.method public QConfigService_GetXMLConfigImValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/IMConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "imConfig"    # Lcom/qualcomm/qti/config/IMConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 991
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 992
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 995
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 996
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 997
    if-eqz p2, :cond_1

    .line 998
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 999
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1004
    :goto_0
    if-eqz p3, :cond_2

    .line 1005
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1006
    const/4 v3, 0x0

    invoke-virtual {p3, v0, v3}, Lcom/qualcomm/qti/config/IMConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1011
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/16 v4, 0xb

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 1012
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 1013
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    .line 1014
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 1019
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 1020
    invoke-virtual {p3, v1}, Lcom/qualcomm/qti/config/IMConfig;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1024
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1025
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 1027
    return-object v2

    .line 1002
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1024
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1025
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 1009
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1017
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_2
.end method

.method public QConfigService_GetXMLConfigImsSettingsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/ImsSettingsConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "imsSettingsConfig"    # Lcom/qualcomm/qti/config/ImsSettingsConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 791
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 792
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 795
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 796
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 797
    if-eqz p2, :cond_1

    .line 798
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 799
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->writeToParcel(Landroid/os/Parcel;I)V

    .line 804
    :goto_0
    if-eqz p3, :cond_2

    .line 805
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 806
    const/4 v3, 0x0

    invoke-virtual {p3, v0, v3}, Lcom/qualcomm/qti/config/ImsSettingsConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 811
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 812
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 813
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    .line 814
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 819
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 820
    invoke-virtual {p3, v1}, Lcom/qualcomm/qti/config/ImsSettingsConfig;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 824
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 825
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 827
    return-object v2

    .line 802
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 824
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 825
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 809
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 817
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_2
.end method

.method public QConfigService_GetXMLConfigMsgValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/MsgConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "msgConfig"    # Lcom/qualcomm/qti/config/MsgConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 751
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 752
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 755
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 756
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 757
    if-eqz p2, :cond_1

    .line 758
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 759
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->writeToParcel(Landroid/os/Parcel;I)V

    .line 764
    :goto_0
    if-eqz p3, :cond_2

    .line 765
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 766
    const/4 v3, 0x0

    invoke-virtual {p3, v0, v3}, Lcom/qualcomm/qti/config/MsgConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 771
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x5

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 772
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 773
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    .line 774
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 779
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 780
    invoke-virtual {p3, v1}, Lcom/qualcomm/qti/config/MsgConfig;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 784
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 785
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 787
    return-object v2

    .line 762
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 784
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 785
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 769
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 777
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_2
.end method

.method public QConfigService_GetXMLConfigOtherRcsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/OtherRCSConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "otherRCSConfig"    # Lcom/qualcomm/qti/config/OtherRCSConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1151
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 1152
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 1155
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 1156
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1157
    if-eqz p2, :cond_1

    .line 1158
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1159
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1164
    :goto_0
    if-eqz p3, :cond_2

    .line 1165
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1166
    const/4 v3, 0x0

    invoke-virtual {p3, v0, v3}, Lcom/qualcomm/qti/config/OtherRCSConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1171
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/16 v4, 0xf

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 1172
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 1173
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    .line 1174
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 1179
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 1180
    invoke-virtual {p3, v1}, Lcom/qualcomm/qti/config/OtherRCSConfig;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1184
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1185
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 1187
    return-object v2

    .line 1162
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1184
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1185
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 1169
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1177
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_2
.end method

.method public QConfigService_GetXMLConfigPresenceValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/PresenceConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "presenceConfig"    # Lcom/qualcomm/qti/config/PresenceConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 871
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 872
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 875
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 876
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 877
    if-eqz p2, :cond_1

    .line 878
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 879
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->writeToParcel(Landroid/os/Parcel;I)V

    .line 884
    :goto_0
    if-eqz p3, :cond_2

    .line 885
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 886
    const/4 v3, 0x0

    invoke-virtual {p3, v0, v3}, Lcom/qualcomm/qti/config/PresenceConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 891
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/16 v4, 0x8

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 892
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 893
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    .line 894
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 899
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 900
    invoke-virtual {p3, v1}, Lcom/qualcomm/qti/config/PresenceConfig;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 904
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 905
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 907
    return-object v2

    .line 882
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 904
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 905
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 889
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 897
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_2
.end method

.method public QConfigService_GetXMLConfigServicesValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/Services;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "services"    # Lcom/qualcomm/qti/config/Services;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 831
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 832
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 835
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 836
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 837
    if-eqz p2, :cond_1

    .line 838
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 839
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->writeToParcel(Landroid/os/Parcel;I)V

    .line 844
    :goto_0
    if-eqz p3, :cond_2

    .line 845
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 846
    const/4 v3, 0x0

    invoke-virtual {p3, v0, v3}, Lcom/qualcomm/qti/config/Services;->writeToParcel(Landroid/os/Parcel;I)V

    .line 851
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x7

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 852
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 853
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    .line 854
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 859
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 860
    invoke-virtual {p3, v1}, Lcom/qualcomm/qti/config/Services;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 864
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 865
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 867
    return-object v2

    .line 842
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 864
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 865
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 849
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 857
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_2
.end method

.method public QConfigService_GetXMLConfigSuplValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/SuplConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "suplConfig"    # Lcom/qualcomm/qti/config/SuplConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 951
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 952
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 955
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 956
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 957
    if-eqz p2, :cond_1

    .line 958
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 959
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->writeToParcel(Landroid/os/Parcel;I)V

    .line 964
    :goto_0
    if-eqz p3, :cond_2

    .line 965
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 966
    const/4 v3, 0x0

    invoke-virtual {p3, v0, v3}, Lcom/qualcomm/qti/config/SuplConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 971
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/16 v4, 0xa

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 972
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 973
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    .line 974
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 979
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 980
    invoke-virtual {p3, v1}, Lcom/qualcomm/qti/config/SuplConfig;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 984
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 985
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 987
    return-object v2

    .line 962
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 984
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 985
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 969
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 977
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_2
.end method

.method public QConfigService_GetXMLConfigVersionValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/VersionConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "versionConfig"    # Lcom/qualcomm/qti/config/VersionConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 711
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 712
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 715
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 716
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 717
    if-eqz p2, :cond_1

    .line 718
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 719
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->writeToParcel(Landroid/os/Parcel;I)V

    .line 724
    :goto_0
    if-eqz p3, :cond_2

    .line 725
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 726
    const/4 v3, 0x0

    invoke-virtual {p3, v0, v3}, Lcom/qualcomm/qti/config/VersionConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 731
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 732
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 733
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    .line 734
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 739
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 740
    invoke-virtual {p3, v1}, Lcom/qualcomm/qti/config/VersionConfig;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 744
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 745
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 747
    return-object v2

    .line 722
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 744
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 745
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 729
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 737
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_2
.end method

.method public QConfigService_GetXMLConfigXdmsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/XDMSConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "xdmsConfig"    # Lcom/qualcomm/qti/config/XDMSConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 911
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 912
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 915
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 916
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 917
    if-eqz p2, :cond_1

    .line 918
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 919
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->writeToParcel(Landroid/os/Parcel;I)V

    .line 924
    :goto_0
    if-eqz p3, :cond_2

    .line 925
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 926
    const/4 v3, 0x0

    invoke-virtual {p3, v0, v3}, Lcom/qualcomm/qti/config/XDMSConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 931
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/16 v4, 0x9

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 932
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 933
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    .line 934
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 939
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 940
    invoke-virtual {p3, v1}, Lcom/qualcomm/qti/config/XDMSConfig;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 944
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 945
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 947
    return-object v2

    .line 922
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 944
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 945
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 929
    :cond_2
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 937
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_2
.end method

.method public QConfigService_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "configServiceUserData"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 687
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 688
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 691
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 692
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 693
    invoke-virtual {v0, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    .line 694
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 695
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 696
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 697
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 704
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 705
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 707
    return-object v2

    .line 700
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_0

    .line 704
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 705
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method

.method public QConfigService_SetDispositionConfig(ILcom/qualcomm/qti/config/DispositionConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 6
    .param p1, "configServiceHandle"    # I
    .param p2, "dispositionConfig"    # Lcom/qualcomm/qti/config/DispositionConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1191
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 1192
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 1195
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 1196
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1197
    if-eqz p2, :cond_0

    .line 1198
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1199
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/config/DispositionConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1204
    :goto_0
    iget-object v3, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/16 v4, 0x10

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 1205
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 1206
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    .line 1207
    sget-object v3, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1214
    .local v2, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1215
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 1217
    return-object v2

    .line 1202
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_0
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1214
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1215
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 1210
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_1
.end method

.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 610
    iget-object v0, p0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 614
    const-string v0, "com.qualcomm.qti.config.IQConfigService"

    return-object v0
.end method
