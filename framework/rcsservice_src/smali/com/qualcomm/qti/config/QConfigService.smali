.class public Lcom/qualcomm/qti/config/QConfigService;
.super Lcom/qualcomm/qti/config/IQConfigService$Stub;
.source "QConfigService.java"


# instance fields
.field private objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/qualcomm/qti/config/IQConfigService$Stub;-><init>()V

    .line 18
    new-instance v0, Lcom/qualcomm/qti/config/NativeConfig;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/NativeConfig;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    return-void
.end method


# virtual methods
.method public QConfigService_AcceptEvent(IZ)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "accepted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceAcceptEvent(IZ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_AddListener(ILcom/qualcomm/qti/config/IQConfigServiceListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "configServiceListener"    # Lcom/qualcomm/qti/config/IQConfigServiceListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceAddListener(ILcom/qualcomm/qti/config/IQConfigServiceListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_GetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "versionInfo"    # Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceGetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_GetXMLConfigApnValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/APNConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "apnConfig"    # Lcom/qualcomm/qti/config/APNConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceGetXMLConfigApnValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/APNConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_GetXMLConfigCdValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/CDConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "cdConfig"    # Lcom/qualcomm/qti/config/CDConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 128
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceGetXMLConfigCdValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/CDConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_GetXMLConfigCpmValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/CPMConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "cpmConfig"    # Lcom/qualcomm/qti/config/CPMConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceGetXMLConfigCpmValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/CPMConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_GetXMLConfigImValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/IMConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "imConfig"    # Lcom/qualcomm/qti/config/IMConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceGetXMLConfigImValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/IMConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_GetXMLConfigImsSettingsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/ImsSettingsConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "imsSettingsConfig"    # Lcom/qualcomm/qti/config/ImsSettingsConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceGetXMLConfigImsSettingsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/ImsSettingsConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_GetXMLConfigMsgValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/MsgConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "msgConfig"    # Lcom/qualcomm/qti/config/MsgConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceGetXMLConfigMsgValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/MsgConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_GetXMLConfigOtherRcsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/OtherRCSConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "otherRCSConfig"    # Lcom/qualcomm/qti/config/OtherRCSConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceGetXMLConfigOtherRcsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/OtherRCSConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_GetXMLConfigPresenceValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/PresenceConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "presenceConfig"    # Lcom/qualcomm/qti/config/PresenceConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceGetXMLConfigPresenceValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/PresenceConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_GetXMLConfigServicesValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/Services;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "services"    # Lcom/qualcomm/qti/config/Services;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceGetXMLConfigServicesValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/Services;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_GetXMLConfigSuplValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/SuplConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "suplConfig"    # Lcom/qualcomm/qti/config/SuplConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceGetXMLConfigSuplValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/SuplConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_GetXMLConfigVersionValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/VersionConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "versionConfig"    # Lcom/qualcomm/qti/config/VersionConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceGetXMLConfigVersionValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/VersionConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_GetXMLConfigXdmsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/XDMSConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "eConfigParamType"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .param p3, "xdmsConfig"    # Lcom/qualcomm/qti/config/XDMSConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceGetXMLConfigXdmsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/XDMSConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 2
    .param p1, "configServiceHandle"    # I
    .param p2, "configServiceUserData"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceRemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QConfigService_SetDispositionConfig(ILcom/qualcomm/qti/config/DispositionConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "configServiceHandle"    # I
    .param p2, "dispositionConfig"    # Lcom/qualcomm/qti/config/DispositionConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/qualcomm/qti/config/QConfigService;->objNativeConfig:Lcom/qualcomm/qti/config/NativeConfig;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/config/NativeConfig;->QConfigServiceSetDispositionConfig(ILcom/qualcomm/qti/config/DispositionConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public getConfigServiceImpl()Lcom/qualcomm/qti/config/IQConfigService$Stub;
    .locals 0

    .prologue
    .line 21
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 29
    :try_start_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 30
    :catch_0
    move-exception v0

    .line 32
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v1, "AIDL_QConfigService"

    const-string v2, "Unexpected remote exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 33
    throw v0
.end method
