.class public Lcom/qualcomm/qti/config/AuthConfig;
.super Ljava/lang/Object;
.source "AuthConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/AuthConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private eAuthType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

.field private realm:Ljava/lang/String;

.field private userName:Ljava/lang/String;

.field private userPwd:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 197
    new-instance v0, Lcom/qualcomm/qti/config/AuthConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/AuthConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/AuthConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/AuthConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 212
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/AuthConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/AuthConfig$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/AuthConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/qualcomm/qti/config/AuthConfig;->eAuthType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/qualcomm/qti/config/AuthConfig;->realm:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 193
    iget-object v0, p0, Lcom/qualcomm/qti/config/AuthConfig;->userName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lcom/qualcomm/qti/config/AuthConfig;->userPwd:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 195
    return-void

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/config/AuthConfig;->eAuthType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    return v0
.end method

.method public getAuthType()Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/qualcomm/qti/config/AuthConfig;->eAuthType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    return-object v0
.end method

.method public getRealm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/qualcomm/qti/config/AuthConfig;->realm:Ljava/lang/String;

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/qualcomm/qti/config/AuthConfig;->userName:Ljava/lang/String;

    return-object v0
.end method

.method public getUserPwd()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/qualcomm/qti/config/AuthConfig;->userPwd:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 217
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/AuthConfig;->eAuthType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/AuthConfig;->realm:Ljava/lang/String;

    .line 222
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/AuthConfig;->userName:Ljava/lang/String;

    .line 223
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/AuthConfig;->userPwd:Ljava/lang/String;

    .line 224
    return-void

    .line 218
    :catch_0
    move-exception v0

    .line 219
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->QRCS_AUTH_TYPE_UNKNOWN:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    iput-object v1, p0, Lcom/qualcomm/qti/config/AuthConfig;->eAuthType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    goto :goto_0
.end method

.method public setAuthType(I)V
    .locals 3
    .param p1, "eAuthType"    # I

    .prologue
    .line 82
    sparse-switch p1, :sswitch_data_0

    .line 100
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "eAuthType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    sget-object v0, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->QRCS_AUTH_TYPE_UNKNOWN:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/config/AuthConfig;->eAuthType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    .line 104
    :goto_0
    return-void

    .line 85
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->QRCS_AUTH_TYPE_EARLY_IMS:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/config/AuthConfig;->eAuthType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    goto :goto_0

    .line 88
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->QRCS_AUTH_TYPE_AKA:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/config/AuthConfig;->eAuthType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    goto :goto_0

    .line 91
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->QRCS_AUTH_TYPE_DIGEST:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/config/AuthConfig;->eAuthType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    goto :goto_0

    .line 94
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->QRCS_AUTH_TYPE_INVALID:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/config/AuthConfig;->eAuthType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    goto :goto_0

    .line 97
    :sswitch_4
    sget-object v0, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->QRCS_AUTH_TYPE_MAX32:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/config/AuthConfig;->eAuthType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    goto :goto_0

    .line 82
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x10000000 -> :sswitch_4
    .end sparse-switch
.end method

.method public setRealm(Ljava/lang/String;)V
    .locals 0
    .param p1, "realm"    # Ljava/lang/String;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/qualcomm/qti/config/AuthConfig;->realm:Ljava/lang/String;

    .line 125
    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 0
    .param p1, "userName"    # Ljava/lang/String;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/qualcomm/qti/config/AuthConfig;->userName:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public setUserPwd(Ljava/lang/String;)V
    .locals 0
    .param p1, "userPwd"    # Ljava/lang/String;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/qualcomm/qti/config/AuthConfig;->userPwd:Ljava/lang/String;

    .line 167
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 185
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/AuthConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 187
    return-void
.end method
