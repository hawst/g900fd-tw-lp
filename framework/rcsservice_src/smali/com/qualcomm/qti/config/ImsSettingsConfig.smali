.class public Lcom/qualcomm/qti/config/ImsSettingsConfig;
.super Ljava/lang/Object;
.source "ImsSettingsConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/ImsSettingsConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private appauth:Lcom/qualcomm/qti/config/AuthConfig;

.field private conRefsSize:I

.field private conrefs:[Ljava/lang/String;

.field private ext:Lcom/qualcomm/qti/config/ExtConfig;

.field private homeNetworkDomainName:Ljava/lang/String;

.field private icisListSize:I

.field private icsiList:[Lcom/qualcomm/qti/config/IcsiListConfig;

.field private keepAliveEnabled:Z

.field private lboPCSCFAddressListSize:I

.field private lbo_p_cscs_address:[Lcom/qualcomm/qti/config/LboPCscfAddressConfig;

.field private mobilityManagementIMSVoiceTermination:Z

.field private pdpContextOperPref:Z

.field private phoneContextListSize:I

.field private phonecontextList:[Lcom/qualcomm/qti/config/PhoneContextList;

.field private privateUserIdentity:Ljava/lang/String;

.field private publicUserIdentityList:[Ljava/lang/String;

.field private publicUseridentityListSize:I

.field private regRetryBaseTime:I

.field private regRetryMaxTime:I

.field private smsOverIPNetworksIndication:Z

.field private timerT1:I

.field private timerT2:I

.field private timerT4:I

.field private voiceDomainPreferenceEUTRAN:I

.field private voiceDomainPreferenceUTRAN:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 606
    new-instance v0, Lcom/qualcomm/qti/config/ImsSettingsConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/ImsSettingsConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 620
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/ImsSettingsConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 621
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/ImsSettingsConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/ImsSettingsConfig$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/ImsSettingsConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 535
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->pdpContextOperPref:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 536
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->smsOverIPNetworksIndication:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 537
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->keepAliveEnabled:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 538
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->mobilityManagementIMSVoiceTermination:Z

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 540
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->timerT1:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 541
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->timerT2:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 542
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->timerT4:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 543
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->privateUserIdentity:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 544
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->publicUseridentityListSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 545
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->homeNetworkDomainName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 546
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->voiceDomainPreferenceEUTRAN:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 547
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->voiceDomainPreferenceUTRAN:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 548
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->regRetryBaseTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 549
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->regRetryMaxTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 550
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->conRefsSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 551
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->icisListSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 552
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->lboPCSCFAddressListSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 553
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->phoneContextListSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 554
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->conrefs:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->conrefs:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 556
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->conrefs:[Ljava/lang/String;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 557
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->conrefs:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 563
    :goto_4
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->publicUserIdentityList:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->publicUserIdentityList:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 565
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->publicUserIdentityList:[Ljava/lang/String;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 566
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->publicUserIdentityList:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 572
    :goto_5
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->ext:Lcom/qualcomm/qti/config/ExtConfig;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 573
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->icsiList:[Lcom/qualcomm/qti/config/IcsiListConfig;

    if-eqz v0, :cond_6

    .line 575
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->icsiList:[Lcom/qualcomm/qti/config/IcsiListConfig;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 576
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->icsiList:[Lcom/qualcomm/qti/config/IcsiListConfig;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 583
    :goto_6
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->lbo_p_cscs_address:[Lcom/qualcomm/qti/config/LboPCscfAddressConfig;

    if-eqz v0, :cond_7

    .line 585
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->lbo_p_cscs_address:[Lcom/qualcomm/qti/config/LboPCscfAddressConfig;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 586
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->lbo_p_cscs_address:[Lcom/qualcomm/qti/config/LboPCscfAddressConfig;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 593
    :goto_7
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->phonecontextList:[Lcom/qualcomm/qti/config/PhoneContextList;

    if-eqz v0, :cond_8

    .line 595
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->phonecontextList:[Lcom/qualcomm/qti/config/PhoneContextList;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 596
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->phonecontextList:[Lcom/qualcomm/qti/config/PhoneContextList;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 602
    :goto_8
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->appauth:Lcom/qualcomm/qti/config/AuthConfig;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 604
    return-void

    :cond_0
    move v0, v2

    .line 535
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 536
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 537
    goto/16 :goto_2

    :cond_3
    move v1, v2

    .line 538
    goto/16 :goto_3

    .line 561
    :cond_4
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_4

    .line 570
    :cond_5
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_5

    .line 580
    :cond_6
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_6

    .line 590
    :cond_7
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_7

    .line 600
    :cond_8
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_8
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 525
    const/4 v0, 0x0

    return v0
.end method

.method public getAppauth()Lcom/qualcomm/qti/config/AuthConfig;
    .locals 1

    .prologue
    .line 704
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->appauth:Lcom/qualcomm/qti/config/AuthConfig;

    return-object v0
.end method

.method public getConRefsSize()I
    .locals 1

    .prologue
    .line 436
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->conRefsSize:I

    return v0
.end method

.method public getConrefs()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 680
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->conrefs:[Ljava/lang/String;

    return-object v0
.end method

.method public getExt()Lcom/qualcomm/qti/config/ExtConfig;
    .locals 1

    .prologue
    .line 728
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->ext:Lcom/qualcomm/qti/config/ExtConfig;

    return-object v0
.end method

.method public getHomeNetworkDomainName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->homeNetworkDomainName:Ljava/lang/String;

    return-object v0
.end method

.method public getIcisListSize()I
    .locals 1

    .prologue
    .line 457
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->icisListSize:I

    return v0
.end method

.method public getIcsiList()[Lcom/qualcomm/qti/config/IcsiListConfig;
    .locals 1

    .prologue
    .line 720
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->icsiList:[Lcom/qualcomm/qti/config/IcsiListConfig;

    return-object v0
.end method

.method public getLboPCSCFAddressListSize()I
    .locals 1

    .prologue
    .line 478
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->lboPCSCFAddressListSize:I

    return v0
.end method

.method public getLbo_p_cscs_address()[Lcom/qualcomm/qti/config/LboPCscfAddressConfig;
    .locals 1

    .prologue
    .line 712
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->lbo_p_cscs_address:[Lcom/qualcomm/qti/config/LboPCscfAddressConfig;

    return-object v0
.end method

.method public getPhoneContextListSize()I
    .locals 1

    .prologue
    .line 499
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->phoneContextListSize:I

    return v0
.end method

.method public getPhonecontextList()[Lcom/qualcomm/qti/config/PhoneContextList;
    .locals 1

    .prologue
    .line 696
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->phonecontextList:[Lcom/qualcomm/qti/config/PhoneContextList;

    return-object v0
.end method

.method public getPrivateUserIdentity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->privateUserIdentity:Ljava/lang/String;

    return-object v0
.end method

.method public getPublicUserIdentityList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 688
    iget-object v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->publicUserIdentityList:[Ljava/lang/String;

    return-object v0
.end method

.method public getPublicUseridentityListSize()I
    .locals 1

    .prologue
    .line 239
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->publicUseridentityListSize:I

    return v0
.end method

.method public getRegRetryBaseTime()I
    .locals 1

    .prologue
    .line 394
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->regRetryBaseTime:I

    return v0
.end method

.method public getRegRetryMaxTime()I
    .locals 1

    .prologue
    .line 415
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->regRetryMaxTime:I

    return v0
.end method

.method public getTimerT1()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->timerT1:I

    return v0
.end method

.method public getTimerT2()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->timerT2:I

    return v0
.end method

.method public getTimerT4()I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->timerT4:I

    return v0
.end method

.method public getVoiceDomainPreferenceEUTRAN()I
    .locals 1

    .prologue
    .line 282
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->voiceDomainPreferenceEUTRAN:I

    return v0
.end method

.method public getVoiceDomainPreferenceUTRAN()I
    .locals 1

    .prologue
    .line 348
    iget v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->voiceDomainPreferenceUTRAN:I

    return v0
.end method

.method public isKeepAliveEnabled()Z
    .locals 1

    .prologue
    .line 326
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->keepAliveEnabled:Z

    return v0
.end method

.method public isMobilityManagementIMSVoiceTermination()Z
    .locals 1

    .prologue
    .line 370
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->mobilityManagementIMSVoiceTermination:Z

    return v0
.end method

.method public isPdpContextOperPref()Z
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->pdpContextOperPref:Z

    return v0
.end method

.method public isSmsOverIPNetworksIndication()Z
    .locals 1

    .prologue
    .line 305
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->smsOverIPNetworksIndication:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 8
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 625
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-nez v5, :cond_5

    move v5, v6

    :goto_0
    iput-boolean v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->pdpContextOperPref:Z

    .line 626
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-nez v5, :cond_6

    move v5, v6

    :goto_1
    iput-boolean v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->smsOverIPNetworksIndication:Z

    .line 627
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-nez v5, :cond_7

    move v5, v6

    :goto_2
    iput-boolean v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->keepAliveEnabled:Z

    .line 628
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-nez v5, :cond_8

    :goto_3
    iput-boolean v6, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->mobilityManagementIMSVoiceTermination:Z

    .line 630
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->timerT1:I

    .line 631
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->timerT2:I

    .line 632
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->timerT4:I

    .line 633
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->privateUserIdentity:Ljava/lang/String;

    .line 634
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->publicUseridentityListSize:I

    .line 635
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->homeNetworkDomainName:Ljava/lang/String;

    .line 636
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->voiceDomainPreferenceEUTRAN:I

    .line 637
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->voiceDomainPreferenceUTRAN:I

    .line 638
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->regRetryBaseTime:I

    .line 639
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->regRetryMaxTime:I

    .line 640
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->conRefsSize:I

    .line 641
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->icisListSize:I

    .line 642
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->lboPCSCFAddressListSize:I

    .line 643
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->phoneContextListSize:I

    .line 644
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 645
    .local v1, "conrefsLength":I
    if-lez v1, :cond_0

    .line 647
    new-array v5, v1, [Ljava/lang/String;

    iput-object v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->conrefs:[Ljava/lang/String;

    .line 648
    iget-object v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->conrefs:[Ljava/lang/String;

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 650
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 651
    .local v4, "publicUserIdentityListLength":I
    if-lez v4, :cond_1

    .line 653
    new-array v5, v4, [Ljava/lang/String;

    iput-object v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->publicUserIdentityList:[Ljava/lang/String;

    .line 654
    iget-object v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->publicUserIdentityList:[Ljava/lang/String;

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 656
    :cond_1
    const-class v5, Lcom/qualcomm/qti/config/ExtConfig;

    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/qualcomm/qti/config/ExtConfig;

    iput-object v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->ext:Lcom/qualcomm/qti/config/ExtConfig;

    .line 657
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 658
    .local v2, "icsiListLength":I
    if-lez v2, :cond_2

    .line 660
    new-array v5, v2, [Lcom/qualcomm/qti/config/IcsiListConfig;

    iput-object v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->icsiList:[Lcom/qualcomm/qti/config/IcsiListConfig;

    .line 661
    iget-object v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->icsiList:[Lcom/qualcomm/qti/config/IcsiListConfig;

    sget-object v6, Lcom/qualcomm/qti/config/IcsiListConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v5, v6}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    .line 663
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 664
    .local v0, "addressLength":I
    if-lez v0, :cond_3

    .line 666
    new-array v5, v0, [Lcom/qualcomm/qti/config/LboPCscfAddressConfig;

    iput-object v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->lbo_p_cscs_address:[Lcom/qualcomm/qti/config/LboPCscfAddressConfig;

    .line 667
    iget-object v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->lbo_p_cscs_address:[Lcom/qualcomm/qti/config/LboPCscfAddressConfig;

    sget-object v6, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v5, v6}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    .line 669
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 670
    .local v3, "phonecontextListLength":I
    if-lez v3, :cond_4

    .line 672
    new-array v5, v3, [Lcom/qualcomm/qti/config/PhoneContextList;

    iput-object v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->phonecontextList:[Lcom/qualcomm/qti/config/PhoneContextList;

    .line 673
    iget-object v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->phonecontextList:[Lcom/qualcomm/qti/config/PhoneContextList;

    sget-object v6, Lcom/qualcomm/qti/config/PhoneContextList;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v5, v6}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    .line 675
    :cond_4
    const-class v5, Lcom/qualcomm/qti/config/AuthConfig;

    invoke-virtual {v5}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/qualcomm/qti/config/AuthConfig;

    iput-object v5, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->appauth:Lcom/qualcomm/qti/config/AuthConfig;

    .line 677
    return-void

    .end local v0    # "addressLength":I
    .end local v1    # "conrefsLength":I
    .end local v2    # "icsiListLength":I
    .end local v3    # "phonecontextListLength":I
    .end local v4    # "publicUserIdentityListLength":I
    :cond_5
    move v5, v7

    .line 625
    goto/16 :goto_0

    :cond_6
    move v5, v7

    .line 626
    goto/16 :goto_1

    :cond_7
    move v5, v7

    .line 627
    goto/16 :goto_2

    :cond_8
    move v6, v7

    .line 628
    goto/16 :goto_3
.end method

.method public setAppauth(Lcom/qualcomm/qti/config/AuthConfig;)V
    .locals 0
    .param p1, "appauth"    # Lcom/qualcomm/qti/config/AuthConfig;

    .prologue
    .line 708
    iput-object p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->appauth:Lcom/qualcomm/qti/config/AuthConfig;

    .line 709
    return-void
.end method

.method public setConRefsSize(I)V
    .locals 0
    .param p1, "nConRefsSize"    # I

    .prologue
    .line 447
    iput p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->conRefsSize:I

    .line 448
    return-void
.end method

.method public setConrefs([Ljava/lang/String;)V
    .locals 0
    .param p1, "conrefs"    # [Ljava/lang/String;

    .prologue
    .line 684
    iput-object p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->conrefs:[Ljava/lang/String;

    .line 685
    return-void
.end method

.method public setExt(Lcom/qualcomm/qti/config/ExtConfig;)V
    .locals 0
    .param p1, "ext"    # Lcom/qualcomm/qti/config/ExtConfig;

    .prologue
    .line 732
    iput-object p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->ext:Lcom/qualcomm/qti/config/ExtConfig;

    .line 733
    return-void
.end method

.method public setHomeNetworkDomainName(Ljava/lang/String;)V
    .locals 0
    .param p1, "homeNetworkDomainName"    # Ljava/lang/String;

    .prologue
    .line 271
    iput-object p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->homeNetworkDomainName:Ljava/lang/String;

    .line 272
    return-void
.end method

.method public setIcisListSize(I)V
    .locals 0
    .param p1, "nIcisListSize"    # I

    .prologue
    .line 468
    iput p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->icisListSize:I

    .line 469
    return-void
.end method

.method public setIcsiList([Lcom/qualcomm/qti/config/IcsiListConfig;)V
    .locals 0
    .param p1, "icsiList"    # [Lcom/qualcomm/qti/config/IcsiListConfig;

    .prologue
    .line 724
    iput-object p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->icsiList:[Lcom/qualcomm/qti/config/IcsiListConfig;

    .line 725
    return-void
.end method

.method public setKeepAliveEnabled(Z)V
    .locals 0
    .param p1, "bKeepAliveEnabled"    # Z

    .prologue
    .line 337
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->keepAliveEnabled:Z

    .line 338
    return-void
.end method

.method public setLboPCSCFAddressListSize(I)V
    .locals 0
    .param p1, "nLboPCSCFAddressListSize"    # I

    .prologue
    .line 489
    iput p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->lboPCSCFAddressListSize:I

    .line 490
    return-void
.end method

.method public setLbo_p_cscs_address([Lcom/qualcomm/qti/config/LboPCscfAddressConfig;)V
    .locals 0
    .param p1, "lbo_p_cscs_address"    # [Lcom/qualcomm/qti/config/LboPCscfAddressConfig;

    .prologue
    .line 716
    iput-object p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->lbo_p_cscs_address:[Lcom/qualcomm/qti/config/LboPCscfAddressConfig;

    .line 717
    return-void
.end method

.method public setMobilityManagementIMSVoiceTermination(Z)V
    .locals 0
    .param p1, "bMobilityManagementIMSVoiceTermination"    # Z

    .prologue
    .line 384
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->mobilityManagementIMSVoiceTermination:Z

    .line 385
    return-void
.end method

.method public setPdpContextOperPref(Z)V
    .locals 0
    .param p1, "bPdpContextOperPref"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->pdpContextOperPref:Z

    .line 142
    return-void
.end method

.method public setPhoneContextListSize(I)V
    .locals 0
    .param p1, "nPhoneContextListSize"    # I

    .prologue
    .line 510
    iput p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->phoneContextListSize:I

    .line 511
    return-void
.end method

.method public setPhonecontextList([Lcom/qualcomm/qti/config/PhoneContextList;)V
    .locals 0
    .param p1, "phonecontextList"    # [Lcom/qualcomm/qti/config/PhoneContextList;

    .prologue
    .line 700
    iput-object p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->phonecontextList:[Lcom/qualcomm/qti/config/PhoneContextList;

    .line 701
    return-void
.end method

.method public setPrivateUserIdentity(Ljava/lang/String;)V
    .locals 0
    .param p1, "privateUserIdentity"    # Ljava/lang/String;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->privateUserIdentity:Ljava/lang/String;

    .line 230
    return-void
.end method

.method public setPublicUserIdentityList([Ljava/lang/String;)V
    .locals 0
    .param p1, "publicUserIdentityList"    # [Ljava/lang/String;

    .prologue
    .line 692
    iput-object p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->publicUserIdentityList:[Ljava/lang/String;

    .line 693
    return-void
.end method

.method public setPublicUseridentityListSize(I)V
    .locals 0
    .param p1, "nPublicUseridentityListSize"    # I

    .prologue
    .line 250
    iput p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->publicUseridentityListSize:I

    .line 251
    return-void
.end method

.method public setRegRetryBaseTime(I)V
    .locals 0
    .param p1, "nRegRetryBaseTime"    # I

    .prologue
    .line 405
    iput p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->regRetryBaseTime:I

    .line 406
    return-void
.end method

.method public setRegRetryMaxTime(I)V
    .locals 0
    .param p1, "nRegRetryMaxTime"    # I

    .prologue
    .line 426
    iput p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->regRetryMaxTime:I

    .line 427
    return-void
.end method

.method public setSmsOverIPNetworksIndication(Z)V
    .locals 0
    .param p1, "bSmsOverIPNetworksIndication"    # Z

    .prologue
    .line 316
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->smsOverIPNetworksIndication:Z

    .line 317
    return-void
.end method

.method public setTimerT1(I)V
    .locals 0
    .param p1, "nTimerT1"    # I

    .prologue
    .line 162
    iput p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->timerT1:I

    .line 163
    return-void
.end method

.method public setTimerT2(I)V
    .locals 0
    .param p1, "nTimerT2"    # I

    .prologue
    .line 185
    iput p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->timerT2:I

    .line 186
    return-void
.end method

.method public setTimerT4(I)V
    .locals 0
    .param p1, "nTimerT4"    # I

    .prologue
    .line 208
    iput p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->timerT4:I

    .line 209
    return-void
.end method

.method public setVoiceDomainPreferenceEUTRAN(I)V
    .locals 0
    .param p1, "nVoiceDomainPreferenceEUTRAN"    # I

    .prologue
    .line 295
    iput p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->voiceDomainPreferenceEUTRAN:I

    .line 296
    return-void
.end method

.method public setVoiceDomainPreferenceUTRAN(I)V
    .locals 0
    .param p1, "bVoiceDomainPreferenceUTRAN"    # I

    .prologue
    .line 360
    iput p1, p0, Lcom/qualcomm/qti/config/ImsSettingsConfig;->voiceDomainPreferenceUTRAN:I

    .line 361
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 529
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/ImsSettingsConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 531
    return-void
.end method
