.class Lcom/qualcomm/qti/config/ConfigListener$4;
.super Ljava/lang/Thread;
.source "ConfigListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/qti/config/ConfigListener;->QConfigServiceListener_ReceivedFailureResponse(JLcom/qualcomm/qti/config/ConfigFailureMsg;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/config/ConfigListener;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/config/ConfigListener;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/qualcomm/qti/config/ConfigListener$4;->this$0:Lcom/qualcomm/qti/config/ConfigListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 140
    :try_start_0
    const-string v1, "AIDL"

    const-string v2, "  QConfigServiceListener_ReceivedFailureResponse start inside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-object v1, p0, Lcom/qualcomm/qti/config/ConfigListener$4;->this$0:Lcom/qualcomm/qti/config/ConfigListener;

    iget-object v1, v1, Lcom/qualcomm/qti/config/ConfigListener;->m_IQConfigListener:Lcom/qualcomm/qti/config/IQConfigServiceListener;

    iget-object v2, p0, Lcom/qualcomm/qti/config/ConfigListener$4;->this$0:Lcom/qualcomm/qti/config/ConfigListener;

    iget-wide v2, v2, Lcom/qualcomm/qti/config/ConfigListener;->m_configListenerHandle:J

    iget-object v4, p0, Lcom/qualcomm/qti/config/ConfigListener$4;->this$0:Lcom/qualcomm/qti/config/ConfigListener;

    iget-object v4, v4, Lcom/qualcomm/qti/config/ConfigListener;->m_failureMessage:Lcom/qualcomm/qti/config/ConfigFailureMsg;

    invoke-interface {v1, v2, v3, v4}, Lcom/qualcomm/qti/config/IQConfigServiceListener;->QConfigServiceListener_ReceivedFailureResponse(JLcom/qualcomm/qti/config/ConfigFailureMsg;)V

    .line 144
    const-string v1, "AIDL"

    const-string v2, "  QConfigServiceListener_ReceivedFailureResponse end inside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :goto_0
    return-void

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
