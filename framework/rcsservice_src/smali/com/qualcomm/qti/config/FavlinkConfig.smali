.class public Lcom/qualcomm/qti/config/FavlinkConfig;
.super Ljava/lang/Object;
.source "FavlinkConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/FavlinkConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private autMa:I

.field private labelMaxLength:I

.field private links:Lcom/qualcomm/qti/config/LinksConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/qualcomm/qti/config/FavlinkConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/FavlinkConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/FavlinkConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/FavlinkConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 70
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/FavlinkConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/FavlinkConfig$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/FavlinkConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public getAutMa()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/qualcomm/qti/config/FavlinkConfig;->autMa:I

    return v0
.end method

.method public getLabelMaxLength()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/qualcomm/qti/config/FavlinkConfig;->labelMaxLength:I

    return v0
.end method

.method public getLinks()Lcom/qualcomm/qti/config/LinksConfig;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/qualcomm/qti/config/FavlinkConfig;->links:Lcom/qualcomm/qti/config/LinksConfig;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/FavlinkConfig;->autMa:I

    .line 74
    const-class v0, Lcom/qualcomm/qti/config/LinksConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/config/LinksConfig;

    iput-object v0, p0, Lcom/qualcomm/qti/config/FavlinkConfig;->links:Lcom/qualcomm/qti/config/LinksConfig;

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/FavlinkConfig;->labelMaxLength:I

    .line 76
    return-void
.end method

.method public setAutMa(I)V
    .locals 0
    .param p1, "autMa"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/qualcomm/qti/config/FavlinkConfig;->autMa:I

    .line 86
    return-void
.end method

.method public setLabelMaxLength(I)V
    .locals 0
    .param p1, "labelMaxLength"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/qualcomm/qti/config/FavlinkConfig;->labelMaxLength:I

    .line 102
    return-void
.end method

.method public setLinks(Lcom/qualcomm/qti/config/LinksConfig;)V
    .locals 0
    .param p1, "links"    # Lcom/qualcomm/qti/config/LinksConfig;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/qualcomm/qti/config/FavlinkConfig;->links:Lcom/qualcomm/qti/config/LinksConfig;

    .line 94
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 50
    iget v0, p0, Lcom/qualcomm/qti/config/FavlinkConfig;->autMa:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 51
    iget-object v0, p0, Lcom/qualcomm/qti/config/FavlinkConfig;->links:Lcom/qualcomm/qti/config/LinksConfig;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 52
    iget v0, p0, Lcom/qualcomm/qti/config/FavlinkConfig;->labelMaxLength:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 54
    return-void
.end method
