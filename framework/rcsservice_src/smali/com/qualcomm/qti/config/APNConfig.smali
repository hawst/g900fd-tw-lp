.class public Lcom/qualcomm/qti/config/APNConfig;
.super Ljava/lang/Object;
.source "APNConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/APNConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private alwaysUseIMSAPN:I

.field private enableRcseSwitch:Z

.field private rcseOnlyAPN:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 122
    new-instance v0, Lcom/qualcomm/qti/config/APNConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/APNConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/APNConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/APNConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 137
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/APNConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/APNConfig$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/APNConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/APNConfig;->enableRcseSwitch:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 118
    iget-object v0, p0, Lcom/qualcomm/qti/config/APNConfig;->rcseOnlyAPN:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 119
    iget v0, p0, Lcom/qualcomm/qti/config/APNConfig;->alwaysUseIMSAPN:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 120
    return-void

    .line 117
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public getAlwaysUseIMSAPN()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/qualcomm/qti/config/APNConfig;->alwaysUseIMSAPN:I

    return v0
.end method

.method public getRcseOnlyAPN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/qualcomm/qti/config/APNConfig;->rcseOnlyAPN:Ljava/lang/String;

    return-object v0
.end method

.method public isEnableRcseSwitch()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/APNConfig;->enableRcseSwitch:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 141
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/APNConfig;->enableRcseSwitch:Z

    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/APNConfig;->rcseOnlyAPN:Ljava/lang/String;

    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/APNConfig;->alwaysUseIMSAPN:I

    .line 145
    return-void

    .line 141
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setAlwaysUseIMSAPN(I)V
    .locals 0
    .param p1, "alwaysUseIMSAPN"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/qualcomm/qti/config/APNConfig;->alwaysUseIMSAPN:I

    .line 93
    return-void
.end method

.method public setEnableRcseSwitch(Z)V
    .locals 0
    .param p1, "bnableRcseSwitch"    # Z

    .prologue
    .line 84
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/APNConfig;->enableRcseSwitch:Z

    .line 85
    return-void
.end method

.method public setRcseOnlyAPN(Ljava/lang/String;)V
    .locals 0
    .param p1, "rcseOnlyAPN"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/qualcomm/qti/config/APNConfig;->rcseOnlyAPN:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/APNConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 113
    return-void
.end method
