.class public Lcom/qualcomm/qti/config/XDMSConfig;
.super Ljava/lang/Object;
.source "XDMSConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/XDMSConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public eXcapAuthenticationType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

.field private enablePNBManagement:Z

.field private enableXDMSubscribe:Z

.field private revokeTimer:I

.field private xcapAuthenticationSecret:Ljava/lang/String;

.field private xcapAuthenticationUserName:Ljava/lang/String;

.field private xcapRootURI:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 212
    new-instance v0, Lcom/qualcomm/qti/config/XDMSConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/XDMSConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/XDMSConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/XDMSConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 227
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/XDMSConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/XDMSConfig$1;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/XDMSConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 202
    iget v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->revokeTimer:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 203
    iget-object v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->xcapRootURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 204
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->enablePNBManagement:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 205
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->enableXDMSubscribe:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 206
    iget-object v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->xcapAuthenticationUserName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->xcapAuthenticationSecret:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->eXcapAuthenticationType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 210
    return-void

    :cond_0
    move v0, v2

    .line 204
    goto :goto_0

    :cond_1
    move v1, v2

    .line 205
    goto :goto_1

    .line 208
    :cond_2
    iget-object v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->eXcapAuthenticationType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x0

    return v0
.end method

.method public getRevokeTimer()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->revokeTimer:I

    return v0
.end method

.method public getXcapAuthenticationSecret()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->xcapAuthenticationSecret:Ljava/lang/String;

    return-object v0
.end method

.method public getXcapAuthenticationType()Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->eXcapAuthenticationType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    return-object v0
.end method

.method public getXcapAuthenticationUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->xcapAuthenticationUserName:Ljava/lang/String;

    return-object v0
.end method

.method public getXcapRootURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->xcapRootURI:Ljava/lang/String;

    return-object v0
.end method

.method public isEnablePNBManagement()Z
    .locals 1

    .prologue
    .line 246
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->enablePNBManagement:Z

    return v0
.end method

.method public isEnableXDMSubscribe()Z
    .locals 1

    .prologue
    .line 254
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->enableXDMSubscribe:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 231
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/qualcomm/qti/config/XDMSConfig;->revokeTimer:I

    .line 232
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/XDMSConfig;->xcapRootURI:Ljava/lang/String;

    .line 233
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/qualcomm/qti/config/XDMSConfig;->enablePNBManagement:Z

    .line 234
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    iput-boolean v2, p0, Lcom/qualcomm/qti/config/XDMSConfig;->enableXDMSubscribe:Z

    .line 235
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/XDMSConfig;->xcapAuthenticationUserName:Ljava/lang/String;

    .line 236
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/XDMSConfig;->xcapAuthenticationSecret:Ljava/lang/String;

    .line 239
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/XDMSConfig;->eXcapAuthenticationType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    :goto_2
    return-void

    :cond_0
    move v1, v3

    .line 233
    goto :goto_0

    :cond_1
    move v2, v3

    .line 234
    goto :goto_1

    .line 240
    :catch_0
    move-exception v0

    .line 241
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/qualcomm/qti/config/XDMSConfig;->eXcapAuthenticationType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    goto :goto_2
.end method

.method public setEnablePNBManagement(Z)V
    .locals 0
    .param p1, "enablePNBManagement"    # Z

    .prologue
    .line 250
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/XDMSConfig;->enablePNBManagement:Z

    .line 251
    return-void
.end method

.method public setEnableXDMSubscribe(Z)V
    .locals 0
    .param p1, "enableXDMSubscribe"    # Z

    .prologue
    .line 258
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/XDMSConfig;->enableXDMSubscribe:Z

    .line 259
    return-void
.end method

.method public setRevokeTimer(I)V
    .locals 0
    .param p1, "revokeTimer"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/qualcomm/qti/config/XDMSConfig;->revokeTimer:I

    .line 74
    return-void
.end method

.method public setXcapAuthenticationSecret(Ljava/lang/String;)V
    .locals 0
    .param p1, "xcapAuthenticationSecret"    # Ljava/lang/String;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/qualcomm/qti/config/XDMSConfig;->xcapAuthenticationSecret:Ljava/lang/String;

    .line 139
    return-void
.end method

.method public setXcapAuthenticationType(I)V
    .locals 1
    .param p1, "eXcapAuthenticationType"    # I

    .prologue
    .line 160
    sparse-switch p1, :sswitch_data_0

    .line 178
    :goto_0
    return-void

    .line 163
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->QRCS_AUTH_TYPE_EARLY_IMS:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->eXcapAuthenticationType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    goto :goto_0

    .line 166
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->QRCS_AUTH_TYPE_AKA:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->eXcapAuthenticationType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    goto :goto_0

    .line 169
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->QRCS_AUTH_TYPE_DIGEST:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->eXcapAuthenticationType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    goto :goto_0

    .line 172
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->QRCS_AUTH_TYPE_INVALID:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->eXcapAuthenticationType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    goto :goto_0

    .line 175
    :sswitch_4
    sget-object v0, Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;->QRCS_AUTH_TYPE_MAX32:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    iput-object v0, p0, Lcom/qualcomm/qti/config/XDMSConfig;->eXcapAuthenticationType:Lcom/qualcomm/qti/config/AuthConfig$QRCS_AUTH_TYPE;

    goto :goto_0

    .line 160
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x10000000 -> :sswitch_4
    .end sparse-switch
.end method

.method public setXcapAuthenticationUserName(Ljava/lang/String;)V
    .locals 0
    .param p1, "xcapAuthenticationUserName"    # Ljava/lang/String;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/qualcomm/qti/config/XDMSConfig;->xcapAuthenticationUserName:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public setXcapRootURI(Ljava/lang/String;)V
    .locals 0
    .param p1, "xcapRootURI"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/qualcomm/qti/config/XDMSConfig;->xcapRootURI:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 196
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/XDMSConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 198
    return-void
.end method
