.class public interface abstract Lcom/qualcomm/qti/config/IQConfigService;
.super Ljava/lang/Object;
.source "IQConfigService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/config/IQConfigService$Stub;
    }
.end annotation


# virtual methods
.method public abstract QConfigService_AcceptEvent(IZ)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_AddListener(ILcom/qualcomm/qti/config/IQConfigServiceListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_GetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_GetXMLConfigApnValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/APNConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_GetXMLConfigCdValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/CDConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_GetXMLConfigCpmValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/CPMConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_GetXMLConfigImValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/IMConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_GetXMLConfigImsSettingsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/ImsSettingsConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_GetXMLConfigMsgValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/MsgConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_GetXMLConfigOtherRcsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/OtherRCSConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_GetXMLConfigPresenceValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/PresenceConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_GetXMLConfigServicesValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/Services;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_GetXMLConfigSuplValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/SuplConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_GetXMLConfigVersionValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/VersionConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_GetXMLConfigXdmsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/XDMSConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigService_SetDispositionConfig(ILcom/qualcomm/qti/config/DispositionConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
