.class public Lcom/qualcomm/qti/config/SecondaryDevice;
.super Ljava/lang/Object;
.source "SecondaryDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/SecondaryDevice;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private chat:Z

.field private fileTransfer:Z

.field private geoLocPush:Z

.field private imageShare:Z

.field private sendMms:Z

.field private sendSms:Z

.field private videoCall:Z

.field private videoShare:Z

.field private voiceCall:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 331
    new-instance v0, Lcom/qualcomm/qti/config/SecondaryDevice$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/SecondaryDevice$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/SecondaryDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 305
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/SecondaryDevice;->readFromParcel(Landroid/os/Parcel;)V

    .line 346
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/SecondaryDevice$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/SecondaryDevice$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/SecondaryDevice;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 319
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->voiceCall:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 320
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->chat:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 321
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->sendSms:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 322
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->fileTransfer:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 323
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->videoShare:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 324
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->imageShare:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 325
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->videoCall:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 326
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->geoLocPush:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 327
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->sendMms:Z

    if-eqz v0, :cond_8

    :goto_8
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 329
    return-void

    :cond_0
    move v0, v2

    .line 319
    goto :goto_0

    :cond_1
    move v0, v2

    .line 320
    goto :goto_1

    :cond_2
    move v0, v2

    .line 321
    goto :goto_2

    :cond_3
    move v0, v2

    .line 322
    goto :goto_3

    :cond_4
    move v0, v2

    .line 323
    goto :goto_4

    :cond_5
    move v0, v2

    .line 324
    goto :goto_5

    :cond_6
    move v0, v2

    .line 325
    goto :goto_6

    :cond_7
    move v0, v2

    .line 326
    goto :goto_7

    :cond_8
    move v1, v2

    .line 327
    goto :goto_8
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 309
    const/4 v0, 0x0

    return v0
.end method

.method public isChat()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->chat:Z

    return v0
.end method

.method public isFileTransfer()Z
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->fileTransfer:Z

    return v0
.end method

.method public isGeoLocPush()Z
    .locals 1

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->geoLocPush:Z

    return v0
.end method

.method public isImageShare()Z
    .locals 1

    .prologue
    .line 213
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->imageShare:Z

    return v0
.end method

.method public isSendMms()Z
    .locals 1

    .prologue
    .line 282
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->sendMms:Z

    return v0
.end method

.method public isSendSms()Z
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->sendSms:Z

    return v0
.end method

.method public isVideoCall()Z
    .locals 1

    .prologue
    .line 236
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->videoCall:Z

    return v0
.end method

.method public isVideoShare()Z
    .locals 1

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->videoShare:Z

    return v0
.end method

.method public isVoiceCall()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->voiceCall:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 350
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->voiceCall:Z

    .line 351
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->chat:Z

    .line 352
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->sendSms:Z

    .line 353
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->fileTransfer:Z

    .line 354
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->videoShare:Z

    .line 355
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->imageShare:Z

    .line 356
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->videoCall:Z

    .line 357
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->geoLocPush:Z

    .line 358
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_8

    :goto_8
    iput-boolean v1, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->sendMms:Z

    .line 359
    return-void

    :cond_0
    move v0, v2

    .line 350
    goto :goto_0

    :cond_1
    move v0, v2

    .line 351
    goto :goto_1

    :cond_2
    move v0, v2

    .line 352
    goto :goto_2

    :cond_3
    move v0, v2

    .line 353
    goto :goto_3

    :cond_4
    move v0, v2

    .line 354
    goto :goto_4

    :cond_5
    move v0, v2

    .line 355
    goto :goto_5

    :cond_6
    move v0, v2

    .line 356
    goto :goto_6

    :cond_7
    move v0, v2

    .line 357
    goto :goto_7

    :cond_8
    move v1, v2

    .line 358
    goto :goto_8
.end method

.method public setChat(Z)V
    .locals 0
    .param p1, "chat"    # Z

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->chat:Z

    .line 134
    return-void
.end method

.method public setFileTransfer(Z)V
    .locals 0
    .param p1, "fileTransfer"    # Z

    .prologue
    .line 179
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->fileTransfer:Z

    .line 180
    return-void
.end method

.method public setGeoLocPush(Z)V
    .locals 0
    .param p1, "geoLocPush"    # Z

    .prologue
    .line 271
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->geoLocPush:Z

    .line 272
    return-void
.end method

.method public setImageShare(Z)V
    .locals 0
    .param p1, "imageShare"    # Z

    .prologue
    .line 225
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->imageShare:Z

    .line 226
    return-void
.end method

.method public setSendMms(Z)V
    .locals 0
    .param p1, "sendMms"    # Z

    .prologue
    .line 294
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->sendMms:Z

    .line 295
    return-void
.end method

.method public setSendSms(Z)V
    .locals 0
    .param p1, "sendSms"    # Z

    .prologue
    .line 156
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->sendSms:Z

    .line 157
    return-void
.end method

.method public setVideoCall(Z)V
    .locals 0
    .param p1, "videoCall"    # Z

    .prologue
    .line 248
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->videoCall:Z

    .line 249
    return-void
.end method

.method public setVideoShare(Z)V
    .locals 0
    .param p1, "videoShare"    # Z

    .prologue
    .line 202
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->videoShare:Z

    .line 203
    return-void
.end method

.method public setVoiceCall(Z)V
    .locals 0
    .param p1, "voiceCall"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/SecondaryDevice;->voiceCall:Z

    .line 111
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 313
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/SecondaryDevice;->writeToParcel(Landroid/os/Parcel;)V

    .line 315
    return-void
.end method
