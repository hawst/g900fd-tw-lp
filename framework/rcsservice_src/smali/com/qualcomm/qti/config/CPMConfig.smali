.class public Lcom/qualcomm/qti/config/CPMConfig;
.super Ljava/lang/Object;
.source "CPMConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/CPMConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private maxSizeStandalone:I

.field private messageStore:Lcom/qualcomm/qti/config/MessageststoreConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/qualcomm/qti/config/CPMConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/CPMConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/CPMConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/CPMConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/CPMConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/CPMConfig$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/CPMConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    return v0
.end method

.method public getMaxSizeStandalone()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/qualcomm/qti/config/CPMConfig;->maxSizeStandalone:I

    return v0
.end method

.method public getMessageStore()Lcom/qualcomm/qti/config/MessageststoreConfig;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/qualcomm/qti/config/CPMConfig;->messageStore:Lcom/qualcomm/qti/config/MessageststoreConfig;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/CPMConfig;->maxSizeStandalone:I

    .line 52
    const-class v0, Lcom/qualcomm/qti/config/MessageststoreConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/config/MessageststoreConfig;

    iput-object v0, p0, Lcom/qualcomm/qti/config/CPMConfig;->messageStore:Lcom/qualcomm/qti/config/MessageststoreConfig;

    .line 54
    return-void
.end method

.method public setMaxSizeStandalone(I)V
    .locals 0
    .param p1, "maxSizeStandalone"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/qualcomm/qti/config/CPMConfig;->maxSizeStandalone:I

    .line 62
    return-void
.end method

.method public setMessageStore(Lcom/qualcomm/qti/config/MessageststoreConfig;)V
    .locals 0
    .param p1, "messageStore"    # Lcom/qualcomm/qti/config/MessageststoreConfig;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/qualcomm/qti/config/CPMConfig;->messageStore:Lcom/qualcomm/qti/config/MessageststoreConfig;

    .line 70
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 29
    iget v0, p0, Lcom/qualcomm/qti/config/CPMConfig;->maxSizeStandalone:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 30
    iget-object v0, p0, Lcom/qualcomm/qti/config/CPMConfig;->messageStore:Lcom/qualcomm/qti/config/MessageststoreConfig;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 32
    return-void
.end method
