.class public interface abstract Lcom/qualcomm/qti/config/IQConfigServiceListener;
.super Ljava/lang/Object;
.source "IQConfigServiceListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/config/IQConfigServiceListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract QConfigServiceListener_ConfigSettingsUpdated(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigServiceListener_ReceivedFailureResponse(JLcom/qualcomm/qti/config/ConfigFailureMsg;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigServiceListener_ReceivedSuccessResponse(JLcom/qualcomm/qti/config/ConfigSuccessMsg;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigServiceListener_RequestSent(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QConfigServiceListener_ServiceCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
