.class public Lcom/qualcomm/qti/config/IMConfig;
.super Ljava/lang/Object;
.source "IMConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/IMConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private autAccept:Z

.field private autAcceptGroupChat:Z

.field private confFctyUri:Ljava/lang/String;

.field private deferredMsgFuncUri:Ljava/lang/String;

.field private exploderUri:Ljava/lang/String;

.field private firstMessageInvite:Z

.field private ftAutAccept:Z

.field private ftCapAlwaysON:Z

.field private ftDefaultMech:Ljava/lang/String;

.field private ftHTTPCSPwd:Ljava/lang/String;

.field private ftHTTPCSURI:Ljava/lang/String;

.field private ftHTTPCSUser:Ljava/lang/String;

.field private ftStAndFwEnabled:Z

.field private ftThumb:Z

.field private ftWarnSize:I

.field private groupChatFullStandFwd:Z

.field private groupChatOnlyFStandFwd:Z

.field private imCapAlwaysOn:Z

.field private imCapNonRCS:Z

.field private imMsgTech:Z

.field private imSessionStart:I

.field private imWarnIW:Z

.field private imWarnSF:Z

.field private maxAdhocGroupSize:I

.field private maxConcurrentSession:I

.field private maxSize1to1:I

.field private maxSize1toM:I

.field private maxSizeFileTr:I

.field private multiMediaChat:Z

.field private presSrvCap:Z

.field private smsFallBackAuth:Z

.field private timerIdle:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 758
    new-instance v0, Lcom/qualcomm/qti/config/IMConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/IMConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/IMConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 705
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 706
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 771
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 772
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/IMConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 773
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/IMConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/IMConfig$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/IMConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 720
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imMsgTech:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 721
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imCapAlwaysOn:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 722
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->groupChatFullStandFwd:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 723
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->groupChatOnlyFStandFwd:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 724
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imWarnSF:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 725
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->smsFallBackAuth:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 726
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imCapNonRCS:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 727
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imWarnIW:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 728
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->autAccept:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 729
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->autAcceptGroupChat:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 730
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imSessionStart:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 731
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->firstMessageInvite:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 732
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->timerIdle:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 733
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->maxConcurrentSession:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 734
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->multiMediaChat:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 735
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->maxSize1to1:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 736
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->maxSize1toM:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 737
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftWarnSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 738
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->maxSizeFileTr:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 739
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftThumb:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 740
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftStAndFwEnabled:Z

    if-eqz v0, :cond_d

    move v0, v1

    :goto_d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 741
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftCapAlwaysON:Z

    if-eqz v0, :cond_e

    move v0, v1

    :goto_e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 742
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftAutAccept:Z

    if-eqz v0, :cond_f

    move v0, v1

    :goto_f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 743
    iget-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftHTTPCSURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 744
    iget-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftHTTPCSUser:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 745
    iget-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftHTTPCSPwd:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 746
    iget-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftDefaultMech:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 747
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->presSrvCap:Z

    if-eqz v0, :cond_10

    :goto_10
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 748
    iget-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->deferredMsgFuncUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 749
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->maxAdhocGroupSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 750
    iget-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->confFctyUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 751
    iget-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->exploderUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 756
    return-void

    :cond_0
    move v0, v2

    .line 720
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 721
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 722
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 723
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 724
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 725
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 726
    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 727
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 728
    goto/16 :goto_8

    :cond_9
    move v0, v2

    .line 729
    goto/16 :goto_9

    :cond_a
    move v0, v2

    .line 731
    goto/16 :goto_a

    :cond_b
    move v0, v2

    .line 734
    goto/16 :goto_b

    :cond_c
    move v0, v2

    .line 739
    goto :goto_c

    :cond_d
    move v0, v2

    .line 740
    goto :goto_d

    :cond_e
    move v0, v2

    .line 741
    goto :goto_e

    :cond_f
    move v0, v2

    .line 742
    goto :goto_f

    :cond_10
    move v1, v2

    .line 747
    goto :goto_10
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 710
    const/4 v0, 0x0

    return v0
.end method

.method public getConfFctyUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 508
    iget-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->confFctyUri:Ljava/lang/String;

    return-object v0
.end method

.method public getDeferredMsgFuncUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->deferredMsgFuncUri:Ljava/lang/String;

    return-object v0
.end method

.method public getExploderUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 531
    iget-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->exploderUri:Ljava/lang/String;

    return-object v0
.end method

.method public getFtDefaultMech()Ljava/lang/String;
    .locals 1

    .prologue
    .line 691
    iget-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftDefaultMech:Ljava/lang/String;

    return-object v0
.end method

.method public getFtHTTPCSPwd()Ljava/lang/String;
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftHTTPCSPwd:Ljava/lang/String;

    return-object v0
.end method

.method public getFtHTTPCSURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 667
    iget-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftHTTPCSURI:Ljava/lang/String;

    return-object v0
.end method

.method public getFtHTTPCSUser()Ljava/lang/String;
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftHTTPCSUser:Ljava/lang/String;

    return-object v0
.end method

.method public getFtWarnSize()I
    .locals 1

    .prologue
    .line 619
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftWarnSize:I

    return v0
.end method

.method public getImSessionStart()I
    .locals 1

    .prologue
    .line 300
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imSessionStart:I

    return v0
.end method

.method public getMaxAdhocGroupSize()I
    .locals 1

    .prologue
    .line 485
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->maxAdhocGroupSize:I

    return v0
.end method

.method public getMaxConcurrentSession()I
    .locals 1

    .prologue
    .line 603
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->maxConcurrentSession:I

    return v0
.end method

.method public getMaxSize1to1()I
    .locals 1

    .prologue
    .line 371
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->maxSize1to1:I

    return v0
.end method

.method public getMaxSize1toM()I
    .locals 1

    .prologue
    .line 392
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->maxSize1toM:I

    return v0
.end method

.method public getMaxSizeFileTr()I
    .locals 1

    .prologue
    .line 627
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->maxSizeFileTr:I

    return v0
.end method

.method public getTimerIdle()I
    .locals 1

    .prologue
    .line 414
    iget v0, p0, Lcom/qualcomm/qti/config/IMConfig;->timerIdle:I

    return v0
.end method

.method public isAutAccept()Z
    .locals 1

    .prologue
    .line 349
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->autAccept:Z

    return v0
.end method

.method public isAutAcceptGroupChat()Z
    .locals 1

    .prologue
    .line 587
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->autAcceptGroupChat:Z

    return v0
.end method

.method public isFirstMessageInvite()Z
    .locals 1

    .prologue
    .line 595
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->firstMessageInvite:Z

    return v0
.end method

.method public isFtAutAccept()Z
    .locals 1

    .prologue
    .line 659
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftAutAccept:Z

    return v0
.end method

.method public isFtCapAlwaysON()Z
    .locals 1

    .prologue
    .line 651
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftCapAlwaysON:Z

    return v0
.end method

.method public isFtStAndFwEnabled()Z
    .locals 1

    .prologue
    .line 643
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftStAndFwEnabled:Z

    return v0
.end method

.method public isFtThumb()Z
    .locals 1

    .prologue
    .line 635
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftThumb:Z

    return v0
.end method

.method public isGroupChatFullStandFwd()Z
    .locals 1

    .prologue
    .line 555
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->groupChatFullStandFwd:Z

    return v0
.end method

.method public isGroupChatOnlyFStandFwd()Z
    .locals 1

    .prologue
    .line 563
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->groupChatOnlyFStandFwd:Z

    return v0
.end method

.method public isImCapAlwaysOn()Z
    .locals 1

    .prologue
    .line 254
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imCapAlwaysOn:Z

    return v0
.end method

.method public isImCapNonRCS()Z
    .locals 1

    .prologue
    .line 571
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imCapNonRCS:Z

    return v0
.end method

.method public isImMsgTech()Z
    .locals 1

    .prologue
    .line 547
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imMsgTech:Z

    return v0
.end method

.method public isImWarnIW()Z
    .locals 1

    .prologue
    .line 579
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imWarnIW:Z

    return v0
.end method

.method public isImWarnSF()Z
    .locals 1

    .prologue
    .line 277
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imWarnSF:Z

    return v0
.end method

.method public isMultiMediaChat()Z
    .locals 1

    .prologue
    .line 611
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->multiMediaChat:Z

    return v0
.end method

.method public isPresSrvCap()Z
    .locals 1

    .prologue
    .line 438
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->presSrvCap:Z

    return v0
.end method

.method public isSmsFallBackAuth()Z
    .locals 1

    .prologue
    .line 325
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->smsFallBackAuth:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 777
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imMsgTech:Z

    .line 778
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imCapAlwaysOn:Z

    .line 779
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->groupChatFullStandFwd:Z

    .line 780
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->groupChatOnlyFStandFwd:Z

    .line 781
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imWarnSF:Z

    .line 782
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->smsFallBackAuth:Z

    .line 783
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imCapNonRCS:Z

    .line 784
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imWarnIW:Z

    .line 785
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->autAccept:Z

    .line 786
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->autAcceptGroupChat:Z

    .line 787
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/IMConfig;->imSessionStart:I

    .line 788
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->firstMessageInvite:Z

    .line 789
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/IMConfig;->timerIdle:I

    .line 790
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/IMConfig;->maxConcurrentSession:I

    .line 791
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->multiMediaChat:Z

    .line 792
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/IMConfig;->maxSize1to1:I

    .line 793
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/IMConfig;->maxSize1toM:I

    .line 794
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftWarnSize:I

    .line 795
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/IMConfig;->maxSizeFileTr:I

    .line 796
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftThumb:Z

    .line 797
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftStAndFwEnabled:Z

    .line 798
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_e

    move v0, v1

    :goto_e
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftCapAlwaysON:Z

    .line 799
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_f

    move v0, v1

    :goto_f
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftAutAccept:Z

    .line 800
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftHTTPCSURI:Ljava/lang/String;

    .line 801
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftHTTPCSUser:Ljava/lang/String;

    .line 802
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftHTTPCSPwd:Ljava/lang/String;

    .line 803
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->ftDefaultMech:Ljava/lang/String;

    .line 804
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_10

    :goto_10
    iput-boolean v1, p0, Lcom/qualcomm/qti/config/IMConfig;->presSrvCap:Z

    .line 805
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->deferredMsgFuncUri:Ljava/lang/String;

    .line 806
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/IMConfig;->maxAdhocGroupSize:I

    .line 807
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->confFctyUri:Ljava/lang/String;

    .line 808
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/IMConfig;->exploderUri:Ljava/lang/String;

    .line 809
    return-void

    :cond_0
    move v0, v2

    .line 777
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 778
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 779
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 780
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 781
    goto/16 :goto_4

    :cond_5
    move v0, v2

    .line 782
    goto/16 :goto_5

    :cond_6
    move v0, v2

    .line 783
    goto/16 :goto_6

    :cond_7
    move v0, v2

    .line 784
    goto/16 :goto_7

    :cond_8
    move v0, v2

    .line 785
    goto/16 :goto_8

    :cond_9
    move v0, v2

    .line 786
    goto/16 :goto_9

    :cond_a
    move v0, v2

    .line 788
    goto/16 :goto_a

    :cond_b
    move v0, v2

    .line 791
    goto/16 :goto_b

    :cond_c
    move v0, v2

    .line 796
    goto :goto_c

    :cond_d
    move v0, v2

    .line 797
    goto :goto_d

    :cond_e
    move v0, v2

    .line 798
    goto :goto_e

    :cond_f
    move v0, v2

    .line 799
    goto :goto_f

    :cond_10
    move v1, v2

    .line 804
    goto :goto_10
.end method

.method public setAutAccept(Z)V
    .locals 0
    .param p1, "autAccept"    # Z

    .prologue
    .line 361
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->autAccept:Z

    .line 362
    return-void
.end method

.method public setAutAcceptGroupChat(Z)V
    .locals 0
    .param p1, "autAcceptGroupChat"    # Z

    .prologue
    .line 591
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->autAcceptGroupChat:Z

    .line 592
    return-void
.end method

.method public setConfFctyUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "confFctyUri"    # Ljava/lang/String;

    .prologue
    .line 520
    iput-object p1, p0, Lcom/qualcomm/qti/config/IMConfig;->confFctyUri:Ljava/lang/String;

    .line 521
    return-void
.end method

.method public setDeferredMsgFuncUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "deferredMsgFuncUri"    # Ljava/lang/String;

    .prologue
    .line 474
    iput-object p1, p0, Lcom/qualcomm/qti/config/IMConfig;->deferredMsgFuncUri:Ljava/lang/String;

    .line 475
    return-void
.end method

.method public setExploderUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "exploderUri"    # Ljava/lang/String;

    .prologue
    .line 543
    iput-object p1, p0, Lcom/qualcomm/qti/config/IMConfig;->exploderUri:Ljava/lang/String;

    .line 544
    return-void
.end method

.method public setFirstMessageInvite(Z)V
    .locals 0
    .param p1, "firstMessageInvite"    # Z

    .prologue
    .line 599
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->firstMessageInvite:Z

    .line 600
    return-void
.end method

.method public setFtAutAccept(Z)V
    .locals 0
    .param p1, "ftAutAccept"    # Z

    .prologue
    .line 663
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->ftAutAccept:Z

    .line 664
    return-void
.end method

.method public setFtCapAlwaysON(Z)V
    .locals 0
    .param p1, "ftCapAlwaysON"    # Z

    .prologue
    .line 655
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->ftCapAlwaysON:Z

    .line 656
    return-void
.end method

.method public setFtDefaultMech(Ljava/lang/String;)V
    .locals 0
    .param p1, "ftDefaultMech"    # Ljava/lang/String;

    .prologue
    .line 695
    iput-object p1, p0, Lcom/qualcomm/qti/config/IMConfig;->ftDefaultMech:Ljava/lang/String;

    .line 696
    return-void
.end method

.method public setFtHTTPCSPwd(Ljava/lang/String;)V
    .locals 0
    .param p1, "ftHTTPCSPwd"    # Ljava/lang/String;

    .prologue
    .line 687
    iput-object p1, p0, Lcom/qualcomm/qti/config/IMConfig;->ftHTTPCSPwd:Ljava/lang/String;

    .line 688
    return-void
.end method

.method public setFtHTTPCSURI(Ljava/lang/String;)V
    .locals 0
    .param p1, "ftHTTPCSURI"    # Ljava/lang/String;

    .prologue
    .line 671
    iput-object p1, p0, Lcom/qualcomm/qti/config/IMConfig;->ftHTTPCSURI:Ljava/lang/String;

    .line 672
    return-void
.end method

.method public setFtHTTPCSUser(Ljava/lang/String;)V
    .locals 0
    .param p1, "ftHTTPCSUser"    # Ljava/lang/String;

    .prologue
    .line 679
    iput-object p1, p0, Lcom/qualcomm/qti/config/IMConfig;->ftHTTPCSUser:Ljava/lang/String;

    .line 680
    return-void
.end method

.method public setFtStAndFwEnabled(Z)V
    .locals 0
    .param p1, "ftStAndFwEnabled"    # Z

    .prologue
    .line 647
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->ftStAndFwEnabled:Z

    .line 648
    return-void
.end method

.method public setFtThumb(Z)V
    .locals 0
    .param p1, "ftThumb"    # Z

    .prologue
    .line 639
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->ftThumb:Z

    .line 640
    return-void
.end method

.method public setFtWarnSize(I)V
    .locals 0
    .param p1, "ftWarnSize"    # I

    .prologue
    .line 623
    iput p1, p0, Lcom/qualcomm/qti/config/IMConfig;->ftWarnSize:I

    .line 624
    return-void
.end method

.method public setGroupChatFullStandFwd(Z)V
    .locals 0
    .param p1, "groupChatFullStandFwd"    # Z

    .prologue
    .line 559
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->groupChatFullStandFwd:Z

    .line 560
    return-void
.end method

.method public setGroupChatOnlyFStandFwd(Z)V
    .locals 0
    .param p1, "groupChatOnlyFStandFwd"    # Z

    .prologue
    .line 567
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->groupChatOnlyFStandFwd:Z

    .line 568
    return-void
.end method

.method public setImCapAlwaysOn(Z)V
    .locals 0
    .param p1, "imCapAlwaysOn"    # Z

    .prologue
    .line 266
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->imCapAlwaysOn:Z

    .line 267
    return-void
.end method

.method public setImCapNonRCS(Z)V
    .locals 0
    .param p1, "imCapNonRCS"    # Z

    .prologue
    .line 575
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->imCapNonRCS:Z

    .line 576
    return-void
.end method

.method public setImMsgTech(Z)V
    .locals 0
    .param p1, "imMsgTech"    # Z

    .prologue
    .line 551
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->imMsgTech:Z

    .line 552
    return-void
.end method

.method public setImSessionStart(I)V
    .locals 0
    .param p1, "imSessionStart"    # I

    .prologue
    .line 313
    iput p1, p0, Lcom/qualcomm/qti/config/IMConfig;->imSessionStart:I

    .line 314
    return-void
.end method

.method public setImWarnIW(Z)V
    .locals 0
    .param p1, "imWarnIW"    # Z

    .prologue
    .line 583
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->imWarnIW:Z

    .line 584
    return-void
.end method

.method public setImWarnSF(Z)V
    .locals 0
    .param p1, "imWarnSF"    # Z

    .prologue
    .line 289
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->imWarnSF:Z

    .line 290
    return-void
.end method

.method public setMaxAdhocGroupSize(I)V
    .locals 0
    .param p1, "maxAdhocGroupSize"    # I

    .prologue
    .line 497
    iput p1, p0, Lcom/qualcomm/qti/config/IMConfig;->maxAdhocGroupSize:I

    .line 498
    return-void
.end method

.method public setMaxConcurrentSession(I)V
    .locals 0
    .param p1, "maxConcurrentSession"    # I

    .prologue
    .line 607
    iput p1, p0, Lcom/qualcomm/qti/config/IMConfig;->maxConcurrentSession:I

    .line 608
    return-void
.end method

.method public setMaxSize1to1(I)V
    .locals 0
    .param p1, "maxSize1to1"    # I

    .prologue
    .line 382
    iput p1, p0, Lcom/qualcomm/qti/config/IMConfig;->maxSize1to1:I

    .line 383
    return-void
.end method

.method public setMaxSize1toM(I)V
    .locals 0
    .param p1, "maxSize1toM"    # I

    .prologue
    .line 403
    iput p1, p0, Lcom/qualcomm/qti/config/IMConfig;->maxSize1toM:I

    .line 404
    return-void
.end method

.method public setMaxSizeFileTr(I)V
    .locals 0
    .param p1, "maxSizeFileTr"    # I

    .prologue
    .line 631
    iput p1, p0, Lcom/qualcomm/qti/config/IMConfig;->maxSizeFileTr:I

    .line 632
    return-void
.end method

.method public setMultiMediaChat(Z)V
    .locals 0
    .param p1, "multiMediaChat"    # Z

    .prologue
    .line 615
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->multiMediaChat:Z

    .line 616
    return-void
.end method

.method public setPresSrvCap(Z)V
    .locals 0
    .param p1, "presSrvCap"    # Z

    .prologue
    .line 451
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->presSrvCap:Z

    .line 452
    return-void
.end method

.method public setSmsFallBackAuth(Z)V
    .locals 0
    .param p1, "smsFallBackAuth"    # Z

    .prologue
    .line 339
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/IMConfig;->smsFallBackAuth:Z

    .line 340
    return-void
.end method

.method public setTimerIdle(I)V
    .locals 0
    .param p1, "timerIdle"    # I

    .prologue
    .line 426
    iput p1, p0, Lcom/qualcomm/qti/config/IMConfig;->timerIdle:I

    .line 427
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 714
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/IMConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 716
    return-void
.end method
