.class public Lcom/qualcomm/qti/config/ConfigSuccessMsg;
.super Ljava/lang/Object;
.source "ConfigSuccessMsg.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/ConfigSuccessMsg;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private acceptBtn:Z

.field private message:Ljava/lang/String;

.field private rejectBtn:Z

.field private title:Ljava/lang/String;

.field private validity:I

.field private version:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 213
    new-instance v0, Lcom/qualcomm/qti/config/ConfigSuccessMsg$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/ConfigSuccessMsg$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0
    .param p1, "validity"    # I
    .param p2, "version"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "acceptBtn"    # Z
    .param p6, "rejectBtn"    # Z

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->validity:I

    .line 42
    iput-object p2, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->version:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->title:Ljava/lang/String;

    .line 44
    iput-object p4, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->message:Ljava/lang/String;

    .line 45
    iput-boolean p5, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->acceptBtn:Z

    .line 46
    iput-boolean p6, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->rejectBtn:Z

    .line 47
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 227
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->readFromParcel(Landroid/os/Parcel;)V

    .line 228
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/ConfigSuccessMsg$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/ConfigSuccessMsg$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/ConfigSuccessMsg;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getConfigSuccessMsgInstance()Lcom/qualcomm/qti/config/ConfigSuccessMsg;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/ConfigSuccessMsg;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 205
    iget v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->validity:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 206
    iget-object v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->version:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->message:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 209
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->acceptBtn:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 210
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->rejectBtn:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 211
    return-void

    :cond_0
    move v0, v2

    .line 209
    goto :goto_0

    :cond_1
    move v1, v2

    .line 210
    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x0

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getValidity()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->validity:I

    return v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->version:Ljava/lang/String;

    return-object v0
.end method

.method public isAcceptBtn()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->acceptBtn:Z

    return v0
.end method

.method public isRejectBtn()Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->rejectBtn:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 232
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->validity:I

    .line 233
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->version:Ljava/lang/String;

    .line 234
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->title:Ljava/lang/String;

    .line 235
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->message:Ljava/lang/String;

    .line 236
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->acceptBtn:Z

    .line 237
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->rejectBtn:Z

    .line 238
    return-void

    :cond_0
    move v0, v2

    .line 236
    goto :goto_0

    :cond_1
    move v1, v2

    .line 237
    goto :goto_1
.end method

.method public setAcceptBtn(Z)V
    .locals 0
    .param p1, "acceptBtn"    # Z

    .prologue
    .line 164
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->acceptBtn:Z

    .line 165
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->message:Ljava/lang/String;

    .line 143
    return-void
.end method

.method public setRejectBtn(Z)V
    .locals 0
    .param p1, "rejectBtn"    # Z

    .prologue
    .line 186
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->rejectBtn:Z

    .line 187
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->title:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public setValidity(I)V
    .locals 0
    .param p1, "validity"    # I

    .prologue
    .line 79
    iput p1, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->validity:I

    .line 80
    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->version:Ljava/lang/String;

    .line 101
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 199
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->writeToParcel(Landroid/os/Parcel;)V

    .line 201
    return-void
.end method
