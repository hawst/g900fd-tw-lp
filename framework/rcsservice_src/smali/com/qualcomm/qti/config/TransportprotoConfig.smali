.class public Lcom/qualcomm/qti/config/TransportprotoConfig;
.super Ljava/lang/Object;
.source "TransportprotoConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;,
        Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;,
        Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/TransportprotoConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private ePsMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

.field private ePsRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

.field private ePsSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

.field private eWifiMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

.field private eWifiRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

.field private eWifiSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 407
    new-instance v0, Lcom/qualcomm/qti/config/TransportprotoConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/TransportprotoConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 384
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 421
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/TransportprotoConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 422
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/TransportprotoConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/TransportprotoConfig$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/TransportprotoConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 398
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 399
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 400
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 401
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    if-nez v0, :cond_3

    const-string v0, ""

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 402
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    if-nez v0, :cond_4

    const-string v0, ""

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 403
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    if-nez v0, :cond_5

    const-string v0, ""

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 405
    return-void

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    invoke-virtual {v0}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 399
    :cond_1
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    invoke-virtual {v0}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 400
    :cond_2
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    invoke-virtual {v0}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 401
    :cond_3
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    invoke-virtual {v0}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 402
    :cond_4
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    invoke-virtual {v0}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 403
    :cond_5
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    invoke-virtual {v0}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x0

    return v0
.end method

.method public getPsMedia()Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    return-object v0
.end method

.method public getPsRTMedia()Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    return-object v0
.end method

.method public getPsSignalling()Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    return-object v0
.end method

.method public getWifiMedia()Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    return-object v0
.end method

.method public getWifiRTMedia()Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    return-object v0
.end method

.method public getWifiSignalling()Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 427
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 433
    :goto_0
    :try_start_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 439
    :goto_1
    :try_start_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    .line 445
    :goto_2
    :try_start_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_3

    .line 451
    :goto_3
    :try_start_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_4

    .line 458
    :goto_4
    :try_start_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_5

    .line 462
    :goto_5
    return-void

    .line 428
    :catch_0
    move-exception v0

    .line 429
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    iput-object v1, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    goto :goto_0

    .line 434
    .end local v0    # "x":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 435
    .restart local v0    # "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    iput-object v1, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    goto :goto_1

    .line 440
    .end local v0    # "x":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 441
    .restart local v0    # "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->QRCS_RT_MEDIA_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    iput-object v1, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    goto :goto_2

    .line 446
    .end local v0    # "x":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v0

    .line 447
    .restart local v0    # "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    iput-object v1, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    goto :goto_3

    .line 452
    .end local v0    # "x":Ljava/lang/IllegalArgumentException;
    :catch_4
    move-exception v0

    .line 453
    .restart local v0    # "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    iput-object v1, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    goto :goto_4

    .line 459
    .end local v0    # "x":Ljava/lang/IllegalArgumentException;
    :catch_5
    move-exception v0

    .line 460
    .restart local v0    # "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->QRCS_RT_MEDIA_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    iput-object v1, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    goto :goto_5
.end method

.method public setPsMedia(I)V
    .locals 3
    .param p1, "ePsMedia"    # I

    .prologue
    .line 168
    sparse-switch p1, :sswitch_data_0

    .line 187
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    .line 191
    :goto_0
    return-void

    .line 171
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_MSRP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    goto :goto_0

    .line 175
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_MSRP_OVER_TLS:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    goto :goto_0

    .line 179
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_INVALID:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    goto :goto_0

    .line 183
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_MAX32:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    goto :goto_0

    .line 168
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x10000000 -> :sswitch_3
    .end sparse-switch
.end method

.method public setPsRTMedia(I)V
    .locals 3
    .param p1, "ePsRTMedia"    # I

    .prologue
    .line 213
    sparse-switch p1, :sswitch_data_0

    .line 232
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->QRCS_RT_MEDIA_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    .line 236
    :goto_0
    return-void

    .line 216
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->QRCS_RT_MEDIA_PROTO_RTP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    goto :goto_0

    .line 220
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->QRCS_RT_MEDIA_PROTO_SRTP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    goto :goto_0

    .line 224
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->QRCS_RT_MEDIA_PROTO_INVALID:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    goto :goto_0

    .line 228
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->QRCS_RT_MEDIA_PROTO_MAX32:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    goto :goto_0

    .line 213
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x10000000 -> :sswitch_3
    .end sparse-switch
.end method

.method public setPsSignalling(I)V
    .locals 3
    .param p1, "ePsSignalling"    # I

    .prologue
    .line 120
    sparse-switch p1, :sswitch_data_0

    .line 142
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    .line 146
    :goto_0
    return-void

    .line 123
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_SIP_OVER_UDP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    goto :goto_0

    .line 127
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_SIP_OVER_TCP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    goto :goto_0

    .line 131
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_SIP_OVER_TLS:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    goto :goto_0

    .line 134
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_INVALID:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    goto :goto_0

    .line 138
    :sswitch_4
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_MAX32:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->ePsSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    goto :goto_0

    .line 120
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x10000000 -> :sswitch_4
    .end sparse-switch
.end method

.method public setWifiMedia(I)V
    .locals 3
    .param p1, "eWifiMedia"    # I

    .prologue
    .line 306
    sparse-switch p1, :sswitch_data_0

    .line 325
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    .line 329
    :goto_0
    return-void

    .line 309
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_MSRP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    goto :goto_0

    .line 313
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_MSRP_OVER_TLS:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    goto :goto_0

    .line 317
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_INVALID:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    goto :goto_0

    .line 321
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_MAX32:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    goto :goto_0

    .line 306
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x10000000 -> :sswitch_3
    .end sparse-switch
.end method

.method public setWifiRTMedia(I)V
    .locals 3
    .param p1, "eWifiRTMedia"    # I

    .prologue
    .line 351
    sparse-switch p1, :sswitch_data_0

    .line 370
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->QRCS_RT_MEDIA_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    .line 374
    :goto_0
    return-void

    .line 354
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->QRCS_RT_MEDIA_PROTO_RTP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    goto :goto_0

    .line 358
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->QRCS_RT_MEDIA_PROTO_SRTP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    goto :goto_0

    .line 362
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->QRCS_RT_MEDIA_PROTO_INVALID:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    goto :goto_0

    .line 366
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;->QRCS_RT_MEDIA_PROTO_MAX32:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiRTMedia:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_RT_MEDIA_PROTO;

    goto :goto_0

    .line 351
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x10000000 -> :sswitch_3
    .end sparse-switch
.end method

.method public setWifiSignalling(I)V
    .locals 3
    .param p1, "eWifiSignalling"    # I

    .prologue
    .line 258
    sparse-switch p1, :sswitch_data_0

    .line 280
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    .line 284
    :goto_0
    return-void

    .line 261
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_SIP_OVER_UDP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    goto :goto_0

    .line 265
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_SIP_OVER_TCP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    goto :goto_0

    .line 269
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_SIP_OVER_TLS:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    goto :goto_0

    .line 272
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_INVALID:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    goto :goto_0

    .line 276
    :sswitch_4
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_MAX32:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    iput-object v0, p0, Lcom/qualcomm/qti/config/TransportprotoConfig;->eWifiSignalling:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    goto :goto_0

    .line 258
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x10000000 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 392
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/TransportprotoConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 394
    return-void
.end method
