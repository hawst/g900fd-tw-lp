.class public Lcom/qualcomm/qti/config/SuplConfig;
.super Ljava/lang/Object;
.source "SuplConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/SuplConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private ext:Lcom/qualcomm/qti/config/GetextConfig;

.field private geolocPullApiGwAddress:Ljava/lang/String;

.field private geolocPullBlockTimer:I

.field private geolocPullOpen:Z

.field private locInfoMaxValidTime:I

.field private textMaxLength:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/qualcomm/qti/config/SuplConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/SuplConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/SuplConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/SuplConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 76
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/SuplConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/SuplConfig$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/SuplConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public getExt()Lcom/qualcomm/qti/config/GetextConfig;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->ext:Lcom/qualcomm/qti/config/GetextConfig;

    return-object v0
.end method

.method public getGeolocPullApiGwAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->geolocPullApiGwAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getGeolocPullBlockTimer()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->geolocPullBlockTimer:I

    return v0
.end method

.method public getLocInfoMaxValidTime()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->locInfoMaxValidTime:I

    return v0
.end method

.method public getTextMaxLength()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->textMaxLength:I

    return v0
.end method

.method public isGeolocPullOpen()Z
    .locals 1

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->geolocPullOpen:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->textMaxLength:I

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->locInfoMaxValidTime:I

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->geolocPullOpen:Z

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->geolocPullApiGwAddress:Ljava/lang/String;

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->geolocPullBlockTimer:I

    .line 85
    const-class v0, Lcom/qualcomm/qti/config/GetextConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/config/GetextConfig;

    iput-object v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->ext:Lcom/qualcomm/qti/config/GetextConfig;

    .line 86
    return-void

    .line 82
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setExt(Lcom/qualcomm/qti/config/GetextConfig;)V
    .locals 0
    .param p1, "ext"    # Lcom/qualcomm/qti/config/GetextConfig;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/qualcomm/qti/config/SuplConfig;->ext:Lcom/qualcomm/qti/config/GetextConfig;

    .line 134
    return-void
.end method

.method public setGeolocPullApiGwAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "geolocPullApiGwAddress"    # Ljava/lang/String;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/qualcomm/qti/config/SuplConfig;->geolocPullApiGwAddress:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public setGeolocPullBlockTimer(I)V
    .locals 0
    .param p1, "geolocPullBlockTimer"    # I

    .prologue
    .line 125
    iput p1, p0, Lcom/qualcomm/qti/config/SuplConfig;->geolocPullBlockTimer:I

    .line 126
    return-void
.end method

.method public setGeolocPullOpen(Z)V
    .locals 0
    .param p1, "geolocPullOpen"    # Z

    .prologue
    .line 109
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/SuplConfig;->geolocPullOpen:Z

    .line 110
    return-void
.end method

.method public setLocInfoMaxValidTime(I)V
    .locals 0
    .param p1, "locInfoMaxValidTime"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/qualcomm/qti/config/SuplConfig;->locInfoMaxValidTime:I

    .line 102
    return-void
.end method

.method public setTextMaxLength(I)V
    .locals 0
    .param p1, "textMaxLength"    # I

    .prologue
    .line 93
    iput p1, p0, Lcom/qualcomm/qti/config/SuplConfig;->textMaxLength:I

    .line 94
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 53
    iget v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->textMaxLength:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 54
    iget v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->locInfoMaxValidTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 55
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->geolocPullOpen:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 56
    iget-object v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->geolocPullApiGwAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 57
    iget v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->geolocPullBlockTimer:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 58
    iget-object v0, p0, Lcom/qualcomm/qti/config/SuplConfig;->ext:Lcom/qualcomm/qti/config/GetextConfig;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 60
    return-void

    .line 55
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
