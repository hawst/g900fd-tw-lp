.class public Lcom/qualcomm/qti/config/LboPCscfAddressConfig;
.super Ljava/lang/Object;
.source "LboPCscfAddressConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/LboPCscfAddressConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private address:Ljava/lang/String;

.field private eAddressType:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->address:Ljava/lang/String;

    .line 114
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->address:Ljava/lang/String;

    .line 146
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 147
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/LboPCscfAddressConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/LboPCscfAddressConfig$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->address:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->eAddressType:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 130
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->eAddressType:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    invoke-virtual {v0}, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getAddressType()Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->eAddressType:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->address:Ljava/lang/String;

    .line 153
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->eAddressType:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    :goto_0
    return-void

    .line 154
    :catch_0
    move-exception v0

    .line 155
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_UNKNOWN:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    iput-object v1, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->eAddressType:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    goto :goto_0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->address:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public setAddressType(I)V
    .locals 3
    .param p1, "eAddressType"    # I

    .prologue
    .line 61
    sparse-switch p1, :sswitch_data_0

    .line 79
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    sget-object v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_UNKNOWN:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    iput-object v0, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->eAddressType:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    .line 83
    :goto_0
    return-void

    .line 64
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_FQDN:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    iput-object v0, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->eAddressType:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    goto :goto_0

    .line 67
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_IPV4:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    iput-object v0, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->eAddressType:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    goto :goto_0

    .line 70
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_IPV6:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    iput-object v0, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->eAddressType:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    goto :goto_0

    .line 73
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_INVALID:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    iput-object v0, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->eAddressType:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    goto :goto_0

    .line 76
    :sswitch_4
    sget-object v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_MAX32:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    iput-object v0, p0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->eAddressType:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    goto :goto_0

    .line 61
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x10000000 -> :sswitch_4
    .end sparse-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/LboPCscfAddressConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 124
    return-void
.end method
