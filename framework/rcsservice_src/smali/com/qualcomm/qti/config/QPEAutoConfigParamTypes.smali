.class public Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
.super Ljava/lang/Object;
.source "QPEAutoConfigParamTypes.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->readFromParcel(Landroid/os/Parcel;)V

    .line 71
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method public getEnumValueAsInt()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    invoke-virtual {v0}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->ordinal()I

    move-result v0

    return v0
.end method

.method public getQPEAutoConfigParamTypesValue()Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 77
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_0
    return-void

    .line 79
    :catch_0
    move-exception v0

    .line 80
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_UNKNOWN:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    iput-object v1, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    goto :goto_0
.end method

.method public setQPEAutoConfigParamTypesValue(I)V
    .locals 3
    .param p1, "eQpeAutoConfigParamTypes"    # I

    .prologue
    .line 114
    packed-switch p1, :pswitch_data_0

    .line 153
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    sget-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_UNKNOWN:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    iput-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    .line 159
    :goto_0
    return-void

    .line 117
    :pswitch_0
    sget-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_VERS_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    iput-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    goto :goto_0

    .line 120
    :pswitch_1
    sget-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_MSG_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    iput-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    goto :goto_0

    .line 123
    :pswitch_2
    sget-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_IMS_SETTINGS_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    iput-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    goto :goto_0

    .line 126
    :pswitch_3
    sget-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_SERVICES_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    iput-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    goto :goto_0

    .line 129
    :pswitch_4
    sget-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_PRESENCE_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    iput-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    goto :goto_0

    .line 132
    :pswitch_5
    sget-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_XDMS_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    iput-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    goto :goto_0

    .line 135
    :pswitch_6
    sget-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_SUPL_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    iput-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    goto :goto_0

    .line 138
    :pswitch_7
    sget-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_IM_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    iput-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    goto :goto_0

    .line 141
    :pswitch_8
    sget-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_CPM_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    iput-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    goto :goto_0

    .line 144
    :pswitch_9
    sget-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_CAPDISCOVERY_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    iput-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    goto :goto_0

    .line 147
    :pswitch_a
    sget-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_APN_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    iput-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    goto :goto_0

    .line 150
    :pswitch_b
    sget-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_OTHER_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    iput-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    goto :goto_0

    .line 114
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 55
    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->eQpeAutoConfigParamTypes:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    invoke-virtual {v0}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
