.class public final enum Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;
.super Ljava/lang/Enum;
.source "TransportprotoConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/config/TransportprotoConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QRCS_SGN_PROTO"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

.field public static final enum QRCS_SGN_PROTO_INVALID:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

.field public static final enum QRCS_SGN_PROTO_MAX32:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

.field public static final enum QRCS_SGN_PROTO_SIP_OVER_TCP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

.field public static final enum QRCS_SGN_PROTO_SIP_OVER_TLS:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

.field public static final enum QRCS_SGN_PROTO_SIP_OVER_UDP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

.field public static final enum QRCS_SGN_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 17
    new-instance v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    const-string v1, "QRCS_SGN_PROTO_SIP_OVER_UDP"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_SIP_OVER_UDP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    .line 19
    new-instance v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    const-string v1, "QRCS_SGN_PROTO_SIP_OVER_TCP"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_SIP_OVER_TCP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    .line 21
    new-instance v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    const-string v1, "QRCS_SGN_PROTO_SIP_OVER_TLS"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_SIP_OVER_TLS:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    .line 23
    new-instance v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    const-string v1, "QRCS_SGN_PROTO_INVALID"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_INVALID:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    .line 25
    new-instance v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    const-string v1, "QRCS_SGN_PROTO_MAX32"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_MAX32:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    .line 26
    new-instance v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    const-string v1, "QRCS_SGN_PROTO_UNKNOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    .line 15
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_SIP_OVER_UDP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_SIP_OVER_TCP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_SIP_OVER_TLS:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_INVALID:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_MAX32:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->QRCS_SGN_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->$VALUES:[Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->$VALUES:[Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_SGN_PROTO;

    return-object v0
.end method
