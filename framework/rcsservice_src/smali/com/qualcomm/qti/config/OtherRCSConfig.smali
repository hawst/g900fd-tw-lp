.class public Lcom/qualcomm/qti/config/OtherRCSConfig;
.super Ljava/lang/Object;
.source "OtherRCSConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/OtherRCSConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private allowVSSave:I

.field private endUserConfReqId:Ljava/lang/String;

.field private ipCallBreakOut:I

.field private ipCallBreakOutCS:I

.field private rcsIPVideoCallUpgradeAttemptEarly:Z

.field private rcsIPVideoCallUpgradeFromCS:I

.field private rcsIPVideoCallUpgradeOnCapError:Z

.field private transportProto:Lcom/qualcomm/qti/config/TransportprotoConfig;

.field private useruidValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Lcom/qualcomm/qti/config/OtherRCSConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/OtherRCSConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/OtherRCSConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/OtherRCSConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 111
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/OtherRCSConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/OtherRCSConfig$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/OtherRCSConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 85
    iget-object v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->endUserConfReqId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 86
    iget v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->allowVSSave:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 87
    iget-object v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->transportProto:Lcom/qualcomm/qti/config/TransportprotoConfig;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 88
    iget-object v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->useruidValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 89
    iget v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->ipCallBreakOut:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 90
    iget v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->ipCallBreakOutCS:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    iget v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->rcsIPVideoCallUpgradeFromCS:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->rcsIPVideoCallUpgradeOnCapError:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 93
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->rcsIPVideoCallUpgradeAttemptEarly:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    return-void

    :cond_0
    move v0, v2

    .line 92
    goto :goto_0

    :cond_1
    move v1, v2

    .line 93
    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public getAllowVSSave()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->allowVSSave:I

    return v0
.end method

.method public getEndUserConfReqId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->endUserConfReqId:Ljava/lang/String;

    return-object v0
.end method

.method public getIPCallBreakOut()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->ipCallBreakOut:I

    return v0
.end method

.method public getIPCallBreakOutCS()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->ipCallBreakOutCS:I

    return v0
.end method

.method public getRcsIPVideoCallUpgradeFromCS()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->rcsIPVideoCallUpgradeFromCS:I

    return v0
.end method

.method public getTransportProto()Lcom/qualcomm/qti/config/TransportprotoConfig;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->transportProto:Lcom/qualcomm/qti/config/TransportprotoConfig;

    return-object v0
.end method

.method public getUseruidValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->useruidValue:Ljava/lang/String;

    return-object v0
.end method

.method public isRcsIPVideoCallUpgradeAttemptEarly()Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->rcsIPVideoCallUpgradeAttemptEarly:Z

    return v0
.end method

.method public isRcsIPVideoCallUpgradeOnCapError()Z
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->rcsIPVideoCallUpgradeOnCapError:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->endUserConfReqId:Ljava/lang/String;

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->allowVSSave:I

    .line 116
    const-class v0, Lcom/qualcomm/qti/config/TransportprotoConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/config/TransportprotoConfig;

    iput-object v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->transportProto:Lcom/qualcomm/qti/config/TransportprotoConfig;

    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->useruidValue:Ljava/lang/String;

    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->ipCallBreakOut:I

    .line 120
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->ipCallBreakOutCS:I

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->rcsIPVideoCallUpgradeFromCS:I

    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->rcsIPVideoCallUpgradeOnCapError:Z

    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->rcsIPVideoCallUpgradeAttemptEarly:Z

    .line 124
    return-void

    :cond_0
    move v0, v2

    .line 122
    goto :goto_0

    :cond_1
    move v1, v2

    .line 123
    goto :goto_1
.end method

.method public setAllowVSSave(I)V
    .locals 0
    .param p1, "iAllowVSSave"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->allowVSSave:I

    .line 142
    return-void
.end method

.method public setEndUserConfReqId(Ljava/lang/String;)V
    .locals 0
    .param p1, "endUserConfReqId"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->endUserConfReqId:Ljava/lang/String;

    .line 134
    return-void
.end method

.method public setIPCallBreakOut(I)V
    .locals 0
    .param p1, "ipCallBreakOut"    # I

    .prologue
    .line 165
    iput p1, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->ipCallBreakOut:I

    .line 166
    return-void
.end method

.method public setIPCallBreakOutCS(I)V
    .locals 0
    .param p1, "ipCallBreakOutCS"    # I

    .prologue
    .line 173
    iput p1, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->ipCallBreakOutCS:I

    .line 174
    return-void
.end method

.method public setRcsIPVideoCallUpgradeAttemptEarly(Z)V
    .locals 0
    .param p1, "rcsIPVideoCallUpgradeAttemptEarly"    # Z

    .prologue
    .line 197
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->rcsIPVideoCallUpgradeAttemptEarly:Z

    .line 198
    return-void
.end method

.method public setRcsIPVideoCallUpgradeFromCS(I)V
    .locals 0
    .param p1, "rcsIPVideoCallUpgradeFromCS"    # I

    .prologue
    .line 181
    iput p1, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->rcsIPVideoCallUpgradeFromCS:I

    .line 182
    return-void
.end method

.method public setRcsIPVideoCallUpgradeOnCapError(Z)V
    .locals 0
    .param p1, "rcsIPVideoCallUpgradeOnCapError"    # Z

    .prologue
    .line 189
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->rcsIPVideoCallUpgradeOnCapError:Z

    .line 190
    return-void
.end method

.method public setTransportProto(Lcom/qualcomm/qti/config/TransportprotoConfig;)V
    .locals 0
    .param p1, "transportProto"    # Lcom/qualcomm/qti/config/TransportprotoConfig;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->transportProto:Lcom/qualcomm/qti/config/TransportprotoConfig;

    .line 150
    return-void
.end method

.method public setUseruidValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "useruidValue"    # Ljava/lang/String;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/qualcomm/qti/config/OtherRCSConfig;->useruidValue:Ljava/lang/String;

    .line 158
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/OtherRCSConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 81
    return-void
.end method
