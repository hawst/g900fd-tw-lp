.class public abstract Lcom/qualcomm/qti/config/IQConfigService$Stub;
.super Landroid/os/Binder;
.source "IQConfigService.java"

# interfaces
.implements Lcom/qualcomm/qti/config/IQConfigService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/config/IQConfigService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.config.IQConfigService"

.field static final TRANSACTION_QConfigService_AcceptEvent:I = 0x11

.field static final TRANSACTION_QConfigService_AddListener:I = 0x2

.field static final TRANSACTION_QConfigService_GetVersion:I = 0x1

.field static final TRANSACTION_QConfigService_GetXMLConfigApnValue:I = 0xe

.field static final TRANSACTION_QConfigService_GetXMLConfigCdValue:I = 0xd

.field static final TRANSACTION_QConfigService_GetXMLConfigCpmValue:I = 0xc

.field static final TRANSACTION_QConfigService_GetXMLConfigImValue:I = 0xb

.field static final TRANSACTION_QConfigService_GetXMLConfigImsSettingsValue:I = 0x6

.field static final TRANSACTION_QConfigService_GetXMLConfigMsgValue:I = 0x5

.field static final TRANSACTION_QConfigService_GetXMLConfigOtherRcsValue:I = 0xf

.field static final TRANSACTION_QConfigService_GetXMLConfigPresenceValue:I = 0x8

.field static final TRANSACTION_QConfigService_GetXMLConfigServicesValue:I = 0x7

.field static final TRANSACTION_QConfigService_GetXMLConfigSuplValue:I = 0xa

.field static final TRANSACTION_QConfigService_GetXMLConfigVersionValue:I = 0x4

.field static final TRANSACTION_QConfigService_GetXMLConfigXdmsValue:I = 0x9

.field static final TRANSACTION_QConfigService_RemoveListener:I = 0x3

.field static final TRANSACTION_QConfigService_SetDispositionConfig:I = 0x10


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/config/IQConfigService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.config.IQConfigService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/config/IQConfigService;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/config/IQConfigService;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/config/IQConfigService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 599
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v5

    :goto_0
    return v5

    .line 44
    :sswitch_0
    const-string v6, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 53
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_0

    .line 54
    sget-object v7, Lcom/qualcomm/qti/rcsservice/VersionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/VersionInfo;

    .line 59
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    :goto_1
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_GetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 60
    .local v4, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 61
    if-eqz v4, :cond_1

    .line 62
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 63
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 68
    :goto_2
    if-eqz v2, :cond_2

    .line 69
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    invoke-virtual {v2, p3, v5}, Lcom/qualcomm/qti/rcsservice/VersionInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 57
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    goto :goto_1

    .line 66
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    .line 73
    :cond_2
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 79
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_2
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 83
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Lcom/qualcomm/qti/config/IQConfigServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/config/IQConfigServiceListener;

    move-result-object v2

    .line 84
    .local v2, "_arg1":Lcom/qualcomm/qti/config/IQConfigServiceListener;
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_AddListener(ILcom/qualcomm/qti/config/IQConfigServiceListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 85
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 86
    if-eqz v4, :cond_3

    .line 87
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 88
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 91
    :cond_3
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 97
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/IQConfigServiceListener;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_3
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 99
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 101
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 102
    .local v2, "_arg1":J
    invoke-virtual {p0, v0, v2, v3}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 103
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 104
    if-eqz v4, :cond_4

    .line 105
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 106
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 109
    :cond_4
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 115
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":J
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_4
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 119
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_5

    .line 120
    sget-object v7, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;

    .line 126
    .local v2, "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_6

    .line 127
    sget-object v7, Lcom/qualcomm/qti/config/VersionConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/config/VersionConfig;

    .line 132
    .local v1, "_arg2":Lcom/qualcomm/qti/config/VersionConfig;
    :goto_4
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_GetXMLConfigVersionValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/VersionConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 133
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 134
    if-eqz v4, :cond_7

    .line 135
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 136
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 141
    :goto_5
    if-eqz v1, :cond_8

    .line 142
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 143
    invoke-virtual {v1, p3, v5}, Lcom/qualcomm/qti/config/VersionConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 123
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/VersionConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_5
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    goto :goto_3

    .line 130
    :cond_6
    const/4 v1, 0x0

    .restart local v1    # "_arg2":Lcom/qualcomm/qti/config/VersionConfig;
    goto :goto_4

    .line 139
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_7
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_5

    .line 146
    :cond_8
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 152
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/VersionConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_5
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 154
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 156
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_9

    .line 157
    sget-object v7, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;

    .line 163
    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_a

    .line 164
    sget-object v7, Lcom/qualcomm/qti/config/MsgConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/config/MsgConfig;

    .line 169
    .local v1, "_arg2":Lcom/qualcomm/qti/config/MsgConfig;
    :goto_7
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_GetXMLConfigMsgValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/MsgConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 170
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 171
    if-eqz v4, :cond_b

    .line 172
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 173
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 178
    :goto_8
    if-eqz v1, :cond_c

    .line 179
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 180
    invoke-virtual {v1, p3, v5}, Lcom/qualcomm/qti/config/MsgConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 160
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/MsgConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_9
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    goto :goto_6

    .line 167
    :cond_a
    const/4 v1, 0x0

    .restart local v1    # "_arg2":Lcom/qualcomm/qti/config/MsgConfig;
    goto :goto_7

    .line 176
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_b
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_8

    .line 183
    :cond_c
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 189
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/MsgConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_6
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 191
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 193
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_d

    .line 194
    sget-object v7, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;

    .line 200
    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    :goto_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_e

    .line 201
    sget-object v7, Lcom/qualcomm/qti/config/ImsSettingsConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/config/ImsSettingsConfig;

    .line 206
    .local v1, "_arg2":Lcom/qualcomm/qti/config/ImsSettingsConfig;
    :goto_a
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_GetXMLConfigImsSettingsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/ImsSettingsConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 207
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 208
    if-eqz v4, :cond_f

    .line 209
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 210
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 215
    :goto_b
    if-eqz v1, :cond_10

    .line 216
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 217
    invoke-virtual {v1, p3, v5}, Lcom/qualcomm/qti/config/ImsSettingsConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 197
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/ImsSettingsConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_d
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    goto :goto_9

    .line 204
    :cond_e
    const/4 v1, 0x0

    .restart local v1    # "_arg2":Lcom/qualcomm/qti/config/ImsSettingsConfig;
    goto :goto_a

    .line 213
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_f
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_b

    .line 220
    :cond_10
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 226
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/ImsSettingsConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_7
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 228
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 230
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_11

    .line 231
    sget-object v7, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;

    .line 237
    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    :goto_c
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_12

    .line 238
    sget-object v7, Lcom/qualcomm/qti/config/Services;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/config/Services;

    .line 243
    .local v1, "_arg2":Lcom/qualcomm/qti/config/Services;
    :goto_d
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_GetXMLConfigServicesValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/Services;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 244
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 245
    if-eqz v4, :cond_13

    .line 246
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 247
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 252
    :goto_e
    if-eqz v1, :cond_14

    .line 253
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 254
    invoke-virtual {v1, p3, v5}, Lcom/qualcomm/qti/config/Services;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 234
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/Services;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_11
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    goto :goto_c

    .line 241
    :cond_12
    const/4 v1, 0x0

    .restart local v1    # "_arg2":Lcom/qualcomm/qti/config/Services;
    goto :goto_d

    .line 250
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_13
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_e

    .line 257
    :cond_14
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 263
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/Services;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_8
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 265
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 267
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_15

    .line 268
    sget-object v7, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;

    .line 274
    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    :goto_f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_16

    .line 275
    sget-object v7, Lcom/qualcomm/qti/config/PresenceConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/config/PresenceConfig;

    .line 280
    .local v1, "_arg2":Lcom/qualcomm/qti/config/PresenceConfig;
    :goto_10
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_GetXMLConfigPresenceValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/PresenceConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 281
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 282
    if-eqz v4, :cond_17

    .line 283
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 284
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 289
    :goto_11
    if-eqz v1, :cond_18

    .line 290
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 291
    invoke-virtual {v1, p3, v5}, Lcom/qualcomm/qti/config/PresenceConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 271
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/PresenceConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_15
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    goto :goto_f

    .line 278
    :cond_16
    const/4 v1, 0x0

    .restart local v1    # "_arg2":Lcom/qualcomm/qti/config/PresenceConfig;
    goto :goto_10

    .line 287
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_17
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_11

    .line 294
    :cond_18
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 300
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/PresenceConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_9
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 302
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 304
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_19

    .line 305
    sget-object v7, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;

    .line 311
    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    :goto_12
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_1a

    .line 312
    sget-object v7, Lcom/qualcomm/qti/config/XDMSConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/config/XDMSConfig;

    .line 317
    .local v1, "_arg2":Lcom/qualcomm/qti/config/XDMSConfig;
    :goto_13
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_GetXMLConfigXdmsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/XDMSConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 318
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 319
    if-eqz v4, :cond_1b

    .line 320
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 321
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 326
    :goto_14
    if-eqz v1, :cond_1c

    .line 327
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 328
    invoke-virtual {v1, p3, v5}, Lcom/qualcomm/qti/config/XDMSConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 308
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/XDMSConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_19
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    goto :goto_12

    .line 315
    :cond_1a
    const/4 v1, 0x0

    .restart local v1    # "_arg2":Lcom/qualcomm/qti/config/XDMSConfig;
    goto :goto_13

    .line 324
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1b
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_14

    .line 331
    :cond_1c
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 337
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/XDMSConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_a
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 339
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 341
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_1d

    .line 342
    sget-object v7, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;

    .line 348
    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    :goto_15
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_1e

    .line 349
    sget-object v7, Lcom/qualcomm/qti/config/SuplConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/config/SuplConfig;

    .line 354
    .local v1, "_arg2":Lcom/qualcomm/qti/config/SuplConfig;
    :goto_16
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_GetXMLConfigSuplValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/SuplConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 355
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 356
    if-eqz v4, :cond_1f

    .line 357
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 358
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 363
    :goto_17
    if-eqz v1, :cond_20

    .line 364
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 365
    invoke-virtual {v1, p3, v5}, Lcom/qualcomm/qti/config/SuplConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 345
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/SuplConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1d
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    goto :goto_15

    .line 352
    :cond_1e
    const/4 v1, 0x0

    .restart local v1    # "_arg2":Lcom/qualcomm/qti/config/SuplConfig;
    goto :goto_16

    .line 361
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1f
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_17

    .line 368
    :cond_20
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 374
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/SuplConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_b
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 376
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 378
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_21

    .line 379
    sget-object v7, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;

    .line 385
    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    :goto_18
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_22

    .line 386
    sget-object v7, Lcom/qualcomm/qti/config/IMConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/config/IMConfig;

    .line 391
    .local v1, "_arg2":Lcom/qualcomm/qti/config/IMConfig;
    :goto_19
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_GetXMLConfigImValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/IMConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 392
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 393
    if-eqz v4, :cond_23

    .line 394
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 395
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 400
    :goto_1a
    if-eqz v1, :cond_24

    .line 401
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 402
    invoke-virtual {v1, p3, v5}, Lcom/qualcomm/qti/config/IMConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 382
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/IMConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_21
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    goto :goto_18

    .line 389
    :cond_22
    const/4 v1, 0x0

    .restart local v1    # "_arg2":Lcom/qualcomm/qti/config/IMConfig;
    goto :goto_19

    .line 398
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_23
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1a

    .line 405
    :cond_24
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 411
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/IMConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_c
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 413
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 415
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_25

    .line 416
    sget-object v7, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;

    .line 422
    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    :goto_1b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_26

    .line 423
    sget-object v7, Lcom/qualcomm/qti/config/CPMConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/config/CPMConfig;

    .line 428
    .local v1, "_arg2":Lcom/qualcomm/qti/config/CPMConfig;
    :goto_1c
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_GetXMLConfigCpmValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/CPMConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 429
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 430
    if-eqz v4, :cond_27

    .line 431
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 432
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 437
    :goto_1d
    if-eqz v1, :cond_28

    .line 438
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 439
    invoke-virtual {v1, p3, v5}, Lcom/qualcomm/qti/config/CPMConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 419
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/CPMConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_25
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    goto :goto_1b

    .line 426
    :cond_26
    const/4 v1, 0x0

    .restart local v1    # "_arg2":Lcom/qualcomm/qti/config/CPMConfig;
    goto :goto_1c

    .line 435
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_27
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1d

    .line 442
    :cond_28
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 448
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/CPMConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_d
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 450
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 452
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_29

    .line 453
    sget-object v7, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;

    .line 459
    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    :goto_1e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_2a

    .line 460
    sget-object v7, Lcom/qualcomm/qti/config/CDConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/config/CDConfig;

    .line 465
    .local v1, "_arg2":Lcom/qualcomm/qti/config/CDConfig;
    :goto_1f
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_GetXMLConfigCdValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/CDConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 466
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 467
    if-eqz v4, :cond_2b

    .line 468
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 469
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 474
    :goto_20
    if-eqz v1, :cond_2c

    .line 475
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 476
    invoke-virtual {v1, p3, v5}, Lcom/qualcomm/qti/config/CDConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 456
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/CDConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_29
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    goto :goto_1e

    .line 463
    :cond_2a
    const/4 v1, 0x0

    .restart local v1    # "_arg2":Lcom/qualcomm/qti/config/CDConfig;
    goto :goto_1f

    .line 472
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_2b
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_20

    .line 479
    :cond_2c
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 485
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/CDConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_e
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 487
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 489
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_2d

    .line 490
    sget-object v7, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;

    .line 496
    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    :goto_21
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_2e

    .line 497
    sget-object v7, Lcom/qualcomm/qti/config/APNConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/config/APNConfig;

    .line 502
    .local v1, "_arg2":Lcom/qualcomm/qti/config/APNConfig;
    :goto_22
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_GetXMLConfigApnValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/APNConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 503
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 504
    if-eqz v4, :cond_2f

    .line 505
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 506
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 511
    :goto_23
    if-eqz v1, :cond_30

    .line 512
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 513
    invoke-virtual {v1, p3, v5}, Lcom/qualcomm/qti/config/APNConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 493
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/APNConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_2d
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    goto :goto_21

    .line 500
    :cond_2e
    const/4 v1, 0x0

    .restart local v1    # "_arg2":Lcom/qualcomm/qti/config/APNConfig;
    goto :goto_22

    .line 509
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_2f
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_23

    .line 516
    :cond_30
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 522
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/APNConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_f
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 524
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 526
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_31

    .line 527
    sget-object v7, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;

    .line 533
    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    :goto_24
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_32

    .line 534
    sget-object v7, Lcom/qualcomm/qti/config/OtherRCSConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/config/OtherRCSConfig;

    .line 539
    .local v1, "_arg2":Lcom/qualcomm/qti/config/OtherRCSConfig;
    :goto_25
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_GetXMLConfigOtherRcsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/OtherRCSConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 540
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 541
    if-eqz v4, :cond_33

    .line 542
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 543
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 548
    :goto_26
    if-eqz v1, :cond_34

    .line 549
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 550
    invoke-virtual {v1, p3, v5}, Lcom/qualcomm/qti/config/OtherRCSConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 530
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/OtherRCSConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_31
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    goto :goto_24

    .line 537
    :cond_32
    const/4 v1, 0x0

    .restart local v1    # "_arg2":Lcom/qualcomm/qti/config/OtherRCSConfig;
    goto :goto_25

    .line 546
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_33
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_26

    .line 553
    :cond_34
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 559
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":Lcom/qualcomm/qti/config/OtherRCSConfig;
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_10
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 561
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 563
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_35

    .line 564
    sget-object v7, Lcom/qualcomm/qti/config/DispositionConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/config/DispositionConfig;

    .line 569
    .local v2, "_arg1":Lcom/qualcomm/qti/config/DispositionConfig;
    :goto_27
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_SetDispositionConfig(ILcom/qualcomm/qti/config/DispositionConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 570
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 571
    if-eqz v4, :cond_36

    .line 572
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 573
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 567
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/DispositionConfig;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_35
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/DispositionConfig;
    goto :goto_27

    .line 576
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_36
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 582
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/DispositionConfig;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_11
    const-string v7, "com.qualcomm.qti.config.IQConfigService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 584
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 586
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_37

    move v2, v5

    .line 587
    .local v2, "_arg1":Z
    :goto_28
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/qti/config/IQConfigService$Stub;->QConfigService_AcceptEvent(IZ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 588
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 589
    if-eqz v4, :cond_38

    .line 590
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 591
    invoke-virtual {v4, p3, v5}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .end local v2    # "_arg1":Z
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_37
    move v2, v6

    .line 586
    goto :goto_28

    .line 594
    .restart local v2    # "_arg1":Z
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_38
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
