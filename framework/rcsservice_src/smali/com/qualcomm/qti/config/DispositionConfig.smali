.class public Lcom/qualcomm/qti/config/DispositionConfig;
.super Ljava/lang/Object;
.source "DispositionConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/DispositionConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private requestDeliveryNotification:Z

.field private requestDisplayNotification:Z

.field private sendDeliveryNotification:Z

.field private sendDisplayNotification:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/qualcomm/qti/config/DispositionConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/DispositionConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/DispositionConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/DispositionConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 62
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/DispositionConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/DispositionConfig$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/DispositionConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method

.method public isRequestDeliveryNotification()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/DispositionConfig;->requestDeliveryNotification:Z

    return v0
.end method

.method public isRequestDisplayNotification()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/DispositionConfig;->requestDisplayNotification:Z

    return v0
.end method

.method public isSendDeliveryNotification()Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/DispositionConfig;->sendDeliveryNotification:Z

    return v0
.end method

.method public isSendDisplayNotification()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/DispositionConfig;->sendDisplayNotification:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/DispositionConfig;->requestDeliveryNotification:Z

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/DispositionConfig;->requestDisplayNotification:Z

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/DispositionConfig;->sendDeliveryNotification:Z

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/qualcomm/qti/config/DispositionConfig;->sendDisplayNotification:Z

    .line 69
    return-void

    :cond_0
    move v0, v2

    .line 65
    goto :goto_0

    :cond_1
    move v0, v2

    .line 66
    goto :goto_1

    :cond_2
    move v0, v2

    .line 67
    goto :goto_2

    :cond_3
    move v1, v2

    .line 68
    goto :goto_3
.end method

.method public setRequestDeliveryNotification(Z)V
    .locals 0
    .param p1, "requestDeliveryNotification"    # Z

    .prologue
    .line 91
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/DispositionConfig;->requestDeliveryNotification:Z

    .line 92
    return-void
.end method

.method public setRequestDisplayNotification(Z)V
    .locals 0
    .param p1, "requestDisplayNotification"    # Z

    .prologue
    .line 112
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/DispositionConfig;->requestDisplayNotification:Z

    .line 113
    return-void
.end method

.method public setSendDeliveryNotification(Z)V
    .locals 0
    .param p1, "sendDeliveryNotification"    # Z

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/DispositionConfig;->sendDeliveryNotification:Z

    .line 134
    return-void
.end method

.method public setSendDisplayNotification(Z)V
    .locals 0
    .param p1, "sendDisplayNotification"    # Z

    .prologue
    .line 154
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/DispositionConfig;->sendDisplayNotification:Z

    .line 155
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/DispositionConfig;->requestDeliveryNotification:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 43
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/DispositionConfig;->requestDisplayNotification:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 44
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/DispositionConfig;->sendDeliveryNotification:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 45
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/DispositionConfig;->sendDisplayNotification:Z

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 46
    return-void

    :cond_0
    move v0, v2

    .line 42
    goto :goto_0

    :cond_1
    move v0, v2

    .line 43
    goto :goto_1

    :cond_2
    move v0, v2

    .line 44
    goto :goto_2

    :cond_3
    move v1, v2

    .line 45
    goto :goto_3
.end method
