.class public Lcom/qualcomm/qti/config/NativeConfig;
.super Ljava/lang/Object;
.source "NativeConfig.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native QConfigServiceAcceptEvent(IZ)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceAddListener(ILcom/qualcomm/qti/config/IQConfigServiceListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceGetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceGetXMLConfigApnValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/APNConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceGetXMLConfigCdValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/CDConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceGetXMLConfigCpmValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/CPMConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceGetXMLConfigGroupValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;II)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceGetXMLConfigImValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/IMConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceGetXMLConfigImsSettingsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/ImsSettingsConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceGetXMLConfigMsgValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/MsgConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceGetXMLConfigOtherRcsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/OtherRCSConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceGetXMLConfigPresenceValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/PresenceConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceGetXMLConfigServicesValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/Services;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceGetXMLConfigSuplValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/SuplConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceGetXMLConfigVersionValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/VersionConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceGetXMLConfigXdmsValue(ILcom/qualcomm/qti/config/QPEAutoConfigParamTypes;Lcom/qualcomm/qti/config/XDMSConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceRemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QConfigServiceSetDispositionConfig(ILcom/qualcomm/qti/config/DispositionConfig;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method
