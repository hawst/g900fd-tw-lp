.class public Lcom/qualcomm/qti/config/PhoneContextList;
.super Ljava/lang/Object;
.source "PhoneContextList.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/PhoneContextList;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private phoneContext:Ljava/lang/String;

.field private publicUserIdentity:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/qualcomm/qti/config/PhoneContextList$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/PhoneContextList$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/PhoneContextList;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/config/PhoneContextList;->phoneContext:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/config/PhoneContextList;->publicUserIdentity:Ljava/lang/String;

    .line 36
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/config/PhoneContextList;->phoneContext:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/config/PhoneContextList;->publicUserIdentity:Ljava/lang/String;

    .line 62
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/PhoneContextList;->readFromParcel(Landroid/os/Parcel;)V

    .line 63
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/PhoneContextList$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/PhoneContextList$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/PhoneContextList;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return v0
.end method

.method public getPhoneContext()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/qualcomm/qti/config/PhoneContextList;->phoneContext:Ljava/lang/String;

    return-object v0
.end method

.method public getPublicUserIdentity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/qualcomm/qti/config/PhoneContextList;->publicUserIdentity:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/PhoneContextList;->phoneContext:Ljava/lang/String;

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/PhoneContextList;->publicUserIdentity:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setPhoneContext(Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneContext"    # Ljava/lang/String;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/qualcomm/qti/config/PhoneContextList;->phoneContext:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public setPublicUserIdentity(Ljava/lang/String;)V
    .locals 0
    .param p1, "publicUserIdentity"    # Ljava/lang/String;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/qualcomm/qti/config/PhoneContextList;->publicUserIdentity:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 45
    iget-object v0, p0, Lcom/qualcomm/qti/config/PhoneContextList;->phoneContext:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/qualcomm/qti/config/PhoneContextList;->publicUserIdentity:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    return-void
.end method
