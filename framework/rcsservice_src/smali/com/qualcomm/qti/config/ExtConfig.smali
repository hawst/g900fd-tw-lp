.class public Lcom/qualcomm/qti/config/ExtConfig;
.super Ljava/lang/Object;
.source "ExtConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/ExtConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private intUrlFmt:Z

.field private maxSizeImageShare:I

.field private maxTimeVideoShare:I

.field private natUrlFmt:Z

.field private sQValue:Ljava/lang/String;

.field private secondaryDeviceConfig:Lcom/qualcomm/qti/config/SecondaryDevice;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 228
    new-instance v0, Lcom/qualcomm/qti/config/ExtConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/ExtConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/ExtConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/ExtConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 243
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/ExtConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/ExtConfig$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/ExtConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 219
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->natUrlFmt:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 220
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->intUrlFmt:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 221
    iget-object v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->sQValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 222
    iget v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->maxSizeImageShare:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 223
    iget v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->maxTimeVideoShare:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 224
    iget-object v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->secondaryDeviceConfig:Lcom/qualcomm/qti/config/SecondaryDevice;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 226
    return-void

    :cond_0
    move v0, v2

    .line 219
    goto :goto_0

    :cond_1
    move v1, v2

    .line 220
    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x0

    return v0
.end method

.method public getIntUrlFmt()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->intUrlFmt:Z

    return v0
.end method

.method public getMaxSizeImageShare()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->maxSizeImageShare:I

    return v0
.end method

.method public getMaxTimeVideoShare()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->maxTimeVideoShare:I

    return v0
.end method

.method public getNatUrlFmt()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->natUrlFmt:Z

    return v0
.end method

.method public getSecondaryDeviceConfig()Lcom/qualcomm/qti/config/SecondaryDevice;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->secondaryDeviceConfig:Lcom/qualcomm/qti/config/SecondaryDevice;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->sQValue:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 247
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->natUrlFmt:Z

    .line 248
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/qualcomm/qti/config/ExtConfig;->intUrlFmt:Z

    .line 249
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->sQValue:Ljava/lang/String;

    .line 250
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->maxSizeImageShare:I

    .line 251
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->maxTimeVideoShare:I

    .line 252
    const-class v0, Lcom/qualcomm/qti/config/SecondaryDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/config/SecondaryDevice;

    iput-object v0, p0, Lcom/qualcomm/qti/config/ExtConfig;->secondaryDeviceConfig:Lcom/qualcomm/qti/config/SecondaryDevice;

    .line 255
    return-void

    :cond_0
    move v0, v2

    .line 247
    goto :goto_0

    :cond_1
    move v1, v2

    .line 248
    goto :goto_1
.end method

.method public setIntUrlFmt(Z)V
    .locals 0
    .param p1, "intUrlFmt"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/ExtConfig;->intUrlFmt:Z

    .line 106
    return-void
.end method

.method public setMaxSizeImageShare(I)V
    .locals 0
    .param p1, "maxSizeImageShare"    # I

    .prologue
    .line 151
    iput p1, p0, Lcom/qualcomm/qti/config/ExtConfig;->maxSizeImageShare:I

    .line 152
    return-void
.end method

.method public setMaxTimeVideoShare(I)V
    .locals 0
    .param p1, "maxTimeVideoShare"    # I

    .prologue
    .line 173
    iput p1, p0, Lcom/qualcomm/qti/config/ExtConfig;->maxTimeVideoShare:I

    .line 174
    return-void
.end method

.method public setNatUrlFmt(Z)V
    .locals 0
    .param p1, "natUrlFmt"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/ExtConfig;->natUrlFmt:Z

    .line 84
    return-void
.end method

.method public setSecondaryDeviceConfig(Lcom/qualcomm/qti/config/SecondaryDevice;)V
    .locals 0
    .param p1, "secondaryDeviceConfig"    # Lcom/qualcomm/qti/config/SecondaryDevice;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/qualcomm/qti/config/ExtConfig;->secondaryDeviceConfig:Lcom/qualcomm/qti/config/SecondaryDevice;

    .line 195
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "qValue"    # Ljava/lang/String;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/qualcomm/qti/config/ExtConfig;->sQValue:Ljava/lang/String;

    .line 128
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 213
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/ExtConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 215
    return-void
.end method
