.class public Lcom/qualcomm/qti/config/CDConfig;
.super Ljava/lang/Object;
.source "CDConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/CDConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private capDiscCommonStack:Z

.field private capInfoExpiry:I

.field private defaultDisc:Z

.field private pollingPeriod:I

.field private pollingRate:I

.field private pollingRatePeriod:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 183
    new-instance v0, Lcom/qualcomm/qti/config/CDConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/CDConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/CDConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/CDConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 198
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/CDConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/CDConfig$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/CDConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 175
    iget v0, p0, Lcom/qualcomm/qti/config/CDConfig;->pollingPeriod:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 176
    iget v0, p0, Lcom/qualcomm/qti/config/CDConfig;->pollingRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 177
    iget v0, p0, Lcom/qualcomm/qti/config/CDConfig;->pollingRatePeriod:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 178
    iget v0, p0, Lcom/qualcomm/qti/config/CDConfig;->capInfoExpiry:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 179
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/CDConfig;->defaultDisc:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 180
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/CDConfig;->capDiscCommonStack:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 181
    return-void

    :cond_0
    move v0, v2

    .line 179
    goto :goto_0

    :cond_1
    move v1, v2

    .line 180
    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    return v0
.end method

.method public getCapInfoExpiry()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/qualcomm/qti/config/CDConfig;->capInfoExpiry:I

    return v0
.end method

.method public getPollingPeriod()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/qualcomm/qti/config/CDConfig;->pollingPeriod:I

    return v0
.end method

.method public getPollingRate()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/qualcomm/qti/config/CDConfig;->pollingRate:I

    return v0
.end method

.method public getPollingRatePeriod()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/qualcomm/qti/config/CDConfig;->pollingRatePeriod:I

    return v0
.end method

.method public isCapDiscCommonStack()Z
    .locals 1

    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/CDConfig;->capDiscCommonStack:Z

    return v0
.end method

.method public isDefaultDisc()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/CDConfig;->defaultDisc:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 202
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/CDConfig;->pollingPeriod:I

    .line 203
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/CDConfig;->pollingRate:I

    .line 204
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/CDConfig;->pollingRatePeriod:I

    .line 205
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/CDConfig;->capInfoExpiry:I

    .line 206
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/CDConfig;->defaultDisc:Z

    .line 207
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/qualcomm/qti/config/CDConfig;->capDiscCommonStack:Z

    .line 209
    return-void

    :cond_0
    move v0, v2

    .line 206
    goto :goto_0

    :cond_1
    move v1, v2

    .line 207
    goto :goto_1
.end method

.method public setCapDiscCommonStack(Z)V
    .locals 0
    .param p1, "capDiscCommonStack"    # Z

    .prologue
    .line 150
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/CDConfig;->capDiscCommonStack:Z

    .line 151
    return-void
.end method

.method public setCapInfoExpiry(I)V
    .locals 0
    .param p1, "capInfoExpiry"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/qualcomm/qti/config/CDConfig;->capInfoExpiry:I

    .line 105
    return-void
.end method

.method public setDefaultDisc(Z)V
    .locals 0
    .param p1, "defaultDisc"    # Z

    .prologue
    .line 126
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/CDConfig;->defaultDisc:Z

    .line 127
    return-void
.end method

.method public setPollingPeriod(I)V
    .locals 0
    .param p1, "pollingPeriod"    # I

    .prologue
    .line 82
    iput p1, p0, Lcom/qualcomm/qti/config/CDConfig;->pollingPeriod:I

    .line 83
    return-void
.end method

.method public setPollingRate(I)V
    .locals 0
    .param p1, "pollingRate"    # I

    .prologue
    .line 134
    iput p1, p0, Lcom/qualcomm/qti/config/CDConfig;->pollingRate:I

    .line 135
    return-void
.end method

.method public setPollingRatePeriod(I)V
    .locals 0
    .param p1, "pollingRatePeriod"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/qualcomm/qti/config/CDConfig;->pollingRatePeriod:I

    .line 143
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 169
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/CDConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 171
    return-void
.end method
