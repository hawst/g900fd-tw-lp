.class public Lcom/qualcomm/qti/config/LocationParamConfig;
.super Ljava/lang/Object;
.source "LocationParamConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/LocationParamConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private locInfoMaxValidTime:I

.field private textMaxLength:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/qualcomm/qti/config/LocationParamConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/LocationParamConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/LocationParamConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/LocationParamConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 64
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/LocationParamConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/LocationParamConfig$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/LocationParamConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public getLocInfoMaxValidTime()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/qualcomm/qti/config/LocationParamConfig;->locInfoMaxValidTime:I

    return v0
.end method

.method public getTextMaxLength()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/qualcomm/qti/config/LocationParamConfig;->textMaxLength:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/LocationParamConfig;->textMaxLength:I

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/LocationParamConfig;->locInfoMaxValidTime:I

    .line 69
    return-void
.end method

.method public setLocInfoMaxValidTime(I)V
    .locals 0
    .param p1, "locInfoMaxValidTime"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/qualcomm/qti/config/LocationParamConfig;->locInfoMaxValidTime:I

    .line 87
    return-void
.end method

.method public setTextMaxLength(I)V
    .locals 0
    .param p1, "textMaxLength"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/qualcomm/qti/config/LocationParamConfig;->textMaxLength:I

    .line 79
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 46
    iget v0, p0, Lcom/qualcomm/qti/config/LocationParamConfig;->textMaxLength:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 47
    iget v0, p0, Lcom/qualcomm/qti/config/LocationParamConfig;->locInfoMaxValidTime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 48
    return-void
.end method
