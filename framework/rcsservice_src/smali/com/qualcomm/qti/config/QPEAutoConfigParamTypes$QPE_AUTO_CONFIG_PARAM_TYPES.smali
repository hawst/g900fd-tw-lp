.class public final enum Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;
.super Ljava/lang/Enum;
.source "QPEAutoConfigParamTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QPE_AUTO_CONFIG_PARAM_TYPES"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

.field public static final enum QP_RCS_APN_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

.field public static final enum QP_RCS_CAPDISCOVERY_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

.field public static final enum QP_RCS_CPM_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

.field public static final enum QP_RCS_IMS_SETTINGS_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

.field public static final enum QP_RCS_IM_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

.field public static final enum QP_RCS_MSG_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

.field public static final enum QP_RCS_OTHER_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

.field public static final enum QP_RCS_PRESENCE_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

.field public static final enum QP_RCS_SERVICES_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

.field public static final enum QP_RCS_SUPL_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

.field public static final enum QP_RCS_UNKNOWN:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

.field public static final enum QP_RCS_VERS_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

.field public static final enum QP_RCS_XDMS_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    new-instance v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    const-string v1, "QP_RCS_VERS_CONFIG"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_VERS_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    .line 25
    new-instance v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    const-string v1, "QP_RCS_MSG_CONFIG"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_MSG_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    .line 26
    new-instance v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    const-string v1, "QP_RCS_IMS_SETTINGS_CONFIG"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_IMS_SETTINGS_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    .line 27
    new-instance v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    const-string v1, "QP_RCS_SERVICES_CONFIG"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_SERVICES_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    .line 28
    new-instance v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    const-string v1, "QP_RCS_PRESENCE_CONFIG"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_PRESENCE_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    .line 29
    new-instance v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    const-string v1, "QP_RCS_XDMS_CONFIG"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_XDMS_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    .line 30
    new-instance v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    const-string v1, "QP_RCS_SUPL_CONFIG"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_SUPL_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    .line 31
    new-instance v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    const-string v1, "QP_RCS_IM_CONFIG"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_IM_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    .line 32
    new-instance v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    const-string v1, "QP_RCS_CPM_CONFIG"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_CPM_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    .line 33
    new-instance v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    const-string v1, "QP_RCS_CAPDISCOVERY_CONFIG"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_CAPDISCOVERY_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    .line 34
    new-instance v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    const-string v1, "QP_RCS_APN_CONFIG"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_APN_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    .line 35
    new-instance v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    const-string v1, "QP_RCS_OTHER_CONFIG"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_OTHER_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    .line 36
    new-instance v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    const-string v1, "QP_RCS_UNKNOWN"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_UNKNOWN:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    .line 22
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    sget-object v1, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_VERS_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_MSG_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_IMS_SETTINGS_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_SERVICES_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_PRESENCE_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_XDMS_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_SUPL_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_IM_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_CPM_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_CAPDISCOVERY_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_APN_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_OTHER_CONFIG:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->QP_RCS_UNKNOWN:Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->$VALUES:[Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->$VALUES:[Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/config/QPEAutoConfigParamTypes$QPE_AUTO_CONFIG_PARAM_TYPES;

    return-object v0
.end method
