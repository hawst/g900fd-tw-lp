.class public Lcom/qualcomm/qti/config/MsgConfig;
.super Ljava/lang/Object;
.source "MsgConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/MsgConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private acceptBtn:Z

.field private message:Ljava/lang/String;

.field private rejectBtn:Z

.field private title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 165
    new-instance v0, Lcom/qualcomm/qti/config/MsgConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/MsgConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/MsgConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0
    .param p1, "validity"    # I
    .param p2, "version"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "acceptBtn"    # Z
    .param p6, "rejectBtn"    # Z

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p3, p0, Lcom/qualcomm/qti/config/MsgConfig;->title:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Lcom/qualcomm/qti/config/MsgConfig;->message:Ljava/lang/String;

    .line 41
    iput-boolean p5, p0, Lcom/qualcomm/qti/config/MsgConfig;->acceptBtn:Z

    .line 42
    iput-boolean p6, p0, Lcom/qualcomm/qti/config/MsgConfig;->rejectBtn:Z

    .line 43
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/MsgConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 180
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/MsgConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/MsgConfig$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/MsgConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getConfigMsgInstance()Lcom/qualcomm/qti/config/MsgConfig;
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/qualcomm/qti/config/MsgConfig;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/MsgConfig;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 159
    iget-object v0, p0, Lcom/qualcomm/qti/config/MsgConfig;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/qualcomm/qti/config/MsgConfig;->message:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 161
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/MsgConfig;->acceptBtn:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 162
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/MsgConfig;->rejectBtn:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 163
    return-void

    :cond_0
    move v0, v2

    .line 161
    goto :goto_0

    :cond_1
    move v1, v2

    .line 162
    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    return v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/qualcomm/qti/config/MsgConfig;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/qualcomm/qti/config/MsgConfig;->title:Ljava/lang/String;

    return-object v0
.end method

.method public isAcceptBtn()Z
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/MsgConfig;->acceptBtn:Z

    return v0
.end method

.method public isRejectBtn()Z
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/MsgConfig;->rejectBtn:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 184
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/MsgConfig;->title:Ljava/lang/String;

    .line 185
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/MsgConfig;->message:Ljava/lang/String;

    .line 186
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/MsgConfig;->acceptBtn:Z

    .line 187
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/qualcomm/qti/config/MsgConfig;->rejectBtn:Z

    .line 188
    return-void

    :cond_0
    move v0, v2

    .line 186
    goto :goto_0

    :cond_1
    move v1, v2

    .line 187
    goto :goto_1
.end method

.method public setAcceptBtn(Z)V
    .locals 0
    .param p1, "acceptBtn"    # Z

    .prologue
    .line 118
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/MsgConfig;->acceptBtn:Z

    .line 119
    return-void
.end method

.method public setMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 96
    iput-object p1, p0, Lcom/qualcomm/qti/config/MsgConfig;->message:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public setRejectBtn(Z)V
    .locals 0
    .param p1, "rejectBtn"    # Z

    .prologue
    .line 140
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/MsgConfig;->rejectBtn:Z

    .line 141
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/qualcomm/qti/config/MsgConfig;->title:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 153
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/MsgConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 155
    return-void
.end method
