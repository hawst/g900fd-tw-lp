.class public Lcom/qualcomm/qti/config/LinksConfig;
.super Ljava/lang/Object;
.source "LinksConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/LinksConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private opFavUrl:[Ljava/lang/String;

.field private opFavUrlCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/qualcomm/qti/config/LinksConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/LinksConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/LinksConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/LinksConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 67
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/LinksConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/LinksConfig$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/LinksConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public getOpFavUrl()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/qualcomm/qti/config/LinksConfig;->opFavUrl:[Ljava/lang/String;

    return-object v0
.end method

.method public getOpFavUrlCount()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/qualcomm/qti/config/LinksConfig;->opFavUrlCount:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/qualcomm/qti/config/LinksConfig;->opFavUrlCount:I

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 72
    .local v0, "lenght":I
    if-lez v0, :cond_0

    .line 74
    new-array v1, v0, [Ljava/lang/String;

    iput-object v1, p0, Lcom/qualcomm/qti/config/LinksConfig;->opFavUrl:[Ljava/lang/String;

    .line 75
    iget-object v1, p0, Lcom/qualcomm/qti/config/LinksConfig;->opFavUrl:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 77
    :cond_0
    return-void
.end method

.method public setOpFavUrl([Ljava/lang/String;)V
    .locals 1
    .param p1, "opFavUrl"    # [Ljava/lang/String;

    .prologue
    .line 94
    if-eqz p1, :cond_0

    .line 96
    array-length v0, p1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/qualcomm/qti/config/LinksConfig;->opFavUrl:[Ljava/lang/String;

    .line 97
    iput-object p1, p0, Lcom/qualcomm/qti/config/LinksConfig;->opFavUrl:[Ljava/lang/String;

    .line 99
    :cond_0
    return-void
.end method

.method public setOpFavUrlCount(I)V
    .locals 0
    .param p1, "iOpFavUrlCount"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/qualcomm/qti/config/LinksConfig;->opFavUrlCount:I

    .line 87
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 41
    iget v0, p0, Lcom/qualcomm/qti/config/LinksConfig;->opFavUrlCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 42
    iget-object v0, p0, Lcom/qualcomm/qti/config/LinksConfig;->opFavUrl:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/qualcomm/qti/config/LinksConfig;->opFavUrl:[Ljava/lang/String;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 45
    iget-object v0, p0, Lcom/qualcomm/qti/config/LinksConfig;->opFavUrl:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 51
    :goto_0
    return-void

    .line 49
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
