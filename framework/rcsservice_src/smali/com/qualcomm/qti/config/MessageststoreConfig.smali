.class public Lcom/qualcomm/qti/config/MessageststoreConfig;
.super Ljava/lang/Object;
.source "MessageststoreConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/MessageststoreConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private URL:Ljava/lang/String;

.field private authProt:Z

.field private userName:Ljava/lang/String;

.field private userPwd:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/qualcomm/qti/config/MessageststoreConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/MessageststoreConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/MessageststoreConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/MessageststoreConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 67
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/MessageststoreConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/MessageststoreConfig$1;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/MessageststoreConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->URL:Ljava/lang/String;

    return-object v0
.end method

.method public getUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->userName:Ljava/lang/String;

    return-object v0
.end method

.method public getUserPwd()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->userPwd:Ljava/lang/String;

    return-object v0
.end method

.method public isAuthProt()Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->authProt:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->URL:Ljava/lang/String;

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->authProt:Z

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->userName:Ljava/lang/String;

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->userPwd:Ljava/lang/String;

    .line 74
    return-void

    .line 71
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setAuthProt(Z)V
    .locals 0
    .param p1, "authProt"    # Z

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->authProt:Z

    .line 90
    return-void
.end method

.method public setURL(Ljava/lang/String;)V
    .locals 0
    .param p1, "uRL"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->URL:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public setUserName(Ljava/lang/String;)V
    .locals 0
    .param p1, "userName"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->userName:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setUserPwd(Ljava/lang/String;)V
    .locals 0
    .param p1, "userPwd"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->userPwd:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->URL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 48
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->authProt:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 49
    iget-object v0, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->userName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/qualcomm/qti/config/MessageststoreConfig;->userPwd:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 51
    return-void

    .line 48
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
