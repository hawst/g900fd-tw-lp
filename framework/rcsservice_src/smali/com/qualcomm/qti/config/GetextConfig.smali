.class public Lcom/qualcomm/qti/config/GetextConfig;
.super Ljava/lang/Object;
.source "GetextConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/GetextConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private address:Ljava/lang/String;

.field private addressType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lcom/qualcomm/qti/config/GetextConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/GetextConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/GetextConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/config/GetextConfig;->address:Ljava/lang/String;

    .line 60
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/config/GetextConfig;->address:Ljava/lang/String;

    .line 92
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/GetextConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 93
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/GetextConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/GetextConfig$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/GetextConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/qualcomm/qti/config/GetextConfig;->address:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 75
    iget v0, p0, Lcom/qualcomm/qti/config/GetextConfig;->addressType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 76
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/qualcomm/qti/config/GetextConfig;->address:Ljava/lang/String;

    return-object v0
.end method

.method public getAddressType()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/qualcomm/qti/config/GetextConfig;->addressType:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/GetextConfig;->address:Ljava/lang/String;

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/GetextConfig;->addressType:I

    .line 98
    return-void
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/qualcomm/qti/config/GetextConfig;->address:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setAddressType(I)V
    .locals 0
    .param p1, "addressType"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/qualcomm/qti/config/GetextConfig;->addressType:I

    .line 50
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/GetextConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 70
    return-void
.end method
