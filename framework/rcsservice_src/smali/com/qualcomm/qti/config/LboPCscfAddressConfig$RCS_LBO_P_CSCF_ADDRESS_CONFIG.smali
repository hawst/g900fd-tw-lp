.class public final enum Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;
.super Ljava/lang/Enum;
.source "LboPCscfAddressConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/config/LboPCscfAddressConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RCS_LBO_P_CSCF_ADDRESS_CONFIG"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

.field public static final enum QRCS_ADDRESS_TYPE_FQDN:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

.field public static final enum QRCS_ADDRESS_TYPE_INVALID:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

.field public static final enum QRCS_ADDRESS_TYPE_IPV4:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

.field public static final enum QRCS_ADDRESS_TYPE_IPV6:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

.field public static final enum QRCS_ADDRESS_TYPE_MAX32:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

.field public static final enum QRCS_ADDRESS_TYPE_UNKNOWN:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    const-string v1, "QRCS_ADDRESS_TYPE_FQDN"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_FQDN:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    .line 24
    new-instance v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    const-string v1, "QRCS_ADDRESS_TYPE_IPV4"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_IPV4:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    .line 26
    new-instance v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    const-string v1, "QRCS_ADDRESS_TYPE_IPV6"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_IPV6:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    .line 28
    new-instance v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    const-string v1, "QRCS_ADDRESS_TYPE_INVALID"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_INVALID:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    .line 30
    new-instance v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    const-string v1, "QRCS_ADDRESS_TYPE_MAX32"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_MAX32:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    .line 31
    new-instance v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    const-string v1, "QRCS_ADDRESS_TYPE_UNKNOWN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_UNKNOWN:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    .line 20
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    sget-object v1, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_FQDN:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_IPV4:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_IPV6:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_INVALID:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_MAX32:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->QRCS_ADDRESS_TYPE_UNKNOWN:Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->$VALUES:[Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->$VALUES:[Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/config/LboPCscfAddressConfig$RCS_LBO_P_CSCF_ADDRESS_CONFIG;

    return-object v0
.end method
