.class public final enum Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;
.super Ljava/lang/Enum;
.source "TransportprotoConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/config/TransportprotoConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QRCS_MEDIA_PROTO"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

.field public static final enum QRCS_MEDIA_PROTO_INVALID:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

.field public static final enum QRCS_MEDIA_PROTO_MAX32:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

.field public static final enum QRCS_MEDIA_PROTO_MSRP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

.field public static final enum QRCS_MEDIA_PROTO_MSRP_OVER_TLS:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

.field public static final enum QRCS_MEDIA_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-instance v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    const-string v1, "QRCS_MEDIA_PROTO_MSRP"

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_MSRP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    .line 35
    new-instance v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    const-string v1, "QRCS_MEDIA_PROTO_MSRP_OVER_TLS"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_MSRP_OVER_TLS:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    .line 37
    new-instance v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    const-string v1, "QRCS_MEDIA_PROTO_INVALID"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_INVALID:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    .line 39
    new-instance v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    const-string v1, "QRCS_MEDIA_PROTO_MAX32"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_MAX32:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    .line 40
    new-instance v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    const-string v1, "QRCS_MEDIA_PROTO_UNKNOWN"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    .line 31
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_MSRP:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_MSRP_OVER_TLS:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_INVALID:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_MAX32:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->QRCS_MEDIA_PROTO_UNKNOWN:Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    aput-object v1, v0, v6

    sput-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->$VALUES:[Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->$VALUES:[Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/config/TransportprotoConfig$QRCS_MEDIA_PROTO;

    return-object v0
.end method
