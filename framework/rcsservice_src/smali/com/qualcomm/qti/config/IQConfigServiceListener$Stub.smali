.class public abstract Lcom/qualcomm/qti/config/IQConfigServiceListener$Stub;
.super Landroid/os/Binder;
.source "IQConfigServiceListener.java"

# interfaces
.implements Lcom/qualcomm/qti/config/IQConfigServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/config/IQConfigServiceListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/config/IQConfigServiceListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.config.IQConfigServiceListener"

.field static final TRANSACTION_QConfigServiceListener_ConfigSettingsUpdated:I = 0x5

.field static final TRANSACTION_QConfigServiceListener_ReceivedFailureResponse:I = 0x4

.field static final TRANSACTION_QConfigServiceListener_ReceivedSuccessResponse:I = 0x3

.field static final TRANSACTION_QConfigServiceListener_RequestSent:I = 0x2

.field static final TRANSACTION_QConfigServiceListener_ServiceCreated:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.config.IQConfigServiceListener"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/config/IQConfigServiceListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/config/IQConfigServiceListener;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.config.IQConfigServiceListener"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/config/IQConfigServiceListener;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/config/IQConfigServiceListener;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/config/IQConfigServiceListener$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/config/IQConfigServiceListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 116
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    .line 44
    :sswitch_0
    const-string v5, "com.qualcomm.qti.config.IQConfigServiceListener"

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v5, "com.qualcomm.qti.config.IQConfigServiceListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 53
    .local v0, "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_0

    .line 54
    sget-object v5, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 60
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 61
    .local v3, "_arg2":I
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/qualcomm/qti/config/IQConfigServiceListener$Stub;->QConfigServiceListener_ServiceCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;I)V

    .line 62
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 57
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    .end local v3    # "_arg2":I
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_1

    .line 67
    .end local v0    # "_arg0":J
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_2
    const-string v5, "com.qualcomm.qti.config.IQConfigServiceListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 70
    .restart local v0    # "_arg0":J
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/config/IQConfigServiceListener$Stub;->QConfigServiceListener_RequestSent(J)V

    .line 71
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 76
    .end local v0    # "_arg0":J
    :sswitch_3
    const-string v5, "com.qualcomm.qti.config.IQConfigServiceListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 80
    .restart local v0    # "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_1

    .line 81
    sget-object v5, Lcom/qualcomm/qti/config/ConfigSuccessMsg;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/config/ConfigSuccessMsg;

    .line 86
    .local v2, "_arg1":Lcom/qualcomm/qti/config/ConfigSuccessMsg;
    :goto_2
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/config/IQConfigServiceListener$Stub;->QConfigServiceListener_ReceivedSuccessResponse(JLcom/qualcomm/qti/config/ConfigSuccessMsg;)V

    .line 87
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 84
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/ConfigSuccessMsg;
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/ConfigSuccessMsg;
    goto :goto_2

    .line 92
    .end local v0    # "_arg0":J
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/ConfigSuccessMsg;
    :sswitch_4
    const-string v5, "com.qualcomm.qti.config.IQConfigServiceListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 96
    .restart local v0    # "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_2

    .line 97
    sget-object v5, Lcom/qualcomm/qti/config/ConfigFailureMsg;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/config/ConfigFailureMsg;

    .line 102
    .local v2, "_arg1":Lcom/qualcomm/qti/config/ConfigFailureMsg;
    :goto_3
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/config/IQConfigServiceListener$Stub;->QConfigServiceListener_ReceivedFailureResponse(JLcom/qualcomm/qti/config/ConfigFailureMsg;)V

    .line 103
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 100
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/ConfigFailureMsg;
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/config/ConfigFailureMsg;
    goto :goto_3

    .line 108
    .end local v0    # "_arg0":J
    .end local v2    # "_arg1":Lcom/qualcomm/qti/config/ConfigFailureMsg;
    :sswitch_5
    const-string v5, "com.qualcomm.qti.config.IQConfigServiceListener"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 110
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    .line 111
    .restart local v0    # "_arg0":J
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/config/IQConfigServiceListener$Stub;->QConfigServiceListener_ConfigSettingsUpdated(J)V

    .line 112
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
