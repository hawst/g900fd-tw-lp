.class public Lcom/qualcomm/qti/config/PresenceConfig;
.super Ljava/lang/Object;
.source "PresenceConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/PresenceConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private availabilityAuth:Z

.field private clientObjDataLimit:I

.field private contentServerUri:Ljava/lang/String;

.field private favlink:Lcom/qualcomm/qti/config/FavlinkConfig;

.field private iconMaxSize:I

.field private locationParamConfig:Lcom/qualcomm/qti/config/LocationParamConfig;

.field private maxSubsInPresenceList:I

.field private nickNameLength:I

.field private noteMaxSize:I

.field private publishTimer:I

.field private rlsURI:Ljava/lang/String;

.field private serviceUriTemplate:Ljava/lang/String;

.field private sourceThrottlePublish:I

.field private vipcontacts:Lcom/qualcomm/qti/config/VipContactsConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 359
    new-instance v0, Lcom/qualcomm/qti/config/PresenceConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/PresenceConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/PresenceConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/PresenceConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 374
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/PresenceConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/PresenceConfig$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/PresenceConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 342
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->availabilityAuth:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 343
    iget v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->iconMaxSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 344
    iget v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->noteMaxSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 345
    iget-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->favlink:Lcom/qualcomm/qti/config/FavlinkConfig;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 346
    iget-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->vipcontacts:Lcom/qualcomm/qti/config/VipContactsConfig;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 347
    iget v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->publishTimer:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 348
    iget v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->nickNameLength:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 349
    iget-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->locationParamConfig:Lcom/qualcomm/qti/config/LocationParamConfig;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 350
    iget v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->clientObjDataLimit:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 351
    iget-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->contentServerUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 352
    iget v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->sourceThrottlePublish:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 353
    iget v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->maxSubsInPresenceList:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 354
    iget-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->serviceUriTemplate:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 355
    iget-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->rlsURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 357
    return-void

    .line 342
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 331
    const/4 v0, 0x0

    return v0
.end method

.method public getClientObjDataLimit()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->clientObjDataLimit:I

    return v0
.end method

.method public getContentServerUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->contentServerUri:Ljava/lang/String;

    return-object v0
.end method

.method public getFavlink()Lcom/qualcomm/qti/config/FavlinkConfig;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->favlink:Lcom/qualcomm/qti/config/FavlinkConfig;

    return-object v0
.end method

.method public getIconMaxSize()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->iconMaxSize:I

    return v0
.end method

.method public getLocationParamConfig()Lcom/qualcomm/qti/config/LocationParamConfig;
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->locationParamConfig:Lcom/qualcomm/qti/config/LocationParamConfig;

    return-object v0
.end method

.method public getMaxSubsInPresenceList()I
    .locals 1

    .prologue
    .line 242
    iget v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->maxSubsInPresenceList:I

    return v0
.end method

.method public getNickNameLength()I
    .locals 1

    .prologue
    .line 296
    iget v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->nickNameLength:I

    return v0
.end method

.method public getNoteMaxSize()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->noteMaxSize:I

    return v0
.end method

.method public getPublishTimer()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->publishTimer:I

    return v0
.end method

.method public getRlsURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->rlsURI:Ljava/lang/String;

    return-object v0
.end method

.method public getServiceUriTemplate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->serviceUriTemplate:Ljava/lang/String;

    return-object v0
.end method

.method public getSourceThrottlePublish()I
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->sourceThrottlePublish:I

    return v0
.end method

.method public getVipcontacts()Lcom/qualcomm/qti/config/VipContactsConfig;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->vipcontacts:Lcom/qualcomm/qti/config/VipContactsConfig;

    return-object v0
.end method

.method public isAvailabilityAuth()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->availabilityAuth:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 378
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->availabilityAuth:Z

    .line 379
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->iconMaxSize:I

    .line 380
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->noteMaxSize:I

    .line 381
    const-class v0, Lcom/qualcomm/qti/config/FavlinkConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/config/FavlinkConfig;

    iput-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->favlink:Lcom/qualcomm/qti/config/FavlinkConfig;

    .line 382
    const-class v0, Lcom/qualcomm/qti/config/VipContactsConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/config/VipContactsConfig;

    iput-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->vipcontacts:Lcom/qualcomm/qti/config/VipContactsConfig;

    .line 384
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->publishTimer:I

    .line 385
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->nickNameLength:I

    .line 386
    const-class v0, Lcom/qualcomm/qti/config/LocationParamConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/config/LocationParamConfig;

    iput-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->locationParamConfig:Lcom/qualcomm/qti/config/LocationParamConfig;

    .line 388
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->clientObjDataLimit:I

    .line 389
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->contentServerUri:Ljava/lang/String;

    .line 390
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->sourceThrottlePublish:I

    .line 391
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->maxSubsInPresenceList:I

    .line 392
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->serviceUriTemplate:Ljava/lang/String;

    .line 393
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/config/PresenceConfig;->rlsURI:Ljava/lang/String;

    .line 394
    return-void

    .line 378
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setAvailabilityAuth(Z)V
    .locals 0
    .param p1, "availabilityAuth"    # Z

    .prologue
    .line 101
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/PresenceConfig;->availabilityAuth:Z

    .line 102
    return-void
.end method

.method public setClientObjDataLimit(I)V
    .locals 0
    .param p1, "clientObjDataLimit"    # I

    .prologue
    .line 185
    iput p1, p0, Lcom/qualcomm/qti/config/PresenceConfig;->clientObjDataLimit:I

    .line 186
    return-void
.end method

.method public setContentServerUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "contentServerUri"    # Ljava/lang/String;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/qualcomm/qti/config/PresenceConfig;->contentServerUri:Ljava/lang/String;

    .line 209
    return-void
.end method

.method public setFavlink(Lcom/qualcomm/qti/config/FavlinkConfig;)V
    .locals 0
    .param p1, "favlink"    # Lcom/qualcomm/qti/config/FavlinkConfig;

    .prologue
    .line 284
    iput-object p1, p0, Lcom/qualcomm/qti/config/PresenceConfig;->favlink:Lcom/qualcomm/qti/config/FavlinkConfig;

    .line 285
    return-void
.end method

.method public setIconMaxSize(I)V
    .locals 0
    .param p1, "iconMaxSize"    # I

    .prologue
    .line 122
    iput p1, p0, Lcom/qualcomm/qti/config/PresenceConfig;->iconMaxSize:I

    .line 123
    return-void
.end method

.method public setLocationParamConfig(Lcom/qualcomm/qti/config/LocationParamConfig;)V
    .locals 0
    .param p1, "locationParamConfig"    # Lcom/qualcomm/qti/config/LocationParamConfig;

    .prologue
    .line 308
    iput-object p1, p0, Lcom/qualcomm/qti/config/PresenceConfig;->locationParamConfig:Lcom/qualcomm/qti/config/LocationParamConfig;

    .line 309
    return-void
.end method

.method public setMaxSubsInPresenceList(I)V
    .locals 0
    .param p1, "maxSubsInPresenceList"    # I

    .prologue
    .line 255
    iput p1, p0, Lcom/qualcomm/qti/config/PresenceConfig;->maxSubsInPresenceList:I

    .line 256
    return-void
.end method

.method public setNickNameLength(I)V
    .locals 0
    .param p1, "nickNameLength"    # I

    .prologue
    .line 300
    iput p1, p0, Lcom/qualcomm/qti/config/PresenceConfig;->nickNameLength:I

    .line 301
    return-void
.end method

.method public setNoteMaxSize(I)V
    .locals 0
    .param p1, "noteMaxSize"    # I

    .prologue
    .line 143
    iput p1, p0, Lcom/qualcomm/qti/config/PresenceConfig;->noteMaxSize:I

    .line 144
    return-void
.end method

.method public setPublishTimer(I)V
    .locals 0
    .param p1, "publishTimer"    # I

    .prologue
    .line 164
    iput p1, p0, Lcom/qualcomm/qti/config/PresenceConfig;->publishTimer:I

    .line 165
    return-void
.end method

.method public setRlsURI(Ljava/lang/String;)V
    .locals 0
    .param p1, "rlsURI"    # Ljava/lang/String;

    .prologue
    .line 316
    iput-object p1, p0, Lcom/qualcomm/qti/config/PresenceConfig;->rlsURI:Ljava/lang/String;

    .line 317
    return-void
.end method

.method public setServiceUriTemplate(Ljava/lang/String;)V
    .locals 0
    .param p1, "serviceUriTemplate"    # Ljava/lang/String;

    .prologue
    .line 276
    iput-object p1, p0, Lcom/qualcomm/qti/config/PresenceConfig;->serviceUriTemplate:Ljava/lang/String;

    .line 277
    return-void
.end method

.method public setSourceThrottlePublish(I)V
    .locals 0
    .param p1, "sourceThrottlePublish"    # I

    .prologue
    .line 231
    iput p1, p0, Lcom/qualcomm/qti/config/PresenceConfig;->sourceThrottlePublish:I

    .line 232
    return-void
.end method

.method public setVipcontacts(Lcom/qualcomm/qti/config/VipContactsConfig;)V
    .locals 0
    .param p1, "vipcontacts"    # Lcom/qualcomm/qti/config/VipContactsConfig;

    .prologue
    .line 292
    iput-object p1, p0, Lcom/qualcomm/qti/config/PresenceConfig;->vipcontacts:Lcom/qualcomm/qti/config/VipContactsConfig;

    .line 293
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 335
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/PresenceConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 337
    return-void
.end method
