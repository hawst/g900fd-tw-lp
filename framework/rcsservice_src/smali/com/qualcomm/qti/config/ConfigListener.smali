.class public Lcom/qualcomm/qti/config/ConfigListener;
.super Ljava/lang/Object;
.source "ConfigListener.java"


# instance fields
.field m_IQConfigListener:Lcom/qualcomm/qti/config/IQConfigServiceListener;

.field m_configListenerHandle:J

.field m_configServiceHandle:I

.field m_configServiceListenerUserData:J

.field m_failureMessage:Lcom/qualcomm/qti/config/ConfigFailureMsg;

.field m_pConfigServiceListenerHdl:I

.field m_pFailureMessage:Lcom/qualcomm/qti/config/MsgConfig;

.field m_pSuccessMessage:Lcom/qualcomm/qti/config/MsgConfig;

.field m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

.field m_successMessage:Lcom/qualcomm/qti/config/ConfigSuccessMsg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static getConfigListenerInstance()Lcom/qualcomm/qti/config/ConfigListener;
    .locals 2

    .prologue
    .line 33
    const-string v0, "AIDL"

    const-string v1, "ConfigListener     getConfigListenerInstance  start   "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    new-instance v0, Lcom/qualcomm/qti/config/ConfigListener;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/ConfigListener;-><init>()V

    return-object v0
.end method


# virtual methods
.method public QConfigServiceListener_ConfigSettingsUpdated(J)V
    .locals 3
    .param p1, "configListenerHandle"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 160
    iput-wide p1, p0, Lcom/qualcomm/qti/config/ConfigListener;->m_configListenerHandle:J

    .line 162
    const-string v1, "AIDL"

    const-string v2, "  QConfigServiceListener_ConfigSettingsUpdated start outside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    new-instance v0, Lcom/qualcomm/qti/config/ConfigListener$5;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/config/ConfigListener$5;-><init>(Lcom/qualcomm/qti/config/ConfigListener;)V

    .line 181
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 182
    const-string v1, "AIDL"

    const-string v2, "  QConfigServiceListener_ConfigSettingsUpdated end outside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    return-void
.end method

.method public QConfigServiceListener_ReceivedFailureResponse(JLcom/qualcomm/qti/config/ConfigFailureMsg;)V
    .locals 3
    .param p1, "configListenerHandle"    # J
    .param p3, "failureMessage"    # Lcom/qualcomm/qti/config/ConfigFailureMsg;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 131
    iput-wide p1, p0, Lcom/qualcomm/qti/config/ConfigListener;->m_configListenerHandle:J

    .line 132
    iput-object p3, p0, Lcom/qualcomm/qti/config/ConfigListener;->m_failureMessage:Lcom/qualcomm/qti/config/ConfigFailureMsg;

    .line 133
    const-string v1, "AIDL"

    const-string v2, "  QConfigServiceListener_ReceivedFailureResponse start outside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    new-instance v0, Lcom/qualcomm/qti/config/ConfigListener$4;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/config/ConfigListener$4;-><init>(Lcom/qualcomm/qti/config/ConfigListener;)V

    .line 152
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 153
    const-string v1, "AIDL"

    const-string v2, "  QConfigServiceListener_ReceivedSuccessResponse end outside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    return-void
.end method

.method public QConfigServiceListener_ReceivedSuccessResponse(JLcom/qualcomm/qti/config/ConfigSuccessMsg;)V
    .locals 3
    .param p1, "configListenerHandle"    # J
    .param p3, "successMessage"    # Lcom/qualcomm/qti/config/ConfigSuccessMsg;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 102
    iput-wide p1, p0, Lcom/qualcomm/qti/config/ConfigListener;->m_configListenerHandle:J

    .line 103
    iput-object p3, p0, Lcom/qualcomm/qti/config/ConfigListener;->m_successMessage:Lcom/qualcomm/qti/config/ConfigSuccessMsg;

    .line 104
    const-string v1, "AIDL"

    const-string v2, "  QConfigServiceListener_ReceivedSuccessResponse start outside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    new-instance v0, Lcom/qualcomm/qti/config/ConfigListener$3;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/config/ConfigListener$3;-><init>(Lcom/qualcomm/qti/config/ConfigListener;)V

    .line 123
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 124
    const-string v1, "AIDL"

    const-string v2, "  QConfigServiceListener_ReceivedSuccessResponse end outside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    return-void
.end method

.method public QConfigServiceListener_RequestSent(J)V
    .locals 3
    .param p1, "configListenerHandle"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 75
    iput-wide p1, p0, Lcom/qualcomm/qti/config/ConfigListener;->m_configListenerHandle:J

    .line 76
    const-string v1, "AIDL"

    const-string v2, "  QConfigServiceListener_RequestSent start outside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    new-instance v0, Lcom/qualcomm/qti/config/ConfigListener$2;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/config/ConfigListener$2;-><init>(Lcom/qualcomm/qti/config/ConfigListener;)V

    .line 94
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 95
    const-string v1, "AIDL"

    const-string v2, "  QConfigServiceListener_RequestSent end outside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    return-void
.end method

.method QConfigServiceListener_ServiceCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;I)V
    .locals 3
    .param p1, "configServiceListenerUserData"    # J
    .param p3, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .param p4, "configServiceHandle"    # I

    .prologue
    .line 46
    iput-wide p1, p0, Lcom/qualcomm/qti/config/ConfigListener;->m_configServiceListenerUserData:J

    .line 47
    iput-object p3, p0, Lcom/qualcomm/qti/config/ConfigListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 48
    iput p4, p0, Lcom/qualcomm/qti/config/ConfigListener;->m_configServiceHandle:I

    .line 49
    const-string v1, "AIDL"

    const-string v2, "  QConfigServiceListener_ServiceCreated start outside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    new-instance v0, Lcom/qualcomm/qti/config/ConfigListener$1;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/config/ConfigListener$1;-><init>(Lcom/qualcomm/qti/config/ConfigListener;)V

    .line 68
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 69
    const-string v1, "AIDL"

    const-string v2, "  QConfigServiceListener_ServiceCreated end outside thread "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    return-void
.end method

.method public setIQConfigListener(Lcom/qualcomm/qti/config/IQConfigServiceListener;)V
    .locals 2
    .param p1, "objIQConfigListener"    # Lcom/qualcomm/qti/config/IQConfigServiceListener;

    .prologue
    .line 39
    const-string v0, "AIDL"

    const-string v1, "  setIQConfigListener start "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    iput-object p1, p0, Lcom/qualcomm/qti/config/ConfigListener;->m_IQConfigListener:Lcom/qualcomm/qti/config/IQConfigServiceListener;

    .line 41
    return-void
.end method
