.class public Lcom/qualcomm/qti/config/VersionConfig;
.super Ljava/lang/Object;
.source "VersionConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/VersionConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private validity:I

.field private version:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/qualcomm/qti/config/VersionConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/VersionConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/VersionConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/VersionConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 65
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/VersionConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/VersionConfig$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/VersionConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method

.method public getValidity()I
    .locals 1

    .prologue
    .line 102
    iget v0, p0, Lcom/qualcomm/qti/config/VersionConfig;->validity:I

    return v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/qualcomm/qti/config/VersionConfig;->version:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/VersionConfig;->version:I

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/VersionConfig;->validity:I

    .line 70
    return-void
.end method

.method public setValidity(I)V
    .locals 0
    .param p1, "validity"    # I

    .prologue
    .line 113
    iput p1, p0, Lcom/qualcomm/qti/config/VersionConfig;->validity:I

    .line 114
    return-void
.end method

.method public setVersion(I)V
    .locals 0
    .param p1, "version"    # I

    .prologue
    .line 92
    iput p1, p0, Lcom/qualcomm/qti/config/VersionConfig;->version:I

    .line 93
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 47
    iget v0, p0, Lcom/qualcomm/qti/config/VersionConfig;->version:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 48
    iget v0, p0, Lcom/qualcomm/qti/config/VersionConfig;->validity:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 49
    return-void
.end method
