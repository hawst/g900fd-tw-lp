.class public Lcom/qualcomm/qti/config/Services;
.super Ljava/lang/Object;
.source "Services.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/config/Services;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private chatAuth:Z

.field private ftAuth:Z

.field private geolocPullAuth:I

.field private geolocPushAuth:Z

.field private groupChatAuth:Z

.field private isAuth:Z

.field private presenceAuth:Z

.field private rcsIPVideoCallAuth:Z

.field private rcsIPVoiceCallAuth:Z

.field private standaloneMsgAuth:Z

.field private vsAuth:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcom/qualcomm/qti/config/Services$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/config/Services$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/config/Services;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/config/Services;->readFromParcel(Landroid/os/Parcel;)V

    .line 119
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/config/Services$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/config/Services$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/config/Services;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public getGeolocPullAuth()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/qualcomm/qti/config/Services;->geolocPullAuth:I

    return v0
.end method

.method public isChatAuth()Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->chatAuth:Z

    return v0
.end method

.method public isFtAuth()Z
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->ftAuth:Z

    return v0
.end method

.method public isGeolocPushAuth()Z
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->geolocPushAuth:Z

    return v0
.end method

.method public isGroupChatAuth()Z
    .locals 1

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->groupChatAuth:Z

    return v0
.end method

.method public isIsAuth()Z
    .locals 1

    .prologue
    .line 204
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->isAuth:Z

    return v0
.end method

.method public isPresenceAuth()Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->presenceAuth:Z

    return v0
.end method

.method public isRcsIPVideoCallAuth()Z
    .locals 1

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->rcsIPVideoCallAuth:Z

    return v0
.end method

.method public isRcsIPVoiceCallAuth()Z
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->rcsIPVoiceCallAuth:Z

    return v0
.end method

.method public isStandaloneMsgAuth()Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->standaloneMsgAuth:Z

    return v0
.end method

.method public isVsAuth()Z
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->vsAuth:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->presenceAuth:Z

    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->chatAuth:Z

    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->groupChatAuth:Z

    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->ftAuth:Z

    .line 127
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->standaloneMsgAuth:Z

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/config/Services;->geolocPullAuth:I

    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->geolocPushAuth:Z

    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->vsAuth:Z

    .line 131
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->isAuth:Z

    .line 132
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->rcsIPVoiceCallAuth:Z

    .line 133
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_9

    :goto_9
    iput-boolean v1, p0, Lcom/qualcomm/qti/config/Services;->rcsIPVideoCallAuth:Z

    .line 135
    return-void

    :cond_0
    move v0, v2

    .line 123
    goto :goto_0

    :cond_1
    move v0, v2

    .line 124
    goto :goto_1

    :cond_2
    move v0, v2

    .line 125
    goto :goto_2

    :cond_3
    move v0, v2

    .line 126
    goto :goto_3

    :cond_4
    move v0, v2

    .line 127
    goto :goto_4

    :cond_5
    move v0, v2

    .line 129
    goto :goto_5

    :cond_6
    move v0, v2

    .line 130
    goto :goto_6

    :cond_7
    move v0, v2

    .line 131
    goto :goto_7

    :cond_8
    move v0, v2

    .line 132
    goto :goto_8

    :cond_9
    move v1, v2

    .line 133
    goto :goto_9
.end method

.method public setChatAuth(Z)V
    .locals 0
    .param p1, "chatAuth"    # Z

    .prologue
    .line 152
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/Services;->chatAuth:Z

    .line 153
    return-void
.end method

.method public setFtAuth(Z)V
    .locals 0
    .param p1, "ftAuth"    # Z

    .prologue
    .line 168
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/Services;->ftAuth:Z

    .line 169
    return-void
.end method

.method public setGeolocPullAuth(I)V
    .locals 0
    .param p1, "geolocPullAuth"    # I

    .prologue
    .line 184
    iput p1, p0, Lcom/qualcomm/qti/config/Services;->geolocPullAuth:I

    .line 185
    return-void
.end method

.method public setGeolocPushAuth(Z)V
    .locals 0
    .param p1, "geolocPushAuth"    # Z

    .prologue
    .line 192
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/Services;->geolocPushAuth:Z

    .line 193
    return-void
.end method

.method public setGroupChatAuth(Z)V
    .locals 0
    .param p1, "groupChatAuth"    # Z

    .prologue
    .line 160
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/Services;->groupChatAuth:Z

    .line 161
    return-void
.end method

.method public setIsAuth(Z)V
    .locals 0
    .param p1, "isAuth"    # Z

    .prologue
    .line 208
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/Services;->isAuth:Z

    .line 209
    return-void
.end method

.method public setPresenceAuth(Z)V
    .locals 0
    .param p1, "presenceAuth"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/Services;->presenceAuth:Z

    .line 145
    return-void
.end method

.method public setRcsIPVideoCallAuth(Z)V
    .locals 0
    .param p1, "rcsIPVideoCallAuth"    # Z

    .prologue
    .line 224
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/Services;->rcsIPVideoCallAuth:Z

    .line 225
    return-void
.end method

.method public setRcsIPVoiceCallAuth(Z)V
    .locals 0
    .param p1, "rcsIPVoiceCallAuth"    # Z

    .prologue
    .line 216
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/Services;->rcsIPVoiceCallAuth:Z

    .line 217
    return-void
.end method

.method public setStandaloneMsgAuth(Z)V
    .locals 0
    .param p1, "standaloneMsgAuth"    # Z

    .prologue
    .line 176
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/Services;->standaloneMsgAuth:Z

    .line 177
    return-void
.end method

.method public setVsAuth(Z)V
    .locals 0
    .param p1, "vsAuth"    # Z

    .prologue
    .line 200
    iput-boolean p1, p0, Lcom/qualcomm/qti/config/Services;->vsAuth:Z

    .line 201
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 91
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->presenceAuth:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->chatAuth:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 93
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->groupChatAuth:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->ftAuth:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 95
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->standaloneMsgAuth:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    iget v0, p0, Lcom/qualcomm/qti/config/Services;->geolocPullAuth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 97
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->geolocPushAuth:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 98
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->vsAuth:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 99
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->isAuth:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 100
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->rcsIPVoiceCallAuth:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 101
    iget-boolean v0, p0, Lcom/qualcomm/qti/config/Services;->rcsIPVideoCallAuth:Z

    if-eqz v0, :cond_9

    :goto_9
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 103
    return-void

    :cond_0
    move v0, v2

    .line 91
    goto :goto_0

    :cond_1
    move v0, v2

    .line 92
    goto :goto_1

    :cond_2
    move v0, v2

    .line 93
    goto :goto_2

    :cond_3
    move v0, v2

    .line 94
    goto :goto_3

    :cond_4
    move v0, v2

    .line 95
    goto :goto_4

    :cond_5
    move v0, v2

    .line 97
    goto :goto_5

    :cond_6
    move v0, v2

    .line 98
    goto :goto_6

    :cond_7
    move v0, v2

    .line 99
    goto :goto_7

    :cond_8
    move v0, v2

    .line 100
    goto :goto_8

    :cond_9
    move v1, v2

    .line 101
    goto :goto_9
.end method
