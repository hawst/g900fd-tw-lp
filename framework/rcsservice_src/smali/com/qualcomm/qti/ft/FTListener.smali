.class public Lcom/qualcomm/qti/ft/FTListener;
.super Ljava/lang/Object;
.source "FTListener.java"


# instance fields
.field m_FTServiceListener:Lcom/qualcomm/qti/ft/IQFTServiceListener;

.field m_FTSessionListener:Lcom/qualcomm/qti/ft/IQFTSessionListener;

.field private m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

.field private m_error:Lcom/qualcomm/qti/rcsservice/Error;

.field private m_errorDesc:Ljava/lang/String;

.field m_featureTag:Ljava/lang/String;

.field m_ftServiceHandle:I

.field private m_ftServiceListenerUserData:J

.field private m_ftSessionHandle:I

.field private m_ftSessionHdl:I

.field private m_msgID:Ljava/lang/String;

.field private m_reqStatus:Lcom/qualcomm/qti/rcsservice/ReqStatus;

.field private m_reqUserData:I

.field private m_sessionInfo:Lcom/qualcomm/qti/rcsservice/SessionInfo;

.field m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

.field private m_statusInfo:[Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/qualcomm/qti/ft/FTListener;)J
    .locals 2
    .param p0, "x0"    # Lcom/qualcomm/qti/ft/FTListener;

    .prologue
    .line 20
    iget-wide v0, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftServiceListenerUserData:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/qualcomm/qti/ft/FTListener;)Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/ft/FTListener;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/qualcomm/qti/ft/FTListener;->m_sessionInfo:Lcom/qualcomm/qti/rcsservice/SessionInfo;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/qualcomm/qti/ft/FTListener;)Lcom/qualcomm/qti/rcsservice/ReqStatus;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/ft/FTListener;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/qualcomm/qti/ft/FTListener;->m_reqStatus:Lcom/qualcomm/qti/rcsservice/ReqStatus;

    return-object v0
.end method

.method static synthetic access$200(Lcom/qualcomm/qti/ft/FTListener;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/ft/FTListener;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/qualcomm/qti/ft/FTListener;->m_msgID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/qualcomm/qti/ft/FTListener;)Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/ft/FTListener;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/qualcomm/qti/ft/FTListener;->m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    return-object v0
.end method

.method static synthetic access$400(Lcom/qualcomm/qti/ft/FTListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/ft/FTListener;

    .prologue
    .line 20
    iget v0, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftSessionHandle:I

    return v0
.end method

.method static synthetic access$500(Lcom/qualcomm/qti/ft/FTListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/ft/FTListener;

    .prologue
    .line 20
    iget v0, p0, Lcom/qualcomm/qti/ft/FTListener;->m_reqUserData:I

    return v0
.end method

.method static synthetic access$600(Lcom/qualcomm/qti/ft/FTListener;)[Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/ft/FTListener;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/qualcomm/qti/ft/FTListener;->m_statusInfo:[Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;

    return-object v0
.end method

.method static synthetic access$700(Lcom/qualcomm/qti/ft/FTListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/ft/FTListener;

    .prologue
    .line 20
    iget v0, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftSessionHdl:I

    return v0
.end method

.method static synthetic access$800(Lcom/qualcomm/qti/ft/FTListener;)Lcom/qualcomm/qti/rcsservice/Error;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/ft/FTListener;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/qualcomm/qti/ft/FTListener;->m_error:Lcom/qualcomm/qti/rcsservice/Error;

    return-object v0
.end method

.method static synthetic access$900(Lcom/qualcomm/qti/ft/FTListener;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/ft/FTListener;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/qualcomm/qti/ft/FTListener;->m_errorDesc:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public QFTServiceListener_HandleContentDeliveryStatus([Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;)V
    .locals 1
    .param p1, "statusInfo"    # [Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 176
    iput-object p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_statusInfo:[Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;

    .line 177
    new-instance v0, Lcom/qualcomm/qti/ft/FTListener$6;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/FTListener$6;-><init>(Lcom/qualcomm/qti/ft/FTListener;)V

    .line 190
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 192
    return-void
.end method

.method public QFTServiceListener_HandleIncomingSession(Lcom/qualcomm/qti/rcsservice/SessionInfo;Ljava/lang/String;Lcom/qualcomm/qti/rcsservice/ContentInfo;)V
    .locals 1
    .param p1, "sessionInfo"    # Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .param p2, "msgID"    # Ljava/lang/String;
    .param p3, "contentInfo"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 125
    iput-object p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_sessionInfo:Lcom/qualcomm/qti/rcsservice/SessionInfo;

    .line 126
    iput-object p2, p0, Lcom/qualcomm/qti/ft/FTListener;->m_msgID:Ljava/lang/String;

    .line 127
    iput-object p3, p0, Lcom/qualcomm/qti/ft/FTListener;->m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 129
    new-instance v0, Lcom/qualcomm/qti/ft/FTListener$4;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/FTListener$4;-><init>(Lcom/qualcomm/qti/ft/FTListener;)V

    .line 142
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 144
    return-void
.end method

.method public QFTServiceListener_ServiceAvailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .locals 1
    .param p1, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 83
    iput-object p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 85
    new-instance v0, Lcom/qualcomm/qti/ft/FTListener$2;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/FTListener$2;-><init>(Lcom/qualcomm/qti/ft/FTListener;)V

    .line 97
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 99
    return-void
.end method

.method public QFTServiceListener_ServiceCreated(JLjava/lang/String;Lcom/qualcomm/qti/rcsservice/StatusCode;I)V
    .locals 1
    .param p1, "ftServiceListenerUserData"    # J
    .param p3, "featureTag"    # Ljava/lang/String;
    .param p4, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .param p5, "ftServiceHandle"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 58
    iput-wide p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftServiceListenerUserData:J

    .line 59
    iput-object p3, p0, Lcom/qualcomm/qti/ft/FTListener;->m_featureTag:Ljava/lang/String;

    .line 60
    iput-object p4, p0, Lcom/qualcomm/qti/ft/FTListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 61
    iput p5, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftServiceHandle:I

    .line 63
    new-instance v0, Lcom/qualcomm/qti/ft/FTListener$1;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/FTListener$1;-><init>(Lcom/qualcomm/qti/ft/FTListener;)V

    .line 77
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 79
    return-void
.end method

.method public QFTServiceListener_ServiceUnavailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .locals 1
    .param p1, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 103
    iput-object p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 105
    new-instance v0, Lcom/qualcomm/qti/ft/FTListener$3;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/FTListener$3;-><init>(Lcom/qualcomm/qti/ft/FTListener;)V

    .line 117
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 119
    return-void
.end method

.method public QFTServiceListener_SessionCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;ILjava/lang/String;I)V
    .locals 1
    .param p1, "ftServiceListenerUserData"    # J
    .param p3, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .param p4, "ftSessionHandle"    # I
    .param p5, "msgID"    # Ljava/lang/String;
    .param p6, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 150
    iput-wide p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftServiceListenerUserData:J

    .line 151
    iput-object p3, p0, Lcom/qualcomm/qti/ft/FTListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 152
    iput p4, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftSessionHandle:I

    .line 153
    iput-object p5, p0, Lcom/qualcomm/qti/ft/FTListener;->m_msgID:Ljava/lang/String;

    .line 154
    iput p6, p0, Lcom/qualcomm/qti/ft/FTListener;->m_reqUserData:I

    .line 155
    new-instance v0, Lcom/qualcomm/qti/ft/FTListener$5;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/FTListener$5;-><init>(Lcom/qualcomm/qti/ft/FTListener;)V

    .line 169
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 171
    return-void
.end method

.method public QFTSessionListener_HandleInteruption(ILcom/qualcomm/qti/rcsservice/ContentInfo;)V
    .locals 1
    .param p1, "iMSessionHandle"    # I
    .param p2, "contentInfo"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 354
    iput p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftSessionHdl:I

    .line 355
    iput-object p2, p0, Lcom/qualcomm/qti/ft/FTListener;->m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 357
    new-instance v0, Lcom/qualcomm/qti/ft/FTListener$14;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/FTListener$14;-><init>(Lcom/qualcomm/qti/ft/FTListener;)V

    .line 370
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 372
    return-void
.end method

.method public QFTSessionListener_HandleSessionCanceled(I)V
    .locals 1
    .param p1, "ftSessionHandle"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 219
    iput p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftSessionHdl:I

    .line 221
    new-instance v0, Lcom/qualcomm/qti/ft/FTListener$8;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/FTListener$8;-><init>(Lcom/qualcomm/qti/ft/FTListener;)V

    .line 233
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 235
    return-void
.end method

.method public QFTSessionListener_HandleSessionClosed(I)V
    .locals 1
    .param p1, "ftSessionHandle"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 239
    iput p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftSessionHdl:I

    .line 241
    new-instance v0, Lcom/qualcomm/qti/ft/FTListener$9;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/FTListener$9;-><init>(Lcom/qualcomm/qti/ft/FTListener;)V

    .line 253
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 255
    return-void
.end method

.method public QFTSessionListener_HandleSessionEstablished(I)V
    .locals 1
    .param p1, "ftSessionHandle"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 198
    iput p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftSessionHdl:I

    .line 200
    new-instance v0, Lcom/qualcomm/qti/ft/FTListener$7;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/FTListener$7;-><init>(Lcom/qualcomm/qti/ft/FTListener;)V

    .line 212
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 214
    return-void
.end method

.method public QFTSessionListener_HandleSessionInfoUpdate(ILcom/qualcomm/qti/rcsservice/SessionInfo;)V
    .locals 1
    .param p1, "ftSessionHandle"    # I
    .param p2, "sessionInfo"    # Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 331
    iput p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftSessionHdl:I

    .line 332
    iput-object p2, p0, Lcom/qualcomm/qti/ft/FTListener;->m_sessionInfo:Lcom/qualcomm/qti/rcsservice/SessionInfo;

    .line 334
    new-instance v0, Lcom/qualcomm/qti/ft/FTListener$13;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/FTListener$13;-><init>(Lcom/qualcomm/qti/ft/FTListener;)V

    .line 347
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 349
    return-void
.end method

.method public QFTSessionListener_HandleSessionReqStatus(ILcom/qualcomm/qti/rcsservice/ReqStatus;)V
    .locals 1
    .param p1, "ftSessionHandle"    # I
    .param p2, "reqStatus"    # Lcom/qualcomm/qti/rcsservice/ReqStatus;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 377
    iput p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftSessionHdl:I

    .line 378
    iput-object p2, p0, Lcom/qualcomm/qti/ft/FTListener;->m_reqStatus:Lcom/qualcomm/qti/rcsservice/ReqStatus;

    .line 380
    new-instance v0, Lcom/qualcomm/qti/ft/FTListener$15;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/FTListener$15;-><init>(Lcom/qualcomm/qti/ft/FTListener;)V

    .line 393
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 395
    return-void
.end method

.method public QFTSessionListener_HandleTransferError(ILcom/qualcomm/qti/rcsservice/Error;Ljava/lang/String;)V
    .locals 1
    .param p1, "ftSessionHandle"    # I
    .param p2, "errorCode"    # Lcom/qualcomm/qti/rcsservice/Error;
    .param p3, "errorDesc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 283
    iput p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftSessionHdl:I

    .line 284
    iput-object p2, p0, Lcom/qualcomm/qti/ft/FTListener;->m_error:Lcom/qualcomm/qti/rcsservice/Error;

    .line 285
    iput-object p3, p0, Lcom/qualcomm/qti/ft/FTListener;->m_errorDesc:Ljava/lang/String;

    .line 287
    new-instance v0, Lcom/qualcomm/qti/ft/FTListener$11;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/FTListener$11;-><init>(Lcom/qualcomm/qti/ft/FTListener;)V

    .line 300
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 302
    return-void
.end method

.method public QFTSessionListener_HandleTransferProgress(ILcom/qualcomm/qti/rcsservice/ContentInfo;)V
    .locals 1
    .param p1, "ftSessionHandle"    # I
    .param p2, "contentInfo"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 260
    iput p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftSessionHdl:I

    .line 261
    iput-object p2, p0, Lcom/qualcomm/qti/ft/FTListener;->m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 263
    new-instance v0, Lcom/qualcomm/qti/ft/FTListener$10;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/FTListener$10;-><init>(Lcom/qualcomm/qti/ft/FTListener;)V

    .line 276
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 278
    return-void
.end method

.method public QFTSessionListener_HandleTransferSuccess(ILcom/qualcomm/qti/rcsservice/ContentInfo;)V
    .locals 1
    .param p1, "ftSessionHandle"    # I
    .param p2, "contentInfo"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 307
    iput p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_ftSessionHdl:I

    .line 308
    iput-object p2, p0, Lcom/qualcomm/qti/ft/FTListener;->m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 310
    new-instance v0, Lcom/qualcomm/qti/ft/FTListener$12;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/FTListener$12;-><init>(Lcom/qualcomm/qti/ft/FTListener;)V

    .line 323
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 325
    return-void
.end method

.method public setIQFTServiceListener(Lcom/qualcomm/qti/ft/IQFTServiceListener;)V
    .locals 2
    .param p1, "objIQFTServiceListener"    # Lcom/qualcomm/qti/ft/IQFTServiceListener;

    .prologue
    .line 44
    const-string v0, "AIDL"

    const-string v1, "  setIQFTServiceListener start "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    iput-object p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_FTServiceListener:Lcom/qualcomm/qti/ft/IQFTServiceListener;

    .line 46
    return-void
.end method

.method public setIQFTSessionListener(Lcom/qualcomm/qti/ft/IQFTSessionListener;)V
    .locals 2
    .param p1, "objIQFTSessionListener"    # Lcom/qualcomm/qti/ft/IQFTSessionListener;

    .prologue
    .line 50
    const-string v0, "AIDL"

    const-string v1, "  setIQFTSessionListener start "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    iput-object p1, p0, Lcom/qualcomm/qti/ft/FTListener;->m_FTSessionListener:Lcom/qualcomm/qti/ft/IQFTSessionListener;

    .line 52
    return-void
.end method
