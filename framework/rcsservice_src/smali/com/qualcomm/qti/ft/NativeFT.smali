.class public Lcom/qualcomm/qti/ft/NativeFT;
.super Ljava/lang/Object;
.source "NativeFT.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native QFTServiceAddListener(ILcom/qualcomm/qti/ft/IQFTServiceListener;)J
.end method

.method public native QFTServiceFreeSessionList(ILcom/qualcomm/qti/rcsservice/SessionList;)V
.end method

.method public native QFTServiceGetActiveSessions(ILcom/qualcomm/qti/rcsservice/SessionList;Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QFTServiceGetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QFTServiceRemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QFTServiceTransferFile(ILcom/qualcomm/qti/ft/IQFTSessionListener;Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;Lcom/qualcomm/qti/rcsservice/ContentInfo;I)I
.end method

.method public native QFTSessionAccept(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QFTSessionAddListener(ILcom/qualcomm/qti/ft/IQFTSessionListener;)J
.end method

.method public native QFTSessionCancel(II)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QFTSessionFreeContentInfo(ILcom/qualcomm/qti/rcsservice/ContentInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QFTSessionFreeSessionInfo(ILcom/qualcomm/qti/rcsservice/SessionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QFTSessionGetContentInfo(ILcom/qualcomm/qti/rcsservice/ContentInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QFTSessionGetSessionInfo(ILcom/qualcomm/qti/rcsservice/SessionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QFTSessionReject(II)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QFTSessionRemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method
