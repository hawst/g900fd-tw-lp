.class public abstract Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub;
.super Landroid/os/Binder;
.source "IQFTSessionListener.java"

# interfaces
.implements Lcom/qualcomm/qti/ft/IQFTSessionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/ft/IQFTSessionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.ft.IQFTSessionListener"

.field static final TRANSACTION_QFTSessionListener_HandleInteruption:I = 0x8

.field static final TRANSACTION_QFTSessionListener_HandleSessionCanceled:I = 0x2

.field static final TRANSACTION_QFTSessionListener_HandleSessionClosed:I = 0x3

.field static final TRANSACTION_QFTSessionListener_HandleSessionEstablished:I = 0x1

.field static final TRANSACTION_QFTSessionListener_HandleSessionInfoUpdate:I = 0x7

.field static final TRANSACTION_QFTSessionListener_HandleSessionReqStatus:I = 0x9

.field static final TRANSACTION_QFTSessionListener_HandleTransferError:I = 0x5

.field static final TRANSACTION_QFTSessionListener_HandleTransferProgress:I = 0x4

.field static final TRANSACTION_QFTSessionListener_HandleTransferSuccess:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.ft.IQFTSessionListener"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/ft/IQFTSessionListener;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.ft.IQFTSessionListener"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/ft/IQFTSessionListener;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/ft/IQFTSessionListener;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 173
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v3

    :goto_0
    return v3

    .line 44
    :sswitch_0
    const-string v4, "com.qualcomm.qti.ft.IQFTSessionListener"

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v4, "com.qualcomm.qti.ft.IQFTSessionListener"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 52
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub;->QFTSessionListener_HandleSessionEstablished(I)V

    .line 53
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 58
    .end local v0    # "_arg0":I
    :sswitch_2
    const-string v4, "com.qualcomm.qti.ft.IQFTSessionListener"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 61
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub;->QFTSessionListener_HandleSessionCanceled(I)V

    .line 62
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 67
    .end local v0    # "_arg0":I
    :sswitch_3
    const-string v4, "com.qualcomm.qti.ft.IQFTSessionListener"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 70
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub;->QFTSessionListener_HandleSessionClosed(I)V

    .line 71
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 76
    .end local v0    # "_arg0":I
    :sswitch_4
    const-string v4, "com.qualcomm.qti.ft.IQFTSessionListener"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 80
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_0

    .line 81
    sget-object v4, Lcom/qualcomm/qti/rcsservice/ContentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 86
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub;->QFTSessionListener_HandleTransferProgress(ILcom/qualcomm/qti/rcsservice/ContentInfo;)V

    .line 87
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 84
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    goto :goto_1

    .line 92
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :sswitch_5
    const-string v4, "com.qualcomm.qti.ft.IQFTSessionListener"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 96
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1

    .line 97
    sget-object v4, Lcom/qualcomm/qti/rcsservice/Error;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/Error;

    .line 103
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/Error;
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 104
    .local v2, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub;->QFTSessionListener_HandleTransferError(ILcom/qualcomm/qti/rcsservice/Error;Ljava/lang/String;)V

    .line 105
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 100
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/Error;
    .end local v2    # "_arg2":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/Error;
    goto :goto_2

    .line 110
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/Error;
    :sswitch_6
    const-string v4, "com.qualcomm.qti.ft.IQFTSessionListener"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 114
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2

    .line 115
    sget-object v4, Lcom/qualcomm/qti/rcsservice/ContentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 120
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :goto_3
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub;->QFTSessionListener_HandleTransferSuccess(ILcom/qualcomm/qti/rcsservice/ContentInfo;)V

    .line 121
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 118
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    goto :goto_3

    .line 126
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :sswitch_7
    const-string v4, "com.qualcomm.qti.ft.IQFTSessionListener"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 128
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 130
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3

    .line 131
    sget-object v4, Lcom/qualcomm/qti/rcsservice/SessionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/SessionInfo;

    .line 136
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    :goto_4
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub;->QFTSessionListener_HandleSessionInfoUpdate(ILcom/qualcomm/qti/rcsservice/SessionInfo;)V

    .line 137
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 134
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    :cond_3
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    goto :goto_4

    .line 142
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    :sswitch_8
    const-string v4, "com.qualcomm.qti.ft.IQFTSessionListener"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 144
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 146
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_4

    .line 147
    sget-object v4, Lcom/qualcomm/qti/rcsservice/ContentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 152
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :goto_5
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub;->QFTSessionListener_HandleInteruption(ILcom/qualcomm/qti/rcsservice/ContentInfo;)V

    .line 153
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 150
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    goto :goto_5

    .line 158
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :sswitch_9
    const-string v4, "com.qualcomm.qti.ft.IQFTSessionListener"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 160
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 162
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_5

    .line 163
    sget-object v4, Lcom/qualcomm/qti/rcsservice/ReqStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/ReqStatus;

    .line 168
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/ReqStatus;
    :goto_6
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub;->QFTSessionListener_HandleSessionReqStatus(ILcom/qualcomm/qti/rcsservice/ReqStatus;)V

    .line 169
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 166
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/ReqStatus;
    :cond_5
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/ReqStatus;
    goto :goto_6

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
