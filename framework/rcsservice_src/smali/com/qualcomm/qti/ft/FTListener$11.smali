.class Lcom/qualcomm/qti/ft/FTListener$11;
.super Ljava/lang/Thread;
.source "FTListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/qti/ft/FTListener;->QFTSessionListener_HandleTransferError(ILcom/qualcomm/qti/rcsservice/Error;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/ft/FTListener;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/ft/FTListener;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/qualcomm/qti/ft/FTListener$11;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 293
    :try_start_0
    iget-object v1, p0, Lcom/qualcomm/qti/ft/FTListener$11;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    iget-object v1, v1, Lcom/qualcomm/qti/ft/FTListener;->m_FTSessionListener:Lcom/qualcomm/qti/ft/IQFTSessionListener;

    iget-object v2, p0, Lcom/qualcomm/qti/ft/FTListener$11;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    # getter for: Lcom/qualcomm/qti/ft/FTListener;->m_ftSessionHdl:I
    invoke-static {v2}, Lcom/qualcomm/qti/ft/FTListener;->access$700(Lcom/qualcomm/qti/ft/FTListener;)I

    move-result v2

    iget-object v3, p0, Lcom/qualcomm/qti/ft/FTListener$11;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    # getter for: Lcom/qualcomm/qti/ft/FTListener;->m_error:Lcom/qualcomm/qti/rcsservice/Error;
    invoke-static {v3}, Lcom/qualcomm/qti/ft/FTListener;->access$800(Lcom/qualcomm/qti/ft/FTListener;)Lcom/qualcomm/qti/rcsservice/Error;

    move-result-object v3

    iget-object v4, p0, Lcom/qualcomm/qti/ft/FTListener$11;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    # getter for: Lcom/qualcomm/qti/ft/FTListener;->m_errorDesc:Ljava/lang/String;
    invoke-static {v4}, Lcom/qualcomm/qti/ft/FTListener;->access$900(Lcom/qualcomm/qti/ft/FTListener;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/qualcomm/qti/ft/IQFTSessionListener;->QFTSessionListener_HandleTransferError(ILcom/qualcomm/qti/rcsservice/Error;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    :goto_0
    return-void

    .line 295
    :catch_0
    move-exception v0

    .line 296
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
