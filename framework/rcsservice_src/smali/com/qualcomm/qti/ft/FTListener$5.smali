.class Lcom/qualcomm/qti/ft/FTListener$5;
.super Ljava/lang/Thread;
.source "FTListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/qti/ft/FTListener;->QFTServiceListener_SessionCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;ILjava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/ft/FTListener;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/ft/FTListener;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/qualcomm/qti/ft/FTListener$5;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 161
    :try_start_0
    iget-object v1, p0, Lcom/qualcomm/qti/ft/FTListener$5;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    iget-object v1, v1, Lcom/qualcomm/qti/ft/FTListener;->m_FTServiceListener:Lcom/qualcomm/qti/ft/IQFTServiceListener;

    iget-object v2, p0, Lcom/qualcomm/qti/ft/FTListener$5;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    # getter for: Lcom/qualcomm/qti/ft/FTListener;->m_ftServiceListenerUserData:J
    invoke-static {v2}, Lcom/qualcomm/qti/ft/FTListener;->access$000(Lcom/qualcomm/qti/ft/FTListener;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/qualcomm/qti/ft/FTListener$5;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    iget-object v4, v4, Lcom/qualcomm/qti/ft/FTListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    iget-object v5, p0, Lcom/qualcomm/qti/ft/FTListener$5;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    # getter for: Lcom/qualcomm/qti/ft/FTListener;->m_ftSessionHandle:I
    invoke-static {v5}, Lcom/qualcomm/qti/ft/FTListener;->access$400(Lcom/qualcomm/qti/ft/FTListener;)I

    move-result v5

    iget-object v6, p0, Lcom/qualcomm/qti/ft/FTListener$5;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    # getter for: Lcom/qualcomm/qti/ft/FTListener;->m_msgID:Ljava/lang/String;
    invoke-static {v6}, Lcom/qualcomm/qti/ft/FTListener;->access$200(Lcom/qualcomm/qti/ft/FTListener;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/qualcomm/qti/ft/FTListener$5;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    # getter for: Lcom/qualcomm/qti/ft/FTListener;->m_reqUserData:I
    invoke-static {v7}, Lcom/qualcomm/qti/ft/FTListener;->access$500(Lcom/qualcomm/qti/ft/FTListener;)I

    move-result v7

    invoke-interface/range {v1 .. v7}, Lcom/qualcomm/qti/ft/IQFTServiceListener;->QFTServiceListener_SessionCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;ILjava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :goto_0
    return-void

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
