.class public abstract Lcom/qualcomm/qti/ft/IQFTServiceListener$Stub;
.super Landroid/os/Binder;
.source "IQFTServiceListener.java"

# interfaces
.implements Lcom/qualcomm/qti/ft/IQFTServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/ft/IQFTServiceListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/ft/IQFTServiceListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.ft.IQFTServiceListener"

.field static final TRANSACTION_QFTServiceListener_HandleContentDeliveryStatus:I = 0x6

.field static final TRANSACTION_QFTServiceListener_HandleIncomingSession:I = 0x4

.field static final TRANSACTION_QFTServiceListener_ServiceAvailable:I = 0x2

.field static final TRANSACTION_QFTServiceListener_ServiceCreated:I = 0x1

.field static final TRANSACTION_QFTServiceListener_ServiceUnavailable:I = 0x3

.field static final TRANSACTION_QFTServiceListener_SessionCreated:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.ft.IQFTServiceListener"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/ft/IQFTServiceListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/ft/IQFTServiceListener;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.ft.IQFTServiceListener"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/ft/IQFTServiceListener;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/ft/IQFTServiceListener;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/ft/IQFTServiceListener$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/IQFTServiceListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 150
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 44
    :sswitch_0
    const-string v1, "com.qualcomm.qti.ft.IQFTServiceListener"

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v1, "com.qualcomm.qti.ft.IQFTServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 53
    .local v2, "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 55
    .local v4, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    sget-object v1, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 62
    .local v5, "_arg2":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .local v6, "_arg3":I
    move-object v1, p0

    .line 63
    invoke-virtual/range {v1 .. v6}, Lcom/qualcomm/qti/ft/IQFTServiceListener$Stub;->QFTServiceListener_ServiceCreated(JLjava/lang/String;Lcom/qualcomm/qti/rcsservice/StatusCode;I)V

    .line 64
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 59
    .end local v5    # "_arg2":Lcom/qualcomm/qti/rcsservice/StatusCode;
    .end local v6    # "_arg3":I
    :cond_0
    const/4 v5, 0x0

    .restart local v5    # "_arg2":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_1

    .line 69
    .end local v2    # "_arg0":J
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_2
    const-string v1, "com.qualcomm.qti.ft.IQFTServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 71
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_1

    .line 72
    sget-object v1, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 77
    .local v2, "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_2
    invoke-virtual {p0, v2}, Lcom/qualcomm/qti/ft/IQFTServiceListener$Stub;->QFTServiceListener_ServiceAvailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V

    .line 78
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 75
    .end local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_2

    .line 83
    .end local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_3
    const-string v1, "com.qualcomm.qti.ft.IQFTServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_2

    .line 86
    sget-object v1, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 91
    .restart local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_3
    invoke-virtual {p0, v2}, Lcom/qualcomm/qti/ft/IQFTServiceListener$Stub;->QFTServiceListener_ServiceUnavailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V

    .line 92
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 89
    .end local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_3

    .line 97
    .end local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_4
    const-string v1, "com.qualcomm.qti.ft.IQFTServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 99
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_3

    .line 100
    sget-object v1, Lcom/qualcomm/qti/rcsservice/SessionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/SessionInfo;

    .line 106
    .local v2, "_arg0":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 108
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_4

    .line 109
    sget-object v1, Lcom/qualcomm/qti/rcsservice/ContentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 114
    .local v5, "_arg2":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :goto_5
    invoke-virtual {p0, v2, v4, v5}, Lcom/qualcomm/qti/ft/IQFTServiceListener$Stub;->QFTServiceListener_HandleIncomingSession(Lcom/qualcomm/qti/rcsservice/SessionInfo;Ljava/lang/String;Lcom/qualcomm/qti/rcsservice/ContentInfo;)V

    .line 115
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 103
    .end local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    goto :goto_4

    .line 112
    .restart local v4    # "_arg1":Ljava/lang/String;
    :cond_4
    const/4 v5, 0x0

    .restart local v5    # "_arg2":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    goto :goto_5

    .line 120
    .end local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :sswitch_5
    const-string v1, "com.qualcomm.qti.ft.IQFTServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 124
    .local v2, "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_5

    .line 125
    sget-object v1, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 131
    .local v4, "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 133
    .local v5, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 135
    .local v6, "_arg3":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .local v7, "_arg4":I
    move-object v1, p0

    .line 136
    invoke-virtual/range {v1 .. v7}, Lcom/qualcomm/qti/ft/IQFTServiceListener$Stub;->QFTServiceListener_SessionCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;ILjava/lang/String;I)V

    .line 137
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 128
    .end local v4    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    .end local v5    # "_arg2":I
    .end local v6    # "_arg3":Ljava/lang/String;
    .end local v7    # "_arg4":I
    :cond_5
    const/4 v4, 0x0

    .restart local v4    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_6

    .line 142
    .end local v2    # "_arg0":J
    .end local v4    # "_arg1":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_6
    const-string v1, "com.qualcomm.qti.ft.IQFTServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 144
    sget-object v1, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;

    .line 145
    .local v2, "_arg0":[Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;
    invoke-virtual {p0, v2}, Lcom/qualcomm/qti/ft/IQFTServiceListener$Stub;->QFTServiceListener_HandleContentDeliveryStatus([Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;)V

    .line 146
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
