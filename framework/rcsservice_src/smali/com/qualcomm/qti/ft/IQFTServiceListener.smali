.class public interface abstract Lcom/qualcomm/qti/ft/IQFTServiceListener;
.super Ljava/lang/Object;
.source "IQFTServiceListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/ft/IQFTServiceListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract QFTServiceListener_HandleContentDeliveryStatus([Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QFTServiceListener_HandleIncomingSession(Lcom/qualcomm/qti/rcsservice/SessionInfo;Ljava/lang/String;Lcom/qualcomm/qti/rcsservice/ContentInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QFTServiceListener_ServiceAvailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QFTServiceListener_ServiceCreated(JLjava/lang/String;Lcom/qualcomm/qti/rcsservice/StatusCode;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QFTServiceListener_ServiceUnavailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QFTServiceListener_SessionCreated(JLcom/qualcomm/qti/rcsservice/StatusCode;ILjava/lang/String;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
