.class Lcom/qualcomm/qti/ft/FTListener$4;
.super Ljava/lang/Thread;
.source "FTListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/qti/ft/FTListener;->QFTServiceListener_HandleIncomingSession(Lcom/qualcomm/qti/rcsservice/SessionInfo;Ljava/lang/String;Lcom/qualcomm/qti/rcsservice/ContentInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/ft/FTListener;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/ft/FTListener;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/qualcomm/qti/ft/FTListener$4;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 135
    :try_start_0
    iget-object v1, p0, Lcom/qualcomm/qti/ft/FTListener$4;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    iget-object v1, v1, Lcom/qualcomm/qti/ft/FTListener;->m_FTServiceListener:Lcom/qualcomm/qti/ft/IQFTServiceListener;

    iget-object v2, p0, Lcom/qualcomm/qti/ft/FTListener$4;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    # getter for: Lcom/qualcomm/qti/ft/FTListener;->m_sessionInfo:Lcom/qualcomm/qti/rcsservice/SessionInfo;
    invoke-static {v2}, Lcom/qualcomm/qti/ft/FTListener;->access$100(Lcom/qualcomm/qti/ft/FTListener;)Lcom/qualcomm/qti/rcsservice/SessionInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/qualcomm/qti/ft/FTListener$4;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    # getter for: Lcom/qualcomm/qti/ft/FTListener;->m_msgID:Ljava/lang/String;
    invoke-static {v3}, Lcom/qualcomm/qti/ft/FTListener;->access$200(Lcom/qualcomm/qti/ft/FTListener;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/qualcomm/qti/ft/FTListener$4;->this$0:Lcom/qualcomm/qti/ft/FTListener;

    # getter for: Lcom/qualcomm/qti/ft/FTListener;->m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;
    invoke-static {v4}, Lcom/qualcomm/qti/ft/FTListener;->access$300(Lcom/qualcomm/qti/ft/FTListener;)Lcom/qualcomm/qti/rcsservice/ContentInfo;

    move-result-object v4

    invoke-interface {v1, v2, v3, v4}, Lcom/qualcomm/qti/ft/IQFTServiceListener;->QFTServiceListener_HandleIncomingSession(Lcom/qualcomm/qti/rcsservice/SessionInfo;Ljava/lang/String;Lcom/qualcomm/qti/rcsservice/ContentInfo;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :goto_0
    return-void

    .line 137
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
