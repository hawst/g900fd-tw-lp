.class Lcom/qualcomm/qti/ft/QFTService$1;
.super Lcom/qualcomm/qti/ft/IQFTService$Stub;
.source "QFTService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/ft/QFTService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

.field final synthetic this$0:Lcom/qualcomm/qti/ft/QFTService;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/ft/QFTService;)V
    .locals 1

    .prologue
    .line 29
    iput-object p1, p0, Lcom/qualcomm/qti/ft/QFTService$1;->this$0:Lcom/qualcomm/qti/ft/QFTService;

    invoke-direct {p0}, Lcom/qualcomm/qti/ft/IQFTService$Stub;-><init>()V

    .line 31
    new-instance v0, Lcom/qualcomm/qti/ft/NativeFT;

    invoke-direct {v0}, Lcom/qualcomm/qti/ft/NativeFT;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    return-void
.end method


# virtual methods
.method public QFTService_AddListener(ILcom/qualcomm/qti/ft/IQFTServiceListener;)J
    .locals 2
    .param p1, "ftServiceHdl"    # I
    .param p2, "ftServiceListener"    # Lcom/qualcomm/qti/ft/IQFTServiceListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/ft/NativeFT;->QFTServiceAddListener(ILcom/qualcomm/qti/ft/IQFTServiceListener;)J

    move-result-wide v0

    return-wide v0
.end method

.method public QFTService_FreeSessionList(ILcom/qualcomm/qti/rcsservice/SessionList;)V
    .locals 1
    .param p1, "ftServiceHdl"    # I
    .param p2, "sessionList"    # Lcom/qualcomm/qti/rcsservice/SessionList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/ft/NativeFT;->QFTServiceFreeSessionList(ILcom/qualcomm/qti/rcsservice/SessionList;)V

    .line 69
    return-void
.end method

.method public QFTService_GetActiveSessions(ILcom/qualcomm/qti/rcsservice/SessionList;Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "ftServiceHdl"    # I
    .param p2, "sessionList"    # Lcom/qualcomm/qti/rcsservice/SessionList;
    .param p3, "remoteContactURI"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/ft/NativeFT;->QFTServiceGetActiveSessions(ILcom/qualcomm/qti/rcsservice/SessionList;Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QFTService_GetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "ftServiceHdl"    # I
    .param p2, "versionInfo"    # Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/ft/NativeFT;->QFTServiceGetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QFTService_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 2
    .param p1, "ftServiceHdl"    # I
    .param p2, "ftServiceListener"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/ft/NativeFT;->QFTServiceRemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QFTService_TransferFile(ILcom/qualcomm/qti/ft/IQFTSessionListener;Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;Lcom/qualcomm/qti/rcsservice/ContentInfo;I)I
    .locals 6
    .param p1, "ftServiceHdl"    # I
    .param p2, "ftSessionListener"    # Lcom/qualcomm/qti/ft/IQFTSessionListener;
    .param p3, "sessionInfo"    # Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;
    .param p4, "contentInfo"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .param p5, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qti/ft/NativeFT;->QFTServiceTransferFile(ILcom/qualcomm/qti/ft/IQFTSessionListener;Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;Lcom/qualcomm/qti/rcsservice/ContentInfo;I)I

    move-result v0

    return v0
.end method

.method public QFTSession_Accept(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "ftSessionHdl"    # I
    .param p2, "pFilePath"    # Ljava/lang/String;
    .param p3, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/ft/NativeFT;->QFTSessionAccept(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QFTSession_AddListener(ILcom/qualcomm/qti/ft/IQFTSessionListener;)J
    .locals 2
    .param p1, "ftSessionHdl"    # I
    .param p2, "ftSessionListener"    # Lcom/qualcomm/qti/ft/IQFTSessionListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/ft/NativeFT;->QFTSessionAddListener(ILcom/qualcomm/qti/ft/IQFTSessionListener;)J

    move-result-wide v0

    return-wide v0
.end method

.method public QFTSession_Cancel(II)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "ftSessionHdl"    # I
    .param p2, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/ft/NativeFT;->QFTSessionCancel(II)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QFTSession_FreeContentInfo(ILcom/qualcomm/qti/rcsservice/ContentInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "ftSessionHdl"    # I
    .param p2, "ContentInfo"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/ft/NativeFT;->QFTSessionFreeContentInfo(ILcom/qualcomm/qti/rcsservice/ContentInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QFTSession_FreeSessionInfo(ILcom/qualcomm/qti/rcsservice/SessionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "ftSessionHdl"    # I
    .param p2, "sessionInfo"    # Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/ft/NativeFT;->QFTSessionFreeSessionInfo(ILcom/qualcomm/qti/rcsservice/SessionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QFTSession_GetContentInfo(ILcom/qualcomm/qti/rcsservice/ContentInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "ftSessionHdl"    # I
    .param p2, "contentInfo"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/ft/NativeFT;->QFTSessionGetContentInfo(ILcom/qualcomm/qti/rcsservice/ContentInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QFTSession_GetSessionInfo(ILcom/qualcomm/qti/rcsservice/SessionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "ftSessionHdl"    # I
    .param p2, "sessionInfo"    # Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/ft/NativeFT;->QFTSessionGetSessionInfo(ILcom/qualcomm/qti/rcsservice/SessionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QFTSession_Reject(II)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "ftSessionHdl"    # I
    .param p2, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/ft/NativeFT;->QFTSessionReject(II)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QFTSession_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 2
    .param p1, "ftSessionHdl"    # I
    .param p2, "ftSessionListenerHdl"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/qualcomm/qti/ft/QFTService$1;->objNativeFT:Lcom/qualcomm/qti/ft/NativeFT;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/ft/NativeFT;->QFTSessionRemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 122
    :try_start_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 124
    :catch_0
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v1, "AIDL_QFTService"

    const-string v2, "Unexpected remote exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 127
    throw v0
.end method
