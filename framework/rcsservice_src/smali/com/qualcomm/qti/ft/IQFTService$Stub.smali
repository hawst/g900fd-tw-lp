.class public abstract Lcom/qualcomm/qti/ft/IQFTService$Stub;
.super Landroid/os/Binder;
.source "IQFTService.java"

# interfaces
.implements Lcom/qualcomm/qti/ft/IQFTService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/ft/IQFTService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/ft/IQFTService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.ft.IQFTService"

.field static final TRANSACTION_QFTService_AddListener:I = 0x2

.field static final TRANSACTION_QFTService_FreeSessionList:I = 0x6

.field static final TRANSACTION_QFTService_GetActiveSessions:I = 0x5

.field static final TRANSACTION_QFTService_GetVersion:I = 0x1

.field static final TRANSACTION_QFTService_RemoveListener:I = 0x3

.field static final TRANSACTION_QFTService_TransferFile:I = 0x4

.field static final TRANSACTION_QFTSession_Accept:I = 0xd

.field static final TRANSACTION_QFTSession_AddListener:I = 0x7

.field static final TRANSACTION_QFTSession_Cancel:I = 0xf

.field static final TRANSACTION_QFTSession_FreeContentInfo:I = 0xc

.field static final TRANSACTION_QFTSession_FreeSessionInfo:I = 0xa

.field static final TRANSACTION_QFTSession_GetContentInfo:I = 0xb

.field static final TRANSACTION_QFTSession_GetSessionInfo:I = 0x9

.field static final TRANSACTION_QFTSession_Reject:I = 0xe

.field static final TRANSACTION_QFTSession_RemoveListener:I = 0x8


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/ft/IQFTService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.ft.IQFTService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/ft/IQFTService;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/ft/IQFTService;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/ft/IQFTService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/ft/IQFTService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 376
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 44
    :sswitch_0
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 45
    const/4 v0, 0x1

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 53
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    sget-object v0, Lcom/qualcomm/qti/rcsservice/VersionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/VersionInfo;

    .line 59
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    :goto_1
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->QFTService_GetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 60
    .local v8, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 61
    if-eqz v8, :cond_1

    .line 62
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 63
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 68
    :goto_2
    if-eqz v2, :cond_2

    .line 69
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    const/4 v0, 0x1

    invoke-virtual {v2, p3, v0}, Lcom/qualcomm/qti/rcsservice/VersionInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 75
    :goto_3
    const/4 v0, 0x1

    goto :goto_0

    .line 57
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    goto :goto_1

    .line 66
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    .line 73
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3

    .line 79
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_2
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 83
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/qualcomm/qti/ft/IQFTServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/ft/IQFTServiceListener;

    move-result-object v2

    .line 84
    .local v2, "_arg1":Lcom/qualcomm/qti/ft/IQFTServiceListener;
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->QFTService_AddListener(ILcom/qualcomm/qti/ft/IQFTServiceListener;)J

    move-result-wide v8

    .line 85
    .local v8, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 86
    invoke-virtual {p3, v8, v9}, Landroid/os/Parcel;->writeLong(J)V

    .line 87
    const/4 v0, 0x1

    goto :goto_0

    .line 91
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/ft/IQFTServiceListener;
    .end local v8    # "_result":J
    :sswitch_3
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 95
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 96
    .local v6, "_arg1":J
    invoke-virtual {p0, v1, v6, v7}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->QFTService_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 97
    .local v8, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 98
    if-eqz v8, :cond_3

    .line 99
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 100
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 105
    :goto_4
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 103
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_4

    .line 109
    .end local v1    # "_arg0":I
    .end local v6    # "_arg1":J
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_4
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 111
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 113
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/ft/IQFTSessionListener;

    move-result-object v2

    .line 115
    .local v2, "_arg1":Lcom/qualcomm/qti/ft/IQFTSessionListener;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    .line 116
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;

    .line 122
    .local v3, "_arg2":Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    .line 123
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 129
    .local v4, "_arg3":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .local v5, "_arg4":I
    move-object v0, p0

    .line 130
    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->QFTService_TransferFile(ILcom/qualcomm/qti/ft/IQFTSessionListener;Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;Lcom/qualcomm/qti/rcsservice/ContentInfo;I)I

    move-result v8

    .line 131
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 132
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 133
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 119
    .end local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;
    .end local v4    # "_arg3":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .end local v5    # "_arg4":I
    .end local v8    # "_result":I
    :cond_4
    const/4 v3, 0x0

    .restart local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;
    goto :goto_5

    .line 126
    :cond_5
    const/4 v4, 0x0

    .restart local v4    # "_arg3":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    goto :goto_6

    .line 137
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/ft/IQFTSessionListener;
    .end local v3    # "_arg2":Lcom/qualcomm/qti/rcsservice/SessionCreationInfo;
    .end local v4    # "_arg3":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :sswitch_5
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 141
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    .line 142
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionList;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/SessionList;

    .line 148
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/SessionList;
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 149
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->QFTService_GetActiveSessions(ILcom/qualcomm/qti/rcsservice/SessionList;Ljava/lang/String;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 150
    .local v8, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 151
    if-eqz v8, :cond_7

    .line 152
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 153
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 158
    :goto_8
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 145
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionList;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_6
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionList;
    goto :goto_7

    .line 156
    .restart local v3    # "_arg2":Ljava/lang/String;
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_8

    .line 162
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionList;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_6
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 164
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 166
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_8

    .line 167
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionList;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/SessionList;

    .line 172
    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionList;
    :goto_9
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->QFTService_FreeSessionList(ILcom/qualcomm/qti/rcsservice/SessionList;)V

    .line 173
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 174
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 170
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionList;
    :cond_8
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionList;
    goto :goto_9

    .line 178
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionList;
    :sswitch_7
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 180
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 182
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/qualcomm/qti/ft/IQFTSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/ft/IQFTSessionListener;

    move-result-object v2

    .line 183
    .local v2, "_arg1":Lcom/qualcomm/qti/ft/IQFTSessionListener;
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->QFTSession_AddListener(ILcom/qualcomm/qti/ft/IQFTSessionListener;)J

    move-result-wide v8

    .line 184
    .local v8, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 185
    invoke-virtual {p3, v8, v9}, Landroid/os/Parcel;->writeLong(J)V

    .line 186
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 190
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/ft/IQFTSessionListener;
    .end local v8    # "_result":J
    :sswitch_8
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 192
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 194
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 195
    .restart local v6    # "_arg1":J
    invoke-virtual {p0, v1, v6, v7}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->QFTSession_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 196
    .local v8, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 197
    if-eqz v8, :cond_9

    .line 198
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 199
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 204
    :goto_a
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 202
    :cond_9
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_a

    .line 208
    .end local v1    # "_arg0":I
    .end local v6    # "_arg1":J
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_9
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 210
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 212
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_a

    .line 213
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/SessionInfo;

    .line 218
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    :goto_b
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->QFTSession_GetSessionInfo(ILcom/qualcomm/qti/rcsservice/SessionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 219
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 220
    if-eqz v8, :cond_b

    .line 221
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 222
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 227
    :goto_c
    if-eqz v2, :cond_c

    .line 228
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 229
    const/4 v0, 0x1

    invoke-virtual {v2, p3, v0}, Lcom/qualcomm/qti/rcsservice/SessionInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 234
    :goto_d
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 216
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_a
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    goto :goto_b

    .line 225
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_b
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_c

    .line 232
    :cond_c
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_d

    .line 238
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_a
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 240
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 242
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_d

    .line 243
    sget-object v0, Lcom/qualcomm/qti/rcsservice/SessionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/SessionInfo;

    .line 248
    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    :goto_e
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->QFTSession_FreeSessionInfo(ILcom/qualcomm/qti/rcsservice/SessionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 249
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 250
    if-eqz v8, :cond_e

    .line 251
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 252
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 257
    :goto_f
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 246
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_d
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    goto :goto_e

    .line 255
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_e
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_f

    .line 261
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/SessionInfo;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_b
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 263
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 265
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_f

    .line 266
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 271
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :goto_10
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->QFTSession_GetContentInfo(ILcom/qualcomm/qti/rcsservice/ContentInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 272
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 273
    if-eqz v8, :cond_10

    .line 274
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 275
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 280
    :goto_11
    if-eqz v2, :cond_11

    .line 281
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 282
    const/4 v0, 0x1

    invoke-virtual {v2, p3, v0}, Lcom/qualcomm/qti/rcsservice/ContentInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 287
    :goto_12
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 269
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_f
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    goto :goto_10

    .line 278
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_10
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_11

    .line 285
    :cond_11
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_12

    .line 291
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_c
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 293
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 295
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_12

    .line 296
    sget-object v0, Lcom/qualcomm/qti/rcsservice/ContentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 301
    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :goto_13
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->QFTSession_FreeContentInfo(ILcom/qualcomm/qti/rcsservice/ContentInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 302
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 303
    if-eqz v8, :cond_13

    .line 304
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 305
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 310
    :goto_14
    if-eqz v2, :cond_14

    .line 311
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 312
    const/4 v0, 0x1

    invoke-virtual {v2, p3, v0}, Lcom/qualcomm/qti/rcsservice/ContentInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 317
    :goto_15
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 299
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_12
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    goto :goto_13

    .line 308
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_13
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_14

    .line 315
    :cond_14
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_15

    .line 321
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_d
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 323
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 325
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 327
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 328
    .local v3, "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->QFTSession_Accept(ILjava/lang/String;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 329
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 330
    if-eqz v8, :cond_15

    .line 331
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 332
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 337
    :goto_16
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 335
    :cond_15
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_16

    .line 341
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_e
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 343
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 345
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 346
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->QFTSession_Reject(II)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 347
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 348
    if-eqz v8, :cond_16

    .line 349
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 350
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 355
    :goto_17
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 353
    :cond_16
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_17

    .line 359
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_f
    const-string v0, "com.qualcomm.qti.ft.IQFTService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 361
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 363
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 364
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/qualcomm/qti/ft/IQFTService$Stub;->QFTSession_Cancel(II)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v8

    .line 365
    .restart local v8    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 366
    if-eqz v8, :cond_17

    .line 367
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 368
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 373
    :goto_18
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 371
    :cond_17
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_18

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
