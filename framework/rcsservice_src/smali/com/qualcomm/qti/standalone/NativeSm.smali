.class public Lcom/qualcomm/qti/standalone/NativeSm;
.super Ljava/lang/Object;
.source "NativeSm.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native QSMServiceAcceptInvitation(III)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QSMServiceAddListener(ILcom/qualcomm/qti/standalone/IQSMServiceListener;)J
.end method

.method public native QSMServiceCancelInvitation(III)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QSMServiceGetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QSMServiceRejectInvitation(III)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QSMServiceRemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QSMServiceSendDisplayNotification(ILcom/qualcomm/qti/standalone/NotificationMessageInfo;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QSMServiceSendStandaloneMessage(ILcom/qualcomm/qti/standalone/MessageInfo;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method
