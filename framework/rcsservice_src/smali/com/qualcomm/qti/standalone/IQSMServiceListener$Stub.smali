.class public abstract Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;
.super Landroid/os/Binder;
.source "IQSMServiceListener.java"

# interfaces
.implements Lcom/qualcomm/qti/standalone/IQSMServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/standalone/IQSMServiceListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.standalone.IQSMServiceListener"

.field static final TRANSACTION_QSMServiceListener_HandleIncomingInvitation:I = 0x9

.field static final TRANSACTION_QSMServiceListener_HandleIncomingMessage:I = 0x4

.field static final TRANSACTION_QSMServiceListener_HandleInvitationCanceled:I = 0x8

.field static final TRANSACTION_QSMServiceListener_HandleMessageDeliveryStatus:I = 0x7

.field static final TRANSACTION_QSMServiceListener_HandleReceiveSuccess:I = 0xd

.field static final TRANSACTION_QSMServiceListener_HandleServiceReqStatus:I = 0xe

.field static final TRANSACTION_QSMServiceListener_HandleSmError:I = 0x6

.field static final TRANSACTION_QSMServiceListener_HandleSmSendSuccess:I = 0x5

.field static final TRANSACTION_QSMServiceListener_HandleTransferProgress:I = 0xc

.field static final TRANSACTION_QSMServiceListener_HandleTransferStarted:I = 0xa

.field static final TRANSACTION_QSMServiceListener_HandleTransferTerminatedByRemote:I = 0xb

.field static final TRANSACTION_QSMServiceListener_ServiceAvailable:I = 0x1

.field static final TRANSACTION_QSMServiceListener_ServiceCreated:I = 0x3

.field static final TRANSACTION_QSMServiceListener_ServiceUnavailable:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/standalone/IQSMServiceListener;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/standalone/IQSMServiceListener;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/standalone/IQSMServiceListener;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 7
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 244
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 44
    :sswitch_0
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    sget-object v1, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 57
    .local v2, "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_1
    invoke-virtual {p0, v2}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->QSMServiceListener_ServiceAvailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V

    .line 58
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 55
    .end local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_1

    .line 63
    .end local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_2
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    sget-object v1, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 71
    .restart local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_2
    invoke-virtual {p0, v2}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->QSMServiceListener_ServiceUnavailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V

    .line 72
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 69
    .end local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_2

    .line 77
    .end local v2    # "_arg0":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_3
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 81
    .local v2, "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 83
    .local v4, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_2

    .line 84
    sget-object v1, Lcom/qualcomm/qti/rcsservice/StatusCode;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 90
    .local v5, "_arg2":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .local v6, "_arg3":I
    move-object v1, p0

    .line 91
    invoke-virtual/range {v1 .. v6}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->QSMServiceListener_ServiceCreated(JLjava/lang/String;Lcom/qualcomm/qti/rcsservice/StatusCode;I)V

    .line 92
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 87
    .end local v5    # "_arg2":Lcom/qualcomm/qti/rcsservice/StatusCode;
    .end local v6    # "_arg3":I
    :cond_2
    const/4 v5, 0x0

    .restart local v5    # "_arg2":Lcom/qualcomm/qti/rcsservice/StatusCode;
    goto :goto_3

    .line 97
    .end local v2    # "_arg0":J
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_4
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 99
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_3

    .line 100
    sget-object v1, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;

    .line 105
    .local v2, "_arg0":Lcom/qualcomm/qti/standalone/IncomingMsgInfo;
    :goto_4
    invoke-virtual {p0, v2}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->QSMServiceListener_HandleIncomingMessage(Lcom/qualcomm/qti/standalone/IncomingMsgInfo;)V

    .line 106
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 103
    .end local v2    # "_arg0":Lcom/qualcomm/qti/standalone/IncomingMsgInfo;
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Lcom/qualcomm/qti/standalone/IncomingMsgInfo;
    goto :goto_4

    .line 111
    .end local v2    # "_arg0":Lcom/qualcomm/qti/standalone/IncomingMsgInfo;
    :sswitch_5
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 115
    .local v2, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_4

    .line 116
    sget-object v1, Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    .line 121
    .local v4, "_arg1":Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;
    :goto_5
    invoke-virtual {p0, v2, v4}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->QSMServiceListener_HandleSmSendSuccess(ILcom/qualcomm/qti/rcsservice/ImdnReqInfo;)V

    .line 122
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 119
    .end local v4    # "_arg1":Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;
    :cond_4
    const/4 v4, 0x0

    .restart local v4    # "_arg1":Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;
    goto :goto_5

    .line 127
    .end local v2    # "_arg0":I
    .end local v4    # "_arg1":Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;
    :sswitch_6
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 129
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 131
    .restart local v2    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_5

    .line 132
    sget-object v1, Lcom/qualcomm/qti/rcsservice/Error;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/qualcomm/qti/rcsservice/Error;

    .line 138
    .local v4, "_arg1":Lcom/qualcomm/qti/rcsservice/Error;
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 139
    .local v5, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v2, v4, v5}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->QSMServiceListener_HandleSmError(ILcom/qualcomm/qti/rcsservice/Error;Ljava/lang/String;)V

    .line 140
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 135
    .end local v4    # "_arg1":Lcom/qualcomm/qti/rcsservice/Error;
    .end local v5    # "_arg2":Ljava/lang/String;
    :cond_5
    const/4 v4, 0x0

    .restart local v4    # "_arg1":Lcom/qualcomm/qti/rcsservice/Error;
    goto :goto_6

    .line 145
    .end local v2    # "_arg0":I
    .end local v4    # "_arg1":Lcom/qualcomm/qti/rcsservice/Error;
    :sswitch_7
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 147
    sget-object v1, Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;

    .line 148
    .local v2, "_arg0":[Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;
    invoke-virtual {p0, v2}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->QSMServiceListener_HandleMessageDeliveryStatus([Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;)V

    .line 149
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 154
    .end local v2    # "_arg0":[Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;
    :sswitch_8
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 156
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 157
    .local v2, "_arg0":I
    invoke-virtual {p0, v2}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->QSMServiceListener_HandleInvitationCanceled(I)V

    .line 158
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 163
    .end local v2    # "_arg0":I
    :sswitch_9
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 165
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 167
    .restart local v2    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_6

    .line 168
    sget-object v1, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;

    .line 173
    .local v4, "_arg1":Lcom/qualcomm/qti/standalone/IncomingMsgInfo;
    :goto_7
    invoke-virtual {p0, v2, v4}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->QSMServiceListener_HandleIncomingInvitation(ILcom/qualcomm/qti/standalone/IncomingMsgInfo;)V

    .line 174
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 171
    .end local v4    # "_arg1":Lcom/qualcomm/qti/standalone/IncomingMsgInfo;
    :cond_6
    const/4 v4, 0x0

    .restart local v4    # "_arg1":Lcom/qualcomm/qti/standalone/IncomingMsgInfo;
    goto :goto_7

    .line 179
    .end local v2    # "_arg0":I
    .end local v4    # "_arg1":Lcom/qualcomm/qti/standalone/IncomingMsgInfo;
    :sswitch_a
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 181
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 182
    .restart local v2    # "_arg0":I
    invoke-virtual {p0, v2}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->QSMServiceListener_HandleTransferStarted(I)V

    .line 183
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 188
    .end local v2    # "_arg0":I
    :sswitch_b
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 190
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 191
    .restart local v2    # "_arg0":I
    invoke-virtual {p0, v2}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->QSMServiceListener_HandleTransferTerminatedByRemote(I)V

    .line 192
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 197
    .end local v2    # "_arg0":I
    :sswitch_c
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 199
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 201
    .restart local v2    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_7

    .line 202
    sget-object v1, Lcom/qualcomm/qti/rcsservice/ContentInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 207
    .local v4, "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :goto_8
    invoke-virtual {p0, v2, v4}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->QSMServiceListener_HandleTransferProgress(ILcom/qualcomm/qti/rcsservice/ContentInfo;)V

    .line 208
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 205
    .end local v4    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :cond_7
    const/4 v4, 0x0

    .restart local v4    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    goto :goto_8

    .line 213
    .end local v2    # "_arg0":I
    .end local v4    # "_arg1":Lcom/qualcomm/qti/rcsservice/ContentInfo;
    :sswitch_d
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 215
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 217
    .restart local v2    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_8

    .line 218
    sget-object v1, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;

    .line 223
    .local v4, "_arg1":Lcom/qualcomm/qti/standalone/IncomingMsgInfo;
    :goto_9
    invoke-virtual {p0, v2, v4}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->QSMServiceListener_HandleReceiveSuccess(ILcom/qualcomm/qti/standalone/IncomingMsgInfo;)V

    .line 224
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 221
    .end local v4    # "_arg1":Lcom/qualcomm/qti/standalone/IncomingMsgInfo;
    :cond_8
    const/4 v4, 0x0

    .restart local v4    # "_arg1":Lcom/qualcomm/qti/standalone/IncomingMsgInfo;
    goto :goto_9

    .line 229
    .end local v2    # "_arg0":I
    .end local v4    # "_arg1":Lcom/qualcomm/qti/standalone/IncomingMsgInfo;
    :sswitch_e
    const-string v1, "com.qualcomm.qti.standalone.IQSMServiceListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 231
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 233
    .restart local v2    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_9

    .line 234
    sget-object v1, Lcom/qualcomm/qti/rcsservice/ReqStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/qualcomm/qti/rcsservice/ReqStatus;

    .line 239
    .local v4, "_arg1":Lcom/qualcomm/qti/rcsservice/ReqStatus;
    :goto_a
    invoke-virtual {p0, v2, v4}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->QSMServiceListener_HandleServiceReqStatus(ILcom/qualcomm/qti/rcsservice/ReqStatus;)V

    .line 240
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 237
    .end local v4    # "_arg1":Lcom/qualcomm/qti/rcsservice/ReqStatus;
    :cond_9
    const/4 v4, 0x0

    .restart local v4    # "_arg1":Lcom/qualcomm/qti/rcsservice/ReqStatus;
    goto :goto_a

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
