.class Lcom/qualcomm/qti/standalone/SMListener$3;
.super Ljava/lang/Thread;
.source "SMListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/qti/standalone/SMListener;->QSMServiceListener_ServiceCreated(JLjava/lang/String;Lcom/qualcomm/qti/rcsservice/StatusCode;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/standalone/SMListener;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/standalone/SMListener;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/SMListener$3;->this$0:Lcom/qualcomm/qti/standalone/SMListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 101
    :try_start_0
    const-string v1, "AIDL"

    const-string v2, " QSMServiceListener_ServiceCreated : start inside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    iget-object v1, p0, Lcom/qualcomm/qti/standalone/SMListener$3;->this$0:Lcom/qualcomm/qti/standalone/SMListener;

    # getter for: Lcom/qualcomm/qti/standalone/SMListener;->m_QSMServiceListener:Lcom/qualcomm/qti/standalone/IQSMServiceListener;
    invoke-static {v1}, Lcom/qualcomm/qti/standalone/SMListener;->access$100(Lcom/qualcomm/qti/standalone/SMListener;)Lcom/qualcomm/qti/standalone/IQSMServiceListener;

    move-result-object v1

    iget-object v2, p0, Lcom/qualcomm/qti/standalone/SMListener$3;->this$0:Lcom/qualcomm/qti/standalone/SMListener;

    # getter for: Lcom/qualcomm/qti/standalone/SMListener;->m_smServiceListenerUserData:J
    invoke-static {v2}, Lcom/qualcomm/qti/standalone/SMListener;->access$200(Lcom/qualcomm/qti/standalone/SMListener;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/qualcomm/qti/standalone/SMListener$3;->this$0:Lcom/qualcomm/qti/standalone/SMListener;

    # getter for: Lcom/qualcomm/qti/standalone/SMListener;->m_featureTag:Ljava/lang/String;
    invoke-static {v4}, Lcom/qualcomm/qti/standalone/SMListener;->access$300(Lcom/qualcomm/qti/standalone/SMListener;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/qualcomm/qti/standalone/SMListener$3;->this$0:Lcom/qualcomm/qti/standalone/SMListener;

    # getter for: Lcom/qualcomm/qti/standalone/SMListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-static {v5}, Lcom/qualcomm/qti/standalone/SMListener;->access$000(Lcom/qualcomm/qti/standalone/SMListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v5

    iget-object v6, p0, Lcom/qualcomm/qti/standalone/SMListener$3;->this$0:Lcom/qualcomm/qti/standalone/SMListener;

    # getter for: Lcom/qualcomm/qti/standalone/SMListener;->m_smServiceHandle:I
    invoke-static {v6}, Lcom/qualcomm/qti/standalone/SMListener;->access$400(Lcom/qualcomm/qti/standalone/SMListener;)I

    move-result v6

    invoke-interface/range {v1 .. v6}, Lcom/qualcomm/qti/standalone/IQSMServiceListener;->QSMServiceListener_ServiceCreated(JLjava/lang/String;Lcom/qualcomm/qti/rcsservice/StatusCode;I)V

    .line 105
    const-string v1, "AIDL"

    const-string v2, " QSMServiceListener_ServiceCreated : end inside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    :goto_0
    return-void

    .line 106
    :catch_0
    move-exception v0

    .line 107
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
