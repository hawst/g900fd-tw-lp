.class public Lcom/qualcomm/qti/standalone/QSMService;
.super Lcom/qualcomm/qti/standalone/IQSMService$Stub;
.source "QSMService.java"


# instance fields
.field private objNativeSM:Lcom/qualcomm/qti/standalone/NativeSm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/qualcomm/qti/standalone/IQSMService$Stub;-><init>()V

    .line 16
    new-instance v0, Lcom/qualcomm/qti/standalone/NativeSm;

    invoke-direct {v0}, Lcom/qualcomm/qti/standalone/NativeSm;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/QSMService;->objNativeSM:Lcom/qualcomm/qti/standalone/NativeSm;

    return-void
.end method


# virtual methods
.method public QSMService_AcceptInvitation(III)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "smServiceHandle"    # I
    .param p2, "tid"    # I
    .param p3, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/QSMService;->objNativeSM:Lcom/qualcomm/qti/standalone/NativeSm;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/standalone/NativeSm;->QSMServiceAcceptInvitation(III)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QSMService_AddListener(ILcom/qualcomm/qti/standalone/IQSMServiceListener;)J
    .locals 2
    .param p1, "smServiceHandle"    # I
    .param p2, "smServiceListener"    # Lcom/qualcomm/qti/standalone/IQSMServiceListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/QSMService;->objNativeSM:Lcom/qualcomm/qti/standalone/NativeSm;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/standalone/NativeSm;->QSMServiceAddListener(ILcom/qualcomm/qti/standalone/IQSMServiceListener;)J

    move-result-wide v0

    return-wide v0
.end method

.method public QSMService_CancelInvitation(III)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "smServiceHandle"    # I
    .param p2, "tid"    # I
    .param p3, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/QSMService;->objNativeSM:Lcom/qualcomm/qti/standalone/NativeSm;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/standalone/NativeSm;->QSMServiceCancelInvitation(III)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QSMService_GetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "smServiceHandle"    # I
    .param p2, "versionInfo"    # Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/QSMService;->objNativeSM:Lcom/qualcomm/qti/standalone/NativeSm;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/standalone/NativeSm;->QSMServiceGetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QSMService_RejectInvitation(III)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "smServiceHandle"    # I
    .param p2, "tid"    # I
    .param p3, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/QSMService;->objNativeSM:Lcom/qualcomm/qti/standalone/NativeSm;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/standalone/NativeSm;->QSMServiceRejectInvitation(III)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QSMService_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 2
    .param p1, "smServiceHandle"    # I
    .param p2, "smServiceUserData"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/QSMService;->objNativeSM:Lcom/qualcomm/qti/standalone/NativeSm;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/standalone/NativeSm;->QSMServiceRemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QSMService_SendDisplayNotification(ILcom/qualcomm/qti/standalone/NotificationMessageInfo;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "smServiceHandle"    # I
    .param p2, "messageInfo"    # Lcom/qualcomm/qti/standalone/NotificationMessageInfo;
    .param p3, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/QSMService;->objNativeSM:Lcom/qualcomm/qti/standalone/NativeSm;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/standalone/NativeSm;->QSMServiceSendDisplayNotification(ILcom/qualcomm/qti/standalone/NotificationMessageInfo;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QSMService_SendStandaloneMessage(ILcom/qualcomm/qti/standalone/MessageInfo;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "smServiceHandle"    # I
    .param p2, "messageInfo"    # Lcom/qualcomm/qti/standalone/MessageInfo;
    .param p3, "reqUserData"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/QSMService;->objNativeSM:Lcom/qualcomm/qti/standalone/NativeSm;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/standalone/NativeSm;->QSMServiceSendStandaloneMessage(ILcom/qualcomm/qti/standalone/MessageInfo;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public getSmService()Lcom/qualcomm/qti/standalone/IQSMService$Stub;
    .locals 0

    .prologue
    .line 19
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 27
    :try_start_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/qualcomm/qti/standalone/IQSMService$Stub;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 28
    :catch_0
    move-exception v0

    .line 30
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v1, "AIDL_QSMService"

    const-string v2, "Unexpected remote exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 31
    throw v0
.end method
