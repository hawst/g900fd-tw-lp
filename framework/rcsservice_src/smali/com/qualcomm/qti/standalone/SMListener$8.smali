.class Lcom/qualcomm/qti/standalone/SMListener$8;
.super Ljava/lang/Thread;
.source "SMListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/qti/standalone/SMListener;->QSMServiceListener_HandleInvitationCanceled(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/standalone/SMListener;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/standalone/SMListener;)V
    .locals 0

    .prologue
    .line 218
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/SMListener$8;->this$0:Lcom/qualcomm/qti/standalone/SMListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 223
    :try_start_0
    const-string v1, "AIDL"

    const-string v2, " QSMServiceListener_HandleInvitationCanceled : start inside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    iget-object v1, p0, Lcom/qualcomm/qti/standalone/SMListener$8;->this$0:Lcom/qualcomm/qti/standalone/SMListener;

    # getter for: Lcom/qualcomm/qti/standalone/SMListener;->m_QSMServiceListener:Lcom/qualcomm/qti/standalone/IQSMServiceListener;
    invoke-static {v1}, Lcom/qualcomm/qti/standalone/SMListener;->access$100(Lcom/qualcomm/qti/standalone/SMListener;)Lcom/qualcomm/qti/standalone/IQSMServiceListener;

    move-result-object v1

    iget-object v2, p0, Lcom/qualcomm/qti/standalone/SMListener$8;->this$0:Lcom/qualcomm/qti/standalone/SMListener;

    # getter for: Lcom/qualcomm/qti/standalone/SMListener;->m_tid:I
    invoke-static {v2}, Lcom/qualcomm/qti/standalone/SMListener;->access$600(Lcom/qualcomm/qti/standalone/SMListener;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/qualcomm/qti/standalone/IQSMServiceListener;->QSMServiceListener_HandleInvitationCanceled(I)V

    .line 226
    const-string v1, "AIDL"

    const-string v2, " QSMServiceListener_HandleInvitationCanceled : end inside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    :goto_0
    return-void

    .line 228
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
