.class public interface abstract Lcom/qualcomm/qti/standalone/IQSMServiceListener;
.super Ljava/lang/Object;
.source "IQSMServiceListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract QSMServiceListener_HandleIncomingInvitation(ILcom/qualcomm/qti/standalone/IncomingMsgInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMServiceListener_HandleIncomingMessage(Lcom/qualcomm/qti/standalone/IncomingMsgInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMServiceListener_HandleInvitationCanceled(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMServiceListener_HandleMessageDeliveryStatus([Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMServiceListener_HandleReceiveSuccess(ILcom/qualcomm/qti/standalone/IncomingMsgInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMServiceListener_HandleServiceReqStatus(ILcom/qualcomm/qti/rcsservice/ReqStatus;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMServiceListener_HandleSmError(ILcom/qualcomm/qti/rcsservice/Error;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMServiceListener_HandleSmSendSuccess(ILcom/qualcomm/qti/rcsservice/ImdnReqInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMServiceListener_HandleTransferProgress(ILcom/qualcomm/qti/rcsservice/ContentInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMServiceListener_HandleTransferStarted(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMServiceListener_HandleTransferTerminatedByRemote(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMServiceListener_ServiceAvailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMServiceListener_ServiceCreated(JLjava/lang/String;Lcom/qualcomm/qti/rcsservice/StatusCode;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMServiceListener_ServiceUnavailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
