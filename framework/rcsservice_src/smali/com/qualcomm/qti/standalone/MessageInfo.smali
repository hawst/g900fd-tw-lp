.class public Lcom/qualcomm/qti/standalone/MessageInfo;
.super Ljava/lang/Object;
.source "MessageInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/standalone/MessageInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

.field private dnReq:Lcom/qualcomm/qti/rcsservice/DNReq;

.field private expiresValue:I

.field private idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

.field private listName:Ljava/lang/String;

.field private recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

.field private replyTo:Ljava/lang/String;

.field private reqURI:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/qualcomm/qti/standalone/MessageInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/standalone/MessageInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/standalone/MessageInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->reqURI:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->replyTo:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->listName:Ljava/lang/String;

    .line 40
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->reqURI:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->replyTo:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->listName:Ljava/lang/String;

    .line 78
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/standalone/MessageInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 79
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/standalone/MessageInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/standalone/MessageInfo$1;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/standalone/MessageInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public getContentInfo()Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    return-object v0
.end method

.method public getDNReq()Lcom/qualcomm/qti/rcsservice/DNReq;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->dnReq:Lcom/qualcomm/qti/rcsservice/DNReq;

    return-object v0
.end method

.method public getExpiresValue()I
    .locals 1

    .prologue
    .line 233
    iget v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->expiresValue:I

    return v0
.end method

.method public getIdHeaderInfo()Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    return-object v0
.end method

.method public getListName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->listName:Ljava/lang/String;

    return-object v0
.end method

.method public getRecipientInfoArray()[Lcom/qualcomm/qti/rcsservice/RecipientInfo;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    return-object v0
.end method

.method public getReplyTo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->replyTo:Ljava/lang/String;

    return-object v0
.end method

.method public getReqURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->reqURI:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->reqURI:Ljava/lang/String;

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->replyTo:Ljava/lang/String;

    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 85
    .local v0, "length":I
    if-lez v0, :cond_0

    .line 87
    new-array v1, v0, [Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    iput-object v1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    .line 88
    iget-object v1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    sget-object v2, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    .line 90
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->listName:Ljava/lang/String;

    .line 91
    const-class v1, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    iput-object v1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .line 92
    const-class v1, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    iput-object v1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 93
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->expiresValue:I

    .line 94
    const-class v1, Lcom/qualcomm/qti/rcsservice/DNReq;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/DNReq;

    iput-object v1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->dnReq:Lcom/qualcomm/qti/rcsservice/DNReq;

    .line 96
    return-void
.end method

.method public setContentInfo(Lcom/qualcomm/qti/rcsservice/ContentInfo;)V
    .locals 0
    .param p1, "contentInfo"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .prologue
    .line 223
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 224
    return-void
.end method

.method public setDNReq(Lcom/qualcomm/qti/rcsservice/DNReq;)V
    .locals 0
    .param p1, "dnReq"    # Lcom/qualcomm/qti/rcsservice/DNReq;

    .prologue
    .line 265
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->dnReq:Lcom/qualcomm/qti/rcsservice/DNReq;

    .line 266
    return-void
.end method

.method public setExpiresValue(I)V
    .locals 0
    .param p1, "expiresValue"    # I

    .prologue
    .line 244
    iput p1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->expiresValue:I

    .line 245
    return-void
.end method

.method public setIdHeaderInfo(Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;)V
    .locals 0
    .param p1, "idHeaderInfo"    # Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .line 203
    return-void
.end method

.method public setListName(Ljava/lang/String;)V
    .locals 0
    .param p1, "listName"    # Ljava/lang/String;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->listName:Ljava/lang/String;

    .line 182
    return-void
.end method

.method public setRecipientInfoArray([Lcom/qualcomm/qti/rcsservice/RecipientInfo;)V
    .locals 0
    .param p1, "recipientInfoArray"    # [Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    .prologue
    .line 160
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    .line 161
    return-void
.end method

.method public setReplyTo(Ljava/lang/String;)V
    .locals 0
    .param p1, "replyTo"    # Ljava/lang/String;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->replyTo:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public setReqURI(Ljava/lang/String;)V
    .locals 0
    .param p1, "reqURI"    # Ljava/lang/String;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->reqURI:Ljava/lang/String;

    .line 119
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x0

    .line 48
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->reqURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->replyTo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 53
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 57
    :goto_0
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->listName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 59
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 60
    iget v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->expiresValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 61
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/MessageInfo;->dnReq:Lcom/qualcomm/qti/rcsservice/DNReq;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 63
    return-void

    .line 55
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
