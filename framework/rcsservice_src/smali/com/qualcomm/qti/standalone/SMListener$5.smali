.class Lcom/qualcomm/qti/standalone/SMListener$5;
.super Ljava/lang/Thread;
.source "SMListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/qti/standalone/SMListener;->QSMServiceListener_HandleSmSendSuccess(ILcom/qualcomm/qti/rcsservice/ImdnReqInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/standalone/SMListener;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/standalone/SMListener;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/SMListener$5;->this$0:Lcom/qualcomm/qti/standalone/SMListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 149
    :try_start_0
    const-string v1, "AIDL"

    const-string v2, " QSMServiceListener_HandleSmSendSuccess : start inside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v1, p0, Lcom/qualcomm/qti/standalone/SMListener$5;->this$0:Lcom/qualcomm/qti/standalone/SMListener;

    # getter for: Lcom/qualcomm/qti/standalone/SMListener;->m_QSMServiceListener:Lcom/qualcomm/qti/standalone/IQSMServiceListener;
    invoke-static {v1}, Lcom/qualcomm/qti/standalone/SMListener;->access$100(Lcom/qualcomm/qti/standalone/SMListener;)Lcom/qualcomm/qti/standalone/IQSMServiceListener;

    move-result-object v1

    iget-object v2, p0, Lcom/qualcomm/qti/standalone/SMListener$5;->this$0:Lcom/qualcomm/qti/standalone/SMListener;

    # getter for: Lcom/qualcomm/qti/standalone/SMListener;->m_tid:I
    invoke-static {v2}, Lcom/qualcomm/qti/standalone/SMListener;->access$600(Lcom/qualcomm/qti/standalone/SMListener;)I

    move-result v2

    iget-object v3, p0, Lcom/qualcomm/qti/standalone/SMListener$5;->this$0:Lcom/qualcomm/qti/standalone/SMListener;

    # getter for: Lcom/qualcomm/qti/standalone/SMListener;->m_imdnReqInfo:Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;
    invoke-static {v3}, Lcom/qualcomm/qti/standalone/SMListener;->access$700(Lcom/qualcomm/qti/standalone/SMListener;)Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/qualcomm/qti/standalone/IQSMServiceListener;->QSMServiceListener_HandleSmSendSuccess(ILcom/qualcomm/qti/rcsservice/ImdnReqInfo;)V

    .line 152
    const-string v1, "AIDL"

    const-string v2, " QSMServiceListener_HandleSmSendSuccess : end inside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :goto_0
    return-void

    .line 153
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
