.class public Lcom/qualcomm/qti/standalone/NotificationMessageInfo;
.super Ljava/lang/Object;
.source "NotificationMessageInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/standalone/NotificationMessageInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private dateTime:Ljava/lang/String;

.field private expiresValue:I

.field private idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

.field private imdnRecordRoute:Ljava/lang/String;

.field private msgID:Ljava/lang/String;

.field private originalTo:Ljava/lang/String;

.field private remoteContact:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/standalone/NotificationMessageInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->remoteContact:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->msgID:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->dateTime:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->imdnRecordRoute:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->originalTo:Ljava/lang/String;

    .line 35
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->remoteContact:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->msgID:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->dateTime:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->imdnRecordRoute:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->originalTo:Ljava/lang/String;

    .line 66
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 67
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/standalone/NotificationMessageInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/standalone/NotificationMessageInfo$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method

.method public getDateTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->dateTime:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiresValue()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->expiresValue:I

    return v0
.end method

.method public getIdHeaderInfo()Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    return-object v0
.end method

.method public getImdnRecordRoute()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->imdnRecordRoute:Ljava/lang/String;

    return-object v0
.end method

.method public getOriginalTo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->originalTo:Ljava/lang/String;

    return-object v0
.end method

.method public getRemoteContact()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->remoteContact:Ljava/lang/String;

    return-object v0
.end method

.method public getmsgID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->msgID:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->remoteContact:Ljava/lang/String;

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->msgID:Ljava/lang/String;

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->dateTime:Ljava/lang/String;

    .line 73
    const-class v0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->expiresValue:I

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->imdnRecordRoute:Ljava/lang/String;

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->originalTo:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setDateTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "dateTime"    # Ljava/lang/String;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->dateTime:Ljava/lang/String;

    .line 143
    return-void
.end method

.method public setExpiresValue(I)V
    .locals 0
    .param p1, "expiresValue"    # I

    .prologue
    .line 184
    iput p1, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->expiresValue:I

    .line 185
    return-void
.end method

.method public setIdHeaderInfo(Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;)V
    .locals 0
    .param p1, "idHeaderInfo"    # Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .line 164
    return-void
.end method

.method public setImdnRecordRoute(Ljava/lang/String;)V
    .locals 0
    .param p1, "imdnRecordRoute"    # Ljava/lang/String;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->imdnRecordRoute:Ljava/lang/String;

    .line 206
    return-void
.end method

.method public setMsgID(Ljava/lang/String;)V
    .locals 0
    .param p1, "msgID"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->msgID:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public setOriginalTo(Ljava/lang/String;)V
    .locals 0
    .param p1, "originalTo"    # Ljava/lang/String;

    .prologue
    .line 226
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->originalTo:Ljava/lang/String;

    .line 227
    return-void
.end method

.method public setRemoteContact(Ljava/lang/String;)V
    .locals 0
    .param p1, "remoteContact"    # Ljava/lang/String;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->remoteContact:Ljava/lang/String;

    .line 101
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 43
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->remoteContact:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->msgID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->dateTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 47
    iget v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->expiresValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 48
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->imdnRecordRoute:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 49
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->originalTo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 51
    return-void
.end method
