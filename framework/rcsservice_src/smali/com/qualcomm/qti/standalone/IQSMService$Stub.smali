.class public abstract Lcom/qualcomm/qti/standalone/IQSMService$Stub;
.super Landroid/os/Binder;
.source "IQSMService.java"

# interfaces
.implements Lcom/qualcomm/qti/standalone/IQSMService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/standalone/IQSMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/standalone/IQSMService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.standalone.IQSMService"

.field static final TRANSACTION_QSMService_AcceptInvitation:I = 0x6

.field static final TRANSACTION_QSMService_AddListener:I = 0x2

.field static final TRANSACTION_QSMService_CancelInvitation:I = 0x8

.field static final TRANSACTION_QSMService_GetVersion:I = 0x1

.field static final TRANSACTION_QSMService_RejectInvitation:I = 0x7

.field static final TRANSACTION_QSMService_RemoveListener:I = 0x3

.field static final TRANSACTION_QSMService_SendDisplayNotification:I = 0x5

.field static final TRANSACTION_QSMService_SendStandaloneMessage:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.standalone.IQSMService"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/standalone/IQSMService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/standalone/IQSMService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.standalone.IQSMService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/standalone/IQSMService;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/standalone/IQSMService;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/standalone/IQSMService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/IQSMService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 218
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 44
    :sswitch_0
    const-string v7, "com.qualcomm.qti.standalone.IQSMService"

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v7, "com.qualcomm.qti.standalone.IQSMService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 53
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_0

    .line 54
    sget-object v7, Lcom/qualcomm/qti/rcsservice/VersionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/VersionInfo;

    .line 59
    .local v2, "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    :goto_1
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/qti/standalone/IQSMService$Stub;->QSMService_GetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 60
    .local v4, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 61
    if-eqz v4, :cond_1

    .line 62
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 63
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 68
    :goto_2
    if-eqz v2, :cond_2

    .line 69
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    invoke-virtual {v2, p3, v6}, Lcom/qualcomm/qti/rcsservice/VersionInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 57
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    goto :goto_1

    .line 66
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_1
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    .line 73
    :cond_2
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 79
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/rcsservice/VersionInfo;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_2
    const-string v7, "com.qualcomm.qti.standalone.IQSMService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 83
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Lcom/qualcomm/qti/standalone/IQSMServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/standalone/IQSMServiceListener;

    move-result-object v2

    .line 84
    .local v2, "_arg1":Lcom/qualcomm/qti/standalone/IQSMServiceListener;
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/qti/standalone/IQSMService$Stub;->QSMService_AddListener(ILcom/qualcomm/qti/standalone/IQSMServiceListener;)J

    move-result-wide v4

    .line 85
    .local v4, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 86
    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    goto :goto_0

    .line 91
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/standalone/IQSMServiceListener;
    .end local v4    # "_result":J
    :sswitch_3
    const-string v7, "com.qualcomm.qti.standalone.IQSMService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 95
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 96
    .local v2, "_arg1":J
    invoke-virtual {p0, v0, v2, v3}, Lcom/qualcomm/qti/standalone/IQSMService$Stub;->QSMService_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 97
    .local v4, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 98
    if-eqz v4, :cond_3

    .line 99
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 100
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 103
    :cond_3
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 109
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":J
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_4
    const-string v7, "com.qualcomm.qti.standalone.IQSMService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 111
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 113
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_4

    .line 114
    sget-object v7, Lcom/qualcomm/qti/standalone/MessageInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/standalone/MessageInfo;

    .line 120
    .local v2, "_arg1":Lcom/qualcomm/qti/standalone/MessageInfo;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 121
    .local v1, "_arg2":I
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/standalone/IQSMService$Stub;->QSMService_SendStandaloneMessage(ILcom/qualcomm/qti/standalone/MessageInfo;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 122
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 123
    if-eqz v4, :cond_5

    .line 124
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 125
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 117
    .end local v1    # "_arg2":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/standalone/MessageInfo;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/standalone/MessageInfo;
    goto :goto_3

    .line 128
    .restart local v1    # "_arg2":I
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_5
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 134
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/standalone/MessageInfo;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_5
    const-string v7, "com.qualcomm.qti.standalone.IQSMService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 136
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 138
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_6

    .line 139
    sget-object v7, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/standalone/NotificationMessageInfo;

    .line 145
    .local v2, "_arg1":Lcom/qualcomm/qti/standalone/NotificationMessageInfo;
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 146
    .restart local v1    # "_arg2":I
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/standalone/IQSMService$Stub;->QSMService_SendDisplayNotification(ILcom/qualcomm/qti/standalone/NotificationMessageInfo;I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 147
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 148
    if-eqz v4, :cond_7

    .line 149
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 150
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 142
    .end local v1    # "_arg2":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/standalone/NotificationMessageInfo;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_6
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/qti/standalone/NotificationMessageInfo;
    goto :goto_4

    .line 153
    .restart local v1    # "_arg2":I
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_7
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 159
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":I
    .end local v2    # "_arg1":Lcom/qualcomm/qti/standalone/NotificationMessageInfo;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_6
    const-string v7, "com.qualcomm.qti.standalone.IQSMService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 161
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 163
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 165
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 166
    .restart local v1    # "_arg2":I
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/standalone/IQSMService$Stub;->QSMService_AcceptInvitation(III)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 167
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 168
    if-eqz v4, :cond_8

    .line 169
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 170
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 173
    :cond_8
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 179
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":I
    .end local v2    # "_arg1":I
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_7
    const-string v7, "com.qualcomm.qti.standalone.IQSMService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 181
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 183
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 185
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 186
    .restart local v1    # "_arg2":I
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/standalone/IQSMService$Stub;->QSMService_RejectInvitation(III)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 187
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 188
    if-eqz v4, :cond_9

    .line 189
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 190
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 193
    :cond_9
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 199
    .end local v0    # "_arg0":I
    .end local v1    # "_arg2":I
    .end local v2    # "_arg1":I
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_8
    const-string v7, "com.qualcomm.qti.standalone.IQSMService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 201
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 203
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 205
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 206
    .restart local v1    # "_arg2":I
    invoke-virtual {p0, v0, v2, v1}, Lcom/qualcomm/qti/standalone/IQSMService$Stub;->QSMService_CancelInvitation(III)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 207
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 208
    if-eqz v4, :cond_a

    .line 209
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 210
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 213
    :cond_a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
