.class public Lcom/qualcomm/qti/standalone/IncomingInviteInfo;
.super Ljava/lang/Object;
.source "IncomingInviteInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/standalone/IncomingInviteInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

.field private expiresValue:I

.field private idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

.field private listName:Ljava/lang/String;

.field private recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

.field private replyTo:Ljava/lang/String;

.field private reqURI:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/standalone/IncomingInviteInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->reqURI:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->replyTo:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->listName:Ljava/lang/String;

    .line 37
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->reqURI:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->replyTo:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->listName:Ljava/lang/String;

    .line 75
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 76
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/standalone/IncomingInviteInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/standalone/IncomingInviteInfo$1;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public getContentInfo()Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    return-object v0
.end method

.method public getExpiresValue()I
    .locals 1

    .prologue
    .line 230
    iget v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->expiresValue:I

    return v0
.end method

.method public getIdHeaderInfo()Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    return-object v0
.end method

.method public getListName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->listName:Ljava/lang/String;

    return-object v0
.end method

.method public getRecipientInfoArray()[Lcom/qualcomm/qti/rcsservice/RecipientInfo;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    return-object v0
.end method

.method public getReplyTo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->replyTo:Ljava/lang/String;

    return-object v0
.end method

.method public getReqURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->reqURI:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->reqURI:Ljava/lang/String;

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->replyTo:Ljava/lang/String;

    .line 83
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 84
    .local v0, "length":I
    if-lez v0, :cond_0

    .line 86
    new-array v1, v0, [Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    iput-object v1, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    .line 87
    iget-object v1, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    sget-object v2, Lcom/qualcomm/qti/rcsservice/RecipientInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    .line 89
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->listName:Ljava/lang/String;

    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->expiresValue:I

    .line 91
    const-class v1, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    iput-object v1, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .line 92
    const-class v1, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/ContentInfo;

    iput-object v1, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 93
    return-void
.end method

.method public setContentInfo(Lcom/qualcomm/qti/rcsservice/ContentInfo;)V
    .locals 0
    .param p1, "contentInfo"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 221
    return-void
.end method

.method public setExpiresValue(I)V
    .locals 0
    .param p1, "expiresValue"    # I

    .prologue
    .line 241
    iput p1, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->expiresValue:I

    .line 242
    return-void
.end method

.method public setIdHeaderInfo(Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;)V
    .locals 0
    .param p1, "idHeaderInfo"    # Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    .line 200
    return-void
.end method

.method public setListName(Ljava/lang/String;)V
    .locals 0
    .param p1, "listName"    # Ljava/lang/String;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->listName:Ljava/lang/String;

    .line 179
    return-void
.end method

.method public setRecipientInfoArray([Lcom/qualcomm/qti/rcsservice/RecipientInfo;)V
    .locals 0
    .param p1, "recipientInfoArray"    # [Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    .line 158
    return-void
.end method

.method public setReplyTo(Ljava/lang/String;)V
    .locals 0
    .param p1, "replyTo"    # Ljava/lang/String;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->replyTo:Ljava/lang/String;

    .line 137
    return-void
.end method

.method public setReqURI(Ljava/lang/String;)V
    .locals 0
    .param p1, "reqURI"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->reqURI:Ljava/lang/String;

    .line 116
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x0

    .line 45
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->reqURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->replyTo:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 51
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->recipientInfoArray:[Lcom/qualcomm/qti/rcsservice/RecipientInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 55
    :goto_0
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->listName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 56
    iget v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->expiresValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 57
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->idHeaderInfo:Lcom/qualcomm/qti/rcsservice/IdHeaderInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 58
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingInviteInfo;->contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 59
    return-void

    .line 53
    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
