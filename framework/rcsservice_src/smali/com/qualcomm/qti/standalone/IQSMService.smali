.class public interface abstract Lcom/qualcomm/qti/standalone/IQSMService;
.super Ljava/lang/Object;
.source "IQSMService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/standalone/IQSMService$Stub;
    }
.end annotation


# virtual methods
.method public abstract QSMService_AcceptInvitation(III)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMService_AddListener(ILcom/qualcomm/qti/standalone/IQSMServiceListener;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMService_CancelInvitation(III)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMService_GetVersion(ILcom/qualcomm/qti/rcsservice/VersionInfo;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMService_RejectInvitation(III)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMService_RemoveListener(IJ)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMService_SendDisplayNotification(ILcom/qualcomm/qti/standalone/NotificationMessageInfo;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QSMService_SendStandaloneMessage(ILcom/qualcomm/qti/standalone/MessageInfo;I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
