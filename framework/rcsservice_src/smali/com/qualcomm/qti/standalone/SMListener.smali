.class public Lcom/qualcomm/qti/standalone/SMListener;
.super Ljava/lang/Object;
.source "SMListener.java"


# instance fields
.field private m_QSMServiceListener:Lcom/qualcomm/qti/standalone/IQSMServiceListener;

.field private m_SMListenerHandle:I

.field private m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

.field private m_errorCode:Lcom/qualcomm/qti/rcsservice/Error;

.field private m_errorDesc:Ljava/lang/String;

.field private m_featureTag:Ljava/lang/String;

.field private m_imdnReqInfo:Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

.field private m_reqStatus:Lcom/qualcomm/qti/rcsservice/ReqStatus;

.field private m_smIncomingMsgInfo:Lcom/qualcomm/qti/standalone/IncomingMsgInfo;

.field private m_smServiceHandle:I

.field private m_smServiceListenerUserData:J

.field private m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

.field private m_statusInfoArray:[Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;

.field private m_tid:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_featureTag:Ljava/lang/String;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_errorDesc:Ljava/lang/String;

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/qualcomm/qti/standalone/SMListener;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/standalone/SMListener;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    return-object v0
.end method

.method static synthetic access$100(Lcom/qualcomm/qti/standalone/SMListener;)Lcom/qualcomm/qti/standalone/IQSMServiceListener;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/standalone/SMListener;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_QSMServiceListener:Lcom/qualcomm/qti/standalone/IQSMServiceListener;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/qualcomm/qti/standalone/SMListener;)[Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/standalone/SMListener;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_statusInfoArray:[Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/qualcomm/qti/standalone/SMListener;)Lcom/qualcomm/qti/rcsservice/ContentInfo;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/standalone/SMListener;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/qualcomm/qti/standalone/SMListener;)Lcom/qualcomm/qti/rcsservice/ReqStatus;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/standalone/SMListener;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_reqStatus:Lcom/qualcomm/qti/rcsservice/ReqStatus;

    return-object v0
.end method

.method static synthetic access$200(Lcom/qualcomm/qti/standalone/SMListener;)J
    .locals 2
    .param p0, "x0"    # Lcom/qualcomm/qti/standalone/SMListener;

    .prologue
    .line 17
    iget-wide v0, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_smServiceListenerUserData:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/qualcomm/qti/standalone/SMListener;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/standalone/SMListener;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_featureTag:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/qualcomm/qti/standalone/SMListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/standalone/SMListener;

    .prologue
    .line 17
    iget v0, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_smServiceHandle:I

    return v0
.end method

.method static synthetic access$500(Lcom/qualcomm/qti/standalone/SMListener;)Lcom/qualcomm/qti/standalone/IncomingMsgInfo;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/standalone/SMListener;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_smIncomingMsgInfo:Lcom/qualcomm/qti/standalone/IncomingMsgInfo;

    return-object v0
.end method

.method static synthetic access$600(Lcom/qualcomm/qti/standalone/SMListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/standalone/SMListener;

    .prologue
    .line 17
    iget v0, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_tid:I

    return v0
.end method

.method static synthetic access$700(Lcom/qualcomm/qti/standalone/SMListener;)Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/standalone/SMListener;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_imdnReqInfo:Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    return-object v0
.end method

.method static synthetic access$800(Lcom/qualcomm/qti/standalone/SMListener;)Lcom/qualcomm/qti/rcsservice/Error;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/standalone/SMListener;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_errorCode:Lcom/qualcomm/qti/rcsservice/Error;

    return-object v0
.end method

.method static synthetic access$900(Lcom/qualcomm/qti/standalone/SMListener;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qti/standalone/SMListener;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_errorDesc:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method QSMServiceListener_HandleIncomingInvitation(ILcom/qualcomm/qti/standalone/IncomingMsgInfo;)V
    .locals 1
    .param p1, "tid"    # I
    .param p2, "smIncomingMsgInfo"    # Lcom/qualcomm/qti/standalone/IncomingMsgInfo;

    .prologue
    .line 239
    iput p1, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_tid:I

    .line 240
    iput-object p2, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_smIncomingMsgInfo:Lcom/qualcomm/qti/standalone/IncomingMsgInfo;

    .line 242
    new-instance v0, Lcom/qualcomm/qti/standalone/SMListener$9;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/SMListener$9;-><init>(Lcom/qualcomm/qti/standalone/SMListener;)V

    .line 259
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 261
    return-void
.end method

.method QSMServiceListener_HandleIncomingMessage(Lcom/qualcomm/qti/standalone/IncomingMsgInfo;)V
    .locals 1
    .param p1, "smIncomingMsgInfo"    # Lcom/qualcomm/qti/standalone/IncomingMsgInfo;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_smIncomingMsgInfo:Lcom/qualcomm/qti/standalone/IncomingMsgInfo;

    .line 119
    new-instance v0, Lcom/qualcomm/qti/standalone/SMListener$4;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/SMListener$4;-><init>(Lcom/qualcomm/qti/standalone/SMListener;)V

    .line 134
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 136
    return-void
.end method

.method QSMServiceListener_HandleInvitationCanceled(I)V
    .locals 1
    .param p1, "tid"    # I

    .prologue
    .line 215
    iput p1, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_tid:I

    .line 217
    new-instance v0, Lcom/qualcomm/qti/standalone/SMListener$8;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/SMListener$8;-><init>(Lcom/qualcomm/qti/standalone/SMListener;)V

    .line 233
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 235
    return-void
.end method

.method QSMServiceListener_HandleMessageDeliveryStatus([Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;)V
    .locals 1
    .param p1, "statusInfoArray"    # [Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_statusInfoArray:[Lcom/qualcomm/qti/rcsservice/DeliveryStatusInfo;

    .line 192
    new-instance v0, Lcom/qualcomm/qti/standalone/SMListener$7;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/SMListener$7;-><init>(Lcom/qualcomm/qti/standalone/SMListener;)V

    .line 209
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 211
    return-void
.end method

.method QSMServiceListener_HandleReceiveSuccess(ILcom/qualcomm/qti/standalone/IncomingMsgInfo;)V
    .locals 1
    .param p1, "tid"    # I
    .param p2, "msgInfo"    # Lcom/qualcomm/qti/standalone/IncomingMsgInfo;

    .prologue
    .line 337
    iput p1, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_tid:I

    .line 338
    iput-object p2, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_smIncomingMsgInfo:Lcom/qualcomm/qti/standalone/IncomingMsgInfo;

    .line 340
    new-instance v0, Lcom/qualcomm/qti/standalone/SMListener$13;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/SMListener$13;-><init>(Lcom/qualcomm/qti/standalone/SMListener;)V

    .line 355
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 357
    return-void
.end method

.method QSMServiceListener_HandleServiceReqStatus(ILcom/qualcomm/qti/rcsservice/ReqStatus;)V
    .locals 1
    .param p1, "tid"    # I
    .param p2, "reqStatus"    # Lcom/qualcomm/qti/rcsservice/ReqStatus;

    .prologue
    .line 361
    iput p1, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_tid:I

    .line 362
    iput-object p2, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_reqStatus:Lcom/qualcomm/qti/rcsservice/ReqStatus;

    .line 364
    new-instance v0, Lcom/qualcomm/qti/standalone/SMListener$14;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/SMListener$14;-><init>(Lcom/qualcomm/qti/standalone/SMListener;)V

    .line 380
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 382
    return-void
.end method

.method QSMServiceListener_HandleSmError(ILcom/qualcomm/qti/rcsservice/Error;Ljava/lang/String;)V
    .locals 1
    .param p1, "tid"    # I
    .param p2, "errorCode"    # Lcom/qualcomm/qti/rcsservice/Error;
    .param p3, "errorDesc"    # Ljava/lang/String;

    .prologue
    .line 164
    iput p1, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_tid:I

    .line 165
    iput-object p2, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_errorCode:Lcom/qualcomm/qti/rcsservice/Error;

    .line 166
    iput-object p3, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_errorDesc:Ljava/lang/String;

    .line 168
    new-instance v0, Lcom/qualcomm/qti/standalone/SMListener$6;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/SMListener$6;-><init>(Lcom/qualcomm/qti/standalone/SMListener;)V

    .line 183
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 185
    return-void
.end method

.method QSMServiceListener_HandleSmSendSuccess(ILcom/qualcomm/qti/rcsservice/ImdnReqInfo;)V
    .locals 1
    .param p1, "tid"    # I
    .param p2, "imdnReqInfo"    # Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    .prologue
    .line 140
    iput p1, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_tid:I

    .line 141
    iput-object p2, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_imdnReqInfo:Lcom/qualcomm/qti/rcsservice/ImdnReqInfo;

    .line 143
    new-instance v0, Lcom/qualcomm/qti/standalone/SMListener$5;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/SMListener$5;-><init>(Lcom/qualcomm/qti/standalone/SMListener;)V

    .line 158
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 160
    return-void
.end method

.method QSMServiceListener_HandleTransferProgress(ILcom/qualcomm/qti/rcsservice/ContentInfo;)V
    .locals 1
    .param p1, "tid"    # I
    .param p2, "contentInfo"    # Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .prologue
    .line 312
    iput p1, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_tid:I

    .line 313
    iput-object p2, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_contentInfo:Lcom/qualcomm/qti/rcsservice/ContentInfo;

    .line 315
    new-instance v0, Lcom/qualcomm/qti/standalone/SMListener$12;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/SMListener$12;-><init>(Lcom/qualcomm/qti/standalone/SMListener;)V

    .line 331
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 333
    return-void
.end method

.method QSMServiceListener_HandleTransferStarted(I)V
    .locals 1
    .param p1, "tid"    # I

    .prologue
    .line 265
    iput p1, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_tid:I

    .line 267
    new-instance v0, Lcom/qualcomm/qti/standalone/SMListener$10;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/SMListener$10;-><init>(Lcom/qualcomm/qti/standalone/SMListener;)V

    .line 281
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 283
    return-void
.end method

.method QSMServiceListener_HandleTransferTerminatedByRemote(I)V
    .locals 1
    .param p1, "tid"    # I

    .prologue
    .line 287
    iput p1, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_tid:I

    .line 289
    new-instance v0, Lcom/qualcomm/qti/standalone/SMListener$11;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/SMListener$11;-><init>(Lcom/qualcomm/qti/standalone/SMListener;)V

    .line 306
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 308
    return-void
.end method

.method QSMServiceListener_ServiceAvailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .locals 1
    .param p1, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 48
    new-instance v0, Lcom/qualcomm/qti/standalone/SMListener$1;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/SMListener$1;-><init>(Lcom/qualcomm/qti/standalone/SMListener;)V

    .line 62
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 64
    return-void
.end method

.method QSMServiceListener_ServiceCreated(JLjava/lang/String;Lcom/qualcomm/qti/rcsservice/StatusCode;I)V
    .locals 1
    .param p1, "smServiceListenerUserData"    # J
    .param p3, "featureTag"    # Ljava/lang/String;
    .param p4, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .param p5, "smServiceHandle"    # I

    .prologue
    .line 90
    iput-wide p1, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_smServiceListenerUserData:J

    .line 91
    iput-object p3, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_featureTag:Ljava/lang/String;

    .line 92
    iput-object p4, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 93
    iput p5, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_smServiceHandle:I

    .line 95
    new-instance v0, Lcom/qualcomm/qti/standalone/SMListener$3;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/SMListener$3;-><init>(Lcom/qualcomm/qti/standalone/SMListener;)V

    .line 111
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 113
    return-void
.end method

.method QSMServiceListener_ServiceUnavailable(Lcom/qualcomm/qti/rcsservice/StatusCode;)V
    .locals 1
    .param p1, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 70
    new-instance v0, Lcom/qualcomm/qti/standalone/SMListener$2;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/standalone/SMListener$2;-><init>(Lcom/qualcomm/qti/standalone/SMListener;)V

    .line 84
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 86
    return-void
.end method

.method public setIQSMServiceListener(Lcom/qualcomm/qti/standalone/IQSMServiceListener;)V
    .locals 2
    .param p1, "objIQSMServiceListener"    # Lcom/qualcomm/qti/standalone/IQSMServiceListener;

    .prologue
    .line 39
    const-string v0, "AIDL"

    const-string v1, " QSMListener : setQSMListenerInstance start "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/SMListener;->m_QSMServiceListener:Lcom/qualcomm/qti/standalone/IQSMServiceListener;

    .line 41
    const-string v0, "AIDL"

    const-string v1, " QSMListener : setQSMListenerInstance end "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    return-void
.end method
