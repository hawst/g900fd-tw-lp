.class public Lcom/qualcomm/qti/standalone/IncomingMsgInfo;
.super Ljava/lang/Object;
.source "IncomingMsgInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/standalone/IncomingMsgInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private dateTime:Ljava/lang/String;

.field private msgID:Ljava/lang/String;

.field private msgInfo:Lcom/qualcomm/qti/standalone/MessageInfo;

.field remoteContactInfo:Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/standalone/IncomingMsgInfo$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->msgID:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->dateTime:Ljava/lang/String;

    .line 33
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->msgID:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->dateTime:Ljava/lang/String;

    .line 61
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 62
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/standalone/IncomingMsgInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/standalone/IncomingMsgInfo$1;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public getDateTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->dateTime:Ljava/lang/String;

    return-object v0
.end method

.method public getIncomingContactInfo()Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->remoteContactInfo:Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;

    return-object v0
.end method

.method public getMessageID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->msgID:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageInfo()Lcom/qualcomm/qti/standalone/MessageInfo;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->msgInfo:Lcom/qualcomm/qti/standalone/MessageInfo;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 65
    const-class v0, Lcom/qualcomm/qti/standalone/MessageInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/standalone/MessageInfo;

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->msgInfo:Lcom/qualcomm/qti/standalone/MessageInfo;

    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->msgID:Ljava/lang/String;

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->dateTime:Ljava/lang/String;

    .line 68
    const-class v0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;

    iput-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->remoteContactInfo:Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;

    .line 70
    return-void
.end method

.method public setDateTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "dateTime"    # Ljava/lang/String;

    .prologue
    .line 134
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->dateTime:Ljava/lang/String;

    .line 135
    return-void
.end method

.method public setIncomingContactInfo(Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;)V
    .locals 0
    .param p1, "remoteContactInfo"    # Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->remoteContactInfo:Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;

    .line 156
    return-void
.end method

.method public setMessageID(Ljava/lang/String;)V
    .locals 0
    .param p1, "msgID"    # Ljava/lang/String;

    .prologue
    .line 113
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->msgID:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public setMessageInfo(Lcom/qualcomm/qti/standalone/MessageInfo;)V
    .locals 0
    .param p1, "msgInfo"    # Lcom/qualcomm/qti/standalone/MessageInfo;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->msgInfo:Lcom/qualcomm/qti/standalone/MessageInfo;

    .line 93
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 41
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->msgInfo:Lcom/qualcomm/qti/standalone/MessageInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 42
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->msgID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->dateTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/qualcomm/qti/standalone/IncomingMsgInfo;->remoteContactInfo:Lcom/qualcomm/qti/rcsservice/IncomingContactInfo;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 45
    return-void
.end method
