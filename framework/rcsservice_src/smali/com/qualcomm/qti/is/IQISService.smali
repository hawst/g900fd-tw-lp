.class public interface abstract Lcom/qualcomm/qti/is/IQISService;
.super Ljava/lang/Object;
.source "IQISService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/is/IQISService$Stub;
    }
.end annotation


# virtual methods
.method public abstract QISService_AddListener(ILcom/qualcomm/qti/is/IQISServiceListener;Lcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISService_GetActiveImageShareSessions(I[Lcom/qualcomm/qti/rcsservice/QRCSInt;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISService_GetImageShareSessionWithContact(ILjava/lang/String;[Lcom/qualcomm/qti/rcsservice/QRCSInt;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISService_GetVersion(ILcom/qualcomm/qti/rcsservice/QRCSString;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISService_RemoveListener(ILcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISService_TransferFile(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISSession_AcceptSession(I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISSession_AddSessionListener(ILcom/qualcomm/qti/is/IQISSessionListener;Lcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISSession_CancelSession(I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISSession_GetFileType(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISSession_GetFilename(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISSession_GetFilesize(I)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISSession_GetFilesizeRemaining(I)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISSession_GetRemoteContact(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISSession_GetSessionID(I)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISSession_GetSessionState(I)Lcom/qualcomm/qti/is/ISSessionState;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISSession_RejectSession(I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QISSession_RemoveSessionListener(ILcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
