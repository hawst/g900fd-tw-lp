.class public abstract Lcom/qualcomm/qti/is/IQISService$Stub;
.super Landroid/os/Binder;
.source "IQISService.java"

# interfaces
.implements Lcom/qualcomm/qti/is/IQISService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/is/IQISService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/is/IQISService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.is.IQISService"

.field static final TRANSACTION_QISService_AddListener:I = 0x2

.field static final TRANSACTION_QISService_GetActiveImageShareSessions:I = 0x5

.field static final TRANSACTION_QISService_GetImageShareSessionWithContact:I = 0x6

.field static final TRANSACTION_QISService_GetVersion:I = 0x1

.field static final TRANSACTION_QISService_RemoveListener:I = 0x3

.field static final TRANSACTION_QISService_TransferFile:I = 0x4

.field static final TRANSACTION_QISSession_AcceptSession:I = 0xe

.field static final TRANSACTION_QISSession_AddSessionListener:I = 0x11

.field static final TRANSACTION_QISSession_CancelSession:I = 0x10

.field static final TRANSACTION_QISSession_GetFileType:I = 0xc

.field static final TRANSACTION_QISSession_GetFilename:I = 0xa

.field static final TRANSACTION_QISSession_GetFilesize:I = 0xb

.field static final TRANSACTION_QISSession_GetFilesizeRemaining:I = 0xd

.field static final TRANSACTION_QISSession_GetRemoteContact:I = 0x8

.field static final TRANSACTION_QISSession_GetSessionID:I = 0x7

.field static final TRANSACTION_QISSession_GetSessionState:I = 0x9

.field static final TRANSACTION_QISSession_RejectSession:I = 0xf

.field static final TRANSACTION_QISSession_RemoveSessionListener:I = 0x12


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/is/IQISService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/is/IQISService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.is.IQISService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/is/IQISService;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/is/IQISService;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/is/IQISService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/is/IQISService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 410
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 44
    :sswitch_0
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 53
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_0

    .line 54
    sget-object v7, Lcom/qualcomm/qti/rcsservice/QRCSString;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/QRCSString;

    .line 60
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_1

    .line 61
    sget-object v7, Lcom/qualcomm/qti/rcsservice/QRCSInt;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/QRCSInt;

    .line 66
    .local v2, "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    :goto_2
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISService_GetVersion(ILcom/qualcomm/qti/rcsservice/QRCSString;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 67
    .local v4, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 68
    if-eqz v4, :cond_2

    .line 69
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 75
    :goto_3
    if-eqz v1, :cond_3

    .line 76
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 77
    invoke-virtual {v1, p3, v6}, Lcom/qualcomm/qti/rcsservice/QRCSString;->writeToParcel(Landroid/os/Parcel;I)V

    .line 82
    :goto_4
    if-eqz v2, :cond_4

    .line 83
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 84
    invoke-virtual {v2, p3, v6}, Lcom/qualcomm/qti/rcsservice/QRCSInt;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 57
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    goto :goto_1

    .line 64
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    goto :goto_2

    .line 73
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_2
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3

    .line 80
    :cond_3
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_4

    .line 87
    :cond_4
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 93
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSString;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_2
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 97
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Lcom/qualcomm/qti/is/IQISServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/is/IQISServiceListener;

    move-result-object v1

    .line 99
    .local v1, "_arg1":Lcom/qualcomm/qti/is/IQISServiceListener;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_5

    .line 100
    sget-object v7, Lcom/qualcomm/qti/rcsservice/QRCSLong;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/QRCSLong;

    .line 105
    .local v2, "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    :goto_5
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISService_AddListener(ILcom/qualcomm/qti/is/IQISServiceListener;Lcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 106
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 107
    if-eqz v4, :cond_6

    .line 108
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 114
    :goto_6
    if-eqz v2, :cond_7

    .line 115
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 116
    invoke-virtual {v2, p3, v6}, Lcom/qualcomm/qti/rcsservice/QRCSLong;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 103
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_5
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    goto :goto_5

    .line 112
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_6
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_6

    .line 119
    :cond_7
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 125
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/is/IQISServiceListener;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_3
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 127
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 129
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_8

    .line 130
    sget-object v7, Lcom/qualcomm/qti/rcsservice/QRCSLong;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/QRCSLong;

    .line 135
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    :goto_7
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISService_RemoveListener(ILcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 136
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 137
    if-eqz v4, :cond_9

    .line 138
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 139
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 133
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_8
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    goto :goto_7

    .line 142
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_9
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 148
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_4
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 152
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 154
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 156
    .local v2, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 157
    .local v3, "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISService_TransferFile(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 158
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 159
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 164
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v2    # "_arg2":Ljava/lang/String;
    .end local v3    # "_arg3":Ljava/lang/String;
    .end local v4    # "_result":I
    :sswitch_5
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 166
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 168
    .restart local v0    # "_arg0":I
    sget-object v7, Lcom/qualcomm/qti/rcsservice/QRCSInt;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/qualcomm/qti/rcsservice/QRCSInt;

    .line 170
    .local v1, "_arg1":[Lcom/qualcomm/qti/rcsservice/QRCSInt;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_a

    .line 171
    sget-object v7, Lcom/qualcomm/qti/rcsservice/QRCSInt;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/QRCSInt;

    .line 176
    .local v2, "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    :goto_8
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISService_GetActiveImageShareSessions(I[Lcom/qualcomm/qti/rcsservice/QRCSInt;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 177
    .local v4, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 178
    if-eqz v4, :cond_b

    .line 179
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 180
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 185
    :goto_9
    invoke-virtual {p3, v1, v6}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 186
    if-eqz v2, :cond_c

    .line 187
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 188
    invoke-virtual {v2, p3, v6}, Lcom/qualcomm/qti/rcsservice/QRCSInt;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 174
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_a
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    goto :goto_8

    .line 183
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_9

    .line 191
    :cond_c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 197
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":[Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_6
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 199
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 201
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 203
    .local v1, "_arg1":Ljava/lang/String;
    sget-object v7, Lcom/qualcomm/qti/rcsservice/QRCSInt;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/qualcomm/qti/rcsservice/QRCSInt;

    .line 205
    .local v2, "_arg2":[Lcom/qualcomm/qti/rcsservice/QRCSInt;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_d

    .line 206
    sget-object v7, Lcom/qualcomm/qti/rcsservice/QRCSInt;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/qualcomm/qti/rcsservice/QRCSInt;

    .line 211
    .local v3, "_arg3":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    :goto_a
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISService_GetImageShareSessionWithContact(ILjava/lang/String;[Lcom/qualcomm/qti/rcsservice/QRCSInt;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 212
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 213
    if-eqz v4, :cond_e

    .line 214
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 215
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 220
    :goto_b
    invoke-virtual {p3, v2, v6}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 221
    if-eqz v3, :cond_f

    .line 222
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 223
    invoke-virtual {v3, p3, v6}, Lcom/qualcomm/qti/rcsservice/QRCSInt;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 209
    .end local v3    # "_arg3":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_d
    const/4 v3, 0x0

    .restart local v3    # "_arg3":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    goto :goto_a

    .line 218
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_e
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_b

    .line 226
    :cond_f
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 232
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v2    # "_arg2":[Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .end local v3    # "_arg3":Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_7
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 234
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 235
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISSession_GetSessionID(I)Ljava/lang/String;

    move-result-object v4

    .line 236
    .local v4, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 237
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 242
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_8
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 244
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 245
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISSession_GetRemoteContact(I)Ljava/lang/String;

    move-result-object v4

    .line 246
    .restart local v4    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 247
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 252
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_9
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 254
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 255
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISSession_GetSessionState(I)Lcom/qualcomm/qti/is/ISSessionState;

    move-result-object v4

    .line 256
    .local v4, "_result":Lcom/qualcomm/qti/is/ISSessionState;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 257
    if-eqz v4, :cond_10

    .line 258
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 259
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/is/ISSessionState;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 262
    :cond_10
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 268
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Lcom/qualcomm/qti/is/ISSessionState;
    :sswitch_a
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 270
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 271
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISSession_GetFilename(I)Ljava/lang/String;

    move-result-object v4

    .line 272
    .local v4, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 273
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 278
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_b
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 280
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 281
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISSession_GetFilesize(I)J

    move-result-wide v4

    .line 282
    .local v4, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 283
    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 288
    .end local v0    # "_arg0":I
    .end local v4    # "_result":J
    :sswitch_c
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 290
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 291
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISSession_GetFileType(I)Ljava/lang/String;

    move-result-object v4

    .line 292
    .local v4, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 293
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 298
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_d
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 300
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 301
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISSession_GetFilesizeRemaining(I)J

    move-result-wide v4

    .line 302
    .local v4, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 303
    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 308
    .end local v0    # "_arg0":I
    .end local v4    # "_result":J
    :sswitch_e
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 310
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 311
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISSession_AcceptSession(I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 312
    .local v4, "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 313
    if-eqz v4, :cond_11

    .line 314
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 315
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 318
    :cond_11
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 324
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_f
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 326
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 327
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISSession_RejectSession(I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 328
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 329
    if-eqz v4, :cond_12

    .line 330
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 331
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 334
    :cond_12
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 340
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_10
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 342
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 343
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISSession_CancelSession(I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 344
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 345
    if-eqz v4, :cond_13

    .line 346
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 347
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 350
    :cond_13
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 356
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_11
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 358
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 360
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Lcom/qualcomm/qti/is/IQISSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/is/IQISSessionListener;

    move-result-object v1

    .line 362
    .local v1, "_arg1":Lcom/qualcomm/qti/is/IQISSessionListener;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_14

    .line 363
    sget-object v7, Lcom/qualcomm/qti/rcsservice/QRCSLong;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsservice/QRCSLong;

    .line 368
    .local v2, "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    :goto_c
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISSession_AddSessionListener(ILcom/qualcomm/qti/is/IQISSessionListener;Lcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 369
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 370
    if-eqz v4, :cond_15

    .line 371
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 372
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    .line 377
    :goto_d
    if-eqz v2, :cond_16

    .line 378
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 379
    invoke-virtual {v2, p3, v6}, Lcom/qualcomm/qti/rcsservice/QRCSLong;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 366
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_14
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    goto :goto_c

    .line 375
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_15
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_d

    .line 382
    :cond_16
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 388
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Lcom/qualcomm/qti/is/IQISSessionListener;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :sswitch_12
    const-string v7, "com.qualcomm.qti.is.IQISService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 390
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 392
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_17

    .line 393
    sget-object v7, Lcom/qualcomm/qti/rcsservice/QRCSLong;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsservice/QRCSLong;

    .line 398
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    :goto_e
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/is/IQISService$Stub;->QISSession_RemoveSessionListener(ILcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v4

    .line 399
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 400
    if-eqz v4, :cond_18

    .line 401
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 402
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/qti/rcsservice/StatusCode;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 396
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_17
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    goto :goto_e

    .line 405
    .restart local v4    # "_result":Lcom/qualcomm/qti/rcsservice/StatusCode;
    :cond_18
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
