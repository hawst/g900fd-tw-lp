.class public Lcom/qualcomm/qti/is/ISSessionState;
.super Ljava/lang/Object;
.source "ISSessionState.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/is/ISSessionState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 152
    new-instance v0, Lcom/qualcomm/qti/is/ISSessionState$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/is/ISSessionState$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/is/ISSessionState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/is/ISSessionState;->readFromParcel(Landroid/os/Parcel;)V

    .line 167
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/is/ISSessionState$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/is/ISSessionState$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/is/ISSessionState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getISSessionStateInstance()Lcom/qualcomm/qti/is/ISSessionState;
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/qualcomm/qti/is/ISSessionState;

    invoke-direct {v0}, Lcom/qualcomm/qti/is/ISSessionState;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/qualcomm/qti/is/ISSessionState;->eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150
    return-void

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/is/ISSessionState;->eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    invoke-virtual {v0}, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return v0
.end method

.method public getISSessionState()Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/qualcomm/qti/is/ISSessionState;->eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 172
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/is/ISSessionState;->eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :goto_0
    return-void

    .line 173
    :catch_0
    move-exception v0

    .line 174
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_UNKNOWN:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    iput-object v1, p0, Lcom/qualcomm/qti/is/ISSessionState;->eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    goto :goto_0
.end method

.method public setISSessionState(I)V
    .locals 3
    .param p1, "nISSessionState"    # I

    .prologue
    .line 91
    packed-switch p1, :pswitch_data_0

    .line 126
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/qualcomm/qti/is/ISSessionState;->eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    sget-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_UNKNOWN:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ISSessionState;->eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    .line 131
    :goto_0
    return-void

    .line 94
    :pswitch_0
    sget-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_UNKNOWN:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ISSessionState;->eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    goto :goto_0

    .line 98
    :pswitch_1
    sget-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_INIT:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ISSessionState;->eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    goto :goto_0

    .line 102
    :pswitch_2
    sget-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_PENDING:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ISSessionState;->eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    goto :goto_0

    .line 106
    :pswitch_3
    sget-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_CONNECTING:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ISSessionState;->eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    goto :goto_0

    .line 110
    :pswitch_4
    sget-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_ESTABLISHED:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ISSessionState;->eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    goto :goto_0

    .line 114
    :pswitch_5
    sget-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_CANCELLED:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ISSessionState;->eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    goto :goto_0

    .line 118
    :pswitch_6
    sget-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_TERMINATING:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ISSessionState;->eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    goto :goto_0

    .line 122
    :pswitch_7
    sget-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_TERMINATED:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ISSessionState;->eISSessionState:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    goto :goto_0

    .line 91
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 143
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/is/ISSessionState;->writeToParcel(Landroid/os/Parcel;)V

    .line 145
    return-void
.end method
