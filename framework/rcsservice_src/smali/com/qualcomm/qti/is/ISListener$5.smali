.class Lcom/qualcomm/qti/is/ISListener$5;
.super Ljava/lang/Thread;
.source "ISListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/qti/is/ISListener;->Service_IQISSession_HandleSessionTerminatedByRemote(JI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/is/ISListener;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/is/ISListener;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/qualcomm/qti/is/ISListener$5;->this$0:Lcom/qualcomm/qti/is/ISListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 185
    :try_start_0
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleSessionTerminatedByRemote start inside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    iget-object v1, p0, Lcom/qualcomm/qti/is/ISListener$5;->this$0:Lcom/qualcomm/qti/is/ISListener;

    iget-object v1, v1, Lcom/qualcomm/qti/is/ISListener;->m_IQISSessionListener:Lcom/qualcomm/qti/is/IQISSessionListener;

    iget-object v2, p0, Lcom/qualcomm/qti/is/ISListener$5;->this$0:Lcom/qualcomm/qti/is/ISListener;

    iget-wide v2, v2, Lcom/qualcomm/qti/is/ISListener;->m_pISSessionListenerHdl:J

    iget-object v4, p0, Lcom/qualcomm/qti/is/ISListener$5;->this$0:Lcom/qualcomm/qti/is/ISListener;

    iget v4, v4, Lcom/qualcomm/qti/is/ISListener;->m_pIsSessionHdlInt:I

    invoke-interface {v1, v2, v3, v4}, Lcom/qualcomm/qti/is/IQISSessionListener;->IQISSession_HandleSessionTerminatedByRemote(JI)V

    .line 189
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleSessionTerminatedByRemote end inside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :goto_0
    return-void

    .line 192
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
