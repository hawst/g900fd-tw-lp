.class public interface abstract Lcom/qualcomm/qti/is/IQISServiceListener;
.super Ljava/lang/Object;
.source "IQISServiceListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/is/IQISServiceListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract IQISService_GetVersion(JLcom/qualcomm/qti/rcsservice/QRCSString;Lcom/qualcomm/qti/rcsservice/QRCSInt;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQISService_HandleIncomingSession(JLcom/qualcomm/qti/rcsservice/QRCSInt;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQISService_ServiceAvailable(JLcom/qualcomm/qti/rcsservice/StatusCode;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQISService_ServiceUnavailable(JLcom/qualcomm/qti/rcsservice/StatusCode;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
