.class Lcom/qualcomm/qti/is/ISListener$3;
.super Ljava/lang/Thread;
.source "ISListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/qti/is/ISListener;->Service_IQISService_ServiceAvailable(JLcom/qualcomm/qti/rcsservice/StatusCode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/is/ISListener;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/is/ISListener;)V
    .locals 0

    .prologue
    .line 124
    iput-object p1, p0, Lcom/qualcomm/qti/is/ISListener$3;->this$0:Lcom/qualcomm/qti/is/ISListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 129
    :try_start_0
    const-string v1, "AIDL"

    const-string v2, " Service_IQISService_ServiceAvailable start inside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    iget-object v1, p0, Lcom/qualcomm/qti/is/ISListener$3;->this$0:Lcom/qualcomm/qti/is/ISListener;

    iget-object v1, v1, Lcom/qualcomm/qti/is/ISListener;->m_IQISServiceListener:Lcom/qualcomm/qti/is/IQISServiceListener;

    iget-object v2, p0, Lcom/qualcomm/qti/is/ISListener$3;->this$0:Lcom/qualcomm/qti/is/ISListener;

    iget-wide v2, v2, Lcom/qualcomm/qti/is/ISListener;->m_pIsServiceListenerHdl:J

    iget-object v4, p0, Lcom/qualcomm/qti/is/ISListener$3;->this$0:Lcom/qualcomm/qti/is/ISListener;

    iget-object v4, v4, Lcom/qualcomm/qti/is/ISListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    invoke-interface {v1, v2, v3, v4}, Lcom/qualcomm/qti/is/IQISServiceListener;->IQISService_ServiceAvailable(JLcom/qualcomm/qti/rcsservice/StatusCode;)V

    .line 132
    const-string v1, "AIDL"

    const-string v2, " Service_IQISService_ServiceAvailable end inside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :goto_0
    return-void

    .line 134
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
