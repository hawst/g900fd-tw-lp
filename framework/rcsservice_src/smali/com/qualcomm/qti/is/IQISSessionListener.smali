.class public interface abstract Lcom/qualcomm/qti/is/IQISSessionListener;
.super Ljava/lang/Object;
.source "IQISSessionListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/is/IQISSessionListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract IQISSession_HandleSessionAborted(JI)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQISSession_HandleSessionStarted(JI)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQISSession_HandleSessionTerminatedByRemote(JI)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQISSession_HandleTransferError(JILcom/qualcomm/qti/is/ImageShareError;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQISSession_HandleTransferProgress(JILcom/qualcomm/qti/rcsservice/QRCSLong;Lcom/qualcomm/qti/rcsservice/QRCSLong;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract IQISSession_HandleTransferSuccess(JILcom/qualcomm/qti/rcsservice/QRCSString;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
