.class Lcom/qualcomm/qti/is/ISListener$9;
.super Ljava/lang/Thread;
.source "ISListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/qti/is/ISListener;->Service_IQISSession_HandleTransferError(JILcom/qualcomm/qti/is/ImageShareError;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/is/ISListener;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/is/ISListener;)V
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/qualcomm/qti/is/ISListener$9;->this$0:Lcom/qualcomm/qti/is/ISListener;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 303
    :try_start_0
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleTransferError start inside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    iget-object v1, p0, Lcom/qualcomm/qti/is/ISListener$9;->this$0:Lcom/qualcomm/qti/is/ISListener;

    iget-object v1, v1, Lcom/qualcomm/qti/is/ISListener;->m_IQISSessionListener:Lcom/qualcomm/qti/is/IQISSessionListener;

    iget-object v2, p0, Lcom/qualcomm/qti/is/ISListener$9;->this$0:Lcom/qualcomm/qti/is/ISListener;

    iget-wide v2, v2, Lcom/qualcomm/qti/is/ISListener;->m_pISSessionListenerHdl:J

    iget-object v4, p0, Lcom/qualcomm/qti/is/ISListener$9;->this$0:Lcom/qualcomm/qti/is/ISListener;

    iget v4, v4, Lcom/qualcomm/qti/is/ISListener;->m_pIsSessionHdlInt:I

    iget-object v5, p0, Lcom/qualcomm/qti/is/ISListener$9;->this$0:Lcom/qualcomm/qti/is/ISListener;

    iget-object v5, v5, Lcom/qualcomm/qti/is/ISListener;->m_error:Lcom/qualcomm/qti/is/ImageShareError;

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/qualcomm/qti/is/IQISSessionListener;->IQISSession_HandleTransferError(JILcom/qualcomm/qti/is/ImageShareError;)V

    .line 306
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleTransferError end inside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 311
    :goto_0
    return-void

    .line 308
    :catch_0
    move-exception v0

    .line 309
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
