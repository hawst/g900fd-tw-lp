.class public Lcom/qualcomm/qti/is/ISListener;
.super Ljava/lang/Object;
.source "ISListener.java"


# instance fields
.field m_IQISServiceListener:Lcom/qualcomm/qti/is/IQISServiceListener;

.field m_IQISSessionListener:Lcom/qualcomm/qti/is/IQISSessionListener;

.field m_currentSize:Lcom/qualcomm/qti/rcsservice/QRCSLong;

.field m_error:Lcom/qualcomm/qti/is/ImageShareError;

.field m_fileName:Lcom/qualcomm/qti/rcsservice/QRCSString;

.field m_pISSessionListenerHdl:J

.field m_pIsServiceListenerHdl:J

.field m_pIsSessionHdl:Lcom/qualcomm/qti/rcsservice/QRCSInt;

.field m_pIsSessionHdlInt:I

.field m_pURI:Lcom/qualcomm/qti/rcsservice/QRCSString;

.field m_pVersion:Lcom/qualcomm/qti/rcsservice/QRCSString;

.field m_pVersionLen:Lcom/qualcomm/qti/rcsservice/QRCSInt;

.field m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

.field m_totalSize:Lcom/qualcomm/qti/rcsservice/QRCSLong;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    return-void
.end method

.method public static getISListenerInstance()Lcom/qualcomm/qti/is/ISListener;
    .locals 2

    .prologue
    .line 41
    const-string v0, "AIDL"

    const-string v1, "ISListener     getISListenerInstance  start  "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 42
    new-instance v0, Lcom/qualcomm/qti/is/ISListener;

    invoke-direct {v0}, Lcom/qualcomm/qti/is/ISListener;-><init>()V

    return-object v0
.end method


# virtual methods
.method public Service_IQISService_GetVersion(JLcom/qualcomm/qti/rcsservice/QRCSString;Lcom/qualcomm/qti/rcsservice/QRCSInt;)V
    .locals 3
    .param p1, "pIsServiceListenerHdl"    # J
    .param p3, "pVersion"    # Lcom/qualcomm/qti/rcsservice/QRCSString;
    .param p4, "pVersionLen"    # Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 63
    const-string v1, "AIDL"

    const-string v2, " Service_IQISService_GetVersion start outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iput-wide p1, p0, Lcom/qualcomm/qti/is/ISListener;->m_pIsServiceListenerHdl:J

    .line 65
    iput-object p3, p0, Lcom/qualcomm/qti/is/ISListener;->m_pVersion:Lcom/qualcomm/qti/rcsservice/QRCSString;

    .line 66
    iput-object p4, p0, Lcom/qualcomm/qti/is/ISListener;->m_pVersionLen:Lcom/qualcomm/qti/rcsservice/QRCSInt;

    .line 67
    new-instance v0, Lcom/qualcomm/qti/is/ISListener$1;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/is/ISListener$1;-><init>(Lcom/qualcomm/qti/is/ISListener;)V

    .line 84
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 85
    const-string v1, "AIDL"

    const-string v2, " Service_IQISService_GetVersion end outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    return-void
.end method

.method public Service_IQISService_HandleIncomingSession(JLcom/qualcomm/qti/rcsservice/QRCSInt;)V
    .locals 3
    .param p1, "pIsServiceListenerHdl"    # J
    .param p3, "pIsSessionHdl"    # Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 92
    const-string v1, "AIDL"

    const-string v2, " Service_IQISService_HandleIncomingSession start outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iput-wide p1, p0, Lcom/qualcomm/qti/is/ISListener;->m_pIsServiceListenerHdl:J

    .line 94
    iput-object p3, p0, Lcom/qualcomm/qti/is/ISListener;->m_pIsSessionHdl:Lcom/qualcomm/qti/rcsservice/QRCSInt;

    .line 95
    new-instance v0, Lcom/qualcomm/qti/is/ISListener$2;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/is/ISListener$2;-><init>(Lcom/qualcomm/qti/is/ISListener;)V

    .line 112
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 113
    const-string v1, "AIDL"

    const-string v2, " Service_IQISService_HandleIncomingSession end outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    return-void
.end method

.method public Service_IQISService_ServiceAvailable(JLcom/qualcomm/qti/rcsservice/StatusCode;)V
    .locals 3
    .param p1, "pIsServiceListenerHdl"    # J
    .param p3, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 120
    const-string v1, "AIDL"

    const-string v2, " Service_IQISService_ServiceAvailable start outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iput-wide p1, p0, Lcom/qualcomm/qti/is/ISListener;->m_pIsServiceListenerHdl:J

    .line 122
    iput-object p3, p0, Lcom/qualcomm/qti/is/ISListener;->m_statusCode:Lcom/qualcomm/qti/rcsservice/StatusCode;

    .line 123
    new-instance v0, Lcom/qualcomm/qti/is/ISListener$3;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/is/ISListener$3;-><init>(Lcom/qualcomm/qti/is/ISListener;)V

    .line 140
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 141
    const-string v1, "AIDL"

    const-string v2, " Service_IQISService_ServiceAvailable end outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    return-void
.end method

.method public Service_IQISService_ServiceUnavailable(JLcom/qualcomm/qti/rcsservice/StatusCode;)V
    .locals 3
    .param p1, "pIsServiceListenerHdl"    # J
    .param p3, "statusCode"    # Lcom/qualcomm/qti/rcsservice/StatusCode;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 148
    const-string v1, "AIDL"

    const-string v2, " Service_IQISService_ServiceUnavailable start outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    new-instance v0, Lcom/qualcomm/qti/is/ISListener$4;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/is/ISListener$4;-><init>(Lcom/qualcomm/qti/is/ISListener;)V

    .line 166
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 167
    const-string v1, "AIDL"

    const-string v2, " Service_IQISService_ServiceUnavailable end outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    return-void
.end method

.method public Service_IQISSession_HandleSessionAborted(JI)V
    .locals 3
    .param p1, "pISSessionListenerHdl"    # J
    .param p3, "pIsSessionHdl"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 233
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleSessionAborted start outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    iput-wide p1, p0, Lcom/qualcomm/qti/is/ISListener;->m_pISSessionListenerHdl:J

    .line 235
    iput p3, p0, Lcom/qualcomm/qti/is/ISListener;->m_pIsSessionHdlInt:I

    .line 236
    new-instance v0, Lcom/qualcomm/qti/is/ISListener$7;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/is/ISListener$7;-><init>(Lcom/qualcomm/qti/is/ISListener;)V

    .line 253
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 254
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleSessionAborted start outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    return-void
.end method

.method public Service_IQISSession_HandleSessionStarted(JI)V
    .locals 3
    .param p1, "pISSessionListenerHdl"    # J
    .param p3, "pIsSessionHdl"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 205
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleSessionStarted start outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    iput-wide p1, p0, Lcom/qualcomm/qti/is/ISListener;->m_pISSessionListenerHdl:J

    .line 207
    iput p3, p0, Lcom/qualcomm/qti/is/ISListener;->m_pIsSessionHdlInt:I

    .line 209
    new-instance v0, Lcom/qualcomm/qti/is/ISListener$6;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/is/ISListener$6;-><init>(Lcom/qualcomm/qti/is/ISListener;)V

    .line 226
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 227
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleSessionStarted end outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    return-void
.end method

.method public Service_IQISSession_HandleSessionTerminatedByRemote(JI)V
    .locals 3
    .param p1, "pISSessionListenerHdl"    # J
    .param p3, "pIsSessionHdl"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 175
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleSessionTerminatedByRemote start outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iput-wide p1, p0, Lcom/qualcomm/qti/is/ISListener;->m_pISSessionListenerHdl:J

    .line 178
    iput p3, p0, Lcom/qualcomm/qti/is/ISListener;->m_pIsSessionHdlInt:I

    .line 179
    new-instance v0, Lcom/qualcomm/qti/is/ISListener$5;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/is/ISListener$5;-><init>(Lcom/qualcomm/qti/is/ISListener;)V

    .line 198
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 199
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleSessionTerminatedByRemote end outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    return-void
.end method

.method public Service_IQISSession_HandleTransferError(JILcom/qualcomm/qti/is/ImageShareError;)V
    .locals 3
    .param p1, "pISSessionListenerHdl"    # J
    .param p3, "pIsSessionHdl"    # I
    .param p4, "error"    # Lcom/qualcomm/qti/is/ImageShareError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 293
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleTransferError start outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    iput-wide p1, p0, Lcom/qualcomm/qti/is/ISListener;->m_pISSessionListenerHdl:J

    .line 295
    iput p3, p0, Lcom/qualcomm/qti/is/ISListener;->m_pIsSessionHdlInt:I

    .line 296
    iput-object p4, p0, Lcom/qualcomm/qti/is/ISListener;->m_error:Lcom/qualcomm/qti/is/ImageShareError;

    .line 297
    new-instance v0, Lcom/qualcomm/qti/is/ISListener$9;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/is/ISListener$9;-><init>(Lcom/qualcomm/qti/is/ISListener;)V

    .line 314
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 315
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleTransferError end outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    return-void
.end method

.method public Service_IQISSession_HandleTransferProgress(JILcom/qualcomm/qti/rcsservice/QRCSLong;Lcom/qualcomm/qti/rcsservice/QRCSLong;)V
    .locals 3
    .param p1, "pISSessionListenerHdl"    # J
    .param p3, "pIsSessionHdl"    # I
    .param p4, "currentSize"    # Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .param p5, "totalSize"    # Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 262
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleTransferProgress start outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    iput-wide p1, p0, Lcom/qualcomm/qti/is/ISListener;->m_pISSessionListenerHdl:J

    .line 264
    iput p3, p0, Lcom/qualcomm/qti/is/ISListener;->m_pIsSessionHdlInt:I

    .line 265
    iput-object p4, p0, Lcom/qualcomm/qti/is/ISListener;->m_currentSize:Lcom/qualcomm/qti/rcsservice/QRCSLong;

    .line 266
    iput-object p5, p0, Lcom/qualcomm/qti/is/ISListener;->m_totalSize:Lcom/qualcomm/qti/rcsservice/QRCSLong;

    .line 267
    new-instance v0, Lcom/qualcomm/qti/is/ISListener$8;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/is/ISListener$8;-><init>(Lcom/qualcomm/qti/is/ISListener;)V

    .line 285
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 286
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleTransferProgress end outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    return-void
.end method

.method public Service_IQISSession_HandleTransferSuccess(JILcom/qualcomm/qti/rcsservice/QRCSString;)V
    .locals 3
    .param p1, "pISSessionListenerHdl"    # J
    .param p3, "pIsSessionHdl"    # I
    .param p4, "fileName"    # Lcom/qualcomm/qti/rcsservice/QRCSString;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 322
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleTransferSuccess start outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    iput-wide p1, p0, Lcom/qualcomm/qti/is/ISListener;->m_pISSessionListenerHdl:J

    .line 324
    iput p3, p0, Lcom/qualcomm/qti/is/ISListener;->m_pIsSessionHdlInt:I

    .line 325
    iput-object p4, p0, Lcom/qualcomm/qti/is/ISListener;->m_fileName:Lcom/qualcomm/qti/rcsservice/QRCSString;

    .line 326
    new-instance v0, Lcom/qualcomm/qti/is/ISListener$10;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/is/ISListener$10;-><init>(Lcom/qualcomm/qti/is/ISListener;)V

    .line 343
    .local v0, "thread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 344
    const-string v1, "AIDL"

    const-string v2, " Service_IQISSession_HandleTransferSuccess end outside thread"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    return-void
.end method

.method public setIQISServiceListener(Lcom/qualcomm/qti/is/IQISServiceListener;)V
    .locals 2
    .param p1, "objIQISServiceListener"    # Lcom/qualcomm/qti/is/IQISServiceListener;

    .prologue
    .line 47
    const-string v0, "AIDL"

    const-string v1, "  setIQISServiceListener start "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    iput-object p1, p0, Lcom/qualcomm/qti/is/ISListener;->m_IQISServiceListener:Lcom/qualcomm/qti/is/IQISServiceListener;

    .line 49
    return-void
.end method

.method public setIQISSessionListener(Lcom/qualcomm/qti/is/IQISSessionListener;)V
    .locals 2
    .param p1, "objIQISSessionListener"    # Lcom/qualcomm/qti/is/IQISSessionListener;

    .prologue
    .line 53
    const-string v0, "AIDL"

    const-string v1, "  setIQISSessionListener start "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    iput-object p1, p0, Lcom/qualcomm/qti/is/ISListener;->m_IQISSessionListener:Lcom/qualcomm/qti/is/IQISSessionListener;

    .line 55
    return-void
.end method
