.class Lcom/qualcomm/qti/is/QISService$1;
.super Lcom/qualcomm/qti/is/IQISService$Stub;
.source "QISService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/is/QISService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

.field final synthetic this$0:Lcom/qualcomm/qti/is/QISService;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/is/QISService;)V
    .locals 2

    .prologue
    .line 81
    iput-object p1, p0, Lcom/qualcomm/qti/is/QISService$1;->this$0:Lcom/qualcomm/qti/is/QISService;

    invoke-direct {p0}, Lcom/qualcomm/qti/is/IQISService$Stub;-><init>()V

    .line 83
    new-instance v0, Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    iget-object v1, p0, Lcom/qualcomm/qti/is/QISService$1;->this$0:Lcom/qualcomm/qti/is/QISService;

    invoke-direct {v0, v1}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;-><init>(Lcom/qualcomm/qti/is/QISService;)V

    iput-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    return-void
.end method


# virtual methods
.method public QISService_AddListener(ILcom/qualcomm/qti/is/IQISServiceListener;Lcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "pSericeHdl"    # I
    .param p2, "pISServiceListener"    # Lcom/qualcomm/qti/is/IQISServiceListener;
    .param p3, "pISServiceListenerHdl"    # Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISService_AddListener(ILcom/qualcomm/qti/is/IQISServiceListener;Lcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QISService_GetActiveImageShareSessions(I[Lcom/qualcomm/qti/rcsservice/QRCSInt;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "pSericeHdl"    # I
    .param p2, "pIsSessions"    # [Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .param p3, "numOfSessions"    # Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISService_GetActiveImageShareSessions(I[Lcom/qualcomm/qti/rcsservice/QRCSInt;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QISService_GetImageShareSessionWithContact(ILjava/lang/String;[Lcom/qualcomm/qti/rcsservice/QRCSInt;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "pSericeHdl"    # I
    .param p2, "contact_URI"    # Ljava/lang/String;
    .param p3, "pIsSessions"    # [Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .param p4, "numOfSessions"    # Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 121
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISService_GetImageShareSessionWithContact(ILjava/lang/String;[Lcom/qualcomm/qti/rcsservice/QRCSInt;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QISService_GetVersion(ILcom/qualcomm/qti/rcsservice/QRCSString;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "pSericeHdl"    # I
    .param p2, "pVersion"    # Lcom/qualcomm/qti/rcsservice/QRCSString;
    .param p3, "pVersionLen"    # Lcom/qualcomm/qti/rcsservice/QRCSInt;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISService_GetVersion(ILcom/qualcomm/qti/rcsservice/QRCSString;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QISService_RemoveListener(ILcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "pSericeHdl"    # I
    .param p2, "pISServiceListenerHdl"    # Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISService_RemoveListener(ILcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QISService_TransferFile(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "pSericeHdl"    # I
    .param p2, "Contact_URI"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISService_TransferFile(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public QISSession_AcceptSession(I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "pIsSessionHdl"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISSession_AcceptSession(I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QISSession_AddSessionListener(ILcom/qualcomm/qti/is/IQISSessionListener;Lcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "pIsSessionHdl"    # I
    .param p2, "pSessionListener"    # Lcom/qualcomm/qti/is/IQISSessionListener;
    .param p3, "pISSessionListenerHdl"    # Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1, p2, p3}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISSession_AddSessionListener(ILcom/qualcomm/qti/is/IQISSessionListener;Lcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QISSession_CancelSession(I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "pIsSessionHdl"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 182
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISSession_CancelSession(I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QISSession_GetFileType(I)Ljava/lang/String;
    .locals 1
    .param p1, "pIsSessionHdl"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISSession_GetFileType(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public QISSession_GetFilename(I)Ljava/lang/String;
    .locals 1
    .param p1, "pIsSessionHdl"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISSession_GetFilename(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public QISSession_GetFilesize(I)J
    .locals 2
    .param p1, "pIsSessionHdl"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISSession_GetFilesize(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public QISSession_GetFilesizeRemaining(I)J
    .locals 2
    .param p1, "pIsSessionHdl"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISSession_GetFilesizeRemaining(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public QISSession_GetRemoteContact(I)Ljava/lang/String;
    .locals 1
    .param p1, "pIsSessionHdl"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISSession_GetRemoteContact(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public QISSession_GetSessionID(I)Ljava/lang/String;
    .locals 1
    .param p1, "pIsSessionHdl"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISSession_GetSessionID(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public QISSession_GetSessionState(I)Lcom/qualcomm/qti/is/ISSessionState;
    .locals 1
    .param p1, "pIsSessionHdl"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISSession_GetSessionState(I)Lcom/qualcomm/qti/is/ISSessionState;

    move-result-object v0

    return-object v0
.end method

.method public QISSession_RejectSession(I)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "pIsSessionHdl"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 177
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISSession_RejectSession(I)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public QISSession_RemoveSessionListener(ILcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;
    .locals 1
    .param p1, "pIsSessionHdl"    # I
    .param p2, "pISSessionListenerHdl"    # Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lcom/qualcomm/qti/is/QISService$1;->nativeISImpl:Lcom/qualcomm/qti/is/QISService$NativeISImpl;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->QISSession_RemoveSessionListener(ILcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;

    move-result-object v0

    return-object v0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 203
    :try_start_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/qualcomm/qti/is/IQISService$Stub;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 205
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v1, "MyClass"

    const-string v2, "Unexpected remote exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 207
    throw v0
.end method
