.class public final enum Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;
.super Ljava/lang/Enum;
.source "ImageShareError.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/is/ImageShareError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QRCS_IS_ERROR"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

.field public static final enum QRCS_IS_ERROR_EXCEED_MAX_SIZE:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

.field public static final enum QRCS_IS_ERROR_EXCEED_WARN_SIZE:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

.field public static final enum QRCS_IS_ERROR_FILE_NOT_PRESENT:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

.field public static final enum QRCS_IS_ERROR_MAX:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

.field public static final enum QRCS_IS_ERROR_NETWORK_LOST:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

.field public static final enum QRCS_IS_ERROR_UNKNOWN:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

.field public static final enum QRCS_IS_ERROR_USER_CANCEL:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

.field public static final enum QRCS_IS_UNKNOWN_ERROR:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    const-string v1, "QRCS_IS_ERROR_UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_UNKNOWN:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    .line 23
    new-instance v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    const-string v1, "QRCS_IS_ERROR_USER_CANCEL"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_USER_CANCEL:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    .line 25
    new-instance v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    const-string v1, "QRCS_IS_ERROR_NETWORK_LOST"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_NETWORK_LOST:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    .line 27
    new-instance v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    const-string v1, "QRCS_IS_ERROR_FILE_NOT_PRESENT"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_FILE_NOT_PRESENT:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    .line 29
    new-instance v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    const-string v1, "QRCS_IS_ERROR_EXCEED_MAX_SIZE"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_EXCEED_MAX_SIZE:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    .line 31
    new-instance v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    const-string v1, "QRCS_IS_ERROR_EXCEED_WARN_SIZE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_EXCEED_WARN_SIZE:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    .line 33
    new-instance v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    const-string v1, "QRCS_IS_ERROR_MAX"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_MAX:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    .line 34
    new-instance v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    const-string v1, "QRCS_IS_UNKNOWN_ERROR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_UNKNOWN_ERROR:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    .line 20
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    sget-object v1, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_UNKNOWN:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_USER_CANCEL:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_NETWORK_LOST:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_FILE_NOT_PRESENT:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_EXCEED_MAX_SIZE:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_EXCEED_WARN_SIZE:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_MAX:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_UNKNOWN_ERROR:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->$VALUES:[Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->$VALUES:[Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    return-object v0
.end method
