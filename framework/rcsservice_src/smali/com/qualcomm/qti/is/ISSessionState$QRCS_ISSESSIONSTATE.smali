.class public final enum Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;
.super Ljava/lang/Enum;
.source "ISSessionState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/is/ISSessionState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "QRCS_ISSESSIONSTATE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

.field public static final enum QRCS_IMAGE_SHARE_SESSION_STATE_CANCELLED:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

.field public static final enum QRCS_IMAGE_SHARE_SESSION_STATE_CONNECTING:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

.field public static final enum QRCS_IMAGE_SHARE_SESSION_STATE_ESTABLISHED:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

.field public static final enum QRCS_IMAGE_SHARE_SESSION_STATE_INIT:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

.field public static final enum QRCS_IMAGE_SHARE_SESSION_STATE_PENDING:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

.field public static final enum QRCS_IMAGE_SHARE_SESSION_STATE_TERMINATED:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

.field public static final enum QRCS_IMAGE_SHARE_SESSION_STATE_TERMINATING:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

.field public static final enum QRCS_IMAGE_SHARE_SESSION_STATE_UNKNOWN:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

.field public static final enum QRCS_IMAGE_SHARE_SESSION_UNKNOWN:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    const-string v1, "QRCS_IMAGE_SHARE_SESSION_STATE_UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_UNKNOWN:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    .line 26
    new-instance v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    const-string v1, "QRCS_IMAGE_SHARE_SESSION_STATE_INIT"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_INIT:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    .line 31
    new-instance v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    const-string v1, "QRCS_IMAGE_SHARE_SESSION_STATE_PENDING"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_PENDING:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    .line 33
    new-instance v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    const-string v1, "QRCS_IMAGE_SHARE_SESSION_STATE_CONNECTING"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_CONNECTING:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    .line 35
    new-instance v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    const-string v1, "QRCS_IMAGE_SHARE_SESSION_STATE_ESTABLISHED"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_ESTABLISHED:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    .line 37
    new-instance v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    const-string v1, "QRCS_IMAGE_SHARE_SESSION_STATE_CANCELLED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_CANCELLED:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    .line 39
    new-instance v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    const-string v1, "QRCS_IMAGE_SHARE_SESSION_STATE_TERMINATING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_TERMINATING:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    .line 41
    new-instance v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    const-string v1, "QRCS_IMAGE_SHARE_SESSION_STATE_TERMINATED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_TERMINATED:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    .line 46
    new-instance v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    const-string v1, "QRCS_IMAGE_SHARE_SESSION_UNKNOWN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_UNKNOWN:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    .line 20
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    sget-object v1, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_UNKNOWN:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_INIT:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_PENDING:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_CONNECTING:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_ESTABLISHED:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_CANCELLED:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_TERMINATING:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_STATE_TERMINATED:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->QRCS_IMAGE_SHARE_SESSION_UNKNOWN:Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->$VALUES:[Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->$VALUES:[Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/is/ISSessionState$QRCS_ISSESSIONSTATE;

    return-object v0
.end method
