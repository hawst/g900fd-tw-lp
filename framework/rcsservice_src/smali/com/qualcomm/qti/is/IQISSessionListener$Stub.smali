.class public abstract Lcom/qualcomm/qti/is/IQISSessionListener$Stub;
.super Landroid/os/Binder;
.source "IQISSessionListener.java"

# interfaces
.implements Lcom/qualcomm/qti/is/IQISSessionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/is/IQISSessionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/is/IQISSessionListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.is.IQISSessionListener"

.field static final TRANSACTION_IQISSession_HandleSessionAborted:I = 0x2

.field static final TRANSACTION_IQISSession_HandleSessionStarted:I = 0x1

.field static final TRANSACTION_IQISSession_HandleSessionTerminatedByRemote:I = 0x3

.field static final TRANSACTION_IQISSession_HandleTransferError:I = 0x5

.field static final TRANSACTION_IQISSession_HandleTransferProgress:I = 0x4

.field static final TRANSACTION_IQISSession_HandleTransferSuccess:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.is.IQISSessionListener"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/is/IQISSessionListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/is/IQISSessionListener;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.is.IQISSessionListener"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/is/IQISSessionListener;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/is/IQISSessionListener;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/is/IQISSessionListener$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/is/IQISSessionListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 170
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 44
    :sswitch_0
    const-string v1, "com.qualcomm.qti.is.IQISSessionListener"

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v1, "com.qualcomm.qti.is.IQISSessionListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 53
    .local v2, "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 54
    .local v4, "_arg1":I
    invoke-virtual {p0, v2, v3, v4}, Lcom/qualcomm/qti/is/IQISSessionListener$Stub;->IQISSession_HandleSessionStarted(JI)V

    .line 55
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 60
    .end local v2    # "_arg0":J
    .end local v4    # "_arg1":I
    :sswitch_2
    const-string v1, "com.qualcomm.qti.is.IQISSessionListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 64
    .restart local v2    # "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 65
    .restart local v4    # "_arg1":I
    invoke-virtual {p0, v2, v3, v4}, Lcom/qualcomm/qti/is/IQISSessionListener$Stub;->IQISSession_HandleSessionAborted(JI)V

    .line 66
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 71
    .end local v2    # "_arg0":J
    .end local v4    # "_arg1":I
    :sswitch_3
    const-string v1, "com.qualcomm.qti.is.IQISSessionListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 75
    .restart local v2    # "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 76
    .restart local v4    # "_arg1":I
    invoke-virtual {p0, v2, v3, v4}, Lcom/qualcomm/qti/is/IQISSessionListener$Stub;->IQISSession_HandleSessionTerminatedByRemote(JI)V

    .line 77
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 82
    .end local v2    # "_arg0":J
    .end local v4    # "_arg1":I
    :sswitch_4
    const-string v1, "com.qualcomm.qti.is.IQISSessionListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 86
    .restart local v2    # "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 88
    .restart local v4    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    sget-object v1, Lcom/qualcomm/qti/rcsservice/QRCSLong;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/qualcomm/qti/rcsservice/QRCSLong;

    .line 95
    .local v5, "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_1

    .line 96
    sget-object v1, Lcom/qualcomm/qti/rcsservice/QRCSLong;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/qualcomm/qti/rcsservice/QRCSLong;

    .local v6, "_arg3":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    :goto_2
    move-object v1, p0

    .line 101
    invoke-virtual/range {v1 .. v6}, Lcom/qualcomm/qti/is/IQISSessionListener$Stub;->IQISSession_HandleTransferProgress(JILcom/qualcomm/qti/rcsservice/QRCSLong;Lcom/qualcomm/qti/rcsservice/QRCSLong;)V

    .line 102
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 103
    if-eqz v5, :cond_2

    .line 104
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 105
    invoke-virtual {v5, p3, v0}, Lcom/qualcomm/qti/rcsservice/QRCSLong;->writeToParcel(Landroid/os/Parcel;I)V

    .line 110
    :goto_3
    if-eqz v6, :cond_3

    .line 111
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    invoke-virtual {v6, p3, v0}, Lcom/qualcomm/qti/rcsservice/QRCSLong;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 92
    .end local v5    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v6    # "_arg3":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    :cond_0
    const/4 v5, 0x0

    .restart local v5    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    goto :goto_1

    .line 99
    :cond_1
    const/4 v6, 0x0

    .restart local v6    # "_arg3":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    goto :goto_2

    .line 108
    :cond_2
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3

    .line 115
    :cond_3
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 121
    .end local v2    # "_arg0":J
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    .end local v6    # "_arg3":Lcom/qualcomm/qti/rcsservice/QRCSLong;
    :sswitch_5
    const-string v1, "com.qualcomm.qti.is.IQISSessionListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 123
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 125
    .restart local v2    # "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 127
    .restart local v4    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_4

    .line 128
    sget-object v1, Lcom/qualcomm/qti/is/ImageShareError;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/qualcomm/qti/is/ImageShareError;

    .line 133
    .local v5, "_arg2":Lcom/qualcomm/qti/is/ImageShareError;
    :goto_4
    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/qualcomm/qti/is/IQISSessionListener$Stub;->IQISSession_HandleTransferError(JILcom/qualcomm/qti/is/ImageShareError;)V

    .line 134
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 135
    if-eqz v5, :cond_5

    .line 136
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 137
    invoke-virtual {v5, p3, v0}, Lcom/qualcomm/qti/is/ImageShareError;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 131
    .end local v5    # "_arg2":Lcom/qualcomm/qti/is/ImageShareError;
    :cond_4
    const/4 v5, 0x0

    .restart local v5    # "_arg2":Lcom/qualcomm/qti/is/ImageShareError;
    goto :goto_4

    .line 140
    :cond_5
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 146
    .end local v2    # "_arg0":J
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":Lcom/qualcomm/qti/is/ImageShareError;
    :sswitch_6
    const-string v1, "com.qualcomm.qti.is.IQISSessionListener"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 148
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 150
    .restart local v2    # "_arg0":J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 152
    .restart local v4    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_6

    .line 153
    sget-object v1, Lcom/qualcomm/qti/rcsservice/QRCSString;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/qualcomm/qti/rcsservice/QRCSString;

    .line 158
    .local v5, "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSString;
    :goto_5
    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/qualcomm/qti/is/IQISSessionListener$Stub;->IQISSession_HandleTransferSuccess(JILcom/qualcomm/qti/rcsservice/QRCSString;)V

    .line 159
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 160
    if-eqz v5, :cond_7

    .line 161
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 162
    invoke-virtual {v5, p3, v0}, Lcom/qualcomm/qti/rcsservice/QRCSString;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 156
    .end local v5    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSString;
    :cond_6
    const/4 v5, 0x0

    .restart local v5    # "_arg2":Lcom/qualcomm/qti/rcsservice/QRCSString;
    goto :goto_5

    .line 165
    :cond_7
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 40
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
