.class public Lcom/qualcomm/qti/is/ImageShareError;
.super Ljava/lang/Object;
.source "ImageShareError.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/is/ImageShareError;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private eISError:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 135
    new-instance v0, Lcom/qualcomm/qti/is/ImageShareError$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/is/ImageShareError$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/is/ImageShareError;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/is/ImageShareError;->readFromParcel(Landroid/os/Parcel;)V

    .line 150
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/is/ImageShareError$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/is/ImageShareError$1;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/is/ImageShareError;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getStatusCodeInstance()Lcom/qualcomm/qti/is/ImageShareError;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/qualcomm/qti/is/ImageShareError;

    invoke-direct {v0}, Lcom/qualcomm/qti/is/ImageShareError;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/qualcomm/qti/is/ImageShareError;->eISError:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 133
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/is/ImageShareError;->eISError:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    invoke-virtual {v0}, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return v0
.end method

.method public getISError()Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/qualcomm/qti/is/ImageShareError;->eISError:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 155
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/is/ImageShareError;->eISError:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :goto_0
    return-void

    .line 156
    :catch_0
    move-exception v0

    .line 157
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_UNKNOWN_ERROR:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    iput-object v1, p0, Lcom/qualcomm/qti/is/ImageShareError;->eISError:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    goto :goto_0
.end method

.method public setISError(I)V
    .locals 3
    .param p1, "nISError"    # I

    .prologue
    .line 79
    sparse-switch p1, :sswitch_data_0

    .line 110
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/qualcomm/qti/is/ImageShareError;->eISError:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    sget-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_UNKNOWN_ERROR:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ImageShareError;->eISError:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    .line 114
    :goto_0
    return-void

    .line 82
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_UNKNOWN:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ImageShareError;->eISError:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    goto :goto_0

    .line 86
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_USER_CANCEL:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ImageShareError;->eISError:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    goto :goto_0

    .line 90
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_NETWORK_LOST:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ImageShareError;->eISError:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    goto :goto_0

    .line 94
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_FILE_NOT_PRESENT:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ImageShareError;->eISError:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    goto :goto_0

    .line 98
    :sswitch_4
    sget-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_EXCEED_MAX_SIZE:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ImageShareError;->eISError:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    goto :goto_0

    .line 102
    :sswitch_5
    sget-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_EXCEED_WARN_SIZE:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ImageShareError;->eISError:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    goto :goto_0

    .line 106
    :sswitch_6
    sget-object v0, Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;->QRCS_IS_ERROR_MAX:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    iput-object v0, p0, Lcom/qualcomm/qti/is/ImageShareError;->eISError:Lcom/qualcomm/qti/is/ImageShareError$QRCS_IS_ERROR;

    goto :goto_0

    .line 79
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_5
        0x10000000 -> :sswitch_6
    .end sparse-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/is/ImageShareError;->writeToParcel(Landroid/os/Parcel;)V

    .line 128
    return-void
.end method
