.class public Lcom/qualcomm/qti/is/QISService$NativeISImpl;
.super Ljava/lang/Object;
.source "QISService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/is/QISService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "NativeISImpl"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/is/QISService;


# direct methods
.method public constructor <init>(Lcom/qualcomm/qti/is/QISService;)V
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Lcom/qualcomm/qti/is/QISService$NativeISImpl;->this$0:Lcom/qualcomm/qti/is/QISService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native QISService_AddListener(ILcom/qualcomm/qti/is/IQISServiceListener;Lcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QISService_FreeSessionList(I[Lcom/qualcomm/qti/rcsservice/QRCSInt;)V
.end method

.method public native QISService_GetActiveImageShareSessions(I[Lcom/qualcomm/qti/rcsservice/QRCSInt;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QISService_GetImageShareSessionWithContact(ILjava/lang/String;[Lcom/qualcomm/qti/rcsservice/QRCSInt;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QISService_GetVersion(ILcom/qualcomm/qti/rcsservice/QRCSString;Lcom/qualcomm/qti/rcsservice/QRCSInt;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QISService_RemoveListener(ILcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QISService_TransferFile(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native QISSession_AcceptSession(I)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QISSession_AddSessionListener(ILcom/qualcomm/qti/is/IQISSessionListener;Lcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QISSession_CancelSession(I)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QISSession_GetFileType(I)Ljava/lang/String;
.end method

.method public native QISSession_GetFilename(I)Ljava/lang/String;
.end method

.method public native QISSession_GetFilesize(I)J
.end method

.method public native QISSession_GetFilesizeRemaining(I)J
.end method

.method public native QISSession_GetRemoteContact(I)Ljava/lang/String;
.end method

.method public native QISSession_GetSessionID(I)Ljava/lang/String;
.end method

.method public native QISSession_GetSessionState(I)Lcom/qualcomm/qti/is/ISSessionState;
.end method

.method public native QISSession_RejectSession(I)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method

.method public native QISSession_RemoveSessionListener(ILcom/qualcomm/qti/rcsservice/QRCSLong;)Lcom/qualcomm/qti/rcsservice/StatusCode;
.end method
