.class public final enum Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PreferredConnectivity"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;

.field public static final enum P2P:Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;

.field public static final enum TDLS:Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 500
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;

    const-string v1, "P2P"

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;->P2P:Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;

    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;

    const-string v1, "TDLS"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;->TDLS:Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;

    .line 499
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;->P2P:Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;->TDLS:Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;

    aput-object v1, v0, v3

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 499
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 499
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;
    .locals 1

    .prologue
    .line 499
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;

    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;

    return-object v0
.end method
