.class public final enum Lcom/qualcomm/wfd/WfdEnums$VideoFormat;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VideoFormat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$VideoFormat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

.field public static final enum WFD_VIDEO_3D:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

.field public static final enum WFD_VIDEO_H264:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

.field public static final enum WFD_VIDEO_INVALID:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

.field public static final enum WFD_VIDEO_UNK:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 452
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    const-string v1, "WFD_VIDEO_UNK"

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_UNK:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    .line 453
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    const-string v1, "WFD_VIDEO_H264"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_H264:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    .line 454
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    const-string v1, "WFD_VIDEO_3D"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_3D:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    .line 455
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    const-string v1, "WFD_VIDEO_INVALID"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_INVALID:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    .line 451
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_UNK:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_H264:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_3D:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->WFD_VIDEO_INVALID:Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    aput-object v1, v0, v5

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 451
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$VideoFormat;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 451
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$VideoFormat;
    .locals 1

    .prologue
    .line 451
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$VideoFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$VideoFormat;

    return-object v0
.end method
