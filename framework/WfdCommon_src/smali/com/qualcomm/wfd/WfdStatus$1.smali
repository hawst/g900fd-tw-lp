.class final Lcom/qualcomm/wfd/WfdStatus$1;
.super Ljava/lang/Object;
.source "WfdStatus.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/qualcomm/wfd/WfdStatus;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/qualcomm/wfd/WfdStatus;
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 31
    new-instance v0, Lcom/qualcomm/wfd/WfdStatus;

    invoke-direct {v0}, Lcom/qualcomm/wfd/WfdStatus;-><init>()V

    .line 32
    .local v0, "ret":Lcom/qualcomm/wfd/WfdStatus;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/qualcomm/wfd/WfdStatus;->state:I

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/qualcomm/wfd/WfdStatus;->sessionId:I

    .line 34
    const-class v1, Lcom/qualcomm/wfd/WfdDevice;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/wfd/WfdDevice;

    iput-object v1, v0, Lcom/qualcomm/wfd/WfdStatus;->connectedDevice:Lcom/qualcomm/wfd/WfdDevice;

    .line 35
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/qualcomm/wfd/WfdStatus$1;->createFromParcel(Landroid/os/Parcel;)Lcom/qualcomm/wfd/WfdStatus;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/qualcomm/wfd/WfdStatus;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 40
    new-array v0, p1, [Lcom/qualcomm/wfd/WfdStatus;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/qualcomm/wfd/WfdStatus$1;->newArray(I)[Lcom/qualcomm/wfd/WfdStatus;

    move-result-object v0

    return-object v0
.end method
