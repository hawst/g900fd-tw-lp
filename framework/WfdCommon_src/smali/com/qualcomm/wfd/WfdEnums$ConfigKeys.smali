.class public final enum Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConfigKeys"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum AUDIO_AV_SYNC_DEL:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum AUDIO_IN_SUSPEND:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum CYCLIC_IR:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum CYCLIC_IR_NUM_MACRO_BLK:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum DISABLE_AVSYNC_MODE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum DISABLE_NALU_FILLER:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum DYN_BIT_ADAP:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum ENABLE_AUDIO_TRACK_LATENCY_MODE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum ENCRYPT_AUDIO_DECISION:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum ENCRYPT_NON_SECURE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum HDCP_ENFORCED:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum MAX_FPS_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum PERF_LEVEL_PERF_MODE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum PERF_LEVEL_TURBO_MODE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum RTP_DUMP_ENABLE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum TOTAL_CFG_KEYS:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum UIBC_M14:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum UIBC_VALID:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

.field public static final enum VIDEO_PKTLOSS_FRAME_DROP_MODE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 593
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "AUDIO_AV_SYNC_DEL"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->AUDIO_AV_SYNC_DEL:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 594
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "AUDIO_IN_SUSPEND"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->AUDIO_IN_SUSPEND:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 595
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "CYCLIC_IR"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->CYCLIC_IR:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 596
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "CYCLIC_IR_NUM_MACRO_BLK"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->CYCLIC_IR_NUM_MACRO_BLK:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 597
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "DISABLE_NALU_FILLER"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->DISABLE_NALU_FILLER:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 598
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "DYN_BIT_ADAP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->DYN_BIT_ADAP:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 599
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "ENCRYPT_AUDIO_DECISION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->ENCRYPT_AUDIO_DECISION:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 600
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "ENCRYPT_NON_SECURE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->ENCRYPT_NON_SECURE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 601
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "HDCP_ENFORCED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->HDCP_ENFORCED:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 602
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "PERF_LEVEL_PERF_MODE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->PERF_LEVEL_PERF_MODE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 603
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "PERF_LEVEL_TURBO_MODE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->PERF_LEVEL_TURBO_MODE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 604
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "RTP_DUMP_ENABLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->RTP_DUMP_ENABLE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 605
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "UIBC_M14"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->UIBC_M14:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 606
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "UIBC_VALID"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->UIBC_VALID:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 607
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "VIDEO_PKTLOSS_FRAME_DROP_MODE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->VIDEO_PKTLOSS_FRAME_DROP_MODE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 608
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "DISABLE_AVSYNC_MODE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->DISABLE_AVSYNC_MODE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 609
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "MAX_FPS_SUPPORTED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->MAX_FPS_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 610
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "ENABLE_AUDIO_TRACK_LATENCY_MODE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->ENABLE_AUDIO_TRACK_LATENCY_MODE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 611
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    const-string v1, "TOTAL_CFG_KEYS"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->TOTAL_CFG_KEYS:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    .line 592
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->AUDIO_AV_SYNC_DEL:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->AUDIO_IN_SUSPEND:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->CYCLIC_IR:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->CYCLIC_IR_NUM_MACRO_BLK:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->DISABLE_NALU_FILLER:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->DYN_BIT_ADAP:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->ENCRYPT_AUDIO_DECISION:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->ENCRYPT_NON_SECURE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->HDCP_ENFORCED:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->PERF_LEVEL_PERF_MODE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->PERF_LEVEL_TURBO_MODE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->RTP_DUMP_ENABLE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->UIBC_M14:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->UIBC_VALID:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->VIDEO_PKTLOSS_FRAME_DROP_MODE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->DISABLE_AVSYNC_MODE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->MAX_FPS_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->ENABLE_AUDIO_TRACK_LATENCY_MODE:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->TOTAL_CFG_KEYS:Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 592
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 592
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;
    .locals 1

    .prologue
    .line 592
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;

    return-object v0
.end method
