.class final enum Lcom/qualcomm/wfd/WfdOperation;
.super Ljava/lang/Enum;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdOperation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdOperation;

.field public static final enum PAUSE:Lcom/qualcomm/wfd/WfdOperation;

.field public static final enum PLAY:Lcom/qualcomm/wfd/WfdOperation;

.field public static final enum RESUME:Lcom/qualcomm/wfd/WfdOperation;

.field public static final enum STANDBY:Lcom/qualcomm/wfd/WfdOperation;

.field public static final enum START_UIBC:Lcom/qualcomm/wfd/WfdOperation;

.field public static final enum STOP_UIBC:Lcom/qualcomm/wfd/WfdOperation;

.field public static final enum TEARDOWN:Lcom/qualcomm/wfd/WfdOperation;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 62
    new-instance v0, Lcom/qualcomm/wfd/WfdOperation;

    const-string v1, "PLAY"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->PLAY:Lcom/qualcomm/wfd/WfdOperation;

    .line 63
    new-instance v0, Lcom/qualcomm/wfd/WfdOperation;

    const-string v1, "PAUSE"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->PAUSE:Lcom/qualcomm/wfd/WfdOperation;

    .line 64
    new-instance v0, Lcom/qualcomm/wfd/WfdOperation;

    const-string v1, "STANDBY"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WfdOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->STANDBY:Lcom/qualcomm/wfd/WfdOperation;

    .line 65
    new-instance v0, Lcom/qualcomm/wfd/WfdOperation;

    const-string v1, "RESUME"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/wfd/WfdOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->RESUME:Lcom/qualcomm/wfd/WfdOperation;

    .line 66
    new-instance v0, Lcom/qualcomm/wfd/WfdOperation;

    const-string v1, "START_UIBC"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/wfd/WfdOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->START_UIBC:Lcom/qualcomm/wfd/WfdOperation;

    .line 67
    new-instance v0, Lcom/qualcomm/wfd/WfdOperation;

    const-string v1, "STOP_UIBC"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->STOP_UIBC:Lcom/qualcomm/wfd/WfdOperation;

    .line 68
    new-instance v0, Lcom/qualcomm/wfd/WfdOperation;

    const-string v1, "TEARDOWN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdOperation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->TEARDOWN:Lcom/qualcomm/wfd/WfdOperation;

    .line 61
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/qualcomm/wfd/WfdOperation;

    sget-object v1, Lcom/qualcomm/wfd/WfdOperation;->PLAY:Lcom/qualcomm/wfd/WfdOperation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/wfd/WfdOperation;->PAUSE:Lcom/qualcomm/wfd/WfdOperation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/wfd/WfdOperation;->STANDBY:Lcom/qualcomm/wfd/WfdOperation;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/wfd/WfdOperation;->RESUME:Lcom/qualcomm/wfd/WfdOperation;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/wfd/WfdOperation;->START_UIBC:Lcom/qualcomm/wfd/WfdOperation;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/wfd/WfdOperation;->STOP_UIBC:Lcom/qualcomm/wfd/WfdOperation;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/wfd/WfdOperation;->TEARDOWN:Lcom/qualcomm/wfd/WfdOperation;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/wfd/WfdOperation;->$VALUES:[Lcom/qualcomm/wfd/WfdOperation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdOperation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 61
    const-class v0, Lcom/qualcomm/wfd/WfdOperation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/wfd/WfdOperation;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdOperation;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/qualcomm/wfd/WfdOperation;->$VALUES:[Lcom/qualcomm/wfd/WfdOperation;

    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdOperation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/wfd/WfdOperation;

    return-object v0
.end method
