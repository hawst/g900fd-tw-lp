.class final enum Lcom/qualcomm/wfd/WFDState;
.super Ljava/lang/Enum;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WFDState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WFDState;

.field public static final enum BINDING:Lcom/qualcomm/wfd/WFDState;

.field public static final enum BOUND:Lcom/qualcomm/wfd/WFDState;

.field public static final enum DEINIT:Lcom/qualcomm/wfd/WFDState;

.field public static final enum ESTABLISHED:Lcom/qualcomm/wfd/WFDState;

.field public static final enum ESTABLISHING:Lcom/qualcomm/wfd/WFDState;

.field public static final enum INITIALIZED:Lcom/qualcomm/wfd/WFDState;

.field public static final enum INITIALIZING:Lcom/qualcomm/wfd/WFDState;

.field public static final enum PLAY:Lcom/qualcomm/wfd/WFDState;

.field public static final enum PLAYING:Lcom/qualcomm/wfd/WFDState;

.field public static final enum TEARDOWN:Lcom/qualcomm/wfd/WFDState;

.field public static final enum TEARINGDOWN:Lcom/qualcomm/wfd/WFDState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 79
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    const-string v1, "DEINIT"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WFDState;->DEINIT:Lcom/qualcomm/wfd/WFDState;

    .line 80
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    const-string v1, "BINDING"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WFDState;->BINDING:Lcom/qualcomm/wfd/WFDState;

    .line 81
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    const-string v1, "BOUND"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WFDState;->BOUND:Lcom/qualcomm/wfd/WFDState;

    .line 82
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    const-string v1, "INITIALIZING"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WFDState;->INITIALIZING:Lcom/qualcomm/wfd/WFDState;

    .line 83
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    const-string v1, "INITIALIZED"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WFDState;->INITIALIZED:Lcom/qualcomm/wfd/WFDState;

    .line 84
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    const-string v1, "ESTABLISHING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WFDState;->ESTABLISHING:Lcom/qualcomm/wfd/WFDState;

    .line 85
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    const-string v1, "ESTABLISHED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WFDState;->ESTABLISHED:Lcom/qualcomm/wfd/WFDState;

    .line 86
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    const-string v1, "PLAY"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WFDState;->PLAY:Lcom/qualcomm/wfd/WFDState;

    .line 87
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    const-string v1, "PLAYING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WFDState;->PLAYING:Lcom/qualcomm/wfd/WFDState;

    .line 88
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    const-string v1, "TEARINGDOWN"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WFDState;->TEARINGDOWN:Lcom/qualcomm/wfd/WFDState;

    .line 89
    new-instance v0, Lcom/qualcomm/wfd/WFDState;

    const-string v1, "TEARDOWN"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WFDState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WFDState;->TEARDOWN:Lcom/qualcomm/wfd/WFDState;

    .line 78
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/qualcomm/wfd/WFDState;

    sget-object v1, Lcom/qualcomm/wfd/WFDState;->DEINIT:Lcom/qualcomm/wfd/WFDState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/wfd/WFDState;->BINDING:Lcom/qualcomm/wfd/WFDState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/wfd/WFDState;->BOUND:Lcom/qualcomm/wfd/WFDState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/wfd/WFDState;->INITIALIZING:Lcom/qualcomm/wfd/WFDState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/wfd/WFDState;->INITIALIZED:Lcom/qualcomm/wfd/WFDState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/wfd/WFDState;->ESTABLISHING:Lcom/qualcomm/wfd/WFDState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/wfd/WFDState;->ESTABLISHED:Lcom/qualcomm/wfd/WFDState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/qualcomm/wfd/WFDState;->PLAY:Lcom/qualcomm/wfd/WFDState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/qualcomm/wfd/WFDState;->PLAYING:Lcom/qualcomm/wfd/WFDState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/qualcomm/wfd/WFDState;->TEARINGDOWN:Lcom/qualcomm/wfd/WFDState;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/qualcomm/wfd/WFDState;->TEARDOWN:Lcom/qualcomm/wfd/WFDState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/wfd/WFDState;->$VALUES:[Lcom/qualcomm/wfd/WFDState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WFDState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 78
    const-class v0, Lcom/qualcomm/wfd/WFDState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/wfd/WFDState;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WFDState;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/qualcomm/wfd/WFDState;->$VALUES:[Lcom/qualcomm/wfd/WFDState;

    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WFDState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/wfd/WFDState;

    return-object v0
.end method
