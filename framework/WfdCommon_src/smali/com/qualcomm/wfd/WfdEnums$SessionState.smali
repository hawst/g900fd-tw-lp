.class public final enum Lcom/qualcomm/wfd/WfdEnums$SessionState;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SessionState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$SessionState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum ESTABLISHED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum IDLE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum PAUSING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum PLAYING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum STANDBY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum STANDING_BY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum TEARDOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

.field public static final enum TEARING_DOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 459
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    .line 460
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    const-string v1, "INITIALIZED"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    .line 461
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->IDLE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    .line 462
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    const-string v1, "PLAY"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    .line 463
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    const-string v1, "PAUSE"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    .line 464
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    const-string v1, "ESTABLISHED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ESTABLISHED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    .line 465
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    const-string v1, "TEARDOWN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARDOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    .line 466
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    const-string v1, "PLAYING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAYING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    .line 467
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    const-string v1, "PAUSING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    .line 468
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    const-string v1, "STANDBY"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDBY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    .line 469
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    const-string v1, "STANDING_BY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDING_BY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    .line 470
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    const-string v1, "TEARING_DOWN"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$SessionState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARING_DOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    .line 458
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$SessionState;

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->IDLE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ESTABLISHED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARDOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PLAYING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->PAUSING:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDBY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->STANDING_BY:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$SessionState;->TEARING_DOWN:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$SessionState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 458
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$SessionState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 458
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$SessionState;
    .locals 1

    .prologue
    .line 458
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$SessionState;

    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$SessionState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$SessionState;

    return-object v0
.end method
