.class public final enum Lcom/qualcomm/wfd/WfdEnums$CapabilityType;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CapabilityType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$CapabilityType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_3D_VIDEO_FORMATS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_AUDIO_CODECS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_CEA_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_CONTENT_PROTECTION_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_COUPLED_SINK:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_COUPLED_SINK_SUPPORTED_BY_SINK:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_COUPLED_SINK_SUPPORTED_BY_SOURCE:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_DISPLAY_EDID:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_HH_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_I2C:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_SERVICE_DISCOVERY_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_STANDBY_RESUME_CAPABILITY:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_TIME_SYNC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_UIBC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_VESA_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

.field public static final enum WFD_VIDEO_FORMATS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 417
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_AUDIO_CODECS"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_AUDIO_CODECS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 418
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_VIDEO_FORMATS"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_VIDEO_FORMATS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 419
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_3D_VIDEO_FORMATS"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_3D_VIDEO_FORMATS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 420
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_CEA_RESOLUTIONS_BITMAP"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_CEA_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 421
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_VESA_RESOLUTIONS_BITMAP"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_VESA_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 422
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_HH_RESOLUTIONS_BITMAP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_HH_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 423
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_DISPLAY_EDID"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_DISPLAY_EDID:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 424
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_COUPLED_SINK"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_COUPLED_SINK:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 425
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_I2C"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_I2C:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 426
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_UIBC_SUPPORTED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_UIBC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 427
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_STANDBY_RESUME_CAPABILITY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_STANDBY_RESUME_CAPABILITY:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 428
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_COUPLED_SINK_SUPPORTED_BY_SOURCE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_COUPLED_SINK_SUPPORTED_BY_SOURCE:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 429
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_COUPLED_SINK_SUPPORTED_BY_SINK"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_COUPLED_SINK_SUPPORTED_BY_SINK:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 430
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_SERVICE_DISCOVERY_SUPPORTED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_SERVICE_DISCOVERY_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 431
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_CONTENT_PROTECTION_SUPPORTED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_CONTENT_PROTECTION_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 432
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    const-string v1, "WFD_TIME_SYNC_SUPPORTED"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_TIME_SYNC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    .line 416
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_AUDIO_CODECS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_VIDEO_FORMATS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_3D_VIDEO_FORMATS:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_CEA_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_VESA_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_HH_RESOLUTIONS_BITMAP:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_DISPLAY_EDID:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_COUPLED_SINK:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_I2C:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_UIBC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_STANDBY_RESUME_CAPABILITY:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_COUPLED_SINK_SUPPORTED_BY_SOURCE:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_COUPLED_SINK_SUPPORTED_BY_SINK:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_SERVICE_DISCOVERY_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_CONTENT_PROTECTION_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->WFD_TIME_SYNC_SUPPORTED:Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 416
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$CapabilityType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 416
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$CapabilityType;
    .locals 1

    .prologue
    .line 416
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$CapabilityType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$CapabilityType;

    return-object v0
.end method
