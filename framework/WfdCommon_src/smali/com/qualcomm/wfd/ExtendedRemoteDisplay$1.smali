.class Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;
.super Ljava/lang/Object;
.source "ExtendedRemoteDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->notifyDisplayConnected(Landroid/view/Surface;III)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

.field final synthetic val$flags:I

.field final synthetic val$height:I

.field final synthetic val$surface:Landroid/view/Surface;

.field final synthetic val$width:I


# direct methods
.method constructor <init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/view/Surface;III)V
    .locals 0

    .prologue
    .line 524
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    iput-object p2, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$surface:Landroid/view/Surface;

    iput p3, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$width:I

    iput p4, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$height:I

    iput p5, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$flags:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 527
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    const/4 v1, 0x1

    # setter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mConnected:Z
    invoke-static {v0, v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$002(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Z)Z

    .line 528
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mListener:Landroid/media/RemoteDisplay$Listener;
    invoke-static {v0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/media/RemoteDisplay$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$surface:Landroid/view/Surface;

    iget v2, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$width:I

    iget v3, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$height:I

    iget v4, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;->val$flags:I

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Landroid/media/RemoteDisplay$Listener;->onDisplayConnected(Landroid/view/Surface;IIII)V

    .line 529
    return-void
.end method
