.class Lcom/qualcomm/wfd/WfdActionListenerImpl;
.super Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/wfd/WfdActionListenerImpl$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ExtendedRemoteDisplay.WfdActionListenerImpl"


# instance fields
.field mHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 203
    invoke-direct {p0}, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;-><init>()V

    .line 204
    iput-object p1, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    .line 205
    return-void
.end method


# virtual methods
.method public notify(Landroid/os/Bundle;I)V
    .locals 4
    .param p1, "b"    # Landroid/os/Bundle;
    .param p2, "sessionId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 283
    if-eqz p1, :cond_0

    .line 284
    const-string v2, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    const-string v3, "Notify from WFDService"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    const-string v2, "event"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 286
    .local v0, "event":Ljava/lang/String;
    const-string v2, "MMStreamStarted"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 287
    iget-object v2, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 289
    .local v1, "messageEvent":Landroid/os/Message;
    invoke-virtual {v1, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 290
    iget-object v2, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 293
    .end local v0    # "event":Ljava/lang/String;
    .end local v1    # "messageEvent":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public notifyEvent(II)V
    .locals 0
    .param p1, "event"    # I
    .param p2, "sessionId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 279
    return-void
.end method

.method public onStateUpdate(II)V
    .locals 11
    .param p1, "newState"    # I
    .param p2, "sessionId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x4

    .line 210
    invoke-static {}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->values()[Lcom/qualcomm/wfd/WfdEnums$SessionState;

    move-result-object v8

    aget-object v7, v8, p1

    .line 211
    .local v7, "state":Lcom/qualcomm/wfd/WfdEnums$SessionState;
    sget-object v8, Lcom/qualcomm/wfd/WfdActionListenerImpl$1;->$SwitchMap$com$qualcomm$wfd$WfdEnums$SessionState:[I

    invoke-virtual {v7}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 275
    :goto_0
    return-void

    .line 213
    :pswitch_0
    const-string v8, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    const-string v9, "WfdEnums.SessionState==INITIALIZED"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    if-lez p2, :cond_0

    .line 215
    const-string v8, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    const-string v9, "WfdEnums.SessionState==INITIALIZED, sessionId > 0"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    .line 219
    .local v6, "messageTeardown":Landroid/os/Message;
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 221
    .end local v6    # "messageTeardown":Landroid/os/Message;
    :cond_0
    const-string v8, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    const-string v9, "WfdEnums.SessionState==INITIALIZED, Init callback"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    const/16 v9, 0x9

    invoke-virtual {v8, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 225
    .local v1, "messageInit":Landroid/os/Message;
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 230
    .end local v1    # "messageInit":Landroid/os/Message;
    :pswitch_1
    const-string v8, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    const-string v9, "WfdEnums.SessionState==INVALID"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    const/16 v9, 0xb

    invoke-virtual {v8, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 233
    .local v2, "messageInvalid":Landroid/os/Message;
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 237
    .end local v2    # "messageInvalid":Landroid/os/Message;
    :pswitch_2
    const-string v8, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    const-string v9, "WfdEnums.SessionState==IDLE"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 241
    :pswitch_3
    const-string v8, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    const-string v9, "WfdEnums.SessionState==PLAY"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v4

    .line 244
    .local v4, "messagePlay":Landroid/os/Message;
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 248
    .end local v4    # "messagePlay":Landroid/os/Message;
    :pswitch_4
    const-string v8, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    const-string v9, "WfdEnums.SessionState==PAUSE"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 251
    .local v3, "messagePause":Landroid/os/Message;
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 255
    .end local v3    # "messagePause":Landroid/os/Message;
    :pswitch_5
    const-string v8, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    const-string v9, "WfdEnums.SessionState = STANDING_BY"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v5

    .line 258
    .local v5, "messageStandby":Landroid/os/Message;
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 262
    .end local v5    # "messageStandby":Landroid/os/Message;
    :pswitch_6
    const-string v8, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    const-string v9, "WfdEnums.SessionState==ESTABLISHED"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 265
    .local v0, "messageEstablishedCallback":Landroid/os/Message;
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 269
    .end local v0    # "messageEstablishedCallback":Landroid/os/Message;
    :pswitch_7
    const-string v8, "ExtendedRemoteDisplay.WfdActionListenerImpl"

    const-string v9, "WfdEnums.SessionState==TEARDOWN"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    .line 272
    .restart local v6    # "messageTeardown":Landroid/os/Message;
    iget-object v8, p0, Lcom/qualcomm/wfd/WfdActionListenerImpl;->mHandler:Landroid/os/Handler;

    invoke-virtual {v8, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 211
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
