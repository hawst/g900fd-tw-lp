.class public final enum Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AVPlaybackMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

.field public static final enum AUDIO_ONLY:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

.field public static final enum AUDIO_VIDEO:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

.field public static final enum NO_AUDIO_VIDEO:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

.field public static final enum VIDEO_ONLY:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 437
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    const-string v1, "NO_AUDIO_VIDEO"

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->NO_AUDIO_VIDEO:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    .line 438
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    const-string v1, "AUDIO_ONLY"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->AUDIO_ONLY:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    .line 439
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    const-string v1, "VIDEO_ONLY"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->VIDEO_ONLY:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    .line 440
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    const-string v1, "AUDIO_VIDEO"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->AUDIO_VIDEO:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    .line 436
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->NO_AUDIO_VIDEO:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->AUDIO_ONLY:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->VIDEO_ONLY:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->AUDIO_VIDEO:Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 436
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 436
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;
    .locals 1

    .prologue
    .line 436
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;

    return-object v0
.end method
