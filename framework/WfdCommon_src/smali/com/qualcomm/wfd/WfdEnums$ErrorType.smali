.class public final enum Lcom/qualcomm/wfd/WfdEnums$ErrorType;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ErrorType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$ErrorType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum ALREADY_INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum INCORRECT_STATE_FOR_OPERATION:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum NOT_INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum NOT_SINK_DEVICE:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum NOT_SOURCE_DEVICE:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum OPERATION_TIMED_OUT:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum SESSION_IN_PROGRESS:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum UIBC_ALREADY_ENABLED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum UIBC_NOT_ENABLED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

.field public static final enum UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$ErrorType;


# instance fields
.field private final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 557
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    .line 558
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    const-string v1, "INVALID_ARG"

    const/4 v2, -0x2

    invoke-direct {v0, v1, v5, v2}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    .line 559
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    const-string v1, "HDMI_CABLE_CONNECTED"

    const/4 v2, -0x3

    invoke-direct {v0, v1, v6, v2}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    .line 560
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    const-string v1, "OPERATION_TIMED_OUT"

    const/4 v2, -0x4

    invoke-direct {v0, v1, v7, v2}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->OPERATION_TIMED_OUT:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    .line 561
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    const-string v1, "ALREADY_INITIALIZED"

    const/16 v2, -0xa

    invoke-direct {v0, v1, v8, v2}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->ALREADY_INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    .line 562
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    const-string v1, "NOT_INITIALIZED"

    const/4 v2, 0x5

    const/16 v3, -0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->NOT_INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    .line 563
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    const-string v1, "SESSION_IN_PROGRESS"

    const/4 v2, 0x6

    const/16 v3, -0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->SESSION_IN_PROGRESS:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    .line 564
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    const-string v1, "INCORRECT_STATE_FOR_OPERATION"

    const/4 v2, 0x7

    const/16 v3, -0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INCORRECT_STATE_FOR_OPERATION:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    .line 565
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    const-string v1, "NOT_SINK_DEVICE"

    const/16 v2, 0x8

    const/16 v3, -0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->NOT_SINK_DEVICE:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    .line 566
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    const-string v1, "NOT_SOURCE_DEVICE"

    const/16 v2, 0x9

    const/16 v3, -0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->NOT_SOURCE_DEVICE:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    .line 567
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    const-string v1, "UIBC_NOT_ENABLED"

    const/16 v2, 0xa

    const/16 v3, -0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UIBC_NOT_ENABLED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    .line 568
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    const-string v1, "UIBC_ALREADY_ENABLED"

    const/16 v2, 0xb

    const/16 v3, -0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UIBC_ALREADY_ENABLED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    .line 556
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INVALID_ARG:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->HDMI_CABLE_CONNECTED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->OPERATION_TIMED_OUT:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->ALREADY_INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->NOT_INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->SESSION_IN_PROGRESS:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->INCORRECT_STATE_FOR_OPERATION:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->NOT_SINK_DEVICE:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->NOT_SOURCE_DEVICE:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UIBC_NOT_ENABLED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->UIBC_ALREADY_ENABLED:Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "c"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 572
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 573
    iput p3, p0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->code:I

    .line 574
    return-void
.end method

.method public static getValue(I)Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    .locals 5
    .param p0, "c"    # I

    .prologue
    .line 581
    invoke-static {}, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->values()[Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    move-result-object v0

    .local v0, "arr$":[Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 582
    .local v1, "e":Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    iget v4, v1, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->code:I

    if-ne v4, p0, :cond_0

    .line 585
    .end local v1    # "e":Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    :goto_1
    return-object v1

    .line 581
    .restart local v1    # "e":Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 585
    .end local v1    # "e":Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 556
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$ErrorType;
    .locals 1

    .prologue
    .line 556
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$ErrorType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$ErrorType;

    return-object v0
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 577
    iget v0, p0, Lcom/qualcomm/wfd/WfdEnums$ErrorType;->code:I

    return v0
.end method
