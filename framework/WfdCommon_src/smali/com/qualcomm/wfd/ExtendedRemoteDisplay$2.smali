.class Lcom/qualcomm/wfd/ExtendedRemoteDisplay$2;
.super Ljava/lang/Object;
.source "ExtendedRemoteDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->notifyDisplayDisconnected()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;


# direct methods
.method constructor <init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)V
    .locals 0

    .prologue
    .line 534
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$2;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 537
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$2;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mConnected:Z
    invoke-static {v0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$000(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 538
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$2;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mListener:Landroid/media/RemoteDisplay$Listener;
    invoke-static {v0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/media/RemoteDisplay$Listener;

    move-result-object v0

    invoke-interface {v0}, Landroid/media/RemoteDisplay$Listener;->onDisplayDisconnected()V

    .line 539
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$2;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    const/4 v1, 0x0

    # setter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mConnected:Z
    invoke-static {v0, v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$002(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Z)Z

    .line 541
    :cond_0
    return-void
.end method
