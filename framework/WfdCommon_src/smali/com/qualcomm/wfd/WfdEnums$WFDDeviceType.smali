.class public final enum Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "WFDDeviceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

.field public static final enum PRIMARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

.field public static final enum SECONDARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

.field public static final enum SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

.field public static final enum SOURCE_PRIMARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

.field public static final enum UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;


# instance fields
.field private final code:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 392
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    const-string v1, "SOURCE"

    invoke-direct {v0, v1, v2, v2}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    const-string v1, "PRIMARY_SINK"

    invoke-direct {v0, v1, v3, v3}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->PRIMARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    const-string v1, "SECONDARY_SINK"

    invoke-direct {v0, v1, v4, v4}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SECONDARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    const-string v1, "SOURCE_PRIMARY_SINK"

    invoke-direct {v0, v1, v5, v5}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE_PRIMARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6, v6}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    .line 391
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->PRIMARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SECONDARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->SOURCE_PRIMARY_SINK:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->UNKNOWN:Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "c"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 396
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 397
    iput p3, p0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->code:I

    .line 398
    return-void
.end method

.method public static getValue(I)Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    .locals 5
    .param p0, "c"    # I

    .prologue
    .line 405
    invoke-static {}, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->values()[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    move-result-object v0

    .local v0, "arr$":[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 406
    .local v1, "e":Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    iget v4, v1, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->code:I

    if-ne v4, p0, :cond_0

    .line 409
    .end local v1    # "e":Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    :goto_1
    return-object v1

    .line 405
    .restart local v1    # "e":Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 409
    .end local v1    # "e":Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 391
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    .locals 1

    .prologue
    .line 391
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;

    return-object v0
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 401
    iget v0, p0, Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;->code:I

    return v0
.end method
