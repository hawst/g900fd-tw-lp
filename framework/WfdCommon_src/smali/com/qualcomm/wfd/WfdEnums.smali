.class public Lcom/qualcomm/wfd/WfdEnums;
.super Ljava/lang/Object;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/wfd/WfdEnums$HIDDataType;,
        Lcom/qualcomm/wfd/WfdEnums$ConfigKeys;,
        Lcom/qualcomm/wfd/WfdEnums$ErrorType;,
        Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;,
        Lcom/qualcomm/wfd/WfdEnums$RuntimecmdType;,
        Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;,
        Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;,
        Lcom/qualcomm/wfd/WfdEnums$PreferredConnectivity;,
        Lcom/qualcomm/wfd/WfdEnums$WfdEvent;,
        Lcom/qualcomm/wfd/WfdEnums$SessionState;,
        Lcom/qualcomm/wfd/WfdEnums$VideoFormat;,
        Lcom/qualcomm/wfd/WfdEnums$AudioCodec;,
        Lcom/qualcomm/wfd/WfdEnums$AVPlaybackMode;,
        Lcom/qualcomm/wfd/WfdEnums$CapabilityType;,
        Lcom/qualcomm/wfd/WfdEnums$WFDDeviceType;
    }
.end annotation


# static fields
.field public static final ACTION_WIFI_DISPLAY_BITRATE:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_BITRATE"

.field public static final ACTION_WIFI_DISPLAY_DISABLED:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_DISABLED"

.field public static final ACTION_WIFI_DISPLAY_ENABLED:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_ENABLED"

.field public static final ACTION_WIFI_DISPLAY_PLAYBACK_MODE:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_PLAYBACK_MODE"

.field public static final ACTION_WIFI_DISPLAY_RESOLUTION:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_RESOLUTION"

.field public static final ACTION_WIFI_DISPLAY_RTP_TRANSPORT:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_RTP_TRANSPORT"

.field public static final ACTION_WIFI_DISPLAY_TCP_PLAYBACK_CONTROL:Ljava/lang/String; = "qualcomm.intent.action.WIFI_DISPLAY_TCP_PLAYBACK_CONTROL"

.field static final BIT0:I = 0x1

.field static final BIT1:I = 0x2

.field static final BIT10:I = 0x400

.field static final BIT11:I = 0x800

.field static final BIT12:I = 0x1000

.field static final BIT13:I = 0x2000

.field static final BIT14:I = 0x4000

.field static final BIT15:I = 0x8000

.field static final BIT16:I = 0x10000

.field static final BIT17:I = 0x20000

.field static final BIT18:I = 0x40000

.field static final BIT19:I = 0x80000

.field static final BIT2:I = 0x4

.field static final BIT20:I = 0x100000

.field static final BIT21:I = 0x200000

.field static final BIT22:I = 0x400000

.field static final BIT23:I = 0x800000

.field static final BIT24:I = 0x1000000

.field static final BIT25:I = 0x2000000

.field static final BIT26:I = 0x4000000

.field static final BIT27:I = 0x8000000

.field static final BIT28:I = 0x10000000

.field static final BIT29:I = 0x20000000

.field static final BIT3:I = 0x8

.field static final BIT30:I = 0x40000000

.field static final BIT31:I = -0x80000000

.field static final BIT4:I = 0x10

.field static final BIT5:I = 0x20

.field static final BIT6:I = 0x40

.field static final BIT7:I = 0x80

.field static final BIT8:I = 0x100

.field static final BIT9:I = 0x200

.field public static final CONFIG_BUNDLE_KEY:Ljava/lang/String; = "wfd_config"

.field public static final H264_CEA_1280x720p24:I = 0x8000

.field public static final H264_CEA_1280x720p25:I = 0x400

.field public static final H264_CEA_1280x720p30:I = 0x20

.field public static final H264_CEA_1280x720p50:I = 0x800

.field public static final H264_CEA_1280x720p60:I = 0x40

.field public static final H264_CEA_1920x1080i50:I = 0x4000

.field public static final H264_CEA_1920x1080i60:I = 0x200

.field public static final H264_CEA_1920x1080p24:I = 0x10000

.field public static final H264_CEA_1920x1080p25:I = 0x1000

.field public static final H264_CEA_1920x1080p30:I = 0x80

.field public static final H264_CEA_1920x1080p50:I = 0x2000

.field public static final H264_CEA_1920x1080p60:I = 0x100

.field public static final H264_CEA_640x480p60:I = 0x1

.field public static final H264_CEA_720x480i60:I = 0x4

.field public static final H264_CEA_720x480p60:I = 0x2

.field public static final H264_CEA_720x576i50:I = 0x10

.field public static final H264_CEA_720x576p50:I = 0x8

.field public static final H264_HH_640x360p30:I = 0x40

.field public static final H264_HH_640x360p60:I = 0x80

.field public static final H264_HH_800x480p30:I = 0x1

.field public static final H264_HH_800x480p60:I = 0x2

.field public static final H264_HH_848x480p30:I = 0x400

.field public static final H264_HH_848x480p60:I = 0x800

.field public static final H264_HH_854x480p30:I = 0x4

.field public static final H264_HH_854x480p60:I = 0x8

.field public static final H264_HH_864x480p30:I = 0x10

.field public static final H264_HH_864x480p60:I = 0x20

.field public static final H264_HH_960x540p30:I = 0x100

.field public static final H264_VESA_1024x768p30:I = 0x4

.field public static final H264_VESA_1024x768p60:I = 0x8

.field public static final H264_VESA_1152x864p30:I = 0x10

.field public static final H264_VESA_1152x864p60:I = 0x20

.field public static final H264_VESA_1280x1024p30:I = 0x4000

.field public static final H264_VESA_1280x1024p60:I = 0x8000

.field public static final H264_VESA_1280x768p30:I = 0x40

.field public static final H264_VESA_1280x768p60:I = 0x80

.field public static final H264_VESA_1280x800p30:I = 0x100

.field public static final H264_VESA_1280x800p60:I = 0x200

.field public static final H264_VESA_1360x768p30:I = 0x400

.field public static final H264_VESA_1360x768p60:I = 0x800

.field public static final H264_VESA_1366x768p30:I = 0x1000

.field public static final H264_VESA_1366x768p60:I = 0x2000

.field public static final H264_VESA_1400x1050p30:I = 0x10000

.field public static final H264_VESA_1400x1050p60:I = 0x20000

.field public static final H264_VESA_1440x900p30:I = 0x40000

.field public static final H264_VESA_1440x900p60:I = 0x80000

.field public static final H264_VESA_1600x1200p30:I = 0x400000

.field public static final H264_VESA_1600x1200p60:I = 0x800000

.field public static final H264_VESA_1600x900p30:I = 0x100000

.field public static final H264_VESA_1600x900p60:I = 0x200000

.field public static final H264_VESA_1680x1024p30:I = 0x1000000

.field public static final H264_VESA_1680x1024p60:I = 0x2000000

.field public static final H264_VESA_1680x1050p30:I = 0x4000000

.field public static final H264_VESA_1680x1050p60:I = 0x8000000

.field public static final H264_VESA_1920x1200p30:I = 0x10000000

.field public static final H264_VESA_1920x1200p60:I = 0x20000000

.field public static final H264_VESA_800x600p30:I = 0x1

.field public static final H264_VESA_800x600p60:I = 0x2

.field private static resFps:I

.field private static resHeight:I

.field private static resWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 62
    sput v0, Lcom/qualcomm/wfd/WfdEnums;->resWidth:I

    sput v0, Lcom/qualcomm/wfd/WfdEnums;->resHeight:I

    sput v0, Lcom/qualcomm/wfd/WfdEnums;->resFps:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 615
    return-void
.end method

.method public static getResParams()[I
    .locals 3

    .prologue
    .line 90
    const/4 v1, 0x3

    new-array v0, v1, [I

    .line 91
    .local v0, "resParams":[I
    const/4 v1, 0x0

    sget v2, Lcom/qualcomm/wfd/WfdEnums;->resWidth:I

    aput v2, v0, v1

    .line 92
    const/4 v1, 0x1

    sget v2, Lcom/qualcomm/wfd/WfdEnums;->resHeight:I

    aput v2, v0, v1

    .line 93
    const/4 v1, 0x2

    sget v2, Lcom/qualcomm/wfd/WfdEnums;->resFps:I

    aput v2, v0, v1

    .line 94
    return-object v0
.end method

.method public static final isCeaResolution(I)Z
    .locals 6
    .param p0, "resolution"    # I

    .prologue
    const/16 v5, 0x32

    const/16 v4, 0x3c

    const/16 v3, 0x780

    const/16 v1, 0x438

    const/16 v2, 0x2d0

    .line 98
    sparse-switch p0, :sswitch_data_0

    .line 168
    const/4 v0, 0x0

    .line 170
    :goto_0
    return v0

    .line 100
    :sswitch_0
    const/16 v0, 0x280

    const/16 v1, 0x1e0

    invoke-static {v0, v1, v4}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    .line 170
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 104
    :sswitch_1
    const/16 v0, 0x1e0

    invoke-static {v2, v0, v4}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 108
    :sswitch_2
    const/16 v0, 0x1e0

    invoke-static {v2, v0, v4}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 112
    :sswitch_3
    const/16 v0, 0x240

    invoke-static {v2, v0, v5}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 116
    :sswitch_4
    const/16 v0, 0x240

    invoke-static {v2, v0, v5}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 120
    :sswitch_5
    const/16 v0, 0x500

    const/16 v1, 0x1e

    invoke-static {v0, v2, v1}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 124
    :sswitch_6
    const/16 v0, 0x500

    invoke-static {v0, v2, v4}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 128
    :sswitch_7
    const/16 v0, 0x1e

    invoke-static {v3, v1, v0}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 132
    :sswitch_8
    invoke-static {v3, v1, v4}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 136
    :sswitch_9
    invoke-static {v3, v1, v4}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 140
    :sswitch_a
    const/16 v0, 0x500

    const/16 v1, 0x19

    invoke-static {v0, v2, v1}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 144
    :sswitch_b
    const/16 v0, 0x500

    invoke-static {v0, v2, v5}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 148
    :sswitch_c
    const/16 v0, 0x19

    invoke-static {v3, v1, v0}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 152
    :sswitch_d
    invoke-static {v3, v1, v5}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 156
    :sswitch_e
    invoke-static {v3, v1, v5}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 160
    :sswitch_f
    const/16 v0, 0x500

    const/16 v1, 0x18

    invoke-static {v0, v2, v1}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 164
    :sswitch_10
    const/16 v0, 0x18

    invoke-static {v3, v1, v0}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 98
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
        0x20 -> :sswitch_5
        0x40 -> :sswitch_6
        0x80 -> :sswitch_7
        0x100 -> :sswitch_8
        0x200 -> :sswitch_9
        0x400 -> :sswitch_a
        0x800 -> :sswitch_b
        0x1000 -> :sswitch_c
        0x2000 -> :sswitch_d
        0x4000 -> :sswitch_e
        0x8000 -> :sswitch_f
        0x10000 -> :sswitch_10
    .end sparse-switch
.end method

.method public static final isHhResolution(I)Z
    .locals 5
    .param p0, "resolution"    # I

    .prologue
    const/16 v4, 0x280

    const/16 v0, 0x168

    const/16 v3, 0x3c

    const/16 v2, 0x1e

    const/16 v1, 0x1e0

    .line 347
    sparse-switch p0, :sswitch_data_0

    .line 385
    const/4 v0, 0x0

    .line 387
    :goto_0
    return v0

    .line 349
    :sswitch_0
    const/16 v0, 0x320

    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    .line 387
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 353
    :sswitch_1
    const/16 v0, 0x320

    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 357
    :sswitch_2
    const/16 v0, 0x360

    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 361
    :sswitch_3
    const/16 v0, 0x360

    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 365
    :sswitch_4
    invoke-static {v4, v0, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 369
    :sswitch_5
    invoke-static {v4, v0, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 373
    :sswitch_6
    const/16 v0, 0x3c0

    const/16 v1, 0x21c

    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 377
    :sswitch_7
    const/16 v0, 0x350

    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 381
    :sswitch_8
    const/16 v0, 0x350

    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 347
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x10 -> :sswitch_2
        0x20 -> :sswitch_3
        0x40 -> :sswitch_4
        0x80 -> :sswitch_5
        0x100 -> :sswitch_6
        0x400 -> :sswitch_7
        0x800 -> :sswitch_8
    .end sparse-switch
.end method

.method public static final isVesaResolution(I)Z
    .locals 6
    .param p0, "resolution"    # I

    .prologue
    const/16 v5, 0x500

    const/16 v4, 0x400

    const/16 v1, 0x300

    const/16 v3, 0x3c

    const/16 v2, 0x1e

    .line 206
    sparse-switch p0, :sswitch_data_0

    .line 328
    const/4 v0, 0x0

    .line 330
    :goto_0
    return v0

    .line 208
    :sswitch_0
    const/16 v0, 0x320

    const/16 v1, 0x258

    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    .line 330
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 212
    :sswitch_1
    const/16 v0, 0x320

    const/16 v1, 0x258

    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 216
    :sswitch_2
    invoke-static {v4, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 220
    :sswitch_3
    invoke-static {v4, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 224
    :sswitch_4
    const/16 v0, 0x480

    const/16 v1, 0x360

    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 228
    :sswitch_5
    const/16 v0, 0x480

    const/16 v1, 0x360

    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 232
    :sswitch_6
    invoke-static {v5, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 236
    :sswitch_7
    invoke-static {v5, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 240
    :sswitch_8
    const/16 v0, 0x320

    invoke-static {v5, v0, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 244
    :sswitch_9
    const/16 v0, 0x320

    invoke-static {v5, v0, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 248
    :sswitch_a
    const/16 v0, 0x550

    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 252
    :sswitch_b
    const/16 v0, 0x550

    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 256
    :sswitch_c
    const/16 v0, 0x556

    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 260
    :sswitch_d
    const/16 v0, 0x556

    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 264
    :sswitch_e
    invoke-static {v5, v4, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 268
    :sswitch_f
    invoke-static {v5, v4, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 272
    :sswitch_10
    const/16 v0, 0x578

    const/16 v1, 0x41a

    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 276
    :sswitch_11
    const/16 v0, 0x578

    const/16 v1, 0x41a

    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 280
    :sswitch_12
    const/16 v0, 0x5a0

    const/16 v1, 0x384

    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 284
    :sswitch_13
    const/16 v0, 0x5a0

    const/16 v1, 0x384

    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 288
    :sswitch_14
    const/16 v0, 0x640

    const/16 v1, 0x384

    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto :goto_1

    .line 292
    :sswitch_15
    const/16 v0, 0x640

    const/16 v1, 0x384

    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto/16 :goto_1

    .line 296
    :sswitch_16
    const/16 v0, 0x640

    const/16 v1, 0x4b0

    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto/16 :goto_1

    .line 300
    :sswitch_17
    const/16 v0, 0x640

    const/16 v1, 0x4b0

    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto/16 :goto_1

    .line 304
    :sswitch_18
    const/16 v0, 0x690

    invoke-static {v0, v4, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto/16 :goto_1

    .line 308
    :sswitch_19
    const/16 v0, 0x690

    invoke-static {v0, v4, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto/16 :goto_1

    .line 312
    :sswitch_1a
    const/16 v0, 0x690

    const/16 v1, 0x41a

    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto/16 :goto_1

    .line 316
    :sswitch_1b
    const/16 v0, 0x690

    const/16 v1, 0x41a

    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto/16 :goto_1

    .line 320
    :sswitch_1c
    const/16 v0, 0x780

    const/16 v1, 0x4b0

    invoke-static {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto/16 :goto_1

    .line 324
    :sswitch_1d
    const/16 v0, 0x780

    const/16 v1, 0x4b0

    invoke-static {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums;->setResParams(III)V

    goto/16 :goto_1

    .line 206
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
        0x20 -> :sswitch_5
        0x40 -> :sswitch_6
        0x80 -> :sswitch_7
        0x100 -> :sswitch_8
        0x200 -> :sswitch_9
        0x400 -> :sswitch_a
        0x800 -> :sswitch_b
        0x1000 -> :sswitch_c
        0x2000 -> :sswitch_d
        0x4000 -> :sswitch_e
        0x8000 -> :sswitch_f
        0x10000 -> :sswitch_10
        0x20000 -> :sswitch_11
        0x40000 -> :sswitch_12
        0x80000 -> :sswitch_13
        0x100000 -> :sswitch_14
        0x200000 -> :sswitch_15
        0x400000 -> :sswitch_16
        0x800000 -> :sswitch_17
        0x1000000 -> :sswitch_18
        0x2000000 -> :sswitch_19
        0x4000000 -> :sswitch_1a
        0x8000000 -> :sswitch_1b
        0x10000000 -> :sswitch_1c
        0x20000000 -> :sswitch_1d
    .end sparse-switch
.end method

.method private static setResParams(III)V
    .locals 0
    .param p0, "width"    # I
    .param p1, "height"    # I
    .param p2, "fps"    # I

    .prologue
    .line 84
    sput p0, Lcom/qualcomm/wfd/WfdEnums;->resWidth:I

    .line 85
    sput p1, Lcom/qualcomm/wfd/WfdEnums;->resHeight:I

    .line 86
    sput p2, Lcom/qualcomm/wfd/WfdEnums;->resFps:I

    .line 87
    return-void
.end method
