.class public final enum Lcom/qualcomm/wfd/WfdEnums$HIDDataType;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HIDDataType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$HIDDataType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

.field public static final enum HID_INVALID_DATA:Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

.field public static final enum HID_REPORT:Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

.field public static final enum HID_REPORT_DESCRIPTOR:Lcom/qualcomm/wfd/WfdEnums$HIDDataType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 616
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

    const-string v1, "HID_INVALID_DATA"

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;->HID_INVALID_DATA:Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

    const-string v1, "HID_REPORT"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;->HID_REPORT:Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

    const-string v1, "HID_REPORT_DESCRIPTOR"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;->HID_REPORT_DESCRIPTOR:Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

    .line 615
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;->HID_INVALID_DATA:Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;->HID_REPORT:Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;->HID_REPORT_DESCRIPTOR:Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 615
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$HIDDataType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 615
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$HIDDataType;
    .locals 1

    .prologue
    .line 615
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$HIDDataType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$HIDDataType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$HIDDataType;

    return-object v0
.end method
