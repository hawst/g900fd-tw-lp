.class Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;
.super Landroid/os/Handler;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ExtendedRemoteDisplayEventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;


# direct methods
.method constructor <init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)V
    .locals 0

    .prologue
    .line 558
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 21
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 562
    const-string v17, "ExtendedRemoteDisplay"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Event handler received: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v17, v0

    packed-switch v17, :pswitch_data_0

    .line 803
    :pswitch_0
    const-string v17, "ExtendedRemoteDisplay"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Unknown event received: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    :cond_0
    :goto_0
    return-void

    .line 567
    :pswitch_1
    sget-object v17, Lcom/qualcomm/wfd/WFDState;->PLAY:Lcom/qualcomm/wfd/WFDState;

    sput-object v17, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;

    .line 568
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "WFDService in PLAY state"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 573
    :pswitch_2
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "WFDService in PAUSE state"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 578
    :pswitch_3
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "WFDService in STANDBY state"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 583
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    new-instance v18, Lcom/qualcomm/wfd/WfdActionListenerImpl;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v19, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mEventHandler:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;
    invoke-static/range {v19 .. v19}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$300(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Lcom/qualcomm/wfd/WfdActionListenerImpl;-><init>(Landroid/os/Handler;)V

    # setter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mActionListener:Lcom/qualcomm/wfd/service/IWfdActionListener;
    invoke-static/range {v17 .. v18}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$202(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Lcom/qualcomm/wfd/service/IWfdActionListener;)Lcom/qualcomm/wfd/service/IWfdActionListener;

    .line 585
    sget-object v17, Lcom/qualcomm/wfd/WFDState;->BOUND:Lcom/qualcomm/wfd/WFDState;

    sput-object v17, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;

    .line 586
    const-string v17, "persist.sys.wfd.virtual"

    const-string v18, "1"

    invoke-static/range {v17 .. v18}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    :try_start_0
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;

    move-result-object v17

    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Lcom/qualcomm/wfd/service/ISessionManagerService;->setDeviceType(I)I

    move-result v12

    .line 591
    .local v12, "setDeviceTypeRet":I
    if-nez v12, :cond_3

    .line 592
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "mWfdService.setDeviceType called."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    :goto_1
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lcom/qualcomm/wfd/service/ISessionManagerService;->getStatus()Lcom/qualcomm/wfd/WfdStatus;

    move-result-object v13

    .line 603
    .local v13, "wfdStatus":Lcom/qualcomm/wfd/WfdStatus;
    const-string v17, "ExtendedRemoteDisplay"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "wfdStatus.state= "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget v0, v13, Lcom/qualcomm/wfd/WfdStatus;->state:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    iget v0, v13, Lcom/qualcomm/wfd/WfdStatus;->state:I

    move/from16 v17, v0

    sget-object v18, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    invoke-virtual/range {v18 .. v18}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_1

    iget v0, v13, Lcom/qualcomm/wfd/WfdStatus;->state:I

    move/from16 v17, v0

    sget-object v18, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INITIALIZED:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    invoke-virtual/range {v18 .. v18}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    .line 609
    :cond_1
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "wfdStatus.state is INVALID or INITIALIZED"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;
    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$400(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WfdDevice;

    move-result-object v17

    if-eqz v17, :cond_0

    .line 614
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v18, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mActionListener:Lcom/qualcomm/wfd/service/IWfdActionListener;
    invoke-static/range {v18 .. v18}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$200(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/service/IWfdActionListener;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v19, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;
    invoke-static/range {v19 .. v19}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$400(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WfdDevice;

    move-result-object v19

    invoke-interface/range {v17 .. v19}, Lcom/qualcomm/wfd/service/ISessionManagerService;->init(Lcom/qualcomm/wfd/service/IWfdActionListener;Lcom/qualcomm/wfd/WfdDevice;)I

    .line 617
    sget-object v17, Lcom/qualcomm/wfd/WFDState;->INITIALIZING:Lcom/qualcomm/wfd/WFDState;

    sput-object v17, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 621
    .end local v12    # "setDeviceTypeRet":I
    .end local v13    # "wfdStatus":Lcom/qualcomm/wfd/WfdStatus;
    :catch_0
    move-exception v6

    .line 622
    .local v6, "e":Landroid/os/RemoteException;
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "WfdService init() failed"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 594
    .end local v6    # "e":Landroid/os/RemoteException;
    .restart local v12    # "setDeviceTypeRet":I
    :cond_3
    :try_start_1
    const-string v17, "ExtendedRemoteDisplay"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "mWfdService.setDeviceType failed error code: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 629
    .end local v12    # "setDeviceTypeRet":I
    :pswitch_5
    :try_start_2
    sget-object v17, Lcom/qualcomm/wfd/WFDState;->TEARDOWN:Lcom/qualcomm/wfd/WFDState;

    sput-object v17, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;

    .line 630
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    # invokes: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->notifyDisplayDisconnected()V
    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$500(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)V

    .line 631
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Lcom/qualcomm/wfd/service/ISessionManagerService;->deinit()I

    .line 632
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPtr:I
    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$600(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I

    move-result v17

    if-eqz v17, :cond_4

    .line 636
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v18, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPtr:I
    invoke-static/range {v18 .. v18}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$600(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v19, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->surface:Landroid/view/Surface;
    invoke-static/range {v19 .. v19}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/view/Surface;

    move-result-object v19

    # invokes: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->destroySurface(ILandroid/view/Surface;)I
    invoke-static/range {v17 .. v19}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$800(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;ILandroid/view/Surface;)I

    .line 637
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v18, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPtr:I
    invoke-static/range {v18 .. v18}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$600(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I

    move-result v18

    # invokes: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->destroyNativeObject(I)V
    invoke-static/range {v17 .. v18}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$900(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)V

    .line 638
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->surface:Landroid/view/Surface;
    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/view/Surface;

    move-result-object v17

    if-eqz v17, :cond_6

    .line 639
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->surface:Landroid/view/Surface;
    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/view/Surface;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/view/Surface;->isValid()Z

    move-result v17

    if-eqz v17, :cond_5

    .line 640
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->surface:Landroid/view/Surface;
    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/view/Surface;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/view/Surface;->release()V

    .line 641
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "Released surface successfully"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    # setter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPtr:I
    invoke-static/range {v17 .. v18}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$602(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)I

    .line 651
    :cond_4
    const-string v17, "persist.sys.wfd.virtual"

    const-string v18, "0"

    invoke-static/range {v17 .. v18}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 652
    :catch_1
    move-exception v6

    .line 653
    .restart local v6    # "e":Landroid/os/RemoteException;
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "EventHandler:Remote exception when calling deinit()"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v1, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 644
    .end local v6    # "e":Landroid/os/RemoteException;
    :cond_5
    :try_start_3
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "surface not valid"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 647
    :cond_6
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "Why on earth is surface null??"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    .line 662
    :pswitch_6
    sget-object v17, Lcom/qualcomm/wfd/WFDState;->PLAYING:Lcom/qualcomm/wfd/WFDState;

    sput-object v17, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;

    .line 663
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    .line 664
    .local v3, "b":Landroid/os/Bundle;
    const-string v17, "width"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    .line 665
    .local v14, "width":I
    const-string v17, "height"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 667
    .local v7, "height":I
    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sIsDownscaleSupported:Z
    invoke-static {}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1000()Z

    move-result v17

    if-eqz v17, :cond_8

    .line 671
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mContext:Landroid/content/Context;
    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/content/Context;

    move-result-object v17

    const-string v18, "display"

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/hardware/display/DisplayManager;

    .line 673
    .local v4, "displayManager":Landroid/hardware/display/DisplayManager;
    invoke-virtual {v4}, Landroid/hardware/display/DisplayManager;->getDisplays()[Landroid/view/Display;

    move-result-object v5

    .line 674
    .local v5, "displays":[Landroid/view/Display;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_3
    array-length v0, v5

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v8, v0, :cond_8

    .line 675
    aget-object v17, v5, v8

    invoke-virtual/range {v17 .. v17}, Landroid/view/Display;->getType()I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_a

    .line 676
    new-instance v9, Landroid/graphics/Point;

    invoke-direct {v9}, Landroid/graphics/Point;-><init>()V

    .line 677
    .local v9, "point":Landroid/graphics/Point;
    aget-object v17, v5, v8

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 678
    const-string v17, "ExtendedRemoteDisplay"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Primary Height and width: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget v0, v9, Landroid/graphics/Point;->x:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    iget v0, v9, Landroid/graphics/Point;->y:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 680
    const/4 v15, 0x0

    .line 681
    .local v15, "xres":I
    const/16 v16, 0x0

    .line 682
    .local v16, "yres":I
    iget v0, v9, Landroid/graphics/Point;->x:I

    move/from16 v17, v0

    const/16 v18, 0x800

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_8

    iget v0, v9, Landroid/graphics/Point;->y:I

    move/from16 v17, v0

    const/16 v18, 0x800

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_8

    .line 683
    iget v0, v9, Landroid/graphics/Point;->x:I

    move/from16 v17, v0

    iget v0, v9, Landroid/graphics/Point;->y:I

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_9

    .line 684
    iget v15, v9, Landroid/graphics/Point;->x:I

    .line 685
    iget v0, v9, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    .line 690
    :goto_4
    if-gt v15, v14, :cond_7

    move/from16 v0, v16

    if-le v0, v7, :cond_8

    .line 691
    :cond_7
    move v14, v15

    .line 692
    move/from16 v7, v16

    .line 705
    .end local v4    # "displayManager":Landroid/hardware/display/DisplayManager;
    .end local v5    # "displays":[Landroid/view/Display;
    .end local v8    # "i":I
    .end local v9    # "point":Landroid/graphics/Point;
    .end local v15    # "xres":I
    .end local v16    # "yres":I
    :cond_8
    const-string v17, "hdcp"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 707
    .local v11, "secure":I
    const-string v17, "ExtendedRemoteDisplay"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "MM Stream Started Height, width and secure:  "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "width"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "height"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "hdcp"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 712
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPtr:I
    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$600(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I

    move-result v17

    if-nez v17, :cond_d

    .line 713
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "Create Native object"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 714
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v18, v0

    # invokes: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->getNativeObject()I
    invoke-static/range {v18 .. v18}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1200(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I

    move-result v18

    # setter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPtr:I
    invoke-static/range {v17 .. v18}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$602(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)I

    .line 715
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPtr:I
    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$600(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I

    move-result v17

    if-nez v17, :cond_b

    .line 716
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "ExtendedRemoteDisplay Failed to get Native Object"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    :goto_5
    if-eqz v11, :cond_c

    .line 723
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v18, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->surface:Landroid/view/Surface;
    invoke-static/range {v18 .. v18}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/view/Surface;

    move-result-object v18

    const/16 v19, 0x1

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v19

    # invokes: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->notifyDisplayConnected(Landroid/view/Surface;III)V
    invoke-static {v0, v1, v14, v7, v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1400(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/view/Surface;III)V

    goto/16 :goto_0

    .line 687
    .end local v11    # "secure":I
    .restart local v4    # "displayManager":Landroid/hardware/display/DisplayManager;
    .restart local v5    # "displays":[Landroid/view/Display;
    .restart local v8    # "i":I
    .restart local v9    # "point":Landroid/graphics/Point;
    .restart local v15    # "xres":I
    .restart local v16    # "yres":I
    :cond_9
    iget v15, v9, Landroid/graphics/Point;->y:I

    .line 688
    iget v0, v9, Landroid/graphics/Point;->x:I

    move/from16 v16, v0

    goto/16 :goto_4

    .line 674
    .end local v9    # "point":Landroid/graphics/Point;
    .end local v15    # "xres":I
    .end local v16    # "yres":I
    :cond_a
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_3

    .line 719
    .end local v4    # "displayManager":Landroid/hardware/display/DisplayManager;
    .end local v5    # "displays":[Landroid/view/Display;
    .end local v8    # "i":I
    .restart local v11    # "secure":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v19, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPtr:I
    invoke-static/range {v19 .. v19}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$600(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    # invokes: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->getSurface(III)Landroid/view/Surface;
    invoke-static {v0, v1, v14, v7}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1300(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;III)Landroid/view/Surface;

    move-result-object v18

    # setter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->surface:Landroid/view/Surface;
    invoke-static/range {v17 .. v18}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$702(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/view/Surface;)Landroid/view/Surface;

    goto :goto_5

    .line 726
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v18, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->surface:Landroid/view/Surface;
    invoke-static/range {v18 .. v18}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/view/Surface;

    move-result-object v18

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v19

    # invokes: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->notifyDisplayConnected(Landroid/view/Surface;III)V
    invoke-static {v0, v1, v14, v7, v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1400(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/view/Surface;III)V

    goto/16 :goto_0

    .line 729
    :cond_d
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "ExtendedRemoteDisplay Not honor stream start"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 737
    .end local v3    # "b":Landroid/os/Bundle;
    .end local v7    # "height":I
    .end local v11    # "secure":I
    .end local v14    # "width":I
    :pswitch_7
    :try_start_4
    sget-object v17, Lcom/qualcomm/wfd/WFDState;->ESTABLISHING:Lcom/qualcomm/wfd/WFDState;

    sput-object v17, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;

    .line 738
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v18, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;
    invoke-static/range {v18 .. v18}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1500(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WfdDevice;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Lcom/qualcomm/wfd/service/ISessionManagerService;->startWfdSession(Lcom/qualcomm/wfd/WfdDevice;)I

    move-result v10

    .line 740
    .local v10, "ret":I
    const-string v17, "ExtendedRemoteDisplay"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "EventHandler:startSession-initiated"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput-boolean v0, v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mInvalid:Z
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 742
    .end local v10    # "ret":I
    :catch_2
    move-exception v6

    .line 743
    .restart local v6    # "e":Landroid/os/RemoteException;
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "EventHandler: startSession- failed"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 744
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    # invokes: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->notifyDisplayError(I)V
    invoke-static/range {v17 .. v18}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1600(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)V

    goto/16 :goto_0

    .line 750
    .end local v6    # "e":Landroid/os/RemoteException;
    :pswitch_8
    sget-object v17, Lcom/qualcomm/wfd/WFDState;->ESTABLISHED:Lcom/qualcomm/wfd/WFDState;

    sput-object v17, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;

    .line 751
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "EventHandler: startSession- completed"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 756
    :pswitch_9
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "EventHandler: uibcActionCompleted- completed"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 761
    :pswitch_a
    sget-object v18, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sERDLock:Ljava/lang/Object;

    monitor-enter v18

    .line 762
    :try_start_5
    sget-object v17, Lcom/qualcomm/wfd/WFDState;->DEINIT:Lcom/qualcomm/wfd/WFDState;

    sput-object v17, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;

    .line 763
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, v17

    iput-boolean v0, v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mInvalid:Z

    .line 764
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getmServiceAlreadyBound()Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v17

    if-eqz v17, :cond_e

    .line 766
    :try_start_6
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v19, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mActionListener:Lcom/qualcomm/wfd/service/IWfdActionListener;
    invoke-static/range {v19 .. v19}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$200(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/service/IWfdActionListener;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Lcom/qualcomm/wfd/service/ISessionManagerService;->unregisterListener(Lcom/qualcomm/wfd/service/IWfdActionListener;)I
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 775
    :cond_e
    :goto_6
    :try_start_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLooper:Landroid/os/Looper;
    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/os/Looper;

    move-result-object v17

    if-eqz v17, :cond_f

    .line 776
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLooper:Landroid/os/Looper;
    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/os/Looper;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/os/Looper;->quitSafely()V

    .line 777
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    const/16 v19, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    # setter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLooper:Landroid/os/Looper;
    invoke-static {v0, v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1702(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/os/Looper;)Landroid/os/Looper;

    .line 780
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHThread:Landroid/os/HandlerThread;
    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1800(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/os/HandlerThread;

    move-result-object v17

    if-eqz v17, :cond_10

    .line 781
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHThread:Landroid/os/HandlerThread;
    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1800(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/os/HandlerThread;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/os/HandlerThread;->quitSafely()Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 783
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHThread:Landroid/os/HandlerThread;
    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1800(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/os/HandlerThread;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/os/HandlerThread;->join()V
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 789
    :cond_10
    :goto_7
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    move-object/from16 v17, v0

    if-eqz v17, :cond_11

    .line 790
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    const/16 v19, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    .line 793
    :cond_11
    const-string v17, "ExtendedRemoteDisplay"

    const-string v19, "Unbind the WFD service"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    move-object/from16 v17, v0

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mContext:Landroid/content/Context;
    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/qualcomm/wfd/ServiceUtil;->unbindService(Landroid/content/Context;)V

    .line 796
    sget-object v17, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sERDLock:Ljava/lang/Object;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->notifyAll()V

    .line 797
    monitor-exit v18
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 798
    const-string v17, "ExtendedRemoteDisplay"

    const-string v18, "ERD instance invalidated"

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 768
    :catch_3
    move-exception v6

    .line 769
    .restart local v6    # "e":Landroid/os/RemoteException;
    :try_start_a
    const-string v17, "ExtendedRemoteDisplay"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "RemoteException in unregistering listener"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 797
    .end local v6    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v17

    monitor-exit v18
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    throw v17

    .line 784
    :catch_4
    move-exception v6

    .line 785
    .local v6, "e":Ljava/lang/InterruptedException;
    :try_start_b
    const-string v17, "ExtendedRemoteDisplay"

    const-string v19, "EventHandler: Failed to join for mHThread"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_7

    .line 564
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_9
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_a
    .end packed-switch
.end method
