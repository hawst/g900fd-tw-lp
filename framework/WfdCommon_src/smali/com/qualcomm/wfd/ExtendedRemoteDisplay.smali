.class public final Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
.super Ljava/lang/Object;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;,
        Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;
    }
.end annotation


# static fields
.field public static final DISPLAY_ERROR_CONNECTION_DROPPED:I = 0x2

.field public static final DISPLAY_ERROR_UNKOWN:I = 0x1

.field public static final DISPLAY_FLAG_SECURE:I = 0x1

.field private static final OP_TIMEOUT:I = 0x2710

.field private static final TAG:Ljava/lang/String; = "ExtendedRemoteDisplay"

.field private static final WFD_PRIMARY_SINK:I = 0x1

.field private static final WFD_SOURCE:I = 0x0

.field public static mRefs:I = 0x0

.field private static final sDownscaleKey:Ljava/lang/String; = "sys.hwc.mdp_downscale_enabled"

.field public static sERDLock:Ljava/lang/Object;

.field private static sIsDownscaleSupported:Z

.field public static sState:Lcom/qualcomm/wfd/WFDState;


# instance fields
.field private mActionListener:Lcom/qualcomm/wfd/service/IWfdActionListener;

.field public mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

.field private mConnected:Z

.field private mContext:Landroid/content/Context;

.field private mEventHandler:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;

.field private final mGuard:Ldalvik/system/CloseGuard;

.field private mHThread:Landroid/os/HandlerThread;

.field private mHandler:Landroid/os/Handler;

.field private mIface:Ljava/lang/String;

.field public mInvalid:Z

.field private mListener:Landroid/media/RemoteDisplay$Listener;

.field private mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

.field private mLooper:Landroid/os/Looper;

.field private mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

.field private mPtr:I

.field private surface:Landroid/view/Surface;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 308
    sput-boolean v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sIsDownscaleSupported:Z

    .line 311
    const-string v1, "extendedremotedisplay"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 312
    const-string v1, "ExtendedRemoteDisplay"

    const-string v2, "Loaded extendedremotedisplay.so"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    :try_start_0
    const-string v1, "sys.hwc.mdp_downscale_enabled"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sIsDownscaleSupported:Z

    .line 317
    sget-boolean v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sIsDownscaleSupported:Z

    if-eqz v1, :cond_0

    .line 318
    const-string v1, "ExtendedRemoteDisplay"

    const-string v2, "Downscaling property available"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    .local v0, "e":Ljava/lang/Exception;
    :cond_0
    :goto_0
    sget v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mRefs:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mRefs:I

    .line 361
    sget-object v1, Lcom/qualcomm/wfd/WFDState;->DEINIT:Lcom/qualcomm/wfd/WFDState;

    sput-object v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;

    .line 362
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    sput-object v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sERDLock:Ljava/lang/Object;

    return-void

    .line 320
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 321
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "ExtendedRemoteDisplay"

    const-string v2, "Exception while getting system Property"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/media/RemoteDisplay$Listener;Landroid/os/Handler;Landroid/content/Context;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/RemoteDisplay$Listener;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 340
    invoke-static {}, Ldalvik/system/CloseGuard;->get()Ldalvik/system/CloseGuard;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mGuard:Ldalvik/system/CloseGuard;

    .line 359
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mInvalid:Z

    .line 372
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mListener:Landroid/media/RemoteDisplay$Listener;

    .line 373
    iput-object p2, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHandler:Landroid/os/Handler;

    .line 374
    iput-object p3, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mContext:Landroid/content/Context;

    .line 375
    new-instance v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;

    invoke-direct {v0, p0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;-><init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)V

    iput-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mEventHandler:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;

    .line 377
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ExtendedRemoteDisplay"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHThread:Landroid/os/HandlerThread;

    .line 378
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 379
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLooper:Landroid/os/Looper;

    .line 380
    new-instance v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLooper:Landroid/os/Looper;

    invoke-direct {v0, p0, v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;-><init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    .line 381
    return-void
.end method

.method static synthetic access$000(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Z
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    .prologue
    .line 303
    iget-boolean v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mConnected:Z

    return v0
.end method

.method static synthetic access$002(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    .param p1, "x1"    # Z

    .prologue
    .line 303
    iput-boolean p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mConnected:Z

    return p1
.end method

.method static synthetic access$100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/media/RemoteDisplay$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mListener:Landroid/media/RemoteDisplay$Listener;

    return-object v0
.end method

.method static synthetic access$1000()Z
    .locals 1

    .prologue
    .line 303
    sget-boolean v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sIsDownscaleSupported:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    .prologue
    .line 303
    invoke-direct {p0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->getNativeObject()I

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;III)Landroid/view/Surface;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 303
    invoke-direct {p0, p1, p2, p3}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->getSurface(III)Landroid/view/Surface;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/view/Surface;III)V
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    .param p1, "x1"    # Landroid/view/Surface;
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 303
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->notifyDisplayConnected(Landroid/view/Surface;III)V

    return-void
.end method

.method static synthetic access$1500(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WfdDevice;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)V
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    .param p1, "x1"    # I

    .prologue
    .line 303
    invoke-direct {p0, p1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->notifyDisplayError(I)V

    return-void
.end method

.method static synthetic access$1700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/os/Looper;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLooper:Landroid/os/Looper;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/os/Looper;)Landroid/os/Looper;
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    .param p1, "x1"    # Landroid/os/Looper;

    .prologue
    .line 303
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLooper:Landroid/os/Looper;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/os/HandlerThread;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHThread:Landroid/os/HandlerThread;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mIface:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/service/IWfdActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mActionListener:Lcom/qualcomm/wfd/service/IWfdActionListener;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 303
    invoke-direct {p0, p1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->createLocalWFDDevice(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$202(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Lcom/qualcomm/wfd/service/IWfdActionListener;)Lcom/qualcomm/wfd/service/IWfdActionListener;
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    .param p1, "x1"    # Lcom/qualcomm/wfd/service/IWfdActionListener;

    .prologue
    .line 303
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mActionListener:Lcom/qualcomm/wfd/service/IWfdActionListener;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 303
    invoke-direct {p0, p1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->createPeerWFDDevice(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mEventHandler:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WfdDevice;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    return-object v0
.end method

.method static synthetic access$500(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)V
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    .prologue
    .line 303
    invoke-direct {p0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->notifyDisplayDisconnected()V

    return-void
.end method

.method static synthetic access$600(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)I
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    .prologue
    .line 303
    iget v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPtr:I

    return v0
.end method

.method static synthetic access$602(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)I
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    .param p1, "x1"    # I

    .prologue
    .line 303
    iput p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPtr:I

    return p1
.end method

.method static synthetic access$700(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/view/Surface;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    .prologue
    .line 303
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->surface:Landroid/view/Surface;

    return-object v0
.end method

.method static synthetic access$702(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/view/Surface;)Landroid/view/Surface;
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    .param p1, "x1"    # Landroid/view/Surface;

    .prologue
    .line 303
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->surface:Landroid/view/Surface;

    return-object p1
.end method

.method static synthetic access$800(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;ILandroid/view/Surface;)I
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/Surface;

    .prologue
    .line 303
    invoke-direct {p0, p1, p2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->destroySurface(ILandroid/view/Surface;)I

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)V
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    .param p1, "x1"    # I

    .prologue
    .line 303
    invoke-direct {p0, p1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->destroyNativeObject(I)V

    return-void
.end method

.method private createLocalWFDDevice(Ljava/lang/String;)V
    .locals 6
    .param p1, "iface"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 488
    new-instance v1, Lcom/qualcomm/wfd/WfdDevice;

    invoke-direct {v1}, Lcom/qualcomm/wfd/WfdDevice;-><init>()V

    iput-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    .line 489
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    iput v4, v1, Lcom/qualcomm/wfd/WfdDevice;->deviceType:I

    .line 490
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    iput-object v5, v1, Lcom/qualcomm/wfd/WfdDevice;->macAddress:Ljava/lang/String;

    .line 492
    if-eqz p1, :cond_1

    .line 493
    const-string v1, "ExtendedRemoteDisplay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Create WFDDevice from"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    const/16 v1, 0x3a

    invoke-virtual {p1, v1, v4}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 495
    .local v0, "index":I
    if-lez v0, :cond_1

    .line 496
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    invoke-virtual {p1, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/qualcomm/wfd/WfdDevice;->ipAddress:Ljava/lang/String;

    .line 497
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/qualcomm/wfd/WfdDevice;->rtspPort:I

    .line 500
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    iget-object v1, v1, Lcom/qualcomm/wfd/WfdDevice;->ipAddress:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    iget v1, v1, Lcom/qualcomm/wfd/WfdDevice;->rtspPort:I

    const/4 v2, 0x1

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    iget v1, v1, Lcom/qualcomm/wfd/WfdDevice;->rtspPort:I

    const v2, 0xffff

    if-le v1, v2, :cond_1

    .line 503
    :cond_0
    const-string v1, "ExtendedRemoteDisplay"

    const-string v2, "Invalid RTSP port received or no valid IP"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    iput-object v5, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    .line 508
    .end local v0    # "index":I
    :cond_1
    return-void
.end method

.method private createPeerWFDDevice(Ljava/lang/String;)V
    .locals 3
    .param p1, "iface"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 514
    new-instance v0, Lcom/qualcomm/wfd/WfdDevice;

    invoke-direct {v0}, Lcom/qualcomm/wfd/WfdDevice;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    .line 515
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    const/4 v1, 0x1

    iput v1, v0, Lcom/qualcomm/wfd/WfdDevice;->deviceType:I

    .line 516
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    iput-object v2, v0, Lcom/qualcomm/wfd/WfdDevice;->macAddress:Ljava/lang/String;

    .line 517
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    iput-object v2, v0, Lcom/qualcomm/wfd/WfdDevice;->ipAddress:Ljava/lang/String;

    .line 518
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;

    const/4 v1, 0x0

    iput v1, v0, Lcom/qualcomm/wfd/WfdDevice;->rtspPort:I

    .line 520
    return-void
.end method

.method private native destroyNativeObject(I)V
.end method

.method private native destroySurface(ILandroid/view/Surface;)I
.end method

.method private dispose(Z)V
    .locals 3
    .param p1, "finalized"    # Z

    .prologue
    .line 444
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mGuard:Ldalvik/system/CloseGuard;

    if-eqz v1, :cond_0

    .line 445
    if-eqz p1, :cond_2

    .line 446
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mGuard:Ldalvik/system/CloseGuard;

    invoke-virtual {v1}, Ldalvik/system/CloseGuard;->warnIfOpen()V

    .line 451
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    if-eqz v1, :cond_1

    .line 461
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 462
    .local v0, "messageEnd":Landroid/os/Message;
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    invoke-virtual {v1, v0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->sendMessage(Landroid/os/Message;)Z

    .line 464
    .end local v0    # "messageEnd":Landroid/os/Message;
    :cond_1
    return-void

    .line 448
    :cond_2
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mGuard:Ldalvik/system/CloseGuard;

    invoke-virtual {v1}, Ldalvik/system/CloseGuard;->close()V

    goto :goto_0
.end method

.method private native getNativeObject()I
.end method

.method private native getSurface(III)Landroid/view/Surface;
.end method

.method public static listen(Ljava/lang/String;Landroid/media/RemoteDisplay$Listener;Landroid/os/Handler;Landroid/content/Context;)Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    .locals 3
    .param p0, "iface"    # Ljava/lang/String;
    .param p1, "listener"    # Landroid/media/RemoteDisplay$Listener;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 413
    if-nez p0, :cond_0

    .line 414
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "iface must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 416
    :cond_0
    if-nez p1, :cond_1

    .line 417
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "listener must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 419
    :cond_1
    if-nez p2, :cond_2

    .line 420
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "handler must not be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 423
    :cond_2
    new-instance v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    invoke-direct {v0, p1, p2, p3}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;-><init>(Landroid/media/RemoteDisplay$Listener;Landroid/os/Handler;Landroid/content/Context;)V

    .line 426
    .local v0, "display":Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    iget-object v1, v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    if-nez v1, :cond_3

    .line 427
    const/4 v0, 0x0

    .line 432
    .end local v0    # "display":Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    :goto_0
    return-object v0

    .line 430
    .restart local v0    # "display":Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
    :cond_3
    invoke-virtual {v0, p0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->startListening(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private notifyDisplayConnected(Landroid/view/Surface;III)V
    .locals 7
    .param p1, "surface"    # Landroid/view/Surface;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "flags"    # I

    .prologue
    .line 524
    iget-object v6, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$1;-><init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/view/Surface;III)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 531
    return-void
.end method

.method private notifyDisplayDisconnected()V
    .locals 2

    .prologue
    .line 534
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$2;

    invoke-direct {v1, p0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$2;-><init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 543
    return-void
.end method

.method private notifyDisplayError(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 546
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$3;

    invoke-direct {v1, p0, p1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$3;-><init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 552
    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    .prologue
    .line 439
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->dispose(Z)V

    .line 440
    return-void
.end method

.method protected finalize()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 385
    const-string v0, "ExtendedRemoteDisplay"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finalize called on "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    :try_start_0
    iget-boolean v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mInvalid:Z

    if-nez v0, :cond_0

    .line 388
    const-string v0, "ExtendedRemoteDisplay"

    const-string v1, "!!!Finalized without being invalidated!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->dispose(Z)V

    .line 394
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mEventHandler:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;

    if-eqz v0, :cond_1

    .line 395
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mEventHandler:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 398
    :cond_1
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 400
    return-void

    .line 398
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public startListening(Ljava/lang/String;)V
    .locals 4
    .param p1, "iface"    # Ljava/lang/String;

    .prologue
    .line 470
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mIface:Ljava/lang/String;

    .line 471
    const-string v1, "ExtendedRemoteDisplay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "New ERD instance"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 472
    const-string v1, "persist.sys.wfd.virtual"

    const-string v2, "1"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 476
    .local v0, "messageStart":Landroid/os/Message;
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mCmdHdlr:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;

    invoke-virtual {v1, v0}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->sendMessage(Landroid/os/Message;)Z

    .line 478
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mGuard:Ldalvik/system/CloseGuard;

    const-string v2, "dispose"

    invoke-virtual {v1, v2}, Ldalvik/system/CloseGuard;->open(Ljava/lang/String;)V

    .line 479
    return-void
.end method
