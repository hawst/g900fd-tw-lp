.class public Lcom/qualcomm/wfd/WfdStatus;
.super Ljava/lang/Object;
.source "WfdStatus.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/wfd/WfdStatus;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public connectedDevice:Lcom/qualcomm/wfd/WfdDevice;

.field public sessionId:I

.field public state:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/qualcomm/wfd/WfdStatus$1;

    invoke-direct {v0}, Lcom/qualcomm/wfd/WfdStatus$1;-><init>()V

    sput-object v0, Lcom/qualcomm/wfd/WfdStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$SessionState;->INVALID:Lcom/qualcomm/wfd/WfdEnums$SessionState;

    invoke-virtual {v0}, Lcom/qualcomm/wfd/WfdEnums$SessionState;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/wfd/WfdStatus;->state:I

    .line 23
    const/4 v0, -0x1

    iput v0, p0, Lcom/qualcomm/wfd/WfdStatus;->sessionId:I

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/qualcomm/wfd/WfdStatus;->connectedDevice:Lcom/qualcomm/wfd/WfdDevice;

    .line 25
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 51
    iget v0, p0, Lcom/qualcomm/wfd/WfdStatus;->state:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 52
    iget v0, p0, Lcom/qualcomm/wfd/WfdStatus;->sessionId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 53
    iget-object v0, p0, Lcom/qualcomm/wfd/WfdStatus;->connectedDevice:Lcom/qualcomm/wfd/WfdDevice;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 54
    return-void
.end method
