.class final Lcom/qualcomm/wfd/ServiceUtil$1;
.super Ljava/lang/Object;
.source "ExtendedRemoteDisplay.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/ServiceUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 179
    const-string v1, "ExtendedRemoteDisplay.ServiceUtil"

    const-string v2, "Connection object created"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    const/4 v1, 0x1

    # setter for: Lcom/qualcomm/wfd/ServiceUtil;->mServiceAlreadyBound:Z
    invoke-static {v1}, Lcom/qualcomm/wfd/ServiceUtil;->access$102(Z)Z

    .line 181
    invoke-static {p2}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/wfd/service/ISessionManagerService;

    move-result-object v1

    # setter for: Lcom/qualcomm/wfd/ServiceUtil;->uniqueInstance:Lcom/qualcomm/wfd/service/ISessionManagerService;
    invoke-static {v1}, Lcom/qualcomm/wfd/ServiceUtil;->access$202(Lcom/qualcomm/wfd/service/ISessionManagerService;)Lcom/qualcomm/wfd/service/ISessionManagerService;

    .line 182
    const-class v2, Lcom/qualcomm/wfd/ServiceUtil;

    monitor-enter v2

    .line 183
    :try_start_0
    const-class v1, Lcom/qualcomm/wfd/ServiceUtil;

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 184
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    # getter for: Lcom/qualcomm/wfd/ServiceUtil;->eventHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->access$300()Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 187
    .local v0, "messageBound":Landroid/os/Message;
    # getter for: Lcom/qualcomm/wfd/ServiceUtil;->eventHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->access$300()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 188
    return-void

    .line 184
    .end local v0    # "messageBound":Landroid/os/Message;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 191
    const-string v0, "ExtendedRemoteDisplay.ServiceUtil"

    const-string v1, "Remote service disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    const/4 v0, 0x0

    # setter for: Lcom/qualcomm/wfd/ServiceUtil;->mServiceAlreadyBound:Z
    invoke-static {v0}, Lcom/qualcomm/wfd/ServiceUtil;->access$102(Z)Z

    .line 193
    return-void
.end method
