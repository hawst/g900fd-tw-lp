.class public final enum Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RtpTransportType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

.field public static final enum TCP:Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

.field public static final enum UDP:Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 504
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    const-string v1, "UDP"

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->UDP:Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    const-string v1, "TCP"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->TCP:Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    .line 503
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->UDP:Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->TCP:Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 503
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 503
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;
    .locals 1

    .prologue
    .line 503
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$RtpTransportType;

    return-object v0
.end method
