.class public abstract Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;
.super Landroid/os/Binder;
.source "ISessionManagerService.java"

# interfaces
.implements Lcom/qualcomm/wfd/service/ISessionManagerService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/service/ISessionManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.wfd.service.ISessionManagerService"

.field static final TRANSACTION_deinit:I = 0x6

.field static final TRANSACTION_enableDynamicBitrateAdaptation:I = 0x1d

.field static final TRANSACTION_getCommonCapabilities:I = 0x16

.field static final TRANSACTION_getCommonResolution:I = 0x1b

.field static final TRANSACTION_getConfigItems:I = 0x18

.field static final TRANSACTION_getNegotiatedResolution:I = 0x1c

.field static final TRANSACTION_getStatus:I = 0x3

.field static final TRANSACTION_getSupportedTypes:I = 0x2

.field static final TRANSACTION_getUIBCStatus:I = 0x1a

.field static final TRANSACTION_init:I = 0x4

.field static final TRANSACTION_pause:I = 0x11

.field static final TRANSACTION_play:I = 0x10

.field static final TRANSACTION_queryTCPTransportSupport:I = 0xe

.field static final TRANSACTION_registerHIDEventListener:I = 0x1e

.field static final TRANSACTION_sendEvent:I = 0x21

.field static final TRANSACTION_setAvPlaybackMode:I = 0xf

.field static final TRANSACTION_setBitrate:I = 0xa

.field static final TRANSACTION_setDecoderLatency:I = 0xd

.field static final TRANSACTION_setDeviceType:I = 0x1

.field static final TRANSACTION_setNegotiatedCapabilities:I = 0x17

.field static final TRANSACTION_setResolution:I = 0x9

.field static final TRANSACTION_setRtpTransport:I = 0xb

.field static final TRANSACTION_setSurface:I = 0x20

.field static final TRANSACTION_setSurfaceProp:I = 0x22

.field static final TRANSACTION_setUIBC:I = 0x19

.field static final TRANSACTION_standby:I = 0x13

.field static final TRANSACTION_startUibcSession:I = 0x14

.field static final TRANSACTION_startWfdSession:I = 0x7

.field static final TRANSACTION_stopUibcSession:I = 0x15

.field static final TRANSACTION_stopWfdSession:I = 0x8

.field static final TRANSACTION_tcpPlaybackControl:I = 0xc

.field static final TRANSACTION_teardown:I = 0x12

.field static final TRANSACTION_uibcRotate:I = 0x23

.field static final TRANSACTION_unregisterHIDEventListener:I = 0x1f

.field static final TRANSACTION_unregisterListener:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 45
    const-string v0, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/wfd/service/ISessionManagerService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 53
    if-nez p0, :cond_0

    .line 54
    const/4 v0, 0x0

    .line 60
    :goto_0
    return-object v0

    .line 56
    :cond_0
    const-string v1, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 57
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/wfd/service/ISessionManagerService;

    if-eqz v1, :cond_1

    .line 58
    check-cast v0, Lcom/qualcomm/wfd/service/ISessionManagerService;

    goto :goto_0

    .line 60
    :cond_1
    new-instance v0, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 64
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 68
    sparse-switch p1, :sswitch_data_0

    .line 482
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 72
    :sswitch_0
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :sswitch_1
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 80
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setDeviceType(I)I

    move-result v4

    .line 81
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 82
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 87
    .end local v0    # "_arg0":I
    .end local v4    # "_result":I
    :sswitch_2
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 89
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 90
    .local v1, "_arg0_length":I
    if-gez v1, :cond_0

    .line 91
    const/4 v0, 0x0

    .line 96
    .local v0, "_arg0":[I
    :goto_1
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->getSupportedTypes([I)I

    move-result v4

    .line 97
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 98
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 99
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    goto :goto_0

    .line 94
    .end local v0    # "_arg0":[I
    .end local v4    # "_result":I
    :cond_0
    new-array v0, v1, [I

    .restart local v0    # "_arg0":[I
    goto :goto_1

    .line 104
    .end local v0    # "_arg0":[I
    .end local v1    # "_arg0_length":I
    :sswitch_3
    const-string v7, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->getStatus()Lcom/qualcomm/wfd/WfdStatus;

    move-result-object v4

    .line 106
    .local v4, "_result":Lcom/qualcomm/wfd/WfdStatus;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 107
    if-eqz v4, :cond_1

    .line 108
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    invoke-virtual {v4, p3, v6}, Lcom/qualcomm/wfd/WfdStatus;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 112
    :cond_1
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 118
    .end local v4    # "_result":Lcom/qualcomm/wfd/WfdStatus;
    :sswitch_4
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/wfd/service/IWfdActionListener;

    move-result-object v0

    .line 122
    .local v0, "_arg0":Lcom/qualcomm/wfd/service/IWfdActionListener;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_2

    .line 123
    sget-object v5, Lcom/qualcomm/wfd/WfdDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/wfd/WfdDevice;

    .line 128
    .local v2, "_arg1":Lcom/qualcomm/wfd/WfdDevice;
    :goto_2
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->init(Lcom/qualcomm/wfd/service/IWfdActionListener;Lcom/qualcomm/wfd/WfdDevice;)I

    move-result v4

    .line 129
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 130
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 126
    .end local v2    # "_arg1":Lcom/qualcomm/wfd/WfdDevice;
    .end local v4    # "_result":I
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/qualcomm/wfd/WfdDevice;
    goto :goto_2

    .line 135
    .end local v0    # "_arg0":Lcom/qualcomm/wfd/service/IWfdActionListener;
    .end local v2    # "_arg1":Lcom/qualcomm/wfd/WfdDevice;
    :sswitch_5
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/qualcomm/wfd/service/IWfdActionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/wfd/service/IWfdActionListener;

    move-result-object v0

    .line 138
    .restart local v0    # "_arg0":Lcom/qualcomm/wfd/service/IWfdActionListener;
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->unregisterListener(Lcom/qualcomm/wfd/service/IWfdActionListener;)I

    move-result v4

    .line 139
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 140
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 145
    .end local v0    # "_arg0":Lcom/qualcomm/wfd/service/IWfdActionListener;
    .end local v4    # "_result":I
    :sswitch_6
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 146
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->deinit()I

    move-result v4

    .line 147
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 148
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 153
    .end local v4    # "_result":I
    :sswitch_7
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 155
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_3

    .line 156
    sget-object v5, Lcom/qualcomm/wfd/WfdDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/wfd/WfdDevice;

    .line 161
    .local v0, "_arg0":Lcom/qualcomm/wfd/WfdDevice;
    :goto_3
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->startWfdSession(Lcom/qualcomm/wfd/WfdDevice;)I

    move-result v4

    .line 162
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 163
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 159
    .end local v0    # "_arg0":Lcom/qualcomm/wfd/WfdDevice;
    .end local v4    # "_result":I
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/qualcomm/wfd/WfdDevice;
    goto :goto_3

    .line 168
    .end local v0    # "_arg0":Lcom/qualcomm/wfd/WfdDevice;
    :sswitch_8
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 169
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->stopWfdSession()I

    move-result v4

    .line 170
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 171
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 176
    .end local v4    # "_result":I
    :sswitch_9
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 178
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 180
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 181
    .local v2, "_arg1":I
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setResolution(II)I

    move-result v4

    .line 182
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 183
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 188
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v4    # "_result":I
    :sswitch_a
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 190
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 191
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setBitrate(I)I

    move-result v4

    .line 192
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 193
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 198
    .end local v0    # "_arg0":I
    .end local v4    # "_result":I
    :sswitch_b
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 200
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 202
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 204
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 205
    .local v3, "_arg2":I
    invoke-virtual {p0, v0, v2, v3}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setRtpTransport(III)I

    move-result v4

    .line 206
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 207
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 212
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_result":I
    :sswitch_c
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 214
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 216
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 217
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v0, v2}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->tcpPlaybackControl(II)I

    move-result v4

    .line 218
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 219
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 224
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v4    # "_result":I
    :sswitch_d
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 226
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 227
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setDecoderLatency(I)I

    move-result v4

    .line 228
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 229
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 234
    .end local v0    # "_arg0":I
    .end local v4    # "_result":I
    :sswitch_e
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 235
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->queryTCPTransportSupport()I

    move-result v4

    .line 236
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 237
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 242
    .end local v4    # "_result":I
    :sswitch_f
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 244
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 245
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setAvPlaybackMode(I)I

    move-result v4

    .line 246
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 247
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 252
    .end local v0    # "_arg0":I
    .end local v4    # "_result":I
    :sswitch_10
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 253
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->play()I

    move-result v4

    .line 254
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 255
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 260
    .end local v4    # "_result":I
    :sswitch_11
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 261
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->pause()I

    move-result v4

    .line 262
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 263
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 268
    .end local v4    # "_result":I
    :sswitch_12
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 269
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->teardown()I

    move-result v4

    .line 270
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 271
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 276
    .end local v4    # "_result":I
    :sswitch_13
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 277
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->standby()I

    move-result v4

    .line 278
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 279
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 284
    .end local v4    # "_result":I
    :sswitch_14
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 285
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->startUibcSession()I

    move-result v4

    .line 286
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 287
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 292
    .end local v4    # "_result":I
    :sswitch_15
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 293
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->stopUibcSession()I

    move-result v4

    .line 294
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 295
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 300
    .end local v4    # "_result":I
    :sswitch_16
    const-string v7, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 302
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 303
    .local v0, "_arg0":Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->getCommonCapabilities(Landroid/os/Bundle;)I

    move-result v4

    .line 304
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 305
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 306
    if-eqz v0, :cond_4

    .line 307
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 308
    invoke-virtual {v0, p3, v6}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 311
    :cond_4
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 317
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v4    # "_result":I
    :sswitch_17
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 319
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_5

    .line 320
    sget-object v5, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 325
    .restart local v0    # "_arg0":Landroid/os/Bundle;
    :goto_4
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setNegotiatedCapabilities(Landroid/os/Bundle;)I

    move-result v4

    .line 326
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 327
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 323
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v4    # "_result":I
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_4

    .line 332
    .end local v0    # "_arg0":Landroid/os/Bundle;
    :sswitch_18
    const-string v7, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 334
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 335
    .restart local v0    # "_arg0":Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->getConfigItems(Landroid/os/Bundle;)I

    move-result v4

    .line 336
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 337
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 338
    if-eqz v0, :cond_6

    .line 339
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 340
    invoke-virtual {v0, p3, v6}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 343
    :cond_6
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 349
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v4    # "_result":I
    :sswitch_19
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 350
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setUIBC()I

    move-result v4

    .line 351
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 352
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 357
    .end local v4    # "_result":I
    :sswitch_1a
    const-string v7, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 358
    invoke-virtual {p0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->getUIBCStatus()Z

    move-result v4

    .line 359
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 360
    if-eqz v4, :cond_7

    move v5, v6

    :cond_7
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 365
    .end local v4    # "_result":Z
    :sswitch_1b
    const-string v7, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 367
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 368
    .restart local v0    # "_arg0":Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->getCommonResolution(Landroid/os/Bundle;)I

    move-result v4

    .line 369
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 370
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 371
    if-eqz v0, :cond_8

    .line 372
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 373
    invoke-virtual {v0, p3, v6}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 376
    :cond_8
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 382
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v4    # "_result":I
    :sswitch_1c
    const-string v7, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 384
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 385
    .restart local v0    # "_arg0":Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->getNegotiatedResolution(Landroid/os/Bundle;)I

    move-result v4

    .line 386
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 387
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 388
    if-eqz v0, :cond_9

    .line 389
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 390
    invoke-virtual {v0, p3, v6}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 393
    :cond_9
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 399
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v4    # "_result":I
    :sswitch_1d
    const-string v7, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 401
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_a

    move v0, v6

    .line 402
    .local v0, "_arg0":Z
    :goto_5
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->enableDynamicBitrateAdaptation(Z)I

    move-result v4

    .line 403
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 404
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_a
    move v0, v5

    .line 401
    goto :goto_5

    .line 409
    :sswitch_1e
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 411
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/qualcomm/wfd/service/IHIDEventListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/wfd/service/IHIDEventListener;

    move-result-object v0

    .line 412
    .local v0, "_arg0":Lcom/qualcomm/wfd/service/IHIDEventListener;
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->registerHIDEventListener(Lcom/qualcomm/wfd/service/IHIDEventListener;)I

    move-result v4

    .line 413
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 414
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 419
    .end local v0    # "_arg0":Lcom/qualcomm/wfd/service/IHIDEventListener;
    .end local v4    # "_result":I
    :sswitch_1f
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 421
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/qualcomm/wfd/service/IHIDEventListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/wfd/service/IHIDEventListener;

    move-result-object v0

    .line 422
    .restart local v0    # "_arg0":Lcom/qualcomm/wfd/service/IHIDEventListener;
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->unregisterHIDEventListener(Lcom/qualcomm/wfd/service/IHIDEventListener;)I

    move-result v4

    .line 423
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 424
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 429
    .end local v0    # "_arg0":Lcom/qualcomm/wfd/service/IHIDEventListener;
    .end local v4    # "_result":I
    :sswitch_20
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 431
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_b

    .line 432
    sget-object v5, Landroid/view/Surface;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Surface;

    .line 437
    .local v0, "_arg0":Landroid/view/Surface;
    :goto_6
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setSurface(Landroid/view/Surface;)I

    move-result v4

    .line 438
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 439
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 435
    .end local v0    # "_arg0":Landroid/view/Surface;
    .end local v4    # "_result":I
    :cond_b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/view/Surface;
    goto :goto_6

    .line 444
    .end local v0    # "_arg0":Landroid/view/Surface;
    :sswitch_21
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 446
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_c

    .line 447
    sget-object v5, Landroid/view/InputEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/InputEvent;

    .line 452
    .local v0, "_arg0":Landroid/view/InputEvent;
    :goto_7
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->sendEvent(Landroid/view/InputEvent;)I

    move-result v4

    .line 453
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 454
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 450
    .end local v0    # "_arg0":Landroid/view/InputEvent;
    .end local v4    # "_result":I
    :cond_c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/view/InputEvent;
    goto :goto_7

    .line 459
    .end local v0    # "_arg0":Landroid/view/InputEvent;
    :sswitch_22
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 461
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 463
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 465
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 466
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v2, v3}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->setSurfaceProp(III)I

    move-result v4

    .line 467
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 468
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 473
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v4    # "_result":I
    :sswitch_23
    const-string v5, "com.qualcomm.wfd.service.ISessionManagerService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 475
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 476
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Lcom/qualcomm/wfd/service/ISessionManagerService$Stub;->uibcRotate(I)I

    move-result v4

    .line 477
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 478
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 68
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
