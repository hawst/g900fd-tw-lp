.class public interface abstract Lcom/qualcomm/wfd/service/IHIDEventListener;
.super Ljava/lang/Object;
.source "IHIDEventListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/wfd/service/IHIDEventListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract onHIDReprtDescRcv([B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onHIDReprtRcv([B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
