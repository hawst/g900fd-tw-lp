.class Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;
.super Landroid/os/Handler;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/ExtendedRemoteDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ExtendedRemoteDisplayCmdHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;


# direct methods
.method public constructor <init>(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 814
    iput-object p1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    .line 815
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 816
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 820
    const-string v1, "ExtendedRemoteDisplay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cmd handler received: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 896
    const-string v1, "ExtendedRemoteDisplay"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown cmd received: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    :goto_0
    return-void

    .line 825
    :pswitch_0
    sget-object v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;

    sget-object v2, Lcom/qualcomm/wfd/WFDState;->DEINIT:Lcom/qualcomm/wfd/WFDState;

    if-eq v1, v2, :cond_0

    .line 826
    const-string v1, "ExtendedRemoteDisplay"

    const-string v2, "Waiting to move to DEINIT"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 827
    sget-object v2, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sERDLock:Ljava/lang/Object;

    monitor-enter v2

    .line 829
    :try_start_0
    sget-object v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sERDLock:Ljava/lang/Object;

    const-wide/16 v4, 0x2710

    invoke-virtual {v1, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 836
    :goto_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 838
    :cond_0
    sget-object v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;

    sget-object v2, Lcom/qualcomm/wfd/WFDState;->DEINIT:Lcom/qualcomm/wfd/WFDState;

    if-eq v1, v2, :cond_1

    .line 839
    const-string v1, "ExtendedRemoteDisplay"

    const-string v2, "Something\'s gone kaput!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 840
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    const/4 v2, 0x1

    # invokes: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->notifyDisplayError(I)V
    invoke-static {v1, v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1600(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;I)V

    goto :goto_0

    .line 831
    :catch_0
    move-exception v0

    .line 832
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v1, "ExtendedRemoteDisplay"

    const-string v3, "InterruptedException while waiting to move to DEINIT"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 836
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 843
    :cond_1
    const-string v1, "ExtendedRemoteDisplay"

    const-string v2, "Started......"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    iget-object v2, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mIface:Ljava/lang/String;
    invoke-static {v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1900(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->createLocalWFDDevice(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$2000(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Ljava/lang/String;)V

    .line 846
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    iget-object v2, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mIface:Ljava/lang/String;
    invoke-static {v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1900(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->createPeerWFDDevice(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$2100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;Ljava/lang/String;)V

    .line 848
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mLocalWfdDevice:Lcom/qualcomm/wfd/WfdDevice;
    invoke-static {v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$400(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WfdDevice;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mPeerWfdDevice:Lcom/qualcomm/wfd/WfdDevice;
    invoke-static {v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1500(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/WfdDevice;

    move-result-object v1

    if-nez v1, :cond_3

    .line 849
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not start listening for remote display connection on \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mIface:Ljava/lang/String;
    invoke-static {v3}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1900(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 855
    :cond_3
    :try_start_3
    sget-object v1, Lcom/qualcomm/wfd/WFDState;->BINDING:Lcom/qualcomm/wfd/WFDState;

    sput-object v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;

    .line 856
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mEventHandler:Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;
    invoke-static {v2}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$300(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayEventHandler;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/qualcomm/wfd/ServiceUtil;->bindService(Landroid/content/Context;Landroid/os/Handler;)V
    :try_end_3
    .catch Lcom/qualcomm/wfd/ServiceUtil$ServiceFailedToBindException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 857
    :catch_1
    move-exception v0

    .line 858
    .local v0, "e":Lcom/qualcomm/wfd/ServiceUtil$ServiceFailedToBindException;
    const-string v1, "ExtendedRemoteDisplay"

    const-string v2, "ServiceFailedToBindException received"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 866
    .end local v0    # "e":Lcom/qualcomm/wfd/ServiceUtil$ServiceFailedToBindException;
    :pswitch_1
    const-string v1, "ExtendedRemoteDisplay"

    const-string v2, "Ending........."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 867
    const-string v1, "ExtendedRemoteDisplay"

    const-string v2, "Notify Display Disconnected"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 868
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    # invokes: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->notifyDisplayDisconnected()V
    invoke-static {v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$500(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)V

    .line 869
    sget-object v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;

    sget-object v2, Lcom/qualcomm/wfd/WFDState;->DEINIT:Lcom/qualcomm/wfd/WFDState;

    if-ne v1, v2, :cond_4

    .line 870
    const-string v1, "persist.sys.wfd.virtual"

    const-string v2, "0"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 873
    :cond_4
    sget-object v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;

    sget-object v2, Lcom/qualcomm/wfd/WFDState;->INITIALIZED:Lcom/qualcomm/wfd/WFDState;

    if-eq v1, v2, :cond_5

    sget-object v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;

    sget-object v2, Lcom/qualcomm/wfd/WFDState;->INITIALIZING:Lcom/qualcomm/wfd/WFDState;

    if-ne v1, v2, :cond_6

    .line 876
    :cond_5
    :try_start_4
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;

    move-result-object v1

    invoke-interface {v1}, Lcom/qualcomm/wfd/service/ISessionManagerService;->deinit()I

    .line 877
    const-string v1, "ExtendedRemoteDisplay"

    const-string v2, "Unbind the WFD service"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 878
    iget-object v1, p0, Lcom/qualcomm/wfd/ExtendedRemoteDisplay$ExtendedRemoteDisplayCmdHandler;->this$0:Lcom/qualcomm/wfd/ExtendedRemoteDisplay;

    # getter for: Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->access$1100(Lcom/qualcomm/wfd/ExtendedRemoteDisplay;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/wfd/ServiceUtil;->unbindService(Landroid/content/Context;)V

    .line 879
    const-string v1, "persist.sys.wfd.virtual"

    const-string v2, "0"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 880
    :catch_2
    move-exception v0

    .line 881
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "ExtendedRemoteDisplay"

    const-string v2, "RemoteException in deInit"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 885
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_6
    const-string v1, "ExtendedRemoteDisplay"

    const-string v2, "Teardown WFD Session"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 887
    :try_start_5
    sget-object v1, Lcom/qualcomm/wfd/WFDState;->TEARINGDOWN:Lcom/qualcomm/wfd/WFDState;

    sput-object v1, Lcom/qualcomm/wfd/ExtendedRemoteDisplay;->sState:Lcom/qualcomm/wfd/WFDState;

    .line 888
    invoke-static {}, Lcom/qualcomm/wfd/ServiceUtil;->getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;

    move-result-object v1

    invoke-interface {v1}, Lcom/qualcomm/wfd/service/ISessionManagerService;->teardown()I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_0

    .line 889
    :catch_3
    move-exception v0

    .line 890
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v1, "ExtendedRemoteDisplay"

    const-string v2, "RemoteException in teardown"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 822
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
