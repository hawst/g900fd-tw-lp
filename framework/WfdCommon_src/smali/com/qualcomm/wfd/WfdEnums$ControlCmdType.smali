.class public final enum Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ControlCmdType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

.field public static final enum FLUSH:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

.field public static final enum PAUSE:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

.field public static final enum PLAY:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

.field public static final enum STATUS:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 508
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    const-string v1, "FLUSH"

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->FLUSH:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    .line 509
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    const-string v1, "PLAY"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->PLAY:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    .line 510
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    const-string v1, "PAUSE"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    .line 511
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    const-string v1, "STATUS"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->STATUS:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    .line 507
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->FLUSH:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->PLAY:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->PAUSE:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->STATUS:Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 507
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 507
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;
    .locals 1

    .prologue
    .line 507
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$ControlCmdType;

    return-object v0
.end method
