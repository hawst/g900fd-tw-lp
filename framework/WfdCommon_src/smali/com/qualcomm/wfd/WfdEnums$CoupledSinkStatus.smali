.class public final enum Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;
.super Ljava/lang/Enum;
.source "WfdEnums.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/wfd/WfdEnums;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CoupledSinkStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

.field public static final enum COUPLED:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

.field public static final enum NOT_COUPLED:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

.field public static final enum TEARDOWN_COUPLING:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;


# instance fields
.field private code:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 541
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    const-string v1, "NOT_COUPLED"

    invoke-direct {v0, v1, v2, v2}, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->NOT_COUPLED:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    .line 542
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    const-string v1, "COUPLED"

    invoke-direct {v0, v1, v3, v3}, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->COUPLED:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    .line 543
    new-instance v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    const-string v1, "TEARDOWN_COUPLING"

    invoke-direct {v0, v1, v4, v4}, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->TEARDOWN_COUPLING:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    .line 540
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->NOT_COUPLED:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->COUPLED:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->TEARDOWN_COUPLING:Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "c"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 547
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 548
    iput p3, p0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->code:I

    .line 549
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 540
    const-class v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;
    .locals 1

    .prologue
    .line 540
    sget-object v0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->$VALUES:[Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    invoke-virtual {v0}, [Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;

    return-object v0
.end method


# virtual methods
.method public getCode()I
    .locals 1

    .prologue
    .line 552
    iget v0, p0, Lcom/qualcomm/wfd/WfdEnums$CoupledSinkStatus;->code:I

    return v0
.end method
