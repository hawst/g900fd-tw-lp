.class Lcom/qualcomm/wfd/ServiceUtil;
.super Ljava/lang/Object;
.source "ExtendedRemoteDisplay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/wfd/ServiceUtil$ServiceFailedToBindException;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ExtendedRemoteDisplay.ServiceUtil"

.field private static eventHandler:Landroid/os/Handler;

.field protected static mConnection:Landroid/content/ServiceConnection;

.field private static mServiceAlreadyBound:Z

.field private static uniqueInstance:Lcom/qualcomm/wfd/service/ISessionManagerService;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 116
    sput-object v1, Lcom/qualcomm/wfd/ServiceUtil;->uniqueInstance:Lcom/qualcomm/wfd/service/ISessionManagerService;

    .line 117
    const/4 v0, 0x0

    sput-boolean v0, Lcom/qualcomm/wfd/ServiceUtil;->mServiceAlreadyBound:Z

    .line 118
    sput-object v1, Lcom/qualcomm/wfd/ServiceUtil;->eventHandler:Landroid/os/Handler;

    .line 176
    new-instance v0, Lcom/qualcomm/wfd/ServiceUtil$1;

    invoke-direct {v0}, Lcom/qualcomm/wfd/ServiceUtil$1;-><init>()V

    sput-object v0, Lcom/qualcomm/wfd/ServiceUtil;->mConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    return-void
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 113
    sput-boolean p0, Lcom/qualcomm/wfd/ServiceUtil;->mServiceAlreadyBound:Z

    return p0
.end method

.method static synthetic access$202(Lcom/qualcomm/wfd/service/ISessionManagerService;)Lcom/qualcomm/wfd/service/ISessionManagerService;
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/wfd/service/ISessionManagerService;

    .prologue
    .line 113
    sput-object p0, Lcom/qualcomm/wfd/ServiceUtil;->uniqueInstance:Lcom/qualcomm/wfd/service/ISessionManagerService;

    return-object p0
.end method

.method static synthetic access$300()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/qualcomm/wfd/ServiceUtil;->eventHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static bindService(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "inEventHandler"    # Landroid/os/Handler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/qualcomm/wfd/ServiceUtil$ServiceFailedToBindException;
        }
    .end annotation

    .prologue
    .line 126
    sget-boolean v1, Lcom/qualcomm/wfd/ServiceUtil;->mServiceAlreadyBound:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/qualcomm/wfd/ServiceUtil;->uniqueInstance:Lcom/qualcomm/wfd/service/ISessionManagerService;

    if-nez v1, :cond_1

    .line 127
    :cond_0
    const-string v1, "ExtendedRemoteDisplay.ServiceUtil"

    const-string v2, "bindService- !AlreadyBound||uniqueInstance=null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.qualcomm.wfd.service.WfdService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 132
    .local v0, "serviceIntent":Landroid/content/Intent;
    const-string v1, "com.qualcomm.wfd.service"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 133
    sput-object p1, Lcom/qualcomm/wfd/ServiceUtil;->eventHandler:Landroid/os/Handler;

    .line 134
    sget-object v1, Lcom/qualcomm/wfd/ServiceUtil;->mConnection:Landroid/content/ServiceConnection;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 136
    const-string v1, "ExtendedRemoteDisplay.ServiceUtil"

    const-string v2, "Failed to connect to Provider service"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    new-instance v1, Lcom/qualcomm/wfd/ServiceUtil$ServiceFailedToBindException;

    const-string v2, "Failed to connect to Provider service"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/qualcomm/wfd/ServiceUtil$ServiceFailedToBindException;-><init>(Ljava/lang/String;Lcom/qualcomm/wfd/ServiceUtil$1;)V

    throw v1

    .line 141
    .end local v0    # "serviceIntent":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/qualcomm/wfd/service/ISessionManagerService;
    .locals 5

    .prologue
    .line 156
    const-class v2, Lcom/qualcomm/wfd/ServiceUtil;

    monitor-enter v2

    .local v0, "e":Ljava/lang/InterruptedException;
    :goto_0
    :try_start_0
    sget-object v1, Lcom/qualcomm/wfd/ServiceUtil;->uniqueInstance:Lcom/qualcomm/wfd/service/ISessionManagerService;

    if-nez v1, :cond_0

    .line 157
    const-string v1, "ExtendedRemoteDisplay.ServiceUtil"

    const-string v3, "Waiting for service to bind ..."

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    :try_start_1
    const-class v1, Lcom/qualcomm/wfd/ServiceUtil;

    invoke-virtual {v1}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 160
    :catch_0
    move-exception v0

    .line 161
    :try_start_2
    const-string v1, "ExtendedRemoteDisplay.ServiceUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "InterruptedException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 156
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 164
    :cond_0
    :try_start_3
    sget-object v1, Lcom/qualcomm/wfd/ServiceUtil;->uniqueInstance:Lcom/qualcomm/wfd/service/ISessionManagerService;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit v2

    return-object v1
.end method

.method protected static getmServiceAlreadyBound()Z
    .locals 1

    .prologue
    .line 121
    sget-boolean v0, Lcom/qualcomm/wfd/ServiceUtil;->mServiceAlreadyBound:Z

    return v0
.end method

.method public static unbindService(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 144
    sget-boolean v1, Lcom/qualcomm/wfd/ServiceUtil;->mServiceAlreadyBound:Z

    if-eqz v1, :cond_0

    .line 146
    :try_start_0
    sget-object v1, Lcom/qualcomm/wfd/ServiceUtil;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :goto_0
    const/4 v1, 0x0

    sput-boolean v1, Lcom/qualcomm/wfd/ServiceUtil;->mServiceAlreadyBound:Z

    .line 151
    const/4 v1, 0x0

    sput-object v1, Lcom/qualcomm/wfd/ServiceUtil;->uniqueInstance:Lcom/qualcomm/wfd/service/ISessionManagerService;

    .line 153
    :cond_0
    return-void

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "ExtendedRemoteDisplay.ServiceUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IllegalArgumentException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
