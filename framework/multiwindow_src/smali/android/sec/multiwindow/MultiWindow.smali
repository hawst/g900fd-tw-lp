.class public Landroid/sec/multiwindow/MultiWindow;
.super Landroid/sec/multiwindow/MultiWindowImpl;
.source "MultiWindow.java"

# interfaces
.implements Landroid/sec/multiwindow/IMultiWindow;


# static fields
.field protected static final TAG:Ljava/lang/String; = "MultiWindow"

.field private static sActionBarViewSetMultiWindowReserved:Ljava/lang/reflect/Method;

.field private static sShowMultiwindowPopupIcon:Ljava/lang/reflect/Method;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mArrangeable:Z

.field private mDefaultSize:Landroid/graphics/Rect;

.field private mMaximumSize:Landroid/graphics/Rect;

.field private mMinimumSize:Landroid/graphics/Rect;

.field private mWindowMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    sput-object v0, Landroid/sec/multiwindow/MultiWindow;->sShowMultiwindowPopupIcon:Ljava/lang/reflect/Method;

    .line 42
    sput-object v0, Landroid/sec/multiwindow/MultiWindow;->sActionBarViewSetMultiWindowReserved:Ljava/lang/reflect/Method;

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;)V
    .locals 10
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 133
    invoke-direct {p0, p1}, Landroid/sec/multiwindow/MultiWindowImpl;-><init>(Landroid/content/Context;)V

    .line 38
    iput-boolean v9, p0, Landroid/sec/multiwindow/MultiWindow;->mArrangeable:Z

    .line 135
    iput-object p1, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    .line 136
    iget-object v3, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 138
    .local v0, "activityClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iget-object v5, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    const-string v6, "getWindowMode"

    move-object v3, v4

    check-cast v3, [Ljava/lang/Class;

    invoke-virtual {p0, v0, v5, v6, v3}, Landroid/sec/multiwindow/MultiWindow;->putMethod(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 139
    iget-object v3, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    const-string v5, "setWindowMode"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Class;

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v6, v8

    sget-object v7, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v7, v6, v9

    invoke-virtual {p0, v0, v3, v5, v6}, Landroid/sec/multiwindow/MultiWindow;->putMethod(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 142
    iget-object v3, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    const-string v5, "getWindowInfo"

    check-cast v4, [Ljava/lang/Class;

    invoke-virtual {p0, v0, v3, v5, v4}, Landroid/sec/multiwindow/MultiWindow;->putMethod(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 143
    iget-object v3, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    const-string v4, "setWindowInfo"

    new-array v5, v9, [Ljava/lang/Class;

    const-class v6, Landroid/os/Bundle;

    aput-object v6, v5, v8

    invoke-virtual {p0, v0, v3, v4, v5}, Landroid/sec/multiwindow/MultiWindow;->putMethod(Ljava/lang/Class;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;)V

    .line 147
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->getWindowInfo()Landroid/os/Bundle;

    move-result-object v2

    .line 148
    .local v2, "winInfo":Landroid/os/Bundle;
    sget-object v3, Landroid/sec/multiwindow/Constants$Intent;->EXTRA_WINDOW_DEFAULT_SIZE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    iput-object v3, p0, Landroid/sec/multiwindow/MultiWindow;->mDefaultSize:Landroid/graphics/Rect;

    .line 150
    sget-object v3, Landroid/sec/multiwindow/Constants$Intent;->EXTRA_WINDOW_MINIMUM_SIZE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/graphics/Rect;

    iput-object v3, p0, Landroid/sec/multiwindow/MultiWindow;->mMinimumSize:Landroid/graphics/Rect;

    .line 153
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 154
    .local v1, "size":Landroid/graphics/Point;
    iget-object v3, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 155
    new-instance v3, Landroid/graphics/Rect;

    iget v4, v1, Landroid/graphics/Point;->x:I

    iget v5, v1, Landroid/graphics/Point;->y:I

    invoke-direct {v3, v8, v8, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v3, p0, Landroid/sec/multiwindow/MultiWindow;->mMaximumSize:Landroid/graphics/Rect;

    .line 156
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/sec/multiwindow/IMultiWindowService;Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Landroid/sec/multiwindow/IMultiWindowService;
    .param p3, "componentName"    # Landroid/content/ComponentName;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 581
    invoke-direct {p0, p1}, Landroid/sec/multiwindow/MultiWindowImpl;-><init>(Landroid/content/Context;)V

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/sec/multiwindow/MultiWindow;->mArrangeable:Z

    .line 581
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/sec/multiwindow/IMultiWindowService;Landroid/content/Intent;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Landroid/sec/multiwindow/IMultiWindowService;
    .param p3, "intent"    # Landroid/content/Intent;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 579
    invoke-direct {p0, p1}, Landroid/sec/multiwindow/MultiWindowImpl;-><init>(Landroid/content/Context;)V

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/sec/multiwindow/MultiWindow;->mArrangeable:Z

    .line 579
    return-void
.end method

.method private checkMode(I)Z
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 49
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    invoke-virtual {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->mode(I)I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkOption(I)Z
    .locals 1
    .param p1, "options"    # I

    .prologue
    .line 53
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    invoke-virtual {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->option(I)I

    move-result v0

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static createInstance(Landroid/app/Activity;)Landroid/sec/multiwindow/MultiWindow;
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v0, 0x0

    .line 118
    if-nez p0, :cond_1

    .line 124
    :cond_0
    :goto_0
    return-object v0

    .line 121
    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    sget-object v2, Landroid/sec/multiwindow/Constants$PackageManager;->FEATURE_MULTIWINDOW:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    new-instance v0, Landroid/sec/multiwindow/MultiWindow;

    invoke-direct {v0, p0}, Landroid/sec/multiwindow/MultiWindow;-><init>(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method private getLastSize()Landroid/graphics/Rect;
    .locals 3

    .prologue
    .line 64
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->getWindowInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 65
    .local v0, "info":Landroid/os/Bundle;
    sget-object v2, Landroid/sec/multiwindow/Constants$Intent;->EXTRA_WINDOW_LAST_SIZE:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 67
    .local v1, "lastSize":Landroid/graphics/Rect;
    if-eqz v1, :cond_0

    .end local v1    # "lastSize":Landroid/graphics/Rect;
    :goto_0
    return-object v1

    .restart local v1    # "lastSize":Landroid/graphics/Rect;
    :cond_0
    iget-object v1, p0, Landroid/sec/multiwindow/MultiWindow;->mDefaultSize:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method private getWindowInfo()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 83
    const-string v1, "getWindowInfo"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v0}, Landroid/sec/multiwindow/MultiWindow;->invoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    return-object v0
.end method

.method protected static isEnabledComponentName(Landroid/content/Context;Landroid/content/ComponentName;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 623
    const/4 v0, 0x1

    return v0
.end method

.method private setLastSize(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->getWindowInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 58
    .local v0, "info":Landroid/os/Bundle;
    sget-object v1, Landroid/sec/multiwindow/Constants$Intent;->EXTRA_WINDOW_LAST_SIZE:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 60
    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->setWindowInfo(Landroid/os/Bundle;)V

    .line 61
    return-void
.end method

.method private setWindowInfo(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 79
    const-string v0, "setWindowInfo"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/sec/multiwindow/MultiWindow;->invoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    return-void
.end method

.method private setWindowMode(I)V
    .locals 5
    .param p1, "mode"    # I

    .prologue
    const/4 v4, 0x1

    .line 71
    const-string v0, "setWindowMode"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/sec/multiwindow/MultiWindow;->invoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    return-void
.end method

.method private setWindowParams()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 87
    iget-object v2, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 89
    .local v1, "p":Landroid/view/WindowManager$LayoutParams;
    iget v2, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    invoke-virtual {p0, v2}, Landroid/sec/multiwindow/MultiWindow;->mode(I)I

    move-result v2

    sget v3, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_FREESTYLE:I

    if-ne v2, v3, :cond_0

    .line 90
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->getLastSize()Landroid/graphics/Rect;

    move-result-object v0

    .line 91
    .local v0, "newSize":Landroid/graphics/Rect;
    iget v2, v0, Landroid/graphics/Rect;->left:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 92
    iget v2, v0, Landroid/graphics/Rect;->top:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 93
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 94
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 95
    const/16 v2, 0x33

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 104
    .end local v0    # "newSize":Landroid/graphics/Rect;
    :goto_0
    iget-object v2, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 105
    return-void

    .line 97
    :cond_0
    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 98
    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 99
    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 100
    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 101
    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    goto :goto_0
.end method

.method private updateWindowMode()V
    .locals 2

    .prologue
    .line 75
    const-string v1, "getWindowMode"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Object;

    invoke-virtual {p0, v1, v0}, Landroid/sec/multiwindow/MultiWindow;->invoke(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 76
    return-void
.end method


# virtual methods
.method public finish()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 587
    const/4 v0, 0x0

    return v0
.end method

.method public finish(I)Z
    .locals 1
    .param p1, "taskId"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 585
    const/4 v0, 0x0

    return v0
.end method

.method public fitToHalf(I)Z
    .locals 1
    .param p1, "direction"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 617
    const/4 v0, 0x0

    return v0
.end method

.method public getArrangeable()Z
    .locals 1

    .prologue
    .line 543
    iget-boolean v0, p0, Landroid/sec/multiwindow/MultiWindow;->mArrangeable:Z

    return v0
.end method

.method public getHeight()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 611
    const/4 v0, -0x1

    return v0
.end method

.method public getMinimumSize()Landroid/graphics/Point;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 615
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method

.method public getMultiWindowEnabled()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 518
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 519
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_RESIZE:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->checkOption(I)Z

    move-result v0

    return v0
.end method

.method public getRect()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 473
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_SCALE:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->checkOption(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 474
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 475
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->getLastSize()Landroid/graphics/Rect;

    move-result-object v0

    .line 478
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/sec/multiwindow/MultiWindow;->mMaximumSize:Landroid/graphics/Rect;

    goto :goto_0
.end method

.method public getRect(I)Landroid/graphics/Rect;
    .locals 1
    .param p1, "taskId"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 607
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    return-object v0
.end method

.method public getWidth()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 609
    const/4 v0, -0x1

    return v0
.end method

.method public getZone()I
    .locals 2

    .prologue
    .line 551
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 552
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_MASK:I

    and-int/2addr v0, v1

    return v0
.end method

.method public isMaximized()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 599
    const/4 v0, 0x0

    return v0
.end method

.method public isMaximized(I)Z
    .locals 1
    .param p1, "taskId"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 601
    const/4 v0, 0x0

    return v0
.end method

.method public isMinimized()Z
    .locals 1

    .prologue
    .line 199
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 200
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_FREESTYLE:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->checkMode(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_MINIMIZED:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->checkOption(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMultiWindow()Z
    .locals 1

    .prologue
    .line 176
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 177
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_FREESTYLE:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->checkMode(I)Z

    move-result v0

    return v0
.end method

.method public isNormalWindow()Z
    .locals 1

    .prologue
    .line 165
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 166
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_NORMAL:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->checkMode(I)Z

    move-result v0

    return v0
.end method

.method public isPinup()Z
    .locals 1

    .prologue
    .line 187
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 188
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_FREESTYLE:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->checkMode(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->checkOption(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStartedAsPinup()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 577
    const/4 v0, 0x0

    return v0
.end method

.method public isStartingSplitScreen(Landroid/content/ComponentName;)Z
    .locals 1
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 603
    const/4 v0, 0x0

    return v0
.end method

.method public maximize()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 589
    const/4 v0, 0x0

    return v0
.end method

.method public minimize()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 595
    const/4 v0, 0x0

    return v0
.end method

.method public minimize(I)Z
    .locals 1
    .param p1, "taskId"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 597
    const/4 v0, 0x0

    return v0
.end method

.method public minimize(Z)Z
    .locals 3
    .param p1, "flag"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 407
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->isMultiWindow()Z

    move-result v2

    if-nez v2, :cond_1

    .line 432
    :cond_0
    :goto_0
    return v0

    .line 411
    :cond_1
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 413
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->getMultiWindowEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 417
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->isMinimized()Z

    move-result v0

    if-ne p1, v0, :cond_2

    move v0, v1

    .line 418
    goto :goto_0

    .line 421
    :cond_2
    if-eqz p1, :cond_3

    .line 422
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v2, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_MINIMIZED:I

    or-int/2addr v0, v2

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 423
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v2, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    or-int/2addr v0, v2

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 429
    :goto_1
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->setWindowMode(I)V

    .line 430
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->setWindowParams()V

    move v0, v1

    .line 432
    goto :goto_0

    .line 426
    :cond_3
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v2, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_MINIMIZED:I

    xor-int/lit8 v2, v2, -0x1

    and-int/2addr v0, v2

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    goto :goto_1
.end method

.method public multiWindow()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 295
    invoke-virtual {p0, v0, v0}, Landroid/sec/multiwindow/MultiWindow;->multiWindow(ZZ)Z

    move-result v0

    return v0
.end method

.method public multiWindow(Z)Z
    .locals 1
    .param p1, "pinup"    # Z

    .prologue
    .line 306
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/sec/multiwindow/MultiWindow;->multiWindow(ZZ)Z

    move-result v0

    return v0
.end method

.method public multiWindow(ZZ)Z
    .locals 1
    .param p1, "pinup"    # Z
    .param p2, "minimize"    # Z

    .prologue
    .line 310
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_UNKNOWN:I

    invoke-virtual {p0, p1, p2, v0}, Landroid/sec/multiwindow/MultiWindow;->multiWindow(ZZI)Z

    move-result v0

    return v0
.end method

.method public multiWindow(ZZI)Z
    .locals 2
    .param p1, "pinup"    # Z
    .param p2, "minimize"    # Z
    .param p3, "zone"    # I

    .prologue
    const/4 v0, 0x0

    .line 322
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 324
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->getMultiWindowEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 363
    :cond_0
    :goto_0
    return v0

    .line 328
    :cond_1
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->isMultiWindow()Z

    move-result v1

    if-nez v1, :cond_0

    .line 332
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_MASK:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 333
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_FREESTYLE:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 335
    if-eqz p1, :cond_2

    .line 336
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 342
    :goto_1
    if-eqz p2, :cond_3

    .line 343
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_MINIMIZED:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 349
    :goto_2
    sget v0, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_UNKNOWN:I

    if-ne p3, v0, :cond_4

    .line 360
    :goto_3
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->setWindowParams()V

    .line 361
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->setWindowMode(I)V

    .line 363
    const/4 v0, 0x1

    goto :goto_0

    .line 339
    :cond_2
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    goto :goto_1

    .line 346
    :cond_3
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_MINIMIZED:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    goto :goto_2

    .line 356
    :cond_4
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_MASK:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 357
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    or-int/2addr v0, p3

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    goto :goto_3
.end method

.method public normalWindow()Z
    .locals 2

    .prologue
    .line 211
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 213
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_MASK:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 214
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_NORMAL:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 215
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 216
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_MINIMIZED:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 218
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->setWindowMode(I)V

    .line 219
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->setWindowParams()V

    .line 220
    const/4 v0, 0x1

    return v0
.end method

.method public pinUp(Z)Z
    .locals 1
    .param p1, "value"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 619
    const/4 v0, 0x0

    return v0
.end method

.method public pinUp(ZI)Z
    .locals 1
    .param p1, "value"    # Z
    .param p2, "taskId"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 621
    const/4 v0, 0x0

    return v0
.end method

.method public pinup(Z)Z
    .locals 2
    .param p1, "flag"    # Z

    .prologue
    const/4 v0, 0x0

    .line 375
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->isMultiWindow()Z

    move-result v1

    if-nez v1, :cond_1

    .line 395
    :cond_0
    :goto_0
    return v0

    .line 379
    :cond_1
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 381
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->getMultiWindowEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 385
    if-eqz p1, :cond_2

    .line 386
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    or-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 392
    :goto_1
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->setWindowMode(I)V

    .line 393
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->setWindowParams()V

    .line 395
    const/4 v0, 0x1

    goto :goto_0

    .line 389
    :cond_2
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_COMMON_PINUP:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    goto :goto_1
.end method

.method public relayout(Landroid/graphics/Rect;)Z
    .locals 1
    .param p1, "rect"    # Landroid/graphics/Rect;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 605
    const/4 v0, 0x0

    return v0
.end method

.method public restore()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 591
    const/4 v0, 0x0

    return v0
.end method

.method public restore(I)Z
    .locals 1
    .param p1, "taskId"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 593
    const/4 v0, 0x1

    return v0
.end method

.method public setArrangeable(Z)Z
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 531
    iput-boolean p1, p0, Landroid/sec/multiwindow/MultiWindow;->mArrangeable:Z

    .line 532
    const/4 v0, 0x1

    return v0
.end method

.method public setMinimumSize(II)Z
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 613
    const/4 v0, 0x0

    return v0
.end method

.method public setMultiWindowEnabled(Z)Z
    .locals 1
    .param p1, "flag"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 507
    const/4 v0, 0x0

    return v0
.end method

.method public setRect(Landroid/graphics/Rect;)Z
    .locals 2
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 453
    if-eqz p1, :cond_0

    .line 454
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Landroid/sec/multiwindow/MultiWindow;->mMinimumSize:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v1, p0, Landroid/sec/multiwindow/MultiWindow;->mMinimumSize:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-lt v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Landroid/sec/multiwindow/MultiWindow;->mMaximumSize:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-gt v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget-object v1, p0, Landroid/sec/multiwindow/MultiWindow;->mMaximumSize:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-gt v0, v1, :cond_0

    .line 456
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 457
    invoke-direct {p0, p1}, Landroid/sec/multiwindow/MultiWindow;->setLastSize(Landroid/graphics/Rect;)V

    .line 458
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->setWindowParams()V

    .line 460
    const/4 v0, 0x1

    .line 464
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setZone(I)V
    .locals 2
    .param p1, "zone"    # I

    .prologue
    .line 559
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->updateWindowMode()V

    .line 560
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_MASK:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    .line 561
    iget v0, p0, Landroid/sec/multiwindow/MultiWindow;->mWindowMode:I

    sget v1, Landroid/sec/multiwindow/Constants$WindowManagerPolicy;->WINDOW_MODE_OPTION_SPLIT_ZONE_MASK:I

    and-int/2addr v1, p1

    or-int/2addr v0, v1

    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->setWindowMode(I)V

    .line 562
    return-void
.end method

.method public showMultiWindowActionBarIcon(Z)Z
    .locals 8
    .param p1, "isShow"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 230
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->getMultiWindowEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 252
    :cond_0
    :goto_0
    return v2

    .line 234
    :cond_1
    iget-object v4, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    const v5, 0x102043d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/internal/widget/ActionBarView;

    .line 236
    .local v0, "actionBarView":Lcom/android/internal/widget/ActionBarView;
    if-eqz v0, :cond_0

    .line 241
    :try_start_0
    sget-object v2, Landroid/sec/multiwindow/MultiWindow;->sActionBarViewSetMultiWindowReserved:Ljava/lang/reflect/Method;

    if-nez v2, :cond_2

    .line 242
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v4, "setMultiWindowReserved"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Landroid/sec/multiwindow/MultiWindow;->sActionBarViewSetMultiWindowReserved:Ljava/lang/reflect/Method;

    .line 245
    :cond_2
    sget-object v2, Landroid/sec/multiwindow/MultiWindow;->sActionBarViewSetMultiWindowReserved:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_3

    .line 246
    sget-object v2, Landroid/sec/multiwindow/MultiWindow;->sActionBarViewSetMultiWindowReserved:Ljava/lang/reflect/Method;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    move v2, v3

    .line 252
    goto :goto_0

    .line 248
    :catch_0
    move-exception v1

    .line 249
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public showMultiWindowPopupIcon(Z)Z
    .locals 8
    .param p1, "isShow"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 262
    invoke-virtual {p0}, Landroid/sec/multiwindow/MultiWindow;->getMultiWindowEnabled()Z

    move-result v4

    if-nez v4, :cond_1

    .line 285
    :cond_0
    :goto_0
    return v2

    .line 266
    :cond_1
    sget-object v4, Landroid/sec/multiwindow/MultiWindow;->sShowMultiwindowPopupIcon:Ljava/lang/reflect/Method;

    if-nez v4, :cond_2

    .line 268
    :try_start_0
    const-string v4, "com.android.internal.policy.impl.PhoneWindow"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 269
    .local v0, "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v4, "showMultiwindowPopupIcon"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    sput-object v4, Landroid/sec/multiwindow/MultiWindow;->sShowMultiwindowPopupIcon:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 275
    .end local v0    # "cl":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_2
    :goto_1
    sget-object v4, Landroid/sec/multiwindow/MultiWindow;->sShowMultiwindowPopupIcon:Ljava/lang/reflect/Method;

    if-eqz v4, :cond_0

    .line 280
    :try_start_1
    sget-object v2, Landroid/sec/multiwindow/MultiWindow;->sShowMultiwindowPopupIcon:Ljava/lang/reflect/Method;

    iget-object v4, p0, Landroid/sec/multiwindow/MultiWindow;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    move v2, v3

    .line 285
    goto :goto_0

    .line 270
    :catch_0
    move-exception v1

    .line 271
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 281
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 282
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method public start()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 583
    const/4 v0, 0x0

    return v0
.end method

.method public stayResume(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 568
    invoke-direct {p0}, Landroid/sec/multiwindow/MultiWindow;->getWindowInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 569
    .local v0, "winInfo":Landroid/os/Bundle;
    sget-object v1, Landroid/sec/multiwindow/Constants$Intent;->EXTRA_STAY_RESUME:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 570
    invoke-direct {p0, v0}, Landroid/sec/multiwindow/MultiWindow;->setWindowInfo(Landroid/os/Bundle;)V

    .line 571
    return-void
.end method
