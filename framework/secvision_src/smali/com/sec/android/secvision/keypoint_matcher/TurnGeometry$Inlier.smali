.class public Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;
.super Ljava/lang/Object;
.source "TurnGeometry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Inlier"
.end annotation


# instance fields
.field public m_pt1:Lcom/sec/android/secvision/solutions/virtualtour/Point;

.field public m_pt2:Lcom/sec/android/secvision/solutions/virtualtour/Point;


# direct methods
.method public constructor <init>(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)V
    .locals 1
    .param p1, "pt1"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .param p2, "pt2"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object v0, p0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;->m_pt1:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 32
    iput-object v0, p0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;->m_pt2:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 35
    iput-object p1, p0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;->m_pt1:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 36
    iput-object p2, p0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;->m_pt2:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 37
    return-void
.end method
