.class public Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;
.super Ljava/lang/Object;
.source "TurnGeometry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$1;,
        Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;,
        Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;,
        Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field public static m_aSearchRects:[Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

.field private static m_fF:F

.field public static m_fFOV_h:F

.field private static m_fImageHeight:F

.field private static m_fImageWidth:F

.field private static m_fpx:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 16
    const-string v0, "TurnGeometry"

    sput-object v0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->TAG:Ljava/lang/String;

    .line 19
    const/high16 v0, 0x428c0000    # 70.0f

    sput v0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fFOV_h:F

    .line 20
    const/high16 v0, 0x44000000    # 512.0f

    sput v0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fpx:F

    .line 21
    const v0, 0x4436d1ba

    sput v0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fF:F

    .line 23
    const/high16 v0, 0x44800000    # 1024.0f

    sput v0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fImageWidth:F

    .line 24
    const v0, 0x442a8000    # 682.0f

    sput v0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fImageHeight:F

    .line 27
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    invoke-direct {v2}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;-><init>()V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    invoke-direct {v2}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;-><init>()V

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_aSearchRects:[Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    return-void
.end method

.method private static RayIntersectRay(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .locals 8
    .param p0, "ptPointA"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .param p1, "vDirA"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .param p2, "ptPointB"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .param p3, "vDirB"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 117
    iget v4, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    iget v5, p3, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    mul-float/2addr v4, v5

    iget v5, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v6, p3, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    mul-float/2addr v5, v6

    sub-float v0, v4, v5

    .line 120
    .local v0, "denominator":F
    cmpl-float v4, v0, v7

    if-nez v4, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-object v3

    .line 125
    :cond_1
    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v5, p3, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    mul-float/2addr v4, v5

    iget v5, p3, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v6, p2, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p2, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v6, p3, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    iget v5, p3, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v6, p0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    div-float v1, v4, v0

    .line 126
    .local v1, "u":F
    iget v4, p3, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    cmpl-float v4, v4, v7

    if-eqz v4, :cond_2

    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    iget v5, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    mul-float/2addr v5, v1

    add-float/2addr v4, v5

    iget v5, p2, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    sub-float/2addr v4, v5

    iget v5, p3, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    div-float v2, v4, v5

    .line 128
    .local v2, "v":F
    :goto_1
    cmpl-float v4, v1, v7

    if-lez v4, :cond_0

    cmpl-float v4, v2, v7

    if-lez v4, :cond_0

    .line 129
    new-instance v3, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    iget v5, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    mul-float/2addr v5, v1

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v6, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    mul-float/2addr v6, v1

    add-float/2addr v5, v6

    invoke-direct {v3, v4, v5}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    goto :goto_0

    .line 126
    .end local v2    # "v":F
    :cond_2
    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v5, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    mul-float/2addr v5, v1

    add-float/2addr v4, v5

    iget v5, p2, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    sub-float/2addr v4, v5

    iget v5, p3, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    div-float v2, v4, v5

    goto :goto_1
.end method

.method public static computeCameraParameters(II)V
    .locals 7
    .param p0, "imageWidth"    # I
    .param p1, "imageHeight"    # I

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 56
    int-to-float v1, p0

    sput v1, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fImageWidth:F

    .line 57
    int-to-float v1, p1

    sput v1, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fImageHeight:F

    .line 59
    int-to-float v1, p0

    div-float/2addr v1, v6

    sput v1, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fpx:F

    .line 61
    const/high16 v1, 0x428c0000    # 70.0f

    sput v1, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fFOV_h:F

    .line 62
    sget v1, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fImageWidth:F

    sget v2, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fImageHeight:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 63
    const/high16 v1, 0x3f000000    # 0.5f

    sget v2, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fImageHeight:F

    mul-float/2addr v1, v2

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    sget v4, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fFOV_h:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v2, v2

    div-float v0, v1, v2

    .line 64
    .local v0, "fF":F
    sget v1, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fpx:F

    div-float/2addr v1, v0

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v1, v6

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v2

    double-to-float v1, v2

    sput v1, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fFOV_h:F

    .line 67
    .end local v0    # "fF":F
    :cond_0
    sget v1, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fpx:F

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    sget v4, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fFOV_h:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    const-wide v4, 0x4076800000000000L    # 360.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v2, v2

    div-float/2addr v1, v2

    sput v1, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fF:F

    .line 68
    return-void
.end method

.method private static computeOverlappingWidth(F)F
    .locals 13
    .param p0, "fAngle"    # F

    .prologue
    const/4 v12, 0x0

    .line 95
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    sget v7, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fFOV_h:F

    float-to-double v10, v7

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    double-to-float v0, v8

    .line 97
    .local v0, "fFOVh2_rad":F
    new-instance v3, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    invoke-direct {v3, v12, v12}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    .line 98
    .local v3, "ptP1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    new-instance v4, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    sget v7, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fF:F

    neg-float v7, v7

    float-to-double v8, p0

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v8, v8

    mul-float/2addr v7, v8

    sget v8, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fpx:F

    float-to-double v10, p0

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    double-to-float v9, v10

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    sget v8, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fF:F

    float-to-double v10, p0

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    double-to-float v9, v10

    mul-float/2addr v8, v9

    sget v9, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fpx:F

    float-to-double v10, p0

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    double-to-float v10, v10

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    invoke-direct {v4, v7, v8}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    .line 101
    .local v4, "ptP2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    new-instance v5, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v7, v8

    neg-float v7, v7

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v8, v8

    invoke-direct {v5, v7, v8}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    .line 102
    .local v5, "vDir1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    new-instance v6, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    float-to-double v8, p0

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v7, v8

    neg-float v7, v7

    float-to-double v8, p0

    invoke-static {v8, v9}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    double-to-float v8, v8

    neg-float v8, v8

    invoke-direct {v6, v7, v8}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    .line 104
    .local v6, "vDir2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    const/4 v1, 0x0

    .line 105
    .local v1, "fd":F
    invoke-static {v3, v5, v4, v6}, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->RayIntersectRay(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-result-object v2

    .line 106
    .local v2, "ptI":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    if-eqz v2, :cond_0

    .line 107
    invoke-virtual {v2, v4}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->getDistance(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v1

    .line 110
    :cond_0
    return v1
.end method

.method public static computeSearchRegions(F)V
    .locals 10
    .param p0, "fAngle"    # F

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 73
    const/high16 v3, 0x40a00000    # 5.0f

    sget v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fFOV_h:F

    mul-float/2addr v3, v5

    const/high16 v5, 0x428c0000    # 70.0f

    div-float/2addr v3, v5

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    double-to-float v0, v6

    .line 74
    .local v0, "fEpsilon":F
    cmpl-float v3, p0, v4

    if-lez v3, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 76
    .local v1, "fSign":F
    :goto_0
    mul-float v3, v1, v0

    sub-float/2addr p0, v3

    .line 77
    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {v4, v3}, Ljava/lang/Math;->max(FF)F

    move-result p0

    .line 79
    invoke-static {p0}, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->computeOverlappingWidth(F)F

    move-result v2

    .line 81
    .local v2, "fd":F
    sget-object v3, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_aSearchRects:[Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    aget-object v5, v3, v8

    cmpl-float v3, v1, v4

    if-lez v3, :cond_1

    sget v3, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fImageWidth:F

    sub-float/2addr v3, v2

    :goto_1
    iput v3, v5, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    .line 82
    sget-object v3, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_aSearchRects:[Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    aget-object v3, v3, v8

    iput v4, v3, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    .line 83
    sget-object v3, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_aSearchRects:[Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    aget-object v3, v3, v8

    iput v2, v3, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    .line 84
    sget-object v3, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_aSearchRects:[Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    aget-object v3, v3, v8

    sget v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fImageHeight:F

    iput v5, v3, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    .line 86
    sget-object v3, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_aSearchRects:[Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    aget-object v5, v3, v9

    cmpg-float v3, v1, v4

    if-gez v3, :cond_2

    sget v3, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fImageWidth:F

    sub-float/2addr v3, v2

    :goto_2
    iput v3, v5, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    .line 87
    sget-object v3, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_aSearchRects:[Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    aget-object v3, v3, v9

    iput v4, v3, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    .line 88
    sget-object v3, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_aSearchRects:[Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    aget-object v3, v3, v9

    iput v2, v3, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    .line 89
    sget-object v3, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_aSearchRects:[Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    aget-object v3, v3, v9

    sget v4, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_fImageHeight:F

    iput v4, v3, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    .line 90
    return-void

    .line 74
    .end local v1    # "fSign":F
    .end local v2    # "fd":F
    :cond_0
    const/high16 v1, -0x40800000    # -1.0f

    goto :goto_0

    .restart local v1    # "fSign":F
    .restart local v2    # "fd":F
    :cond_1
    move v3, v4

    .line 81
    goto :goto_1

    :cond_2
    move v3, v4

    .line 86
    goto :goto_2
.end method

.method public static computeTurn(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/secvision/solutions/virtualtour/Point;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/secvision/solutions/virtualtour/Point;",
            ">;)",
            "Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;"
        }
    .end annotation

    .prologue
    .line 140
    .local p0, "inLst1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    .local p1, "inLst2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    new-instance v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;

    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-direct {v5, v0}, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;-><init>(Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$1;)V

    .line 141
    .local v5, "bestSolution":Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v20

    iput-object v0, v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_aInliers:Ljava/util/ArrayList;

    .line 144
    const-wide/high16 v20, 0x4039000000000000L    # 25.0

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-float v4, v0

    .line 146
    .local v4, "MAX_DISTANCE2":F
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 147
    .local v13, "nElements":I
    const/16 v20, 0x2

    move/from16 v0, v20

    if-lt v13, v0, :cond_4

    .line 149
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 154
    .local v7, "curInliers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-wide v20, 0x3f847ae140000000L    # 0.009999999776482582

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->log(D)D

    move-result-wide v20

    const-wide/high16 v22, 0x3ff0000000000000L    # 1.0

    const/high16 v24, 0x40000000    # 2.0f

    int-to-float v0, v13

    move/from16 v25, v0

    div-float v24, v24, v25

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    invoke-static/range {v24 .. v27}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    sub-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->log(D)D

    move-result-wide v22

    div-double v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v15, v0

    .line 156
    .local v15, "nIterations":I
    const/4 v12, 0x0

    .local v12, "it":I
    :goto_0
    if-ge v12, v15, :cond_4

    .line 158
    int-to-double v0, v13

    move-wide/from16 v20, v0

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v22

    mul-double v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->floor(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v14, v0

    .line 160
    .local v14, "nIndex":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 161
    .local v17, "pt1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 163
    .local v18, "pt2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    new-instance v8, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    move/from16 v20, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    move/from16 v21, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v8, v0, v1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    .line 166
    .local v8, "curPivot":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 167
    const/4 v6, 0x0

    .line 168
    .local v6, "curDistance":F
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v20

    if-ge v10, v0, :cond_1

    .line 169
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    .end local v17    # "pt1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    check-cast v17, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 170
    .restart local v17    # "pt1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    .end local v18    # "pt2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    check-cast v18, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 172
    .restart local v18    # "pt2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    move-object/from16 v0, v18

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    move/from16 v20, v0

    iget v0, v8, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    move/from16 v21, v0

    add-float v20, v20, v21

    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    const-wide/high16 v22, 0x4000000000000000L    # 2.0

    invoke-static/range {v20 .. v23}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v20

    move-object/from16 v0, v18

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    move/from16 v22, v0

    iget v0, v8, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    move/from16 v23, v0

    add-float v22, v22, v23

    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    move/from16 v23, v0

    sub-float v22, v22, v23

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    invoke-static/range {v22 .. v25}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v22

    add-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v9, v0

    .line 173
    .local v9, "d":F
    cmpg-float v20, v9, v4

    if-gtz v20, :cond_0

    .line 174
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    add-float/2addr v6, v9

    .line 168
    :cond_0
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 180
    .end local v9    # "d":F
    :cond_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v20

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_3

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v20

    iget-object v0, v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_aInliers:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-gt v0, v1, :cond_2

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v20

    iget-object v0, v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_aInliers:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_3

    iget v0, v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_fAccumError:F

    move/from16 v20, v0

    cmpg-float v20, v6, v20

    if-gez v20, :cond_3

    .line 182
    :cond_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/ArrayList;

    move-object/from16 v0, v20

    iput-object v0, v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_aInliers:Ljava/util/ArrayList;

    .line 183
    iput v6, v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_fAccumError:F

    .line 184
    iput-object v8, v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_ptPivot:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 185
    iput v14, v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_nPivotIndex:I

    .line 189
    :cond_3
    const-wide v20, 0x3f847ae140000000L    # 0.009999999776482582

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->log(D)D

    move-result-wide v20

    const-wide/high16 v22, 0x3ff0000000000000L    # 1.0

    const/16 v24, 0x2

    iget-object v0, v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_aInliers:Ljava/util/ArrayList;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v25

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->max(II)I

    move-result v24

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    int-to-float v0, v13

    move/from16 v25, v0

    div-float v24, v24, v25

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    invoke-static/range {v24 .. v27}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v24

    sub-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->log(D)D

    move-result-wide v22

    div-double v20, v20, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v15, v0

    .line 156
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 194
    .end local v6    # "curDistance":F
    .end local v7    # "curInliers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v8    # "curPivot":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .end local v10    # "i":I
    .end local v12    # "it":I
    .end local v14    # "nIndex":I
    .end local v15    # "nIterations":I
    .end local v17    # "pt1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .end local v18    # "pt2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    :cond_4
    iget-object v0, v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_ptPivot:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v20, v0

    if-nez v20, :cond_6

    .line 195
    const/16 v19, 0x0

    .line 223
    :cond_5
    :goto_2
    return-object v19

    .line 198
    :cond_6
    new-instance v16, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;

    iget v0, v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_nPivotIndex:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    iget v0, v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_nPivotIndex:I

    move/from16 v21, v0

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;-><init>(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)V

    .line 201
    .local v16, "oInlier":Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;->m_pt1:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    move/from16 v20, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;->m_pt2:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    move/from16 v21, v0

    sub-float v20, v20, v21

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v20

    const/high16 v21, 0x43660000    # 230.0f

    cmpl-float v20, v20, v21

    if-lez v20, :cond_7

    .line 202
    const/16 v19, 0x0

    goto :goto_2

    .line 205
    :cond_7
    new-instance v19, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;

    invoke-direct/range {v19 .. v19}, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;-><init>()V

    .line 207
    .local v19, "turnResult":Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;
    new-instance v20, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;->m_pt1:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    move/from16 v21, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;->m_pt2:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    move/from16 v22, v0

    sub-float v21, v21, v22

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;->m_pt1:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    move/from16 v22, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;->m_pt2:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    move/from16 v23, v0

    sub-float v22, v22, v23

    invoke-direct/range {v20 .. v22}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;->m_ptAnchor:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 210
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;->m_oInlier:Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;

    .line 212
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;->m_aInliers:Ljava/util/ArrayList;

    .line 214
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_3
    iget-object v0, v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_aInliers:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v20

    if-ge v10, v0, :cond_5

    .line 215
    iget-object v0, v5, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_aInliers:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 217
    .local v11, "index":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 218
    .restart local v17    # "pt1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 220
    .restart local v18    # "pt2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;->m_aInliers:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    new-instance v23, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;-><init>(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)V

    invoke-virtual/range {v22 .. v23}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    add-int/lit8 v10, v10, 0x1

    goto :goto_3
.end method
