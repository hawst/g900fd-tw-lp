.class public Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;
.super Ljava/lang/Object;
.source "EpipolarGeometry.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$1;,
        Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;,
        Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;,
        Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;
    }
.end annotation


# static fields
.field private static DEFAULT_FOV_V:F

.field private static DEFAULT_IMAGE_HEIGHT:F

.field private static DEFAULT_IMAGE_WIDTH:F

.field private static MIN_DISTANCE_TO_EPIPOLE:F

.field private static TAG:Ljava/lang/String;

.field private static m_fAspect:F

.field private static m_fF:F

.field private static m_fFOV_v:F

.field private static m_fScale:F

.field private static m_fpx:F

.field private static m_fpy:F

.field private static m_rcEpipoleBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

.field private static m_rcImageBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/high16 v4, 0x44800000    # 1024.0f

    const v3, 0x442a8000    # 682.0f

    const/high16 v2, 0x42480000    # 50.0f

    const/4 v1, 0x0

    .line 21
    const-string v0, "EpipolarGeometry"

    sput-object v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->TAG:Ljava/lang/String;

    .line 23
    sput v4, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->DEFAULT_IMAGE_WIDTH:F

    .line 24
    sput v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->DEFAULT_IMAGE_HEIGHT:F

    .line 25
    sput v2, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->DEFAULT_FOV_V:F

    .line 27
    const/high16 v0, 0x42c80000    # 100.0f

    sput v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->MIN_DISTANCE_TO_EPIPOLE:F

    .line 30
    sput v2, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fFOV_v:F

    .line 31
    const/high16 v0, 0x44000000    # 512.0f

    sput v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fpx:F

    .line 32
    const v0, 0x43aa8000    # 341.0f

    sput v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fpy:F

    .line 33
    const v0, 0x4436d1ba

    sput v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fF:F

    .line 34
    const/high16 v0, 0x3fc00000    # 1.5f

    sput v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fAspect:F

    .line 35
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fScale:F

    .line 38
    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    invoke-direct {v0, v1, v1, v4, v3}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;-><init>(FFFF)V

    sput-object v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcImageBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    .line 39
    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    const v1, 0x43998000    # 307.0f

    const/high16 v2, 0x434d0000    # 205.0f

    const/high16 v3, 0x43cd0000    # 410.0f

    const/high16 v4, 0x43880000    # 272.0f

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;-><init>(FFFF)V

    sput-object v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcEpipoleBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    return-void
.end method

.method public static computeEpipole(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/secvision/solutions/virtualtour/Point;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/secvision/solutions/virtualtour/Point;",
            ">;)",
            "Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;"
        }
    .end annotation

    .prologue
    .line 278
    .local p0, "lst1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    .local p1, "lst2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    new-instance v2, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;

    const/4 v12, 0x0

    invoke-direct {v2, v12}, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;-><init>(Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$1;)V

    .line 279
    .local v2, "bestSolution":Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    iput-object v12, v2, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_aInliers:Ljava/util/ArrayList;

    .line 281
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 282
    .local v8, "nElements":I
    const/4 v12, 0x2

    if-lt v8, v12, :cond_6

    .line 284
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 289
    .local v5, "curInliers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const-wide v12, 0x3f847ae140000000L    # 0.009999999776482582

    invoke-static {v12, v13}, Ljava/lang/Math;->log(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    const/high16 v16, 0x40000000    # 2.0f

    int-to-float v0, v8

    move/from16 v17, v0

    div-float v16, v16, v17

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v16, v0

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v16

    sub-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->log(D)D

    move-result-wide v14

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v12

    double-to-int v11, v12

    .line 291
    .local v11, "nIterations":I
    const/4 v7, 0x0

    .local v7, "it":I
    :goto_0
    if-ge v7, v11, :cond_6

    .line 293
    int-to-double v12, v8

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v14

    mul-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->floor(D)D

    move-result-wide v12

    double-to-int v9, v12

    .line 296
    .local v9, "nIndex1":I
    :cond_0
    int-to-double v12, v8

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v14

    mul-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->floor(D)D

    move-result-wide v12

    double-to-int v10, v12

    .line 297
    .local v10, "nIndex2":I
    if-eq v10, v9, :cond_0

    .line 300
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    invoke-static {v12, v13, v14, v15}, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->epipole(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-result-object v4

    .line 302
    .local v4, "curEpipole":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    if-eqz v4, :cond_5

    .line 304
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 305
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 308
    const/4 v3, 0x0

    .line 309
    .local v3, "curDistance":F
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v6, v12, :cond_2

    .line 310
    if-eq v6, v9, :cond_1

    if-eq v6, v10, :cond_1

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    invoke-static {v4, v12, v13}, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->isInlier(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 311
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 312
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    invoke-static {v4, v12, v13}, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->distance(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v12

    add-float/2addr v3, v12

    .line 309
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 317
    :cond_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v12

    const/4 v13, 0x2

    if-le v12, v13, :cond_4

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v12

    iget-object v13, v2, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_aInliers:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-gt v12, v13, :cond_3

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v12

    iget-object v13, v2, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_aInliers:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-ne v12, v13, :cond_4

    iget v12, v2, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_fAccumError:F

    cmpg-float v12, v3, v12

    if-gez v12, :cond_4

    .line 319
    :cond_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/ArrayList;

    iput-object v12, v2, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_aInliers:Ljava/util/ArrayList;

    .line 320
    iput v3, v2, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_fAccumError:F

    .line 321
    iput-object v4, v2, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_ptEpipole:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 325
    :cond_4
    const-wide v12, 0x3f847ae140000000L    # 0.009999999776482582

    invoke-static {v12, v13}, Ljava/lang/Math;->log(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    const/16 v16, 0x2

    iget-object v0, v2, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_aInliers:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->max(II)I

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    int-to-float v0, v8

    move/from16 v17, v0

    div-float v16, v16, v17

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v16, v0

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v16

    sub-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->log(D)D

    move-result-wide v14

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v12

    double-to-int v11, v12

    .line 291
    .end local v3    # "curDistance":F
    .end local v6    # "i":I
    :cond_5
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 330
    .end local v4    # "curEpipole":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .end local v5    # "curInliers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v7    # "it":I
    .end local v9    # "nIndex1":I
    .end local v10    # "nIndex2":I
    .end local v11    # "nIterations":I
    :cond_6
    return-object v2
.end method

.method public static computeTranslation(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;
    .locals 49
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/secvision/solutions/virtualtour/Point;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/secvision/solutions/virtualtour/Point;",
            ">;)",
            "Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;"
        }
    .end annotation

    .prologue
    .line 129
    .local p0, "inLst1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    .local p1, "inLst2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    new-instance v37, Ljava/util/ArrayList;

    invoke-direct/range {v37 .. v37}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v37, "lst1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    new-instance v38, Ljava/util/ArrayList;

    invoke-direct/range {v38 .. v38}, Ljava/util/ArrayList;-><init>()V

    .line 133
    .local v38, "lst2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    const/high16 v4, 0x40a00000    # 5.0f

    sget v5, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fScale:F

    div-float v30, v4, v5

    .line 135
    .local v30, "fd":F
    const/16 v31, 0x0

    .local v31, "i":I
    :goto_0
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v31

    if-ge v0, v4, :cond_3

    .line 136
    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 137
    .local v42, "pt1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v43

    check-cast v43, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 138
    .local v43, "pt2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    invoke-virtual/range {v42 .. v43}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->getDistance(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v4

    cmpl-float v4, v4, v30

    if-lez v4, :cond_1

    .line 139
    const/16 v22, 0x0

    .line 140
    .local v22, "bSimilar":Z
    const/16 v36, 0x0

    .local v36, "j":I
    :goto_1
    invoke-virtual/range {v37 .. v37}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v36

    if-ge v0, v4, :cond_0

    .line 141
    move-object/from16 v0, v37

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 142
    .local v44, "ptaux1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    move-object/from16 v0, v38

    move/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v45

    check-cast v45, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 144
    .local v45, "ptaux2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    move-object/from16 v0, v42

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->getDistance(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v4

    cmpg-float v4, v4, v30

    if-gez v4, :cond_2

    move-object/from16 v0, v43

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->getDistance(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v4

    cmpg-float v4, v4, v30

    if-gez v4, :cond_2

    .line 145
    const/16 v22, 0x1

    .line 150
    .end local v44    # "ptaux1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .end local v45    # "ptaux2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    :cond_0
    if-nez v22, :cond_1

    .line 151
    move-object/from16 v0, v37

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    move-object/from16 v0, v38

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    .end local v22    # "bSimilar":Z
    .end local v36    # "j":I
    :cond_1
    add-int/lit8 v31, v31, 0x1

    goto :goto_0

    .line 140
    .restart local v22    # "bSimilar":Z
    .restart local v36    # "j":I
    .restart local v44    # "ptaux1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .restart local v45    # "ptaux2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    :cond_2
    add-int/lit8 v36, v36, 0x1

    goto :goto_1

    .line 157
    .end local v22    # "bSimilar":Z
    .end local v36    # "j":I
    .end local v42    # "pt1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .end local v43    # "pt2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .end local v44    # "ptaux1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .end local v45    # "ptaux2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    :cond_3
    sget-object v4, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Filtered Points: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v37 .. v37}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    new-instance v47, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;

    invoke-direct/range {v47 .. v47}, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;-><init>()V

    .line 162
    .local v47, "translationResult":Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v47

    iput-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_aInliers:Ljava/util/ArrayList;

    .line 165
    invoke-static/range {v37 .. v38}, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->computeEpipole(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;

    move-result-object v29

    .line 167
    .local v29, "epipoleResult":Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;
    move-object/from16 v0, v29

    iget-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_ptEpipole:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    if-nez v4, :cond_4

    .line 168
    const/4 v4, 0x0

    move-object/from16 v0, v47

    iput-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_ptEpipole:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 169
    new-instance v4, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    const/high16 v5, 0x43960000    # 300.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;-><init>(FFF)V

    move-object/from16 v0, v47

    iput-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_vTranslation:Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .line 264
    :goto_2
    return-object v47

    .line 172
    :cond_4
    move-object/from16 v0, v29

    iget-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_ptEpipole:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v0, v47

    iput-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_ptEpipole:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 176
    new-instance v2, Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;

    sget v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fF:F

    const/4 v4, 0x0

    sget v5, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fpx:F

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget v8, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fF:F

    sget v9, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fpy:F

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/high16 v18, 0x3f800000    # 1.0f

    invoke-direct/range {v2 .. v18}, Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;-><init>(FFFFFFFFFFFFFFFF)V

    .line 182
    .local v2, "K":Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;
    new-instance v3, Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;

    const/4 v4, 0x0

    const/high16 v5, -0x40800000    # -1.0f

    move-object/from16 v0, v47

    iget-object v6, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_ptEpipole:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    iget v6, v6, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    move-object/from16 v0, v47

    iget-object v10, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_ptEpipole:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    iget v10, v10, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    neg-float v10, v10

    const/4 v11, 0x0

    move-object/from16 v0, v47

    iget-object v12, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_ptEpipole:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    iget v12, v12, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    neg-float v12, v12

    move-object/from16 v0, v47

    iget-object v13, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_ptEpipole:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    iget v13, v13, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/high16 v19, 0x3f800000    # 1.0f

    invoke-direct/range {v3 .. v19}, Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;-><init>(FFFFFFFFFFFFFFFF)V

    .line 188
    .local v3, "F":Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;
    invoke-virtual {v2}, Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;->clone()Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;

    move-result-object v20

    .line 189
    .local v20, "E":Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;
    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;->transpose()V

    .line 190
    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;->multiply(Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;)V

    .line 191
    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;->multiply(Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;)V

    .line 194
    new-instance v21, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    move-object/from16 v0, v20

    iget v4, v0, Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;->e6:F

    move-object/from16 v0, v20

    iget v5, v0, Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;->e8:F

    move-object/from16 v0, v20

    iget v6, v0, Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;->e1:F

    move-object/from16 v0, v21

    invoke-direct {v0, v4, v5, v6}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;-><init>(FFF)V

    .line 195
    .local v21, "T":Lcom/sec/android/secvision/solutions/virtualtour/Vector3;
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->normalize()F

    .line 198
    sget-object v4, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcEpipoleBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    iget v4, v4, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float v46, v4, v5

    .line 200
    .local v46, "radious_max":F
    const/16 v33, -0x1

    .line 201
    .local v33, "indexInside":I
    const/16 v34, -0x1

    .line 202
    .local v34, "indexOutside":I
    const v25, 0x7f7fffff    # Float.MAX_VALUE

    .line 203
    .local v25, "distanceInside":F
    const v26, 0x7f7fffff    # Float.MAX_VALUE

    .line 207
    .local v26, "distanceOutside":F
    new-instance v23, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    sget v4, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fpx:F

    sget v5, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fpy:F

    move-object/from16 v0, v23

    invoke-direct {v0, v4, v5}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    .line 212
    .local v23, "center":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    const/16 v31, 0x0

    :goto_3
    move-object/from16 v0, v29

    iget-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_aInliers:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    move/from16 v0, v31

    if-ge v0, v4, :cond_8

    .line 213
    move-object/from16 v0, v29

    iget-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_aInliers:Ljava/util/ArrayList;

    move/from16 v0, v31

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v32

    .line 215
    .local v32, "index":I
    move-object/from16 v0, v37

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v40

    check-cast v40, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 216
    .local v40, "p1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    move-object/from16 v0, v38

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v41

    check-cast v41, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 218
    .local v41, "p2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    new-instance v39, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;

    invoke-direct/range {v39 .. v41}, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;-><init>(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)V

    .line 220
    .local v39, "oInlier":Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;
    move-object/from16 v0, v29

    iget-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_ptEpipole:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v0, v40

    invoke-virtual {v4, v0}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->getDistance(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v4

    move-object/from16 v0, v39

    iput v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_fd1:F

    .line 221
    move-object/from16 v0, v29

    iget-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_ptEpipole:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v0, v41

    invoke-virtual {v4, v0}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->getDistance(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v4

    move-object/from16 v0, v39

    iput v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_fd2:F

    .line 224
    sget v4, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fpx:F

    move-object/from16 v0, v41

    iget v5, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v27

    .line 225
    .local v27, "dx":F
    sget v4, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fpy:F

    move-object/from16 v0, v41

    iget v5, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v28

    .line 227
    .local v28, "dy":F
    cmpl-float v4, v27, v28

    if-lez v4, :cond_6

    move-object/from16 v0, v40

    iget v4, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    move-object/from16 v0, v41

    iget v5, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_6

    sget v4, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fF:F

    move-object/from16 v0, v40

    iget v5, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    move-object/from16 v0, v41

    iget v6, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    sub-float/2addr v5, v6

    mul-float/2addr v4, v5

    sget v5, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fpx:F

    move-object/from16 v0, v41

    iget v6, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    sub-float/2addr v5, v6

    move-object/from16 v0, v21

    iget v6, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    mul-float/2addr v5, v6

    sget v6, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fF:F

    move-object/from16 v0, v21

    iget v7, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    div-float/2addr v4, v5

    :goto_4
    move-object/from16 v0, v39

    iput v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_fs:F

    .line 230
    move-object/from16 v0, v39

    iget v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_fs:F

    sget v5, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fScale:F

    mul-float/2addr v4, v5

    move-object/from16 v0, v39

    iput v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_fs:F

    .line 231
    new-instance v4, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    move-object/from16 v0, v39

    iget v5, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_fs:F

    move-object/from16 v0, v21

    iget v6, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    mul-float/2addr v5, v6

    move-object/from16 v0, v39

    iget v6, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_fs:F

    neg-float v6, v6

    move-object/from16 v0, v21

    iget v7, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    mul-float/2addr v6, v7

    move-object/from16 v0, v39

    iget v7, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_fs:F

    move-object/from16 v0, v21

    iget v8, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    mul-float/2addr v7, v8

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;-><init>(FFF)V

    move-object/from16 v0, v39

    iput-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_vT:Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .line 234
    move-object/from16 v0, v47

    iget-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_aInliers:Ljava/util/ArrayList;

    move-object/from16 v0, v39

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    move-object/from16 v0, v41

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->getDistance(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v4

    sub-float v4, v4, v46

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v24

    .line 238
    .local v24, "distance":F
    move-object/from16 v0, v41

    iget v4, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    sget-object v5, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcEpipoleBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    invoke-virtual {v5}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->getLeft()F

    move-result v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_7

    move-object/from16 v0, v41

    iget v4, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    sget-object v5, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcEpipoleBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    invoke-virtual {v5}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->getRight()F

    move-result v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_7

    .line 239
    cmpg-float v4, v24, v25

    if-gez v4, :cond_5

    move-object/from16 v0, v39

    iget v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_fd1:F

    sget v5, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->MIN_DISTANCE_TO_EPIPOLE:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_5

    move-object/from16 v0, v39

    iget v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_fd2:F

    sget v5, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->MIN_DISTANCE_TO_EPIPOLE:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_5

    .line 240
    move/from16 v25, v24

    .line 241
    move/from16 v33, v31

    .line 212
    :cond_5
    :goto_5
    add-int/lit8 v31, v31, 0x1

    goto/16 :goto_3

    .line 227
    .end local v24    # "distance":F
    :cond_6
    sget v4, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fF:F

    move-object/from16 v0, v40

    iget v5, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    move-object/from16 v0, v41

    iget v6, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    sub-float/2addr v5, v6

    mul-float/2addr v4, v5

    sget v5, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fpy:F

    move-object/from16 v0, v41

    iget v6, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    sub-float/2addr v5, v6

    move-object/from16 v0, v21

    iget v6, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    mul-float/2addr v5, v6

    sget v6, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fF:F

    move-object/from16 v0, v21

    iget v7, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    div-float/2addr v4, v5

    goto/16 :goto_4

    .line 243
    .restart local v24    # "distance":F
    :cond_7
    cmpg-float v4, v24, v26

    if-gez v4, :cond_5

    move-object/from16 v0, v39

    iget v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_fd1:F

    sget v5, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->MIN_DISTANCE_TO_EPIPOLE:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_5

    move-object/from16 v0, v39

    iget v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_fd2:F

    sget v5, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->MIN_DISTANCE_TO_EPIPOLE:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_5

    .line 244
    move/from16 v26, v24

    .line 245
    move/from16 v34, v31

    goto :goto_5

    .line 249
    .end local v24    # "distance":F
    .end local v27    # "dx":F
    .end local v28    # "dy":F
    .end local v32    # "index":I
    .end local v39    # "oInlier":Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;
    .end local v40    # "p1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .end local v41    # "p2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    :cond_8
    cmpg-float v4, v25, v26

    if-gez v4, :cond_9

    move/from16 v35, v33

    .line 251
    .local v35, "inlierIndex":I
    :goto_6
    const/4 v4, -0x1

    move/from16 v0, v35

    if-ne v0, v4, :cond_a

    .line 252
    new-instance v4, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    const/high16 v5, 0x43960000    # 300.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;-><init>(FFF)V

    move-object/from16 v0, v47

    iput-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_vTranslation:Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    goto/16 :goto_2

    .end local v35    # "inlierIndex":I
    :cond_9
    move/from16 v35, v34

    .line 249
    goto :goto_6

    .line 254
    .restart local v35    # "inlierIndex":I
    :cond_a
    move-object/from16 v0, v47

    iget-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_aInliers:Ljava/util/ArrayList;

    move/from16 v0, v35

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;

    move-object/from16 v0, v47

    iput-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_oInlier:Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;

    .line 256
    move-object/from16 v0, v47

    iget-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_oInlier:Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;

    iget-object v0, v4, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_vT:Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    move-object/from16 v48, v0

    .line 257
    .local v48, "vT":Lcom/sec/android/secvision/solutions/virtualtour/Vector3;
    move-object/from16 v0, v48

    iget v4, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    const/high16 v5, 0x43c80000    # 400.0f

    cmpl-float v4, v4, v5

    if-gtz v4, :cond_b

    move-object/from16 v0, v48

    iget v4, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x42c80000    # 100.0f

    cmpl-float v4, v4, v5

    if-gtz v4, :cond_b

    move-object/from16 v0, v48

    iget v4, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x432f0000    # 175.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_c

    .line 258
    :cond_b
    new-instance v4, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    const/high16 v5, 0x43960000    # 300.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;-><init>(FFF)V

    move-object/from16 v0, v47

    iput-object v4, v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_vTranslation:Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    goto/16 :goto_2

    .line 260
    :cond_c
    move-object/from16 v0, v48

    move-object/from16 v1, v47

    iput-object v0, v1, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_vTranslation:Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    goto/16 :goto_2
.end method

.method private static distance(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)F
    .locals 6
    .param p0, "q"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .param p1, "p0"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .param p2, "p1"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .prologue
    .line 385
    invoke-static {p2, p1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->sub(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-result-object v2

    .line 386
    .local v2, "v":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    invoke-static {p0, p1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->sub(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-result-object v3

    .line 388
    .local v3, "w":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    invoke-virtual {v3, v2}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->dot(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v4

    invoke-virtual {v2, v2}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->dot(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v5

    div-float v0, v4, v5

    .line 389
    .local v0, "b":F
    invoke-static {v2, v0}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->scale(Lcom/sec/android/secvision/solutions/virtualtour/Point;F)Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-result-object v4

    invoke-static {p1, v4}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->add(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-result-object v1

    .line 391
    .local v1, "qb":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    invoke-virtual {p0, v1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->getDistance(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v4

    return v4
.end method

.method private static epipole(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .locals 10
    .param p0, "a1"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .param p1, "a2"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .param p2, "b1"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .param p3, "b2"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .prologue
    .line 345
    iget v6, p2, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    iget v7, p3, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    sub-float/2addr v6, v7

    iget v7, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v8, p0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    sub-float/2addr v7, v8

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    iget v8, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    sub-float/2addr v7, v8

    iget v8, p3, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v9, p2, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    sub-float v2, v6, v7

    .line 347
    .local v2, "denominator":F
    invoke-static {p0, p1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->sub(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-result-object v0

    .line 348
    .local v0, "ad":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    invoke-virtual {v0}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->normalize()F

    .line 351
    const/4 v6, 0x0

    cmpl-float v6, v2, v6

    if-nez v6, :cond_0

    .line 352
    iget v6, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    const v7, 0x4cbebc20    # 1.0E8f

    mul-float/2addr v6, v7

    iput v6, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    .line 353
    iget v6, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    const v7, 0x4cbebc20    # 1.0E8f

    mul-float/2addr v6, v7

    iput v6, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    .line 371
    .end local v0    # "ad":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    :goto_0
    return-object v0

    .line 359
    .restart local v0    # "ad":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    :cond_0
    invoke-static {p2, p3}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->sub(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-result-object v1

    .line 360
    .local v1, "bd":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    invoke-virtual {v1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->normalize()F

    .line 362
    iget v6, p0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v7, v1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    mul-float/2addr v6, v7

    iget v7, v1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v8, p2, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iget v7, p2, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v8, v1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    iget v7, v1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v8, p0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    mul-float/2addr v7, v8

    sub-float/2addr v6, v7

    iget v7, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    iget v8, v1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    mul-float/2addr v7, v8

    iget v8, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v9, v1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    div-float v4, v6, v7

    .line 363
    .local v4, "u":F
    iget v6, v1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_3

    iget v6, p0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    iget v7, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    mul-float/2addr v7, v4

    add-float/2addr v6, v7

    iget v7, p2, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    sub-float/2addr v6, v7

    iget v7, v1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    div-float v5, v6, v7

    .line 365
    .local v5, "v":F
    :goto_1
    const/4 v6, 0x0

    cmpl-float v6, v4, v6

    if-lez v6, :cond_4

    const/4 v6, 0x0

    cmpl-float v6, v5, v6

    if-lez v6, :cond_4

    .line 366
    new-instance v3, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    iget v6, p0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    iget v7, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    mul-float/2addr v7, v4

    add-float/2addr v6, v7

    iget v7, p0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v8, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    mul-float/2addr v8, v4

    add-float/2addr v7, v8

    invoke-direct {v3, v6, v7}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    .line 367
    .local v3, "e":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    invoke-virtual {v3, p0}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->getDistance(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v6

    sget v7, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->MIN_DISTANCE_TO_EPIPOLE:F

    cmpg-float v6, v6, v7

    if-ltz v6, :cond_1

    invoke-virtual {v3, p2}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->getDistance(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v6

    sget v7, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->MIN_DISTANCE_TO_EPIPOLE:F

    cmpg-float v6, v6, v7

    if-gez v6, :cond_2

    :cond_1
    const/4 v3, 0x0

    .end local v3    # "e":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    :cond_2
    move-object v0, v3

    goto :goto_0

    .line 363
    .end local v5    # "v":F
    :cond_3
    iget v6, p0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v7, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    mul-float/2addr v7, v4

    add-float/2addr v6, v7

    iget v7, p2, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    sub-float/2addr v6, v7

    iget v7, v1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    div-float v5, v6, v7

    goto :goto_1

    .line 371
    .restart local v5    # "v":F
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public static getEpipoleBoundingRectangle()Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcEpipoleBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    return-object v0
.end method

.method private static isInlier(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Z
    .locals 9
    .param p0, "e"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .param p1, "p1"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .param p2, "p2"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .prologue
    const/4 v6, 0x0

    .line 405
    invoke-static {p2, p1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->sub(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-result-object v2

    .line 406
    .local v2, "v":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    invoke-static {p0, p1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->sub(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-result-object v5

    .line 408
    .local v5, "w":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    invoke-virtual {v5, v2}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->dot(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v7

    invoke-virtual {v2, v2}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->dot(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v8

    div-float v0, v7, v8

    .line 409
    .local v0, "b":F
    invoke-static {v2, v0}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->scale(Lcom/sec/android/secvision/solutions/virtualtour/Point;F)Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-result-object v7

    invoke-static {p1, v7}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->add(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-result-object v1

    .line 411
    .local v1, "eb":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    invoke-virtual {p0, v1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->getDistance(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v7

    const/high16 v8, 0x41200000    # 10.0f

    cmpl-float v7, v7, v8

    if-lez v7, :cond_1

    .line 422
    :cond_0
    :goto_0
    return v6

    .line 414
    :cond_1
    invoke-static {p1, v1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->sub(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-result-object v3

    .line 415
    .local v3, "v1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    invoke-static {p2, v1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->sub(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-result-object v4

    .line 417
    .local v4, "v2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    invoke-virtual {v3, v4}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->dot(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v7

    const/4 v8, 0x0

    cmpg-float v7, v7, v8

    if-ltz v7, :cond_0

    invoke-virtual {v3}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->length()F

    move-result v7

    invoke-virtual {v4}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->length()F

    move-result v8

    cmpl-float v7, v7, v8

    if-gtz v7, :cond_0

    .line 422
    const/4 v6, 0x1

    goto :goto_0
.end method

.method public static setCameraCalibrationMatrixParameters(FII)V
    .locals 10
    .param p0, "fFOV_v"    # F
    .param p1, "imageWidth"    # I
    .param p2, "imageHeight"    # I

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    const v9, 0x3ecccccd    # 0.4f

    const/high16 v8, 0x40000000    # 2.0f

    .line 85
    int-to-float v3, p1

    div-float/2addr v3, v8

    sput v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fpx:F

    .line 86
    int-to-float v3, p2

    div-float/2addr v3, v8

    sput v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fpy:F

    .line 87
    int-to-float v3, p1

    int-to-float v4, p2

    div-float/2addr v3, v4

    sput v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fAspect:F

    .line 89
    sget v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->DEFAULT_IMAGE_HEIGHT:F

    mul-float/2addr v3, v6

    sget v4, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->DEFAULT_FOV_V:F

    div-float/2addr v4, v8

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->tan(D)D

    move-result-wide v4

    double-to-float v4, v4

    div-float v0, v3, v4

    .line 90
    .local v0, "fF":F
    sget v2, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->DEFAULT_IMAGE_WIDTH:F

    .line 91
    .local v2, "nWidth":F
    sget v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fAspect:F

    div-float v3, v2, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v1, v3

    .line 92
    .local v1, "nHeight":F
    mul-float v3, v6, v1

    float-to-double v4, v3

    float-to-double v6, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v3, v8

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v3, v4

    sput v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fFOV_v:F

    .line 94
    sget v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fpy:F

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    sget v6, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fFOV_v:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    const-wide v6, 0x4076800000000000L    # 360.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->tan(D)D

    move-result-wide v4

    double-to-float v4, v4

    div-float/2addr v3, v4

    sput v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fF:F

    .line 96
    sget v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fAspect:F

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    sget v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->DEFAULT_IMAGE_WIDTH:F

    int-to-float v4, p1

    div-float/2addr v3, v4

    :goto_0
    sput v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_fScale:F

    .line 98
    sget-object v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcImageBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    int-to-float v4, p1

    iput v4, v3, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    .line 99
    sget-object v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcImageBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    int-to-float v4, p2

    iput v4, v3, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    .line 101
    sget-object v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcEpipoleBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    int-to-float v4, p1

    mul-float/2addr v4, v9

    iput v4, v3, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    .line 102
    sget-object v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcEpipoleBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    int-to-float v4, p2

    mul-float/2addr v4, v9

    iput v4, v3, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    .line 103
    sget-object v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcEpipoleBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    sget-object v4, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcImageBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    iget v4, v4, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    sget-object v5, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcEpipoleBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    iget v5, v5, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    sub-float/2addr v4, v5

    div-float/2addr v4, v8

    iput v4, v3, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    .line 104
    sget-object v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcEpipoleBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    sget-object v4, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcImageBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    iget v4, v4, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    sget-object v5, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->m_rcEpipoleBound:Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    iget v5, v5, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    sub-float/2addr v4, v5

    div-float/2addr v4, v8

    iput v4, v3, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    .line 105
    return-void

    .line 96
    :cond_0
    sget v3, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->DEFAULT_IMAGE_HEIGHT:F

    int-to-float v4, p2

    div-float/2addr v3, v4

    goto :goto_0
.end method
