.class public Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;
.super Ljava/lang/Object;
.source "TurnGeometry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TurnResult"
.end annotation


# instance fields
.field public m_aInliers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;",
            ">;"
        }
    .end annotation
.end field

.field public m_oInlier:Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;

.field public m_ptAnchor:Lcom/sec/android/secvision/solutions/virtualtour/Point;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v0, p0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;->m_ptAnchor:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 42
    iput-object v0, p0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;->m_oInlier:Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$Inlier;

    .line 43
    iput-object v0, p0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;->m_aInliers:Ljava/util/ArrayList;

    return-void
.end method
