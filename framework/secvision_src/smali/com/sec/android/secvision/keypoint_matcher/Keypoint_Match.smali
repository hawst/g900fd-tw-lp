.class public Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;
.super Ljava/lang/Object;
.source "Keypoint_Match.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field public distance:F

.field public x1:F

.field public x2:F

.field public y1:F

.field public y2:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create_list([F[F[F)Ljava/util/List;
    .locals 5
    .param p0, "m1"    # [F
    .param p1, "m2"    # [F
    .param p2, "distances"    # [F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([F[F[F)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    sget-boolean v3, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    array-length v3, p0

    array-length v4, p1

    if-eq v3, v4, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 35
    :cond_0
    sget-boolean v3, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->$assertionsDisabled:Z

    if-nez v3, :cond_1

    array-length v3, p1

    array-length v4, p2

    mul-int/lit8 v4, v4, 0x2

    if-eq v3, v4, :cond_1

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3

    .line 36
    :cond_1
    array-length v3, p0

    if-nez v3, :cond_3

    .line 37
    const/4 v2, 0x0

    .line 52
    :cond_2
    return-object v2

    .line 39
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    array-length v3, p2

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 40
    .local v2, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p2

    if-ge v0, v3, :cond_2

    .line 41
    new-instance v1, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;

    invoke-direct {v1}, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;-><init>()V

    .line 43
    .local v1, "m":Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;
    mul-int/lit8 v3, v0, 0x2

    aget v3, p0, v3

    iput v3, v1, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->x1:F

    .line 44
    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x1

    aget v3, p0, v3

    iput v3, v1, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->y1:F

    .line 45
    mul-int/lit8 v3, v0, 0x2

    aget v3, p1, v3

    iput v3, v1, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->x2:F

    .line 46
    mul-int/lit8 v3, v0, 0x2

    add-int/lit8 v3, v3, 0x1

    aget v3, p1, v3

    iput v3, v1, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->y2:F

    .line 48
    aget v3, p2, v0

    iput v3, v1, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->distance:F

    .line 50
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
