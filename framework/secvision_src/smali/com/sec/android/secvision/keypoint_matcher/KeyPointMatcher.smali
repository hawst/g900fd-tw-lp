.class public Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;
.super Ljava/lang/Object;
.source "KeyPointMatcher.java"


# instance fields
.field public FeatureExtractor0Exists:Z

.field public FeatureExtractor1Exists:Z

.field public imageMatcherExists:Z

.field public pFeatureExtractor0:J

.field public pFeatureExtractor1:J

.field public pImageMatcher:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-string v0, "somp"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 31
    const-string v0, "OpenCv"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 32
    const-string v0, "keypoint_matcher"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->imageMatcherExists:Z

    .line 26
    iput-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor0Exists:Z

    .line 27
    iput-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor1Exists:Z

    .line 36
    invoke-direct {p0}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->InitMatcher()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pImageMatcher:J

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->imageMatcherExists:Z

    .line 38
    return-void
.end method

.method public constructor <init>(ID)V
    .locals 4
    .param p1, "max_keypoints"    # I
    .param p2, "scale"    # D

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->imageMatcherExists:Z

    .line 26
    iput-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor0Exists:Z

    .line 27
    iput-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor1Exists:Z

    .line 41
    invoke-direct {p0}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->InitFeatureExtractor()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor0:J

    .line 42
    invoke-direct {p0}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->InitFeatureExtractor()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor1:J

    .line 43
    invoke-direct {p0}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->InitFeatureMatcher()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pImageMatcher:J

    .line 44
    iput-boolean v2, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor0Exists:Z

    .line 45
    iput-boolean v2, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor1Exists:Z

    .line 46
    iput-boolean v2, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->imageMatcherExists:Z

    .line 47
    invoke-virtual {p0, p1}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->set_nKeypoints(I)V

    .line 48
    invoke-virtual {p0, p2, p3}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->set_scale(D)V

    .line 49
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "detector"    # Ljava/lang/String;
    .param p2, "descriptor"    # Ljava/lang/String;
    .param p3, "matcher"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->imageMatcherExists:Z

    .line 26
    iput-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor0Exists:Z

    .line 27
    iput-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor1Exists:Z

    .line 67
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->InitMatcher(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pImageMatcher:J

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->imageMatcherExists:Z

    .line 69
    return-void
.end method

.method private native InitFeatureExtractor()J
.end method

.method private native InitFeatureMatcher()J
.end method

.method private native InitMatcher()J
.end method

.method private native InitMatcher(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
.end method

.method private native clearMatcher(J)V
.end method

.method private native deleteMatcher(J)V
.end method

.method private native extractFeaturePoints(JLandroid/graphics/Bitmap;)I
.end method

.method private native getDistances(J)[F
.end method

.method private native getMatches(JJ)[F
.end method

.method private native get_descriptor_distance_threshold(J)F
.end method

.method private native get_nKeypoints(J)I
.end method

.method private native get_ratioThreshold(J)F
.end method

.method private native get_scale(J)D
.end method

.method private native matchImages(JLandroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Z
.end method

.method private native matchKeypoints(JJJ)Z
.end method

.method private native set_descriptor_distance_threshold(JF)V
.end method

.method private native set_nKeypoints(JI)V
.end method

.method private native set_ratioThreshold(JF)V
.end method

.method private native set_scale(JD)V
.end method


# virtual methods
.method public clean()V
    .locals 2

    .prologue
    .line 177
    iget-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pImageMatcher:J

    invoke-direct {p0, v0, v1}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->clearMatcher(J)V

    .line 178
    iget-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor0:J

    invoke-direct {p0, v0, v1}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->clearMatcher(J)V

    .line 179
    iget-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor1:J

    invoke-direct {p0, v0, v1}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->clearMatcher(J)V

    .line 180
    return-void
.end method

.method public clean(I)V
    .locals 2
    .param p1, "image_id"    # I

    .prologue
    .line 183
    if-nez p1, :cond_0

    .line 184
    iget-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor0:J

    invoke-direct {p0, v0, v1}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->clearMatcher(J)V

    .line 187
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor1:J

    invoke-direct {p0, v0, v1}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->clearMatcher(J)V

    goto :goto_0
.end method

.method public extractFeaturePoints(Landroid/graphics/Bitmap;I)I
    .locals 9
    .param p1, "image1"    # Landroid/graphics/Bitmap;
    .param p2, "image_id"    # I

    .prologue
    const/4 v8, 0x1

    .line 121
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 124
    .local v4, "startTime":J
    if-nez p2, :cond_1

    .line 125
    iget-boolean v3, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor0Exists:Z

    if-nez v3, :cond_0

    .line 126
    invoke-direct {p0}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->InitFeatureExtractor()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor0:J

    .line 127
    const-string v3, "KeyPointMatcher"

    const-string v6, "Calling FeatureExtractor0 without initilizing module"

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iput-boolean v8, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor0Exists:Z

    .line 130
    :cond_0
    iget-wide v6, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor0:J

    invoke-direct {p0, v6, v7, p1}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->extractFeaturePoints(JLandroid/graphics/Bitmap;)I

    move-result v2

    .line 141
    .local v2, "featureid":I
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v0, v6, v4

    .line 142
    .local v0, "elapsedTime":J
    const-string v3, "JAVA 10.6"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Call extractFeaturePoints() NATIVE from java, featureid:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    return v2

    .line 133
    .end local v0    # "elapsedTime":J
    .end local v2    # "featureid":I
    :cond_1
    iget-boolean v3, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor1Exists:Z

    if-nez v3, :cond_2

    .line 134
    invoke-direct {p0}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->InitFeatureExtractor()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor1:J

    .line 135
    const-string v3, "KeyPointMatcher"

    const-string v6, "Calling FeatureExtractor1 without initilizing module"

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iput-boolean v8, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor1Exists:Z

    .line 138
    :cond_2
    iget-wide v6, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor1:J

    invoke-direct {p0, v6, v7, p1}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->extractFeaturePoints(JLandroid/graphics/Bitmap;)I

    move-result v2

    .restart local v2    # "featureid":I
    goto :goto_0
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 202
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->imageMatcherExists:Z

    if-eqz v0, :cond_0

    .line 203
    iget-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pImageMatcher:J

    invoke-direct {p0, v0, v1}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->deleteMatcher(J)V

    .line 204
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->imageMatcherExists:Z

    .line 205
    iget-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor0Exists:Z

    if-eqz v0, :cond_1

    .line 206
    iget-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor0:J

    invoke-direct {p0, v0, v1}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->deleteMatcher(J)V

    .line 207
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor0Exists:Z

    .line 208
    iget-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor1Exists:Z

    if-eqz v0, :cond_2

    .line 209
    iget-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor1:J

    invoke-direct {p0, v0, v1}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->deleteMatcher(J)V

    .line 210
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->FeatureExtractor1Exists:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 216
    return-void

    .line 213
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getLastMatchResults()Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 164
    .local v4, "startTime":J
    iget-wide v6, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pImageMatcher:J

    const-wide/16 v8, 0x0

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->getMatches(JJ)[F

    move-result-object v1

    .line 165
    .local v1, "m1":[F
    iget-wide v6, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pImageMatcher:J

    const-wide/16 v8, 0x1

    invoke-direct {p0, v6, v7, v8, v9}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->getMatches(JJ)[F

    move-result-object v2

    .line 166
    .local v2, "m2":[F
    iget-wide v6, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pImageMatcher:J

    invoke-direct {p0, v6, v7}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->getDistances(J)[F

    move-result-object v0

    .line 168
    .local v0, "d":[F
    invoke-static {v1, v2, v0}, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->create_list([F[F[F)Ljava/util/List;

    move-result-object v3

    .line 169
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;>;"
    const-string v6, "KeyPointMatcher"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "result conversion: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v8, v4

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " msec"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    return-object v3
.end method

.method public matchImages(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Ljava/util/List;
    .locals 8
    .param p1, "image1"    # Landroid/graphics/Bitmap;
    .param p2, "image2"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Landroid/graphics/Bitmap;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    iget-boolean v3, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->imageMatcherExists:Z

    if-nez v3, :cond_0

    .line 79
    invoke-direct {p0}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->InitMatcher()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pImageMatcher:J

    .line 80
    const-string v3, "KeyPointMatcher"

    const-string v6, "Calling matchImages without initilizing matcher module"

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->imageMatcherExists:Z

    .line 83
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 84
    .local v4, "startTime":J
    iget-wide v6, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pImageMatcher:J

    invoke-direct {p0, v6, v7, p1, p2}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->matchImages(JLandroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Z

    move-result v2

    .line 85
    .local v2, "matched":Z
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v0, v6, v4

    .line 86
    .local v0, "elapsedTime":J
    const-string v3, "JAVA 10.6"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Call matchImages() NATIVE from java, time[ms]: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    if-nez v2, :cond_1

    .line 88
    const/4 v3, 0x0

    .line 90
    :goto_0
    return-object v3

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->getLastMatchResults()Ljava/util/List;

    move-result-object v3

    goto :goto_0
.end method

.method public matchImages(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;ID)Ljava/util/List;
    .locals 8
    .param p1, "image1"    # Landroid/graphics/Bitmap;
    .param p2, "image2"    # Landroid/graphics/Bitmap;
    .param p3, "max_keypoints"    # I
    .param p4, "scale"    # D
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Landroid/graphics/Bitmap;",
            "ID)",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    iget-boolean v3, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->imageMatcherExists:Z

    if-nez v3, :cond_0

    .line 103
    invoke-direct {p0}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->InitMatcher()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pImageMatcher:J

    .line 104
    const-string v3, "KeyPointMatcher"

    const-string v6, "Calling matchImages without initilizing matcher module"

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->imageMatcherExists:Z

    .line 107
    :cond_0
    invoke-virtual {p0, p3}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->set_nKeypoints(I)V

    .line 108
    invoke-virtual {p0, p4, p5}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->set_scale(D)V

    .line 109
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 110
    .local v4, "startTime":J
    iget-wide v6, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pImageMatcher:J

    invoke-direct {p0, v6, v7, p1, p2}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->matchImages(JLandroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Z

    move-result v2

    .line 111
    .local v2, "matched":Z
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v0, v6, v4

    .line 112
    .local v0, "elapsedTime":J
    const-string v3, "JAVA 10.6"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Call matchImages() NATIVE from java, time[ms]: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    if-nez v2, :cond_1

    .line 114
    const/4 v3, 0x0

    .line 116
    :goto_0
    return-object v3

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->getLastMatchResults()Ljava/util/List;

    move-result-object v3

    goto :goto_0
.end method

.method public matchKeypoints()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    iget-boolean v1, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->imageMatcherExists:Z

    if-nez v1, :cond_0

    .line 150
    const-string v1, "JAVA 10.6"

    const-string v2, "matchKeypoints: imageMatcher does not Exists"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    :cond_0
    iget-wide v2, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pImageMatcher:J

    iget-wide v4, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor0:J

    iget-wide v6, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor1:J

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->matchKeypoints(JJJ)Z

    move-result v0

    .line 154
    .local v0, "result":Z
    const-string v1, "JAVA 10.6"

    const-string v2, "matchKeypoints: after matchKeypoints"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->getLastMatchResults()Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public set_nKeypoints(I)V
    .locals 2
    .param p1, "n_kpt"    # I

    .prologue
    .line 195
    iget-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor0:J

    invoke-direct {p0, v0, v1, p1}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->set_nKeypoints(JI)V

    .line 196
    iget-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor1:J

    invoke-direct {p0, v0, v1, p1}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->set_nKeypoints(JI)V

    .line 198
    return-void
.end method

.method public set_scale(D)V
    .locals 3
    .param p1, "scale"    # D

    .prologue
    .line 190
    iget-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor0:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->set_scale(JD)V

    .line 191
    iget-wide v0, p0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->pFeatureExtractor1:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->set_scale(JD)V

    .line 192
    return-void
.end method
