.class public Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;
.super Ljava/lang/Object;
.source "EpipolarGeometry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Inlier"
.end annotation


# instance fields
.field public m_fd1:F

.field public m_fd2:F

.field public m_fs:F

.field public m_pt1:Lcom/sec/android/secvision/solutions/virtualtour/Point;

.field public m_pt2:Lcom/sec/android/secvision/solutions/virtualtour/Point;

.field public m_vT:Lcom/sec/android/secvision/solutions/virtualtour/Vector3;


# direct methods
.method public constructor <init>(Lcom/sec/android/secvision/solutions/virtualtour/Point;Lcom/sec/android/secvision/solutions/virtualtour/Point;)V
    .locals 2
    .param p1, "pt1"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .param p2, "pt2"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object v1, p0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_pt1:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 44
    iput-object v1, p0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_pt2:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 45
    iput v0, p0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_fd1:F

    .line 46
    iput v0, p0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_fd2:F

    .line 47
    iput v0, p0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_fs:F

    .line 48
    iput-object v1, p0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_vT:Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .line 51
    iput-object p1, p0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_pt1:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 52
    iput-object p2, p0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;->m_pt2:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 53
    return-void
.end method
