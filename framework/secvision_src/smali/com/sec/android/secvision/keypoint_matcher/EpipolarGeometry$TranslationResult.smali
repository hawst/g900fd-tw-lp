.class public Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;
.super Ljava/lang/Object;
.source "EpipolarGeometry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TranslationResult"
.end annotation


# instance fields
.field public m_aInliers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;",
            ">;"
        }
    .end annotation
.end field

.field public m_oInlier:Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;

.field public m_ptEpipole:Lcom/sec/android/secvision/solutions/virtualtour/Point;

.field public m_vTranslation:Lcom/sec/android/secvision/solutions/virtualtour/Vector3;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object v0, p0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_ptEpipole:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 61
    iput-object v0, p0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_oInlier:Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$Inlier;

    .line 62
    iput-object v0, p0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_aInliers:Ljava/util/ArrayList;

    .line 63
    iput-object v0, p0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_vTranslation:Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    return-void
.end method
