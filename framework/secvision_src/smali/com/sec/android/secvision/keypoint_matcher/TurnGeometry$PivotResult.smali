.class Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;
.super Ljava/lang/Object;
.source "TurnGeometry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PivotResult"
.end annotation


# instance fields
.field public m_aInliers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public m_fAccumError:F

.field public m_nPivotIndex:I

.field public m_ptPivot:Lcom/sec/android/secvision/solutions/virtualtour/Point;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object v1, p0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_ptPivot:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_nPivotIndex:I

    .line 49
    iput-object v1, p0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_aInliers:Ljava/util/ArrayList;

    .line 50
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;->m_fAccumError:F

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$1;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$PivotResult;-><init>()V

    return-void
.end method
