.class Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;
.super Ljava/lang/Object;
.source "EpipolarGeometry.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "EpipoleResult"
.end annotation


# instance fields
.field public m_aInliers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public m_fAccumError:F

.field public m_ptEpipole:Lcom/sec/android/secvision/solutions/virtualtour/Point;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object v0, p0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_ptEpipole:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 71
    iput-object v0, p0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_aInliers:Ljava/util/ArrayList;

    .line 72
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;->m_fAccumError:F

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$1;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$EpipoleResult;-><init>()V

    return-void
.end method
