.class Lcom/sec/android/secvision/sef/ConverterObject;
.super Ljava/lang/Object;
.source "ConverterObject.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secvision/sef/ConverterObject$transcodeMode;,
        Lcom/sec/android/secvision/sef/ConverterObject$codecType;,
        Lcom/sec/android/secvision/sef/ConverterObject$videoResType;
    }
.end annotation


# instance fields
.field audioCodecType:I

.field audioLength:I

.field audioOffset:I

.field endTime:I

.field iconFileName:Ljava/lang/String;

.field inputFileName:Ljava/lang/String;

.field maxOutFileDuration:I

.field maxOutFileSize:I

.field outFileResolution:I

.field outputFileName:Ljava/lang/String;

.field startTime:I

.field transcodeMode:I

.field videoCodecType:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method


# virtual methods
.method public getAudioLength()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/android/secvision/sef/ConverterObject;->audioLength:I

    return v0
.end method

.method public getAudioOffset()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/secvision/sef/ConverterObject;->audioOffset:I

    return v0
.end method

.method public getTranscodeMode()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/secvision/sef/ConverterObject;->transcodeMode:I

    return v0
.end method

.method public setAudioCodec(I)V
    .locals 0
    .param p1, "audiocodec"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/sec/android/secvision/sef/ConverterObject;->audioCodecType:I

    .line 91
    return-void
.end method

.method public setAudioLength(I)V
    .locals 0
    .param p1, "audioLength"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/sec/android/secvision/sef/ConverterObject;->audioLength:I

    .line 60
    return-void
.end method

.method public setAudioOffset(I)V
    .locals 0
    .param p1, "audioOffset"    # I

    .prologue
    .line 51
    iput p1, p0, Lcom/sec/android/secvision/sef/ConverterObject;->audioOffset:I

    .line 52
    return-void
.end method

.method public setEndTime(I)V
    .locals 0
    .param p1, "endtime"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/sec/android/secvision/sef/ConverterObject;->endTime:I

    .line 76
    return-void
.end method

.method public setIconFileName(Ljava/lang/String;)V
    .locals 0
    .param p1, "iconFileName"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/sec/android/secvision/sef/ConverterObject;->iconFileName:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public setInputFilename(Ljava/lang/String;)V
    .locals 0
    .param p1, "inputFileName"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/android/secvision/sef/ConverterObject;->inputFileName:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public setMaxDuration(I)V
    .locals 0
    .param p1, "maxduration"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/secvision/sef/ConverterObject;->maxOutFileDuration:I

    .line 79
    return-void
.end method

.method public setMaxSize(I)V
    .locals 0
    .param p1, "maxsize"    # I

    .prologue
    .line 82
    iput p1, p0, Lcom/sec/android/secvision/sef/ConverterObject;->maxOutFileSize:I

    .line 83
    return-void
.end method

.method public setOutputFileResolution(I)V
    .locals 0
    .param p1, "outFileResolution"    # I

    .prologue
    .line 106
    iput p1, p0, Lcom/sec/android/secvision/sef/ConverterObject;->outFileResolution:I

    .line 107
    return-void
.end method

.method public setOutputFilename(Ljava/lang/String;)V
    .locals 0
    .param p1, "outputFileName"    # Ljava/lang/String;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/android/secvision/sef/ConverterObject;->outputFileName:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public setStartTime(I)V
    .locals 0
    .param p1, "starttime"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/sec/android/secvision/sef/ConverterObject;->startTime:I

    .line 72
    return-void
.end method

.method public setTranscodeMode(I)V
    .locals 0
    .param p1, "transcodeMode"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/sec/android/secvision/sef/ConverterObject;->transcodeMode:I

    .line 68
    return-void
.end method

.method public setVideoCodec(I)V
    .locals 0
    .param p1, "vtVideoCodec"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/sec/android/secvision/sef/ConverterObject;->videoCodecType:I

    .line 87
    return-void
.end method
