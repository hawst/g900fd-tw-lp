.class public Lcom/sec/android/secvision/sef/SEFConverter;
.super Ljava/lang/Object;
.source "SEFConverter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secvision/sef/SEFConverter$OnConvertListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SEFConverter"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method public static convertToMP4(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "srcPath"    # Ljava/lang/String;
    .param p1, "targetPath"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-static {p0}, Lcom/sec/android/secvision/sef/SEF;->getMajorDataType(Ljava/lang/String;)I

    move-result v0

    .line 26
    .local v0, "type":I
    packed-switch v0, :pswitch_data_0

    .line 33
    const-string v1, "SEFConverter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "This type of file is not yet supported. type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    :goto_0
    return-void

    .line 28
    :pswitch_0
    invoke-static {}, Lcom/sec/android/secvision/sef/VirtualTourConverter;->getInstance()Lcom/sec/android/secvision/sef/VirtualTourConverter;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Lcom/sec/android/secvision/sef/VirtualTourConverter;->convertToMP4(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 26
    :pswitch_data_0
    .packed-switch 0x850
        :pswitch_0
    .end packed-switch
.end method
