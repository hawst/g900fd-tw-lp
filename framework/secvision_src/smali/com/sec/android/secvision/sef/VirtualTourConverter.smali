.class Lcom/sec/android/secvision/sef/VirtualTourConverter;
.super Ljava/lang/Object;
.source "VirtualTourConverter.java"


# static fields
.field private static final BUFFER_ARRAY_SIZE:I = 0x4

.field private static final CONFIG:I = 0x1

.field private static final FORWARD_VELOCITY:F = 130.0f

.field private static final HIGH_QUALITY:I = 0x0

.field private static final LOW_QUALITY:I = 0x2

.field private static final MAX_FRAME_NUMBER_TO_ENCODE:I = 0x8ca

.field private static final MAX_PLAYING_TIME_IN_SEC:I = 0x5a

.field private static final MEDIUM_QUALITY:I = 0x1

.field private static final NO_ORIENTATION:I = 0x0

.field private static final SIDEWARD_VELOCITY:F = 20.0f

.field private static final TAG:Ljava/lang/String; = "VirtualTourConverter"

.field private static final TARGET_FRAME_MAXDIM:I = 0x320

.field private static final TARGET_FRAME_RATE:I = 0x19

.field private static final TARGET_QUALITY:I

.field private static mInstance:Lcom/sec/android/secvision/sef/VirtualTourConverter;


# instance fields
.field private final MSG_START_CONVERTING:I

.field private mFrameQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<[B>;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mHeight:I

.field private mImageSize:I

.field private mInputBuffer:[Ljava/nio/ByteBuffer;

.field private mIsLast:Z

.field private mIsProcessing:Z

.field private mMutexObject:Ljava/lang/Object;

.field private mOutputMP4file:Ljava/lang/String;

.field private mSweep:Lcom/voovio/sweep/Sweep;

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 274
    const-string v0, "MP4Converter"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 275
    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mSweep:Lcom/voovio/sweep/Sweep;

    .line 45
    iput-object v0, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mOutputMP4file:Ljava/lang/String;

    .line 46
    iput-object v0, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHandler:Landroid/os/Handler;

    .line 47
    iput v2, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->MSG_START_CONVERTING:I

    .line 49
    iput v2, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mImageSize:I

    .line 50
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mFrameQueue:Ljava/util/concurrent/BlockingQueue;

    .line 51
    iput v2, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    .line 52
    iput v2, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    .line 53
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mInputBuffer:[Ljava/nio/ByteBuffer;

    .line 54
    iput-boolean v2, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mIsLast:Z

    .line 55
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mMutexObject:Ljava/lang/Object;

    .line 56
    iput-boolean v2, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mIsProcessing:Z

    .line 60
    return-void
.end method

.method private static native SNConvert2MP4([BII)I
.end method

.method private static native SNConvert2MP4_deinit(II)I
.end method

.method private static native SNConvert2MP4_init(IILjava/lang/String;IIII)I
.end method

.method static synthetic access$000(Lcom/sec/android/secvision/sef/VirtualTourConverter;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secvision/sef/VirtualTourConverter;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/secvision/sef/VirtualTourConverter;->convertToMP4()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/secvision/sef/VirtualTourConverter;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secvision/sef/VirtualTourConverter;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/android/secvision/sef/VirtualTourConverter;->processShare()V

    return-void
.end method

.method private convertToMP4()V
    .locals 14

    .prologue
    .line 227
    iget-object v1, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mMutexObject:Ljava/lang/Object;

    monitor-enter v1

    .line 229
    :try_start_0
    iget v0, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    if-nez v0, :cond_1

    .line 230
    :cond_0
    const-string v0, "VirtualTourConverter"

    const-string v2, "Waiting for width or height notify"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    iget-object v0, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mMutexObject:Ljava/lang/Object;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v2, v3}, Ljava/lang/Object;->wait(J)V

    .line 232
    const-string v0, "VirtualTourConverter"

    const-string v2, "Received notify for width or height waiting over"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    :cond_1
    :goto_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 239
    const-string v0, "VirtualTourConverter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Encoding parameters: width="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mOutputMP4file="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mOutputMP4file:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    .line 241
    .local v12, "timeNow":J
    iget v0, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    iget v1, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    iget-object v2, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mOutputMP4file:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x19

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/sec/android/secvision/sef/VirtualTourConverter;->SNConvert2MP4_init(IILjava/lang/String;IIII)I

    .line 242
    const-string v0, "VirtualTourConverter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Time taken for initialisation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long/2addr v2, v12

    long-to-double v2, v2

    const-wide v4, 0x41cdcd6500000000L    # 1.0E9

    div-double/2addr v2, v4

    double-to-float v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    const-wide/16 v10, 0x0

    .line 245
    .local v10, "timeConvert":J
    const/4 v7, 0x0

    .line 248
    .local v7, "count":I
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mFrameQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [B

    .line 250
    .local v9, "frame":[B
    array-length v0, v9

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/16 v0, 0x8ca

    if-le v7, v0, :cond_3

    .line 251
    :cond_2
    const-string v0, "VirtualTourConverter"

    const-string v1, "Last frame in encoder!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    .line 265
    const-string v0, "VirtualTourConverter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " VConfig: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Time taken for conversion = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    long-to-float v2, v10

    float-to-double v2, v2

    const-wide v4, 0x41cdcd6500000000L    # 1.0E9

    div-double/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Count = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    const-string v0, "VirtualTourConverter"

    const-string v1, "Deinitialising"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    .line 269
    iget v0, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    iget v1, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    invoke-static {v0, v1}, Lcom/sec/android/secvision/sef/VirtualTourConverter;->SNConvert2MP4_deinit(II)I

    .line 270
    const-string v0, "VirtualTourConverter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Time taken for deinitialisation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    long-to-float v2, v2

    long-to-float v3, v12

    sub-float/2addr v2, v3

    float-to-double v2, v2

    const-wide v4, 0x41cdcd6500000000L    # 1.0E9

    div-double/2addr v2, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    return-void

    .line 234
    .end local v7    # "count":I
    .end local v9    # "frame":[B
    .end local v10    # "timeConvert":J
    .end local v12    # "timeNow":J
    :catch_0
    move-exception v8

    .line 235
    .local v8, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v8}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_0

    .line 237
    .end local v8    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 254
    .restart local v7    # "count":I
    .restart local v9    # "frame":[B
    .restart local v10    # "timeConvert":J
    .restart local v12    # "timeNow":J
    :cond_3
    :try_start_4
    const-string v0, "VirtualTourConverter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Entering Count convert = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    .line 256
    iget v0, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    iget v1, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    invoke-static {v9, v0, v1}, Lcom/sec/android/secvision/sef/VirtualTourConverter;->SNConvert2MP4([BII)I

    .line 257
    invoke-static {}, Ljava/lang/System;->nanoTime()J
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    move-result-wide v0

    sub-long/2addr v0, v12

    add-long/2addr v10, v0

    .line 258
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 260
    .end local v9    # "frame":[B
    :catch_1
    move-exception v8

    .line 261
    .restart local v8    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v8}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_1
.end method

.method public static declared-synchronized getInstance()Lcom/sec/android/secvision/sef/VirtualTourConverter;
    .locals 2

    .prologue
    .line 65
    const-class v1, Lcom/sec/android/secvision/sef/VirtualTourConverter;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mInstance:Lcom/sec/android/secvision/sef/VirtualTourConverter;

    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;

    invoke-direct {v0}, Lcom/sec/android/secvision/sef/VirtualTourConverter;-><init>()V

    sput-object v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mInstance:Lcom/sec/android/secvision/sef/VirtualTourConverter;

    .line 68
    :cond_0
    sget-object v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mInstance:Lcom/sec/android/secvision/sef/VirtualTourConverter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private processShare()V
    .locals 20

    .prologue
    .line 151
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mSweep:Lcom/voovio/sweep/Sweep;

    invoke-virtual {v3}, Lcom/voovio/sweep/Sweep;->getImageWidth()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    .line 152
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mSweep:Lcom/voovio/sweep/Sweep;

    invoke-virtual {v3}, Lcom/voovio/sweep/Sweep;->getImageHeight()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    .line 155
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    if-le v3, v4, :cond_1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    const/16 v4, 0x320

    if-le v3, v4, :cond_1

    .line 156
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    int-to-float v4, v4

    div-float v10, v3, v4

    .line 157
    .local v10, "fAspectRatio":F
    const/16 v3, 0x320

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    .line 158
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    int-to-float v3, v3

    mul-float/2addr v3, v10

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    .line 166
    .end local v10    # "fAspectRatio":F
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    div-int/lit8 v3, v3, 0x10

    mul-int/lit8 v3, v3, 0x10

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    .line 167
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    div-int/lit8 v3, v3, 0x10

    mul-int/lit8 v3, v3, 0x10

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    .line 169
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mMutexObject:Ljava/lang/Object;

    monitor-enter v4

    .line 170
    :try_start_0
    const-string v3, "VirtualTourConverter"

    const-string v5, "Notifying"

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mMutexObject:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->notify()V

    .line 172
    const-string v3, "VirtualTourConverter"

    const-string v5, "Notified"

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 176
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    mul-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x4

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mImageSize:I

    .line 181
    new-instance v2, Lcom/voovio/sweep/SweepOffScreen;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    const/16 v5, 0x19

    const/4 v6, 0x1

    new-instance v7, Lcom/sec/android/secvision/sef/SEFImageProvider;

    invoke-direct {v7}, Lcom/sec/android/secvision/sef/SEFImageProvider;-><init>()V

    invoke-direct/range {v2 .. v7}, Lcom/voovio/sweep/SweepOffScreen;-><init>(IIIILcom/voovio/sweep/ImageProvider;)V

    .line 182
    .local v2, "sweepOffScreen":Lcom/voovio/sweep/SweepOffScreen;
    const/high16 v3, 0x43020000    # 130.0f

    const/high16 v4, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3, v4}, Lcom/voovio/sweep/SweepOffScreen;->setAutodriveVelocity(FF)V

    .line 183
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mSweep:Lcom/voovio/sweep/Sweep;

    invoke-virtual {v2, v3}, Lcom/voovio/sweep/SweepOffScreen;->setSweep(Lcom/voovio/sweep/Sweep;)V

    .line 186
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    const/4 v3, 0x4

    if-ge v11, v3, :cond_2

    .line 187
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mInputBuffer:[Ljava/nio/ByteBuffer;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mImageSize:I

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    aput-object v4, v3, v11

    .line 186
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 159
    .end local v2    # "sweepOffScreen":Lcom/voovio/sweep/SweepOffScreen;
    .end local v11    # "i":I
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    if-le v3, v4, :cond_0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    const/16 v4, 0x320

    if-le v3, v4, :cond_0

    .line 160
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    int-to-float v4, v4

    div-float v10, v3, v4

    .line 161
    .restart local v10    # "fAspectRatio":F
    const/16 v3, 0x320

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    .line 162
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    int-to-float v3, v3

    mul-float/2addr v3, v10

    float-to-int v3, v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    goto/16 :goto_0

    .line 173
    .end local v10    # "fAspectRatio":F
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 190
    .restart local v2    # "sweepOffScreen":Lcom/voovio/sweep/SweepOffScreen;
    .restart local v11    # "i":I
    :cond_2
    const/4 v8, 0x0

    .local v8, "count":I
    const/4 v12, 0x0

    .line 192
    .local v12, "index":I
    const-wide/16 v16, 0x0

    .local v16, "timeObtain":J
    const-wide/16 v14, 0x0

    .line 194
    .local v14, "timeNow":J
    :cond_3
    const-string v3, "VirtualTourConverter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Entering Count obtain = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v14

    .line 197
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mInputBuffer:[Ljava/nio/ByteBuffer;

    aget-object v3, v3, v12

    invoke-virtual {v2, v3}, Lcom/voovio/sweep/SweepOffScreen;->getNextFrameAsByteBuffer(Ljava/nio/ByteBuffer;)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mIsLast:Z

    .line 198
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long/2addr v4, v14

    add-long v16, v16, v4

    .line 201
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mFrameQueue:Ljava/util/concurrent/BlockingQueue;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mInputBuffer:[Ljava/nio/ByteBuffer;

    aget-object v4, v4, v12

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    .line 206
    :goto_2
    add-int/lit8 v8, v8, 0x1

    .line 207
    rem-int/lit8 v12, v8, 0x4

    .line 208
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mInputBuffer:[Ljava/nio/ByteBuffer;

    aget-object v3, v3, v12

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 210
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mIsLast:Z

    if-nez v3, :cond_4

    const/16 v3, 0x8ca

    if-le v8, v3, :cond_3

    .line 212
    :cond_4
    :try_start_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mFrameQueue:Ljava/util/concurrent/BlockingQueue;

    const/4 v4, 0x1

    new-array v4, v4, [B

    invoke-interface {v3, v4}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    .line 216
    :goto_3
    const-string v3, "VirtualTourConverter"

    const-string v4, "Last frame in renderer!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mIsProcessing:Z

    .line 218
    invoke-virtual {v2}, Lcom/voovio/sweep/SweepOffScreen;->dispose()V

    .line 223
    const-string v3, "VirtualTourConverter"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " VConfig: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Time taken for obtaining = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v16

    long-to-float v5, v0

    float-to-double v6, v5

    const-wide v18, 0x41cdcd6500000000L    # 1.0E9

    div-double v6, v6, v18

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " count = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Exiting processShare()"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    return-void

    .line 202
    :catch_0
    move-exception v9

    .line 203
    .local v9, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v9}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_2

    .line 213
    .end local v9    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v9

    .line 214
    .restart local v9    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v9}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3
.end method

.method private startWithNewLooper()V
    .locals 2

    .prologue
    .line 130
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 131
    new-instance v0, Lcom/sec/android/secvision/sef/VirtualTourConverter$2;

    invoke-direct {v0, p0}, Lcom/sec/android/secvision/sef/VirtualTourConverter$2;-><init>(Lcom/sec/android/secvision/sef/VirtualTourConverter;)V

    iput-object v0, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHandler:Landroid/os/Handler;

    .line 144
    const-string v0, "VirtualTourConverter"

    const-string v1, "Send starting message: MSG_START_CONVERTING"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    iget-object v0, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 147
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 148
    return-void
.end method


# virtual methods
.method public declared-synchronized convertToMP4(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "srcPath"    # Ljava/lang/String;
    .param p2, "targetPath"    # Ljava/lang/String;

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    iput-object p2, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mOutputMP4file:Ljava/lang/String;

    .line 74
    new-instance v3, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;

    invoke-direct {v3}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;-><init>()V

    .line 76
    .local v3, "virtualTourDataManager":Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;
    const-string v4, "VirtualTourConverter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Sweep version: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/voovio/sweep/Sweep;->VERSION:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    const-string v4, "VirtualTourConverter"

    const-string v5, "Decode virtual tour sef."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    invoke-virtual {v3, p1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->decodeSEF(Ljava/lang/String;)Lcom/voovio/sweep/Sweep;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mSweep:Lcom/voovio/sweep/Sweep;

    .line 79
    iget-object v4, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mSweep:Lcom/voovio/sweep/Sweep;

    if-nez v4, :cond_1

    .line 80
    const-string v4, "VirtualTourConverter"

    const-string v5, "This is not a VirtualTour File"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    :cond_0
    monitor-exit p0

    return-void

    .line 85
    :cond_1
    const/4 v4, 0x1

    :try_start_1
    iput-boolean v4, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mIsProcessing:Z

    .line 86
    const-string v4, "VirtualTourConverter"

    const-string v5, "Starting MP4 Conversion"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    new-instance v0, Ljava/lang/Thread;

    new-instance v4, Lcom/sec/android/secvision/sef/VirtualTourConverter$1;

    invoke-direct {v4, p0}, Lcom/sec/android/secvision/sef/VirtualTourConverter$1;-><init>(Lcom/sec/android/secvision/sef/VirtualTourConverter;)V

    invoke-direct {v0, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 92
    .local v0, "conversionThread":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 96
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v4

    if-nez v4, :cond_3

    .line 97
    const-string v4, "VirtualTourConverter"

    const-string v5, "No looper in this thread."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    invoke-direct {p0}, Lcom/sec/android/secvision/sef/VirtualTourConverter;->startWithNewLooper()V

    .line 103
    :goto_0
    if-eqz v0, :cond_2

    .line 104
    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_2

    .line 106
    :try_start_2
    const-string v4, "VirtualTourConverter"

    const-string v5, "Waiting for Consumer Thread Join"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    invoke-virtual {v0}, Ljava/lang/Thread;->join()V

    .line 108
    const-string v4, "VirtualTourConverter"

    const-string v5, "Consumer Thread joined"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 118
    :cond_2
    :goto_1
    :try_start_3
    iget-object v4, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mFrameQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v4}, Ljava/util/concurrent/BlockingQueue;->clear()V

    .line 119
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mSweep:Lcom/voovio/sweep/Sweep;

    .line 120
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mWidth:I

    .line 121
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mHeight:I

    .line 122
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mIsLast:Z

    .line 123
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mIsProcessing:Z

    .line 124
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    const/4 v4, 0x4

    if-ge v2, v4, :cond_0

    .line 125
    iget-object v4, p0, Lcom/sec/android/secvision/sef/VirtualTourConverter;->mInputBuffer:[Ljava/nio/ByteBuffer;

    aget-object v4, v4, v2

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 124
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 100
    .end local v2    # "i":I
    :cond_3
    invoke-direct {p0}, Lcom/sec/android/secvision/sef/VirtualTourConverter;->processShare()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 72
    .end local v0    # "conversionThread":Ljava/lang/Thread;
    .end local v3    # "virtualTourDataManager":Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 109
    .restart local v0    # "conversionThread":Ljava/lang/Thread;
    .restart local v3    # "virtualTourDataManager":Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;
    :catch_0
    move-exception v1

    .line 110
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method
