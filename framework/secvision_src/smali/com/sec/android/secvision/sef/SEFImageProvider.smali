.class public Lcom/sec/android/secvision/sef/SEFImageProvider;
.super Lcom/voovio/sweep/ImageProvider;
.source "SEFImageProvider.java"


# instance fields
.field private TAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/voovio/sweep/ImageProvider;-><init>()V

    .line 13
    const-string v0, "VTImageProvider"

    iput-object v0, p0, Lcom/sec/android/secvision/sef/SEFImageProvider;->TAG:Ljava/lang/String;

    .line 16
    return-void
.end method


# virtual methods
.method public getImageDataByIndex(Ljava/lang/String;[BI)V
    .locals 10
    .param p1, "strFilePath"    # Ljava/lang/String;
    .param p2, "baImage"    # [B
    .param p3, "imageIndex"    # I

    .prologue
    const/4 v9, 0x0

    .line 23
    iget-object v5, p0, Lcom/sec/android/secvision/sef/SEFImageProvider;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "aKeyName = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 24
    add-int/lit8 v4, p3, 0x1

    .line 26
    .local v4, "tempIndex":I
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "VirtualTour_%03d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "aKeyName":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/secvision/sef/SEFImageProvider;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "aKeyName = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 30
    :try_start_0
    invoke-static {p1, v0}, Lcom/sec/android/secvision/sef/SEF;->getSEFData(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v3

    .line 31
    .local v3, "temp":[B
    array-length v1, v3

    .line 32
    .local v1, "copy_length":I
    array-length v5, v3

    array-length v6, p2

    if-eq v5, v6, :cond_0

    .line 34
    iget-object v5, p0, Lcom/sec/android/secvision/sef/SEFImageProvider;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Temporary array length = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, v3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is not the same as Image array length = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, p2

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    array-length v5, v3

    array-length v6, p2

    if-ge v5, v6, :cond_1

    array-length v1, v3

    .line 37
    :cond_0
    :goto_0
    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v3, v5, p2, v6, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 41
    .end local v1    # "copy_length":I
    .end local v3    # "temp":[B
    :goto_1
    return-void

    .line 35
    .restart local v1    # "copy_length":I
    .restart local v3    # "temp":[B
    :cond_1
    array-length v1, p2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 38
    .end local v1    # "copy_length":I
    .end local v3    # "temp":[B
    :catch_0
    move-exception v2

    .line 39
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method
