.class public Lcom/sec/android/secvision/sef/QdioRecorder;
.super Ljava/lang/Object;
.source "QdioRecorder.java"


# static fields
.field private static final QURAM_RECORDER_BUFFERSIZE:I = 0x4000

.field private static final TAG:Ljava/lang/String; = "SEFQdioRecorder"

.field private static qRecorder:Lcom/sec/android/secvision/sef/QdioRecorder;


# instance fields
.field private QURAM_RECORDER_AUDIO_ENCODING:I

.field private QURAM_RECORDER_BPP:I

.field private QURAM_RECORDER_CHANNELS:I

.field private QURAM_RECORDER_SAMPLERATE:I

.field private bufferSize:I

.field private mAudioSource:I

.field private recorder:Landroid/media/AudioRecord;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 187
    new-instance v0, Lcom/sec/android/secvision/sef/QdioRecorder;

    invoke-direct {v0}, Lcom/sec/android/secvision/sef/QdioRecorder;-><init>()V

    sput-object v0, Lcom/sec/android/secvision/sef/QdioRecorder;->qRecorder:Lcom/sec/android/secvision/sef/QdioRecorder;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    .line 15
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->mAudioSource:I

    .line 16
    const/16 v0, 0x4000

    iput v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    .line 17
    iput v1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_BPP:I

    .line 18
    const v0, 0xac44

    iput v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_SAMPLERATE:I

    .line 19
    iput v1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_CHANNELS:I

    .line 20
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_AUDIO_ENCODING:I

    .line 21
    return-void
.end method

.method public static getInstance()Lcom/sec/android/secvision/sef/QdioRecorder;
    .locals 1

    .prologue
    .line 172
    sget-object v0, Lcom/sec/android/secvision/sef/QdioRecorder;->qRecorder:Lcom/sec/android/secvision/sef/QdioRecorder;

    return-object v0
.end method


# virtual methods
.method public getAudioSampleRate()I
    .locals 1

    .prologue
    .line 176
    iget v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_SAMPLERATE:I

    return v0
.end method

.method public getBufferSize()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    return v0
.end method

.method public getChannel()I
    .locals 2

    .prologue
    .line 184
    iget v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_CHANNELS:I

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getRecorderBPP()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_BPP:I

    return v0
.end method

.method public init(I)Z
    .locals 8
    .param p1, "audioSource"    # I

    .prologue
    const/16 v3, 0x4000

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 24
    iget v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_SAMPLERATE:I

    iget v1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_CHANNELS:I

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_AUDIO_ENCODING:I

    invoke-static {v0, v1, v2}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v0

    iput v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    .line 25
    iget v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    if-ge v0, v3, :cond_0

    .line 26
    iput v3, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    .line 28
    :cond_0
    iput p1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->mAudioSource:I

    .line 29
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    if-eqz v0, :cond_1

    .line 30
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "recorder.getState() = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    :goto_0
    return v6

    .line 34
    :cond_1
    const-string v0, "SEFQdioRecorder"

    const-string v1, "make new Recorder"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mAudioSource : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->mAudioSource:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "QURAM_RECORDER_SAMPLERATE : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_SAMPLERATE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "QURAM_RECORDER_CHANNELS : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_CHANNELS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "QURAM_RECORDER_AUDIO_ENCODING : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_AUDIO_ENCODING:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bufferSize : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    new-instance v0, Landroid/media/AudioRecord;

    iget v1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->mAudioSource:I

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_SAMPLERATE:I

    iget v3, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_CHANNELS:I

    iget v4, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_AUDIO_ENCODING:I

    iget v5, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    .line 41
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    if-nez v0, :cond_2

    .line 42
    const-string v0, "SEFQdioRecorder"

    const-string v1, "===> recorder null!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v7

    .line 43
    goto/16 :goto_0

    .line 45
    :cond_2
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v6

    :goto_1
    move v6, v0

    goto/16 :goto_0

    :cond_3
    move v0, v7

    goto :goto_1
.end method

.method public init(II)Z
    .locals 8
    .param p1, "audioSource"    # I
    .param p2, "rate"    # I

    .prologue
    const/16 v3, 0x4000

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 49
    if-lez p2, :cond_0

    .line 50
    iput p2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_SAMPLERATE:I

    .line 52
    :cond_0
    iget v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_SAMPLERATE:I

    iget v1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_CHANNELS:I

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_AUDIO_ENCODING:I

    invoke-static {v0, v1, v2}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v0

    iput v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    .line 53
    iget v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    if-ge v0, v3, :cond_1

    .line 54
    iput v3, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    .line 56
    :cond_1
    iput p1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->mAudioSource:I

    .line 57
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    if-eqz v0, :cond_2

    .line 58
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "recorder.getState() = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    if-eqz v0, :cond_2

    .line 74
    :goto_0
    return v6

    .line 63
    :cond_2
    const-string v0, "SEFQdioRecorder"

    const-string v1, "make new Recorder"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mAudioSource : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->mAudioSource:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "QURAM_RECORDER_SAMPLERATE : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_SAMPLERATE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "QURAM_RECORDER_CHANNELS : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_CHANNELS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "QURAM_RECORDER_AUDIO_ENCODING : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_AUDIO_ENCODING:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bufferSize : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    new-instance v0, Landroid/media/AudioRecord;

    iget v1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->mAudioSource:I

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_SAMPLERATE:I

    iget v3, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_CHANNELS:I

    iget v4, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_AUDIO_ENCODING:I

    iget v5, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    .line 70
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    if-nez v0, :cond_3

    .line 71
    const-string v0, "SEFQdioRecorder"

    const-string v1, "===> recorder null!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v7

    .line 72
    goto/16 :goto_0

    .line 74
    :cond_3
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v6

    :goto_1
    move v6, v0

    goto/16 :goto_0

    :cond_4
    move v0, v7

    goto :goto_1
.end method

.method public init(III)Z
    .locals 8
    .param p1, "audioSource"    # I
    .param p2, "rate"    # I
    .param p3, "flag"    # I

    .prologue
    const/16 v3, 0x4000

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 78
    if-nez p3, :cond_2

    .line 79
    const/16 v0, 0x10

    iput v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_CHANNELS:I

    .line 83
    :goto_0
    if-lez p2, :cond_0

    .line 84
    iput p2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_SAMPLERATE:I

    .line 86
    :cond_0
    iget v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_SAMPLERATE:I

    iget v1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_CHANNELS:I

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_AUDIO_ENCODING:I

    invoke-static {v0, v1, v2}, Landroid/media/AudioRecord;->getMinBufferSize(III)I

    move-result v0

    iput v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    .line 87
    iget v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    if-ge v0, v3, :cond_1

    .line 88
    iput v3, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    .line 90
    :cond_1
    iput p1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->mAudioSource:I

    .line 91
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    if-eqz v0, :cond_3

    .line 92
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "recorder.getState() = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    if-eqz v0, :cond_3

    .line 108
    :goto_1
    return v6

    .line 81
    :cond_2
    const/16 v0, 0xc

    iput v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_CHANNELS:I

    goto :goto_0

    .line 97
    :cond_3
    const-string v0, "SEFQdioRecorder"

    const-string v1, "make new Recorder"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AudioSource : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->mAudioSource:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RECORDER_SAMPLERATE : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_SAMPLERATE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RECORDER_CHANNELS : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_CHANNELS:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "RECORDER_AUDIO_ENCODING : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_AUDIO_ENCODING:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bufferSize : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    new-instance v0, Landroid/media/AudioRecord;

    iget v1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->mAudioSource:I

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_SAMPLERATE:I

    iget v3, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_CHANNELS:I

    iget v4, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_AUDIO_ENCODING:I

    iget v5, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    invoke-direct/range {v0 .. v5}, Landroid/media/AudioRecord;-><init>(IIIII)V

    iput-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    .line 104
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    if-nez v0, :cond_4

    .line 105
    const-string v0, "SEFQdioRecorder"

    const-string v1, "===> recorder null!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v7

    .line 106
    goto/16 :goto_1

    .line 108
    :cond_4
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v6

    :goto_2
    move v6, v0

    goto/16 :goto_1

    :cond_5
    move v0, v7

    goto :goto_2
.end method

.method public read([B)I
    .locals 4
    .param p1, "buffer"    # [B

    .prologue
    const/4 v1, -0x3

    .line 139
    const/4 v0, 0x0

    .line 140
    .local v0, "ret":I
    iget-object v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    if-nez v2, :cond_0

    .line 150
    :goto_0
    return v1

    .line 143
    :cond_0
    iget-object v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v2}, Landroid/media/AudioRecord;->getState()I

    move-result v2

    if-nez v2, :cond_2

    .line 144
    iget-object v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    monitor-enter v2

    .line 145
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    if-eqz v3, :cond_1

    .line 146
    monitor-exit v2

    goto :goto_0

    .line 147
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149
    :cond_2
    iget-object v1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->bufferSize:I

    invoke-virtual {v1, p1, v2, v3}, Landroid/media/AudioRecord;->read([BII)I

    move-result v0

    move v1, v0

    .line 150
    goto :goto_0
.end method

.method public start()Z
    .locals 3

    .prologue
    .line 116
    const-string v0, "SEFQdioRecorder"

    const-string v1, "qRecorder start start"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    if-nez v0, :cond_0

    .line 118
    iget-object v1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    monitor-enter v1

    .line 119
    :try_start_0
    const-string v0, "SEFQdioRecorder"

    const-string v2, "stop recorder in start"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    .line 122
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    :cond_0
    iget v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->mAudioSource:I

    iget v1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_SAMPLERATE:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/secvision/sef/QdioRecorder;->init(II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 125
    const-string v0, "SEFQdioRecorder"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Audio Recorder init failed samplerate = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->QURAM_RECORDER_SAMPLERATE:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const/4 v0, 0x0

    .line 135
    :goto_0
    return v0

    .line 122
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 128
    :cond_1
    iget-object v1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    monitor-enter v1

    .line 129
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getRecordingState()I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    .line 130
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    .line 131
    const-string v0, "SEFQdioRecorder"

    const-string v2, "record start"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 134
    const-string v0, "SEFQdioRecorder"

    const-string v1, "qrecorder start end"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    const/4 v0, 0x1

    goto :goto_0

    .line 133
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 154
    const-string v0, "SEFQdioRecorder"

    const-string v1, "qRecorder stop start"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    if-nez v0, :cond_0

    .line 168
    :goto_0
    return-void

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->getState()I

    move-result v0

    if-nez v0, :cond_2

    .line 159
    iget-object v1, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    monitor-enter v1

    .line 160
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    if-eqz v0, :cond_1

    .line 161
    monitor-exit v1

    goto :goto_0

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164
    :cond_2
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->stop()V

    .line 165
    iget-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    invoke-virtual {v0}, Landroid/media/AudioRecord;->release()V

    .line 166
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/secvision/sef/QdioRecorder;->recorder:Landroid/media/AudioRecord;

    .line 167
    const-string v0, "SEFQdioRecorder"

    const-string v1, "qRecorder stop end"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
