.class public Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;
.super Landroid/os/AsyncTask;
.source "SoundShotConverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/secvision/sef/SoundShotConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ConvertMP4Task"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/secvision/sef/SoundShotConverter;


# direct methods
.method public constructor <init>(Lcom/sec/android/secvision/sef/SoundShotConverter;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;->this$0:Lcom/sec/android/secvision/sef/SoundShotConverter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 12
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 33
    const/4 v2, 0x0

    .line 34
    .local v2, "progress":I
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 35
    .local v5, "result":Ljava/lang/Integer;
    new-instance v0, Lcom/sec/android/secvision/sef/ConverterObject;

    invoke-direct {v0}, Lcom/sec/android/secvision/sef/ConverterObject;-><init>()V

    .line 36
    .local v0, "converterElement":Lcom/sec/android/secvision/sef/ConverterObject;
    const/4 v6, 0x0

    .line 37
    .local v6, "start_offset":I
    const/4 v1, 0x0

    .line 39
    .local v1, "length":I
    new-array v7, v11, [Ljava/lang/Integer;

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "progress":I
    .local v3, "progress":I
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {p0, v7}, Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;->publishProgress([Ljava/lang/Object;)V

    .line 42
    iget-object v7, p0, Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;->this$0:Lcom/sec/android/secvision/sef/SoundShotConverter;

    # getter for: Lcom/sec/android/secvision/sef/SoundShotConverter;->mSrcPath:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/secvision/sef/SoundShotConverter;->access$000(Lcom/sec/android/secvision/sef/SoundShotConverter;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/secvision/sef/SEF;->checkAudioInJPEG(Ljava/lang/String;)Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;

    move-result-object v4

    .line 44
    .local v4, "qdioData":Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;
    if-eqz v4, :cond_1

    .line 45
    invoke-virtual {v4, v10}, Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;->getStartOffset(I)I

    move-result v6

    .line 46
    invoke-virtual {v4, v10}, Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;->getLength(I)I

    move-result v1

    .line 47
    const-string v7, "SoundShotConverter"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "offset/len = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    if-lez v6, :cond_0

    if-gtz v1, :cond_2

    .line 49
    :cond_0
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .end local v5    # "result":Ljava/lang/Integer;
    move v2, v3

    .line 67
    .end local v3    # "progress":I
    .restart local v2    # "progress":I
    :goto_0
    return-object v5

    .line 52
    .end local v2    # "progress":I
    .restart local v3    # "progress":I
    .restart local v5    # "result":Ljava/lang/Integer;
    :cond_1
    const-string v7, "SoundShotConverter"

    const-string v8, "because qdioData is null, the outFilePath of convertToMP4 is null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .end local v5    # "result":Ljava/lang/Integer;
    move v2, v3

    .end local v3    # "progress":I
    .restart local v2    # "progress":I
    goto :goto_0

    .line 56
    .end local v2    # "progress":I
    .restart local v3    # "progress":I
    .restart local v5    # "result":Ljava/lang/Integer;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;->this$0:Lcom/sec/android/secvision/sef/SoundShotConverter;

    # getter for: Lcom/sec/android/secvision/sef/SoundShotConverter;->mSrcPath:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/secvision/sef/SoundShotConverter;->access$000(Lcom/sec/android/secvision/sef/SoundShotConverter;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/sec/android/secvision/sef/ConverterObject;->setInputFilename(Ljava/lang/String;)V

    .line 57
    iget-object v7, p0, Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;->this$0:Lcom/sec/android/secvision/sef/SoundShotConverter;

    # getter for: Lcom/sec/android/secvision/sef/SoundShotConverter;->mTargetPath:Ljava/lang/String;
    invoke-static {v7}, Lcom/sec/android/secvision/sef/SoundShotConverter;->access$100(Lcom/sec/android/secvision/sef/SoundShotConverter;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/sec/android/secvision/sef/ConverterObject;->setOutputFilename(Ljava/lang/String;)V

    .line 58
    const-string v7, "/system/cameradata/secvision/soundshot/sound_shot_icon.png"

    invoke-virtual {v0, v7}, Lcom/sec/android/secvision/sef/ConverterObject;->setIconFileName(Ljava/lang/String;)V

    .line 59
    const/4 v7, 0x5

    invoke-virtual {v0, v7}, Lcom/sec/android/secvision/sef/ConverterObject;->setOutputFileResolution(I)V

    .line 60
    invoke-virtual {v0, v6}, Lcom/sec/android/secvision/sef/ConverterObject;->setAudioOffset(I)V

    .line 61
    invoke-virtual {v0, v1}, Lcom/sec/android/secvision/sef/ConverterObject;->setAudioLength(I)V

    .line 62
    invoke-virtual {v0, v11}, Lcom/sec/android/secvision/sef/ConverterObject;->setTranscodeMode(I)V

    .line 64
    iget-object v7, p0, Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;->this$0:Lcom/sec/android/secvision/sef/SoundShotConverter;

    invoke-virtual {v7, v0}, Lcom/sec/android/secvision/sef/SoundShotConverter;->startProcessing(Lcom/sec/android/secvision/sef/ConverterObject;)I

    .line 65
    new-array v7, v11, [Ljava/lang/Integer;

    add-int/lit8 v2, v3, 0x1

    .end local v3    # "progress":I
    .restart local v2    # "progress":I
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {p0, v7}, Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;->publishProgress([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 29
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;->this$0:Lcom/sec/android/secvision/sef/SoundShotConverter;

    # getter for: Lcom/sec/android/secvision/sef/SoundShotConverter;->mListener:Lcom/sec/android/secvision/sef/SEFConverter$OnConvertListener;
    invoke-static {v0}, Lcom/sec/android/secvision/sef/SoundShotConverter;->access$200(Lcom/sec/android/secvision/sef/SoundShotConverter;)Lcom/sec/android/secvision/sef/SEFConverter$OnConvertListener;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/secvision/sef/SEFConverter$OnConvertListener;->onConvertCompleted(I)V

    .line 78
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 29
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 3
    .param p1, "progress"    # [Ljava/lang/Integer;

    .prologue
    .line 71
    const-string v0, "SoundShotConverter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onProgressUpdate: progress"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v0, p0, Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;->this$0:Lcom/sec/android/secvision/sef/SoundShotConverter;

    # getter for: Lcom/sec/android/secvision/sef/SoundShotConverter;->mListener:Lcom/sec/android/secvision/sef/SEFConverter$OnConvertListener;
    invoke-static {v0}, Lcom/sec/android/secvision/sef/SoundShotConverter;->access$200(Lcom/sec/android/secvision/sef/SoundShotConverter;)Lcom/sec/android/secvision/sef/SEFConverter$OnConvertListener;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/secvision/sef/SEFConverter$OnConvertListener;->onConvertProgress(I)V

    .line 73
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 29
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
