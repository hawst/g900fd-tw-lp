.class Lcom/sec/android/secvision/sef/SoundShotConverter;
.super Ljava/lang/Object;
.source "SoundShotConverter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;
    }
.end annotation


# static fields
.field private static final SOUND_SHOT_ICON_FILE:Ljava/lang/String; = "/system/cameradata/secvision/soundshot/sound_shot_icon.png"

.field public static final TAG:Ljava/lang/String; = "SoundShotConverter"


# instance fields
.field private mListener:Lcom/sec/android/secvision/sef/SEFConverter$OnConvertListener;

.field private mSrcPath:Ljava/lang/String;

.field private mTargetPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 85
    :try_start_0
    const-string v1, "MP4Converter"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 87
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 89
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object v0, p0, Lcom/sec/android/secvision/sef/SoundShotConverter;->mSrcPath:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/sec/android/secvision/sef/SoundShotConverter;->mTargetPath:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/sec/android/secvision/sef/SoundShotConverter;->mListener:Lcom/sec/android/secvision/sef/SEFConverter$OnConvertListener;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/secvision/sef/SoundShotConverter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/sef/SoundShotConverter;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/android/secvision/sef/SoundShotConverter;->mSrcPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/secvision/sef/SoundShotConverter;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/sef/SoundShotConverter;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/android/secvision/sef/SoundShotConverter;->mTargetPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/secvision/sef/SoundShotConverter;)Lcom/sec/android/secvision/sef/SEFConverter$OnConvertListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/sef/SoundShotConverter;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/sec/android/secvision/sef/SoundShotConverter;->mListener:Lcom/sec/android/secvision/sef/SEFConverter$OnConvertListener;

    return-object v0
.end method


# virtual methods
.method public convertToMP4(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/secvision/sef/SEFConverter$OnConvertListener;)V
    .locals 8
    .param p1, "srcPath"    # Ljava/lang/String;
    .param p2, "targetPath"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/sec/android/secvision/sef/SEFConverter$OnConvertListener;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/sec/android/secvision/sef/SoundShotConverter;->mSrcPath:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/sec/android/secvision/sef/SoundShotConverter;->mTargetPath:Ljava/lang/String;

    .line 22
    iput-object p3, p0, Lcom/sec/android/secvision/sef/SoundShotConverter;->mListener:Lcom/sec/android/secvision/sef/SEFConverter$OnConvertListener;

    .line 24
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 25
    .local v0, "Start":J
    new-instance v2, Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;

    invoke-direct {v2, p0}, Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;-><init>(Lcom/sec/android/secvision/sef/SoundShotConverter;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/sec/android/secvision/sef/SoundShotConverter$ConvertMP4Task;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 26
    const-string v2, "SoundShotConverter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Time taken for converting = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long/2addr v4, v0

    long-to-double v4, v4

    const-wide v6, 0x41cdcd6500000000L    # 1.0E9

    div-double/2addr v4, v6

    double-to-float v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    return-void
.end method

.method public native getInputParamerterAnalysis(Ljava/lang/String;III)I
.end method

.method public native startProcessing(Lcom/sec/android/secvision/sef/ConverterObject;)I
.end method

.method public native stopProcessing()I
.end method
