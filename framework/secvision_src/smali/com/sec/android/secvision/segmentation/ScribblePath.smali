.class public Lcom/sec/android/secvision/segmentation/ScribblePath;
.super Landroid/graphics/Path;
.source "ScribblePath.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mMode:Lcom/sec/android/secvision/segmentation/SegmentationMode;

.field private mPointArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/secvision/segmentation/ScribblePoint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/graphics/Path;-><init>()V

    .line 11
    sget-object v0, Lcom/sec/android/secvision/segmentation/SegmentationMode;->MODE_NONE:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mMode:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    .line 14
    sget-object v0, Lcom/sec/android/secvision/segmentation/SegmentationMode;->MODE_NONE:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mMode:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    .line 15
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    .line 16
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/secvision/segmentation/ScribblePath;)V
    .locals 2
    .param p1, "src"    # Lcom/sec/android/secvision/segmentation/ScribblePath;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    .line 11
    sget-object v0, Lcom/sec/android/secvision/segmentation/SegmentationMode;->MODE_NONE:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mMode:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    .line 20
    iget-object v0, p1, Lcom/sec/android/secvision/segmentation/ScribblePath;->mMode:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mMode:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    .line 22
    return-void
.end method


# virtual methods
.method public getPointArray()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/secvision/segmentation/ScribblePoint;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSegmentationMode()Lcom/sec/android/secvision/segmentation/SegmentationMode;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mMode:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    return-object v0
.end method

.method public lineTo(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/secvision/segmentation/ScribblePoint;

    invoke-direct {v1, p1, p2}, Lcom/sec/android/secvision/segmentation/ScribblePoint;-><init>(FF)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 54
    invoke-super {p0, p1, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 55
    return-void
.end method

.method public moveTo(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/secvision/segmentation/ScribblePoint;

    invoke-direct {v1, p1, p2}, Lcom/sec/android/secvision/segmentation/ScribblePoint;-><init>(FF)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    invoke-super {p0, p1, p2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 65
    return-void
.end method

.method public offset(FF)V
    .locals 3
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 77
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 78
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/secvision/segmentation/ScribblePoint;

    .line 79
    .local v1, "p":Lcom/sec/android/secvision/segmentation/ScribblePoint;
    iget v2, v1, Lcom/sec/android/secvision/segmentation/ScribblePoint;->x:F

    add-float/2addr v2, p1

    iput v2, v1, Lcom/sec/android/secvision/segmentation/ScribblePoint;->x:F

    .line 80
    iget v2, v1, Lcom/sec/android/secvision/segmentation/ScribblePoint;->y:F

    add-float/2addr v2, p2

    iput v2, v1, Lcom/sec/android/secvision/segmentation/ScribblePoint;->y:F

    goto :goto_0

    .line 84
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "p":Lcom/sec/android/secvision/segmentation/ScribblePoint;
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/graphics/Path;->offset(FF)V

    .line 85
    return-void
.end method

.method public offset(FFLandroid/graphics/Path;)V
    .locals 3
    .param p1, "dx"    # F
    .param p2, "dy"    # F
    .param p3, "dst"    # Landroid/graphics/Path;

    .prologue
    .line 89
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    .line 90
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/secvision/segmentation/ScribblePoint;

    .line 91
    .local v1, "p":Lcom/sec/android/secvision/segmentation/ScribblePoint;
    iget v2, v1, Lcom/sec/android/secvision/segmentation/ScribblePoint;->x:F

    add-float/2addr v2, p1

    iput v2, v1, Lcom/sec/android/secvision/segmentation/ScribblePoint;->x:F

    .line 92
    iget v2, v1, Lcom/sec/android/secvision/segmentation/ScribblePoint;->y:F

    add-float/2addr v2, p2

    iput v2, v1, Lcom/sec/android/secvision/segmentation/ScribblePoint;->y:F

    goto :goto_0

    .line 96
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "p":Lcom/sec/android/secvision/segmentation/ScribblePoint;
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/graphics/Path;->offset(FFLandroid/graphics/Path;)V

    .line 97
    return-void
.end method

.method public pointsToPath()Landroid/graphics/Path;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 37
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 38
    .local v1, "path":Landroid/graphics/Path;
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 39
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/secvision/segmentation/ScribblePoint;

    iget v3, v2, Lcom/sec/android/secvision/segmentation/ScribblePoint;->x:F

    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/secvision/segmentation/ScribblePoint;

    iget v2, v2, Lcom/sec/android/secvision/segmentation/ScribblePoint;->y:F

    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 40
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 41
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/secvision/segmentation/ScribblePoint;

    iget v3, v2, Lcom/sec/android/secvision/segmentation/ScribblePoint;->x:F

    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/secvision/segmentation/ScribblePoint;

    iget v2, v2, Lcom/sec/android/secvision/segmentation/ScribblePoint;->y:F

    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    .end local v0    # "i":I
    :cond_0
    return-object v1
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mPointArray:Ljava/util/ArrayList;

    .line 70
    sget-object v0, Lcom/sec/android/secvision/segmentation/SegmentationMode;->MODE_NONE:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mMode:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    .line 72
    invoke-super {p0}, Landroid/graphics/Path;->reset()V

    .line 73
    return-void
.end method

.method public setSegmentationMode(Lcom/sec/android/secvision/segmentation/SegmentationMode;)V
    .locals 0
    .param p1, "mode"    # Lcom/sec/android/secvision/segmentation/SegmentationMode;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/android/secvision/segmentation/ScribblePath;->mMode:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    .line 30
    return-void
.end method
