.class Lcom/sec/android/secvision/segmentation/ScribblePoint;
.super Ljava/lang/Object;
.source "ScribblePoint.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field x:F

.field y:F


# direct methods
.method public constructor <init>(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput p1, p0, Lcom/sec/android/secvision/segmentation/ScribblePoint;->x:F

    .line 11
    iput p2, p0, Lcom/sec/android/secvision/segmentation/ScribblePoint;->y:F

    .line 12
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    iput v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePoint;->x:F

    .line 16
    iput v0, p0, Lcom/sec/android/secvision/segmentation/ScribblePoint;->y:F

    .line 17
    return-void
.end method
