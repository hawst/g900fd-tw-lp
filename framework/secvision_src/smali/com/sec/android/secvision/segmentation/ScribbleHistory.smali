.class Lcom/sec/android/secvision/segmentation/ScribbleHistory;
.super Ljava/util/ArrayList;
.source "ScribbleHistory.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Lcom/sec/android/secvision/segmentation/ScribblePath;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private redoStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/sec/android/secvision/segmentation/ScribblePath;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 10
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->redoStack:Ljava/util/Stack;

    return-void
.end method


# virtual methods
.method public add(Lcom/sec/android/secvision/segmentation/ScribblePath;)Z
    .locals 1
    .param p1, "p"    # Lcom/sec/android/secvision/segmentation/ScribblePath;

    .prologue
    .line 57
    invoke-super {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->redoStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 59
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 7
    check-cast p1, Lcom/sec/android/secvision/segmentation/ScribblePath;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->add(Lcom/sec/android/secvision/segmentation/ScribblePath;)Z

    move-result v0

    return v0
.end method

.method public canRedo()Z
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->redoStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canUndo()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 14
    invoke-virtual {p0}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->size()I

    move-result v1

    if-gt v1, v0, :cond_0

    .line 15
    const/4 v0, 0x0

    .line 17
    :cond_0
    return v0
.end method

.method public dump()Ljava/lang/String;
    .locals 3

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 65
    .local v0, "sb":Ljava/lang/StringBuffer;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "history size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 66
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 67
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "redo stack size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->redoStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 69
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getLast()Lcom/sec/android/secvision/segmentation/ScribblePath;
    .locals 2

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .line 36
    .local v0, "lastIndex":I
    if-gez v0, :cond_0

    .line 37
    const/4 v1, 0x0

    .line 39
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/secvision/segmentation/ScribblePath;

    goto :goto_0
.end method

.method public popLast()Lcom/sec/android/secvision/segmentation/ScribblePath;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->getLast()Lcom/sec/android/secvision/segmentation/ScribblePath;

    move-result-object v0

    .line 51
    .local v0, "p":Lcom/sec/android/secvision/segmentation/ScribblePath;
    invoke-virtual {p0}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->removeLast()V

    .line 52
    return-object v0
.end method

.method public redo()V
    .locals 2

    .prologue
    .line 30
    iget-object v1, p0, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->redoStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/secvision/segmentation/ScribblePath;

    .line 31
    .local v0, "p":Lcom/sec/android/secvision/segmentation/ScribblePath;
    invoke-super {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    return-void
.end method

.method public removeLast()V
    .locals 2

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .line 44
    .local v0, "lastIndex":I
    if-ltz v0, :cond_0

    .line 45
    invoke-virtual {p0, v0}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->remove(I)Ljava/lang/Object;

    .line 47
    :cond_0
    return-void
.end method

.method public undo()V
    .locals 2

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->popLast()Lcom/sec/android/secvision/segmentation/ScribblePath;

    move-result-object v0

    .line 26
    .local v0, "p":Lcom/sec/android/secvision/segmentation/ScribblePath;
    iget-object v1, p0, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->redoStack:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    return-void
.end method
