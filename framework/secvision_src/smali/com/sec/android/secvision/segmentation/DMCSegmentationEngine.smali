.class public Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;
.super Ljava/lang/Object;
.source "DMCSegmentationEngine.java"

# interfaces
.implements Lcom/sec/android/secvision/segmentation/ISegmenter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine$2;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final DEBUG_DUMP:Z

.field private static final DEFAULT_DOWNSIZE_FACTOR:I = 0x1

.field private static final MARKER_COLOR_BG:I = 0x1

.field private static final MARKER_COLOR_BG_PROBABLE:I = 0x3

.field private static final MARKER_COLOR_FG:I = 0x2

.field private static final MARKER_COLOR_FG_PROBABLE:I = 0x4

.field private static final MARKER_COLOR_UNKNOWN:I = 0x0

.field private static final MARKER_TYPE_AUTO_LOOP_BG:I = 0x1

.field private static final MARKER_TYPE_AUTO_LOOP_FG:I = 0x0

.field private static final MARKER_TYPE_AUTO_STROKE_BG:I = 0x3

.field private static final MARKER_TYPE_AUTO_STROKE_FG:I = 0x2

.field private static final MARKER_TYPE_MANUAL_LOOP_BG:I = 0x5

.field private static final MARKER_TYPE_MANUAL_LOOP_FG:I = 0x4

.field private static final MARKER_TYPE_MANUAL_STROKE_BG:I = 0x7

.field private static final MARKER_TYPE_MANUAL_STROKE_FG:I = 0x6

.field private static final MARKER_TYPE_NONE:I = -0x1

.field private static final PEN_WIDTH:I = 0x4

.field private static SEGMENT_BOUND_MARGIN:I = 0x0

.field private static final TAG:Ljava/lang/String; = "DMCSegmentationEngine"

.field private static mInstance:Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;


# instance fields
.field private mAlphaMatteBitmap:Landroid/graphics/Bitmap;

.field private mDownsizeFactor:I

.field private mInputBitmap:Landroid/graphics/Bitmap;

.field private mIntermediateBitmap:Landroid/graphics/Bitmap;

.field private mIsCancelled:Z

.field private mIsInitialized:Z

.field private mIsIntelligentUpscaling:Z

.field private mIsROI:Z

.field private mIsRunning:Z

.field private mIsSRIBColorModel:Z

.field private mIsSRIBGraphCut:Z

.field private mIsSlimcut:Z

.field private mMarkerBitmap:Landroid/graphics/Bitmap;

.field private mMarkerCanvas:Landroid/graphics/Canvas;

.field private mMaskBitmap:Landroid/graphics/Bitmap;

.field private mMatteBitmap:Landroid/graphics/Bitmap;

.field private mOutputBitmap:Landroid/graphics/Bitmap;

.field private mResultBitmap:Landroid/graphics/Bitmap;

.field private mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

.field private mSegmentBound:Landroid/graphics/RectF;

.field private mSegmentContour:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation
.end field

.field private mStrokeBitmap:Landroid/graphics/Bitmap;

.field public m_BetaSearchTime:D

.field public m_ColorEstimateTime:D

.field public m_ConstructGraphTime:D

.field public m_DiceCoefficient:D

.field public m_DjikstraTime:D

.field public m_ImageHeight:I

.field public m_ImageWidth:I

.field public m_MaxFlowTime:D

.field public m_ShapePriorGeodesicTime:D

.field public m_TotalTime:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 46
    const-string v1, "somp"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 47
    const-string v1, "OpenCv"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 48
    const-string v1, "SRIBSE_Lib"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 49
    const-string v1, "SRIBSE_Interface"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 50
    const-string v1, "DualShotMattingCoreLIB"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 51
    const-string v1, "DMCImageMatting"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 56
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v1

    if-eq v1, v0, :cond_0

    :goto_0
    sput-boolean v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->DEBUG_DUMP:Z

    .line 98
    const/16 v0, 0xa

    sput v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->SEGMENT_BOUND_MARGIN:I

    return-void

    .line 56
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    iput-boolean v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsRunning:Z

    .line 101
    iput-boolean v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsCancelled:Z

    .line 102
    iput-boolean v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsInitialized:Z

    .line 105
    iput-boolean v1, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsSRIBGraphCut:Z

    .line 106
    iput v1, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mDownsizeFactor:I

    .line 107
    iput-boolean v1, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsSRIBColorModel:Z

    .line 108
    iput-boolean v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsIntelligentUpscaling:Z

    .line 109
    iput-boolean v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsSlimcut:Z

    .line 110
    iput-boolean v1, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsROI:Z

    return-void
.end method

.method protected static native ManuallyCutObjectJNI(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
.end method

.method protected static native SegmentJNI(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)I
.end method

.method static synthetic access$000(Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->dump()V

    return-void
.end method

.method private algoSettingUpdate(ZIZZZZ)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "sSRIB"    # Z
    .param p2, "mDownsizeFactor"    # I
    .param p3, "sSRIBColorModel"    # Z
    .param p4, "isROI"    # Z
    .param p5, "isIntelligentUpscaling"    # Z
    .param p6, "isSlimCut"    # Z

    .prologue
    .line 713
    invoke-static {p4}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->setROI(Z)V

    .line 714
    invoke-static {p5}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->setIntelligentUpscaling(Z)V

    .line 715
    invoke-static {p1, p3}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->setLibrariesJNI(ZZ)V

    .line 716
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    add-int/lit8 v2, p2, -0x1

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-static {v0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->setScaleFactorJNI(I)V

    .line 717
    invoke-static {p6}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->setSlimcut(Z)V

    .line 718
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mOutputBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method private blendMaskBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "sourceBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "maskBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v5, 0x0

    .line 586
    invoke-static {p1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 587
    .local v1, "b":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-direct {v0, v3}, Landroid/graphics/Paint;-><init>(I)V

    .line 588
    .local v0, "alphaPaint":Landroid/graphics/Paint;
    new-instance v3, Landroid/graphics/PorterDuffXfermode;

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 589
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 590
    .local v2, "c":Landroid/graphics/Canvas;
    invoke-virtual {v2, p2, v5, v5, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 591
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 593
    return-object v1
.end method

.method protected static native canRedoJNI()Z
.end method

.method protected static native canUndoJNI()Z
.end method

.method protected static native cancelSegmentationJNI()V
.end method

.method protected static native deInitSegmentationEngineJNI()V
.end method

.method private deserializeHistory(Ljava/lang/String;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 749
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 750
    .local v1, "fis":Ljava/io/FileInputStream;
    new-instance v2, Ljava/io/ObjectInputStream;

    invoke-direct {v2, v1}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 751
    .local v2, "ois":Ljava/io/ObjectInputStream;
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    iput-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    .line 752
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 760
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v2    # "ois":Ljava/io/ObjectInputStream;
    :goto_0
    return-void

    .line 753
    :catch_0
    move-exception v0

    .line 754
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 755
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v0

    .line 756
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 757
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v0

    .line 758
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private drawPathOnBitmap(Landroid/graphics/Bitmap;Lcom/sec/android/secvision/segmentation/ScribblePath;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "inputBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "path"    # Lcom/sec/android/secvision/segmentation/ScribblePath;

    .prologue
    .line 888
    invoke-static {p1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 889
    .local v0, "bm":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 890
    .local v1, "c":Landroid/graphics/Canvas;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 891
    .local v2, "paint":Landroid/graphics/Paint;
    invoke-virtual {v2}, Landroid/graphics/Paint;->reset()V

    .line 892
    const/high16 v3, 0x40800000    # 4.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 893
    const/high16 v3, -0x10000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 894
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 895
    invoke-virtual {p2}, Lcom/sec/android/secvision/segmentation/ScribblePath;->pointsToPath()Landroid/graphics/Path;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 896
    return-object v0
.end method

.method private dump()V
    .locals 26

    .prologue
    .line 763
    const-string v22, "DMCSegmentationEngine"

    const-string v23, "Dump Start."

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 764
    const-string v4, "/mnt/sdcard/AutoShapeDump"

    .line 765
    .local v4, "SCREENSHOTS_DIR_NAME":Ljava/lang/String;
    const-string v6, "Input_%s_%s_%s.png"

    .line 766
    .local v6, "SCREENSHOT_FILE_NAME_TEMPLATE_INPUT":Ljava/lang/String;
    const-string v10, "Result_%s_%s_%s.png"

    .line 767
    .local v10, "SCREENSHOT_FILE_NAME_TEMPLATE_RESULT":Ljava/lang/String;
    const-string v11, "Stroke_%s_%s_%s.png"

    .line 768
    .local v11, "SCREENSHOT_FILE_NAME_TEMPLATE_STROKE":Ljava/lang/String;
    const-string v7, "Intermediate_%s_%s_%s.png"

    .line 769
    .local v7, "SCREENSHOT_FILE_NAME_TEMPLATE_INTERMEDIATE_RESULT":Ljava/lang/String;
    const-string v8, "Mask_%s_%s_%s.raw"

    .line 770
    .local v8, "SCREENSHOT_FILE_NAME_TEMPLATE_MASK":Ljava/lang/String;
    const-string v5, "AlphaMask_%s_%s_%s.raw"

    .line 771
    .local v5, "SCREENSHOT_FILE_NAME_TEMPLATE_ALPHA":Ljava/lang/String;
    const-string v9, "PathHistory_%s.dat"

    .line 772
    .local v9, "SCREENSHOT_FILE_NAME_TEMPLATE_PATH_HISTORY":Ljava/lang/String;
    const-string v12, "%s/%s/%s"

    .line 774
    .local v12, "SCREENSHOT_FILE_PATH_TEMPLATE":Ljava/lang/String;
    new-instance v22, Ljava/text/SimpleDateFormat;

    const-string v23, "yyyy-MM-dd-HH-mm-ss"

    invoke-direct/range {v22 .. v23}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v23, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    invoke-direct/range {v23 .. v25}, Ljava/util/Date;-><init>(J)V

    invoke-virtual/range {v22 .. v23}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v13

    .line 776
    .local v13, "captureTime":Ljava/lang/String;
    new-instance v14, Ljava/io/File;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "/"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v14, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 777
    .local v14, "dir":Ljava/io/File;
    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v22

    if-nez v22, :cond_0

    .line 778
    invoke-virtual {v14}, Ljava/io/File;->mkdirs()Z

    .line 782
    :cond_0
    :try_start_0
    new-instance v16, Ljava/io/File;

    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v4, v22, v23

    const/16 v23, 0x1

    aput-object v13, v22, v23

    const/16 v23, 0x2

    const-string v24, ".nomedia"

    aput-object v24, v22, v23

    move-object/from16 v0, v22

    invoke-static {v12, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 783
    .local v16, "file":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->exists()Z

    move-result v22

    if-nez v22, :cond_1

    .line 784
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 791
    .end local v16    # "file":Ljava/io/File;
    :cond_1
    :goto_0
    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v13, v22, v23

    move-object/from16 v0, v22

    invoke-static {v9, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    .line 792
    .local v17, "historyFileName":Ljava/lang/String;
    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v4, v22, v23

    const/16 v23, 0x1

    aput-object v13, v22, v23

    const/16 v23, 0x2

    aput-object v17, v22, v23

    move-object/from16 v0, v22

    invoke-static {v12, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 794
    .local v18, "historyFilePath":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->serializeHistory(Lcom/sec/android/secvision/segmentation/ScribbleHistory;Ljava/lang/String;)V

    .line 797
    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mInputBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mInputBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x2

    aput-object v13, v22, v23

    move-object/from16 v0, v22

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 798
    .local v19, "imageFileName":Ljava/lang/String;
    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v4, v22, v23

    const/16 v23, 0x1

    aput-object v13, v22, v23

    const/16 v23, 0x2

    aput-object v19, v22, v23

    move-object/from16 v0, v22

    invoke-static {v12, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .line 801
    .local v20, "imageFilePath":Ljava/lang/String;
    :try_start_1
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 802
    .restart local v16    # "file":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->createNewFile()Z

    .line 803
    new-instance v21, Ljava/io/FileOutputStream;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 804
    .local v21, "out":Ljava/io/FileOutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mInputBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v22, v0

    sget-object v23, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v24, 0x64

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move/from16 v2, v24

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 805
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->flush()V

    .line 806
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 812
    .end local v16    # "file":Ljava/io/File;
    .end local v21    # "out":Ljava/io/FileOutputStream;
    :goto_1
    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mStrokeBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mStrokeBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x2

    aput-object v13, v22, v23

    move-object/from16 v0, v22

    invoke-static {v11, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 813
    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v4, v22, v23

    const/16 v23, 0x1

    aput-object v13, v22, v23

    const/16 v23, 0x2

    aput-object v19, v22, v23

    move-object/from16 v0, v22

    invoke-static {v12, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .line 816
    :try_start_2
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 817
    .restart local v16    # "file":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->createNewFile()Z

    .line 818
    new-instance v21, Ljava/io/FileOutputStream;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 819
    .restart local v21    # "out":Ljava/io/FileOutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mStrokeBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v22, v0

    sget-object v23, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v24, 0x64

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move/from16 v2, v24

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 820
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->flush()V

    .line 821
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 827
    .end local v16    # "file":Ljava/io/File;
    .end local v21    # "out":Ljava/io/FileOutputStream;
    :goto_2
    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mResultBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mResultBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x2

    aput-object v13, v22, v23

    move-object/from16 v0, v22

    invoke-static {v10, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 828
    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v4, v22, v23

    const/16 v23, 0x1

    aput-object v13, v22, v23

    const/16 v23, 0x2

    aput-object v19, v22, v23

    move-object/from16 v0, v22

    invoke-static {v12, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .line 831
    :try_start_3
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 832
    .restart local v16    # "file":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->createNewFile()Z

    .line 833
    new-instance v21, Ljava/io/FileOutputStream;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 834
    .restart local v21    # "out":Ljava/io/FileOutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mResultBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v22, v0

    sget-object v23, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v24, 0x64

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move/from16 v2, v24

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 835
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->flush()V

    .line 836
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 842
    .end local v16    # "file":Ljava/io/File;
    .end local v21    # "out":Ljava/io/FileOutputStream;
    :goto_3
    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIntermediateBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIntermediateBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x2

    aput-object v13, v22, v23

    move-object/from16 v0, v22

    invoke-static {v7, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 843
    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v4, v22, v23

    const/16 v23, 0x1

    aput-object v13, v22, v23

    const/16 v23, 0x2

    aput-object v19, v22, v23

    move-object/from16 v0, v22

    invoke-static {v12, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .line 846
    :try_start_4
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 847
    .restart local v16    # "file":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->createNewFile()Z

    .line 848
    new-instance v21, Ljava/io/FileOutputStream;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 849
    .restart local v21    # "out":Ljava/io/FileOutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIntermediateBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v22, v0

    sget-object v23, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v24, 0x64

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move/from16 v2, v24

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 850
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->flush()V

    .line 851
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 857
    .end local v16    # "file":Ljava/io/File;
    .end local v21    # "out":Ljava/io/FileOutputStream;
    :goto_4
    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMaskBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMaskBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x2

    aput-object v13, v22, v23

    move-object/from16 v0, v22

    invoke-static {v8, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 858
    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v4, v22, v23

    const/16 v23, 0x1

    aput-object v13, v22, v23

    const/16 v23, 0x2

    aput-object v19, v22, v23

    move-object/from16 v0, v22

    invoke-static {v12, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .line 861
    :try_start_5
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 862
    .restart local v16    # "file":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->createNewFile()Z

    .line 863
    new-instance v21, Ljava/io/FileOutputStream;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 864
    .restart local v21    # "out":Ljava/io/FileOutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMaskBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/graphics/Bitmap;->mBuffer:[B

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/io/FileOutputStream;->write([B)V

    .line 865
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->flush()V

    .line 866
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    .line 872
    .end local v16    # "file":Ljava/io/File;
    .end local v21    # "out":Ljava/io/FileOutputStream;
    :goto_5
    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mAlphaMatteBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mAlphaMatteBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v24

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    const/16 v23, 0x2

    aput-object v13, v22, v23

    move-object/from16 v0, v22

    invoke-static {v5, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 873
    const/16 v22, 0x3

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v4, v22, v23

    const/16 v23, 0x1

    aput-object v13, v22, v23

    const/16 v23, 0x2

    aput-object v19, v22, v23

    move-object/from16 v0, v22

    invoke-static {v12, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .line 875
    :try_start_6
    new-instance v16, Ljava/io/File;

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 876
    .restart local v16    # "file":Ljava/io/File;
    invoke-virtual/range {v16 .. v16}, Ljava/io/File;->createNewFile()Z

    .line 877
    new-instance v21, Ljava/io/FileOutputStream;

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 878
    .restart local v21    # "out":Ljava/io/FileOutputStream;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mAlphaMatteBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/graphics/Bitmap;->mBuffer:[B

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/io/FileOutputStream;->write([B)V

    .line 879
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->flush()V

    .line 880
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    .line 884
    .end local v16    # "file":Ljava/io/File;
    .end local v21    # "out":Ljava/io/FileOutputStream;
    :goto_6
    const-string v22, "DMCSegmentationEngine"

    const-string v23, "Dump end."

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 885
    return-void

    .line 786
    .end local v17    # "historyFileName":Ljava/lang/String;
    .end local v18    # "historyFilePath":Ljava/lang/String;
    .end local v19    # "imageFileName":Ljava/lang/String;
    .end local v20    # "imageFilePath":Ljava/lang/String;
    :catch_0
    move-exception v15

    .line 787
    .local v15, "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 807
    .end local v15    # "e":Ljava/io/IOException;
    .restart local v17    # "historyFileName":Ljava/lang/String;
    .restart local v18    # "historyFilePath":Ljava/lang/String;
    .restart local v19    # "imageFileName":Ljava/lang/String;
    .restart local v20    # "imageFilePath":Ljava/lang/String;
    :catch_1
    move-exception v15

    .line 808
    .restart local v15    # "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 822
    .end local v15    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v15

    .line 823
    .restart local v15    # "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    .line 837
    .end local v15    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v15

    .line 838
    .restart local v15    # "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 852
    .end local v15    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v15

    .line 853
    .restart local v15    # "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 867
    .end local v15    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v15

    .line 868
    .restart local v15    # "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    .line 881
    .end local v15    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v15

    .line 882
    .restart local v15    # "e":Ljava/io/IOException;
    invoke-virtual {v15}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6
.end method

.method protected static native exitSegmentationJNI()V
.end method

.method protected static native getBetaSearchTime()D
.end method

.method protected static native getConstructGraphTime()D
.end method

.method protected static native getDiceCoefficient()D
.end method

.method protected static native getDjikstraTime()D
.end method

.method protected static native getGMMColorEstimateTime()D
.end method

.method protected static native getImageHeight()I
.end method

.method protected static native getImageWidth()I
.end method

.method public static declared-synchronized getInstance()Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;
    .locals 2

    .prologue
    .line 169
    const-class v1, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mInstance:Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;

    if-nez v0, :cond_0

    .line 170
    new-instance v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;

    invoke-direct {v0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;-><init>()V

    sput-object v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mInstance:Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;

    .line 172
    :cond_0
    sget-object v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mInstance:Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected static native getMaxFlowTime()D
.end method

.method protected static native getPathArrayJNI()[I
.end method

.method protected static native getShapePriorGeodesicTime()D
.end method

.method protected static native getTotalSegmentationTime()D
.end method

.method protected static native initSegmentationEngineJNI()V
.end method

.method private isAutoMode(Lcom/sec/android/secvision/segmentation/SegmentationMode;)Z
    .locals 3
    .param p1, "mode"    # Lcom/sec/android/secvision/segmentation/SegmentationMode;

    .prologue
    .line 504
    const/4 v0, 0x1

    .line 506
    .local v0, "flag":Z
    sget-object v1, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine$2;->$SwitchMap$com$sec$android$secvision$segmentation$SegmentationMode:[I

    invoke-virtual {p1}, Lcom/sec/android/secvision/segmentation/SegmentationMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 518
    const/4 v0, 0x0

    .line 522
    :goto_0
    return v0

    .line 511
    :pswitch_0
    const/4 v0, 0x1

    .line 512
    goto :goto_0

    .line 506
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private isProcessingCancelled()Z
    .locals 3

    .prologue
    .line 489
    const-string v0, "DMCSegmentationEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isProcessingCancelled() returns "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsCancelled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    iget-boolean v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsCancelled:Z

    return v0
.end method

.method private isProcessingRunning()Z
    .locals 3

    .prologue
    .line 497
    const-string v0, "DMCSegmentationEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isProcessingRunning() returns "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsRunning:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    iget-boolean v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsRunning:Z

    return v0
.end method

.method private isScribbleValid(Lcom/sec/android/secvision/segmentation/ScribblePath;)Z
    .locals 9
    .param p1, "path"    # Lcom/sec/android/secvision/segmentation/ScribblePath;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 675
    iget-object v7, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    invoke-virtual {v7}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->size()I

    move-result v7

    if-nez v7, :cond_0

    .line 676
    invoke-virtual {p1}, Lcom/sec/android/secvision/segmentation/ScribblePath;->getSegmentationMode()Lcom/sec/android/secvision/segmentation/SegmentationMode;

    move-result-object v7

    sget-object v8, Lcom/sec/android/secvision/segmentation/SegmentationMode;->MODE_AUTO_LOOP_FG:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    if-eq v7, v8, :cond_2

    .line 677
    const-string v6, "DMCSegmentationEngine"

    const-string v7, "WARNING! The first stroke must be AUTO LOOP."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    :goto_0
    return v5

    .line 681
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/secvision/segmentation/ScribblePath;->getSegmentationMode()Lcom/sec/android/secvision/segmentation/SegmentationMode;

    move-result-object v7

    sget-object v8, Lcom/sec/android/secvision/segmentation/SegmentationMode;->MODE_AUTO_LOOP_FG:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    if-ne v7, v8, :cond_1

    .line 682
    const-string v6, "DMCSegmentationEngine"

    const-string v7, "WARNING! Currently, consecutive loops are not supported."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 686
    :cond_1
    iget-object v7, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    invoke-virtual {v7, v5}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/secvision/segmentation/ScribblePath;

    .line 687
    .local v0, "head":Lcom/sec/android/secvision/segmentation/ScribblePath;
    invoke-virtual {v0}, Lcom/sec/android/secvision/segmentation/ScribblePath;->getSegmentationMode()Lcom/sec/android/secvision/segmentation/SegmentationMode;

    move-result-object v7

    sget-object v8, Lcom/sec/android/secvision/segmentation/SegmentationMode;->MODE_AUTO_LOOP_FG:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    if-eq v7, v8, :cond_2

    .line 688
    const-string v6, "DMCSegmentationEngine"

    const-string v7, "WARNING! The first stroke must be AUTO LOOP."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 694
    .end local v0    # "head":Lcom/sec/android/secvision/segmentation/ScribblePath;
    :cond_2
    const/4 v2, 0x0

    .line 695
    .local v2, "loopCount":I
    iget-object v7, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    invoke-virtual {v7}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/secvision/segmentation/ScribblePath;

    .line 696
    .local v4, "p":Lcom/sec/android/secvision/segmentation/ScribblePath;
    invoke-virtual {v4}, Lcom/sec/android/secvision/segmentation/ScribblePath;->getSegmentationMode()Lcom/sec/android/secvision/segmentation/SegmentationMode;

    move-result-object v7

    sget-object v8, Lcom/sec/android/secvision/segmentation/SegmentationMode;->MODE_AUTO_LOOP_FG:Lcom/sec/android/secvision/segmentation/SegmentationMode;

    if-ne v7, v8, :cond_3

    .line 697
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "loopCount":I
    .local v3, "loopCount":I
    if-le v2, v6, :cond_5

    .line 698
    const-string v6, "DMCSegmentationEngine"

    const-string v7, "WARNING! Currently, there must be only one AUTO LOOP in the scribble history."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .end local v3    # "loopCount":I
    .end local v4    # "p":Lcom/sec/android/secvision/segmentation/ScribblePath;
    .restart local v2    # "loopCount":I
    :cond_4
    move v5, v6

    .line 704
    goto :goto_0

    .end local v2    # "loopCount":I
    .restart local v3    # "loopCount":I
    .restart local v4    # "p":Lcom/sec/android/secvision/segmentation/ScribblePath;
    :cond_5
    move v2, v3

    .end local v3    # "loopCount":I
    .restart local v2    # "loopCount":I
    goto :goto_1
.end method

.method private makeSegmentContour()V
    .locals 20

    .prologue
    .line 615
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentContour:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    .line 616
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->getPathArrayJNI()[I

    move-result-object v1

    .line 617
    .local v1, "contourArray":[I
    const/4 v13, 0x0

    .line 618
    .local v13, "x":I
    const/4 v14, 0x0

    .line 619
    .local v14, "y":I
    const v7, 0x7fffffff

    .line 620
    .local v7, "minX":I
    const/high16 v5, -0x80000000

    .line 621
    .local v5, "maxX":I
    const v8, 0x7fffffff

    .line 622
    .local v8, "minY":I
    const/high16 v6, -0x80000000

    .line 623
    .local v6, "maxY":I
    const/4 v3, 0x0

    .line 624
    .local v3, "index":I
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "index":I
    .local v4, "index":I
    aget v10, v1, v3

    .line 625
    .local v10, "numContour":I
    const-string v15, "DMCSegmentationEngine"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "numContour = "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    if-gtz v10, :cond_0

    .line 628
    const/4 v6, 0x0

    move v5, v6

    move v8, v6

    move v7, v6

    .line 631
    :cond_0
    const/4 v9, 0x0

    .local v9, "n":I
    :goto_0
    if-ge v9, v10, :cond_3

    .line 632
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "index":I
    .restart local v3    # "index":I
    aget v12, v1, v4

    .line 633
    .local v12, "points":I
    if-lez v12, :cond_2

    .line 634
    new-instance v11, Landroid/graphics/Path;

    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    .line 635
    .local v11, "p":Landroid/graphics/Path;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentContour:Ljava/util/ArrayList;

    invoke-virtual {v15, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 636
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "index":I
    .restart local v4    # "index":I
    aget v13, v1, v3

    .line 637
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "index":I
    .restart local v3    # "index":I
    aget v14, v1, v4

    .line 638
    int-to-float v15, v13

    int-to-float v0, v14

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/graphics/Path;->moveTo(FF)V

    .line 640
    invoke-static {v7, v13}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 641
    invoke-static {v5, v13}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 642
    invoke-static {v8, v14}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 643
    invoke-static {v6, v14}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 645
    const/4 v2, 0x1

    .local v2, "i":I
    move v4, v3

    .end local v3    # "index":I
    .restart local v4    # "index":I
    :goto_1
    if-ge v2, v12, :cond_1

    .line 646
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "index":I
    .restart local v3    # "index":I
    aget v13, v1, v4

    .line 647
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "index":I
    .restart local v4    # "index":I
    aget v14, v1, v3

    .line 648
    int-to-float v15, v13

    int-to-float v0, v14

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v11, v15, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 650
    invoke-static {v7, v13}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 651
    invoke-static {v5, v13}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 652
    invoke-static {v8, v14}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 653
    invoke-static {v6, v14}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 645
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 655
    :cond_1
    invoke-virtual {v11}, Landroid/graphics/Path;->close()V

    .line 656
    add-int/lit8 v3, v4, 0x1

    .line 631
    .end local v2    # "i":I
    .end local v4    # "index":I
    .end local v11    # "p":Landroid/graphics/Path;
    .restart local v3    # "index":I
    :goto_2
    add-int/lit8 v9, v9, 0x1

    move v4, v3

    .end local v3    # "index":I
    .restart local v4    # "index":I
    goto :goto_0

    .line 658
    .end local v4    # "index":I
    .restart local v3    # "index":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 662
    .end local v3    # "index":I
    .end local v12    # "points":I
    .restart local v4    # "index":I
    :cond_3
    sget v15, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->SEGMENT_BOUND_MARGIN:I

    sub-int v15, v7, v15

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 663
    sget v15, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->SEGMENT_BOUND_MARGIN:I

    add-int/2addr v15, v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mOutputBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 664
    sget v15, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->SEGMENT_BOUND_MARGIN:I

    sub-int v15, v8, v15

    const/16 v16, 0x0

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 665
    sget v15, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->SEGMENT_BOUND_MARGIN:I

    add-int/2addr v15, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mOutputBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 666
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    int-to-float v0, v7

    move/from16 v16, v0

    int-to-float v0, v8

    move/from16 v17, v0

    int-to-float v0, v5

    move/from16 v18, v0

    int-to-float v0, v6

    move/from16 v19, v0

    invoke-virtual/range {v15 .. v19}, Landroid/graphics/RectF;->set(FFFF)V

    .line 668
    const-string v15, "DMCSegmentationEngine"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Input bitmap size (width, height): ("

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mOutputBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mOutputBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ")"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 669
    const-string v15, "DMCSegmentationEngine"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Segmented bound (left, top, right, bottom, width, height): ("

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    sub-int v17, v5, v7

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    sub-int v17, v6, v8

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ")"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 671
    return-void
.end method

.method private profile_data(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "imagePath"    # Ljava/lang/String;
    .param p2, "markerPath"    # Ljava/lang/String;
    .param p3, "gtPath"    # Ljava/lang/String;
    .param p4, "resultsPath"    # Ljava/lang/String;

    .prologue
    .line 722
    invoke-static {p1, p2, p3, p4}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->startJNI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->getImageWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->m_ImageWidth:I

    .line 725
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->getImageHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->m_ImageHeight:I

    .line 726
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->getGMMColorEstimateTime()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->m_ColorEstimateTime:D

    .line 727
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->getConstructGraphTime()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->m_ConstructGraphTime:D

    .line 728
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->getDjikstraTime()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->m_DjikstraTime:D

    .line 729
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->getShapePriorGeodesicTime()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->m_ShapePriorGeodesicTime:D

    .line 730
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->getMaxFlowTime()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->m_MaxFlowTime:D

    .line 731
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->getBetaSearchTime()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->m_BetaSearchTime:D

    .line 732
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->getTotalSegmentationTime()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->m_TotalTime:D

    .line 733
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->getDiceCoefficient()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->m_DiceCoefficient:D

    .line 734
    return-void
.end method

.method protected static native redoJNI(Landroid/graphics/Bitmap;)V
.end method

.method protected static native redoJNI(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
.end method

.method protected static native resetJNI()V
.end method

.method private resetProcessing()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 480
    const-string v0, "DMCSegmentationEngine"

    const-string v1, "resetProcessing()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    iput-boolean v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsRunning:Z

    .line 484
    iput-boolean v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsCancelled:Z

    .line 485
    return-void
.end method

.method private save(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPath"    # Ljava/lang/String;

    .prologue
    .line 708
    invoke-static {p1}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->saveImagesJNI(Ljava/lang/String;)V

    .line 709
    return-void
.end method

.method protected static native saveImagesJNI(Ljava/lang/String;)V
.end method

.method private serializeHistory(Lcom/sec/android/secvision/segmentation/ScribbleHistory;Ljava/lang/String;)V
    .locals 3
    .param p1, "history"    # Lcom/sec/android/secvision/segmentation/ScribbleHistory;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 738
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 739
    .local v1, "fos":Ljava/io/FileOutputStream;
    new-instance v2, Ljava/io/ObjectOutputStream;

    invoke-direct {v2, v1}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 740
    .local v2, "oos":Ljava/io/ObjectOutputStream;
    invoke-virtual {v2, p1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 741
    invoke-virtual {v2}, Ljava/io/ObjectOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 745
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    .end local v2    # "oos":Ljava/io/ObjectOutputStream;
    :goto_0
    return-void

    .line 742
    :catch_0
    move-exception v0

    .line 743
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private setCurrentMarkerVector(Lcom/sec/android/secvision/segmentation/ScribblePath;)V
    .locals 8
    .param p1, "path"    # Lcom/sec/android/secvision/segmentation/ScribblePath;

    .prologue
    .line 597
    if-eqz p1, :cond_1

    .line 598
    invoke-virtual {p1}, Lcom/sec/android/secvision/segmentation/ScribblePath;->getPointArray()Ljava/util/ArrayList;

    move-result-object v5

    .line 600
    .local v5, "pointArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/segmentation/ScribblePoint;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    new-array v0, v6, [I

    .line 601
    .local v0, "allPoints":[I
    const/4 v1, 0x0

    .line 602
    .local v1, "i":I
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/secvision/segmentation/ScribblePoint;

    .line 603
    .local v4, "point":Lcom/sec/android/secvision/segmentation/ScribblePoint;
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    iget v6, v4, Lcom/sec/android/secvision/segmentation/ScribblePoint;->x:F

    float-to-int v6, v6

    aput v6, v0, v1

    .line 604
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    iget v6, v4, Lcom/sec/android/secvision/segmentation/ScribblePoint;->y:F

    float-to-int v6, v6

    aput v6, v0, v2

    goto :goto_0

    .line 607
    .end local v4    # "point":Lcom/sec/android/secvision/segmentation/ScribblePoint;
    :cond_0
    const/high16 v6, 0x40800000    # 4.0f

    invoke-static {v6, v0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->setCurrentMarkerVectorJNI(F[I)V

    .line 612
    .end local v0    # "allPoints":[I
    .end local v1    # "i":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "pointArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/segmentation/ScribblePoint;>;"
    :goto_1
    return-void

    .line 610
    :cond_1
    const-string v6, "DMCSegmentationEngine"

    const-string v7, "Scribble path is null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected static native setCurrentMarkerVectorJNI(F[I)V
.end method

.method protected static native setIntelligentUpscaling(Z)V
.end method

.method protected static native setLibrariesJNI(ZZ)V
.end method

.method protected static native setMarkerImageJNI(Landroid/graphics/Bitmap;I)V
.end method

.method protected static native setROI(Z)V
.end method

.method protected static native setScaleFactorJNI(I)V
.end method

.method protected static native setSlimcut(Z)V
.end method

.method protected static native setSourceImageJNI(Landroid/graphics/Bitmap;)V
.end method

.method protected static native startJNI(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private startOfProcessing()V
    .locals 2

    .prologue
    .line 471
    const-string v0, "DMCSegmentationEngine"

    const-string v1, "startOfProcessing()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsRunning:Z

    .line 475
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsCancelled:Z

    .line 476
    return-void
.end method

.method protected static native undoJNI(Landroid/graphics/Bitmap;)V
.end method

.method protected static native undoJNI(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
.end method

.method private writeLatestScribblesToMarkerBitmap()I
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/16 v6, 0xff

    const/4 v5, 0x0

    .line 526
    iget-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    invoke-virtual {v3}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->getLast()Lcom/sec/android/secvision/segmentation/ScribblePath;

    move-result-object v1

    .line 527
    .local v1, "p":Lcom/sec/android/secvision/segmentation/ScribblePath;
    const/4 v0, -0x1

    .line 529
    .local v0, "lastMarkerType":I
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 530
    .local v2, "paint":Landroid/graphics/Paint;
    invoke-virtual {v2}, Landroid/graphics/Paint;->reset()V

    .line 531
    const/high16 v3, 0x40800000    # 4.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 532
    iget-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMarkerBitmap:Landroid/graphics/Bitmap;

    invoke-static {v6, v5, v5, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 534
    sget-object v3, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine$2;->$SwitchMap$com$sec$android$secvision$segmentation$SegmentationMode:[I

    invoke-virtual {v1}, Lcom/sec/android/secvision/segmentation/ScribblePath;->getSegmentationMode()Lcom/sec/android/secvision/segmentation/SegmentationMode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/secvision/segmentation/SegmentationMode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 576
    const-string v3, "DMCSegmentationEngine"

    const-string v4, "Invalide segmentation mode"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    :goto_0
    iget-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMarkerCanvas:Landroid/graphics/Canvas;

    invoke-virtual {v1}, Lcom/sec/android/secvision/segmentation/ScribblePath;->pointsToPath()Landroid/graphics/Path;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 582
    return v0

    .line 536
    :pswitch_0
    const/4 v3, 0x4

    invoke-static {v6, v5, v5, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 537
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 538
    const/4 v0, 0x0

    .line 539
    goto :goto_0

    .line 541
    :pswitch_1
    const/4 v3, 0x3

    invoke-static {v6, v5, v5, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 542
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 543
    const/4 v0, 0x1

    .line 544
    goto :goto_0

    .line 546
    :pswitch_2
    invoke-static {v6, v5, v5, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 547
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 548
    const/4 v0, 0x2

    .line 549
    goto :goto_0

    .line 551
    :pswitch_3
    invoke-static {v6, v5, v5, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 552
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 553
    const/4 v0, 0x3

    .line 554
    goto :goto_0

    .line 556
    :pswitch_4
    invoke-static {v6, v5, v5, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 557
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 558
    const/4 v0, 0x4

    .line 559
    goto :goto_0

    .line 561
    :pswitch_5
    invoke-static {v6, v5, v5, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 562
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 563
    const/4 v0, 0x5

    .line 564
    goto :goto_0

    .line 566
    :pswitch_6
    invoke-static {v6, v5, v5, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 567
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 568
    const/4 v0, 0x6

    .line 569
    goto :goto_0

    .line 571
    :pswitch_7
    invoke-static {v6, v5, v5, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 572
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 573
    const/4 v0, 0x7

    .line 574
    goto :goto_0

    .line 534
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public canRedo()Z
    .locals 4

    .prologue
    .line 317
    iget-boolean v1, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsInitialized:Z

    if-nez v1, :cond_0

    .line 318
    const-string v1, "DMCSegmentationEngine"

    const-string v2, "ERROR! canRedo() is called after deinitialization."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    const/4 v0, 0x0

    .line 328
    :goto_0
    return v0

    .line 322
    :cond_0
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->canRedoJNI()Z

    move-result v0

    .line 325
    .local v0, "ret":Z
    const-string v1, "DMCSegmentationEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "canRedo() returns "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    invoke-virtual {v3}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->dump()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public canUndo()Z
    .locals 4

    .prologue
    .line 272
    iget-boolean v1, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsInitialized:Z

    if-nez v1, :cond_0

    .line 273
    const-string v1, "DMCSegmentationEngine"

    const-string v2, "ERROR! canUndo() is called after deinitialization."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const/4 v0, 0x0

    .line 287
    :goto_0
    return v0

    .line 277
    :cond_0
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->canUndoJNI()Z

    move-result v0

    .line 279
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    invoke-virtual {v1}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->size()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_1

    .line 280
    const/4 v0, 0x0

    .line 284
    :cond_1
    const-string v1, "DMCSegmentationEngine"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "canUndo() returns "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    invoke-virtual {v3}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->dump()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public cancelSegmentation()V
    .locals 2

    .prologue
    .line 261
    const-string v0, "DMCSegmentationEngine"

    const-string v1, "cancelSegmentation()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    iget-boolean v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 263
    const-string v0, "DMCSegmentationEngine"

    const-string v1, "ERROR! cancelSegmentation() is called after deinitialization."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsCancelled:Z

    .line 267
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->cancelSegmentationJNI()V

    .line 268
    return-void
.end method

.method public deInitialize()V
    .locals 2

    .prologue
    .line 433
    const-string v0, "DMCSegmentationEngine"

    const-string v1, "deInitialize()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsInitialized:Z

    .line 436
    invoke-direct {p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->isProcessingRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 437
    const-string v0, "DMCSegmentationEngine"

    const-string v1, "ERROR! deInitialize() has been called while segmentation is still running."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 452
    :goto_0
    return-void

    .line 440
    :cond_0
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentContour:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 441
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentContour:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 444
    :cond_1
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    if-eqz v0, :cond_2

    .line 445
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    invoke-virtual {v0}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->clear()V

    .line 448
    :cond_2
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->deInitSegmentationEngineJNI()V

    .line 450
    invoke-direct {p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->resetProcessing()V

    goto :goto_0
.end method

.method public exitSegmentation()V
    .locals 0

    .prologue
    .line 901
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->exitSegmentationJNI()V

    .line 902
    return-void
.end method

.method public getResultImageMask()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 358
    const-string v0, "DMCSegmentationEngine"

    const-string v1, "getResultImageMask()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMaskBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getResultImageSegment()Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 364
    const-string v1, "DMCSegmentationEngine"

    const-string v2, "getResultImageSegment()"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    iget-boolean v1, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsInitialized:Z

    if-nez v1, :cond_0

    .line 367
    const-string v1, "DMCSegmentationEngine"

    const-string v2, "ERROR! getResultImageSegment() is called after deinitialization."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    :cond_0
    iget-object v1, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mInputBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->blendMaskBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 372
    .local v0, "b":Landroid/graphics/Bitmap;
    new-instance v7, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v7, v3, v3, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 373
    .local v7, "inputRect":Landroid/graphics/RectF;
    iget-object v1, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    invoke-virtual {v7, v1}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 374
    const-string v1, "DMCSegmentationEngine"

    const-string v2, "ERROR! The segment bound exceeds the image boundary."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    :cond_1
    iget-object v1, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    float-to-int v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public getResultImageSegment(I)Landroid/graphics/Bitmap;
    .locals 12
    .param p1, "matteWidth"    # I

    .prologue
    const/4 v2, 0x0

    .line 382
    const-string v3, "DMCSegmentationEngine"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getResultImageSegment("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    iget-boolean v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsInitialized:Z

    if-nez v3, :cond_0

    .line 385
    const-string v3, "DMCSegmentationEngine"

    const-string v4, "ERROR! getResultImageSegment(matteWidth) is called after deinitialization."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    :cond_0
    move v9, p1

    .line 390
    .local v9, "width":I
    const/4 v3, -0x1

    if-ne p1, v3, :cond_1

    .line 391
    invoke-virtual {p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->getResultImageSegment()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 427
    :goto_0
    return-object v2

    .line 394
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 395
    .local v10, "time":J
    const-string v3, "DMCSegmentationEngine"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMatteFromMask("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    new-instance v3, Lcom/sec/android/secvision/segmentation/ImageMatte;

    iget-object v4, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mInputBitmap:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v3, v4, v5}, Lcom/sec/android/secvision/segmentation/ImageMatte;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    invoke-virtual {v3, v9}, Lcom/sec/android/secvision/segmentation/ImageMatte;->getMatteFromMask(I)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mAlphaMatteBitmap:Landroid/graphics/Bitmap;

    .line 398
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v10, v4, v10

    .line 399
    const-string v3, "DMCSegmentationEngine"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "It has taken "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms to matte"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 402
    monitor-enter p0

    .line 403
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mAlphaMatteBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_2

    invoke-direct {p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->isProcessingCancelled()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 404
    :cond_2
    monitor-exit p0

    goto :goto_0

    .line 428
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 407
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mInputBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mAlphaMatteBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v2, v3}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->blendMaskBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 408
    .local v0, "b":Landroid/graphics/Bitmap;
    new-instance v8, Landroid/graphics/RectF;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-direct {v8, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 409
    .local v8, "inputRect":Landroid/graphics/RectF;
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    invoke-virtual {v8, v2}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 410
    const-string v2, "DMCSegmentationEngine"

    const-string v3, "ERROR! The segment bound exceeds the image boundary."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    :cond_4
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    float-to-int v1, v2

    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    iget-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    float-to-int v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mResultBitmap:Landroid/graphics/Bitmap;

    .line 415
    sget-boolean v2, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->DEBUG_DUMP:Z

    if-eqz v2, :cond_5

    .line 416
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mInputBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v2, v3}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->blendMaskBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 417
    .local v1, "bm":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget-object v4, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v4, v4

    iget-object v5, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    float-to-int v5, v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIntermediateBitmap:Landroid/graphics/Bitmap;

    .line 418
    iget-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mInputBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/secvision/segmentation/ScribblePath;

    invoke-direct {p0, v3, v2}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->drawPathOnBitmap(Landroid/graphics/Bitmap;Lcom/sec/android/secvision/segmentation/ScribblePath;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mStrokeBitmap:Landroid/graphics/Bitmap;

    .line 419
    new-instance v2, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine$1;

    invoke-direct {v2, p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine$1;-><init>(Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;)V

    invoke-virtual {v2}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine$1;->start()V

    .line 427
    .end local v1    # "bm":Landroid/graphics/Bitmap;
    :cond_5
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mResultBitmap:Landroid/graphics/Bitmap;

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public initialize(Landroid/graphics/Bitmap;)Z
    .locals 8
    .param p1, "imageToBeSegmented"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v7, 0x1

    .line 177
    const-string v0, "DMCSegmentationEngine"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initialize(): input bitmap = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    iput-boolean v7, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsInitialized:Z

    .line 180
    iput-object p1, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mInputBitmap:Landroid/graphics/Bitmap;

    .line 181
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mOutputBitmap:Landroid/graphics/Bitmap;

    .line 182
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMarkerBitmap:Landroid/graphics/Bitmap;

    .line 183
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMaskBitmap:Landroid/graphics/Bitmap;

    .line 184
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMarkerBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMarkerCanvas:Landroid/graphics/Canvas;

    .line 185
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentContour:Ljava/util/ArrayList;

    .line 186
    new-instance v0, Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    invoke-direct {v0}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    .line 187
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentBound:Landroid/graphics/RectF;

    .line 189
    invoke-direct {p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->resetProcessing()V

    .line 191
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->deInitSegmentationEngineJNI()V

    .line 192
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->resetJNI()V

    .line 193
    invoke-static {p1}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->setSourceImageJNI(Landroid/graphics/Bitmap;)V

    .line 194
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->initSegmentationEngineJNI()V

    .line 196
    iget-boolean v1, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsSRIBGraphCut:Z

    iget v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mDownsizeFactor:I

    iget-boolean v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsSRIBColorModel:Z

    iget-boolean v4, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsROI:Z

    iget-boolean v5, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsIntelligentUpscaling:Z

    iget-boolean v6, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsSlimcut:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->algoSettingUpdate(ZIZZZZ)Landroid/graphics/Bitmap;

    .line 198
    return v7
.end method

.method public redo()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->canRedo()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 334
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    invoke-virtual {v2}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->redo()V

    .line 337
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 338
    .local v0, "time":J
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    invoke-virtual {v2}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->getLast()Lcom/sec/android/secvision/segmentation/ScribblePath;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->setCurrentMarkerVector(Lcom/sec/android/secvision/segmentation/ScribblePath;)V

    .line 339
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mOutputBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-static {v2, v3}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->redoJNI(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 342
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 343
    const-string v2, "DMCSegmentationEngine"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "It has taken "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms to redo"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    invoke-direct {p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->makeSegmentContour()V

    .line 349
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentContour:Ljava/util/ArrayList;

    .line 352
    .end local v0    # "time":J
    :goto_0
    return-object v2

    .line 351
    :cond_0
    const-string v2, "DMCSegmentationEngine"

    const-string v3, "No more scribbles to redo"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentContour:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 456
    const-string v0, "DMCSegmentationEngine"

    const-string v1, "reset()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentContour:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 459
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentContour:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    if-eqz v0, :cond_1

    .line 463
    iget-object v0, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    invoke-virtual {v0}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->clear()V

    .line 466
    :cond_1
    invoke-static {}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->resetJNI()V

    .line 467
    return-void
.end method

.method public segment(Lcom/sec/android/secvision/segmentation/ScribblePath;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "inputPath"    # Lcom/sec/android/secvision/segmentation/ScribblePath;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/secvision/segmentation/ScribblePath;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203
    iget-boolean v4, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mIsInitialized:Z

    if-nez v4, :cond_0

    .line 204
    const-string v4, "DMCSegmentationEngine"

    const-string v5, "ERROR! segment() is called after deinitialization."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    const/4 v4, 0x0

    .line 255
    :goto_0
    return-object v4

    .line 208
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->isScribbleValid(Lcom/sec/android/secvision/segmentation/ScribblePath;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 209
    iget-object v4, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentContour:Ljava/util/ArrayList;

    goto :goto_0

    .line 212
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->startOfProcessing()V

    .line 215
    const-string v4, "DMCSegmentationEngine"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "input mode = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/secvision/segmentation/ScribblePath;->getSegmentationMode()Lcom/sec/android/secvision/segmentation/SegmentationMode;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    invoke-static {p1}, Lcom/sec/android/secvision/segmentation/IntelligentModeSelection;->decideSegmentationMode(Lcom/sec/android/secvision/segmentation/ScribblePath;)Lcom/sec/android/secvision/segmentation/SegmentationMode;

    move-result-object v1

    .line 217
    .local v1, "mode":Lcom/sec/android/secvision/segmentation/SegmentationMode;
    invoke-virtual {p1, v1}, Lcom/sec/android/secvision/segmentation/ScribblePath;->setSegmentationMode(Lcom/sec/android/secvision/segmentation/SegmentationMode;)V

    .line 218
    const-string v4, "DMCSegmentationEngine"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "converted mode = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/android/secvision/segmentation/ScribblePath;->getSegmentationMode()Lcom/sec/android/secvision/segmentation/SegmentationMode;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    iget-object v4, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    new-instance v5, Lcom/sec/android/secvision/segmentation/ScribblePath;

    invoke-direct {v5, p1}, Lcom/sec/android/secvision/segmentation/ScribblePath;-><init>(Lcom/sec/android/secvision/segmentation/ScribblePath;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->add(Lcom/sec/android/secvision/segmentation/ScribblePath;)Z

    .line 224
    invoke-direct {p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->writeLatestScribblesToMarkerBitmap()I

    move-result v0

    .line 227
    .local v0, "lastMarkerType":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 228
    .local v2, "time":J
    invoke-direct {p0, p1}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->setCurrentMarkerVector(Lcom/sec/android/secvision/segmentation/ScribblePath;)V

    .line 229
    iget-object v4, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMarkerBitmap:Landroid/graphics/Bitmap;

    invoke-static {v4, v0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->setMarkerImageJNI(Landroid/graphics/Bitmap;I)V

    .line 231
    invoke-direct {p0, v1}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->isAutoMode(Lcom/sec/android/secvision/segmentation/SegmentationMode;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 232
    iget-object v4, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mOutputBitmap:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-static {v4, v5}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->SegmentJNI(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)I

    .line 238
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 239
    const-string v4, "DMCSegmentationEngine"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "It has taken "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ms to segment"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    monitor-enter p0

    .line 244
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->isProcessingCancelled()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 245
    const-string v4, "DMCSegmentationEngine"

    const-string v5, "segment(): Segmentation has been canceled."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    iget-object v4, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    invoke-virtual {v4}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->removeLast()V

    .line 253
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->resetProcessing()V

    .line 255
    iget-object v4, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentContour:Ljava/util/ArrayList;

    monitor-exit p0

    goto/16 :goto_0

    .line 256
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 234
    :cond_2
    iget-object v4, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mOutputBitmap:Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-static {v4, v5}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->ManuallyCutObjectJNI(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 250
    :cond_3
    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->makeSegmentContour()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public undo()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292
    invoke-virtual {p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->canUndo()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 293
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    invoke-virtual {v2}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->undo()V

    .line 296
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 297
    .local v0, "time":J
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mScribbleHistory:Lcom/sec/android/secvision/segmentation/ScribbleHistory;

    invoke-virtual {v2}, Lcom/sec/android/secvision/segmentation/ScribbleHistory;->getLast()Lcom/sec/android/secvision/segmentation/ScribblePath;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->setCurrentMarkerVector(Lcom/sec/android/secvision/segmentation/ScribblePath;)V

    .line 298
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mOutputBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-static {v2, v3}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->undoJNI(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 301
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 302
    const-string v2, "DMCSegmentationEngine"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "It has taken "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ms to undo"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    invoke-direct {p0}, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->makeSegmentContour()V

    .line 308
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentContour:Ljava/util/ArrayList;

    .line 311
    .end local v0    # "time":J
    :goto_0
    return-object v2

    .line 310
    :cond_0
    const-string v2, "DMCSegmentationEngine"

    const-string v3, "No more scribbles to undo"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    iget-object v2, p0, Lcom/sec/android/secvision/segmentation/DMCSegmentationEngine;->mSegmentContour:Ljava/util/ArrayList;

    goto :goto_0
.end method
