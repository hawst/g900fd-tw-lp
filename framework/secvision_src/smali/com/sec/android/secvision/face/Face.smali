.class public Lcom/sec/android/secvision/face/Face;
.super Ljava/lang/Object;
.source "Face.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/android/secvision/face/Face;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public facialFeature:[Landroid/graphics/PointF;

.field private pitch:F

.field public rect:Landroid/graphics/Rect;

.field private roll:F

.field private yaw:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 134
    new-instance v0, Lcom/sec/android/secvision/face/Face$1;

    invoke-direct {v0}, Lcom/sec/android/secvision/face/Face$1;-><init>()V

    sput-object v0, Lcom/sec/android/secvision/face/Face;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secvision/face/Face;->rect:Landroid/graphics/Rect;

    .line 34
    return-void
.end method

.method public constructor <init>(I)V
    .locals 4
    .param p1, "numFeatures"    # I

    .prologue
    const/4 v3, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/sec/android/secvision/face/Face;->rect:Landroid/graphics/Rect;

    .line 43
    new-array v1, p1, [Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/sec/android/secvision/face/Face;->facialFeature:[Landroid/graphics/PointF;

    .line 44
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 45
    iget-object v1, p0, Lcom/sec/android/secvision/face/Face;->facialFeature:[Landroid/graphics/PointF;

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    aput-object v2, v1, v0

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 47
    :cond_0
    iput v3, p0, Lcom/sec/android/secvision/face/Face;->pitch:F

    .line 48
    iput v3, p0, Lcom/sec/android/secvision/face/Face;->roll:F

    .line 49
    iput v3, p0, Lcom/sec/android/secvision/face/Face;->yaw:F

    .line 50
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1, p2, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/android/secvision/face/Face;->rect:Landroid/graphics/Rect;

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Rect;)V
    .locals 0
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/sec/android/secvision/face/Face;->rect:Landroid/graphics/Rect;

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    invoke-direct {p0, p1}, Lcom/sec/android/secvision/face/Face;->readFromParcel(Landroid/os/Parcel;)V

    .line 74
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 118
    const-class v2, Landroid/graphics/Rect;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    iput-object v2, p0, Lcom/sec/android/secvision/face/Face;->rect:Landroid/graphics/Rect;

    .line 119
    const-class v2, Landroid/graphics/PointF;

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v1

    .line 120
    .local v1, "temp":[Landroid/os/Parcelable;
    if-eqz v1, :cond_0

    .line 121
    array-length v2, v1

    new-array v2, v2, [Landroid/graphics/PointF;

    iput-object v2, p0, Lcom/sec/android/secvision/face/Face;->facialFeature:[Landroid/graphics/PointF;

    .line 122
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 123
    iget-object v3, p0, Lcom/sec/android/secvision/face/Face;->facialFeature:[Landroid/graphics/PointF;

    aget-object v2, v1, v0

    check-cast v2, Landroid/graphics/PointF;

    aput-object v2, v3, v0

    .line 122
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 126
    .end local v0    # "i":I
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    iput v2, p0, Lcom/sec/android/secvision/face/Face;->pitch:F

    .line 127
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    iput v2, p0, Lcom/sec/android/secvision/face/Face;->roll:F

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    iput v2, p0, Lcom/sec/android/secvision/face/Face;->yaw:F

    .line 129
    return-void
.end method

.method private setRectValue(IIII)V
    .locals 1
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/android/secvision/face/Face;->rect:Landroid/graphics/Rect;

    iput p1, v0, Landroid/graphics/Rect;->left:I

    .line 85
    iget-object v0, p0, Lcom/sec/android/secvision/face/Face;->rect:Landroid/graphics/Rect;

    iput p2, v0, Landroid/graphics/Rect;->top:I

    .line 86
    iget-object v0, p0, Lcom/sec/android/secvision/face/Face;->rect:Landroid/graphics/Rect;

    iput p3, v0, Landroid/graphics/Rect;->right:I

    .line 87
    iget-object v0, p0, Lcom/sec/android/secvision/face/Face;->rect:Landroid/graphics/Rect;

    iput p4, v0, Landroid/graphics/Rect;->bottom:I

    .line 88
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    return v0
.end method

.method public getPitch()F
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/sec/android/secvision/face/Face;->pitch:F

    return v0
.end method

.method public getRoll()F
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/secvision/face/Face;->roll:F

    return v0
.end method

.method public getYaw()F
    .locals 1

    .prologue
    .line 114
    iget v0, p0, Lcom/sec/android/secvision/face/Face;->yaw:F

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/android/secvision/face/Face;->rect:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 176
    iget-object v0, p0, Lcom/sec/android/secvision/face/Face;->facialFeature:[Landroid/graphics/PointF;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 177
    iget v0, p0, Lcom/sec/android/secvision/face/Face;->pitch:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 178
    iget v0, p0, Lcom/sec/android/secvision/face/Face;->roll:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 179
    iget v0, p0, Lcom/sec/android/secvision/face/Face;->yaw:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 180
    return-void
.end method
