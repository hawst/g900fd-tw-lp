.class public Lcom/sec/android/secvision/face/FaceDetector;
.super Ljava/lang/Object;
.source "FaceDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secvision/face/FaceDetector$FaceDetectorEngine;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final FACE_DETECTOR_ENGINE_DMC_MVFD:Ljava/lang/String; = "com.sec.android.secvision.face.MVFDEngine"

.field private static final TAG:Ljava/lang/String; = "FaceDetector"

.field private static mDetectorInstance:Lcom/sec/android/secvision/face/FaceDetector;


# instance fields
.field private mEngine:Lcom/sec/android/secvision/face/FaceDetector$FaceDetectorEngine;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/sec/android/secvision/face/FaceDetector;

    invoke-direct {v0}, Lcom/sec/android/secvision/face/FaceDetector;-><init>()V

    sput-object v0, Lcom/sec/android/secvision/face/FaceDetector;->mDetectorInstance:Lcom/sec/android/secvision/face/FaceDetector;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v2, "DMC"

    const-string v3, "DMC"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 36
    :try_start_0
    const-string v2, "com.sec.android.secvision.face.MVFDEngine"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 37
    .local v0, "clazz":Ljava/lang/Class;
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/secvision/face/FaceDetector$FaceDetectorEngine;

    iput-object v2, p0, Lcom/sec/android/secvision/face/FaceDetector;->mEngine:Lcom/sec/android/secvision/face/FaceDetector$FaceDetectorEngine;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 51
    .end local v0    # "clazz":Ljava/lang/Class;
    :goto_0
    return-void

    .line 38
    :catch_0
    move-exception v1

    .line 39
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    const-string v2, "FaceDetector"

    const-string v3, "Class <com.sec.android.secvision.face.MVFDEngine> cannot find"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 41
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v1

    .line 42
    .local v1, "e":Ljava/lang/InstantiationException;
    const-string v2, "FaceDetector"

    const-string v3, "Class <com.sec.android.secvision.face.MVFDEngine> made fail instance"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 44
    .end local v1    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v1

    .line 45
    .local v1, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "FaceDetector"

    const-string v3, "Class <com.sec.android.secvision.face.MVFDEngine> wrong access"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 49
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/secvision/face/FaceDetector;->mEngine:Lcom/sec/android/secvision/face/FaceDetector$FaceDetectorEngine;

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/secvision/face/FaceDetector;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/sec/android/secvision/face/FaceDetector;->mDetectorInstance:Lcom/sec/android/secvision/face/FaceDetector;

    return-object v0
.end method


# virtual methods
.method public findFaces(Landroid/graphics/Bitmap;Ljava/util/ArrayList;)I
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/secvision/face/Face;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 68
    .local p2, "face":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/face/Face;>;"
    iget-object v0, p0, Lcom/sec/android/secvision/face/FaceDetector;->mEngine:Lcom/sec/android/secvision/face/FaceDetector$FaceDetectorEngine;

    if-nez v0, :cond_0

    .line 69
    const-string v0, "FaceDetector"

    const-string v1, "???"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    const/4 v0, -0x1

    .line 72
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/secvision/face/FaceDetector;->mEngine:Lcom/sec/android/secvision/face/FaceDetector$FaceDetectorEngine;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/secvision/face/FaceDetector$FaceDetectorEngine;->findFaces(Landroid/graphics/Bitmap;Ljava/util/ArrayList;)I

    move-result v0

    goto :goto_0
.end method
