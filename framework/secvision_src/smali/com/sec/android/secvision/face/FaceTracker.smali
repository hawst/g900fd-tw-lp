.class public Lcom/sec/android/secvision/face/FaceTracker;
.super Ljava/lang/Object;
.source "FaceTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secvision/face/FaceTracker$FaceTrackerEngine;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final FACE_TRACKER_ENGINE_DMC_HEADPOSE:Ljava/lang/String; = "com.sec.android.secvision.face.HeadPoseEngine"

.field private static final TAG:Ljava/lang/String; = "FaceTracker"

.field private static mTrackerInstance:Lcom/sec/android/secvision/face/FaceTracker;


# instance fields
.field private mEngine:Lcom/sec/android/secvision/face/FaceTracker$FaceTrackerEngine;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/sec/android/secvision/face/FaceTracker;

    invoke-direct {v0}, Lcom/sec/android/secvision/face/FaceTracker;-><init>()V

    sput-object v0, Lcom/sec/android/secvision/face/FaceTracker;->mTrackerInstance:Lcom/sec/android/secvision/face/FaceTracker;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-string v2, "DMC_HS"

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    :try_start_0
    const-string v2, "com.sec.android.secvision.face.HeadPoseEngine"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 40
    .local v0, "clazz":Ljava/lang/Class;
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/secvision/face/FaceTracker$FaceTrackerEngine;

    iput-object v2, p0, Lcom/sec/android/secvision/face/FaceTracker;->mEngine:Lcom/sec/android/secvision/face/FaceTracker$FaceTrackerEngine;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 54
    .end local v0    # "clazz":Ljava/lang/Class;
    :goto_0
    return-void

    .line 41
    :catch_0
    move-exception v1

    .line 42
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    const-string v2, "FaceTracker"

    const-string v3, "Class <com.sec.android.secvision.face.HeadPoseEngine> cannot find"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 44
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v1

    .line 45
    .local v1, "e":Ljava/lang/InstantiationException;
    const-string v2, "FaceTracker"

    const-string v3, "Class <com.sec.android.secvision.face.HeadPoseEngine> made fail instance"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 47
    .end local v1    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v1

    .line 48
    .local v1, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "FaceTracker"

    const-string v3, "Class <com.sec.android.secvision.face.HeadPoseEngine> wrong access"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 52
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/secvision/face/FaceTracker;->mEngine:Lcom/sec/android/secvision/face/FaceTracker$FaceTrackerEngine;

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/android/secvision/face/FaceTracker;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/android/secvision/face/FaceTracker;->mTrackerInstance:Lcom/sec/android/secvision/face/FaceTracker;

    return-object v0
.end method


# virtual methods
.method public init(I)I
    .locals 2
    .param p1, "maxFace"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/android/secvision/face/FaceTracker;->mEngine:Lcom/sec/android/secvision/face/FaceTracker$FaceTrackerEngine;

    if-nez v0, :cond_0

    .line 71
    const-string v0, "FaceTracker"

    const-string v1, "Engine is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const/4 v0, -0x1

    .line 74
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/secvision/face/FaceTracker;->mEngine:Lcom/sec/android/secvision/face/FaceTracker$FaceTrackerEngine;

    invoke-interface {v0, p1}, Lcom/sec/android/secvision/face/FaceTracker$FaceTrackerEngine;->init(I)I

    move-result v0

    goto :goto_0
.end method

.method public process([BIIII[Lcom/sec/android/secvision/face/Face;)I
    .locals 7
    .param p1, "data"    # [B
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "orientation"    # I
    .param p5, "cam"    # I
    .param p6, "faces"    # [Lcom/sec/android/secvision/face/Face;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/secvision/face/FaceTracker;->mEngine:Lcom/sec/android/secvision/face/FaceTracker$FaceTrackerEngine;

    if-nez v0, :cond_0

    .line 101
    const-string v0, "FaceTracker"

    const-string v1, "Engine is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const/4 v0, -0x1

    .line 104
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/secvision/face/FaceTracker;->mEngine:Lcom/sec/android/secvision/face/FaceTracker$FaceTrackerEngine;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/sec/android/secvision/face/FaceTracker$FaceTrackerEngine;->process([BIIII[Lcom/sec/android/secvision/face/Face;)I

    move-result v0

    goto :goto_0
.end method

.method public release()I
    .locals 2

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/android/secvision/face/FaceTracker;->mEngine:Lcom/sec/android/secvision/face/FaceTracker$FaceTrackerEngine;

    if-nez v0, :cond_0

    .line 83
    const-string v0, "FaceTracker"

    const-string v1, "Engine is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    const/4 v0, -0x1

    .line 86
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/secvision/face/FaceTracker;->mEngine:Lcom/sec/android/secvision/face/FaceTracker$FaceTrackerEngine;

    invoke-interface {v0}, Lcom/sec/android/secvision/face/FaceTracker$FaceTrackerEngine;->release()I

    move-result v0

    goto :goto_0
.end method
