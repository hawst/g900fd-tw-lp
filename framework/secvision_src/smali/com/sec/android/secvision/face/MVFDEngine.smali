.class Lcom/sec/android/secvision/face/MVFDEngine;
.super Ljava/lang/Object;
.source "MVFDEngine.java"

# interfaces
.implements Lcom/sec/android/secvision/face/FaceDetector$FaceDetectorEngine;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secvision/face/MVFDEngine$FaceROI;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final TAG:Ljava/lang/String; = "MVFDEngine"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 17
    :try_start_0
    const-string v1, "DmcMVFD"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 18
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 19
    .restart local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "MVFDEngine"

    const-string v2, "load error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 20
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method private native FDInit()I
.end method

.method private native FDRelease()I
.end method

.method private native FDRun(Landroid/graphics/Bitmap;II)I
.end method

.method private native getFaceROI(I)Lcom/sec/android/secvision/face/MVFDEngine$FaceROI;
.end method


# virtual methods
.method public declared-synchronized findFaces(Landroid/graphics/Bitmap;Ljava/util/ArrayList;)I
    .locals 10
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/secvision/face/Face;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 37
    .local p2, "face":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/face/Face;>;"
    monitor-enter p0

    const/4 v1, 0x0

    .line 40
    .local v1, "facenum":I
    :try_start_0
    const-string v5, "MVFDEngine"

    const-string v6, "findFaces"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-direct {p0}, Lcom/sec/android/secvision/face/MVFDEngine;->FDInit()I

    .line 44
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-direct {p0, p1, v5, v6}, Lcom/sec/android/secvision/face/MVFDEngine;->FDRun(Landroid/graphics/Bitmap;II)I

    move-result v1

    .line 46
    const-string v5, "MVFDEngine"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "num of face :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    const/4 v3, 0x0

    .line 50
    .local v3, "maxRect":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 51
    invoke-direct {p0, v2}, Lcom/sec/android/secvision/face/MVFDEngine;->getFaceROI(I)Lcom/sec/android/secvision/face/MVFDEngine$FaceROI;

    move-result-object v4

    .line 52
    .local v4, "tempFace":Lcom/sec/android/secvision/face/MVFDEngine$FaceROI;
    iget v5, v4, Lcom/sec/android/secvision/face/MVFDEngine$FaceROI;->Right:I

    iget v6, v4, Lcom/sec/android/secvision/face/MVFDEngine$FaceROI;->Left:I

    sub-int v0, v5, v6

    .line 53
    .local v0, "faceWidth":I
    if-le v0, v3, :cond_0

    .line 54
    new-instance v5, Lcom/sec/android/secvision/face/Face;

    iget v6, v4, Lcom/sec/android/secvision/face/MVFDEngine$FaceROI;->Left:I

    iget v7, v4, Lcom/sec/android/secvision/face/MVFDEngine$FaceROI;->Top:I

    iget v8, v4, Lcom/sec/android/secvision/face/MVFDEngine$FaceROI;->Right:I

    iget v9, v4, Lcom/sec/android/secvision/face/MVFDEngine$FaceROI;->Bottom:I

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/sec/android/secvision/face/Face;-><init>(IIII)V

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 57
    .end local v0    # "faceWidth":I
    .end local v4    # "tempFace":Lcom/sec/android/secvision/face/MVFDEngine$FaceROI;
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/secvision/face/MVFDEngine;->FDRelease()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    monitor-exit p0

    return v1

    .line 37
    .end local v2    # "i":I
    .end local v3    # "maxRect":I
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method
