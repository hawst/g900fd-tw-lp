.class public Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;
.super Ljava/lang/Object;
.source "CVirtualTourNative.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative$OnVirtualTourEventListener;
    }
.end annotation


# static fields
.field private static mOnVirtualTourEventListener:Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative$OnVirtualTourEventListener;

.field private static final nativeInterface:Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    :try_start_0
    const-string v1, "VirtualTour"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    const/4 v1, 0x0

    sput-object v1, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->mOnVirtualTourEventListener:Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative$OnVirtualTourEventListener;

    .line 148
    new-instance v1, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;

    invoke-direct {v1}, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;-><init>()V

    sput-object v1, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->nativeInterface:Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;

    return-void

    .line 37
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 38
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    return-void
.end method

.method public static InverseJNICaller(I)V
    .locals 1
    .param p0, "nMessageID"    # I

    .prologue
    .line 160
    sget-object v0, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->mOnVirtualTourEventListener:Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative$OnVirtualTourEventListener;

    if-eqz v0, :cond_0

    .line 161
    sget-object v0, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->mOnVirtualTourEventListener:Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative$OnVirtualTourEventListener;

    invoke-interface {v0, p0}, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative$OnVirtualTourEventListener;->onStateChanged(I)V

    .line 163
    :cond_0
    return-void
.end method

.method public static native SNGetAccelerationLpfVector([F)I
.end method

.method public static native SNGetTemplate(I[F[I[I)V
.end method

.method public static native SNOnUndoFlagUpdate()V
.end method

.method public static native SNVTCaptureCallback([BIII)V
.end method

.method public static native SNVTCreatorClose()V
.end method

.method public static native SNVTCreatorOpen(IIIIIIFF)V
.end method

.method public static native SNVTCreatorReset(IIIIIIFF)V
.end method

.method public static native SNVTNV21ToARGB([I[BIIII)I
.end method

.method public static native SNVTOrientationSwitchUpdate(I)I
.end method

.method public static native SNVTPreviewCallback([BIII)V
.end method

.method public static SOnStepWalk_RemainingStepsUpdate(I)V
    .locals 1
    .param p0, "nRemainingStepCount"    # I

    .prologue
    .line 166
    sget-object v0, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->mOnVirtualTourEventListener:Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative$OnVirtualTourEventListener;

    if-eqz v0, :cond_0

    .line 167
    sget-object v0, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->mOnVirtualTourEventListener:Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative$OnVirtualTourEventListener;

    invoke-interface {v0, p0}, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative$OnVirtualTourEventListener;->SOnStepWalk_RemainingStepsUpdate(I)V

    .line 169
    :cond_0
    return-void
.end method

.method public static getInstance()Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->nativeInterface:Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;

    return-object v0
.end method


# virtual methods
.method public setStackStateListener(Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative$OnVirtualTourEventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative$OnVirtualTourEventListener;

    .prologue
    .line 151
    sput-object p1, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->mOnVirtualTourEventListener:Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative$OnVirtualTourEventListener;

    .line 152
    return-void
.end method
