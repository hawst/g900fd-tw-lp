.class Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$3;
.super Ljava/lang/Object;
.source "VirtualTourModeler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->startCalibrationThread()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;


# direct methods
.method constructor <init>(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)V
    .locals 0

    .prologue
    .line 621
    iput-object p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$3;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 624
    const/4 v1, 0x2

    .line 625
    .local v1, "orientation":I
    const/4 v5, 0x3

    new-array v3, v5, [D

    fill-array-data v3, :array_0

    .line 627
    .local v3, "pf64Angle":[D
    :goto_0
    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$3;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mRunning:Z
    invoke-static {v5}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$2000(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 632
    const/4 v5, 0x3

    :try_start_0
    new-array v2, v5, [F

    fill-array-data v2, :array_1

    .line 633
    .local v2, "pf32AccVec3":[F
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->SNGetAccelerationLpfVector([F)I

    move-result v4

    .line 635
    .local v4, "ret":I
    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 636
    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$3;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mOrientation:I
    invoke-static {v5}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$2100(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 646
    :goto_1
    const/4 v5, 0x2

    if-ne v1, v5, :cond_1

    .line 647
    const/4 v5, 0x1

    const/4 v6, 0x0

    aget v6, v2, v6

    float-to-double v6, v6

    const/4 v8, 0x2

    aget v8, v2, v8

    float-to-double v8, v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    neg-double v6, v6

    const-wide v8, 0x3ff921fb54442d18L    # 1.5707963267948966

    add-double/2addr v6, v8

    aput-wide v6, v3, v5

    .line 648
    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$3;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    const/4 v6, 0x1

    aget-wide v6, v3, v6

    # invokes: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->updateHorizon(D)V
    invoke-static {v5, v6, v7}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$2200(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;D)V

    .line 654
    :cond_0
    :goto_2
    const-wide/16 v6, 0x32

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 658
    .end local v2    # "pf32AccVec3":[F
    .end local v4    # "ret":I
    :catch_0
    move-exception v0

    .line 659
    .local v0, "e":Ljava/lang/Exception;
    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$3;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    const/4 v6, 0x0

    # setter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mRunning:Z
    invoke-static {v5, v6}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$2002(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;Z)Z

    goto :goto_0

    .line 639
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "pf32AccVec3":[F
    .restart local v4    # "ret":I
    :pswitch_0
    const/4 v1, 0x2

    .line 640
    goto :goto_1

    .line 643
    :pswitch_1
    const/4 v1, 0x1

    goto :goto_1

    .line 649
    :cond_1
    const/4 v5, 0x1

    if-ne v1, v5, :cond_0

    .line 650
    const/4 v5, 0x0

    const/4 v6, 0x1

    :try_start_1
    aget v6, v2, v6

    float-to-double v6, v6

    const/4 v8, 0x2

    aget v8, v2, v8

    float-to-double v8, v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    neg-double v6, v6

    const-wide v8, 0x3ff921fb54442d18L    # 1.5707963267948966

    add-double/2addr v6, v8

    aput-wide v6, v3, v5

    .line 651
    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$3;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    const/4 v6, 0x0

    aget-wide v6, v3, v6

    # invokes: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->updateHorizon(D)V
    invoke-static {v5, v6, v7}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$2200(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;D)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 662
    .end local v2    # "pf32AccVec3":[F
    .end local v4    # "ret":I
    :cond_2
    return-void

    .line 625
    nop

    :array_0
    .array-data 8
        0x0
        0x0
        0x0
    .end array-data

    .line 632
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    .line 636
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
