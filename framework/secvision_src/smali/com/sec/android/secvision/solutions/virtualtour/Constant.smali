.class public Lcom/sec/android/secvision/solutions/virtualtour/Constant;
.super Ljava/lang/Object;
.source "Constant.java"


# static fields
.field public static final B_USE_ACC_NATIVE:Z = true

.field public static B_USE_ROT_VECTOR_PROCESSING:Z = false

.field public static final EG_VT_ORIENTATION_LANDSCAPE:I = 0x2

.field public static final EG_VT_ORIENTATION_PORTRAIT:I = 0x1

.field public static final EG_VT_ORIENTATION_REVERSE_LANDSCAPE:I = 0x4

.field public static final EG_VT_ORIENTATION_REVERSE_PORTRAIT:I = 0x3

.field public static final EG_VT_WALK_INDICATOR:F = 1000.0f

.field public static EXPORT_PATH:Ljava/lang/String; = null

.field public static final IMAGE_ALIGNMENT_THREAD:Z = true

.field public static final MINI_MAP_SIZE:I = 0x12c

.field public static MODELLER_IMAGE_ALIGNMENT_MODULE:Z = false

.field public static final NODE_TYPE_NORMAL:I = 0x0

.field public static final NODE_TYPE_TURN:I = 0x1

.field public static final PATH_LENTH:I = 0x32

.field public static final SELECTED_SWEEP_KEY:Ljava/lang/String; = "Sweep_Selected"

.field public static final SWEEP_MAX_PHOTO:I = 0x1e

.field public static final THUMBNAIL_MAX_PHOTO:I = 0x6

.field public static final TRANSTION_TYPE_FORWARD_WALK:I = 0x9

.field public static final TRANSTION_TYPE_FORWARD_WALK_PLUS:I = 0xa

.field public static final TRANSTION_TYPE_LATERAL_WALK_LEFT:I = 0xd

.field public static final TRANSTION_TYPE_LATERAL_WALK_PLUS_LEFT:I = 0xb

.field public static final TRANSTION_TYPE_LATERAL_WALK_PLUS_RIGHT:I = 0xc

.field public static final TRANSTION_TYPE_LATERAL_WALK_RIGHT:I = 0xe

.field public static final TRANSTION_TYPE_NONE:I = 0x0

.field public static final TRANSTION_TYPE_STAIRS_DOWN:I = 0xf

.field public static final TRANSTION_TYPE_STAIRS_DOWN_PLUS:I = 0x10

.field public static final TRANSTION_TYPE_STAIRS_UP:I = 0x11

.field public static final TRANSTION_TYPE_STAIRS_UP_PLUS:I = 0x12

.field public static final TRANSTION_TYPE_STEP_DOWN:I = 0x5

.field public static final TRANSTION_TYPE_STEP_DOWN_PLUS:I = 0x6

.field public static final TRANSTION_TYPE_STEP_UP:I = 0x7

.field public static final TRANSTION_TYPE_STEP_UP_PLUS:I = 0x8

.field public static final TRANSTION_TYPE_TURN_LEFT:I = 0x1

.field public static final TRANSTION_TYPE_TURN_RIGHT:I = 0x2

.field public static final TRANSTION_TYPE_WALK_AND_TURN_90_LEFT:I = 0x3

.field public static final TRANSTION_TYPE_WALK_AND_TURN_90_RIGHT:I = 0x4

.field public static final TYPE_AGIF:I = 0x2

.field public static final TYPE_JPG:I = 0x0

.field public static final TYPE_PNG:I = 0x1

.field public static final VIRTUAL_TOUR_INFO:I = 0x850

.field public static final m_fImageAspect:F = 1.7777778f


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/secvision/solutions/virtualtour/Constant;->MODELLER_IMAGE_ALIGNMENT_MODULE:Z

    .line 30
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/secvision/solutions/virtualtour/Constant;->B_USE_ROT_VECTOR_PROCESSING:Z

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Sweeps/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/secvision/solutions/virtualtour/Constant;->EXPORT_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
