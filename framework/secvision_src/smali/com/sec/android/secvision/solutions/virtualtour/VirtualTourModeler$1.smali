.class Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;
.super Ljava/lang/Object;
.source "VirtualTourModeler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;


# direct methods
.method constructor <init>(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x6

    const/4 v6, 0x0

    .line 259
    :goto_0
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadRunning:Z
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$000(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 260
    const/4 v1, 0x0

    .line 262
    .local v1, "ret":I
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadState:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$100(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v8, :cond_1

    .line 263
    const-string v2, "3DAlign"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Killing mBackGroundThreadState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadState:Ljava/lang/Integer;
    invoke-static {v4}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$100(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # setter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadRunning:Z
    invoke-static {v2, v6}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$002(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;Z)Z

    .line 304
    .end local v1    # "ret":I
    :cond_0
    :goto_1
    const-string v2, "3DAlign"

    const-string v3, "Working thread exited. Loop broken"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # setter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I
    invoke-static {v2, v6}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$202(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;I)I

    .line 306
    return-void

    .line 266
    .restart local v1    # "ret":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadState:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$100(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v7, :cond_3

    .line 267
    const-string v2, "3DAlign"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FINISH: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I
    invoke-static {v4}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$200(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Size:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;
    invoke-static {v4}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$300(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :goto_2
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$200(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;
    invoke-static {v3}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$300(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_2

    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadState:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$100(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v7, :cond_2

    .line 270
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # invokes: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->checkUndoforIA()V
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$400(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)V

    .line 271
    const-string v2, "3DAlign"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "m_nIAProgressCount "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I
    invoke-static {v4}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$200(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I
    invoke-static {v3}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$200(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    move-result v3

    # invokes: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->imageAlignment(I)I
    invoke-static {v2, v3}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$500(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;I)I

    move-result v1

    .line 273
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # operator++ for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$208(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    goto :goto_2

    .line 275
    :cond_2
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # setter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadRunning:Z
    invoke-static {v2, v6}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$002(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;Z)Z

    goto/16 :goto_1

    .line 277
    :cond_3
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadState:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$100(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 279
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # invokes: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->checkUndoforIA()V
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$400(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)V

    .line 280
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$200(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;
    invoke-static {v3}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$300(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/util/Vector;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_4

    .line 281
    const-string v2, "3DAlign"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RUN: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I
    invoke-static {v4}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$200(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "Size:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;
    invoke-static {v4}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$300(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I
    invoke-static {v3}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$200(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    move-result v3

    # invokes: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->imageAlignment(I)I
    invoke-static {v2, v3}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$500(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;I)I

    move-result v1

    .line 283
    const-string v2, "3DAlign"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "IA ret = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # operator++ for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$208(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    .line 287
    :cond_4
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$600(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Runnable;

    move-result-object v3

    monitor-enter v3

    .line 289
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadState:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$100(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v8, :cond_5

    .line 291
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    const/4 v4, 0x0

    # setter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadRunning:Z
    invoke-static {v2, v4}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$002(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;Z)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 292
    :try_start_1
    monitor-exit v3

    goto/16 :goto_1

    .line 302
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 294
    :cond_5
    :try_start_2
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadState:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$100(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    if-ne v2, v7, :cond_6

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 296
    :cond_6
    :try_start_4
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$200(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    move-result v2

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;
    invoke-static {v4}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$300(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/util/Vector;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-lt v2, v4, :cond_7

    .line 297
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$600(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 302
    :cond_7
    :goto_3
    :try_start_5
    monitor-exit v3

    goto/16 :goto_0

    .line 299
    :catch_0
    move-exception v0

    .line 300
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3
.end method
