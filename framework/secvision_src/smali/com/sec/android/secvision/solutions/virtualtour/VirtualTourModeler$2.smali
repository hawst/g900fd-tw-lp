.class Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;
.super Ljava/lang/Object;
.source "VirtualTourModeler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->saveTour(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

.field final synthetic val$DestinationDirectory:Ljava/lang/String;

.field final synthetic val$TemporaryStorageDirectory:Ljava/lang/String;

.field final synthetic val$dateTaken:J

.field final synthetic val$fileName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    iput-object p2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->val$DestinationDirectory:Ljava/lang/String;

    iput-object p3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->val$fileName:Ljava/lang/String;

    iput-wide p4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->val$dateTaken:J

    iput-object p6, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->val$TemporaryStorageDirectory:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 413
    const-string v0, "3DAlign"

    const-string v1, "saveTour Run"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 417
    .local v10, "SavingStartTime":J
    :goto_0
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCapturing:Z
    invoke-static {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$700(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 419
    const-wide/16 v0, 0x32

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 425
    :goto_1
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAMutexObject_State:Ljava/lang/Object;
    invoke-static {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$800(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    .line 426
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadState:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$100(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABackGroundThread:Ljava/lang/Thread;
    invoke-static {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$900(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Thread;

    move-result-object v0

    if-nez v0, :cond_0

    .line 428
    const-string v0, "VirtualTourModeler"

    const-string v1, "Breaking wait for mCapturing on Interrupt->Kill & Join of IA Thread"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->val$DestinationDirectory:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->val$fileName:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->val$dateTaken:J

    # invokes: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->postSEFEncode(ILjava/lang/String;Ljava/lang/String;J)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1000(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;ILjava/lang/String;Ljava/lang/String;J)V

    .line 430
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 454
    :goto_2
    return-void

    .line 420
    :catch_0
    move-exception v12

    .line 421
    .local v12, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v12}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 432
    .end local v12    # "e":Ljava/lang/InterruptedException;
    :cond_0
    :try_start_2
    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 435
    :cond_1
    const-string v0, "3DAlign"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Total Photos:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1100(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    iget-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->val$TemporaryStorageDirectory:Ljava/lang/String;

    # setter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTemporaryStorageDirectory:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1202(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;Ljava/lang/String;)Ljava/lang/String;

    .line 438
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    iget-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->val$DestinationDirectory:Ljava/lang/String;

    # setter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mDestinationDirectory:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1302(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;Ljava/lang/String;)Ljava/lang/String;

    .line 439
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    const/4 v1, 0x6

    # invokes: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->stopImageAlignmentThread(I)V
    invoke-static {v0, v1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1400(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;I)V

    .line 442
    const-string v0, "3DAlign"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "encodeSEF count = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1100(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    iget-object v13, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVirtualTourDataManager:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;

    iget-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mDestinationDirectory:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1300(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTemporaryStorageDirectory:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1200(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I
    invoke-static {v3}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1100(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsType:[I
    invoke-static {v4}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1600(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)[I

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsData:[F
    invoke-static {v5}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1700(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)[F

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIsTranslationValid:[I
    invoke-static {v6}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1800(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)[I

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTranslations:[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;
    invoke-static {v7}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1900(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->encodeSEF(Ljava/lang/String;Ljava/lang/String;I[I[F[I[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)I

    move-result v0

    # setter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mEncodeResult:I
    invoke-static {v13, v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1502(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;I)I

    .line 447
    const-string v0, "3DAlign"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SEF Encoding done result = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mEncodeResult:I
    invoke-static {v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1500(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I
    invoke-static {v0, v1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1102(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;I)I

    .line 450
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 451
    .local v8, "SavingEndTime":J
    const-string v0, "3DAlign"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Saving Time = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sub-long v2, v8, v10

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    iget-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->this$0:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mEncodeResult:I
    invoke-static {v1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1500(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->val$DestinationDirectory:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->val$fileName:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;->val$dateTaken:J

    # invokes: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->postSEFEncode(ILjava/lang/String;Ljava/lang/String;J)V
    invoke-static/range {v0 .. v5}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->access$1000(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;ILjava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_2
.end method
