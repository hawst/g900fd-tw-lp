.class public Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;
.super Ljava/lang/Object;
.source "CDirectionEstimator.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private m_s32ScreenOreintation:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-string v0, "3DTour"

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;->TAG:Ljava/lang/String;

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;->m_s32ScreenOreintation:I

    return-void
.end method


# virtual methods
.method public Close()V
    .locals 0

    .prologue
    .line 46
    invoke-static {}, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->SNVTCreatorClose()V

    .line 47
    return-void
.end method

.method public declared-synchronized GetTemplate(I[F[I)I
    .locals 8
    .param p1, "PhotoIndex"    # I
    .param p2, "pf32YawDeltaDegree"    # [F
    .param p3, "ps32Direction"    # [I

    .prologue
    const/4 v7, -0x1

    .line 99
    monitor-enter p0

    const/4 v1, 0x0

    .line 100
    .local v1, "ret":I
    const/4 v2, 0x1

    :try_start_0
    new-array v0, v2, [I

    const/4 v2, 0x0

    const/4 v3, -0x1

    aput v3, v0, v2

    .line 102
    .local v0, "ps32TemplateID":[I
    invoke-static {p1, p2, p3, v0}, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->SNGetTemplate(I[F[I[I)V

    .line 104
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;->TAG:Ljava/lang/String;

    const-string v3, "DELTA DEGREES: %f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    aget v6, p2, v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;->TAG:Ljava/lang/String;

    const-string v3, "DIRECTION: %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    aget v6, p3, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    if-eqz p1, :cond_0

    const/4 v2, 0x0

    aget v2, v0, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v2, v7, :cond_0

    .line 107
    const/4 v1, 0x1

    .line 109
    :cond_0
    monitor-exit p0

    return v1

    .line 99
    .end local v0    # "ps32TemplateID":[I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public Open(IIIII)V
    .locals 8
    .param p1, "PictureWidth"    # I
    .param p2, "PictureHeight"    # I
    .param p3, "PreviewWidth"    # I
    .param p4, "PreviewHeight"    # I
    .param p5, "ScreenOreintation"    # I

    .prologue
    .line 61
    iput p5, p0, Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;->m_s32ScreenOreintation:I

    .line 62
    const/16 v5, 0x1e

    sget v6, Lcom/sec/android/secvision/solutions/virtualtour/Settings;->cameraHorizontalViewAngle:F

    sget v7, Lcom/sec/android/secvision/solutions/virtualtour/Settings;->cameraVerticalViewAngle:F

    move v0, p1

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-static/range {v0 .. v7}, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->SNVTCreatorOpen(IIIIIIFF)V

    .line 64
    return-void
.end method

.method public ReStart(IIIII)V
    .locals 0
    .param p1, "PictureWidth"    # I
    .param p2, "PictureHeight"    # I
    .param p3, "PreviewWidth"    # I
    .param p4, "PreviewHeight"    # I
    .param p5, "ScreeOreintation"    # I

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;->Close()V

    .line 83
    invoke-virtual/range {p0 .. p5}, Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;->Open(IIIII)V

    .line 84
    return-void
.end method

.method public Reset(IIIII)V
    .locals 8
    .param p1, "PictureWidth"    # I
    .param p2, "PictureHeight"    # I
    .param p3, "PreviewWidth"    # I
    .param p4, "PreviewHeight"    # I
    .param p5, "ScreenOreintation"    # I

    .prologue
    .line 68
    iput p5, p0, Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;->m_s32ScreenOreintation:I

    .line 69
    const/16 v5, 0x1e

    sget v6, Lcom/sec/android/secvision/solutions/virtualtour/Settings;->cameraHorizontalViewAngle:F

    sget v7, Lcom/sec/android/secvision/solutions/virtualtour/Settings;->cameraVerticalViewAngle:F

    move v0, p1

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-static/range {v0 .. v7}, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->SNVTCreatorReset(IIIIIIFF)V

    .line 70
    return-void
.end method
