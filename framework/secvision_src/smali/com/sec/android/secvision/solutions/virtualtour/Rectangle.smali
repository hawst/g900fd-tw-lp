.class public Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;
.super Ljava/lang/Object;
.source "Rectangle.java"


# instance fields
.field public height:F

.field public width:F

.field public x:F

.field public y:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0, v0, v0, v0, v0}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;-><init>(FFFF)V

    .line 15
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "fx"    # F
    .param p2, "fy"    # F
    .param p3, "fwidth"    # F
    .param p4, "fheight"    # F

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    .line 19
    iput p2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    .line 20
    iput p3, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    .line 21
    iput p4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    .line 22
    return-void
.end method


# virtual methods
.method public clone()Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;
    .locals 5

    .prologue
    .line 25
    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;-><init>(FFFF)V

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 7
    invoke-virtual {p0}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->clone()Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    move-result-object v0

    return-object v0
.end method

.method public containsPoint(Lcom/sec/android/secvision/solutions/virtualtour/Point;)Z
    .locals 3
    .param p1, "pt"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .prologue
    .line 29
    iget v0, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    add-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    add-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public extend(Lcom/sec/android/secvision/solutions/virtualtour/Point;)V
    .locals 6
    .param p1, "oPoint"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .prologue
    .line 43
    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    .line 44
    .local v1, "l":F
    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    iget v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    add-float v2, v4, v5

    .line 45
    .local v2, "r":F
    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    .line 46
    .local v3, "t":F
    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    iget v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    add-float v0, v4, v5

    .line 48
    .local v0, "b":F
    iget v4, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    cmpg-float v4, v4, v1

    if-gez v4, :cond_0

    .line 49
    iget v4, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    iput v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    .line 50
    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    sub-float v4, v2, v4

    iput v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    .line 52
    :cond_0
    iget v4, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    cmpg-float v4, v4, v3

    if-gez v4, :cond_1

    .line 53
    iget v4, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iput v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    .line 54
    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    sub-float v4, v0, v4

    iput v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    .line 56
    :cond_1
    iget v4, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    cmpl-float v4, v4, v2

    if-lez v4, :cond_2

    .line 57
    iget v4, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    iget v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    sub-float/2addr v4, v5

    iput v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    .line 58
    :cond_2
    iget v4, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    cmpl-float v4, v4, v0

    if-lez v4, :cond_3

    .line 59
    iget v4, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    sub-float/2addr v4, v5

    iput v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    .line 60
    :cond_3
    return-void
.end method

.method public getBottom()F
    .locals 2

    .prologue
    .line 102
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    add-float/2addr v0, v1

    return v0
.end method

.method public getBottomRight()Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .locals 4

    .prologue
    .line 120
    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    add-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    return-object v0
.end method

.method public getCenter()Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 124
    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    div-float/2addr v2, v4

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    return-object v0
.end method

.method public getLeft()F
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    return v0
.end method

.method public getRight()F
    .locals 2

    .prologue
    .line 86
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    add-float/2addr v0, v1

    return v0
.end method

.method public getTop()F
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    return v0
.end method

.method public getTopLeft()Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .locals 3

    .prologue
    .line 111
    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    return-object v0
.end method

.method public inflate(FF)V
    .locals 3
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 63
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    sub-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    .line 64
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    sub-float/2addr v0, p2

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    .line 65
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    mul-float v1, v2, p1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    .line 66
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    mul-float v1, v2, p2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    .line 67
    return-void
.end method

.method public makePoint(Lcom/sec/android/secvision/solutions/virtualtour/Point;)V
    .locals 0
    .param p1, "oPoint"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->setTopLeft(Lcom/sec/android/secvision/solutions/virtualtour/Point;)V

    .line 39
    invoke-virtual {p0, p1}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->setBottomRight(Lcom/sec/android/secvision/solutions/virtualtour/Point;)V

    .line 40
    return-void
.end method

.method public setBottom(F)V
    .locals 1
    .param p1, "b"    # F

    .prologue
    .line 98
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    sub-float v0, p1, v0

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    .line 99
    return-void
.end method

.method public setBottomRight(Lcom/sec/android/secvision/solutions/virtualtour/Point;)V
    .locals 2
    .param p1, "br"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .prologue
    .line 115
    iget v0, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    .line 116
    iget v0, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    .line 117
    return-void
.end method

.method public setLeft(F)V
    .locals 0
    .param p1, "l"    # F

    .prologue
    .line 74
    iput p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    .line 75
    return-void
.end method

.method public setRight(F)V
    .locals 1
    .param p1, "r"    # F

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    sub-float v0, p1, v0

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    .line 83
    return-void
.end method

.method public setTop(F)V
    .locals 0
    .param p1, "t"    # F

    .prologue
    .line 90
    iput p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    .line 91
    return-void
.end method

.method public setTopLeft(Lcom/sec/android/secvision/solutions/virtualtour/Point;)V
    .locals 1
    .param p1, "tl"    # Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .prologue
    .line 106
    iget v0, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    .line 107
    iget v0, p1, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    .line 108
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "(x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->height:F

    invoke-static {v1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public union(Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;)Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;
    .locals 6
    .param p1, "rc"    # Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    .prologue
    .line 33
    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    iget v2, p1, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->x:F

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    iget v3, p1, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->y:F

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-virtual {p0}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->getRight()F

    move-result v3

    invoke-virtual {p1}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->getRight()F

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v3

    invoke-virtual {p0}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->getBottom()F

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->getBottom()F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;-><init>(FFFF)V

    return-object v0
.end method
