.class public interface abstract Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;
.super Ljava/lang/Object;
.source "VirtualTourModeler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "VirtualTourModelerEventListener"
.end annotation


# virtual methods
.method public abstract horizonIndicatorUpdate(FF)V
.end method

.method public abstract onOrientationChangedEngine(I)V
.end method

.method public abstract onStateChanged(II)V
.end method

.method public abstract postSEFEncode(ILjava/lang/String;Ljava/lang/String;J)V
.end method
