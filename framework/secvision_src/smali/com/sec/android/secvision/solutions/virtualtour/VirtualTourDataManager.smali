.class public Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;
.super Ljava/lang/Object;
.source "VirtualTourDataManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "VirtualTourDataManager"

.field private static final TRANSITION_TEMPLATE_PATH:Ljava/lang/String; = "/system/cameradata/secvision/virtualtour/TransitionTemplates.ttf"

.field private static final TRANSITION_TYPE_FORWARD_WALK:I = 0x9

.field private static final TRANSITION_TYPE_FORWARD_WALK_PLUS:I = 0xa

.field private static final TRANSITION_TYPE_LATERAL_WALK_LEFT:I = 0xd

.field private static final TRANSITION_TYPE_LATERAL_WALK_PLUS_LEFT:I = 0xb

.field private static final TRANSITION_TYPE_LATERAL_WALK_PLUS_RIGHT:I = 0xc

.field private static final TRANSITION_TYPE_LATERAL_WALK_RIGHT:I = 0xe

.field private static final TRANSITION_TYPE_NONE:I = 0x0

.field private static final TRANSITION_TYPE_STAIRS_DOWN:I = 0xf

.field private static final TRANSITION_TYPE_STAIRS_DOWN_PLUS:I = 0x10

.field private static final TRANSITION_TYPE_STAIRS_UP:I = 0x11

.field private static final TRANSITION_TYPE_STAIRS_UP_PLUS:I = 0x12

.field private static final TRANSITION_TYPE_STEP_DOWN:I = 0x5

.field private static final TRANSITION_TYPE_STEP_DOWN_PLUS:I = 0x6

.field private static final TRANSITION_TYPE_STEP_UP:I = 0x7

.field private static final TRANSITION_TYPE_STEP_UP_PLUS:I = 0x8

.field private static final TRANSITION_TYPE_TURN_LEFT:I = 0x1

.field private static final TRANSITION_TYPE_TURN_RIGHT:I = 0x2

.field private static final TRANSITION_TYPE_WALK_AND_TURN_90_LEFT:I = 0x3

.field private static final TRANSITION_TYPE_WALK_AND_TURN_90_RIGHT:I = 0x4

.field private static final VIRTUALTOUR_NAME_PREFIX:Ljava/lang/String; = "3DTour_"


# instance fields
.field public mTransitionAngleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private m_aAngles:[I

.field private m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    .line 56
    iput-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_aAngles:[I

    .line 57
    iput-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->mTransitionAngleList:Ljava/util/ArrayList;

    .line 63
    :try_start_0
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v2, Ljava/io/FileInputStream;

    const-string v3, "/system/cameradata/secvision/virtualtour/TransitionTemplates.ttf"

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 64
    .local v1, "is":Ljava/io/InputStream;
    invoke-static {v1}, Lcom/voovio/sweep/TemplateManager;->createTemplateManager(Ljava/io/InputStream;)Lcom/voovio/sweep/TemplateManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    .line 65
    if-eqz v1, :cond_0

    .line 66
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    :cond_0
    const/4 v2, 0x4

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    iput-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_aAngles:[I

    .line 74
    .end local v1    # "is":Ljava/io/InputStream;
    :goto_0
    return-void

    .line 68
    :catch_0
    move-exception v0

    .line 69
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 73
    nop

    :array_0
    .array-data 4
        0xa
        0x1e
        0x2d
        0x5a
    .end array-data
.end method

.method private copyFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "sourceFilePath"    # Ljava/lang/String;
    .param p2, "targetFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 597
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 598
    .local v3, "srcDataFile":Ljava/io/File;
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 600
    .local v2, "source":Ljava/io/FileInputStream;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 601
    .local v5, "targetDataFile":Ljava/io/File;
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 604
    .local v4, "target":Ljava/io/FileOutputStream;
    const/16 v6, 0x400

    :try_start_0
    new-array v0, v6, [B

    .line 605
    .local v0, "buffer":[B
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v1

    .local v1, "len":I
    :goto_0
    if-lez v1, :cond_0

    .line 606
    const/4 v6, 0x0

    invoke-virtual {v4, v0, v6, v1}, Ljava/io/FileOutputStream;->write([BII)V

    .line 605
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    goto :goto_0

    .line 609
    :cond_0
    if-eqz v2, :cond_1

    .line 610
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 612
    :cond_1
    if-eqz v4, :cond_2

    .line 613
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 616
    :cond_2
    return-void

    .line 609
    .end local v0    # "buffer":[B
    .end local v1    # "len":I
    :catchall_0
    move-exception v6

    if-eqz v2, :cond_3

    .line 610
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 612
    :cond_3
    if-eqz v4, :cond_4

    .line 613
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    :cond_4
    throw v6
.end method

.method private deleteFile(Ljava/lang/String;)V
    .locals 2
    .param p1, "strFile"    # Ljava/lang/String;

    .prologue
    .line 581
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 583
    .local v0, "oFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 584
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 586
    :cond_0
    return-void
.end method

.method private deleteFolder(Ljava/lang/String;)V
    .locals 2
    .param p1, "dir"    # Ljava/lang/String;

    .prologue
    .line 589
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 591
    .local v0, "oFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 592
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 594
    :cond_0
    return-void
.end method

.method private getAngle(Lcom/voovio/sweep/Template;)F
    .locals 4
    .param p1, "oTemplate"    # Lcom/voovio/sweep/Template;

    .prologue
    .line 262
    invoke-virtual {p1}, Lcom/voovio/sweep/Template;->getId()I

    move-result v1

    .line 264
    .local v1, "nId":I
    const/4 v0, 0x0

    .line 265
    .local v0, "fAngle":F
    packed-switch v1, :pswitch_data_0

    .line 296
    :goto_0
    :pswitch_0
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v2, v2

    return v2

    .line 267
    :pswitch_1
    const/high16 v0, -0x3ee00000    # -10.0f

    .line 268
    goto :goto_0

    .line 270
    :pswitch_2
    const/high16 v0, 0x41200000    # 10.0f

    .line 271
    goto :goto_0

    .line 273
    :pswitch_3
    const/high16 v0, -0x3e100000    # -30.0f

    .line 274
    goto :goto_0

    .line 276
    :pswitch_4
    const/high16 v0, 0x41f00000    # 30.0f

    .line 277
    goto :goto_0

    .line 279
    :pswitch_5
    const/high16 v0, -0x3dcc0000    # -45.0f

    .line 280
    goto :goto_0

    .line 282
    :pswitch_6
    const/high16 v0, 0x42340000    # 45.0f

    .line 283
    goto :goto_0

    .line 286
    :pswitch_7
    const/high16 v0, -0x3d4c0000    # -90.0f

    .line 287
    goto :goto_0

    .line 290
    :pswitch_8
    const/high16 v0, 0x42b40000    # 90.0f

    .line 291
    goto :goto_0

    .line 265
    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private getTemplate(IF)Lcom/voovio/sweep/Template;
    .locals 7
    .param p1, "nTransitionType"    # I
    .param p2, "fAngle"    # F

    .prologue
    .line 83
    const/4 v3, 0x0

    .line 85
    .local v3, "oTemplate":Lcom/voovio/sweep/Template;
    float-to-double v4, p2

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v4, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result p2

    .line 87
    const/4 v2, 0x0

    .line 88
    .local v2, "nAngle":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_aAngles:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_0

    .line 89
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_aAngles:[I

    aget v4, v4, v1

    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_aAngles:[I

    add-int/lit8 v6, v1, 0x1

    aget v5, v5, v6

    add-int/2addr v4, v5

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float v0, v4, v5

    .line 90
    .local v0, "fMiddle":F
    cmpg-float v4, p2, v0

    if-gtz v4, :cond_1

    .line 91
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_aAngles:[I

    aget v2, v4, v1

    .line 97
    .end local v0    # "fMiddle":F
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 186
    :goto_1
    :pswitch_0
    return-object v3

    .line 94
    .restart local v0    # "fMiddle":F
    :cond_1
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_aAngles:[I

    add-int/lit8 v5, v1, 0x1

    aget v2, v4, v5

    .line 88
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 103
    .end local v0    # "fMiddle":F
    :pswitch_1
    sparse-switch v2, :sswitch_data_0

    goto :goto_1

    .line 105
    :sswitch_0
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0xc

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 106
    goto :goto_1

    .line 108
    :sswitch_1
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0xe

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 109
    goto :goto_1

    .line 111
    :sswitch_2
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0x12

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 112
    goto :goto_1

    .line 114
    :sswitch_3
    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    invoke-virtual {v4}, Lcom/voovio/sweep/TemplateManager;->getImageAspect()Ljava/lang/String;

    move-result-object v4

    const-string v6, "horizontal"

    if-ne v4, v6, :cond_2

    const/16 v4, 0x14

    :goto_2
    invoke-virtual {v5, v4}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 115
    goto :goto_1

    .line 114
    :cond_2
    const/16 v4, 0x16

    goto :goto_2

    .line 122
    :pswitch_2
    sparse-switch v2, :sswitch_data_1

    goto :goto_1

    .line 124
    :sswitch_4
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0xd

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 125
    goto :goto_1

    .line 127
    :sswitch_5
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0xf

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 128
    goto :goto_1

    .line 130
    :sswitch_6
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0x13

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 131
    goto :goto_1

    .line 133
    :sswitch_7
    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    invoke-virtual {v4}, Lcom/voovio/sweep/TemplateManager;->getImageAspect()Ljava/lang/String;

    move-result-object v4

    const-string v6, "horizontal"

    if-ne v4, v6, :cond_3

    const/16 v4, 0x15

    :goto_3
    invoke-virtual {v5, v4}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 134
    goto :goto_1

    .line 133
    :cond_3
    const/16 v4, 0x17

    goto :goto_3

    .line 141
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 142
    goto :goto_1

    .line 144
    :pswitch_4
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 145
    goto/16 :goto_1

    .line 147
    :pswitch_5
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 148
    goto/16 :goto_1

    .line 150
    :pswitch_6
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0xb

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 151
    goto/16 :goto_1

    .line 153
    :pswitch_7
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0x10

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 154
    goto/16 :goto_1

    .line 156
    :pswitch_8
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/16 v5, 0x11

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 157
    goto/16 :goto_1

    .line 159
    :pswitch_9
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 160
    goto/16 :goto_1

    .line 162
    :pswitch_a
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 163
    goto/16 :goto_1

    .line 165
    :pswitch_b
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 166
    goto/16 :goto_1

    .line 168
    :pswitch_c
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 169
    goto/16 :goto_1

    .line 171
    :pswitch_d
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 172
    goto/16 :goto_1

    .line 174
    :pswitch_e
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 175
    goto/16 :goto_1

    .line 177
    :pswitch_f
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 178
    goto/16 :goto_1

    .line 180
    :pswitch_10
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/voovio/sweep/TemplateManager;->getTemplate(I)Lcom/voovio/sweep/Template;

    move-result-object v3

    .line 181
    goto/16 :goto_1

    .line 97
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 103
    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x1e -> :sswitch_1
        0x2d -> :sswitch_2
        0x5a -> :sswitch_3
    .end sparse-switch

    .line 122
    :sswitch_data_1
    .sparse-switch
        0xa -> :sswitch_4
        0x1e -> :sswitch_5
        0x2d -> :sswitch_6
        0x5a -> :sswitch_7
    .end sparse-switch
.end method

.method private getType(Lcom/voovio/sweep/Template;)I
    .locals 2
    .param p1, "oTemplate"    # Lcom/voovio/sweep/Template;

    .prologue
    .line 192
    const/4 v1, 0x0

    .line 194
    .local v1, "nType":I
    invoke-virtual {p1}, Lcom/voovio/sweep/Template;->getId()I

    move-result v0

    .line 195
    .local v0, "nId":I
    packed-switch v0, :pswitch_data_0

    .line 256
    :goto_0
    return v1

    .line 197
    :pswitch_0
    const/16 v1, 0xb

    .line 198
    goto :goto_0

    .line 200
    :pswitch_1
    const/16 v1, 0xc

    .line 201
    goto :goto_0

    .line 203
    :pswitch_2
    const/16 v1, 0xd

    .line 204
    goto :goto_0

    .line 206
    :pswitch_3
    const/16 v1, 0xe

    .line 207
    goto :goto_0

    .line 209
    :pswitch_4
    const/16 v1, 0xf

    .line 210
    goto :goto_0

    .line 212
    :pswitch_5
    const/16 v1, 0x10

    .line 213
    goto :goto_0

    .line 215
    :pswitch_6
    const/16 v1, 0x11

    .line 216
    goto :goto_0

    .line 218
    :pswitch_7
    const/16 v1, 0x12

    .line 219
    goto :goto_0

    .line 221
    :pswitch_8
    const/4 v1, 0x5

    .line 222
    goto :goto_0

    .line 224
    :pswitch_9
    const/4 v1, 0x6

    .line 225
    goto :goto_0

    .line 227
    :pswitch_a
    const/4 v1, 0x7

    .line 228
    goto :goto_0

    .line 230
    :pswitch_b
    const/16 v1, 0x8

    .line 231
    goto :goto_0

    .line 233
    :pswitch_c
    const/16 v1, 0x9

    .line 234
    goto :goto_0

    .line 236
    :pswitch_d
    const/16 v1, 0xa

    .line 237
    goto :goto_0

    .line 243
    :pswitch_e
    const/4 v1, 0x1

    .line 244
    goto :goto_0

    .line 250
    :pswitch_f
    const/4 v1, 0x2

    .line 251
    goto :goto_0

    .line 195
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_e
        :pswitch_f
        :pswitch_e
        :pswitch_f
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_e
        :pswitch_f
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method


# virtual methods
.method public decodeSEF(Ljava/lang/String;)Lcom/voovio/sweep/Sweep;
    .locals 30
    .param p1, "strFilePath"    # Ljava/lang/String;

    .prologue
    .line 308
    const/16 v25, 0x850

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-static {v0, v1}, Lcom/sec/android/secvision/sef/SEF;->hasDataType(Ljava/lang/String;I)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 309
    const-string v25, "VirtualTourDataManager"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "File"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "\' is a Virtual Tour SEF File"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    const/4 v14, 0x0

    .line 318
    .local v14, "exifReader":Landroid/media/ExifInterface;
    :try_start_0
    new-instance v15, Landroid/media/ExifInterface;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v14    # "exifReader":Landroid/media/ExifInterface;
    .local v15, "exifReader":Landroid/media/ExifInterface;
    move-object v14, v15

    .line 323
    .end local v15    # "exifReader":Landroid/media/ExifInterface;
    .restart local v14    # "exifReader":Landroid/media/ExifInterface;
    :goto_0
    const-string v25, "Orientation"

    const/16 v26, -0x1

    move-object/from16 v0, v25

    move/from16 v1, v26

    invoke-virtual {v14, v0, v1}, Landroid/media/ExifInterface;->getAttributeInt(Ljava/lang/String;I)I

    move-result v6

    .line 326
    .local v6, "orientation":I
    const/16 v23, 0x0

    .line 330
    .local v23, "oSweep":Lcom/voovio/sweep/Sweep;
    invoke-static/range {p1 .. p1}, Lcom/sec/android/secvision/sef/SEF;->getSEFDataCount(Ljava/lang/String;)I

    move-result v22

    .line 331
    .local v22, "numSEFData":I
    const-string v25, "VirtualTourDataManager"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Number of SEF data :"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    const/4 v12, 0x0

    check-cast v12, [[B

    .line 334
    .local v12, "dataBuffer":[[B
    if-lez v22, :cond_1

    .line 335
    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v12, v0, [[B

    .line 339
    const/16 v25, 0x0

    :try_start_1
    const-string v26, "VirtualTour_Info"

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, Lcom/sec/android/secvision/sef/SEF;->getSEFData(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v26

    aput-object v26, v12, v25
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 349
    :goto_1
    const/16 v25, 0x0

    aget-object v25, v12, v25

    if-nez v25, :cond_2

    .line 350
    const-string v25, "VirtualTourDataManager"

    const-string v26, "Failed in SEF decode"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    const/16 v23, 0x0

    .line 453
    .end local v6    # "orientation":I
    .end local v12    # "dataBuffer":[[B
    .end local v14    # "exifReader":Landroid/media/ExifInterface;
    .end local v22    # "numSEFData":I
    .end local v23    # "oSweep":Lcom/voovio/sweep/Sweep;
    :goto_2
    return-object v23

    .line 311
    :cond_0
    const-string v25, "VirtualTourDataManager"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "File"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, "\' is a not Virtual Tour SEF File"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    const/16 v23, 0x0

    goto :goto_2

    .line 319
    .restart local v14    # "exifReader":Landroid/media/ExifInterface;
    :catch_0
    move-exception v13

    .line 320
    .local v13, "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 340
    .end local v13    # "e":Ljava/io/IOException;
    .restart local v6    # "orientation":I
    .restart local v12    # "dataBuffer":[[B
    .restart local v22    # "numSEFData":I
    .restart local v23    # "oSweep":Lcom/voovio/sweep/Sweep;
    :catch_1
    move-exception v13

    .line 341
    .restart local v13    # "e":Ljava/io/IOException;
    invoke-virtual {v13}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 345
    .end local v13    # "e":Ljava/io/IOException;
    :cond_1
    const/16 v23, 0x0

    goto :goto_2

    .line 354
    :cond_2
    const/16 v25, 0x0

    aget-object v25, v12, v25

    invoke-static/range {v25 .. v25}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 356
    .local v11, "bbHeader":Ljava/nio/ByteBuffer;
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v18

    .line 357
    .local v18, "nImages":I
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    .line 358
    .local v4, "nImageWidth":I
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    .line 359
    .local v5, "nImageHeight":I
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    .line 360
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    .line 361
    add-int/lit8 v25, v22, -0x1

    move/from16 v0, v25

    move/from16 v1, v18

    if-eq v0, v1, :cond_3

    .line 363
    const-string v25, "VirtualTourDataManager"

    const-string v26, "Some images are not saved properly"

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    const/16 v23, 0x0

    goto :goto_2

    .line 367
    :cond_3
    const/16 v25, 0x6

    move/from16 v0, v25

    if-eq v6, v0, :cond_4

    const/16 v25, 0x8

    move/from16 v0, v25

    if-ne v6, v0, :cond_5

    .line 368
    :cond_4
    move/from16 v20, v4

    .line 369
    .local v20, "nPivot":I
    move v4, v5

    .line 370
    move/from16 v5, v20

    .line 372
    .end local v20    # "nPivot":I
    :cond_5
    const-string v25, "VirtualTourDataManager"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    const-string v27, " "

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    move-object/from16 v0, v26

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->m_oTemplateManager:Lcom/voovio/sweep/TemplateManager;

    move-object/from16 v26, v0

    if-le v4, v5, :cond_6

    const-string v25, "horizontal"

    :goto_3
    move-object/from16 v0, v26

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/voovio/sweep/TemplateManager;->setImageAspect(Ljava/lang/String;)V

    .line 375
    add-int/lit8 v25, v18, -0x1

    move/from16 v0, v25

    new-array v7, v0, [Lcom/voovio/sweep/Template;

    .line 376
    .local v7, "aTemplates":[Lcom/voovio/sweep/Template;
    add-int/lit8 v25, v18, -0x1

    move/from16 v0, v25

    new-array v8, v0, [Lcom/voovio/voo3d/data/Vector3;

    .line 377
    .local v8, "aTranslations":[Lcom/voovio/voo3d/data/Vector3;
    add-int/lit8 v25, v18, -0x1

    move/from16 v0, v25

    new-array v9, v0, [F

    .line 381
    .local v9, "aTransitionsData":[F
    const/16 v17, 0x0

    .local v17, "index":I
    :goto_4
    add-int/lit8 v25, v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v25

    if-ge v0, v1, :cond_e

    .line 382
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v21

    .line 384
    .local v21, "nTransitionType":I
    const/16 v25, 0x0

    aput-object v25, v8, v17

    .line 385
    const/high16 v25, 0x7fc00000    # NaNf

    aput v25, v9, v17

    .line 387
    if-nez v21, :cond_7

    .line 388
    const-string v25, "SEFParfer"

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "Unsupported Transition Type:"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-static/range {v25 .. v26}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    const/16 v23, 0x0

    goto/16 :goto_2

    .line 374
    .end local v7    # "aTemplates":[Lcom/voovio/sweep/Template;
    .end local v8    # "aTranslations":[Lcom/voovio/voo3d/data/Vector3;
    .end local v9    # "aTransitionsData":[F
    .end local v17    # "index":I
    .end local v21    # "nTransitionType":I
    :cond_6
    const-string v25, "vertical"

    goto :goto_3

    .line 390
    .restart local v7    # "aTemplates":[Lcom/voovio/sweep/Template;
    .restart local v8    # "aTranslations":[Lcom/voovio/voo3d/data/Vector3;
    .restart local v9    # "aTransitionsData":[F
    .restart local v17    # "index":I
    .restart local v21    # "nTransitionType":I
    :cond_7
    const/16 v25, 0x1

    move/from16 v0, v21

    move/from16 v1, v25

    if-eq v0, v1, :cond_8

    const/16 v25, 0x2

    move/from16 v0, v21

    move/from16 v1, v25

    if-ne v0, v1, :cond_9

    .line 391
    :cond_8
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v25

    aput v25, v9, v17

    .line 406
    :goto_5
    aget v25, v9, v17

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->getTemplate(IF)Lcom/voovio/sweep/Template;

    move-result-object v25

    aput-object v25, v7, v17

    .line 381
    add-int/lit8 v17, v17, 0x1

    goto :goto_4

    .line 393
    :cond_9
    const/16 v25, 0x9

    move/from16 v0, v21

    move/from16 v1, v25

    if-eq v0, v1, :cond_a

    const/16 v25, 0xa

    move/from16 v0, v21

    move/from16 v1, v25

    if-ne v0, v1, :cond_b

    .line 394
    :cond_a
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v25

    aput v25, v9, v17

    goto :goto_5

    .line 396
    :cond_b
    const/16 v25, 0xd

    move/from16 v0, v21

    move/from16 v1, v25

    if-eq v0, v1, :cond_c

    const/16 v25, 0xb

    move/from16 v0, v21

    move/from16 v1, v25

    if-eq v0, v1, :cond_c

    const/16 v25, 0xe

    move/from16 v0, v21

    move/from16 v1, v25

    if-eq v0, v1, :cond_c

    const/16 v25, 0xc

    move/from16 v0, v21

    move/from16 v1, v25

    if-ne v0, v1, :cond_d

    .line 400
    :cond_c
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    .line 401
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    goto :goto_5

    .line 403
    :cond_d
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    goto :goto_5

    .line 410
    .end local v21    # "nTransitionType":I
    :cond_e
    const/16 v17, 0x0

    :goto_6
    add-int/lit8 v25, v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v25

    if-ge v0, v1, :cond_10

    .line 411
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v19

    .line 413
    .local v19, "nIsValid":I
    const/16 v25, 0x1

    move/from16 v0, v19

    move/from16 v1, v25

    if-ne v0, v1, :cond_f

    .line 415
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->newVector3(Ljava/nio/ByteBuffer;)Lcom/voovio/voo3d/data/Vector3;

    move-result-object v25

    aput-object v25, v8, v17

    .line 410
    :goto_7
    add-int/lit8 v17, v17, 0x1

    goto :goto_6

    .line 417
    :cond_f
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    .line 418
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    .line 419
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getFloat()F

    goto :goto_7

    .line 429
    .end local v19    # "nIsValid":I
    :cond_10
    add-int/lit8 v25, v22, -0x1

    move/from16 v0, v25

    new-array v3, v0, [I

    .line 431
    .local v3, "aSizes":[I
    const/16 v16, 0x0

    .local v16, "imageIndex":I
    :goto_8
    add-int/lit8 v25, v22, -0x1

    move/from16 v0, v16

    move/from16 v1, v25

    if-ge v0, v1, :cond_12

    .line 434
    sget-object v25, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v26, "VirtualTour_%03d"

    const/16 v27, 0x1

    move/from16 v0, v27

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v27, v0

    const/16 v28, 0x0

    add-int/lit8 v29, v16, 0x1

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    aput-object v29, v27, v28

    invoke-static/range {v25 .. v27}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 435
    .local v10, "aKeyName":Ljava/lang/String;
    if-eqz v10, :cond_11

    .line 436
    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/sec/android/secvision/sef/SEF;->getSEFDataPosition(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/android/secvision/sef/SEF$SEFDataPosition;

    move-result-object v24

    .line 437
    .local v24, "tmp":Lcom/sec/android/secvision/sef/SEF$SEFDataPosition;
    if-eqz v24, :cond_11

    .line 439
    move-object/from16 v0, v24

    iget v0, v0, Lcom/sec/android/secvision/sef/SEF$SEFDataPosition;->length:I

    move/from16 v25, v0

    aput v25, v3, v16

    .line 431
    .end local v24    # "tmp":Lcom/sec/android/secvision/sef/SEF$SEFDataPosition;
    :cond_11
    add-int/lit8 v16, v16, 0x1

    goto :goto_8

    .line 447
    .end local v10    # "aKeyName":Ljava/lang/String;
    :cond_12
    :try_start_2
    invoke-static/range {v3 .. v9}, Lcom/voovio/sweep/Sweep;->createSweep([IIII[Lcom/voovio/sweep/Template;[Lcom/voovio/voo3d/data/Vector3;[F)Lcom/voovio/sweep/Sweep;

    move-result-object v23

    .line 448
    move-object/from16 v0, v23

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/voovio/sweep/Sweep;->setSEFFile(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/voovio/sweep/SweepException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_2

    .line 449
    :catch_2
    move-exception v13

    .line 450
    .local v13, "e":Lcom/voovio/sweep/SweepException;
    invoke-virtual {v13}, Lcom/voovio/sweep/SweepException;->printStackTrace()V

    goto/16 :goto_2
.end method

.method public encodeSEF(Ljava/lang/String;Ljava/lang/String;I[I[F[I[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)I
    .locals 26
    .param p1, "strFilePath"    # Ljava/lang/String;
    .param p2, "strImagesPath"    # Ljava/lang/String;
    .param p3, "nImages"    # I
    .param p4, "m_aTransitionsType"    # [I
    .param p5, "aTransitionsData"    # [F
    .param p6, "aIsTranslationValid"    # [I
    .param p7, "aTranslations"    # [Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .prologue
    .line 460
    if-gtz p3, :cond_1

    .line 461
    const-string v21, "VirtualTourDataManager"

    const-string v22, "No Images to save"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    const/16 v18, -0x2

    .line 574
    :cond_0
    :goto_0
    return v18

    .line 464
    :cond_1
    new-instance v15, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v15}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 465
    .local v15, "options":Landroid/graphics/BitmapFactory$Options;
    const/16 v21, 0x1

    move/from16 v0, v21

    iput-boolean v0, v15, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 466
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-object v22, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "3DTour_"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "1.jpg"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-static {v0, v15}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 468
    iget v14, v15, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 469
    .local v14, "nImageWidth":I
    iget v13, v15, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 470
    .local v13, "nImageHeight":I
    sget v8, Lcom/sec/android/secvision/solutions/virtualtour/Settings;->cameraHorizontalViewAngle:F

    .line 471
    .local v8, "fAoVHorizontal":F
    sget v9, Lcom/sec/android/secvision/solutions/virtualtour/Settings;->cameraVerticalViewAngle:F

    .line 473
    .local v9, "fAoVVertical":F
    const/4 v5, 0x0

    .line 476
    .local v5, "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v6, Ljava/io/FileOutputStream;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-object v22, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "VirtualTourHeader.bin"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-direct {v6, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 479
    .end local v5    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .local v6, "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    const/16 v21, 0x4

    :try_start_1
    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 481
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 483
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 485
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 487
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 490
    const/4 v11, 0x0

    .local v11, "index":I
    :goto_1
    add-int/lit8 v21, p3, -0x1

    move/from16 v0, v21

    if-ge v11, v0, :cond_c

    .line 491
    aget v19, p4, v11

    .line 494
    .local v19, "type":I
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 496
    const/16 v21, 0x1

    move/from16 v0, v19

    move/from16 v1, v21

    if-eq v0, v1, :cond_2

    const/16 v21, 0x2

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    .line 497
    :cond_2
    if-eqz p5, :cond_3

    .line 498
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    aget v22, p5, v11

    invoke-virtual/range {v21 .. v22}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 490
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 500
    :cond_3
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 532
    .end local v11    # "index":I
    .end local v19    # "type":I
    :catch_0
    move-exception v7

    move-object v5, v6

    .line 533
    .end local v6    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .restart local v5    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .local v7, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_2
    invoke-virtual {v7}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 537
    if-eqz v5, :cond_4

    .line 539
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 547
    .end local v7    # "e":Ljava/io/FileNotFoundException;
    :cond_4
    :goto_4
    add-int/lit8 v21, p3, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v16, v0

    .line 548
    .local v16, "sefDataNames":[Ljava/lang/String;
    add-int/lit8 v21, p3, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    .line 549
    .local v17, "sefKeyNames":[Ljava/lang/String;
    const/16 v21, 0x0

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    sget-object v23, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "VirtualTourHeader.bin"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    aput-object v22, v16, v21

    .line 550
    const/16 v21, 0x0

    const-string v22, "VirtualTour_Info"

    aput-object v22, v17, v21

    .line 551
    const/4 v10, 0x1

    .local v10, "i":I
    :goto_5
    move/from16 v0, p3

    if-gt v10, v0, :cond_10

    .line 552
    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    sget-object v22, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "3DTour_"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ".jpg"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    aput-object v21, v16, v10

    .line 553
    sget-object v21, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v22, "VirtualTour_%03d"

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v21 .. v23}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    aput-object v21, v17, v10

    .line 551
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    .line 502
    .end local v5    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .end local v10    # "i":I
    .end local v16    # "sefDataNames":[Ljava/lang/String;
    .end local v17    # "sefKeyNames":[Ljava/lang/String;
    .restart local v6    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .restart local v11    # "index":I
    .restart local v19    # "type":I
    :cond_5
    const/16 v21, 0x9

    move/from16 v0, v19

    move/from16 v1, v21

    if-eq v0, v1, :cond_6

    const/16 v21, 0xa

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_9

    .line 503
    :cond_6
    if-eqz p5, :cond_7

    .line 504
    const/16 v21, 0x4

    :try_start_4
    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    aget v22, p5, v11

    invoke-virtual/range {v21 .. v22}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2

    .line 534
    .end local v11    # "index":I
    .end local v19    # "type":I
    :catch_1
    move-exception v7

    move-object v5, v6

    .line 535
    .end local v6    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .restart local v5    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .local v7, "e":Ljava/io/IOException;
    :goto_6
    :try_start_5
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 537
    if-eqz v5, :cond_4

    .line 539
    :try_start_6
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_4

    .line 540
    :catch_2
    move-exception v7

    .line 541
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 506
    .end local v5    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v6    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .restart local v11    # "index":I
    .restart local v19    # "type":I
    :cond_7
    const/16 v21, 0x4

    :try_start_7
    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    .line 537
    .end local v11    # "index":I
    .end local v19    # "type":I
    :catchall_0
    move-exception v21

    move-object v5, v6

    .end local v6    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .restart local v5    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    :goto_7
    if-eqz v5, :cond_8

    .line 539
    :try_start_8
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 542
    :cond_8
    :goto_8
    throw v21

    .line 508
    .end local v5    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .restart local v6    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .restart local v11    # "index":I
    .restart local v19    # "type":I
    :cond_9
    const/16 v21, 0xd

    move/from16 v0, v19

    move/from16 v1, v21

    if-eq v0, v1, :cond_a

    const/16 v21, 0xb

    move/from16 v0, v19

    move/from16 v1, v21

    if-eq v0, v1, :cond_a

    const/16 v21, 0xe

    move/from16 v0, v19

    move/from16 v1, v21

    if-eq v0, v1, :cond_a

    const/16 v21, 0xc

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_b

    .line 510
    :cond_a
    const/16 v21, 0x4

    :try_start_9
    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 511
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    goto/16 :goto_2

    .line 513
    :cond_b
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    goto/16 :goto_2

    .line 518
    .end local v19    # "type":I
    :cond_c
    const/4 v11, 0x0

    :goto_9
    add-int/lit8 v21, p3, -0x1

    move/from16 v0, v21

    if-ge v11, v0, :cond_f

    .line 519
    if-eqz p6, :cond_d

    if-eqz p7, :cond_d

    aget v12, p6, v11

    .line 520
    .local v12, "isValid":I
    :goto_a
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 522
    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v12, v0, :cond_e

    aget-object v21, p7, v11

    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->isNaN(F)Z

    move-result v21

    if-nez v21, :cond_e

    aget-object v21, p7, v11

    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->isNaN(F)Z

    move-result v21

    if-nez v21, :cond_e

    aget-object v21, p7, v11

    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Float;->isNaN(F)Z

    move-result v21

    if-nez v21, :cond_e

    .line 523
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    aget-object v22, p7, v11

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 524
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    aget-object v22, p7, v11

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 525
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    aget-object v22, p7, v11

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 518
    :goto_b
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_9

    .line 519
    .end local v12    # "isValid":I
    :cond_d
    const/4 v12, 0x0

    goto/16 :goto_a

    .line 527
    .restart local v12    # "isValid":I
    :cond_e
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 528
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 529
    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/nio/ByteBuffer;->putFloat(F)Ljava/nio/ByteBuffer;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x4

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v6, v0, v1, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_b

    .line 537
    .end local v12    # "isValid":I
    :cond_f
    if-eqz v6, :cond_12

    .line 539
    :try_start_a
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    move-object v5, v6

    .line 542
    .end local v6    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .restart local v5    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    goto/16 :goto_4

    .line 540
    .end local v5    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .restart local v6    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v7

    .line 541
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    move-object v5, v6

    .line 542
    .end local v6    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .restart local v5    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    goto/16 :goto_4

    .line 540
    .end local v11    # "index":I
    .local v7, "e":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v7

    .line 541
    .local v7, "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    .line 540
    .end local v7    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v7

    .line 541
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_8

    .line 556
    .end local v7    # "e":Ljava/io/IOException;
    .restart local v10    # "i":I
    .restart local v16    # "sefDataNames":[Ljava/lang/String;
    .restart local v17    # "sefKeyNames":[Ljava/lang/String;
    :cond_10
    invoke-static {}, Lcom/sec/android/secvision/sef/SEF;->getVersion()Ljava/lang/String;

    move-result-object v20

    .line 557
    .local v20, "version":Ljava/lang/String;
    const-string v21, "VirtualTourDataManager"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "SEF version = "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    const/16 v18, 0x0

    .line 561
    .local v18, "status":I
    const/16 v21, 0x1

    :try_start_b
    aget-object v21, v16, v21

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->copyFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    .line 566
    :goto_c
    const/16 v21, 0x0

    aget-object v21, v17, v21

    const/16 v22, 0x0

    aget-object v22, v16, v22

    const/16 v23, 0x850

    const/16 v24, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/secvision/sef/SEF;->addSEFDataFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    move-result v18

    .line 567
    const/4 v10, 0x1

    :goto_d
    move/from16 v0, p3

    if-gt v10, v0, :cond_11

    .line 568
    aget-object v21, v17, v10

    aget-object v22, v16, v10

    const/16 v23, 0x1

    const/16 v24, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, v24

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/secvision/sef/SEF;->addSEFDataFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)I

    move-result v18

    .line 567
    add-int/lit8 v10, v10, 0x1

    goto :goto_d

    .line 562
    :catch_6
    move-exception v7

    .line 563
    .restart local v7    # "e":Ljava/io/IOException;
    invoke-virtual {v7}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 571
    .end local v7    # "e":Ljava/io/IOException;
    :cond_11
    const/16 v21, 0x1

    move/from16 v0, v18

    move/from16 v1, v21

    if-eq v0, v1, :cond_0

    .line 574
    const/16 v18, -0x1

    goto/16 :goto_0

    .line 537
    .end local v10    # "i":I
    .end local v16    # "sefDataNames":[Ljava/lang/String;
    .end local v17    # "sefKeyNames":[Ljava/lang/String;
    .end local v18    # "status":I
    .end local v20    # "version":Ljava/lang/String;
    :catchall_1
    move-exception v21

    goto/16 :goto_7

    .line 534
    :catch_7
    move-exception v7

    goto/16 :goto_6

    .line 532
    :catch_8
    move-exception v7

    goto/16 :goto_3

    .end local v5    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .restart local v6    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .restart local v11    # "index":I
    :cond_12
    move-object v5, v6

    .end local v6    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    .restart local v5    # "VirtualTourFileOutStream":Ljava/io/FileOutputStream;
    goto/16 :goto_4
.end method

.method public getTransitionAngleList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;->mTransitionAngleList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public newVector3(Ljava/nio/ByteBuffer;)Lcom/voovio/voo3d/data/Vector3;
    .locals 4
    .param p1, "bbHeader"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 303
    new-instance v0, Lcom/voovio/voo3d/data/Vector3;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/voovio/voo3d/data/Vector3;-><init>(FFF)V

    return-object v0
.end method
