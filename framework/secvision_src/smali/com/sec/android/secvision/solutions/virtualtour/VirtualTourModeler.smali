.class public Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
.super Ljava/lang/Object;
.source "VirtualTourModeler.java"

# interfaces
.implements Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative$OnVirtualTourEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;,
        Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;
    }
.end annotation


# static fields
.field private static final BACKGROUND_THREAD_SATE_START:I = 0x1

.field private static final BACKGROUND_THREAD_STATE_FINISH_REMAINING:I = 0x6

.field private static final BACKGROUND_THREAD_STATE_KILL:I = 0x5

.field private static final BACKGROUND_THREAD_STATE_PAUSE:I = 0x3

.field private static final BACKGROUND_THREAD_STATE_RESUME:I = 0x4

.field private static final BACKGROUND_THREAD_STATE_RUNNING:I = 0x2

.field private static final DG_VT_RETURN_ERROR:I = -0x1

.field private static final DG_VT_RETURN_ON_UNDO:I = 0x0

.field private static final DG_VT_RETURN_SUCCESS:I = 0x1

.field private static final IMAGE_ALIGNMENT_THREAD_WAIT_TIME_IN_MS:I = 0xc8

.field private static final MAX_SWEEP_COUNT:I = 0x1e

.field public static final MSG_ALERT_ORIENTATION_CHANGED_TO_LANDSCAPE:I = 0x2

.field public static final MSG_ALERT_ORIENTATION_CHANGED_TO_PORTRAIT:I = 0x1

.field public static final MSG_ALERT_ORIENTATION_CHANGED_TO_REVERSE_LANDSCAPE:I = 0x4

.field public static final MSG_ALERT_ORIENTATION_CHANGED_TO_REVERSE_PORTRAIT:I = 0x3

.field public static final MSG_CALIBRATION_DONE:I = 0x7

.field public static final MSG_CALIBRATION_FAILED:I = 0x6

.field public static final MSG_CALIBRATION_IN_PROGRESS:I = 0x9

.field public static final MSG_CALIBRATION_OUT_OF_WINDOW:I = 0x8

.field public static final MSG_CALIBRATION_STARTED:I = 0x5

.field public static final MSG_DEACTIVE_BEND_UI:I = 0x24

.field public static final MSG_DEACTIVE_TURNING_AFTER_WALKING_WARNING_UI:I = 0x26

.field public static final MSG_ERROR_BOTH_WALK_AND_TURN:I = 0x23

.field public static final MSG_ERROR_INTOLERABLE_DEVICE_SHAKE:I = 0x17

.field public static final MSG_ERROR_NO_CAPTURES_TIME_OUT:I = 0x18

.field public static final MSG_ERROR_SENSOR_FAILED:I = 0x27

.field public static final MSG_ERROR_WALKING_AFTER_TURNING:I = 0x22

.field public static final MSG_FIRST_IMAGE_UNDONE:I = 0x10

.field public static final MSG_IN_STATIONARY:I = 0xf

.field public static final MSG_IS_CAPTURABLE:I = 0xa

.field public static final MSG_MAX_SIZE:I = 0x27

.field public static final MSG_STEPCOUNT_UPDATE:I = 0x28

.field public static final MSG_TURN_LEFT_INPROGRESS_BLINKING:I = 0x1a

.field public static final MSG_TURN_LEFT_IN_PROGRESS:I = 0xd

.field public static final MSG_TURN_LEFT_MAX_BOUNDARY_CROSSED:I = 0x1e

.field public static final MSG_TURN_RIGHT_INPROGRESS_BLINKING:I = 0x1b

.field public static final MSG_TURN_RIGHT_IN_PROGRESS:I = 0xe

.field public static final MSG_TURN_RIGHT_MAX_BOUNDARY_CROSSED:I = 0x1f

.field public static final MSG_WALKIN_MAX_BOUNDARY_CROSSED:I = 0xc

.field public static final MSG_WALKIN_MIN_BOUNDARY_CROSSED:I = 0xb

.field public static final MSG_WALK_IN_PROGRESS_BLINKING:I = 0x19

.field public static final MSG_WARNING_APPROACHING_WALK_MAX_BOUNDARY:I = 0x25

.field public static final MSG_WARNING_BEND_DEVICE_DOWN:I = 0x21

.field public static final MSG_WARNING_BEND_DEVICE_UP:I = 0x20

.field public static final MSG_WARNING_DISABLE_TOO_FAST:I = 0x16

.field public static final MSG_WARNING_MAX_SWEEP_PHOTO_CROSSED:I = 0x12

.field public static final MSG_WARNING_PITCH_BOUNDARY_CROSSED:I = 0x13

.field public static final MSG_WARNING_ROLL_BOUNDARY_CROSSED:I = 0x14

.field public static final MSG_WARNING_TOO_FAST:I = 0x15

.field public static final MSG_WARNING_TURNING_AFTER_WALKING:I = 0x11

.field public static final MSG_WARNING_TURN_APPROACHING_LEFT_MAX_BOUNDARY:I = 0x1c

.field public static final MSG_WARNING_TURN_APPROACHING_RIGHT_MAX_BOUNDARY:I = 0x1d

.field public static final SWEEP_MAX_PHOTO:I = 0x1e

.field public static final TAG:Ljava/lang/String; = "VirtualTourModeler"

.field private static final TAGTHRD:Ljava/lang/String; = "3DAlign"

.field private static final UNDO_THRESHOLD:I = 0x2

.field public static final VT_MODELER_ORIENTATION_LANDSCAPE:I = 0x2

.field public static final VT_MODELER_ORIENTATION_PORTRAIT:I = 0x1

.field public static final VT_MODELER_ORIENTATION_REVERSE_LANDSCAPE:I = 0x4

.field public static final VT_MODELER_ORIENTATION_REVERSE_PORTRAIT:I = 0x3

.field private static final mIAImgID:[I

.field private static mVirtualTourModelerEventListener:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;

.field private static final modelerInterface:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

.field static mpf32TransitionData:[F

.field static mps32Transition:[I


# instance fields
.field private volatile mBackGroundThreadRunning:Z

.field private volatile mBackGroundThreadState:Ljava/lang/Integer;

.field private volatile mCapturing:Z

.field private volatile mCurrentPhotoCount:I

.field private mDestinationDirectory:Ljava/lang/String;

.field protected mDirectionEstimator:Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;

.field private mEncodeResult:I

.field private mEpiPole:[Lcom/sec/android/secvision/solutions/virtualtour/Point;

.field private mHorizonCalibrationThread:Ljava/lang/Thread;

.field private mHorizontalAoV:F

.field private mIABackGroundThread:Ljava/lang/Thread;

.field private mIABasePointer:I

.field private mIABmpArray:[Landroid/graphics/Bitmap;

.field private mIAFeatureID:[I

.field private mIAMutexObject:Ljava/lang/Object;

.field private mIAMutexObject_State:Ljava/lang/Object;

.field private mImageAlignmentRunnable:Ljava/lang/Runnable;

.field private mImageAlignmentVector:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;",
            ">;"
        }
    .end annotation
.end field

.field private mImageSavingThread:Ljava/lang/Thread;

.field private mIsTranslationValid:[I

.field private mKeyPointMatcher:Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;

.field private mOrientation:I

.field private mOrientationForPicture:I

.field private mPhotoCountMutexObject:Ljava/lang/Object;

.field private mRunning:Z

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mTemporaryStorageDirectory:Ljava/lang/String;

.field private mTransitionsData:[F

.field private mTransitionsType:[I

.field private mTranslations:[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

.field private mUndoPtr:I

.field private mVerticalAoV:F

.field protected mVirtualTourDataManager:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;

.field private miIAThrdCnt:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 109
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVirtualTourModelerEventListener:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;

    .line 110
    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    invoke-direct {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;-><init>()V

    sput-object v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->modelerInterface:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .line 167
    new-array v0, v1, [I

    aput v2, v0, v2

    sput-object v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mps32Transition:[I

    .line 168
    new-array v0, v1, [F

    const/4 v1, 0x0

    aput v1, v0, v2

    sput-object v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mpf32TransitionData:[F

    .line 206
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAImgID:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/16 v3, 0x1d

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mDirectionEstimator:Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;

    .line 107
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVirtualTourDataManager:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;

    .line 120
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAMutexObject:Ljava/lang/Object;

    .line 121
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAMutexObject_State:Ljava/lang/Object;

    .line 122
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mPhotoCountMutexObject:Ljava/lang/Object;

    .line 127
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mHorizonCalibrationThread:Ljava/lang/Thread;

    .line 132
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageSavingThread:Ljava/lang/Thread;

    .line 137
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABackGroundThread:Ljava/lang/Thread;

    .line 139
    iput-boolean v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCapturing:Z

    .line 140
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mRunning:Z

    .line 142
    iput v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    .line 148
    iput-boolean v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadRunning:Z

    .line 159
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadState:Ljava/lang/Integer;

    .line 161
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsType:[I

    .line 162
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIsTranslationValid:[I

    .line 163
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsData:[F

    .line 164
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTranslations:[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .line 165
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mEpiPole:[Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 170
    iput v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mScreenHeight:I

    .line 171
    iput v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mScreenWidth:I

    .line 172
    iput v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mHorizontalAoV:F

    .line 173
    iput v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVerticalAoV:F

    .line 175
    iput v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mOrientationForPicture:I

    .line 176
    iput v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mOrientation:I

    .line 181
    iput v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->miIAThrdCnt:I

    .line 186
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mEncodeResult:I

    .line 191
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentRunnable:Ljava/lang/Runnable;

    .line 196
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    .line 198
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTemporaryStorageDirectory:Ljava/lang/String;

    .line 199
    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mDestinationDirectory:Ljava/lang/String;

    .line 201
    iput v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    .line 202
    iput v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I

    .line 207
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAFeatureID:[I

    .line 208
    new-array v0, v4, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    .line 236
    invoke-static {}, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->getInstance()Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->setStackStateListener(Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative$OnVirtualTourEventListener;)V

    .line 238
    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;

    invoke-direct {v0}, Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mDirectionEstimator:Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;

    .line 239
    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;

    invoke-direct {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVirtualTourDataManager:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourDataManager;

    .line 241
    new-array v0, v3, [Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTranslations:[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .line 242
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIsTranslationValid:[I

    .line 243
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsData:[F

    .line 244
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsType:[I

    .line 245
    new-array v0, v3, [Lcom/sec/android/secvision/solutions/virtualtour/Point;

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mEpiPole:[Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 247
    sget-boolean v0, Lcom/sec/android/secvision/solutions/virtualtour/Constant;->MODELLER_IMAGE_ALIGNMENT_MODULE:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 249
    new-instance v0, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;

    const/16 v1, 0x3e8

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;-><init>(ID)V

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mKeyPointMatcher:Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;

    .line 252
    :cond_0
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAMutexObject:Ljava/lang/Object;

    .line 253
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAMutexObject_State:Ljava/lang/Object;

    .line 254
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mPhotoCountMutexObject:Ljava/lang/Object;

    .line 257
    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$1;-><init>(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)V

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentRunnable:Ljava/lang/Runnable;

    .line 308
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadRunning:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadRunning:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Integer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadState:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;ILjava/lang/String;Ljava/lang/String;J)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # J

    .prologue
    .line 33
    invoke-direct/range {p0 .. p5}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->postSEFEncode(ILjava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTemporaryStorageDirectory:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTemporaryStorageDirectory:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mDestinationDirectory:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mDestinationDirectory:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    .param p1, "x1"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->stopImageAlignmentThread(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mEncodeResult:I

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mEncodeResult:I

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsType:[I

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)[F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsData:[F

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIsTranslationValid:[I

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTranslations:[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mRunning:Z

    return v0
.end method

.method static synthetic access$2002(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mRunning:Z

    return p1
.end method

.method static synthetic access$202(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I

    return p1
.end method

.method static synthetic access$208(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I

    return v0
.end method

.method static synthetic access$2100(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mOrientation:I

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;D)V
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    .param p1, "x1"    # D

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->updateHorizon(D)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/util/Vector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->checkUndoforIA()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    .param p1, "x1"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->imageAlignment(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCapturing:Z

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAMutexObject_State:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABackGroundThread:Ljava/lang/Thread;

    return-object v0
.end method

.method private checkUndoforIA()V
    .locals 3

    .prologue
    .line 857
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_1

    .line 858
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->m_FileIndex:I
    invoke-static {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->access$2300(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;)I

    move-result v1

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->m_FileIndex:I
    invoke-static {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->access$2300(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;)I

    move-result v0

    if-ne v1, v0, :cond_0

    .line 859
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->removeElementAt(I)V

    .line 860
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I

    .line 861
    iget-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mPhotoCountMutexObject:Ljava/lang/Object;

    monitor-enter v1

    .line 862
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    .line 863
    monitor-exit v1

    .line 871
    :cond_0
    :goto_0
    return-void

    .line 863
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 866
    :cond_1
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I

    add-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_0

    .line 867
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->m_FileIndex:I
    invoke-static {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->access$2300(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;)I

    move-result v1

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->m_FileIndex:I
    invoke-static {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->access$2300(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;)I

    move-result v0

    if-ne v1, v0, :cond_0

    .line 868
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABasePointer:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Vector;->removeElementAt(I)V

    goto :goto_0
.end method

.method private declared-synchronized closeTransitionEstimator()V
    .locals 2

    .prologue
    .line 564
    monitor-enter p0

    :try_start_0
    const-string v0, "VirtualTourModeler"

    const-string v1, "closeTransitionEstimator"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 566
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mDirectionEstimator:Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;

    if-nez v0, :cond_0

    .line 567
    const-string v0, "VirtualTourModeler"

    const-string v1, "Return because of null pointer"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572
    :goto_0
    monitor-exit p0

    return-void

    .line 570
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->stopCalibrationThread()V

    .line 571
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mDirectionEstimator:Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;

    invoke-virtual {v0}, Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;->Close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 564
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private decodeBitmap(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;I)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "ProcessingObject"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;
    .param p2, "nIAIndex"    # I

    .prologue
    const/4 v12, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 881
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 883
    .local v5, "matrix":Landroid/graphics/Matrix;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 885
    .local v8, "start":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 886
    new-instance v7, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    invoke-direct {v7}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;-><init>()V

    .line 887
    .local v7, "Quramoptions":Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    iput v12, v7, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 888
    const/4 v3, 0x7

    iput v3, v7, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    .line 890
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mPhotoCountMutexObject:Ljava/lang/Object;

    monitor-enter v3

    .line 892
    :try_start_0
    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    add-int/lit8 v6, p2, 0x1

    if-ne v4, v6, :cond_0

    .line 893
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    .line 894
    const-string v1, "3DAlign"

    const-string v4, "Undo done before decoding"

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 895
    monitor-exit v3

    move-object v0, v2

    .line 929
    :goto_0
    return-object v0

    .line 897
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 898
    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->m_FileName:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->access$2400(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v7}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->decodeFile(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 899
    .local v0, "BmTmp":Landroid/graphics/Bitmap;
    const-string v3, "3DAlign"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "QRAM Decoding time = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v8

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " msec"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 902
    if-nez v0, :cond_1

    .line 903
    const-string v1, "3DAlign"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BmTmp is Null : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->m_FileName:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->access$2400(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 904
    goto :goto_0

    .line 897
    .end local v0    # "BmTmp":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 907
    .restart local v0    # "BmTmp":Landroid/graphics/Bitmap;
    :cond_1
    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mOrientationForPicture:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 909
    const-string v1, "3DAlign"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Decoded Filename : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->m_FileName:Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->access$2400(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 911
    :cond_2
    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mOrientationForPicture:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_3

    .line 913
    const/high16 v2, 0x43340000    # 180.0f

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 914
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_0

    .line 916
    :cond_3
    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mOrientationForPicture:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    .line 918
    const/high16 v2, 0x43870000    # 270.0f

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 919
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_0

    .line 921
    :cond_4
    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mOrientationForPicture:I

    if-ne v3, v12, :cond_5

    .line 923
    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->setRotate(F)V

    .line 924
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_0

    .line 926
    :cond_5
    const-string v1, "VirtualTourModeler"

    const-string v3, "Wrong Orientation"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 927
    goto/16 :goto_0
.end method

.method public static getInstance()Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;
    .locals 1

    .prologue
    .line 1226
    sget-object v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->modelerInterface:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;

    return-object v0
.end method

.method private getTransitionData()V
    .locals 9

    .prologue
    const/4 v4, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 721
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v0

    .line 722
    .local v0, "CallerClassName":Ljava/lang/String;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v1

    .line 723
    .local v1, "line":I
    const-string v3, "VirtualTourModeler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GetTransitionData ->Called From: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  Line: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    if-ltz v3, :cond_0

    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    const/16 v4, 0x1e

    if-lt v3, v4, :cond_1

    .line 727
    :cond_0
    const-string v3, "VirtualTourModeler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Wrong Case mCurrentPhotoCount= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    :goto_0
    return-void

    .line 731
    :cond_1
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mDirectionEstimator:Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;

    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    sget-object v5, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mpf32TransitionData:[F

    sget-object v6, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mps32Transition:[I

    invoke-virtual {v3, v4, v5, v6}, Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;->GetTemplate(I[F[I)I

    move-result v2

    .line 734
    .local v2, "result":I
    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    if-nez v3, :cond_3

    .line 735
    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mOrientation:I

    iput v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mOrientationForPicture:I

    .line 755
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mPhotoCountMutexObject:Ljava/lang/Object;

    monitor-enter v4

    .line 756
    :try_start_0
    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    .line 757
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 758
    const-string v3, "VirtualTourModeler"

    const-string v4, "createTemplate Exited"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 737
    :cond_3
    if-eqz v2, :cond_2

    .line 738
    const-string v3, "VirtualTourModeler"

    const-string v4, "createTemplate Photo %d"

    new-array v5, v8, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 740
    const-string v3, "VirtualTourModeler"

    const-string v4, "!#!! CURRENTPHOTOoclk %d"

    new-array v5, v8, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsType:[I

    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    add-int/lit8 v4, v4, -0x1

    sget-object v5, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mps32Transition:[I

    aget v5, v5, v7

    aput v5, v3, v4

    .line 743
    sget-object v3, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mps32Transition:[I

    aget v3, v3, v7

    if-ne v3, v8, :cond_4

    .line 744
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsData:[F

    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    add-int/lit8 v4, v4, -0x1

    sget-object v5, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mpf32TransitionData:[F

    aget v5, v5, v7

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    neg-double v6, v6

    double-to-float v5, v6

    aput v5, v3, v4

    goto :goto_1

    .line 747
    :cond_4
    sget-object v3, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mps32Transition:[I

    aget v3, v3, v7

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    .line 748
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsData:[F

    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    add-int/lit8 v4, v4, -0x1

    sget-object v5, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mpf32TransitionData:[F

    aget v5, v5, v7

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    double-to-float v5, v6

    aput v5, v3, v4

    goto :goto_1

    .line 749
    :cond_5
    sget-object v3, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mps32Transition:[I

    aget v3, v3, v7

    const/16 v4, 0x9

    if-ne v3, v4, :cond_6

    .line 750
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsData:[F

    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    add-int/lit8 v4, v4, -0x1

    sget-object v5, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mpf32TransitionData:[F

    aget v5, v5, v7

    aput v5, v3, v4

    goto/16 :goto_1

    .line 751
    :cond_6
    sget-object v3, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mps32Transition:[I

    aget v3, v3, v7

    const/16 v4, 0xa

    if-ne v3, v4, :cond_2

    .line 752
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsData:[F

    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    add-int/lit8 v4, v4, -0x1

    sget-object v5, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mpf32TransitionData:[F

    aget v5, v5, v7

    aput v5, v3, v4

    goto/16 :goto_1

    .line 757
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method private horizonIndicatorUpdate(FF)V
    .locals 1
    .param p1, "dx"    # F
    .param p2, "dy"    # F

    .prologue
    .line 1255
    sget-object v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVirtualTourModelerEventListener:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;

    if-eqz v0, :cond_0

    .line 1256
    sget-object v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVirtualTourModelerEventListener:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;->horizonIndicatorUpdate(FF)V

    .line 1258
    :cond_0
    return-void
.end method

.method private imageAlignment(I)I
    .locals 8
    .param p1, "nIAIndex"    # I

    .prologue
    const/4 v3, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 939
    const/4 v2, 0x0

    .line 941
    .local v2, "ret":I
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    if-eqz v4, :cond_0

    if-gez p1, :cond_1

    .line 942
    :cond_0
    const-string v4, "3DAlign"

    const-string v5, "ImageAlignment : Improper params"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 994
    :goto_0
    return v3

    .line 946
    :cond_1
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-lt p1, v4, :cond_2

    .line 947
    const-string v4, "3DAlign"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ImageAlignment : Improper params-> Vecsize: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "Index: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 951
    :cond_2
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    const/16 v5, 0x1e

    if-gt v4, v5, :cond_3

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    if-gtz v4, :cond_4

    .line 952
    :cond_3
    const-string v4, "3DAlign"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "The vector has exceeded the limit. current size= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    invoke-virtual {v6}, Ljava/util/Vector;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 956
    :cond_4
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    add-int/lit8 v5, p1, 0x1

    invoke-virtual {v4, v5}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;

    .line 958
    .local v0, "CurrentProcessingObject":Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;
    if-nez v0, :cond_5

    .line 959
    const-string v4, "3DAlign"

    const-string v5, "ImageAlignment : Unable to fetch from vector"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 962
    :cond_5
    const-string v3, "3DAlign"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Index0:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Index1: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, p1, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Transition Type:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->m_s32State:I
    invoke-static {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->access$2500(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 964
    if-nez p1, :cond_8

    .line 966
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    invoke-virtual {v3, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;

    .line 967
    .local v1, "TempObject":Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v3, v3, p1

    if-nez v3, :cond_6

    if-eqz v1, :cond_6

    .line 968
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    invoke-direct {p0, v1, p1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->decodeBitmap(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;I)Landroid/graphics/Bitmap;

    move-result-object v4

    aput-object v4, v3, p1

    .line 970
    :cond_6
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    add-int/lit8 v4, p1, 0x1

    invoke-direct {p0, v0, p1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->decodeBitmap(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;I)Landroid/graphics/Bitmap;

    move-result-object v5

    aput-object v5, v3, v4

    .line 979
    .end local v1    # "TempObject":Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;
    :goto_1
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v6

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v7

    if-eqz v3, :cond_7

    .line 981
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v7

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    if-eq v3, v4, :cond_9

    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v7

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    if-eq v3, v4, :cond_9

    .line 982
    const-string v3, "3DAlign"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bm0 ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v6

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v6

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Bm1 ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v7

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v7

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    :goto_2
    move v3, v2

    .line 994
    goto/16 :goto_0

    .line 976
    :cond_8
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    add-int/lit8 v4, p1, 0x1

    rem-int/lit8 v4, v4, 0x2

    invoke-direct {p0, v0, p1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->decodeBitmap(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;I)Landroid/graphics/Bitmap;

    move-result-object v5

    aput-object v5, v3, v4

    goto/16 :goto_1

    .line 983
    :cond_9
    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->m_s32State:I
    invoke-static {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->access$2500(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;)I

    move-result v3

    const/16 v4, 0x9

    if-eq v3, v4, :cond_a

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->m_s32State:I
    invoke-static {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->access$2500(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;)I

    move-result v3

    const/16 v4, 0xa

    if-ne v3, v4, :cond_b

    .line 984
    :cond_a
    const-string v3, "3DAlign"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "WALK Bm ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v6

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v6

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 985
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {p0, v3, v4, p1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->walkVectorEstimation(III)I

    move-result v2

    goto :goto_2

    .line 986
    :cond_b
    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->m_s32State:I
    invoke-static {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->access$2500(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;)I

    move-result v3

    if-eq v3, v7, :cond_c

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->m_s32State:I
    invoke-static {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->access$2500(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_d

    .line 987
    :cond_c
    const-string v3, "3DAlign"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "TURN Bm ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v6

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v6

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 988
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v3, v3, v6

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {p0, v3, v4, p1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->turnVectorEstimation(III)I

    move-result v2

    goto/16 :goto_2

    .line 990
    :cond_d
    const-string v3, "3DAlign"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "NONE: Wrong Transition Type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    # getter for: Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->m_s32State:I
    invoke-static {v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;->access$2500(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method private onOrientationChangedEngine(I)V
    .locals 1
    .param p1, "orientation"    # I

    .prologue
    .line 1273
    sget-object v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVirtualTourModelerEventListener:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;

    if-eqz v0, :cond_0

    .line 1274
    sget-object v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVirtualTourModelerEventListener:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;

    invoke-interface {v0, p1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;->onOrientationChangedEngine(I)V

    .line 1276
    :cond_0
    return-void
.end method

.method private openTransitionEstimator(IIIIIFF)V
    .locals 7
    .param p1, "PictureWidth"    # I
    .param p2, "PictureHeight"    # I
    .param p3, "PreviewWidth"    # I
    .param p4, "PreviewHeight"    # I
    .param p5, "ScreenOrientation"    # I
    .param p6, "HorizontalAoV"    # F
    .param p7, "VerticalAoV"    # F

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 536
    const-string v0, "VirtualTourModeler"

    const-string v1, "openTransitionEstimator"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mDirectionEstimator:Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIsTranslationValid:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTranslations:[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsData:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsType:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mEpiPole:[Lcom/sec/android/secvision/solutions/virtualtour/Point;

    if-nez v0, :cond_1

    .line 540
    :cond_0
    const-string v0, "VirtualTourModeler"

    const-string v1, "Return because of null pointer"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    :goto_0
    return-void

    .line 544
    :cond_1
    iput v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    .line 545
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    const/16 v0, 0x1d

    if-ge v6, v0, :cond_2

    .line 546
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIsTranslationValid:[I

    aput v2, v0, v6

    .line 547
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTranslations:[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    aput-object v3, v0, v6

    .line 548
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsData:[F

    const/4 v1, 0x0

    aput v1, v0, v6

    .line 549
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsType:[I

    aput v2, v0, v6

    .line 550
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mEpiPole:[Lcom/sec/android/secvision/solutions/virtualtour/Point;

    aput-object v3, v0, v6

    .line 545
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 555
    :cond_2
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mDirectionEstimator:Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;

    iget v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mOrientation:I

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;->Open(IIIII)V

    .line 556
    invoke-direct {p0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->startCalibrationThread()V

    goto :goto_0
.end method

.method private postMessage(II)V
    .locals 1
    .param p1, "messageID"    # I
    .param p2, "data"    # I

    .prologue
    .line 1266
    sget-object v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVirtualTourModelerEventListener:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;

    if-eqz v0, :cond_0

    .line 1267
    sget-object v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVirtualTourModelerEventListener:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;->onStateChanged(II)V

    .line 1269
    :cond_0
    return-void
.end method

.method private postSEFEncode(ILjava/lang/String;Ljava/lang/String;J)V
    .locals 6
    .param p1, "result"    # I
    .param p2, "strFilePath"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "dateTaken"    # J

    .prologue
    .line 1279
    sget-object v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVirtualTourModelerEventListener:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;

    if-eqz v0, :cond_0

    .line 1280
    sget-object v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVirtualTourModelerEventListener:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;->postSEFEncode(ILjava/lang/String;Ljava/lang/String;J)V

    .line 1282
    :cond_0
    return-void
.end method

.method private resetTransitionEstimator(IIIIIFF)V
    .locals 7
    .param p1, "PictureWidth"    # I
    .param p2, "PictureHeight"    # I
    .param p3, "PreviewWidth"    # I
    .param p4, "PreviewHeight"    # I
    .param p5, "ScreenOrientation"    # I
    .param p6, "HorizontalAoV"    # F
    .param p7, "VerticalAoV"    # F

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 586
    const-string v0, "VirtualTourModeler"

    const-string v1, "resetTransitionEstimator"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    invoke-direct {p0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->stopCalibrationThread()V

    .line 589
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mDirectionEstimator:Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIsTranslationValid:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTranslations:[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsData:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsType:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mEpiPole:[Lcom/sec/android/secvision/solutions/virtualtour/Point;

    if-nez v0, :cond_1

    .line 591
    :cond_0
    const-string v0, "VirtualTourModeler"

    const-string v1, "Return because of null pointer"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    :goto_0
    return-void

    .line 595
    :cond_1
    iput v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    .line 596
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    const/16 v0, 0x1d

    if-ge v6, v0, :cond_2

    .line 597
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIsTranslationValid:[I

    aput v2, v0, v6

    .line 598
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTranslations:[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    aput-object v3, v0, v6

    .line 599
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsData:[F

    const/4 v1, 0x0

    aput v1, v0, v6

    .line 600
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsType:[I

    aput v2, v0, v6

    .line 601
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mEpiPole:[Lcom/sec/android/secvision/solutions/virtualtour/Point;

    aput-object v3, v0, v6

    .line 596
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 606
    :cond_2
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mDirectionEstimator:Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;

    iget v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mOrientation:I

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/secvision/solutions/virtualtour/CDirectionEstimator;->Reset(IIIII)V

    .line 607
    invoke-direct {p0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->startCalibrationThread()V

    goto :goto_0
.end method

.method private declared-synchronized startCalibrationThread()V
    .locals 2

    .prologue
    .line 616
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mHorizonCalibrationThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 617
    invoke-direct {p0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->stopCalibrationThread()V

    .line 620
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mRunning:Z

    .line 621
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$3;

    invoke-direct {v1, p0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$3;-><init>(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mHorizonCalibrationThread:Ljava/lang/Thread;

    .line 664
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mHorizonCalibrationThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 665
    monitor-exit p0

    return-void

    .line 616
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized startImageAlignmentThread()V
    .locals 5

    .prologue
    .line 769
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v0

    .line 770
    .local v0, "CallerClassName":Ljava/lang/String;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v1

    .line 771
    .local v1, "line":I
    const-string v2, "3DAlign"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startImageAlignmentThread->Called From: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  Line: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABackGroundThread:Ljava/lang/Thread;

    if-eqz v2, :cond_0

    .line 773
    const/4 v2, 0x5

    invoke-direct {p0, v2}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->stopImageAlignmentThread(I)V

    .line 775
    :cond_0
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABackGroundThread:Ljava/lang/Thread;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    if-nez v2, :cond_3

    .line 778
    :cond_1
    const-string v2, "3DAlign"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ThreadInstance: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABackGroundThread:Ljava/lang/Thread;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " And Vector: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " create thread"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 780
    new-instance v2, Ljava/lang/Thread;

    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentRunnable:Ljava/lang/Runnable;

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABackGroundThread:Ljava/lang/Thread;

    .line 781
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadRunning:Z

    .line 782
    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadState:Ljava/lang/Integer;

    .line 783
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABackGroundThread:Ljava/lang/Thread;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "VirtualTourModeler_ImageAlignmentThread: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->miIAThrdCnt:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 784
    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->miIAThrdCnt:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->miIAThrdCnt:I

    .line 786
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    if-nez v2, :cond_2

    .line 787
    const-string v2, "3DAlign"

    const-string v3, "Vector is null also"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 788
    new-instance v2, Ljava/util/Vector;

    const/16 v3, 0x1e

    invoke-direct {v2, v3}, Ljava/util/Vector;-><init>(I)V

    iput-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    .line 794
    :goto_0
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABackGroundThread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 800
    :goto_1
    monitor-exit p0

    return-void

    .line 790
    :cond_2
    :try_start_1
    const-string v2, "3DAlign"

    const-string v3, "Vector already created hence clearing"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->removeAllElements()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 769
    .end local v0    # "CallerClassName":Ljava/lang/String;
    .end local v1    # "line":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 799
    .restart local v0    # "CallerClassName":Ljava/lang/String;
    .restart local v1    # "line":I
    :cond_3
    :try_start_2
    const-string v2, "3DAlign"

    const-string v3, "Image Alignment Start Done"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private declared-synchronized stopCalibrationThread()V
    .locals 6

    .prologue
    .line 672
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v0

    .line 673
    .local v0, "CallerClassName":Ljava/lang/String;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v2

    .line 674
    .local v2, "line":I
    const-string v3, "VirtualTourModeler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stopCalibrationThread->Called From: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  Line: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mRunning:Z

    .line 676
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mHorizonCalibrationThread:Ljava/lang/Thread;

    if-eqz v3, :cond_1

    .line 677
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mHorizonCalibrationThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->isAlive()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    .line 679
    :try_start_1
    const-string v3, "VirtualTourModeler"

    const-string v4, "Calib Thread Join 0"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 680
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mHorizonCalibrationThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->join()V

    .line 681
    const-string v3, "VirtualTourModeler"

    const-string v4, "Calib Thread Join 1"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 686
    :cond_0
    :goto_0
    const/4 v3, 0x0

    :try_start_2
    iput-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mHorizonCalibrationThread:Ljava/lang/Thread;

    .line 688
    :cond_1
    const-string v3, "VirtualTourModeler"

    const-string v4, "stopCalibrationThread Exit"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 689
    monitor-exit p0

    return-void

    .line 682
    :catch_0
    move-exception v1

    .line 683
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 672
    .end local v0    # "CallerClassName":Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .end local v2    # "line":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method private stopImageAlignmentThread(I)V
    .locals 7
    .param p1, "State"    # I

    .prologue
    const/4 v6, 0x5

    const/4 v4, 0x3

    .line 811
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v0

    .line 813
    .local v0, "CallerClassName":Ljava/lang/String;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v2

    .line 815
    .local v2, "line":I
    const-string v3, "3DAlign"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stopImageAlignmentThread->Called From: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  Line: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 816
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAMutexObject_State:Ljava/lang/Object;

    monitor-enter v4

    .line 817
    if-ne p1, v6, :cond_1

    .line 818
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadState:Ljava/lang/Integer;

    .line 824
    :goto_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 825
    const-string v3, "3DAlign"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "stopImageAlignmentThread with state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadState:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 827
    iget-object v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAMutexObject:Ljava/lang/Object;

    monitor-enter v4

    .line 828
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABackGroundThread:Ljava/lang/Thread;

    if-eqz v3, :cond_4

    .line 829
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABackGroundThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->isAlive()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    if-eqz v3, :cond_0

    .line 831
    :try_start_2
    const-string v3, "3DAlign"

    const-string v5, "BackGround Thread Join 0"

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 832
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABackGroundThread:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->join()V

    .line 833
    const-string v3, "3DAlign"

    const-string v5, "BackGround Thread Join 1"

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 837
    :goto_1
    const/4 v3, 0x0

    :try_start_3
    iput-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABackGroundThread:Ljava/lang/Thread;

    .line 840
    :cond_0
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    if-nez v3, :cond_3

    .line 841
    const-string v3, "3DAlign"

    const-string v5, "Return because of null pointer"

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 849
    :goto_2
    return-void

    .line 819
    :cond_1
    const/4 v3, 0x6

    if-ne p1, v3, :cond_2

    .line 820
    :try_start_4
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadState:Ljava/lang/Integer;

    goto :goto_0

    .line 824
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v3

    .line 822
    :cond_2
    :try_start_5
    const-string v3, "3DAlign"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stopImageAlignmentThread called with wrong state:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 834
    :catch_0
    move-exception v1

    .line 835
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_6
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 848
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v3

    .line 844
    :cond_3
    :try_start_7
    iget-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->removeAllElements()V

    .line 845
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadRunning:Z

    .line 846
    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mBackGroundThreadState:Ljava/lang/Integer;

    .line 848
    :cond_4
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2
.end method

.method private turnVectorEstimation(III)I
    .locals 26
    .param p1, "img_width"    # I
    .param p2, "img_height"    # I
    .param p3, "nIAIndex"    # I

    .prologue
    .line 1010
    invoke-static/range {p1 .. p2}, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->computeCameraParameters(II)V

    .line 1011
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsData:[F

    move-object/from16 v20, v0

    aget v20, v20, p3

    invoke-static/range {v20 .. v20}, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->computeSearchRegions(F)V

    .line 1013
    const-string v20, "VirtualTourModeler"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Turn: angle "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsData:[F

    move-object/from16 v22, v0

    aget v22, v22, p3

    move/from16 v0, v22

    float-to-double v0, v0

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v22

    invoke-virtual/range {v21 .. v23}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1016
    sget-object v20, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_aSearchRects:[Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    const/16 v21, 0x0

    aget-object v20, v20, v21

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    move/from16 v20, v0

    const/16 v21, 0x0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_a

    sget-object v20, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_aSearchRects:[Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    const/16 v21, 0x1

    aget-object v20, v20, v21

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->width:F

    move/from16 v20, v0

    const/16 v21, 0x0

    cmpl-float v20, v20, v21

    if-lez v20, :cond_a

    .line 1017
    const/4 v8, 0x0

    .line 1019
    .local v8, "mPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;>;"
    new-instance v8, Ljava/util/ArrayList;

    .end local v8    # "mPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;>;"
    const/16 v20, 0x2

    move/from16 v0, v20

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1020
    .restart local v8    # "mPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;>;"
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1021
    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1023
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 1024
    .local v6, "lst1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 1026
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    .line 1027
    .local v7, "lst2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .line 1029
    const/4 v9, 0x0

    .line 1030
    .local v9, "mResult":Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;
    const/4 v11, 0x0

    .line 1033
    .local v11, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mPhotoCountMutexObject:Ljava/lang/Object;

    move-object/from16 v21, v0

    monitor-enter v21

    .line 1035
    :try_start_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    move/from16 v20, v0

    add-int/lit8 v22, p3, 0x1

    move/from16 v0, v20

    move/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 1036
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    .line 1037
    const-string v20, "3DAlign"

    const-string v22, "Turn: Undo done before extraction"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1038
    const/16 v20, 0x0

    monitor-exit v21

    .line 1128
    .end local v6    # "lst1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    .end local v7    # "lst2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    .end local v8    # "mPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;>;"
    .end local v9    # "mResult":Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;
    .end local v11    # "matches":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;>;"
    :goto_0
    return v20

    .line 1040
    .restart local v6    # "lst1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    .restart local v7    # "lst2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    .restart local v8    # "mPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;>;"
    .restart local v9    # "mResult":Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;
    .restart local v11    # "matches":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;>;"
    :cond_0
    monitor-exit v21
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1043
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 1044
    .local v18, "start_time":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mKeyPointMatcher:Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;

    move-object/from16 v20, v0

    sget-object v21, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAImgID:[I

    add-int/lit8 v22, p3, 0x1

    rem-int/lit8 v22, v22, 0x2

    aget v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->clean(I)V

    .line 1045
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAFeatureID:[I

    move-object/from16 v20, v0

    add-int/lit8 v21, p3, 0x1

    rem-int/lit8 v21, v21, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mKeyPointMatcher:Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    move-object/from16 v23, v0

    add-int/lit8 v24, p3, 0x1

    rem-int/lit8 v24, v24, 0x2

    aget-object v23, v23, v24

    sget-object v24, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAImgID:[I

    add-int/lit8 v25, p3, 0x1

    rem-int/lit8 v25, v25, 0x2

    aget v24, v24, v25

    invoke-virtual/range {v22 .. v24}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->extractFeaturePoints(Landroid/graphics/Bitmap;I)I

    move-result v22

    aput v22, v20, v21

    .line 1046
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v12, v20, v18

    .line 1047
    .local v12, "module_time":J
    const-string v20, "3DAlign"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Turn KeyPoint Extraction Time = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " msec"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1049
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAFeatureID:[I

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aget v20, v20, v21

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAFeatureID:[I

    move-object/from16 v20, v0

    const/16 v21, 0x1

    aget v20, v20, v21

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_9

    .line 1051
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mPhotoCountMutexObject:Ljava/lang/Object;

    move-object/from16 v21, v0

    monitor-enter v21

    .line 1053
    :try_start_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    move/from16 v20, v0

    add-int/lit8 v22, p3, 0x1

    move/from16 v0, v20

    move/from16 v1, v22

    if-ne v0, v1, :cond_1

    .line 1054
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    .line 1055
    const-string v20, "3DAlign"

    const-string v22, "Turn: Undo done before matching"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1056
    const/16 v20, 0x0

    monitor-exit v21

    goto/16 :goto_0

    .line 1058
    :catchall_0
    move-exception v20

    monitor-exit v21
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v20

    .line 1040
    .end local v12    # "module_time":J
    .end local v18    # "start_time":J
    :catchall_1
    move-exception v20

    :try_start_2
    monitor-exit v21
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v20

    .line 1058
    .restart local v12    # "module_time":J
    .restart local v18    # "start_time":J
    :cond_1
    :try_start_3
    monitor-exit v21
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1060
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 1061
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mKeyPointMatcher:Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->matchKeypoints()Ljava/util/List;

    move-result-object v11

    .line 1062
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v12, v20, v18

    .line 1063
    const-string v20, "3DAlign"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Turn: KeyPoint Matcher Time = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " msec"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1065
    if-eqz v11, :cond_8

    .line 1069
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 1070
    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;>;"
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_5

    .line 1071
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;

    .line 1072
    .local v10, "match":Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;
    new-instance v14, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    iget v0, v10, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->x1:F

    move/from16 v20, v0

    iget v0, v10, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->y1:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v14, v0, v1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    .line 1073
    .local v14, "p1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    new-instance v15, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    iget v0, v10, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->x2:F

    move/from16 v20, v0

    iget v0, v10, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->y2:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v15, v0, v1}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    .line 1075
    .local v15, "p2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    sget-object v20, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_aSearchRects:[Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    const/16 v21, 0x0

    aget-object v20, v20, v21

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->containsPoint(Lcom/sec/android/secvision/solutions/virtualtour/Point;)Z

    move-result v20

    if-eqz v20, :cond_2

    sget-object v20, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->m_aSearchRects:[Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;

    const/16 v21, 0x1

    aget-object v20, v20, v21

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Lcom/sec/android/secvision/solutions/virtualtour/Rectangle;->containsPoint(Lcom/sec/android/secvision/solutions/virtualtour/Point;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 1076
    const/high16 v3, 0x41200000    # 10.0f

    .line 1078
    .local v3, "fd":F
    const/4 v2, 0x0

    .line 1079
    .local v2, "bSimilar":Z
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v20

    move/from16 v0, v20

    if-ge v5, v0, :cond_3

    .line 1080
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 1081
    .local v16, "ptaux1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    .line 1082
    .local v17, "ptaux2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->getDistance(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v20

    cmpg-float v20, v20, v3

    if-gez v20, :cond_4

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/sec/android/secvision/solutions/virtualtour/Point;->getDistance(Lcom/sec/android/secvision/solutions/virtualtour/Point;)F

    move-result v20

    cmpg-float v20, v20, v3

    if-gez v20, :cond_4

    .line 1083
    const/4 v2, 0x1

    .line 1087
    .end local v16    # "ptaux1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .end local v17    # "ptaux2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    :cond_3
    if-nez v2, :cond_2

    .line 1088
    invoke-virtual {v6, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1089
    invoke-virtual {v7, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1079
    .restart local v16    # "ptaux1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .restart local v17    # "ptaux2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1095
    .end local v2    # "bSimilar":Z
    .end local v3    # "fd":F
    .end local v5    # "j":I
    .end local v10    # "match":Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;
    .end local v14    # "p1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .end local v15    # "p2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .end local v16    # "ptaux1":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    .end local v17    # "ptaux2":Lcom/sec/android/secvision/solutions/virtualtour/Point;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mPhotoCountMutexObject:Ljava/lang/Object;

    move-object/from16 v21, v0

    monitor-enter v21

    .line 1097
    :try_start_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    move/from16 v20, v0

    add-int/lit8 v22, p3, 0x1

    move/from16 v0, v20

    move/from16 v1, v22

    if-ne v0, v1, :cond_6

    .line 1098
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    .line 1099
    const-string v20, "3DAlign"

    const-string v22, "Turn: Undo done before turn geometry"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1100
    const/16 v20, 0x0

    monitor-exit v21

    goto/16 :goto_0

    .line 1102
    :catchall_2
    move-exception v20

    monitor-exit v21
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v20

    :cond_6
    :try_start_5
    monitor-exit v21
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1105
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 1106
    invoke-static {v6, v7}, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry;->computeTurn(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;

    move-result-object v9

    .line 1107
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v12, v20, v18

    .line 1108
    const-string v20, "3DAlign"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Turn: Estimation Time = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " msec"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1110
    if-eqz v9, :cond_7

    .line 1111
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIsTranslationValid:[I

    move-object/from16 v20, v0

    const/16 v21, 0x1

    aput v21, v20, p3

    .line 1112
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTranslations:[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    move-object/from16 v20, v0

    new-instance v21, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    iget-object v0, v9, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;->m_ptAnchor:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    move/from16 v22, v0

    iget-object v0, v9, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;->m_ptAnchor:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    move/from16 v23, v0

    const/16 v24, 0x0

    invoke-direct/range {v21 .. v24}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;-><init>(FFF)V

    aput-object v21, v20, p3

    .line 1113
    const-string v20, "3DAlign"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Turn: Valid Vectorm_oResult.m_ptAnchor.x "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    iget-object v0, v9, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;->m_ptAnchor:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->x:F

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, " m_oResult.m_ptAnchor.y "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    iget-object v0, v9, Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;->m_ptAnchor:Lcom/sec/android/secvision/solutions/virtualtour/Point;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/sec/android/secvision/solutions/virtualtour/Point;->y:F

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1127
    const-string v20, "3DAlign"

    const-string v21, "Turn Vector sucess"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1128
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 1115
    :cond_7
    const-string v20, "3DAlign"

    const-string v21, "Turn: Invalid Vector"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1116
    const/16 v20, -0x1

    goto/16 :goto_0

    .line 1119
    .end local v4    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;>;"
    :cond_8
    const/16 v20, -0x1

    goto/16 :goto_0

    .line 1121
    :cond_9
    const/16 v20, -0x1

    goto/16 :goto_0

    .line 1123
    .end local v6    # "lst1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    .end local v7    # "lst2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    .end local v8    # "mPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;>;"
    .end local v9    # "mResult":Lcom/sec/android/secvision/keypoint_matcher/TurnGeometry$TurnResult;
    .end local v11    # "matches":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;>;"
    .end local v12    # "module_time":J
    .end local v18    # "start_time":J
    :cond_a
    const-string v20, "3DAlign"

    const-string v21, "Turn: There is no Matching region provided by the input angle"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1124
    const/16 v20, -0x1

    goto/16 :goto_0
.end method

.method private updateHorizon(D)V
    .locals 7
    .param p1, "angle"    # D

    .prologue
    const/4 v6, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    .line 696
    const/4 v1, 0x0

    .line 697
    .local v1, "dy":F
    const/4 v0, 0x0

    .line 698
    .local v0, "dx":F
    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mOrientation:I

    packed-switch v2, :pswitch_data_0

    .line 712
    :goto_0
    invoke-direct {p0, v0, v1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->horizonIndicatorUpdate(FF)V

    .line 713
    return-void

    .line 700
    :pswitch_0
    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mScreenHeight:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v2, v3

    sub-float v1, v6, v2

    .line 701
    goto :goto_0

    .line 703
    :pswitch_1
    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mScreenWidth:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v0, v2, v3

    .line 704
    goto :goto_0

    .line 706
    :pswitch_2
    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mScreenHeight:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float v1, v2, v3

    .line 707
    goto :goto_0

    .line 709
    :pswitch_3
    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mScreenWidth:I

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v2, v3

    sub-float v0, v6, v2

    goto :goto_0

    .line 698
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private waitImageSavingThread()V
    .locals 3

    .prologue
    .line 474
    const-string v1, "3DAlign"

    const-string v2, "waitImageSavingThread"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    iget-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageSavingThread:Ljava/lang/Thread;

    if-eqz v1, :cond_1

    .line 476
    iget-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageSavingThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 478
    :try_start_0
    const-string v1, "VirtualTourModeler"

    const-string v2, "waitImageSavingThread Join 0"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    iget-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageSavingThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->join()V

    .line 480
    const-string v1, "VirtualTourModeler"

    const-string v2, "waitImageSavingThread Join 1"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 485
    :cond_0
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageSavingThread:Ljava/lang/Thread;

    .line 489
    :goto_1
    return-void

    .line 481
    :catch_0
    move-exception v0

    .line 482
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 487
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    const-string v1, "VirtualTourModeler"

    const-string v2, "waitImageSavingThread not require"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private walkVectorEstimation(III)I
    .locals 18
    .param p1, "img_width"    # I
    .param p2, "img_height"    # I
    .param p3, "nIAIndex"    # I

    .prologue
    .line 1141
    const/4 v2, 0x0

    .line 1145
    .local v2, "cTranslationResult":Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mPhotoCountMutexObject:Ljava/lang/Object;

    monitor-enter v13

    .line 1147
    :try_start_0
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    add-int/lit8 v14, p3, 0x1

    if-ne v12, v14, :cond_0

    .line 1148
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    .line 1149
    const-string v12, "3DAlign"

    const-string v14, "Walk: Undo done before extraction"

    invoke-static {v12, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1150
    const/4 v12, 0x0

    monitor-exit v13

    .line 1216
    :goto_0
    return v12

    .line 1152
    :cond_0
    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1155
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 1156
    .local v10, "start_time":J
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mKeyPointMatcher:Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;

    sget-object v13, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAImgID:[I

    add-int/lit8 v14, p3, 0x1

    rem-int/lit8 v14, v14, 0x2

    aget v13, v13, v14

    invoke-virtual {v12, v13}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->clean(I)V

    .line 1157
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAFeatureID:[I

    add-int/lit8 v13, p3, 0x1

    rem-int/lit8 v13, v13, 0x2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mKeyPointMatcher:Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIABmpArray:[Landroid/graphics/Bitmap;

    add-int/lit8 v16, p3, 0x1

    rem-int/lit8 v16, v16, 0x2

    aget-object v15, v15, v16

    sget-object v16, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAImgID:[I

    add-int/lit8 v17, p3, 0x1

    rem-int/lit8 v17, v17, 0x2

    aget v16, v16, v17

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->extractFeaturePoints(Landroid/graphics/Bitmap;I)I

    move-result v14

    aput v14, v12, v13

    .line 1158
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long v8, v12, v10

    .line 1159
    .local v8, "module_time":J
    const-string v12, "3DAlign"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Walk: KeyPoint Extraction time "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " msec"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1161
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAFeatureID:[I

    const/4 v13, 0x0

    aget v12, v12, v13

    const/4 v13, -0x1

    if-eq v12, v13, :cond_4

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIAFeatureID:[I

    const/4 v13, 0x1

    aget v12, v12, v13

    const/4 v13, -0x1

    if-eq v12, v13, :cond_4

    .line 1163
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mPhotoCountMutexObject:Ljava/lang/Object;

    monitor-enter v13

    .line 1165
    :try_start_1
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    add-int/lit8 v14, p3, 0x1

    if-ne v12, v14, :cond_1

    .line 1166
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    .line 1167
    const-string v12, "3DAlign"

    const-string v14, "Walk: Undo done before matching"

    invoke-static {v12, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1168
    const/4 v12, 0x0

    monitor-exit v13

    goto/16 :goto_0

    .line 1170
    :catchall_0
    move-exception v12

    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v12

    .line 1152
    .end local v8    # "module_time":J
    .end local v10    # "start_time":J
    :catchall_1
    move-exception v12

    :try_start_2
    monitor-exit v13
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v12

    .line 1170
    .restart local v8    # "module_time":J
    .restart local v10    # "start_time":J
    :cond_1
    :try_start_3
    monitor-exit v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1172
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 1173
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mKeyPointMatcher:Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;

    invoke-virtual {v12}, Lcom/sec/android/secvision/keypoint_matcher/KeyPointMatcher;->matchKeypoints()Ljava/util/List;

    move-result-object v7

    .line 1174
    .local v7, "matches":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long v8, v12, v10

    .line 1176
    const-string v12, "3DAlign"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Walk: KeyPoint Matching time "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " msec"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1177
    if-eqz v7, :cond_4

    .line 1178
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1179
    .local v4, "lst1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1180
    .local v5, "lst2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1181
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;>;"
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 1182
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;

    .line 1183
    .local v6, "match":Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;
    new-instance v12, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    iget v13, v6, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->x1:F

    iget v14, v6, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->y1:F

    invoke-direct {v12, v13, v14}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    invoke-virtual {v4, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1184
    new-instance v12, Lcom/sec/android/secvision/solutions/virtualtour/Point;

    iget v13, v6, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->x2:F

    iget v14, v6, Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;->y2:F

    invoke-direct {v12, v13, v14}, Lcom/sec/android/secvision/solutions/virtualtour/Point;-><init>(FF)V

    invoke-virtual {v5, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1187
    .end local v6    # "match":Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;
    :cond_2
    const-string v12, "3DAlign"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Walk: Lst1.size() = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1190
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mPhotoCountMutexObject:Ljava/lang/Object;

    monitor-enter v13

    .line 1192
    :try_start_4
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    add-int/lit8 v14, p3, 0x1

    if-ne v12, v14, :cond_3

    .line 1193
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    .line 1194
    const-string v12, "3DAlign"

    const-string v14, "Walk: Undo done before epipolar geometry"

    invoke-static {v12, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1195
    const/4 v12, 0x0

    monitor-exit v13

    goto/16 :goto_0

    .line 1197
    :catchall_2
    move-exception v12

    monitor-exit v13
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v12

    :cond_3
    :try_start_5
    monitor-exit v13
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1199
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 1200
    const/high16 v12, 0x42480000    # 50.0f

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-static {v12, v0, v1}, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->setCameraCalibrationMatrixParameters(FII)V

    .line 1201
    invoke-static {v4, v5}, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry;->computeTranslation(Ljava/util/ArrayList;Ljava/util/ArrayList;)Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;

    move-result-object v2

    .line 1202
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long v8, v12, v10

    .line 1204
    const-string v12, "3DAlign"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Walk: Epipolar Geometry Timing:("

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " * "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ") "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " msec"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1206
    if-eqz v2, :cond_5

    iget-object v12, v2, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_vTranslation:Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    if-eqz v12, :cond_5

    .line 1207
    const-string v12, "3DAlign"

    const-string v13, "Walk: VALID VECTOR"

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1208
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIsTranslationValid:[I

    const/4 v13, 0x1

    aput v13, v12, p3

    .line 1209
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTranslations:[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    iget-object v13, v2, Lcom/sec/android/secvision/keypoint_matcher/EpipolarGeometry$TranslationResult;->m_vTranslation:Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    aput-object v13, v12, p3

    .line 1215
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;>;"
    .end local v4    # "lst1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    .end local v5    # "lst2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    .end local v7    # "matches":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;>;"
    :cond_4
    :goto_2
    const-string v12, "3DAlign"

    const-string v13, "Walk Vector Estimation success"

    invoke-static {v12, v13}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1216
    const/4 v12, 0x1

    goto/16 :goto_0

    .line 1211
    .restart local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;>;"
    .restart local v4    # "lst1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    .restart local v5    # "lst2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/secvision/solutions/virtualtour/Point;>;"
    .restart local v7    # "matches":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/secvision/keypoint_matcher/Keypoint_Match;>;"
    :cond_5
    const-string v12, "3DAlign"

    const-string v13, "Walk: NOT VALID VECTOR"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method public SOnStepWalk_RemainingStepsUpdate(I)V
    .locals 3
    .param p1, "remainingStepCount"    # I

    .prologue
    .line 1288
    const-string v0, "VirtualTourModeler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SOnStepWalk_RemainingStepsUpdate - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1289
    const/16 v0, 0x28

    invoke-direct {p0, v0, p1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->postMessage(II)V

    .line 1290
    return-void
.end method

.method public onImageCaptured(Ljava/lang/String;[I)V
    .locals 4
    .param p1, "FileName"    # Ljava/lang/String;
    .param p2, "Direction"    # [I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 367
    invoke-direct {p0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->getTransitionData()V

    .line 368
    sget-object v1, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mps32Transition:[I

    aget v1, v1, v2

    aput v1, p2, v2

    .line 370
    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    if-ne v1, v3, :cond_0

    .line 371
    invoke-direct {p0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->startImageAlignmentThread()V

    .line 374
    :cond_0
    iget-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    if-eqz v1, :cond_1

    .line 376
    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    if-gt v1, v3, :cond_2

    .line 377
    const/4 v0, 0x0

    .line 381
    .local v0, "s32State":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentVector:Ljava/util/Vector;

    new-instance v2, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;

    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    add-int/lit8 v3, v3, -0x1

    invoke-direct {v2, p0, v3, p1, v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$CustomFeatureClass;-><init>(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;ILjava/lang/String;I)V

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 382
    iget-object v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentRunnable:Ljava/lang/Runnable;

    monitor-enter v2

    .line 383
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageAlignmentRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1}, Ljava/lang/Object;->notify()V

    .line 384
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 386
    .end local v0    # "s32State":I
    :cond_1
    return-void

    .line 379
    :cond_2
    iget-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsType:[I

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    add-int/lit8 v2, v2, -0x2

    aget v0, v1, v2

    .restart local v0    # "s32State":I
    goto :goto_0

    .line 384
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public onStateChanged(I)V
    .locals 3
    .param p1, "msg"    # I

    .prologue
    .line 1296
    const-string v0, "VirtualTourModeler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStateChanged - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1298
    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    const/4 v0, 0x4

    if-gt p1, v0, :cond_0

    .line 1300
    invoke-direct {p0, p1}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->onOrientationChangedEngine(I)V

    .line 1306
    :goto_0
    return-void

    .line 1304
    :cond_0
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->postMessage(II)V

    goto :goto_0
.end method

.method public onUndo()V
    .locals 2

    .prologue
    .line 465
    invoke-static {}, Lcom/sec/android/secvision/solutions/virtualtour/CVirtualTourNative;->SNOnUndoFlagUpdate()V

    .line 466
    iget-object v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mPhotoCountMutexObject:Ljava/lang/Object;

    monitor-enter v1

    .line 467
    :try_start_0
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mUndoPtr:I

    .line 468
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCurrentPhotoCount:I

    .line 469
    monitor-exit v1

    .line 470
    return-void

    .line 469
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public restart(IIIIIFF)V
    .locals 1
    .param p1, "PictureWidth"    # I
    .param p2, "PictureHeight"    # I
    .param p3, "PreviewWidth"    # I
    .param p4, "PreviewHeight"    # I
    .param p5, "ScreenOrientation"    # I
    .param p6, "HorizontalAoV"    # F
    .param p7, "VerticalAoV"    # F

    .prologue
    .line 350
    iput p5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mOrientation:I

    .line 351
    iput p6, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mHorizontalAoV:F

    .line 352
    iput p7, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVerticalAoV:F

    .line 356
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->stopImageAlignmentThread(I)V

    .line 358
    invoke-direct/range {p0 .. p7}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->resetTransitionEstimator(IIIIIFF)V

    .line 359
    return-void
.end method

.method public saveTour(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 8
    .param p1, "TemporaryStorageDirectory"    # Ljava/lang/String;
    .param p2, "DestinationDirectory"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;
    .param p4, "dateTaken"    # J

    .prologue
    .line 406
    invoke-direct {p0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->waitImageSavingThread()V

    .line 407
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mIsTranslationValid:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTranslations:[Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsData:[F

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mTransitionsType:[I

    if-nez v0, :cond_1

    .line 408
    :cond_0
    const-string v0, "VirtualTourModeler"

    const-string v1, "Return because of null pointer"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    :goto_0
    return-void

    .line 411
    :cond_1
    new-instance v7, Ljava/lang/Thread;

    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$2;-><init>(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    invoke-direct {v7, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v7, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageSavingThread:Ljava/lang/Thread;

    .line 456
    iget-object v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mImageSavingThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public setStackStateListener(Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;

    .prologue
    .line 1233
    sput-object p1, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVirtualTourModelerEventListener:Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler$VirtualTourModelerEventListener;

    .line 1234
    return-void
.end method

.method public start(IIIIIIIFF)V
    .locals 9
    .param p1, "PictureWidth"    # I
    .param p2, "PictureHeight"    # I
    .param p3, "PreviewWidth"    # I
    .param p4, "PreviewHeight"    # I
    .param p5, "ScreenOrientation"    # I
    .param p6, "ScreenHeight"    # I
    .param p7, "ScreenWidth"    # I
    .param p8, "HorizontalAoV"    # F
    .param p9, "VerticalAoV"    # F

    .prologue
    .line 320
    iput p5, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mOrientation:I

    .line 321
    iput p6, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mScreenHeight:I

    .line 322
    move/from16 v0, p7

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mScreenWidth:I

    .line 323
    move/from16 v0, p8

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mHorizontalAoV:F

    .line 324
    move/from16 v0, p9

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVerticalAoV:F

    .line 325
    iget v7, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mHorizontalAoV:F

    iget v8, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mVerticalAoV:F

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v8}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->openTransitionEstimator(IIIIIFF)V

    .line 326
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 333
    invoke-direct {p0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->closeTransitionEstimator()V

    .line 335
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->stopImageAlignmentThread(I)V

    .line 337
    invoke-direct {p0}, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->waitImageSavingThread()V

    .line 338
    return-void
.end method

.method public updateCapturingStatus(Z)V
    .locals 3
    .param p1, "Capturing"    # Z

    .prologue
    .line 394
    iput-boolean p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCapturing:Z

    .line 395
    const-string v0, "VirtualTourModeler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mCapturing set to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/VirtualTourModeler;->mCapturing:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    return-void
.end method
