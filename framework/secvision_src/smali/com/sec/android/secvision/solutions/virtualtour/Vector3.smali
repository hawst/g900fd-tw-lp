.class public Lcom/sec/android/secvision/solutions/virtualtour/Vector3;
.super Ljava/lang/Object;
.source "Vector3.java"


# instance fields
.field public x:F

.field public y:F

.field public z:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    .line 18
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0
    .param p1, "nx"    # F
    .param p2, "ny"    # F
    .param p3, "nz"    # F

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    .line 22
    iput p2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    .line 23
    iput p3, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    .line 24
    return-void
.end method


# virtual methods
.method public add(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)V
    .locals 2
    .param p1, "p_oVector"    # Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    iget v1, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    .line 77
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    iget v1, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    .line 78
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    iget v1, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    .line 79
    return-void
.end method

.method public clone()Lcom/sec/android/secvision/solutions/virtualtour/Vector3;
    .locals 4

    .prologue
    .line 37
    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;-><init>(FFF)V

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->clone()Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    move-result-object v0

    return-object v0
.end method

.method public copy(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)V
    .locals 1
    .param p1, "p_oVector"    # Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .prologue
    .line 41
    iget v0, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    .line 42
    iget v0, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    .line 43
    iget v0, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    .line 44
    return-void
.end method

.method public cross(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)Lcom/sec/android/secvision/solutions/virtualtour/Vector3;
    .locals 6
    .param p1, "p_oVector"    # Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .prologue
    .line 102
    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    iget v2, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    iget v3, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    iget v3, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    iget v4, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    iget v4, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    iget v5, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;-><init>(FFF)V

    return-object v0
.end method

.method public dot(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)F
    .locals 3
    .param p1, "p_oVector"    # Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    iget v1, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    iget v2, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    iget v2, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public dotPerp(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)F
    .locals 1
    .param p1, "vB"    # Lcom/sec/android/secvision/solutions/virtualtour/Vector3;
    .param p2, "vN"    # Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .prologue
    .line 98
    invoke-virtual {p0, p1}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->cross(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->dot(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)F

    move-result v0

    return v0
.end method

.method public equals(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)Z
    .locals 2
    .param p1, "p_oVector"    # Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    iget v1, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    iget v1, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    iget v1, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAngle(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)F
    .locals 10
    .param p1, "p_oVector"    # Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .prologue
    const/4 v5, 0x0

    .line 116
    invoke-virtual {p0}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->getNorm()F

    move-result v1

    .line 117
    .local v1, "n1":F
    invoke-virtual {p1}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->getNorm()F

    move-result v2

    .line 118
    .local v2, "n2":F
    mul-float v0, v1, v2

    .line 120
    .local v0, "denom":F
    cmpl-float v6, v0, v5

    if-nez v6, :cond_0

    .line 131
    :goto_0
    return v5

    .line 124
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->dot(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)F

    move-result v6

    div-float v3, v6, v0

    .line 125
    .local v3, "ncos":F
    const/high16 v6, 0x3f800000    # 1.0f

    mul-float v7, v3, v3

    sub-float v4, v6, v7

    .line 126
    .local v4, "sin2":F
    cmpg-float v5, v4, v5

    if-gez v5, :cond_1

    .line 127
    const-string v5, "Vector3::getAngle"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Wrong Sin^2 for Cos: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    const/4 v4, 0x0

    .line 131
    :cond_1
    float-to-double v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    float-to-double v8, v3

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    double-to-float v5, v6

    goto :goto_0
.end method

.method public getAngle(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)F
    .locals 8
    .param p1, "v"    # Lcom/sec/android/secvision/solutions/virtualtour/Vector3;
    .param p2, "vAxis"    # Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v4, -0x40800000    # -1.0f

    .line 135
    invoke-virtual {p2, p0}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->unitCross(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    move-result-object v2

    .line 136
    .local v2, "vN1":Lcom/sec/android/secvision/solutions/virtualtour/Vector3;
    invoke-virtual {p2, p1}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->unitCross(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    move-result-object v3

    .line 138
    .local v3, "vN2":Lcom/sec/android/secvision/solutions/virtualtour/Vector3;
    invoke-virtual {v2, v3}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->dot(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)F

    move-result v0

    .line 139
    .local v0, "fCos":F
    cmpl-float v5, v0, v1

    if-lez v5, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 142
    :cond_0
    :goto_0
    invoke-virtual {v2, v3}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->cross(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    move-result-object v5

    invoke-virtual {v5, p2}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->dot(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)F

    move-result v5

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-lez v5, :cond_2

    .line 144
    .local v1, "fSign":F
    :goto_1
    float-to-double v4, v1

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->acos(D)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v4, v4

    return v4

    .line 140
    .end local v1    # "fSign":F
    :cond_1
    cmpg-float v5, v0, v4

    if-gez v5, :cond_0

    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0

    :cond_2
    move v1, v4

    .line 142
    goto :goto_1
.end method

.method public getNorm()F
    .locals 3

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public getSquaredNorm()F
    .locals 3

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public negate()V
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    neg-float v0, v0

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    .line 52
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    neg-float v0, v0

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    .line 53
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    neg-float v0, v0

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    .line 54
    return-void
.end method

.method public normalize()F
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->getNorm()F

    move-result v0

    .line 66
    .local v0, "norm":F
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-nez v1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v0

    .line 68
    :cond_1
    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    div-float/2addr v1, v0

    iput v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    .line 69
    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    div-float/2addr v1, v0

    iput v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    .line 70
    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    div-float/2addr v1, v0

    iput v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-virtual {p0, v0, v0, v0}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->reset(FFF)V

    .line 28
    return-void
.end method

.method public reset(FFF)V
    .locals 0
    .param p1, "px"    # F
    .param p2, "py"    # F
    .param p3, "pz"    # F

    .prologue
    .line 31
    iput p1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    .line 32
    iput p2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    .line 33
    iput p3, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    .line 34
    return-void
.end method

.method public rotateAxis(FLcom/sec/android/secvision/solutions/virtualtour/Vector3;)V
    .locals 3
    .param p1, "fAngle"    # F
    .param p2, "vAxis"    # Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .prologue
    .line 157
    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;

    invoke-direct {v0}, Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;-><init>()V

    .line 158
    .local v0, "oR":Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;
    const/high16 v1, 0x43340000    # 180.0f

    mul-float/2addr v1, p1

    const v2, 0x40490fdc

    div-float/2addr v1, v2

    invoke-virtual {v0, p2, v1}, Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;->axisRotation(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;F)V

    .line 160
    invoke-virtual {v0, p0}, Lcom/sec/android/secvision/solutions/virtualtour/Matrix4;->vectorMult3x3(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)V

    .line 161
    return-void
.end method

.method public rotateY(F)V
    .locals 6
    .param p1, "fAngle"    # F

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->clone()Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    move-result-object v2

    .line 149
    .local v2, "vW":Lcom/sec/android/secvision/solutions/virtualtour/Vector3;
    float-to-double v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v0, v4

    .line 150
    .local v0, "fCos":F
    float-to-double v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v1, v4

    .line 152
    .local v1, "fSin":F
    iget v3, v2, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    mul-float/2addr v3, v0

    iget v4, v2, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    mul-float/2addr v4, v1

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    .line 153
    iget v3, v2, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    neg-float v3, v3

    mul-float/2addr v3, v1

    iget v4, v2, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    mul-float/2addr v4, v0

    add-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    .line 154
    return-void
.end method

.method public scale(F)V
    .locals 1
    .param p1, "n"    # F

    .prologue
    .line 88
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    .line 89
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    .line 90
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    .line 91
    return-void
.end method

.method public sub(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)V
    .locals 2
    .param p1, "p_oVector"    # Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    iget v1, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    .line 83
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    iget v1, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    .line 84
    iget v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    iget v1, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    .line 85
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->toString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(I)Ljava/lang/String;
    .locals 4
    .param p1, "decPlaces"    # I

    .prologue
    .line 168
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    .line 169
    .local v0, "df":Ljava/text/DecimalFormat;
    invoke-virtual {v0, p1}, Ljava/text/DecimalFormat;->setMaximumFractionDigits(I)V

    .line 171
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    float-to-double v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    float-to-double v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    float-to-double v2, v2

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public unitCross(Lcom/sec/android/secvision/solutions/virtualtour/Vector3;)Lcom/sec/android/secvision/solutions/virtualtour/Vector3;
    .locals 6
    .param p1, "p_oVector"    # Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    .prologue
    .line 108
    new-instance v0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;

    iget v1, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    iget v2, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    iget v3, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    iget v3, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    iget v4, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->z:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    iget v4, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->y:F

    iget v5, p1, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->x:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;-><init>(FFF)V

    .line 111
    .local v0, "vN":Lcom/sec/android/secvision/solutions/virtualtour/Vector3;
    invoke-virtual {v0}, Lcom/sec/android/secvision/solutions/virtualtour/Vector3;->normalize()F

    .line 112
    return-object v0
.end method
