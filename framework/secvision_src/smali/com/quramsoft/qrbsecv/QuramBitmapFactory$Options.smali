.class public Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
.super Ljava/lang/Object;
.source "QuramBitmapFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quramsoft/qrbsecv/QuramBitmapFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Options"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options$Config;,
        Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options$DecodeFromOption;,
        Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options$InputType;
    }
.end annotation


# instance fields
.field public inCancelingRequested:Z

.field public inDecodeFromOption:I

.field public inDither:Z

.field public inInputType:I

.field public inPreferredConfig:I

.field public inQualityOverSpeed:I

.field public inSampleSize:I

.field private mDecodeHandle:I

.field private mExifHandle:I

.field private mHeight:I

.field private mWidth:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1082
    const/4 v0, 0x7

    iput v0, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    .line 1084
    const/4 v0, 0x1

    iput v0, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 1085
    iput-boolean v1, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inDither:Z

    .line 1087
    iput v1, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inQualityOverSpeed:I

    .line 1088
    iput v1, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inInputType:I

    .line 1089
    iput v1, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inDecodeFromOption:I

    .line 1092
    iput v1, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->mDecodeHandle:I

    .line 1093
    iput v1, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->mWidth:I

    .line 1094
    iput v1, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->mHeight:I

    .line 1096
    iput v1, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->mExifHandle:I

    .line 1098
    iput-boolean v1, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inCancelingRequested:Z

    .line 1103
    return-void
.end method

.method static synthetic access$0(Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;I)V
    .locals 0

    .prologue
    .line 1093
    iput p1, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->mWidth:I

    return-void
.end method

.method static synthetic access$1(Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;I)V
    .locals 0

    .prologue
    .line 1094
    iput p1, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->mHeight:I

    return-void
.end method


# virtual methods
.method protected getExif()I
    .locals 1

    .prologue
    .line 1127
    iget v0, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->mExifHandle:I

    return v0
.end method

.method protected getHandle()I
    .locals 1

    .prologue
    .line 1117
    iget v0, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->mDecodeHandle:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 1112
    iget v0, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->mHeight:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 1107
    iget v0, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->mWidth:I

    return v0
.end method

.method protected setExif(I)V
    .locals 0
    .param p1, "handle"    # I

    .prologue
    .line 1132
    iput p1, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->mExifHandle:I

    .line 1133
    return-void
.end method

.method protected setHandle(I)V
    .locals 0
    .param p1, "handle"    # I

    .prologue
    .line 1122
    iput p1, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->mDecodeHandle:I

    .line 1123
    return-void
.end method
