.class public Lcom/quramsoft/qrbsecv/QuramBitmapFactory;
.super Ljava/lang/Object;
.source "QuramBitmapFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;,
        Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field public static final Quram_DECODECANCEL_SUCC:I = 0x6

.field public static final Quram_FAIL:I = 0x0

.field public static final Quram_JPEG:Ljava/lang/String; = "Quram_JPEG"

.field public static final Quram_Progress:I = 0x4

.field public static final Quram_SUCC:I = 0x1

.field private static final TAG:Ljava/lang/String; = "QuramBitmapFactory"

.field protected static final USE_AUTO_FILEMODE:I = 0x0

.field public static final USE_FULLSIZE_BUFFER:I = 0x0

.field public static final USE_ITERSIZE_BUFFER:I = 0x1

.field public static final USE_MAKE_REGIONMAP:I = 0x2

.field protected static final USE_POWER_PROCESS:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    :try_start_0
    const-string v0, "qjpeg_secvision"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    :goto_0
    return-void

    .line 34
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static native AbortJPEGFromFileIter(I)I
.end method

.method public static native CompareJPEG(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Ljava/lang/String;IIJJJI)D
.end method

.method public static native CreateDecodeInfoFromBuffer([BIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I
.end method

.method public static native CreateDecodeInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;I)I
.end method

.method public static native DecodeCancel(I)V
.end method

.method public static native DecodeJPEGFromFile(ILandroid/graphics/Bitmap;III)I
.end method

.method static native DecodeJPEGFromFileIter(ILandroid/graphics/Bitmap;IIII)I
.end method

.method static native DecodeJPEGFromFileIter4LTN(ILandroid/graphics/Bitmap;IIII)I
.end method

.method static native DecodeJPEGFromFileIter4LTNToBuffer(ILjava/nio/Buffer;IIIII)I
.end method

.method static native DecodeJPEGFromFileIterToBuffer(ILjava/nio/Buffer;IIIII)I
.end method

.method static native DecodeJPEGFromFileMultiOutBuf(I[Landroid/graphics/Bitmap;IIII)I
.end method

.method public static native DecodeJPEGThumbnail(ILandroid/graphics/Bitmap;III)I
.end method

.method public static native DecodeJPEGThumbnailToBuffer(ILjava/nio/Buffer;III)I
.end method

.method public static native DecodeJpegFromStream(Ljava/io/InputStream;ILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;[BILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
.end method

.method public static native EncodeJPEG(Landroid/graphics/Bitmap;[BII)I
.end method

.method public static native EncodeJPEGtoFile(Landroid/graphics/Bitmap;Ljava/lang/String;IIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I
.end method

.method public static native EncodeJPEGtoFileFromByte([BLjava/lang/String;IIIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I
.end method

.method public static native GetExifData(Ljava/lang/String;ILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I
.end method

.method public static native GetImageInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I
.end method

.method public static native PDecodeJPEGFromFile(ILandroid/graphics/Bitmap;III)I
.end method

.method public static native PartialDecodeJPEGFromFile(ILandroid/graphics/Bitmap;IIIII)I
.end method

.method public static native PartialDecodeJPEGToBuffer(ILjava/nio/Buffer;IIIII)I
.end method

.method static native PrepareJPEGFromFileIter(III)I
.end method

.method public static native RegionMapCancel(I)V
.end method

.method public static abortFileIter(Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)V
    .locals 1
    .param p0, "options"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    .prologue
    .line 909
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    .line 910
    invoke-virtual {p0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    invoke-static {v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->AbortJPEGFromFileIter(I)I

    .line 911
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    .line 913
    :cond_0
    return-void
.end method

.method public static abortIter(Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)V
    .locals 1
    .param p0, "options"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    .prologue
    .line 901
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    .line 902
    invoke-virtual {p0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    invoke-static {v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->AbortJPEGFromFileIter(I)I

    .line 903
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    .line 905
    :cond_0
    return-void
.end method

.method public static cancelDecode(Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)V
    .locals 1
    .param p0, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    .prologue
    .line 1049
    invoke-virtual {p0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inCancelingRequested:Z

    if-eqz v0, :cond_1

    .line 1053
    :cond_0
    :goto_0
    return-void

    .line 1051
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inCancelingRequested:Z

    .line 1052
    invoke-virtual {p0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    invoke-static {v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeCancel(I)V

    goto :goto_0
.end method

.method public static cancelRegionMap(Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)V
    .locals 1
    .param p0, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    .prologue
    .line 1058
    invoke-virtual {p0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inCancelingRequested:Z

    if-eqz v0, :cond_1

    .line 1063
    :cond_0
    :goto_0
    return-void

    .line 1061
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inCancelingRequested:Z

    .line 1062
    invoke-virtual {p0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    invoke-static {v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->RegionMapCancel(I)V

    goto :goto_0
.end method

.method public static compressToByte(Landroid/graphics/Bitmap;Ljava/lang/String;[BII)I
    .locals 2
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "compressFormat"    # Ljava/lang/String;
    .param p2, "out"    # [B
    .param p3, "out_bufsize"    # I
    .param p4, "quality"    # I

    .prologue
    .line 998
    const/4 v0, 0x0

    .line 999
    .local v0, "ret":I
    const-string v1, "Quram_JPEG"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1002
    invoke-static {p0, p2, p3, p4}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->EncodeJPEG(Landroid/graphics/Bitmap;[BII)I

    move-result v0

    .line 1004
    :cond_0
    return v0
.end method

.method public static compressToFile(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;ILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I
    .locals 7
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "compressFormat"    # Ljava/lang/String;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "quality"    # I
    .param p4, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    .prologue
    .line 1017
    const/4 v6, 0x0

    .line 1018
    .local v6, "ret":I
    const-string v0, "Quram_JPEG"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1021
    const-string v0, "QURAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EncodeJPEGtoFile called - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1022
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    move-object v0, p0

    move-object v1, p2

    move v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->EncodeJPEGtoFile(Landroid/graphics/Bitmap;Ljava/lang/String;IIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I

    move-result v6

    .line 1023
    const-string v0, "QURAM"

    const-string v1, "EncodeJPEGtoFile called complete"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1025
    :cond_0
    return v6
.end method

.method public static compressToFile([BLjava/lang/String;Ljava/lang/String;IIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I
    .locals 8
    .param p0, "data"    # [B
    .param p1, "compressFormat"    # Ljava/lang/String;
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "quality"    # I
    .param p6, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    .prologue
    .line 1030
    const/4 v7, 0x0

    .line 1031
    .local v7, "ret":I
    const-string v0, "Quram_JPEG"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1034
    const-string v0, "QURAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EncodeJPEGtoFile called - "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1037
    const/16 v4, 0x12

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v6}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->EncodeJPEGtoFileFromByte([BLjava/lang/String;IIIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I

    move-result v7

    .line 1038
    const-string v0, "QURAM"

    const-string v1, "EncodeJPEGtoFile called complete"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1040
    :cond_0
    return v7
.end method

.method public static createDecInfo(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I
    .locals 2
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "options"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    .prologue
    const/4 v1, 0x0

    .line 653
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v0, v1

    .line 665
    :cond_1
    :goto_0
    return v0

    .line 656
    :cond_2
    invoke-static {p0, p1, v1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->CreateDecodeInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;I)I

    move-result v0

    .line 661
    .local v0, "ret":I
    if-nez v0, :cond_1

    .line 662
    invoke-virtual {p1, v1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0
.end method

.method public static createDecInfo([BIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I
    .locals 2
    .param p0, "buffer"    # [B
    .param p1, "offset"    # I
    .param p2, "len"    # I
    .param p3, "options"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    .prologue
    const/4 v1, 0x0

    .line 670
    if-eqz p0, :cond_0

    if-nez p3, :cond_2

    :cond_0
    move v0, v1

    .line 679
    :cond_1
    :goto_0
    return v0

    .line 673
    :cond_2
    invoke-static {p0, p1, p2, p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->CreateDecodeInfoFromBuffer([BIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I

    move-result v0

    .line 675
    .local v0, "ret":I
    if-nez v0, :cond_1

    .line 676
    invoke-virtual {p3, v1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0
.end method

.method public static decodeByteArray([BIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 288
    const/4 v2, 0x0

    .line 290
    .local v2, "retBitmap":Landroid/graphics/Bitmap;
    if-gez p1, :cond_1

    .line 337
    :cond_0
    :goto_0
    return-object v4

    .line 295
    :cond_1
    if-lez p2, :cond_0

    .line 300
    array-length v5, p0

    add-int v6, p2, p1

    if-lt v5, v6, :cond_0

    .line 305
    invoke-virtual {p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v5

    if-nez v5, :cond_0

    .line 310
    invoke-static {p0, p1, p2, p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->CreateDecodeInfoFromBuffer([BIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I

    move-result v0

    .line 311
    .local v0, "codecRet":I
    if-nez v0, :cond_2

    .line 312
    invoke-virtual {p3, v7}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 317
    :cond_2
    invoke-virtual {p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getWidth()I

    move-result v3

    .line 318
    .local v3, "width":I
    invoke-virtual {p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHeight()I

    move-result v1

    .line 320
    .local v1, "height":I
    iget v4, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v5, 0x7

    if-ne v4, v5, :cond_4

    .line 321
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v1, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 328
    :goto_1
    invoke-virtual {p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v4

    const/4 v5, 0x1

    invoke-static {v4, v2, v3, v1, v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeJPEGFromFile(ILandroid/graphics/Bitmap;III)I

    move-result v0

    .line 331
    if-nez v0, :cond_3

    .line 333
    const/4 v2, 0x0

    .line 336
    :cond_3
    invoke-virtual {p3, v7}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v4, v2

    .line 337
    goto :goto_0

    .line 323
    :cond_4
    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v1, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_1
.end method

.method public static decodeFile(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 433
    const/4 v1, 0x0

    .line 435
    .local v1, "retBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v5

    if-eqz v5, :cond_1

    .line 488
    :cond_0
    :goto_0
    return-object v4

    .line 442
    :cond_1
    invoke-static {p0, p1, v7}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->CreateDecodeInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;I)I

    move-result v0

    .line 443
    .local v0, "ret":I
    if-nez v0, :cond_2

    .line 445
    invoke-virtual {p1, v7}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 451
    :cond_2
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getWidth()I

    move-result v5

    int-to-float v5, v5

    iget v6, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->round(F)I

    move-result v3

    .line 452
    .local v3, "sampledWidth":I
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHeight()I

    move-result v5

    int-to-float v5, v5

    iget v6, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->round(F)I

    move-result v2

    .line 454
    .local v2, "sampledHeight":I
    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 457
    iget v5, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v6, 0x7

    if-ne v5, v6, :cond_5

    .line 458
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v2, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 462
    :goto_1
    iget v5, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v5, :cond_3

    .line 463
    const/4 v5, 0x1

    iput v5, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 465
    :cond_3
    iget v5, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v6, 0x8

    if-le v5, v6, :cond_6

    .line 467
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v5

    const/4 v6, -0x1

    invoke-static {v5, v1, v3, v2, v6}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeJPEGFromFile(ILandroid/graphics/Bitmap;III)I

    move-result v0

    .line 473
    :goto_2
    if-nez v0, :cond_7

    .line 475
    if-eqz v1, :cond_4

    .line 477
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 478
    const/4 v1, 0x0

    .line 481
    :cond_4
    invoke-virtual {p1, v7}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 460
    :cond_5
    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v2, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1

    .line 471
    :cond_6
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v5

    iget v6, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    invoke-static {v5, v1, v3, v2, v6}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeJPEGFromFile(ILandroid/graphics/Bitmap;III)I

    move-result v0

    goto :goto_2

    .line 487
    :cond_7
    invoke-virtual {p1, v7}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v4, v1

    .line 488
    goto :goto_0
.end method

.method public static decodeFile(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;III)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "origId"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 213
    const/4 v1, 0x0

    .line 215
    .local v1, "retBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v3

    if-eqz v3, :cond_0

    .line 274
    :goto_0
    return-object v2

    .line 222
    :cond_0
    invoke-static {p0, p1, v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->CreateDecodeInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;I)I

    move-result v0

    .line 223
    .local v0, "ret":I
    if-nez v0, :cond_1

    .line 225
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 229
    :cond_1
    iget v3, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v4, 0x7

    if-ne v3, v4, :cond_4

    .line 230
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 234
    :goto_1
    iget v3, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v3, :cond_2

    .line 235
    const/4 v3, 0x1

    iput v3, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 237
    :cond_2
    iget v3, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v4, 0x8

    if-le v3, v4, :cond_5

    .line 239
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v3

    const/4 v4, -0x1

    invoke-static {v3, v1, p2, p3, v4}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeJPEGFromFile(ILandroid/graphics/Bitmap;III)I

    move-result v0

    .line 245
    :goto_2
    if-nez v0, :cond_6

    .line 247
    if-eqz v1, :cond_3

    .line 249
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 250
    const/4 v1, 0x0

    .line 253
    :cond_3
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 232
    :cond_4
    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1

    .line 243
    :cond_5
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v3

    iget v4, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    invoke-static {v3, v1, p2, p3, v4}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeJPEGFromFile(ILandroid/graphics/Bitmap;III)I

    move-result v0

    goto :goto_2

    .line 259
    :cond_6
    const/4 v2, 0x6

    if-ne v0, v2, :cond_7

    .line 263
    if-eqz v1, :cond_7

    .line 265
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 266
    const/4 v1, 0x0

    .line 273
    :cond_7
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v2, v1

    .line 274
    goto :goto_0
.end method

.method public static decodeFileDescriptor(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "fd"    # Ljava/io/FileDescriptor;

    .prologue
    .line 1200
    new-instance v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    invoke-direct {v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;-><init>()V

    .line 1201
    .local v0, "option":Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public static decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 9
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "outpadding"    # Landroid/graphics/Rect;
    .param p2, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    .prologue
    const/4 v8, 0x0

    .line 1206
    const/4 v7, 0x0

    .line 1207
    .local v7, "retBitmap":Landroid/graphics/Bitmap;
    const/16 v1, 0x4000

    new-array v3, v1, [B

    .line 1209
    .local v3, "bytearray":[B
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 1211
    .local v0, "fis":Ljava/io/FileInputStream;
    invoke-virtual {p2}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1213
    const-string v1, "QuramBitmapFactory"

    const-string v2, "option Fail"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1284
    :goto_0
    return-object v8

    .line 1219
    :cond_0
    :try_start_0
    iget v1, p2, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    if-nez v1, :cond_2

    .line 1220
    invoke-virtual {v0}, Ljava/io/FileInputStream;->available()I

    move-result v1

    iget v4, p2, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeJpegFromStream(Ljava/io/InputStream;ILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;[BILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 1273
    :cond_1
    :goto_1
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    .line 1275
    :try_start_1
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v3, v8

    .line 1283
    check-cast v3, [B

    move-object v8, v7

    .line 1284
    goto :goto_0

    .line 1221
    :cond_2
    :try_start_2
    iget v1, p2, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_1

    .line 1222
    invoke-virtual {v0}, Ljava/io/FileInputStream;->available()I

    move-result v1

    iget v4, p2, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeJpegFromStream(Ljava/io/InputStream;ILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;[BILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v7

    goto :goto_1

    .line 1225
    :catch_0
    move-exception v6

    .line 1227
    .local v6, "e":Ljava/lang/Exception;
    const-string v1, "QuramBitmapFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1277
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v6

    .line 1279
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v1, "QuramBitmapFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static decodeFileFromThumbnail(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;III)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "origId"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 59
    const/4 v1, 0x0

    .line 61
    .local v1, "retBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v3

    if-eqz v3, :cond_0

    .line 114
    :goto_0
    return-object v2

    .line 68
    :cond_0
    invoke-static {p0, p1, v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->CreateDecodeInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;I)I

    move-result v0

    .line 69
    .local v0, "ret":I
    if-nez v0, :cond_1

    .line 71
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 75
    :cond_1
    iget v3, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v4, 0x7

    if-ne v3, v4, :cond_4

    .line 76
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 80
    :goto_1
    iget v3, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v3, :cond_2

    .line 81
    iput v6, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 83
    :cond_2
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v3

    invoke-static {v3, v1, p2, p3, v6}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeJPEGThumbnail(ILandroid/graphics/Bitmap;III)I

    move-result v0

    .line 85
    if-nez v0, :cond_5

    .line 87
    if-eqz v1, :cond_3

    .line 89
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 90
    const/4 v1, 0x0

    .line 93
    :cond_3
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 78
    :cond_4
    sget-object v3, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1

    .line 99
    :cond_5
    const/4 v2, 0x6

    if-ne v0, v2, :cond_6

    .line 103
    if-eqz v1, :cond_6

    .line 105
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 106
    const/4 v1, 0x0

    .line 113
    :cond_6
    invoke-virtual {p1, v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v2, v1

    .line 114
    goto :goto_0
.end method

.method public static decodeFileIter(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;Landroid/graphics/Bitmap;IIII)I
    .locals 7
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "options"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    .param p2, "retBitmap"    # Landroid/graphics/Bitmap;
    .param p3, "sampledWidth"    # I
    .param p4, "sampledHeight"    # I
    .param p5, "iterType"    # I
    .param p6, "decodeStep"    # I

    .prologue
    .line 795
    const/4 v6, 0x0

    .line 797
    .local v6, "ret":I
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    .line 798
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 799
    :cond_0
    const/4 v0, 0x0

    .line 837
    :goto_0
    return v0

    .line 802
    :cond_1
    packed-switch p5, :pswitch_data_0

    .line 822
    :goto_1
    :pswitch_0
    if-nez v6, :cond_3

    .line 823
    if-eqz p2, :cond_2

    .line 824
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->recycle()V

    .line 825
    const/4 p2, 0x0

    .line 828
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    .line 831
    const/4 v0, 0x0

    goto :goto_0

    .line 805
    :pswitch_1
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    .line 807
    const/4 v4, 0x0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v5, p6

    .line 804
    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeJPEGFromFileIter(ILandroid/graphics/Bitmap;IIII)I

    move-result v6

    .line 808
    goto :goto_1

    .line 815
    :pswitch_2
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    .line 814
    invoke-static/range {v0 .. v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeJPEGFromFileIter4LTN(ILandroid/graphics/Bitmap;IIII)I

    move-result v6

    goto :goto_1

    .line 833
    :cond_3
    const/4 v0, 0x1

    if-ne v6, v0, :cond_5

    .line 834
    if-eqz p5, :cond_4

    const/4 v0, 0x1

    if-ne p5, v0, :cond_5

    .line 835
    :cond_4
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    :cond_5
    move v0, v6

    .line 837
    goto :goto_0

    .line 802
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static decodeFileIter(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;Ljava/nio/Buffer;IIIII)I
    .locals 8
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "options"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    .param p2, "buffer"    # Ljava/nio/Buffer;
    .param p3, "bufferHeight"    # I
    .param p4, "sampledWidth"    # I
    .param p5, "sampledHeight"    # I
    .param p6, "iterType"    # I
    .param p7, "decodeStep"    # I

    .prologue
    .line 850
    const/4 v7, 0x0

    .line 852
    .local v7, "ret":I
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    .line 853
    if-eqz p2, :cond_0

    if-eqz p4, :cond_0

    if-nez p5, :cond_1

    .line 854
    :cond_0
    const/4 v0, 0x0

    .line 894
    :goto_0
    return v0

    .line 857
    :cond_1
    packed-switch p6, :pswitch_data_0

    .line 879
    :goto_1
    :pswitch_0
    if-nez v7, :cond_4

    .line 880
    if-eqz p2, :cond_2

    .line 882
    const/4 p2, 0x0

    .line 885
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    .line 888
    const/4 v0, 0x0

    goto :goto_0

    .line 860
    :pswitch_1
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    .line 862
    const/4 v5, 0x0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v6, p7

    .line 859
    invoke-static/range {v0 .. v6}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeJPEGFromFileIterToBuffer(ILjava/nio/Buffer;IIIII)I

    move-result v7

    .line 863
    goto :goto_1

    .line 867
    :pswitch_2
    add-int/lit8 v0, p7, 0x2

    if-ne v0, p3, :cond_3

    .line 870
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    .line 869
    invoke-static/range {v0 .. v6}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeJPEGFromFileIter4LTNToBuffer(ILjava/nio/Buffer;IIIII)I

    move-result v7

    goto :goto_1

    .line 874
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 890
    :cond_4
    const/4 v0, 0x1

    if-ne v7, v0, :cond_6

    .line 891
    if-eqz p6, :cond_5

    const/4 v0, 0x1

    if-ne p6, v0, :cond_6

    .line 892
    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    :cond_6
    move v0, v7

    .line 894
    goto :goto_0

    .line 857
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static decodeFileStream(Ljava/io/InputStream;IILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "inputstream"    # Ljava/io/InputStream;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    .prologue
    .line 1390
    invoke-static {p0, p1, p2, p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->decodeStream(Ljava/io/InputStream;IILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeFileToBuffer(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;II)Ljava/nio/ByteBuffer;
    .locals 1
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 130
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->decodeFileToBuffer(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;III)Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public static decodeFileToBuffer(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;III)Ljava/nio/ByteBuffer;
    .locals 7
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "origId"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 135
    const/4 v0, 0x0

    .line 137
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v3

    if-eqz v3, :cond_0

    .line 198
    :goto_0
    return-object v2

    .line 144
    :cond_0
    invoke-static {p0, p1, v6}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->CreateDecodeInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;I)I

    move-result v1

    .line 145
    .local v1, "ret":I
    if-nez v1, :cond_1

    .line 147
    invoke-virtual {p1, v6}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 152
    :cond_1
    iget v3, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v4, 0x7

    if-ne v3, v4, :cond_4

    .line 153
    mul-int v3, p2, p3

    mul-int/lit8 v3, v3, 0x4

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 164
    :goto_1
    sget-object v3, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 166
    iget v3, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v3, :cond_2

    .line 167
    const/4 v3, 0x1

    iput v3, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 169
    :cond_2
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v3

    iget v4, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    invoke-static {v3, v0, p2, p3, v4}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeJPEGThumbnailToBuffer(ILjava/nio/Buffer;III)I

    move-result v1

    .line 171
    if-nez v1, :cond_7

    .line 173
    if-eqz v0, :cond_3

    .line 175
    const/4 v0, 0x0

    .line 178
    :cond_3
    invoke-virtual {p1, v6}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 154
    :cond_4
    iget v3, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    if-nez v3, :cond_5

    .line 155
    mul-int v3, p2, p3

    mul-int/lit8 v3, v3, 0x2

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_1

    .line 156
    :cond_5
    iget v3, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    .line 157
    mul-int v3, p2, p3

    add-int/lit8 v4, p2, 0x1

    shr-int/lit8 v4, v4, 0x1

    add-int/lit8 v5, p3, 0x1

    shr-int/lit8 v5, v5, 0x1

    mul-int/2addr v4, v5

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_1

    .line 160
    :cond_6
    invoke-virtual {p1, v6}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0

    .line 184
    :cond_7
    const/4 v2, 0x6

    if-ne v1, v2, :cond_8

    .line 188
    if-eqz v0, :cond_8

    .line 190
    const/4 v0, 0x0

    .line 197
    :cond_8
    invoke-virtual {p1, v6}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v2, v0

    .line 198
    goto :goto_0
.end method

.method public static decodeIter(Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;Landroid/graphics/Bitmap;IIII)I
    .locals 7
    .param p0, "options"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    .param p1, "retBitmap"    # Landroid/graphics/Bitmap;
    .param p2, "sampledWidth"    # I
    .param p3, "sampledHeight"    # I
    .param p4, "iterType"    # I
    .param p5, "decodeStep"    # I

    .prologue
    .line 765
    const/4 v0, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-static/range {v0 .. v6}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->decodeFileIter(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;Landroid/graphics/Bitmap;IIII)I

    move-result v0

    return v0
.end method

.method public static decodeIter(Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;Ljava/nio/Buffer;IIIII)I
    .locals 8
    .param p0, "options"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    .param p1, "buffer"    # Ljava/nio/Buffer;
    .param p2, "bufferHeight"    # I
    .param p3, "sampledWidth"    # I
    .param p4, "sampledHeight"    # I
    .param p5, "iterType"    # I
    .param p6, "decodeStep"    # I

    .prologue
    .line 777
    const/4 v0, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->decodeFileIter(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;Ljava/nio/Buffer;IIIII)I

    move-result v0

    return v0
.end method

.method public static decodeStream(Ljava/io/InputStream;IILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "inputstream"    # Ljava/io/InputStream;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    .prologue
    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 1290
    const/4 v4, 0x0

    .local v4, "retBitmap":Landroid/graphics/Bitmap;
    move-object v0, v5

    .line 1291
    check-cast v0, [B

    .line 1293
    .local v0, "data":[B
    if-nez p0, :cond_0

    .line 1295
    const-string v6, "QuramBitmapFactory"

    const-string v7, "inputstream is null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1385
    :goto_0
    return-object v5

    .line 1302
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->available()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1310
    .local v2, "len":I
    :goto_1
    if-gtz v2, :cond_1

    .line 1312
    const-string v6, "QuramBitmapFactory"

    const-string v7, "inpustream open fail"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1304
    .end local v2    # "len":I
    :catch_0
    move-exception v1

    .line 1306
    .local v1, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    .line 1307
    .restart local v2    # "len":I
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1316
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    new-array v0, v2, [B

    .line 1317
    if-nez v0, :cond_2

    .line 1319
    const-string v6, "QuramBitmapFactory"

    const-string v7, "data alloc failed"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1325
    :cond_2
    :try_start_1
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1333
    :goto_2
    invoke-virtual {p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v6

    if-eqz v6, :cond_3

    .line 1335
    const-string v6, "QuramBitmapFactory"

    const-string v7, "option Fail"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1327
    :catch_1
    move-exception v1

    .restart local v1    # "e":Ljava/io/IOException;
    move-object v0, v5

    .line 1329
    check-cast v0, [B

    .line 1330
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 1339
    .end local v1    # "e":Ljava/io/IOException;
    :cond_3
    invoke-static {v0, v9, v2, p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->CreateDecodeInfoFromBuffer([BIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I

    move-result v3

    .line 1340
    .local v3, "ret":I
    if-nez v3, :cond_4

    .line 1342
    invoke-virtual {p3, v9}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    .line 1343
    const-string v6, "IET"

    const-string v7, "Create Decode Info Failed"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1350
    :cond_4
    iget v6, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v7, 0x7

    if-ne v6, v7, :cond_7

    .line 1351
    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 1355
    :goto_3
    iget v6, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v6, :cond_5

    .line 1356
    const/4 v6, 0x1

    iput v6, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 1366
    :cond_5
    const-string v6, "IET"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "decode jpeg with w = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", h = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1367
    invoke-virtual {p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v6

    iget v7, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    invoke-static {v6, v4, p1, p2, v7}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeJPEGFromFile(ILandroid/graphics/Bitmap;III)I

    move-result v3

    .line 1371
    if-nez v3, :cond_8

    .line 1373
    if-eqz v4, :cond_6

    .line 1375
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 1376
    const/4 v4, 0x0

    .line 1379
    :cond_6
    invoke-virtual {p3, v9}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    .line 1380
    const-string v6, "QuramBitmapFactory"

    const-string v7, "Decode Fail!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1353
    :cond_7
    sget-object v6, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    goto :goto_3

    .line 1384
    :cond_8
    invoke-virtual {p3, v9}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v5, v4

    .line 1385
    goto/16 :goto_0
.end method

.method public static decodeThumbnailByteArrayToBuffer([BIILcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I
    .locals 9
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "output"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;
    .param p4, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    .prologue
    const/4 v5, 0x0

    .line 358
    const/4 v2, 0x1

    .line 360
    .local v2, "ret":I
    if-gez p1, :cond_1

    .line 421
    :cond_0
    :goto_0
    return v5

    .line 365
    :cond_1
    if-lez p2, :cond_0

    .line 370
    array-length v6, p0

    add-int v7, p2, p1

    if-lt v6, v7, :cond_0

    .line 375
    if-eqz p3, :cond_0

    .line 380
    invoke-virtual {p4}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v6

    if-nez v6, :cond_0

    .line 385
    invoke-static {p0, p1, p2, p4}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->CreateDecodeInfoFromBuffer([BIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I

    move-result v2

    .line 386
    if-eqz v2, :cond_5

    .line 387
    invoke-virtual {p4}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getWidth()I

    move-result v4

    .line 388
    .local v4, "width":I
    invoke-virtual {p4}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHeight()I

    move-result v1

    .line 390
    .local v1, "height":I
    iget v6, p4, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v7, 0x7

    if-ne v6, v7, :cond_6

    .line 391
    const/high16 v0, 0x40800000    # 4.0f

    .line 401
    .local v0, "bpp":F
    :goto_1
    iget v6, p4, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v6, :cond_2

    .line 402
    const/4 v6, 0x1

    iput v6, p4, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 404
    :cond_2
    iput v4, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;->width:I

    .line 405
    iput v1, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;->height:I

    .line 406
    mul-int v6, v4, v1

    int-to-float v6, v6

    mul-float/2addr v6, v0

    float-to-int v3, v6

    .line 408
    .local v3, "size":I
    iget-object v6, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    if-eqz v6, :cond_3

    iget-object v6, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->limit()I

    move-result v6

    if-ge v6, v3, :cond_4

    .line 409
    :cond_3
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v6

    iput-object v6, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    .line 412
    :cond_4
    invoke-virtual {p4}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v6

    iget-object v7, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    iget v8, p4, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    invoke-static {v6, v7, v4, v1, v8}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->DecodeJPEGThumbnailToBuffer(ILjava/nio/Buffer;III)I

    move-result v2

    .line 413
    if-nez v2, :cond_5

    .line 420
    .end local v0    # "bpp":F
    .end local v1    # "height":I
    .end local v3    # "size":I
    .end local v4    # "width":I
    :cond_5
    invoke-virtual {p4, v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    move v5, v2

    .line 421
    goto :goto_0

    .line 392
    .restart local v1    # "height":I
    .restart local v4    # "width":I
    :cond_6
    iget v6, p4, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    if-nez v6, :cond_7

    .line 393
    const/high16 v0, 0x40000000    # 2.0f

    .restart local v0    # "bpp":F
    goto :goto_1

    .line 394
    .end local v0    # "bpp":F
    :cond_7
    iget v6, p4, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_8

    .line 395
    const/high16 v0, 0x3fc00000    # 1.5f

    .restart local v0    # "bpp":F
    goto :goto_1

    .line 397
    .end local v0    # "bpp":F
    :cond_8
    invoke-virtual {p4, v5}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0
.end method

.method public static getExifData(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I
    .locals 3
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "options"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    .prologue
    const/4 v1, 0x0

    .line 684
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move v0, v1

    .line 693
    :cond_1
    :goto_0
    return v0

    .line 687
    :cond_2
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v2

    invoke-static {p0, v2, p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->GetExifData(Ljava/lang/String;ILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I

    move-result v0

    .line 689
    .local v0, "ret":I
    if-nez v0, :cond_1

    .line 690
    invoke-virtual {p1, v1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setExif(I)V

    goto :goto_0
.end method

.method public static partialDecodeByteArray([BIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;IIII)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    .param p4, "left"    # I
    .param p5, "right"    # I
    .param p6, "top"    # I
    .param p7, "bottom"    # I

    .prologue
    .line 500
    const/4 v1, 0x0

    .line 501
    .local v1, "retBitmap":Landroid/graphics/Bitmap;
    iget v10, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 502
    .local v10, "sampleSize":I
    const/4 v11, 0x0

    .line 503
    .local v11, "width":I
    const/4 v8, 0x0

    .line 505
    .local v8, "height":I
    if-gez p1, :cond_0

    .line 507
    const/4 v0, 0x0

    .line 567
    :goto_0
    return-object v0

    .line 510
    :cond_0
    if-gtz p2, :cond_1

    .line 512
    const/4 v0, 0x0

    goto :goto_0

    .line 515
    :cond_1
    array-length v0, p0

    add-int v2, p2, p1

    if-ge v0, v2, :cond_2

    .line 517
    const/4 v0, 0x0

    goto :goto_0

    .line 520
    :cond_2
    invoke-virtual {p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_3

    .line 522
    const/4 v0, 0x0

    goto :goto_0

    .line 525
    :cond_3
    invoke-static {p0, p1, p2, p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->CreateDecodeInfoFromBuffer([BIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I

    move-result v9

    .line 526
    .local v9, "ret":I
    if-nez v9, :cond_4

    .line 527
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    .line 529
    const/4 v0, 0x0

    goto :goto_0

    .line 532
    :cond_4
    iget v0, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v0, :cond_7

    .line 533
    const/4 v0, 0x1

    iput v0, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 537
    :cond_5
    :goto_1
    sub-int v0, p5, p4

    iget v2, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v11, v0, v2

    .line 538
    sub-int v0, p7, p6

    iget v2, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v8, v0, v2

    .line 540
    iget v0, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v2, 0x7

    if-ne v0, v2, :cond_8

    .line 541
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v8, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 546
    :goto_2
    invoke-virtual {p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    sub-int v4, p5, p4

    sub-int v5, p7, p6

    iget v6, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    move/from16 v2, p4

    move/from16 v3, p6

    invoke-static/range {v0 .. v6}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->PartialDecodeJPEGFromFile(ILandroid/graphics/Bitmap;IIIII)I

    move-result v9

    .line 548
    if-nez v9, :cond_9

    .line 549
    if-eqz v1, :cond_6

    .line 550
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 551
    const/4 v1, 0x0

    .line 554
    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    .line 556
    const/4 v0, 0x0

    goto :goto_0

    .line 534
    :cond_7
    iget v0, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v2, 0x8

    if-le v0, v2, :cond_5

    .line 535
    const/16 v0, 0x8

    iput v0, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    goto :goto_1

    .line 543
    :cond_8
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v8, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_2

    .line 559
    :cond_9
    iget v0, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    if-ge v0, v10, :cond_a

    .line 560
    iget v0, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v0, v11

    div-int/2addr v0, v10

    iget v2, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v2, v8

    div-int/2addr v2, v10

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 561
    .local v7, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 562
    move-object v1, v7

    .line 563
    iput v10, p3, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 566
    .end local v7    # "bm":Landroid/graphics/Bitmap;
    :cond_a
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v0, v1

    .line 567
    goto/16 :goto_0
.end method

.method public static partialDecodeByteArrayToBuffer([BIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;IIIILcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;)I
    .locals 13
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    .param p4, "left"    # I
    .param p5, "right"    # I
    .param p6, "top"    # I
    .param p7, "bottom"    # I
    .param p8, "output"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;

    .prologue
    .line 581
    const/4 v10, 0x1

    .line 583
    .local v10, "ret":I
    if-gez p1, :cond_0

    .line 585
    const/4 v1, 0x0

    .line 648
    :goto_0
    return v1

    .line 588
    :cond_0
    if-gtz p2, :cond_1

    .line 590
    const/4 v1, 0x0

    goto :goto_0

    .line 593
    :cond_1
    array-length v1, p0

    add-int v2, p2, p1

    if-ge v1, v2, :cond_2

    .line 595
    const/4 v1, 0x0

    goto :goto_0

    .line 598
    :cond_2
    invoke-virtual/range {p3 .. p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v1

    if-eqz v1, :cond_3

    .line 600
    const/4 v1, 0x0

    goto :goto_0

    .line 603
    :cond_3
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v2, 0x8

    if-gt v1, v2, :cond_4

    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    if-gez v1, :cond_5

    .line 605
    :cond_4
    const/4 v1, 0x0

    goto :goto_0

    .line 608
    :cond_5
    if-eqz p8, :cond_6

    move-object/from16 v0, p8

    iget-object v1, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    if-nez v1, :cond_7

    .line 610
    :cond_6
    const/4 v1, 0x0

    goto :goto_0

    .line 613
    :cond_7
    invoke-static/range {p0 .. p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->CreateDecodeInfoFromBuffer([BIILcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;)I

    move-result v10

    .line 614
    const/4 v1, 0x1

    if-ne v10, v1, :cond_a

    .line 615
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v1, :cond_b

    .line 616
    const/4 v1, 0x1

    move-object/from16 v0, p3

    iput v1, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 620
    :cond_8
    :goto_1
    sub-int v1, p5, p4

    move-object/from16 v0, p3

    iget v2, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v12, v1, v2

    .line 621
    .local v12, "width":I
    sub-int v1, p7, p6

    move-object/from16 v0, p3

    iget v2, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v9, v1, v2

    .line 623
    .local v9, "height":I
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_c

    .line 624
    const/high16 v8, 0x40800000    # 4.0f

    .line 634
    .local v8, "bpp":F
    :goto_2
    move-object/from16 v0, p8

    iput v12, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;->width:I

    .line 635
    move-object/from16 v0, p8

    iput v9, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;->height:I

    .line 636
    mul-int v1, v12, v9

    int-to-float v1, v1

    mul-float/2addr v1, v8

    float-to-int v11, v1

    .line 638
    .local v11, "size":I
    move-object/from16 v0, p8

    iget-object v1, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    if-ge v1, v11, :cond_9

    .line 639
    invoke-static {v11}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    move-object/from16 v0, p8

    iput-object v1, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    .line 642
    :cond_9
    invoke-virtual/range {p3 .. p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v1

    move-object/from16 v0, p8

    iget-object v2, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$ImageBufferData;->buffer:Ljava/nio/ByteBuffer;

    sub-int v5, p5, p4

    sub-int v6, p7, p6

    move-object/from16 v0, p3

    iget v7, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    move/from16 v3, p4

    move/from16 v4, p6

    invoke-static/range {v1 .. v7}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->PartialDecodeJPEGToBuffer(ILjava/nio/Buffer;IIIII)I

    move-result v10

    .line 647
    .end local v8    # "bpp":F
    .end local v9    # "height":I
    .end local v11    # "size":I
    .end local v12    # "width":I
    :cond_a
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    move v1, v10

    .line 648
    goto/16 :goto_0

    .line 617
    :cond_b
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v2, 0x8

    if-le v1, v2, :cond_8

    .line 618
    const/16 v1, 0x8

    move-object/from16 v0, p3

    iput v1, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    goto :goto_1

    .line 625
    .restart local v9    # "height":I
    .restart local v12    # "width":I
    :cond_c
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    if-nez v1, :cond_d

    .line 626
    const/high16 v8, 0x40000000    # 2.0f

    .restart local v8    # "bpp":F
    goto :goto_2

    .line 627
    .end local v8    # "bpp":F
    :cond_d
    move-object/from16 v0, p3

    iget v1, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_e

    .line 628
    const/high16 v8, 0x3fc00000    # 1.5f

    .restart local v8    # "bpp":F
    goto :goto_2

    .line 630
    .end local v8    # "bpp":F
    :cond_e
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    .line 631
    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public static partialDecodeFile(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;IIII)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "option"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    .param p2, "left"    # I
    .param p3, "right"    # I
    .param p4, "top"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 924
    const/4 v1, 0x0

    .line 925
    .local v1, "retBitmap":Landroid/graphics/Bitmap;
    iget v10, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 926
    .local v10, "sampleSize":I
    const/4 v11, 0x0

    .line 927
    .local v11, "width":I
    const/4 v8, 0x0

    .line 929
    .local v8, "height":I
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    if-eqz v0, :cond_0

    .line 933
    const/4 v0, 0x0

    .line 984
    :goto_0
    return-object v0

    .line 936
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->CreateDecodeInfoFromFile(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;I)I

    move-result v9

    .line 937
    .local v9, "ret":I
    if-nez v9, :cond_1

    .line 939
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    .line 942
    const/4 v0, 0x0

    goto :goto_0

    .line 945
    :cond_1
    iget v0, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    if-nez v0, :cond_4

    .line 946
    const/4 v0, 0x1

    iput v0, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 950
    :cond_2
    :goto_1
    sub-int v0, p3, p2

    iget v2, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v11, v0, v2

    .line 951
    sub-int v0, p5, p4

    iget v2, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    div-int v8, v0, v2

    .line 953
    iget v0, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    const/4 v2, 0x7

    if-ne v0, v2, :cond_5

    .line 954
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v8, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 959
    :goto_2
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v0

    sub-int v4, p3, p2

    sub-int v5, p5, p4

    iget v6, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    move v2, p2

    move/from16 v3, p4

    invoke-static/range {v0 .. v6}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->PartialDecodeJPEGFromFile(ILandroid/graphics/Bitmap;IIIII)I

    move-result v9

    .line 961
    if-nez v9, :cond_6

    .line 963
    if-eqz v1, :cond_3

    .line 965
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 966
    const/4 v1, 0x0

    .line 969
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    .line 972
    const/4 v0, 0x0

    goto :goto_0

    .line 947
    :cond_4
    iget v0, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    const/16 v2, 0x8

    if-le v0, v2, :cond_2

    .line 948
    const/16 v0, 0x8

    iput v0, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    goto :goto_1

    .line 956
    :cond_5
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v11, v8, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_2

    .line 975
    :cond_6
    iget v0, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    if-ge v0, v10, :cond_7

    .line 977
    iget v0, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v0, v11

    div-int/2addr v0, v10

    iget v2, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    mul-int/2addr v2, v8

    div-int/2addr v2, v10

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 978
    .local v7, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 979
    move-object v1, v7

    .line 980
    iput v10, p1, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 983
    .end local v7    # "bm":Landroid/graphics/Bitmap;
    :cond_7
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    move-object v0, v1

    .line 984
    goto :goto_0
.end method

.method public static prepareDecodeFileIter(Ljava/lang/String;Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;II)I
    .locals 3
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "options"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    .param p2, "decWidth"    # I
    .param p3, "decHeight"    # I

    .prologue
    const/4 v1, 0x0

    .line 706
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v2

    if-eqz v2, :cond_0

    .line 707
    if-lez p2, :cond_0

    if-gtz p3, :cond_2

    :cond_0
    move v0, v1

    .line 720
    :cond_1
    :goto_0
    return v0

    .line 713
    :cond_2
    invoke-virtual {p1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v2

    invoke-static {v2, p2, p3}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->PrepareJPEGFromFileIter(III)I

    move-result v0

    .line 715
    .local v0, "ret":I
    if-nez v0, :cond_1

    .line 716
    invoke-virtual {p1, v1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0
.end method

.method public static prepareDecodeIter(Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;II)I
    .locals 3
    .param p0, "options"    # Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    .param p1, "decWidth"    # I
    .param p2, "decHeight"    # I

    .prologue
    const/4 v1, 0x0

    .line 732
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v2

    if-eqz v2, :cond_0

    .line 733
    if-lez p1, :cond_0

    if-gtz p2, :cond_2

    :cond_0
    move v0, v1

    .line 746
    :cond_1
    :goto_0
    return v0

    .line 739
    :cond_2
    invoke-virtual {p0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->getHandle()I

    move-result v2

    invoke-static {v2, p1, p2}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory;->PrepareJPEGFromFileIter(III)I

    move-result v0

    .line 741
    .local v0, "ret":I
    if-nez v0, :cond_1

    .line 742
    invoke-virtual {p0, v1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->setHandle(I)V

    goto :goto_0
.end method

.method public static round(F)I
    .locals 1
    .param p0, "val"    # F

    .prologue
    .line 44
    invoke-static {p0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static translateBitmapFactoryOptions(Landroid/graphics/BitmapFactory$Options;)Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    .locals 4
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    const/4 v3, 0x7

    .line 1177
    new-instance v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;

    invoke-direct {v0}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;-><init>()V

    .line 1178
    .local v0, "qrbOptions":Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;
    iget-object v1, p0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v1, v2, :cond_0

    .line 1180
    iput v3, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    .line 1191
    :goto_0
    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iput v1, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inSampleSize:I

    .line 1192
    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v0, v1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->access$0(Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;I)V

    .line 1193
    iget v1, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v0, v1}, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->access$1(Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;I)V

    .line 1195
    return-object v0

    .line 1182
    :cond_0
    iget-object v1, p0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-ne v1, v2, :cond_1

    .line 1184
    const/4 v1, 0x0

    iput v1, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    goto :goto_0

    .line 1188
    :cond_1
    iput v3, v0, Lcom/quramsoft/qrbsecv/QuramBitmapFactory$Options;->inPreferredConfig:I

    goto :goto_0
.end method
