.class public Lcom/sec/dmc/smartux/ConfMaker;
.super Ljava/lang/Object;
.source "ConfMaker.java"


# static fields
.field static final PATH_CONFIG:Ljava/lang/String; = "/data/DMCSmartUXSettings.txt"


# instance fields
.field public Reserved:I

.field public Sensitivity:I

.field public StayTime:I

.field public downSensitivity:I

.field public upSensitivity:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput v0, p0, Lcom/sec/dmc/smartux/ConfMaker;->upSensitivity:I

    .line 24
    iput v0, p0, Lcom/sec/dmc/smartux/ConfMaker;->downSensitivity:I

    .line 25
    iput v0, p0, Lcom/sec/dmc/smartux/ConfMaker;->Sensitivity:I

    .line 26
    iput v0, p0, Lcom/sec/dmc/smartux/ConfMaker;->StayTime:I

    .line 27
    iput v0, p0, Lcom/sec/dmc/smartux/ConfMaker;->Reserved:I

    return-void
.end method


# virtual methods
.method public Load()Z
    .locals 9

    .prologue
    .line 34
    const/4 v0, 0x0

    .local v0, "bResult1":Z
    const/4 v1, 0x0

    .line 36
    .local v1, "bResult2":Z
    new-instance v5, Ljava/io/File;

    const-string v7, "/data/DMCSmartUXSettings.txt"

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 38
    .local v5, "src":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 39
    const/4 v2, 0x0

    .line 41
    .local v2, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/FileReader;

    invoke-direct {v7, v5}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    .end local v2    # "br":Ljava/io/BufferedReader;
    .local v3, "br":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 46
    .local v6, "strTemp":Ljava/lang/String;
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/dmc/smartux/ConfMaker;->upSensitivity:I

    .line 48
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 49
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/dmc/smartux/ConfMaker;->downSensitivity:I

    .line 51
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 52
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/dmc/smartux/ConfMaker;->Sensitivity:I

    .line 54
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 55
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/sec/dmc/smartux/ConfMaker;->StayTime:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 57
    const/4 v0, 0x1

    .line 64
    if-eqz v3, :cond_0

    .line 66
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 72
    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v6    # "strTemp":Ljava/lang/String;
    :cond_0
    :goto_0
    return v0

    .line 58
    .restart local v2    # "br":Ljava/io/BufferedReader;
    :catch_0
    move-exception v4

    .line 61
    .local v4, "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v7, "ConfMaker"

    const-string v8, "couldnt load"

    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    const/4 v0, 0x0

    .line 64
    if-eqz v2, :cond_0

    .line 66
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 67
    :catch_1
    move-exception v7

    goto :goto_0

    .line 64
    .end local v4    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v7

    :goto_2
    if-eqz v2, :cond_1

    .line 66
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 67
    :cond_1
    :goto_3
    throw v7

    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    .restart local v6    # "strTemp":Ljava/lang/String;
    :catch_2
    move-exception v7

    goto :goto_0

    .end local v3    # "br":Ljava/io/BufferedReader;
    .end local v6    # "strTemp":Ljava/lang/String;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    :catch_3
    move-exception v8

    goto :goto_3

    .line 64
    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v7

    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    goto :goto_2

    .line 58
    .end local v2    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "br":Ljava/io/BufferedReader;
    :catch_4
    move-exception v4

    move-object v2, v3

    .end local v3    # "br":Ljava/io/BufferedReader;
    .restart local v2    # "br":Ljava/io/BufferedReader;
    goto :goto_1
.end method
