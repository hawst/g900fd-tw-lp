.class public Lcom/sec/dmc/smartux/DMCresponse;
.super Ljava/lang/Object;
.source "DMCresponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;,
        Lcom/sec/dmc/smartux/DMCresponse$States;
    }
.end annotation


# instance fields
.field private DetectionState:Lcom/sec/dmc/smartux/DMCresponse$States;

.field private guideDirection:Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

.field private mChangeSensorStatus:I

.field private mFaceDistance:I

.field private mIsPlayState:I

.field private mValueX:I

.field private mValueY:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/dmc/smartux/DMCresponse;->mChangeSensorStatus:I

    return-void
.end method


# virtual methods
.method public getChangeSensorStatus()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/dmc/smartux/DMCresponse;->mChangeSensorStatus:I

    return v0
.end method

.method public getFaceDistance()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/sec/dmc/smartux/DMCresponse;->mFaceDistance:I

    return v0
.end method

.method public getGuide()Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/dmc/smartux/DMCresponse;->guideDirection:Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    return-object v0
.end method

.method public getPlayState()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/sec/dmc/smartux/DMCresponse;->mIsPlayState:I

    return v0
.end method

.method public getResponseX()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/dmc/smartux/DMCresponse;->mValueX:I

    return v0
.end method

.method public getResponseY()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/dmc/smartux/DMCresponse;->mValueY:I

    return v0
.end method

.method public getState()Lcom/sec/dmc/smartux/DMCresponse$States;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/dmc/smartux/DMCresponse;->DetectionState:Lcom/sec/dmc/smartux/DMCresponse$States;

    return-object v0
.end method

.method public setChangeSensorStatus(I)V
    .locals 0
    .param p1, "_status"    # I

    .prologue
    .line 109
    iput p1, p0, Lcom/sec/dmc/smartux/DMCresponse;->mChangeSensorStatus:I

    .line 110
    return-void
.end method

.method public setFaceDistance(I)V
    .locals 0
    .param p1, "distance"    # I

    .prologue
    .line 81
    iput p1, p0, Lcom/sec/dmc/smartux/DMCresponse;->mFaceDistance:I

    .line 82
    return-void
.end method

.method public setGuide(Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;)V
    .locals 0
    .param p1, "newGuide"    # Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/dmc/smartux/DMCresponse;->guideDirection:Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    .line 96
    return-void
.end method

.method public setPlayState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/sec/dmc/smartux/DMCresponse;->mIsPlayState:I

    .line 72
    return-void
.end method

.method public setResponse(II)V
    .locals 0
    .param p1, "_valueX"    # I
    .param p2, "_valueY"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/sec/dmc/smartux/DMCresponse;->mValueX:I

    .line 100
    iput p2, p0, Lcom/sec/dmc/smartux/DMCresponse;->mValueY:I

    .line 101
    return-void
.end method

.method public setState(Lcom/sec/dmc/smartux/DMCresponse$States;)V
    .locals 0
    .param p1, "newState"    # Lcom/sec/dmc/smartux/DMCresponse$States;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/dmc/smartux/DMCresponse;->DetectionState:Lcom/sec/dmc/smartux/DMCresponse$States;

    .line 58
    return-void
.end method
