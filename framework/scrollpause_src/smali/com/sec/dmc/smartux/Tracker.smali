.class public Lcom/sec/dmc/smartux/Tracker;
.super Ljava/lang/Object;
.source "Tracker.java"


# static fields
.field private static final COLOR_FORMAT_NV21:I = 0x0

.field public static final FACEMODE_HYBRID:I = 0x1000

.field public static final FACEMODE_SMARTMOTION:I = 0x100

.field public static final FACEMODE_SMARTNONE:I = 0x0

.field public static final FACEMODE_SMARTPAUSE:I = 0x10

.field public static final FACEMODE_SMARTSCROLL:I = 0x1

.field private static final FRAMESKIP:I = 0x8

.field public static final SCROLLINIT_INITIALIZING:I = 0x2

.field public static final SCROLLINIT_NOFACE:I = 0x0

.field public static final SCROLLINIT_NOTFIXEDFACE:I = 0x1

.field public static final SCROLLINIT_TRACKING:I = 0x3

.field private static final TAG:Ljava/lang/String; = "Tracker"

.field private static cntState:I

.field private static mColorFormat:I

.field private static mFrameCnt:I


# instance fields
.field private bInitTracker:Z

.field private bSensorChanged:Z

.field private configTunable:Lcom/sec/dmc/smartux/DMCConfTunable;

.field private currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

.field private mSensorMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    sput v0, Lcom/sec/dmc/smartux/Tracker;->mColorFormat:I

    .line 39
    sput v0, Lcom/sec/dmc/smartux/Tracker;->cntState:I

    .line 44
    sput v0, Lcom/sec/dmc/smartux/Tracker;->mFrameCnt:I

    .line 300
    const-string v0, "dmcSmartUX"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 303
    const-string v0, "CMLads"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 304
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-boolean v0, p0, Lcom/sec/dmc/smartux/Tracker;->bInitTracker:Z

    .line 41
    iput v1, p0, Lcom/sec/dmc/smartux/Tracker;->mSensorMode:I

    .line 42
    iput-boolean v0, p0, Lcom/sec/dmc/smartux/Tracker;->bSensorChanged:Z

    .line 49
    iput v1, p0, Lcom/sec/dmc/smartux/Tracker;->mSensorMode:I

    .line 50
    iput-boolean v0, p0, Lcom/sec/dmc/smartux/Tracker;->bSensorChanged:Z

    .line 51
    sput v0, Lcom/sec/dmc/smartux/Tracker;->mFrameCnt:I

    .line 53
    iput-boolean v0, p0, Lcom/sec/dmc/smartux/Tracker;->bInitTracker:Z

    .line 54
    return-void
.end method

.method public constructor <init>(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-boolean v0, p0, Lcom/sec/dmc/smartux/Tracker;->bInitTracker:Z

    .line 41
    iput v1, p0, Lcom/sec/dmc/smartux/Tracker;->mSensorMode:I

    .line 42
    iput-boolean v0, p0, Lcom/sec/dmc/smartux/Tracker;->bSensorChanged:Z

    .line 59
    iput v1, p0, Lcom/sec/dmc/smartux/Tracker;->mSensorMode:I

    .line 60
    iput-boolean v0, p0, Lcom/sec/dmc/smartux/Tracker;->bSensorChanged:Z

    .line 61
    sput v0, Lcom/sec/dmc/smartux/Tracker;->mFrameCnt:I

    .line 63
    iput-boolean v0, p0, Lcom/sec/dmc/smartux/Tracker;->bInitTracker:Z

    .line 65
    invoke-virtual {p0, p1, p2}, Lcom/sec/dmc/smartux/Tracker;->startTracker(II)V

    .line 66
    return-void
.end method

.method public static native getCountTH()I
.end method

.method public static native initlibrary(II)Z
.end method

.method public static synchronized native declared-synchronized processImage([BLcom/sec/dmc/smartux/FaceInfo;IIII)I
.end method

.method public static native render([BII)I
.end method

.method public static native scrollpause()I
.end method

.method public static native scrollresume()I
.end method

.method public static native setCountTH(I)I
.end method

.method public static native settuningparams(IIIIIIIFF)V
.end method

.method public static native start(IIIIIII)I
.end method

.method public static native stop()I
.end method

.method public static native terminatelibrary()V
.end method

.method private updateGuideDirection(IIIII)Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    .locals 2
    .param p1, "_needClose"    # I
    .param p2, "_moveLeft"    # I
    .param p3, "_moveRight"    # I
    .param p4, "_moveUp"    # I
    .param p5, "_moveDown"    # I

    .prologue
    .line 192
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    sget-object v0, Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;->GUIDE_DIRECTION_CLOSE:Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    .line 222
    :goto_0
    return-object v0

    .line 196
    :cond_0
    if-lez p5, :cond_3

    .line 197
    if-lez p2, :cond_1

    .line 198
    sget-object v0, Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;->GUIDE_DIRECTION_LEFT_DOWN:Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    .local v0, "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    goto :goto_0

    .line 199
    .end local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    :cond_1
    if-lez p3, :cond_2

    .line 200
    sget-object v0, Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;->GUIDE_DIRECTION_RIGHT_DOWN:Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    .restart local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    goto :goto_0

    .line 202
    .end local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    :cond_2
    sget-object v0, Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;->GUIDE_DIRECTION_DOWN:Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    .restart local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    goto :goto_0

    .line 204
    .end local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    :cond_3
    if-lez p4, :cond_6

    .line 205
    if-lez p2, :cond_4

    .line 206
    sget-object v0, Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;->GUIDE_DIRECTION_LEFT_UP:Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    .restart local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    goto :goto_0

    .line 207
    .end local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    :cond_4
    if-lez p3, :cond_5

    .line 208
    sget-object v0, Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;->GUIDE_DIRECTION_RIGHT_UP:Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    .restart local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    goto :goto_0

    .line 210
    .end local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    :cond_5
    sget-object v0, Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;->GUIDE_DIRECTION_UP:Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    .restart local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    goto :goto_0

    .line 213
    .end local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    :cond_6
    if-lez p2, :cond_7

    .line 214
    sget-object v0, Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;->GUIDE_DIRECTION_LEFT:Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    .restart local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    goto :goto_0

    .line 215
    .end local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    :cond_7
    if-lez p3, :cond_8

    .line 216
    sget-object v0, Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;->GUIDE_DIRECTION_RIGHT:Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    .restart local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    goto :goto_0

    .line 218
    .end local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    :cond_8
    sget-object v0, Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;->GUIDE_DIRECTION_OK:Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    .restart local v0    # "newGuide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    goto :goto_0
.end method


# virtual methods
.method public frameUpdate([BIIII)Lcom/sec/dmc/smartux/DMCresponse;
    .locals 17
    .param p1, "data"    # [B
    .param p2, "orient"    # I
    .param p3, "boundaryState"    # I
    .param p4, "iscamland"    # I
    .param p5, "mode"    # I

    .prologue
    .line 133
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/dmc/smartux/Tracker;->bInitTracker:Z

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 188
    :goto_0
    return-object v2

    .line 135
    :cond_0
    sget-object v10, Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;->GUIDE_DIRECTION_OK:Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    .line 137
    .local v10, "guide":Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;
    const/16 v16, 0x0

    .line 139
    .local v16, "sensorStatus":I
    new-instance v3, Lcom/sec/dmc/smartux/FaceInfo;

    invoke-direct {v3}, Lcom/sec/dmc/smartux/FaceInfo;-><init>()V

    .line 140
    .local v3, "mFaceInfo":Lcom/sec/dmc/smartux/FaceInfo;
    new-instance v2, Lcom/sec/dmc/smartux/DMCresponse;

    invoke-direct {v2}, Lcom/sec/dmc/smartux/DMCresponse;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    .line 142
    if-eqz p5, :cond_2

    .line 143
    and-int/lit8 v2, p5, 0x1

    if-nez v2, :cond_1

    const/16 p3, 0x0

    .line 144
    :cond_1
    const-string v2, "Tracker"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "boundaryState: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v2, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v7, p5

    .line 145
    invoke-static/range {v2 .. v7}, Lcom/sec/dmc/smartux/Tracker;->processImage([BLcom/sec/dmc/smartux/FaceInfo;IIII)I

    move-result v12

    .line 146
    .local v12, "nState":I
    invoke-virtual {v3}, Lcom/sec/dmc/smartux/FaceInfo;->getVideoState()I

    move-result v13

    .line 147
    .local v13, "nVideoState":I
    invoke-virtual {v3}, Lcom/sec/dmc/smartux/FaceInfo;->getYScrollState()I

    move-result v15

    .line 148
    .local v15, "nYScrollState":I
    invoke-virtual {v3}, Lcom/sec/dmc/smartux/FaceInfo;->getXScrollState()I

    move-result v14

    .line 149
    .local v14, "nXScrollState":I
    invoke-virtual {v3}, Lcom/sec/dmc/smartux/FaceInfo;->getFaceDistance()I

    move-result v11

    .line 151
    .local v11, "nFaceDistance":I
    packed-switch v12, :pswitch_data_0

    .line 168
    const-string v2, "Tracker"

    const-string v4, "Something wrong."

    invoke-static {v2, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v12}, Lcom/sec/dmc/smartux/Tracker;->sceneDetect([BI)I

    move-result v16

    .line 173
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/sec/dmc/smartux/DMCresponse;->setChangeSensorStatus(I)V

    .line 175
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    invoke-virtual {v2, v10}, Lcom/sec/dmc/smartux/DMCresponse;->setGuide(Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;)V

    .line 176
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    invoke-virtual {v2, v14, v15}, Lcom/sec/dmc/smartux/DMCresponse;->setResponse(II)V

    .line 177
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    invoke-virtual {v2, v13}, Lcom/sec/dmc/smartux/DMCresponse;->setPlayState(I)V

    .line 178
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    invoke-virtual {v2, v11}, Lcom/sec/dmc/smartux/DMCresponse;->setFaceDistance(I)V

    .line 188
    .end local v11    # "nFaceDistance":I
    .end local v12    # "nState":I
    .end local v13    # "nVideoState":I
    .end local v14    # "nXScrollState":I
    .end local v15    # "nYScrollState":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    goto/16 :goto_0

    .line 154
    .restart local v11    # "nFaceDistance":I
    .restart local v12    # "nState":I
    .restart local v13    # "nVideoState":I
    .restart local v14    # "nXScrollState":I
    .restart local v15    # "nYScrollState":I
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    sget-object v4, Lcom/sec/dmc/smartux/DMCresponse$States;->DETECT_IN_PROGRESS:Lcom/sec/dmc/smartux/DMCresponse$States;

    invoke-virtual {v2, v4}, Lcom/sec/dmc/smartux/DMCresponse;->setState(Lcom/sec/dmc/smartux/DMCresponse$States;)V

    .line 155
    invoke-virtual {v3}, Lcom/sec/dmc/smartux/FaceInfo;->getMakeSmallerFace()I

    move-result v5

    invoke-virtual {v3}, Lcom/sec/dmc/smartux/FaceInfo;->getMoveLeftValue()I

    move-result v6

    invoke-virtual {v3}, Lcom/sec/dmc/smartux/FaceInfo;->getMoveRightValue()I

    move-result v7

    invoke-virtual {v3}, Lcom/sec/dmc/smartux/FaceInfo;->getMoveUpValue()I

    move-result v8

    invoke-virtual {v3}, Lcom/sec/dmc/smartux/FaceInfo;->getMoveDownValue()I

    move-result v9

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/sec/dmc/smartux/Tracker;->updateGuideDirection(IIIII)Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    move-result-object v10

    .line 157
    goto :goto_1

    .line 159
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    sget-object v4, Lcom/sec/dmc/smartux/DMCresponse$States;->TRACKING:Lcom/sec/dmc/smartux/DMCresponse$States;

    invoke-virtual {v2, v4}, Lcom/sec/dmc/smartux/DMCresponse;->setState(Lcom/sec/dmc/smartux/DMCresponse$States;)V

    goto :goto_1

    .line 162
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    sget-object v4, Lcom/sec/dmc/smartux/DMCresponse$States;->NO_DETECTION:Lcom/sec/dmc/smartux/DMCresponse$States;

    invoke-virtual {v2, v4}, Lcom/sec/dmc/smartux/DMCresponse;->setState(Lcom/sec/dmc/smartux/DMCresponse$States;)V

    goto :goto_1

    .line 165
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    sget-object v4, Lcom/sec/dmc/smartux/DMCresponse$States;->SHAKING:Lcom/sec/dmc/smartux/DMCresponse$States;

    invoke-virtual {v2, v4}, Lcom/sec/dmc/smartux/DMCresponse;->setState(Lcom/sec/dmc/smartux/DMCresponse$States;)V

    goto :goto_1

    .line 181
    .end local v11    # "nFaceDistance":I
    .end local v12    # "nState":I
    .end local v13    # "nVideoState":I
    .end local v14    # "nXScrollState":I
    .end local v15    # "nYScrollState":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    sget-object v4, Lcom/sec/dmc/smartux/DMCresponse$States;->NO_DETECTION:Lcom/sec/dmc/smartux/DMCresponse$States;

    invoke-virtual {v2, v4}, Lcom/sec/dmc/smartux/DMCresponse;->setState(Lcom/sec/dmc/smartux/DMCresponse$States;)V

    .line 182
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/sec/dmc/smartux/DMCresponse;->setChangeSensorStatus(I)V

    .line 183
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    invoke-virtual {v2, v10}, Lcom/sec/dmc/smartux/DMCresponse;->setGuide(Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;)V

    .line 184
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lcom/sec/dmc/smartux/DMCresponse;->setResponse(II)V

    .line 185
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Lcom/sec/dmc/smartux/DMCresponse;->setPlayState(I)V

    .line 186
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/dmc/smartux/Tracker;->currentResponse:Lcom/sec/dmc/smartux/DMCresponse;

    const/4 v4, -0x1

    invoke-virtual {v2, v4}, Lcom/sec/dmc/smartux/DMCresponse;->setFaceDistance(I)V

    goto :goto_2

    .line 151
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public pauseTracker()V
    .locals 0

    .prologue
    .line 118
    invoke-static {}, Lcom/sec/dmc/smartux/Tracker;->scrollpause()I

    .line 119
    return-void
.end method

.method public resumeTracker()V
    .locals 0

    .prologue
    .line 123
    invoke-static {}, Lcom/sec/dmc/smartux/Tracker;->scrollresume()I

    .line 124
    return-void
.end method

.method public sceneDetect([BI)I
    .locals 5
    .param p1, "data"    # [B
    .param p2, "state"    # I

    .prologue
    const/4 v4, 0x0

    .line 235
    const/4 v0, 0x0

    .line 261
    .local v0, "ret":I
    sget v2, Lcom/sec/dmc/smartux/Tracker;->mFrameCnt:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/sec/dmc/smartux/Tracker;->mFrameCnt:I

    .line 262
    sget v2, Lcom/sec/dmc/smartux/Tracker;->mFrameCnt:I

    const v3, 0xf4240

    rem-int/2addr v2, v3

    sput v2, Lcom/sec/dmc/smartux/Tracker;->mFrameCnt:I

    .line 265
    iget-boolean v2, p0, Lcom/sec/dmc/smartux/Tracker;->bSensorChanged:Z

    if-eqz v2, :cond_0

    sget v2, Lcom/sec/dmc/smartux/Tracker;->cntState:I

    const/16 v3, 0x8

    if-ge v2, v3, :cond_0

    .line 267
    sget v2, Lcom/sec/dmc/smartux/Tracker;->cntState:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/sec/dmc/smartux/Tracker;->cntState:I

    move v1, v0

    .line 284
    .end local v0    # "ret":I
    .local v1, "ret":I
    :goto_0
    return v1

    .line 271
    .end local v1    # "ret":I
    .restart local v0    # "ret":I
    :cond_0
    iget-boolean v2, p0, Lcom/sec/dmc/smartux/Tracker;->bSensorChanged:Z

    if-eqz v2, :cond_1

    .line 272
    iput-boolean v4, p0, Lcom/sec/dmc/smartux/Tracker;->bSensorChanged:Z

    .line 273
    sput v4, Lcom/sec/dmc/smartux/Tracker;->cntState:I

    .line 276
    :cond_1
    if-nez p2, :cond_2

    .line 278
    iget v2, p0, Lcom/sec/dmc/smartux/Tracker;->mSensorMode:I

    sget v3, Lcom/sec/dmc/smartux/Tracker;->mFrameCnt:I

    invoke-static {p1, v2, v3}, Lcom/sec/dmc/smartux/Tracker;->render([BII)I

    move-result v0

    .line 279
    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 280
    const-string v2, "Tracker"

    const-string v3, "Error during detecting scene mode."

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move v1, v0

    .line 284
    .end local v0    # "ret":I
    .restart local v1    # "ret":I
    goto :goto_0
.end method

.method public setSensorChanged(I)V
    .locals 1
    .param p1, "sensormode"    # I

    .prologue
    .line 289
    if-lez p1, :cond_0

    const/4 v0, 0x3

    if-ge p1, v0, :cond_0

    .line 290
    iput p1, p0, Lcom/sec/dmc/smartux/Tracker;->mSensorMode:I

    .line 291
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/dmc/smartux/Tracker;->bSensorChanged:Z

    .line 292
    const/4 v0, 0x0

    sput v0, Lcom/sec/dmc/smartux/Tracker;->cntState:I

    .line 294
    :cond_0
    return-void
.end method

.method public startTracker(II)V
    .locals 11
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 70
    const/4 v8, 0x0

    .line 71
    .local v8, "ucTunable":Lcom/sec/dmc/smartux/DMCConfTunable;
    const/4 v7, 0x0

    .line 73
    .local v7, "cm":Lcom/sec/dmc/smartux/ConfMaker;
    new-instance v7, Lcom/sec/dmc/smartux/ConfMaker;

    .end local v7    # "cm":Lcom/sec/dmc/smartux/ConfMaker;
    invoke-direct {v7}, Lcom/sec/dmc/smartux/ConfMaker;-><init>()V

    .line 76
    .restart local v7    # "cm":Lcom/sec/dmc/smartux/ConfMaker;
    invoke-virtual {v7}, Lcom/sec/dmc/smartux/ConfMaker;->Load()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    new-instance v8, Lcom/sec/dmc/smartux/DMCConfTunable;

    .end local v8    # "ucTunable":Lcom/sec/dmc/smartux/DMCConfTunable;
    iget v0, v7, Lcom/sec/dmc/smartux/ConfMaker;->upSensitivity:I

    iget v1, v7, Lcom/sec/dmc/smartux/ConfMaker;->downSensitivity:I

    iget v2, v7, Lcom/sec/dmc/smartux/ConfMaker;->Sensitivity:I

    iget v3, v7, Lcom/sec/dmc/smartux/ConfMaker;->StayTime:I

    invoke-direct {v8, v0, v1, v2, v3}, Lcom/sec/dmc/smartux/DMCConfTunable;-><init>(IIII)V

    .line 90
    .restart local v8    # "ucTunable":Lcom/sec/dmc/smartux/DMCConfTunable;
    :goto_0
    iput-object v8, p0, Lcom/sec/dmc/smartux/Tracker;->configTunable:Lcom/sec/dmc/smartux/DMCConfTunable;

    .line 93
    sget v2, Lcom/sec/dmc/smartux/Tracker;->mColorFormat:I

    iget-object v0, p0, Lcom/sec/dmc/smartux/Tracker;->configTunable:Lcom/sec/dmc/smartux/DMCConfTunable;

    iget v3, v0, Lcom/sec/dmc/smartux/DMCConfTunable;->upSensitivity:I

    iget-object v0, p0, Lcom/sec/dmc/smartux/Tracker;->configTunable:Lcom/sec/dmc/smartux/DMCConfTunable;

    iget v4, v0, Lcom/sec/dmc/smartux/DMCConfTunable;->downSensitivity:I

    iget-object v0, p0, Lcom/sec/dmc/smartux/Tracker;->configTunable:Lcom/sec/dmc/smartux/DMCConfTunable;

    iget v5, v0, Lcom/sec/dmc/smartux/DMCConfTunable;->Sensitivity:I

    iget-object v0, p0, Lcom/sec/dmc/smartux/Tracker;->configTunable:Lcom/sec/dmc/smartux/DMCConfTunable;

    iget v6, v0, Lcom/sec/dmc/smartux/DMCConfTunable;->StayTime:I

    move v0, p1

    move v1, p2

    invoke-static/range {v0 .. v6}, Lcom/sec/dmc/smartux/Tracker;->start(IIIIIII)I

    .line 98
    invoke-static {p1, p2}, Lcom/sec/dmc/smartux/Tracker;->initlibrary(II)Z

    .line 99
    iput v10, p0, Lcom/sec/dmc/smartux/Tracker;->mSensorMode:I

    .line 100
    iput-boolean v9, p0, Lcom/sec/dmc/smartux/Tracker;->bSensorChanged:Z

    .line 101
    sput v9, Lcom/sec/dmc/smartux/Tracker;->mFrameCnt:I

    .line 103
    iput-boolean v10, p0, Lcom/sec/dmc/smartux/Tracker;->bInitTracker:Z

    .line 104
    return-void

    .line 82
    :cond_0
    new-instance v8, Lcom/sec/dmc/smartux/DMCConfTunable;

    .end local v8    # "ucTunable":Lcom/sec/dmc/smartux/DMCConfTunable;
    invoke-direct {v8, v9, v9, v9, v9}, Lcom/sec/dmc/smartux/DMCConfTunable;-><init>(IIII)V

    .restart local v8    # "ucTunable":Lcom/sec/dmc/smartux/DMCConfTunable;
    goto :goto_0
.end method

.method public stopTracker()V
    .locals 1

    .prologue
    .line 108
    invoke-static {}, Lcom/sec/dmc/smartux/Tracker;->stop()I

    .line 111
    invoke-static {}, Lcom/sec/dmc/smartux/Tracker;->terminatelibrary()V

    .line 113
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/dmc/smartux/Tracker;->bInitTracker:Z

    .line 114
    return-void
.end method
