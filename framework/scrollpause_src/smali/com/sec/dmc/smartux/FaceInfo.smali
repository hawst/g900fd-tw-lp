.class public Lcom/sec/dmc/smartux/FaceInfo;
.super Ljava/lang/Object;
.source "FaceInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/dmc/smartux/FaceInfo$Face;
    }
.end annotation


# instance fields
.field public Status:I

.field private face:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/sec/dmc/smartux/FaceInfo$Face;",
            ">;"
        }
    .end annotation
.end field

.field private faceDistance:I

.field public fx:F

.field public fy:F

.field public fz:F

.field private isplaystate:I

.field private makeSmallerFace:I

.field private motionState:I

.field private moveDown:I

.field private moveLeft:I

.field private moveRight:I

.field private moveUp:I

.field public nbFace:I

.field public posestate:I

.field private scrollX:I

.field private scrollY:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/sec/dmc/smartux/FaceInfo;->face:Ljava/util/Vector;

    .line 86
    return-void
.end method


# virtual methods
.method public addFaceNum(I)V
    .locals 0
    .param p1, "faceNum"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/sec/dmc/smartux/FaceInfo;->nbFace:I

    .line 57
    return-void
.end method

.method public addFacePose(IFFF)V
    .locals 0
    .param p1, "nstate"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "z"    # F

    .prologue
    .line 38
    iput p1, p0, Lcom/sec/dmc/smartux/FaceInfo;->posestate:I

    .line 39
    iput p2, p0, Lcom/sec/dmc/smartux/FaceInfo;->fx:F

    .line 40
    iput p3, p0, Lcom/sec/dmc/smartux/FaceInfo;->fy:F

    .line 41
    iput p4, p0, Lcom/sec/dmc/smartux/FaceInfo;->fz:F

    .line 42
    return-void
.end method

.method public addFaceeye(IIIIIIII)V
    .locals 2
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I
    .param p5, "leftx"    # I
    .param p6, "lefty"    # I
    .param p7, "rightx"    # I
    .param p8, "righty"    # I

    .prologue
    .line 46
    iget-object v1, p0, Lcom/sec/dmc/smartux/FaceInfo;->face:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    .line 47
    new-instance v0, Lcom/sec/dmc/smartux/FaceInfo$Face;

    invoke-direct {v0, p0}, Lcom/sec/dmc/smartux/FaceInfo$Face;-><init>(Lcom/sec/dmc/smartux/FaceInfo;)V

    .line 48
    .local v0, "tmp":Lcom/sec/dmc/smartux/FaceInfo$Face;
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, p1, p2, p3, p4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, v0, Lcom/sec/dmc/smartux/FaceInfo$Face;->faceRect:Landroid/graphics/Rect;

    .line 49
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, p5, p6}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, v0, Lcom/sec/dmc/smartux/FaceInfo$Face;->lefteyePoint:Landroid/graphics/Point;

    .line 50
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, p7, p8}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, v0, Lcom/sec/dmc/smartux/FaceInfo$Face;->righteyePoint:Landroid/graphics/Point;

    .line 51
    iget-object v1, p0, Lcom/sec/dmc/smartux/FaceInfo;->face:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 52
    return-void
.end method

.method public addMotionState(IIIIII)V
    .locals 0
    .param p1, "motion"    # I
    .param p2, "makesmallerface"    # I
    .param p3, "moveleft"    # I
    .param p4, "moveright"    # I
    .param p5, "moveup"    # I
    .param p6, "movedown"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/sec/dmc/smartux/FaceInfo;->motionState:I

    .line 73
    iput p3, p0, Lcom/sec/dmc/smartux/FaceInfo;->moveLeft:I

    .line 74
    iput p4, p0, Lcom/sec/dmc/smartux/FaceInfo;->moveRight:I

    .line 75
    iput p5, p0, Lcom/sec/dmc/smartux/FaceInfo;->moveUp:I

    .line 76
    iput p6, p0, Lcom/sec/dmc/smartux/FaceInfo;->moveDown:I

    .line 77
    iput p2, p0, Lcom/sec/dmc/smartux/FaceInfo;->makeSmallerFace:I

    .line 78
    return-void
.end method

.method public addPauseState(II)V
    .locals 0
    .param p1, "isPlay"    # I
    .param p2, "distance"    # I

    .prologue
    .line 82
    iput p1, p0, Lcom/sec/dmc/smartux/FaceInfo;->isplaystate:I

    .line 83
    iput p2, p0, Lcom/sec/dmc/smartux/FaceInfo;->faceDistance:I

    .line 84
    return-void
.end method

.method public addScrollState(IIIIIII)V
    .locals 0
    .param p1, "xState"    # I
    .param p2, "yState"    # I
    .param p3, "makesmallerface"    # I
    .param p4, "moveleft"    # I
    .param p5, "moveright"    # I
    .param p6, "moveup"    # I
    .param p7, "movedown"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/dmc/smartux/FaceInfo;->scrollX:I

    .line 62
    iput p2, p0, Lcom/sec/dmc/smartux/FaceInfo;->scrollY:I

    .line 63
    iput p4, p0, Lcom/sec/dmc/smartux/FaceInfo;->moveLeft:I

    .line 64
    iput p5, p0, Lcom/sec/dmc/smartux/FaceInfo;->moveRight:I

    .line 65
    iput p6, p0, Lcom/sec/dmc/smartux/FaceInfo;->moveUp:I

    .line 66
    iput p7, p0, Lcom/sec/dmc/smartux/FaceInfo;->moveDown:I

    .line 67
    iput p3, p0, Lcom/sec/dmc/smartux/FaceInfo;->makeSmallerFace:I

    .line 68
    return-void
.end method

.method public getFaceDistance()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/sec/dmc/smartux/FaceInfo;->faceDistance:I

    return v0
.end method

.method public getMakeSmallerFace()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/sec/dmc/smartux/FaceInfo;->makeSmallerFace:I

    return v0
.end method

.method public getMotionState()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/sec/dmc/smartux/FaceInfo;->motionState:I

    return v0
.end method

.method public getMoveDownValue()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/sec/dmc/smartux/FaceInfo;->moveDown:I

    return v0
.end method

.method public getMoveLeftValue()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/sec/dmc/smartux/FaceInfo;->moveLeft:I

    return v0
.end method

.method public getMoveRightValue()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/sec/dmc/smartux/FaceInfo;->moveRight:I

    return v0
.end method

.method public getMoveUpValue()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/sec/dmc/smartux/FaceInfo;->moveUp:I

    return v0
.end method

.method public getVideoState()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/sec/dmc/smartux/FaceInfo;->isplaystate:I

    return v0
.end method

.method public getXScrollState()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/sec/dmc/smartux/FaceInfo;->scrollX:I

    return v0
.end method

.method public getYScrollState()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/dmc/smartux/FaceInfo;->scrollY:I

    return v0
.end method

.method public init()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    iget-object v0, p0, Lcom/sec/dmc/smartux/FaceInfo;->face:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/dmc/smartux/FaceInfo;->fz:F

    iput v0, p0, Lcom/sec/dmc/smartux/FaceInfo;->fy:F

    iput v0, p0, Lcom/sec/dmc/smartux/FaceInfo;->fx:F

    .line 32
    iput v1, p0, Lcom/sec/dmc/smartux/FaceInfo;->posestate:I

    .line 33
    iput v1, p0, Lcom/sec/dmc/smartux/FaceInfo;->nbFace:I

    .line 34
    return-void
.end method
