.class public final enum Lcom/sec/dmc/smartux/DMCresponse$States;
.super Ljava/lang/Enum;
.source "DMCresponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/dmc/smartux/DMCresponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "States"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/dmc/smartux/DMCresponse$States;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/dmc/smartux/DMCresponse$States;

.field public static final enum DETECT_IN_PROGRESS:Lcom/sec/dmc/smartux/DMCresponse$States;

.field public static final enum NO_DETECTION:Lcom/sec/dmc/smartux/DMCresponse$States;

.field public static final enum SHAKING:Lcom/sec/dmc/smartux/DMCresponse$States;

.field public static final enum TRACKING:Lcom/sec/dmc/smartux/DMCresponse$States;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15
    new-instance v0, Lcom/sec/dmc/smartux/DMCresponse$States;

    const-string v1, "DETECT_IN_PROGRESS"

    invoke-direct {v0, v1, v2}, Lcom/sec/dmc/smartux/DMCresponse$States;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/dmc/smartux/DMCresponse$States;->DETECT_IN_PROGRESS:Lcom/sec/dmc/smartux/DMCresponse$States;

    .line 16
    new-instance v0, Lcom/sec/dmc/smartux/DMCresponse$States;

    const-string v1, "TRACKING"

    invoke-direct {v0, v1, v3}, Lcom/sec/dmc/smartux/DMCresponse$States;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/dmc/smartux/DMCresponse$States;->TRACKING:Lcom/sec/dmc/smartux/DMCresponse$States;

    .line 17
    new-instance v0, Lcom/sec/dmc/smartux/DMCresponse$States;

    const-string v1, "NO_DETECTION"

    invoke-direct {v0, v1, v4}, Lcom/sec/dmc/smartux/DMCresponse$States;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/dmc/smartux/DMCresponse$States;->NO_DETECTION:Lcom/sec/dmc/smartux/DMCresponse$States;

    .line 18
    new-instance v0, Lcom/sec/dmc/smartux/DMCresponse$States;

    const-string v1, "SHAKING"

    invoke-direct {v0, v1, v5}, Lcom/sec/dmc/smartux/DMCresponse$States;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/dmc/smartux/DMCresponse$States;->SHAKING:Lcom/sec/dmc/smartux/DMCresponse$States;

    .line 14
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/dmc/smartux/DMCresponse$States;

    sget-object v1, Lcom/sec/dmc/smartux/DMCresponse$States;->DETECT_IN_PROGRESS:Lcom/sec/dmc/smartux/DMCresponse$States;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/dmc/smartux/DMCresponse$States;->TRACKING:Lcom/sec/dmc/smartux/DMCresponse$States;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/dmc/smartux/DMCresponse$States;->NO_DETECTION:Lcom/sec/dmc/smartux/DMCresponse$States;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/dmc/smartux/DMCresponse$States;->SHAKING:Lcom/sec/dmc/smartux/DMCresponse$States;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/dmc/smartux/DMCresponse$States;->$VALUES:[Lcom/sec/dmc/smartux/DMCresponse$States;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/dmc/smartux/DMCresponse$States;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/sec/dmc/smartux/DMCresponse$States;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/dmc/smartux/DMCresponse$States;

    return-object v0
.end method

.method public static values()[Lcom/sec/dmc/smartux/DMCresponse$States;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/dmc/smartux/DMCresponse$States;->$VALUES:[Lcom/sec/dmc/smartux/DMCresponse$States;

    invoke-virtual {v0}, [Lcom/sec/dmc/smartux/DMCresponse$States;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/dmc/smartux/DMCresponse$States;

    return-object v0
.end method
