.class public Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;
.super Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
.source "SprObjectShapePath.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$1;,
        Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$ExtractFloatResult;,
        Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    }
.end annotation


# static fields
.field public static final TYPE_BEZIER_CURVETO:B = 0x4t

.field public static final TYPE_CLOSE:B = 0x6t

.field public static final TYPE_ELLIPTICAL_ARC:B = 0x5t

.field public static final TYPE_LINETO:B = 0x2t

.field public static final TYPE_MOVETO:B = 0x1t

.field public static final TYPE_NONE:B = 0x0t

.field public static final TYPE_QUADRATIC_CURVETO:B = 0x3t


# instance fields
.field public final mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;

.field public mPathInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;",
            ">;"
        }
    .end annotation
.end field

.field public path:Landroid/graphics/Path;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;-><init>(B)V

    .line 63
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    .line 65
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    .line 69
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;

    .line 71
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    .line 73
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V
    .locals 2
    .param p1, "in"    # Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 76
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;-><init>(B)V

    .line 63
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    .line 65
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    .line 77
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;

    .line 79
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    .line 81
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 2
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 85
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;-><init>(B)V

    .line 63
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    .line 65
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    .line 86
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;

    .line 88
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    .line 90
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->fromXml(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 91
    return-void
.end method

.method private addCommand([FCC[F)V
    .locals 22
    .param p1, "current"    # [F
    .param p2, "previousCmd"    # C
    .param p3, "cmd"    # C
    .param p4, "val"    # [F

    .prologue
    .line 681
    const/16 v20, 0x2

    .line 682
    .local v20, "incr":I
    const/4 v3, 0x0

    aget v18, p1, v3

    .line 683
    .local v18, "currentX":F
    const/4 v3, 0x1

    aget v19, p1, v3

    .line 684
    .local v19, "currentY":F
    const/4 v3, 0x2

    aget v16, p1, v3

    .line 685
    .local v16, "ctrlPointX":F
    const/4 v3, 0x3

    aget v17, p1, v3

    .line 689
    .local v17, "ctrlPointY":F
    sparse-switch p3, :sswitch_data_0

    .line 723
    :goto_0
    const/16 v21, 0x0

    .local v21, "k":I
    :goto_1
    move-object/from16 v0, p4

    array-length v3, v0

    move/from16 v0, v21

    if-ge v0, v3, :cond_c

    .line 724
    sparse-switch p3, :sswitch_data_1

    .line 867
    :goto_2
    move/from16 p2, p3

    .line 723
    add-int v21, v21, v20

    goto :goto_1

    .line 692
    .end local v21    # "k":I
    :sswitch_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->close()V

    .line 873
    :goto_3
    return-void

    .line 700
    :sswitch_1
    const/16 v20, 0x2

    .line 701
    goto :goto_0

    .line 706
    :sswitch_2
    const/16 v20, 0x1

    .line 707
    goto :goto_0

    .line 710
    :sswitch_3
    const/16 v20, 0x6

    .line 711
    goto :goto_0

    .line 716
    :sswitch_4
    const/16 v20, 0x4

    .line 717
    goto :goto_0

    .line 720
    :sswitch_5
    const/16 v20, 0x7

    goto :goto_0

    .line 726
    .restart local v21    # "k":I
    :sswitch_6
    add-int/lit8 v3, v21, 0x0

    aget v3, p4, v3

    add-float v18, v18, v3

    .line 727
    add-int/lit8 v3, v21, 0x1

    aget v3, p4, v3

    add-float v19, v19, v3

    .line 729
    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->moveTo(FF)V

    goto :goto_2

    .line 732
    :sswitch_7
    add-int/lit8 v3, v21, 0x0

    aget v18, p4, v3

    .line 733
    add-int/lit8 v3, v21, 0x1

    aget v19, p4, v3

    .line 735
    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->moveTo(FF)V

    goto :goto_2

    .line 738
    :sswitch_8
    add-int/lit8 v3, v21, 0x0

    aget v3, p4, v3

    add-float v18, v18, v3

    .line 739
    add-int/lit8 v3, v21, 0x1

    aget v3, p4, v3

    add-float v19, v19, v3

    .line 740
    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->lineTo(FF)V

    goto :goto_2

    .line 743
    :sswitch_9
    add-int/lit8 v3, v21, 0x0

    aget v18, p4, v3

    .line 744
    add-int/lit8 v3, v21, 0x1

    aget v19, p4, v3

    .line 745
    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->lineTo(FF)V

    goto :goto_2

    .line 749
    :sswitch_a
    add-int/lit8 v3, v21, 0x0

    aget v3, p4, v3

    add-float v18, v18, v3

    .line 750
    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->lineTo(FF)V

    goto :goto_2

    .line 753
    :sswitch_b
    add-int/lit8 v3, v21, 0x0

    aget v18, p4, v3

    .line 754
    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->lineTo(FF)V

    goto/16 :goto_2

    .line 757
    :sswitch_c
    add-int/lit8 v3, v21, 0x0

    aget v3, p4, v3

    add-float v19, v19, v3

    .line 758
    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->lineTo(FF)V

    goto/16 :goto_2

    .line 761
    :sswitch_d
    add-int/lit8 v3, v21, 0x0

    aget v19, p4, v3

    .line 762
    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->lineTo(FF)V

    goto/16 :goto_2

    .line 765
    :sswitch_e
    add-int/lit8 v3, v21, 0x0

    aget v3, p4, v3

    add-float v4, v18, v3

    add-int/lit8 v3, v21, 0x1

    aget v3, p4, v3

    add-float v5, v19, v3

    add-int/lit8 v3, v21, 0x2

    aget v3, p4, v3

    add-float v6, v18, v3

    add-int/lit8 v3, v21, 0x3

    aget v3, p4, v3

    add-float v7, v19, v3

    add-int/lit8 v3, v21, 0x4

    aget v3, p4, v3

    add-float v8, v18, v3

    add-int/lit8 v3, v21, 0x5

    aget v3, p4, v3

    add-float v9, v19, v3

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->cubicTo(FFFFFF)V

    .line 767
    add-int/lit8 v3, v21, 0x2

    aget v3, p4, v3

    add-float v16, v18, v3

    .line 768
    add-int/lit8 v3, v21, 0x3

    aget v3, p4, v3

    add-float v17, v19, v3

    .line 769
    add-int/lit8 v3, v21, 0x4

    aget v3, p4, v3

    add-float v18, v18, v3

    .line 770
    add-int/lit8 v3, v21, 0x5

    aget v3, p4, v3

    add-float v19, v19, v3

    .line 772
    goto/16 :goto_2

    .line 774
    :sswitch_f
    add-int/lit8 v3, v21, 0x0

    aget v4, p4, v3

    add-int/lit8 v3, v21, 0x1

    aget v5, p4, v3

    add-int/lit8 v3, v21, 0x2

    aget v6, p4, v3

    add-int/lit8 v3, v21, 0x3

    aget v7, p4, v3

    add-int/lit8 v3, v21, 0x4

    aget v8, p4, v3

    add-int/lit8 v3, v21, 0x5

    aget v9, p4, v3

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->cubicTo(FFFFFF)V

    .line 775
    add-int/lit8 v3, v21, 0x4

    aget v18, p4, v3

    .line 776
    add-int/lit8 v3, v21, 0x5

    aget v19, p4, v3

    .line 777
    add-int/lit8 v3, v21, 0x2

    aget v16, p4, v3

    .line 778
    add-int/lit8 v3, v21, 0x3

    aget v17, p4, v3

    .line 779
    goto/16 :goto_2

    .line 781
    :sswitch_10
    const/4 v4, 0x0

    .line 782
    .local v4, "reflectiveCtrlPointX":F
    const/4 v5, 0x0

    .line 783
    .local v5, "reflectiveCtrlPointY":F
    const/16 v3, 0x63

    move/from16 v0, p2

    if-eq v0, v3, :cond_0

    const/16 v3, 0x73

    move/from16 v0, p2

    if-eq v0, v3, :cond_0

    const/16 v3, 0x43

    move/from16 v0, p2

    if-eq v0, v3, :cond_0

    const/16 v3, 0x53

    move/from16 v0, p2

    if-ne v0, v3, :cond_1

    .line 784
    :cond_0
    sub-float v4, v18, v16

    .line 785
    sub-float v5, v19, v17

    .line 787
    :cond_1
    add-float v4, v4, v18

    add-float v5, v5, v19

    add-int/lit8 v3, v21, 0x0

    aget v3, p4, v3

    .end local v4    # "reflectiveCtrlPointX":F
    .end local v5    # "reflectiveCtrlPointY":F
    add-float v6, v18, v3

    add-int/lit8 v3, v21, 0x1

    aget v3, p4, v3

    add-float v7, v19, v3

    add-int/lit8 v3, v21, 0x2

    aget v3, p4, v3

    add-float v8, v18, v3

    add-int/lit8 v3, v21, 0x3

    aget v3, p4, v3

    add-float v9, v19, v3

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->cubicTo(FFFFFF)V

    .line 790
    add-int/lit8 v3, v21, 0x0

    aget v3, p4, v3

    add-float v16, v18, v3

    .line 791
    add-int/lit8 v3, v21, 0x1

    aget v3, p4, v3

    add-float v17, v19, v3

    .line 792
    add-int/lit8 v3, v21, 0x2

    aget v3, p4, v3

    add-float v18, v18, v3

    .line 793
    add-int/lit8 v3, v21, 0x3

    aget v3, p4, v3

    add-float v19, v19, v3

    .line 794
    goto/16 :goto_2

    .line 796
    :sswitch_11
    move/from16 v4, v18

    .line 797
    .restart local v4    # "reflectiveCtrlPointX":F
    move/from16 v5, v19

    .line 798
    .restart local v5    # "reflectiveCtrlPointY":F
    const/16 v3, 0x63

    move/from16 v0, p2

    if-eq v0, v3, :cond_2

    const/16 v3, 0x73

    move/from16 v0, p2

    if-eq v0, v3, :cond_2

    const/16 v3, 0x43

    move/from16 v0, p2

    if-eq v0, v3, :cond_2

    const/16 v3, 0x53

    move/from16 v0, p2

    if-ne v0, v3, :cond_3

    .line 799
    :cond_2
    const/high16 v3, 0x40000000    # 2.0f

    mul-float v3, v3, v18

    sub-float v4, v3, v16

    .line 800
    const/high16 v3, 0x40000000    # 2.0f

    mul-float v3, v3, v19

    sub-float v5, v3, v17

    .line 802
    :cond_3
    add-int/lit8 v3, v21, 0x0

    aget v6, p4, v3

    add-int/lit8 v3, v21, 0x1

    aget v7, p4, v3

    add-int/lit8 v3, v21, 0x2

    aget v8, p4, v3

    add-int/lit8 v3, v21, 0x3

    aget v9, p4, v3

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->cubicTo(FFFFFF)V

    .line 803
    add-int/lit8 v3, v21, 0x0

    aget v16, p4, v3

    .line 804
    add-int/lit8 v3, v21, 0x1

    aget v17, p4, v3

    .line 805
    add-int/lit8 v3, v21, 0x2

    aget v18, p4, v3

    .line 806
    add-int/lit8 v3, v21, 0x3

    aget v19, p4, v3

    .line 807
    goto/16 :goto_2

    .line 809
    .end local v4    # "reflectiveCtrlPointX":F
    .end local v5    # "reflectiveCtrlPointY":F
    :sswitch_12
    add-int/lit8 v3, v21, 0x0

    aget v3, p4, v3

    add-float v3, v3, v18

    add-int/lit8 v6, v21, 0x1

    aget v6, p4, v6

    add-float v6, v6, v19

    add-int/lit8 v7, v21, 0x2

    aget v7, p4, v7

    add-float v7, v7, v18

    add-int/lit8 v8, v21, 0x3

    aget v8, p4, v8

    add-float v8, v8, v19

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v6, v7, v8}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->quadTo(FFFF)V

    .line 810
    add-int/lit8 v3, v21, 0x0

    aget v3, p4, v3

    add-float v16, v18, v3

    .line 811
    add-int/lit8 v3, v21, 0x1

    aget v3, p4, v3

    add-float v17, v19, v3

    .line 812
    add-int/lit8 v3, v21, 0x2

    aget v3, p4, v3

    add-float v18, v18, v3

    .line 813
    add-int/lit8 v3, v21, 0x3

    aget v3, p4, v3

    add-float v19, v19, v3

    .line 814
    goto/16 :goto_2

    .line 816
    :sswitch_13
    add-int/lit8 v3, v21, 0x0

    aget v3, p4, v3

    add-int/lit8 v6, v21, 0x1

    aget v6, p4, v6

    add-int/lit8 v7, v21, 0x2

    aget v7, p4, v7

    add-int/lit8 v8, v21, 0x3

    aget v8, p4, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v6, v7, v8}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->quadTo(FFFF)V

    .line 817
    add-int/lit8 v3, v21, 0x0

    aget v16, p4, v3

    .line 818
    add-int/lit8 v3, v21, 0x1

    aget v17, p4, v3

    .line 819
    add-int/lit8 v3, v21, 0x2

    aget v18, p4, v3

    .line 820
    add-int/lit8 v3, v21, 0x3

    aget v19, p4, v3

    .line 821
    goto/16 :goto_2

    .line 823
    :sswitch_14
    const/4 v4, 0x0

    .line 824
    .restart local v4    # "reflectiveCtrlPointX":F
    const/4 v5, 0x0

    .line 825
    .restart local v5    # "reflectiveCtrlPointY":F
    const/16 v3, 0x71

    move/from16 v0, p2

    if-eq v0, v3, :cond_4

    const/16 v3, 0x74

    move/from16 v0, p2

    if-eq v0, v3, :cond_4

    const/16 v3, 0x51

    move/from16 v0, p2

    if-eq v0, v3, :cond_4

    const/16 v3, 0x54

    move/from16 v0, p2

    if-ne v0, v3, :cond_5

    .line 826
    :cond_4
    sub-float v4, v18, v16

    .line 827
    sub-float v5, v19, v17

    .line 829
    :cond_5
    add-float v3, v18, v4

    add-float v6, v19, v5

    add-int/lit8 v7, v21, 0x0

    aget v7, p4, v7

    add-float v7, v7, v18

    add-int/lit8 v8, v21, 0x1

    aget v8, p4, v8

    add-float v8, v8, v19

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v6, v7, v8}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->quadTo(FFFF)V

    .line 831
    add-float v16, v18, v4

    .line 832
    add-float v17, v19, v5

    .line 833
    add-int/lit8 v3, v21, 0x0

    aget v3, p4, v3

    add-float v18, v18, v3

    .line 834
    add-int/lit8 v3, v21, 0x1

    aget v3, p4, v3

    add-float v19, v19, v3

    .line 835
    goto/16 :goto_2

    .line 837
    .end local v4    # "reflectiveCtrlPointX":F
    .end local v5    # "reflectiveCtrlPointY":F
    :sswitch_15
    move/from16 v4, v18

    .line 838
    .restart local v4    # "reflectiveCtrlPointX":F
    move/from16 v5, v19

    .line 839
    .restart local v5    # "reflectiveCtrlPointY":F
    const/16 v3, 0x71

    move/from16 v0, p2

    if-eq v0, v3, :cond_6

    const/16 v3, 0x74

    move/from16 v0, p2

    if-eq v0, v3, :cond_6

    const/16 v3, 0x51

    move/from16 v0, p2

    if-eq v0, v3, :cond_6

    const/16 v3, 0x54

    move/from16 v0, p2

    if-ne v0, v3, :cond_7

    .line 840
    :cond_6
    const/high16 v3, 0x40000000    # 2.0f

    mul-float v3, v3, v18

    sub-float v4, v3, v16

    .line 841
    const/high16 v3, 0x40000000    # 2.0f

    mul-float v3, v3, v19

    sub-float v5, v3, v17

    .line 843
    :cond_7
    add-int/lit8 v3, v21, 0x0

    aget v3, p4, v3

    add-int/lit8 v6, v21, 0x1

    aget v6, p4, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5, v3, v6}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->quadTo(FFFF)V

    .line 844
    move/from16 v16, v4

    .line 845
    move/from16 v17, v5

    .line 846
    add-int/lit8 v3, v21, 0x0

    aget v18, p4, v3

    .line 847
    add-int/lit8 v3, v21, 0x1

    aget v19, p4, v3

    .line 848
    goto/16 :goto_2

    .line 851
    .end local v4    # "reflectiveCtrlPointX":F
    .end local v5    # "reflectiveCtrlPointY":F
    :sswitch_16
    add-int/lit8 v3, v21, 0x5

    aget v3, p4, v3

    add-float v9, v3, v18

    add-int/lit8 v3, v21, 0x6

    aget v3, p4, v3

    add-float v10, v3, v19

    add-int/lit8 v3, v21, 0x0

    aget v11, p4, v3

    add-int/lit8 v3, v21, 0x1

    aget v12, p4, v3

    add-int/lit8 v3, v21, 0x2

    aget v13, p4, v3

    add-int/lit8 v3, v21, 0x3

    aget v3, p4, v3

    const/4 v6, 0x0

    cmpl-float v3, v3, v6

    if-eqz v3, :cond_8

    const/4 v14, 0x1

    :goto_4
    add-int/lit8 v3, v21, 0x4

    aget v3, p4, v3

    const/4 v6, 0x0

    cmpl-float v3, v3, v6

    if-eqz v3, :cond_9

    const/4 v15, 0x1

    :goto_5
    move-object/from16 v6, p0

    move/from16 v7, v18

    move/from16 v8, v19

    invoke-direct/range {v6 .. v15}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->drawArc(FFFFFFFZZ)V

    .line 853
    add-int/lit8 v3, v21, 0x5

    aget v3, p4, v3

    add-float v18, v18, v3

    .line 854
    add-int/lit8 v3, v21, 0x6

    aget v3, p4, v3

    add-float v19, v19, v3

    .line 855
    move/from16 v16, v18

    .line 856
    move/from16 v17, v19

    .line 857
    goto/16 :goto_2

    .line 851
    :cond_8
    const/4 v14, 0x0

    goto :goto_4

    :cond_9
    const/4 v15, 0x0

    goto :goto_5

    .line 859
    :sswitch_17
    add-int/lit8 v3, v21, 0x5

    aget v9, p4, v3

    add-int/lit8 v3, v21, 0x6

    aget v10, p4, v3

    add-int/lit8 v3, v21, 0x0

    aget v11, p4, v3

    add-int/lit8 v3, v21, 0x1

    aget v12, p4, v3

    add-int/lit8 v3, v21, 0x2

    aget v13, p4, v3

    add-int/lit8 v3, v21, 0x3

    aget v3, p4, v3

    const/4 v6, 0x0

    cmpl-float v3, v3, v6

    if-eqz v3, :cond_a

    const/4 v14, 0x1

    :goto_6
    add-int/lit8 v3, v21, 0x4

    aget v3, p4, v3

    const/4 v6, 0x0

    cmpl-float v3, v3, v6

    if-eqz v3, :cond_b

    const/4 v15, 0x1

    :goto_7
    move-object/from16 v6, p0

    move/from16 v7, v18

    move/from16 v8, v19

    invoke-direct/range {v6 .. v15}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->drawArc(FFFFFFFZZ)V

    .line 861
    add-int/lit8 v3, v21, 0x5

    aget v18, p4, v3

    .line 862
    add-int/lit8 v3, v21, 0x6

    aget v19, p4, v3

    .line 863
    move/from16 v16, v18

    .line 864
    move/from16 v17, v19

    goto/16 :goto_2

    .line 859
    :cond_a
    const/4 v14, 0x0

    goto :goto_6

    :cond_b
    const/4 v15, 0x0

    goto :goto_7

    .line 869
    :cond_c
    const/4 v3, 0x0

    aput v18, p1, v3

    .line 870
    const/4 v3, 0x1

    aput v19, p1, v3

    .line 871
    const/4 v3, 0x2

    aput v16, p1, v3

    .line 872
    const/4 v3, 0x3

    aput v17, p1, v3

    goto/16 :goto_3

    .line 689
    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_5
        0x43 -> :sswitch_3
        0x48 -> :sswitch_2
        0x4c -> :sswitch_1
        0x4d -> :sswitch_1
        0x51 -> :sswitch_4
        0x53 -> :sswitch_4
        0x54 -> :sswitch_1
        0x56 -> :sswitch_2
        0x5a -> :sswitch_0
        0x61 -> :sswitch_5
        0x63 -> :sswitch_3
        0x68 -> :sswitch_2
        0x6c -> :sswitch_1
        0x6d -> :sswitch_1
        0x71 -> :sswitch_4
        0x73 -> :sswitch_4
        0x74 -> :sswitch_1
        0x76 -> :sswitch_2
        0x7a -> :sswitch_0
    .end sparse-switch

    .line 724
    :sswitch_data_1
    .sparse-switch
        0x41 -> :sswitch_17
        0x43 -> :sswitch_f
        0x48 -> :sswitch_b
        0x4c -> :sswitch_9
        0x4d -> :sswitch_7
        0x51 -> :sswitch_13
        0x53 -> :sswitch_11
        0x54 -> :sswitch_15
        0x56 -> :sswitch_d
        0x61 -> :sswitch_16
        0x63 -> :sswitch_e
        0x68 -> :sswitch_a
        0x6c -> :sswitch_8
        0x6d -> :sswitch_6
        0x71 -> :sswitch_12
        0x73 -> :sswitch_10
        0x74 -> :sswitch_14
        0x76 -> :sswitch_c
    .end sparse-switch
.end method

.method private arcToBezier(DDDDDDDDD)V
    .locals 55
    .param p1, "cx"    # D
    .param p3, "cy"    # D
    .param p5, "a"    # D
    .param p7, "b"    # D
    .param p9, "e1x"    # D
    .param p11, "e1y"    # D
    .param p13, "theta"    # D
    .param p15, "start"    # D
    .param p17, "sweep"    # D

    .prologue
    .line 961
    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    mul-double v2, v2, p17

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v36

    .line 963
    .local v36, "numSegments":I
    move-wide/from16 v32, p15

    .line 964
    .local v32, "eta1":D
    invoke-static/range {p13 .. p14}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    .line 965
    .local v18, "cosTheta":D
    invoke-static/range {p13 .. p14}, Ljava/lang/Math;->sin(D)D

    move-result-wide v50

    .line 966
    .local v50, "sinTheta":D
    invoke-static/range {v32 .. v33}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    .line 967
    .local v14, "cosEta1":D
    invoke-static/range {v32 .. v33}, Ljava/lang/Math;->sin(D)D

    move-result-wide v46

    .line 968
    .local v46, "sinEta1":D
    move-wide/from16 v0, p5

    neg-double v2, v0

    mul-double v2, v2, v18

    mul-double v2, v2, v46

    mul-double v4, p7, v50

    mul-double/2addr v4, v14

    sub-double v24, v2, v4

    .line 969
    .local v24, "ep1x":D
    move-wide/from16 v0, p5

    neg-double v2, v0

    mul-double v2, v2, v50

    mul-double v2, v2, v46

    mul-double v4, p7, v18

    mul-double/2addr v4, v14

    add-double v26, v2, v4

    .line 971
    .local v26, "ep1y":D
    move/from16 v0, v36

    int-to-double v2, v0

    div-double v12, p17, v2

    .line 972
    .local v12, "anglePerSegment":D
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    move/from16 v0, v36

    if-ge v9, v0, :cond_0

    .line 973
    add-double v34, v32, v12

    .line 974
    .local v34, "eta2":D
    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->sin(D)D

    move-result-wide v48

    .line 975
    .local v48, "sinEta2":D
    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    .line 976
    .local v16, "cosEta2":D
    mul-double v2, p5, v18

    mul-double v2, v2, v16

    add-double v2, v2, p1

    mul-double v4, p7, v50

    mul-double v4, v4, v48

    sub-double v20, v2, v4

    .line 977
    .local v20, "e2x":D
    mul-double v2, p5, v50

    mul-double v2, v2, v16

    add-double v2, v2, p3

    mul-double v4, p7, v18

    mul-double v4, v4, v48

    add-double v22, v2, v4

    .line 978
    .local v22, "e2y":D
    move-wide/from16 v0, p5

    neg-double v2, v0

    mul-double v2, v2, v18

    mul-double v2, v2, v48

    mul-double v4, p7, v50

    mul-double v4, v4, v16

    sub-double v28, v2, v4

    .line 979
    .local v28, "ep2x":D
    move-wide/from16 v0, p5

    neg-double v2, v0

    mul-double v2, v2, v50

    mul-double v2, v2, v48

    mul-double v4, p7, v18

    mul-double v4, v4, v16

    add-double v30, v2, v4

    .line 980
    .local v30, "ep2y":D
    sub-double v2, v34, v32

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v52

    .line 981
    .local v52, "tanDiff2":D
    sub-double v2, v34, v32

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    const-wide/high16 v6, 0x4008000000000000L    # 3.0

    mul-double v6, v6, v52

    mul-double v6, v6, v52

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    div-double v10, v2, v4

    .line 982
    .local v10, "alpha":D
    mul-double v2, v10, v24

    add-double v38, p9, v2

    .line 983
    .local v38, "q1x":D
    mul-double v2, v10, v26

    add-double v40, p11, v2

    .line 984
    .local v40, "q1y":D
    mul-double v2, v10, v28

    sub-double v42, v20, v2

    .line 985
    .local v42, "q2x":D
    mul-double v2, v10, v30

    sub-double v44, v22, v2

    .line 987
    .local v44, "q2y":D
    move-wide/from16 v0, v38

    double-to-float v3, v0

    move-wide/from16 v0, v40

    double-to-float v4, v0

    move-wide/from16 v0, v42

    double-to-float v5, v0

    move-wide/from16 v0, v44

    double-to-float v6, v0

    move-wide/from16 v0, v20

    double-to-float v7, v0

    move-wide/from16 v0, v22

    double-to-float v8, v0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->cubicTo(FFFFFF)V

    .line 988
    move-wide/from16 v32, v34

    .line 989
    move-wide/from16 p9, v20

    .line 990
    move-wide/from16 p11, v22

    .line 991
    move-wide/from16 v24, v28

    .line 992
    move-wide/from16 v26, v30

    .line 972
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 994
    .end local v10    # "alpha":D
    .end local v16    # "cosEta2":D
    .end local v20    # "e2x":D
    .end local v22    # "e2y":D
    .end local v28    # "ep2x":D
    .end local v30    # "ep2y":D
    .end local v34    # "eta2":D
    .end local v38    # "q1x":D
    .end local v40    # "q1y":D
    .end local v42    # "q2x":D
    .end local v44    # "q2y":D
    .end local v48    # "sinEta2":D
    .end local v52    # "tanDiff2":D
    :cond_0
    return-void
.end method

.method private createNodesFromPathData(Ljava/lang/String;)V
    .locals 9
    .param p1, "pathData"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 554
    if-nez p1, :cond_1

    .line 578
    :cond_0
    :goto_0
    return-void

    .line 557
    :cond_1
    const/4 v4, 0x0

    .line 558
    .local v4, "start":I
    const/4 v1, 0x1

    .line 560
    .local v1, "end":I
    const/16 v2, 0x6d

    .line 561
    .local v2, "previousCmd":C
    const/4 v6, 0x4

    new-array v0, v6, [F

    .line 563
    .local v0, "current":[F
    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v1, v6, :cond_3

    .line 564
    invoke-direct {p0, p1, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->nextStart(Ljava/lang/String;I)I

    move-result v1

    .line 565
    invoke-virtual {p1, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 566
    .local v3, "s":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_2

    .line 567
    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->getFloats(Ljava/lang/String;)[F

    move-result-object v5

    .line 568
    .local v5, "val":[F
    invoke-virtual {v3, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-direct {p0, v0, v2, v6, v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->addCommand([FCC[F)V

    .line 569
    invoke-virtual {v3, v8}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 572
    .end local v5    # "val":[F
    :cond_2
    move v4, v1

    .line 573
    add-int/lit8 v1, v1, 0x1

    .line 574
    goto :goto_1

    .line 575
    .end local v3    # "s":Ljava/lang/String;
    :cond_3
    sub-int v6, v1, v4

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v4, v6, :cond_0

    .line 576
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v6

    new-array v7, v8, [F

    invoke-direct {p0, v0, v2, v6, v7}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->addCommand([FCC[F)V

    goto :goto_0
.end method

.method private drawArc(FFFFFFFZZ)V
    .locals 58
    .param p1, "x0"    # F
    .param p2, "y0"    # F
    .param p3, "x1"    # F
    .param p4, "y1"    # F
    .param p5, "a"    # F
    .param p6, "b"    # F
    .param p7, "theta"    # F
    .param p8, "isMoreThanHalf"    # Z
    .param p9, "isPositiveArc"    # Z

    .prologue
    .line 879
    move/from16 v0, p7

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v16

    .line 881
    .local v16, "thetaD":D
    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v24

    .line 882
    .local v24, "cosTheta":D
    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v42

    .line 885
    .local v42, "sinTheta":D
    move/from16 v0, p1

    float-to-double v2, v0

    mul-double v2, v2, v24

    move/from16 v0, p2

    float-to-double v8, v0

    mul-double v8, v8, v42

    add-double/2addr v2, v8

    move/from16 v0, p5

    float-to-double v8, v0

    div-double v46, v2, v8

    .line 886
    .local v46, "x0p":D
    move/from16 v0, p1

    neg-float v2, v0

    float-to-double v2, v2

    mul-double v2, v2, v42

    move/from16 v0, p2

    float-to-double v8, v0

    mul-double v8, v8, v24

    add-double/2addr v2, v8

    move/from16 v0, p6

    float-to-double v8, v0

    div-double v52, v2, v8

    .line 887
    .local v52, "y0p":D
    move/from16 v0, p3

    float-to-double v2, v0

    mul-double v2, v2, v24

    move/from16 v0, p4

    float-to-double v8, v0

    mul-double v8, v8, v42

    add-double/2addr v2, v8

    move/from16 v0, p5

    float-to-double v8, v0

    div-double v48, v2, v8

    .line 888
    .local v48, "x1p":D
    move/from16 v0, p3

    neg-float v2, v0

    float-to-double v2, v2

    mul-double v2, v2, v42

    move/from16 v0, p4

    float-to-double v8, v0

    mul-double v8, v8, v24

    add-double/2addr v2, v8

    move/from16 v0, p6

    float-to-double v8, v0

    div-double v54, v2, v8

    .line 891
    .local v54, "y1p":D
    sub-double v30, v46, v48

    .line 892
    .local v30, "dx":D
    sub-double v32, v52, v54

    .line 893
    .local v32, "dy":D
    add-double v2, v46, v48

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double v50, v2, v8

    .line 894
    .local v50, "xm":D
    add-double v2, v52, v54

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double v56, v2, v8

    .line 896
    .local v56, "ym":D
    mul-double v2, v30, v30

    mul-double v8, v32, v32

    add-double v28, v2, v8

    .line 897
    .local v28, "dsq":D
    const-wide/16 v2, 0x0

    cmpl-double v2, v28, v2

    if-nez v2, :cond_0

    .line 939
    :goto_0
    return-void

    .line 900
    :cond_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    div-double v2, v2, v28

    const-wide/high16 v8, 0x3fd0000000000000L    # 0.25

    sub-double v26, v2, v8

    .line 901
    .local v26, "disc":D
    const-wide/16 v2, 0x0

    cmpg-double v2, v26, v2

    if-gez v2, :cond_1

    .line 902
    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    const-wide v8, 0x3ffffff583a53b8eL    # 1.99999

    div-double/2addr v2, v8

    double-to-float v0, v2

    move/from16 v22, v0

    .line 903
    .local v22, "adjust":F
    mul-float v7, p5, v22

    mul-float v8, p6, v22

    move-object/from16 v2, p0

    move/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    move/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    invoke-direct/range {v2 .. v11}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->drawArc(FFFFFFFZZ)V

    goto :goto_0

    .line 906
    .end local v22    # "adjust":F
    :cond_1
    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v36

    .line 907
    .local v36, "s":D
    mul-double v38, v36, v30

    .line 908
    .local v38, "sdx":D
    mul-double v40, v36, v32

    .line 911
    .local v40, "sdy":D
    move/from16 v0, p8

    move/from16 v1, p9

    if-ne v0, v1, :cond_3

    .line 912
    sub-double v4, v50, v40

    .line 913
    .local v4, "cx":D
    add-double v6, v56, v38

    .line 919
    .local v6, "cy":D
    :goto_1
    sub-double v2, v52, v6

    sub-double v8, v46, v4

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v18

    .line 921
    .local v18, "eta0":D
    sub-double v2, v54, v6

    sub-double v8, v48, v4

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v34

    .line 923
    .local v34, "eta1":D
    sub-double v20, v34, v18

    .line 924
    .local v20, "sweep":D
    const-wide/16 v2, 0x0

    cmpl-double v2, v20, v2

    if-ltz v2, :cond_4

    const/4 v2, 0x1

    :goto_2
    move/from16 v0, p9

    if-eq v0, v2, :cond_2

    .line 925
    const-wide/16 v2, 0x0

    cmpl-double v2, v20, v2

    if-lez v2, :cond_5

    .line 926
    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    sub-double v20, v20, v2

    .line 932
    :cond_2
    :goto_3
    move/from16 v0, p5

    float-to-double v2, v0

    mul-double/2addr v4, v2

    .line 933
    move/from16 v0, p6

    float-to-double v2, v0

    mul-double/2addr v6, v2

    .line 934
    move-wide/from16 v44, v4

    .line 935
    .local v44, "tcx":D
    mul-double v2, v4, v24

    mul-double v8, v6, v42

    sub-double v4, v2, v8

    .line 936
    mul-double v2, v44, v42

    mul-double v8, v6, v24

    add-double v6, v2, v8

    .line 938
    move/from16 v0, p5

    float-to-double v8, v0

    move/from16 v0, p6

    float-to-double v10, v0

    move/from16 v0, p1

    float-to-double v12, v0

    move/from16 v0, p2

    float-to-double v14, v0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v21}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->arcToBezier(DDDDDDDDD)V

    goto/16 :goto_0

    .line 915
    .end local v4    # "cx":D
    .end local v6    # "cy":D
    .end local v18    # "eta0":D
    .end local v20    # "sweep":D
    .end local v34    # "eta1":D
    .end local v44    # "tcx":D
    :cond_3
    add-double v4, v50, v40

    .line 916
    .restart local v4    # "cx":D
    sub-double v6, v56, v38

    .restart local v6    # "cy":D
    goto :goto_1

    .line 924
    .restart local v18    # "eta0":D
    .restart local v20    # "sweep":D
    .restart local v34    # "eta1":D
    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 928
    :cond_5
    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    add-double v20, v20, v2

    goto :goto_3
.end method

.method private drawPath(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;)V
    .locals 7
    .param p1, "p"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;

    .prologue
    .line 246
    iget-byte v0, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->type:B

    packed-switch v0, :pswitch_data_0

    .line 271
    :goto_0
    return-void

    .line 248
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    iget v1, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    iget v2, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    goto :goto_0

    .line 252
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    iget v1, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    iget v2, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_0

    .line 256
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    iget v1, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x1:F

    iget v2, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y1:F

    iget v3, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x2:F

    iget v4, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y2:F

    iget v5, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    iget v6, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    goto :goto_0

    .line 260
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    iget v1, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x1:F

    iget v2, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y1:F

    iget v3, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    iget v4, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto :goto_0

    .line 264
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    new-instance v1, Landroid/graphics/RectF;

    iget v2, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    iget v3, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    iget v4, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x1:F

    iget v5, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y1:F

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget v2, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x2:F

    iget v3, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y2:F

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    goto :goto_0

    .line 268
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    goto :goto_0

    .line 246
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private extract(Ljava/lang/String;ILcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$ExtractFloatResult;)V
    .locals 4
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "start"    # I
    .param p3, "result"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$ExtractFloatResult;

    .prologue
    .line 653
    move v1, p2

    .line 654
    .local v1, "currentIndex":I
    const/4 v2, 0x0

    .line 655
    .local v2, "foundSeparator":Z
    const/4 v3, 0x0

    iput-boolean v3, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$ExtractFloatResult;->mEndWithNegSign:Z

    .line 656
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 657
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 658
    .local v0, "currentChar":C
    sparse-switch v0, :sswitch_data_0

    .line 670
    :cond_0
    :goto_1
    if-eqz v2, :cond_2

    .line 676
    .end local v0    # "currentChar":C
    :cond_1
    iput v1, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$ExtractFloatResult;->mEndPosition:I

    .line 677
    return-void

    .line 661
    .restart local v0    # "currentChar":C
    :sswitch_0
    const/4 v2, 0x1

    .line 662
    goto :goto_1

    .line 664
    :sswitch_1
    if-eq v1, p2, :cond_0

    .line 665
    const/4 v2, 0x1

    .line 666
    const/4 v3, 0x1

    iput-boolean v3, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$ExtractFloatResult;->mEndWithNegSign:Z

    goto :goto_1

    .line 656
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 658
    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0x2c -> :sswitch_0
        0x2d -> :sswitch_1
    .end sparse-switch
.end method

.method private getFloats(Ljava/lang/String;)[F
    .locals 11
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 608
    invoke-virtual {p1, v10}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x7a

    if-eq v8, v9, :cond_0

    invoke-virtual {p1, v10}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x5a

    if-ne v8, v9, :cond_1

    .line 609
    :cond_0
    new-array v8, v10, [F

    .line 638
    :goto_0
    return-object v8

    .line 612
    :cond_1
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    new-array v5, v8, [F

    .line 613
    .local v5, "results":[F
    const/4 v0, 0x0

    .line 614
    .local v0, "count":I
    const/4 v6, 0x1

    .line 615
    .local v6, "startPosition":I
    const/4 v3, 0x0

    .line 617
    .local v3, "endPosition":I
    new-instance v4, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$ExtractFloatResult;

    const/4 v8, 0x0

    invoke-direct {v4, v8}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$ExtractFloatResult;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$1;)V

    .line 618
    .local v4, "result":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$ExtractFloatResult;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    .local v7, "totalLength":I
    move v1, v0

    .line 623
    .end local v0    # "count":I
    .local v1, "count":I
    :goto_1
    if-ge v6, v7, :cond_3

    .line 624
    invoke-direct {p0, p1, v6, v4}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->extract(Ljava/lang/String;ILcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$ExtractFloatResult;)V

    .line 625
    iget v3, v4, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$ExtractFloatResult;->mEndPosition:I

    .line 627
    if-ge v6, v3, :cond_4

    .line 628
    add-int/lit8 v0, v1, 0x1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    invoke-virtual {p1, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v8

    aput v8, v5, v1

    .line 631
    :goto_2
    iget-boolean v8, v4, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$ExtractFloatResult;->mEndWithNegSign:Z

    if-eqz v8, :cond_2

    .line 633
    move v6, v3

    move v1, v0

    .end local v0    # "count":I
    .restart local v1    # "count":I
    goto :goto_1

    .line 635
    .end local v1    # "count":I
    .restart local v0    # "count":I
    :cond_2
    add-int/lit8 v6, v3, 0x1

    move v1, v0

    .end local v0    # "count":I
    .restart local v1    # "count":I
    goto :goto_1

    .line 638
    :cond_3
    invoke-static {v5, v1}, Ljava/util/Arrays;->copyOf([FI)[F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    goto :goto_0

    .line 639
    .end local v1    # "count":I
    .end local v3    # "endPosition":I
    .end local v4    # "result":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$ExtractFloatResult;
    .end local v5    # "results":[F
    .end local v6    # "startPosition":I
    .end local v7    # "totalLength":I
    :catch_0
    move-exception v2

    .line 640
    .local v2, "e":Ljava/lang/NumberFormatException;
    throw v2

    .end local v2    # "e":Ljava/lang/NumberFormatException;
    .restart local v1    # "count":I
    .restart local v3    # "endPosition":I
    .restart local v4    # "result":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$ExtractFloatResult;
    .restart local v5    # "results":[F
    .restart local v6    # "startPosition":I
    .restart local v7    # "totalLength":I
    :cond_4
    move v0, v1

    .end local v1    # "count":I
    .restart local v0    # "count":I
    goto :goto_2
.end method

.method private nextStart(Ljava/lang/String;I)I
    .locals 3
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "end"    # I

    .prologue
    .line 583
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge p2, v1, :cond_0

    .line 584
    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 585
    .local v0, "c":C
    add-int/lit8 v1, v0, -0x41

    add-int/lit8 v2, v0, -0x5a

    mul-int/2addr v1, v2

    if-lez v1, :cond_0

    add-int/lit8 v1, v0, -0x61

    add-int/lit8 v2, v0, -0x7a

    mul-int/2addr v1, v2

    if-gtz v1, :cond_1

    .line 590
    .end local v0    # "c":C
    :cond_0
    return p2

    .line 588
    .restart local v0    # "c":C
    :cond_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public arcTo(FFFFFF)V
    .locals 2
    .param p1, "x1"    # F
    .param p2, "y1"    # F
    .param p3, "x2"    # F
    .param p4, "y2"    # F
    .param p5, "startAngle"    # F
    .param p6, "sweepAngle"    # F

    .prologue
    .line 215
    new-instance v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;-><init>()V

    .line 216
    .local v0, "newPath":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    const/4 v1, 0x5

    iput-byte v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->type:B

    .line 217
    iput p1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    .line 218
    iput p2, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    .line 219
    iput p3, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x1:F

    .line 220
    iput p4, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y1:F

    .line 221
    iput p5, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x2:F

    .line 222
    iput p6, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y2:F

    .line 224
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->drawPath(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;)V

    .line 227
    return-void
.end method

.method public clone()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 530
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->clone()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;

    .line 531
    .local v1, "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    .line 533
    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;

    .line 534
    .local v2, "path":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    iget-object v3, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->clone()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 537
    .end local v2    # "path":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    :cond_0
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    iput-object v3, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    .line 538
    invoke-virtual {v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->drawPath()V

    .line 540
    return-object v1
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->clone()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 2

    .prologue
    .line 230
    new-instance v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;-><init>()V

    .line 231
    .local v0, "newPath":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    const/4 v1, 0x6

    iput-byte v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->type:B

    .line 233
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->drawPath(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;)V

    .line 236
    return-void
.end method

.method public cubicTo(FFFFFF)V
    .locals 2
    .param p1, "x1"    # F
    .param p2, "y1"    # F
    .param p3, "x2"    # F
    .param p4, "y2"    # F
    .param p5, "x"    # F
    .param p6, "y"    # F

    .prologue
    .line 200
    new-instance v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;-><init>()V

    .line 201
    .local v0, "newPath":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    const/4 v1, 0x4

    iput-byte v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->type:B

    .line 202
    iput p5, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    .line 203
    iput p6, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    .line 204
    iput p1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x1:F

    .line 205
    iput p2, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y1:F

    .line 206
    iput p3, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x2:F

    .line 207
    iput p4, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y2:F

    .line 209
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->drawPath(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;)V

    .line 212
    return-void
.end method

.method public drawPath()V
    .locals 3

    .prologue
    .line 239
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 240
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;

    .line 241
    .local v1, "p":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->drawPath(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;)V

    goto :goto_0

    .line 243
    .end local v1    # "p":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    :cond_0
    return-void
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 96
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->finalize()V

    .line 98
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 99
    return-void
.end method

.method public fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V
    .locals 12
    .param p1, "in"    # Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 275
    const/4 v1, 0x0

    .local v1, "x1":F
    const/4 v2, 0x0

    .local v2, "y1":F
    const/4 v3, 0x0

    .local v3, "x2":F
    const/4 v4, 0x0

    .line 276
    .local v4, "y2":F
    const/4 v5, 0x0

    .local v5, "lastX":F
    const/4 v6, 0x0

    .line 278
    .local v6, "lastY":F
    const/4 v7, 0x0

    .local v7, "i":I
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readInt()I

    move-result v8

    .local v8, "n":I
    :goto_0
    if-ge v7, v8, :cond_0

    .line 279
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readByte()B

    move-result v9

    .line 280
    .local v9, "type":B
    packed-switch v9, :pswitch_data_0

    .line 327
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "unsupported command type:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v0, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 282
    :pswitch_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v1

    .line 283
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v2

    .line 284
    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->moveTo(FF)V

    .line 278
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 288
    :pswitch_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v1

    .line 289
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v2

    .line 290
    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->lineTo(FF)V

    goto :goto_1

    .line 294
    :pswitch_2
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v1

    .line 295
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v2

    .line 296
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v3

    .line 297
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v4

    .line 298
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v5

    .line 299
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v6

    move-object v0, p0

    .line 301
    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->cubicTo(FFFFFF)V

    goto :goto_1

    .line 305
    :pswitch_3
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v1

    .line 306
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v2

    .line 307
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v5

    .line 308
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v6

    .line 310
    invoke-virtual {p0, v1, v2, v5, v6}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->quadTo(FFFF)V

    goto :goto_1

    .line 315
    :pswitch_4
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    .line 316
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    .line 317
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    .line 318
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    .line 319
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    .line 320
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    goto :goto_1

    .line 324
    :pswitch_5
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->close()V

    goto :goto_1

    .line 331
    .end local v9    # "type":B
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 332
    return-void

    .line 280
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public fromXml(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 11
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 383
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v4

    .local v4, "n":I
    :goto_0
    if-ge v2, v4, :cond_1f

    .line 384
    invoke-interface {p1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v5

    .line 385
    .local v5, "name":Ljava/lang/String;
    invoke-interface {p1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v7

    .line 387
    .local v7, "value":Ljava/lang/String;
    const-string v8, "name"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 383
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 389
    :cond_1
    const-string v8, "strokeWidth"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 390
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;

    invoke-direct {v1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;-><init>()V

    .line 391
    .local v1, "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;
    invoke-static {v7}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    iput v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->origStrokeWidth:F

    iput v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->strokeWidth:F

    .line 392
    iget v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->origStrokeWidth:F

    const/4 v9, 0x0

    cmpl-float v8, v8, v9

    if-lez v8, :cond_2

    iget v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->origStrokeWidth:F

    const v9, 0x3e99999a    # 0.3f

    cmpg-float v8, v8, v9

    if-gez v8, :cond_2

    .line 393
    const v8, 0x3e99999a    # 0.3f

    iput v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->strokeWidth:F

    .line 395
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 396
    .end local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;
    :cond_3
    const-string v8, "strokeOpacity"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 397
    const/4 v1, 0x0

    .line 398
    .local v1, "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    .line 399
    .local v0, "attr":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    iget-byte v8, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mType:B

    const/16 v9, 0x23

    if-ne v8, v9, :cond_4

    move-object v1, v0

    .line 400
    check-cast v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;

    .line 404
    .end local v0    # "attr":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    :cond_5
    if-nez v1, :cond_6

    .line 405
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;

    .end local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;
    invoke-direct {v1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;-><init>()V

    .line 406
    .restart local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 409
    :cond_6
    invoke-static {v7}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    const/high16 v9, 0x437f0000    # 255.0f

    mul-float/2addr v8, v9

    float-to-int v6, v8

    .line 410
    .local v6, "opacity":I
    iget v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;->color:I

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v9, v6, 0x18

    or-int/2addr v8, v9

    iput v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;->color:I

    goto :goto_1

    .line 411
    .end local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v6    # "opacity":I
    :cond_7
    const-string v8, "strokeColor"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 412
    const/4 v1, 0x0

    .line 413
    .restart local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    .line 414
    .restart local v0    # "attr":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    iget-byte v8, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mType:B

    const/16 v9, 0x23

    if-ne v8, v9, :cond_8

    move-object v1, v0

    .line 415
    check-cast v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;

    .line 419
    .end local v0    # "attr":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    :cond_9
    if-nez v1, :cond_a

    .line 420
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;

    .end local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;
    invoke-direct {v1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;-><init>()V

    .line 421
    .restart local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 424
    :cond_a
    const-string v8, "#"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_b

    .line 425
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x10

    invoke-static {v8, v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v8

    long-to-int v8, v8

    iput v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;->color:I

    .line 430
    :goto_2
    iget v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;->color:I

    const/high16 v9, -0x1000000

    and-int/2addr v8, v9

    iget v9, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;->color:I

    const v10, 0xffffff

    and-int/2addr v9, v10

    xor-int/lit8 v9, v9, -0x1

    or-int/2addr v8, v9

    iput v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;->color:I

    goto/16 :goto_1

    .line 427
    :cond_b
    const/high16 v8, -0x10000

    iput v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;->color:I

    goto :goto_2

    .line 431
    .end local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_c
    const-string v8, "fillColor"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_11

    .line 432
    const/4 v1, 0x0

    .line 433
    .local v1, "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    .line 434
    .restart local v0    # "attr":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    iget-byte v8, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mType:B

    const/16 v9, 0x20

    if-ne v8, v9, :cond_d

    move-object v1, v0

    .line 435
    check-cast v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    .line 439
    .end local v0    # "attr":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    :cond_e
    if-nez v1, :cond_f

    .line 440
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    .end local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;
    invoke-direct {v1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>()V

    .line 441
    .restart local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 444
    :cond_f
    const-string v8, "#"

    invoke-virtual {v7, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 445
    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x10

    invoke-static {v8, v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v8

    long-to-int v8, v8

    iput v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;->color:I

    goto/16 :goto_1

    .line 447
    :cond_10
    const/high16 v8, -0x10000

    iput v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;->color:I

    goto/16 :goto_1

    .line 449
    .end local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_11
    const-string v8, "fillOpacity"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_15

    .line 450
    const/4 v1, 0x0

    .line 451
    .restart local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_12
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_13

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    .line 452
    .restart local v0    # "attr":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    iget-byte v8, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mType:B

    const/16 v9, 0x20

    if-ne v8, v9, :cond_12

    move-object v1, v0

    .line 453
    check-cast v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    .line 457
    .end local v0    # "attr":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    :cond_13
    if-nez v1, :cond_14

    .line 458
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    .end local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;
    invoke-direct {v1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>()V

    .line 459
    .restart local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 462
    :cond_14
    invoke-static {v7}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    const/high16 v9, 0x437f0000    # 255.0f

    mul-float/2addr v8, v9

    float-to-int v6, v8

    .line 463
    .restart local v6    # "opacity":I
    iget v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;->color:I

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v9, v6, 0x18

    or-int/2addr v8, v9

    iput v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;->color:I

    goto/16 :goto_1

    .line 464
    .end local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v6    # "opacity":I
    :cond_15
    const-string v8, "pathData"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_16

    .line 465
    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->createNodesFromPathData(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 466
    :cond_16
    const-string v8, "trimPathStart"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 468
    const-string v8, "trimPathEnd"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 470
    const-string v8, "trimPathOffset"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 472
    const-string v8, "strokeLineCap"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1a

    .line 473
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinecap;

    invoke-direct {v1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinecap;-><init>()V

    .line 474
    .local v1, "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinecap;
    const-string v8, "butt"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_18

    .line 475
    sget-byte v8, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinecap;->STROKE_LINECAP_TYPE_BUTT:B

    iput-byte v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinecap;->linecap:B

    .line 481
    :cond_17
    :goto_3
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 476
    :cond_18
    const-string v8, "round"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_19

    .line 477
    sget-byte v8, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinecap;->STROKE_LINECAP_TYPE_ROUND:B

    iput-byte v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinecap;->linecap:B

    goto :goto_3

    .line 478
    :cond_19
    const-string v8, "square"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_17

    .line 479
    sget-byte v8, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinecap;->STROKE_LINECAP_TYPE_SQUARE:B

    iput-byte v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinecap;->linecap:B

    goto :goto_3

    .line 482
    .end local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinecap;
    :cond_1a
    const-string v8, "strokeLineJoin"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1e

    .line 483
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;

    invoke-direct {v1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;-><init>()V

    .line 484
    .local v1, "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;
    const-string v8, "miter"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1c

    .line 485
    sget-byte v8, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->STROKE_LINEJOIN_TYPE_MITER:B

    iput-byte v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->linejoin:B

    .line 491
    :cond_1b
    :goto_4
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 486
    :cond_1c
    const-string v8, "round"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1d

    .line 487
    sget-byte v8, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->STROKE_LINEJOIN_TYPE_ROUND:B

    iput-byte v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->linejoin:B

    goto :goto_4

    .line 488
    :cond_1d
    const-string v8, "bevel"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1b

    .line 489
    sget-byte v8, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->STROKE_LINEJOIN_TYPE_BEVEL:B

    iput-byte v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->linejoin:B

    goto :goto_4

    .line 492
    .end local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;
    :cond_1e
    const-string v8, "strokeMiterLimit"

    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 493
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeMiterlimit;

    invoke-direct {v1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeMiterlimit;-><init>()V

    .line 494
    .local v1, "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeMiterlimit;
    invoke-static {v7}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    iput v8, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeMiterlimit;->miterLimit:F

    .line 495
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 498
    .end local v1    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeMiterlimit;
    .end local v5    # "name":Ljava/lang/String;
    .end local v7    # "value":Ljava/lang/String;
    :cond_1f
    return-void
.end method

.method public getSPRSize()I
    .locals 4

    .prologue
    .line 502
    const/4 v2, 0x4

    .line 504
    .local v2, "size":I
    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;

    .line 505
    .local v1, "pathInfo":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    iget-byte v3, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->type:B

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 508
    :pswitch_0
    add-int/lit8 v2, v2, 0x8

    .line 509
    goto :goto_0

    .line 512
    :pswitch_1
    add-int/lit8 v2, v2, 0x18

    .line 513
    goto :goto_0

    .line 516
    :pswitch_2
    add-int/lit8 v2, v2, 0x10

    .line 517
    goto :goto_0

    .line 525
    .end local v1    # "pathInfo":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    :cond_0
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->getSPRSize()I

    move-result v3

    add-int/2addr v3, v2

    return v3

    .line 505
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public getTotalElementCount()I
    .locals 1

    .prologue
    .line 550
    const/4 v0, 0x1

    return v0
.end method

.method public getTotalSegmentCount()I
    .locals 1

    .prologue
    .line 545
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public lineTo(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 176
    new-instance v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;-><init>()V

    .line 177
    .local v0, "newPath":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    const/4 v1, 0x2

    iput-byte v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->type:B

    .line 178
    iput p1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    .line 179
    iput p2, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    .line 181
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->drawPath(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;)V

    .line 184
    return-void
.end method

.method public moveTo(FF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 165
    new-instance v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;-><init>()V

    .line 166
    .local v0, "newPath":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    const/4 v1, 0x1

    iput-byte v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->type:B

    .line 167
    iput p1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    .line 168
    iput p2, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    .line 170
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 172
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->drawPath(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;)V

    .line 173
    return-void
.end method

.method public quadTo(FFFF)V
    .locals 2
    .param p1, "x1"    # F
    .param p2, "y1"    # F
    .param p3, "x"    # F
    .param p4, "y"    # F

    .prologue
    .line 187
    new-instance v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;-><init>()V

    .line 188
    .local v0, "newPath":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    const/4 v1, 0x3

    iput-byte v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->type:B

    .line 189
    iput p3, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    .line 190
    iput p4, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    .line 191
    iput p1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x1:F

    .line 192
    iput p2, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y1:F

    .line 194
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 196
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->drawPath(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;)V

    .line 197
    return-void
.end method

.method public setDisplayLayout(ZFFFFFFFFF)V
    .locals 21
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "npLeft"    # F
    .param p4, "npTop"    # F
    .param p5, "npRight"    # F
    .param p6, "npBottom"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "displayWidth"    # F
    .param p10, "displayHeight"    # F

    .prologue
    .line 104
    invoke-super/range {p0 .. p10}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->setDisplayLayout(ZFFFFFFFFF)V

    .line 107
    if-eqz p1, :cond_0

    .line 109
    div-float p9, p9, p2

    .line 110
    div-float p10, p10, p2

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;

    .line 115
    .local v17, "p":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    move-object/from16 v0, v17

    iget-byte v2, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->type:B

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 117
    :pswitch_0
    move-object/from16 v0, v17

    iget v3, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    move-object/from16 v2, p0

    move/from16 v4, p3

    move/from16 v5, p5

    move/from16 v6, p7

    move/from16 v7, p9

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v18

    .line 118
    .local v18, "x":F
    move-object/from16 v0, v17

    iget v3, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    move-object/from16 v2, p0

    move/from16 v4, p4

    move/from16 v5, p6

    move/from16 v6, p8

    move/from16 v7, p10

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v8

    .line 119
    .local v8, "y":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    move/from16 v0, v18

    invoke-virtual {v2, v0, v8}, Landroid/graphics/Path;->moveTo(FF)V

    goto :goto_0

    .line 123
    .end local v8    # "y":F
    .end local v18    # "x":F
    :pswitch_1
    move-object/from16 v0, v17

    iget v3, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    move-object/from16 v2, p0

    move/from16 v4, p3

    move/from16 v5, p5

    move/from16 v6, p7

    move/from16 v7, p9

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v18

    .line 124
    .restart local v18    # "x":F
    move-object/from16 v0, v17

    iget v3, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    move-object/from16 v2, p0

    move/from16 v4, p4

    move/from16 v5, p6

    move/from16 v6, p8

    move/from16 v7, p10

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v8

    .line 125
    .restart local v8    # "y":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    move/from16 v0, v18

    invoke-virtual {v2, v0, v8}, Landroid/graphics/Path;->lineTo(FF)V

    goto :goto_0

    .line 129
    .end local v8    # "y":F
    .end local v18    # "x":F
    :pswitch_2
    move-object/from16 v0, v17

    iget v3, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    move-object/from16 v2, p0

    move/from16 v4, p3

    move/from16 v5, p5

    move/from16 v6, p7

    move/from16 v7, p9

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v18

    .line 130
    .restart local v18    # "x":F
    move-object/from16 v0, v17

    iget v3, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    move-object/from16 v2, p0

    move/from16 v4, p4

    move/from16 v5, p6

    move/from16 v6, p8

    move/from16 v7, p10

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v8

    .line 131
    .restart local v8    # "y":F
    move-object/from16 v0, v17

    iget v3, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x1:F

    move-object/from16 v2, p0

    move/from16 v4, p3

    move/from16 v5, p5

    move/from16 v6, p7

    move/from16 v7, p9

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v19

    .line 132
    .local v19, "x1":F
    move-object/from16 v0, v17

    iget v3, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y1:F

    move-object/from16 v2, p0

    move/from16 v4, p4

    move/from16 v5, p6

    move/from16 v6, p8

    move/from16 v7, p10

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v13

    .line 133
    .local v13, "y1":F
    move-object/from16 v0, v17

    iget v3, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x2:F

    move-object/from16 v2, p0

    move/from16 v4, p3

    move/from16 v5, p5

    move/from16 v6, p7

    move/from16 v7, p9

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v20

    .line 134
    .local v20, "x2":F
    move-object/from16 v0, v17

    iget v3, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y2:F

    move-object/from16 v2, p0

    move/from16 v4, p4

    move/from16 v5, p6

    move/from16 v6, p8

    move/from16 v7, p10

    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v6

    .line 135
    .local v6, "y2":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    move/from16 v3, v19

    move v4, v13

    move/from16 v5, v20

    move/from16 v7, v18

    invoke-virtual/range {v2 .. v8}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    goto/16 :goto_0

    .line 139
    .end local v6    # "y2":F
    .end local v8    # "y":F
    .end local v13    # "y1":F
    .end local v18    # "x":F
    .end local v19    # "x1":F
    .end local v20    # "x2":F
    :pswitch_3
    move-object/from16 v0, v17

    iget v10, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    move-object/from16 v9, p0

    move/from16 v11, p3

    move/from16 v12, p5

    move/from16 v13, p7

    move/from16 v14, p9

    invoke-virtual/range {v9 .. v14}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v18

    .line 140
    .restart local v18    # "x":F
    move-object/from16 v0, v17

    iget v10, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    move-object/from16 v9, p0

    move/from16 v11, p4

    move/from16 v12, p6

    move/from16 v13, p8

    move/from16 v14, p10

    invoke-virtual/range {v9 .. v14}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v8

    .line 141
    .restart local v8    # "y":F
    move-object/from16 v0, v17

    iget v10, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x1:F

    move-object/from16 v9, p0

    move/from16 v11, p3

    move/from16 v12, p5

    move/from16 v13, p7

    move/from16 v14, p9

    invoke-virtual/range {v9 .. v14}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v19

    .line 142
    .restart local v19    # "x1":F
    move-object/from16 v0, v17

    iget v10, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y1:F

    move-object/from16 v9, p0

    move/from16 v11, p4

    move/from16 v12, p6

    move/from16 v13, p8

    move/from16 v14, p10

    invoke-virtual/range {v9 .. v14}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v13

    .line 143
    .restart local v13    # "y1":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    move/from16 v0, v19

    move/from16 v1, v18

    invoke-virtual {v2, v0, v13, v1, v8}, Landroid/graphics/Path;->quadTo(FFFF)V

    goto/16 :goto_0

    .line 147
    .end local v8    # "y":F
    .end local v13    # "y1":F
    .end local v18    # "x":F
    .end local v19    # "x1":F
    :pswitch_4
    move-object/from16 v0, v17

    iget v10, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    move-object/from16 v9, p0

    move/from16 v11, p3

    move/from16 v12, p5

    move/from16 v13, p7

    move/from16 v14, p9

    invoke-virtual/range {v9 .. v14}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v18

    .line 148
    .restart local v18    # "x":F
    move-object/from16 v0, v17

    iget v10, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    move-object/from16 v9, p0

    move/from16 v11, p4

    move/from16 v12, p6

    move/from16 v13, p8

    move/from16 v14, p10

    invoke-virtual/range {v9 .. v14}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v8

    .line 149
    .restart local v8    # "y":F
    move-object/from16 v0, v17

    iget v10, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x1:F

    move-object/from16 v9, p0

    move/from16 v11, p3

    move/from16 v12, p5

    move/from16 v13, p7

    move/from16 v14, p9

    invoke-virtual/range {v9 .. v14}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v19

    .line 150
    .restart local v19    # "x1":F
    move-object/from16 v0, v17

    iget v10, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y1:F

    move-object/from16 v9, p0

    move/from16 v11, p4

    move/from16 v12, p6

    move/from16 v13, p8

    move/from16 v14, p10

    invoke-virtual/range {v9 .. v14}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->adjustNinePatchPosition(FFFFF)F

    move-result v13

    .line 151
    .restart local v13    # "y1":F
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    move-object/from16 v0, v17

    iget v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x2:F

    move-object/from16 v0, v17

    iget v15, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y2:F

    move/from16 v10, v18

    move v11, v8

    move/from16 v12, v19

    invoke-virtual/range {v9 .. v15}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    goto/16 :goto_0

    .line 155
    .end local v8    # "y":F
    .end local v13    # "y1":F
    .end local v18    # "x":F
    .end local v19    # "x1":F
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    goto/16 :goto_0

    .line 160
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v17    # "p":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;

    iget-object v3, v3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    invoke-virtual {v2, v3}, Landroid/graphics/Path;->set(Landroid/graphics/Path;)V

    .line 162
    :cond_1
    return-void

    .line 115
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public toSPR(Ljava/io/DataOutputStream;)V
    .locals 4
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 336
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 338
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->mPathInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;

    .line 339
    .local v1, "pathInfo":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    iget-byte v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->type:B

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 340
    iget-byte v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->type:B

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 343
    :pswitch_0
    iget v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 344
    iget v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeFloat(F)V

    goto :goto_0

    .line 348
    :pswitch_1
    iget v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x1:F

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 349
    iget v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y1:F

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 350
    iget v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x2:F

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 351
    iget v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y2:F

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 352
    iget v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 353
    iget v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeFloat(F)V

    goto :goto_0

    .line 357
    :pswitch_2
    iget v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x1:F

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 358
    iget v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y1:F

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 359
    iget v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->x:F

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 360
    iget v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;->y:F

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeFloat(F)V

    goto :goto_0

    .line 365
    :pswitch_3
    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 366
    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 367
    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 368
    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 369
    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 370
    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 371
    invoke-virtual {p1, v3}, Ljava/io/DataOutputStream;->writeFloat(F)V

    goto :goto_0

    .line 379
    .end local v1    # "pathInfo":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath$PathInfo;
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->toSPR(Ljava/io/DataOutputStream;)V

    .line 380
    return-void

    .line 340
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
