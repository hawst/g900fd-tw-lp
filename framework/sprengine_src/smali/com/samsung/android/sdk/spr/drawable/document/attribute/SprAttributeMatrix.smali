.class public Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;
.super Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
.source "SprAttributeMatrix.java"


# instance fields
.field private final mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;

.field public matrix:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 16
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/graphics/Matrix;)V
    .locals 1
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 20
    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 21
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;

    .line 22
    iput-object p1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;->matrix:Landroid/graphics/Matrix;

    .line 23
    return-void
.end method

.method public constructor <init>(Ljava/io/DataInputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 26
    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 27
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;

    .line 28
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;->fromSPR(Ljava/io/DataInputStream;)V

    .line 29
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;->clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;

    .line 57
    .local v0, "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;
    new-instance v1, Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;->matrix:Landroid/graphics/Matrix;

    invoke-direct {v1, v2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    iput-object v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;->matrix:Landroid/graphics/Matrix;

    .line 59
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;->clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;

    move-result-object v0

    return-object v0
.end method

.method public fromSPR(Ljava/io/DataInputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 33
    invoke-static {p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprMatrix;->fromSPR(Ljava/io/DataInputStream;)Landroid/graphics/Matrix;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;->matrix:Landroid/graphics/Matrix;

    .line 34
    return-void
.end method

.method public getSPRSize()I
    .locals 1

    .prologue
    .line 43
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->getSPRSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x18

    return v0
.end method

.method public setDisplayLayout(ZFFFFFFFFF)V
    .locals 11
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "npLeft"    # F
    .param p4, "npTop"    # F
    .param p5, "npRight"    # F
    .param p6, "npBottom"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "displayWidth"    # F
    .param p10, "displayHeight"    # F

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;

    iget-object v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;->matrix:Landroid/graphics/Matrix;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    invoke-static/range {v0 .. v10}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprMatrix;->setDisplayLayout(Landroid/graphics/Matrix;ZFFFFFFFFF)Landroid/graphics/Matrix;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;->matrix:Landroid/graphics/Matrix;

    .line 52
    return-void
.end method

.method public toSPR(Ljava/io/DataOutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;->matrix:Landroid/graphics/Matrix;

    invoke-static {p1, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprMatrix;->toSPR(Ljava/io/DataOutputStream;Landroid/graphics/Matrix;)V

    .line 39
    return-void
.end method
