.class final Lcom/samsung/android/sdk/spr/drawable/SprDrawable$1;
.super Lcom/samsung/android/sdk/spr/drawable/SprDrawable;
.source "SprDrawable.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->getErrorDrawable(Ljava/lang/String;)Lcom/samsung/android/sdk/spr/drawable/SprDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .prologue
    .line 548
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;)V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v6, 0x1

    const/high16 v5, 0x42200000    # 40.0f

    const/high16 v4, 0x41a00000    # 20.0f

    const/high16 v3, 0x40a00000    # 5.0f

    .line 551
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 553
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 554
    .local v0, "textOutlinePaint":Landroid/graphics/Paint;
    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 555
    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 556
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 557
    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 558
    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 560
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 561
    .local v1, "textPaint":Landroid/graphics/Paint;
    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 562
    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 563
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 564
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 566
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$1;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget-object v2, v2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3, v5, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 567
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$1;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget-object v2, v2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3, v5, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 568
    return-void
.end method
