.class public Lcom/samsung/android/sdk/spr/drawable/SprDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "SprDrawable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;
    }
.end annotation


# static fields
.field private static mDeviceDensity:F = 0.0f

.field private static final mGetLayoutDirection:Ljava/lang/reflect/Method;

.field private static final mUpdateTintFilter:Ljava/lang/reflect/Method;

.field private static final mVersion:I = 0x1


# instance fields
.field private mAllowCaching:Z

.field private mColorFilter:Landroid/graphics/ColorFilter;

.field protected mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

.field private mMutated:Z

.field private mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

.field private mTintFilter:Landroid/graphics/PorterDuffColorFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 48
    const/4 v1, 0x0

    sput v1, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDeviceDensity:F

    .line 59
    const/4 v0, 0x0

    .line 61
    .local v0, "md":Ljava/lang/reflect/Method;
    :try_start_0
    const-class v1, Landroid/graphics/drawable/Drawable;

    const-string v2, "updateTintFilter"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/graphics/PorterDuffColorFilter;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-class v5, Landroid/content/res/ColorStateList;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-class v5, Landroid/graphics/PorterDuff$Mode;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 65
    :goto_0
    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mUpdateTintFilter:Ljava/lang/reflect/Method;

    .line 68
    :try_start_1
    const-class v1, Landroid/graphics/drawable/Drawable;

    const-string v2, "getLayoutDirection"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 71
    :goto_1
    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mGetLayoutDirection:Ljava/lang/reflect/Method;

    .line 72
    return-void

    .line 69
    :catch_0
    move-exception v1

    goto :goto_1

    .line 63
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 74
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 45
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .line 47
    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mMutated:Z

    .line 49
    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mAllowCaching:Z

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)V
    .locals 6
    .param p1, "state"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 101
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 45
    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .line 46
    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .line 47
    iput-boolean v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mMutated:Z

    .line 49
    iput-boolean v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mAllowCaching:Z

    .line 103
    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z
    invoke-static {p1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$000(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 104
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;-><init>(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .line 106
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    invoke-static {v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$100(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->clone()Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v1, v1, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLeft:F

    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v2}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v2, v2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mTop:F

    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v3

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v3, v3, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mRight:F

    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v4}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v4

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v4, v4, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mBottom:F

    iget-object v5, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v5}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-super {p0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 119
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 111
    .end local v0    # "e":Ljava/lang/CloneNotSupportedException;
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .line 112
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    invoke-static {v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$100(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;)V
    .locals 6
    .param p1, "document"    # Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 45
    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .line 46
    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .line 47
    iput-boolean v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mMutated:Z

    .line 49
    iput-boolean v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mAllowCaching:Z

    .line 83
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .line 84
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$000(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    invoke-static {v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$100(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->clone()Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v1, v1, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLeft:F

    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v2}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v2, v2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mTop:F

    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v3

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v3, v3, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mRight:F

    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v4}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v4

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v4, v4, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mBottom:F

    iget-object v5, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v5}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v5

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-super {p0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 98
    return-void

    .line 87
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 91
    .end local v0    # "e":Ljava/lang/CloneNotSupportedException;
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    invoke-static {v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$100(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    goto :goto_0
.end method

.method static synthetic access$1100()F
    .locals 1

    .prologue
    .line 44
    invoke-static {}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->getDeviceDensityScale()F

    move-result v0

    return v0
.end method

.method public static createFromPathName(Ljava/lang/String;)Lcom/samsung/android/sdk/spr/drawable/SprDrawable;
    .locals 6
    .param p0, "pathName"    # Ljava/lang/String;

    .prologue
    .line 605
    const/4 v3, 0x0

    .line 607
    .local v3, "is":Ljava/io/InputStream;
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 608
    .end local v3    # "is":Ljava/io/InputStream;
    .local v4, "is":Ljava/io/InputStream;
    :try_start_1
    invoke-static {p0, v4}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->createFromStreamInternal(Ljava/lang/String;Ljava/io/InputStream;)Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v0

    .line 609
    .local v0, "document":Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 611
    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;

    invoke-direct {v5, v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-object v3, v4

    .line 621
    .end local v0    # "document":Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "is":Ljava/io/InputStream;
    :goto_0
    return-object v5

    .line 612
    :catch_0
    move-exception v1

    .line 613
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    if-eqz v3, :cond_0

    .line 615
    :try_start_2
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 620
    :cond_0
    :goto_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 621
    invoke-static {p0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->getErrorDrawable(Ljava/lang/String;)Lcom/samsung/android/sdk/spr/drawable/SprDrawable;

    move-result-object v5

    goto :goto_0

    .line 616
    :catch_1
    move-exception v2

    .line 617
    .local v2, "e1":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 612
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v3    # "is":Ljava/io/InputStream;
    .restart local v4    # "is":Ljava/io/InputStream;
    :catch_2
    move-exception v1

    move-object v3, v4

    .end local v4    # "is":Ljava/io/InputStream;
    .restart local v3    # "is":Ljava/io/InputStream;
    goto :goto_1
.end method

.method public static createFromResourceStream(Landroid/content/res/Resources;I)Lcom/samsung/android/sdk/spr/drawable/SprDrawable;
    .locals 4
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "resId"    # I

    .prologue
    .line 593
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    .line 594
    .local v2, "is":Ljava/io/InputStream;
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->createFromStreamInternal(Ljava/lang/String;Ljava/io/InputStream;)Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v0

    .line 595
    .local v0, "document":Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 597
    new-instance v3, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;

    invoke-direct {v3, v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 600
    .end local v0    # "document":Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    .end local v2    # "is":Ljava/io/InputStream;
    :goto_0
    return-object v3

    .line 598
    :catch_0
    move-exception v1

    .line 599
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 600
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->getErrorDrawable(Ljava/lang/String;)Lcom/samsung/android/sdk/spr/drawable/SprDrawable;

    move-result-object v3

    goto :goto_0
.end method

.method public static createFromStream(Ljava/io/InputStream;)Lcom/samsung/android/sdk/spr/drawable/SprDrawable;
    .locals 3
    .param p0, "is"    # Ljava/io/InputStream;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 575
    :try_start_0
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;

    const-string v2, "n/a"

    invoke-static {v2, p0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->createFromStreamInternal(Ljava/lang/String;Ljava/io/InputStream;)Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 578
    :goto_0
    return-object v1

    .line 576
    :catch_0
    move-exception v0

    .line 577
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 578
    const-string v1, "n/a"

    invoke-static {v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->getErrorDrawable(Ljava/lang/String;)Lcom/samsung/android/sdk/spr/drawable/SprDrawable;

    move-result-object v1

    goto :goto_0
.end method

.method public static createFromStream(Ljava/lang/String;Ljava/io/InputStream;)Lcom/samsung/android/sdk/spr/drawable/SprDrawable;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 584
    :try_start_0
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;

    invoke-static {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->createFromStreamInternal(Ljava/lang/String;Ljava/io/InputStream;)Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 587
    :goto_0
    return-object v1

    .line 585
    :catch_0
    move-exception v0

    .line 586
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 587
    invoke-static {p0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->getErrorDrawable(Ljava/lang/String;)Lcom/samsung/android/sdk/spr/drawable/SprDrawable;

    move-result-object v1

    goto :goto_0
.end method

.method private static createFromStreamInternal(Ljava/lang/String;Ljava/io/InputStream;)Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    .locals 12
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x53

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v7, 0x3

    const/4 v8, 0x1

    .line 444
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 446
    .local v0, "bis":Ljava/io/BufferedInputStream;
    new-array v4, v7, [B

    .line 448
    .local v4, "header":[B
    invoke-virtual {v0, v7}, Ljava/io/BufferedInputStream;->mark(I)V

    .line 449
    invoke-virtual {v0, v4}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v6

    if-ge v6, v7, :cond_0

    .line 450
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->close()V

    .line 451
    new-instance v6, Ljava/io/IOException;

    const-string v7, "file is too short"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 453
    :cond_0
    invoke-virtual {v0}, Ljava/io/BufferedInputStream;->reset()V

    .line 457
    aget-byte v6, v4, v9

    if-ne v6, v11, :cond_1

    aget-byte v6, v4, v8

    const/16 v7, 0x56

    if-ne v6, v7, :cond_1

    aget-byte v6, v4, v10

    const/16 v7, 0x46

    if-eq v6, v7, :cond_2

    :cond_1
    aget-byte v6, v4, v9

    if-ne v6, v11, :cond_3

    aget-byte v6, v4, v8

    const/16 v7, 0x50

    if-ne v6, v7, :cond_3

    aget-byte v6, v4, v10

    const/16 v7, 0x52

    if-ne v6, v7, :cond_3

    .line 459
    :cond_2
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    invoke-direct {v1, p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;-><init>(Ljava/lang/String;Ljava/io/InputStream;)V

    .line 471
    .local v1, "document":Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    :goto_0
    return-object v1

    .line 462
    .end local v1    # "document":Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    :cond_3
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v3

    .line 463
    .local v3, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 464
    invoke-virtual {v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    .line 465
    .local v5, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v6, 0x0

    invoke-interface {v5, v0, v6}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 466
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    invoke-direct {v1, p0, v5}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;-><init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v1    # "document":Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    goto :goto_0

    .line 467
    .end local v1    # "document":Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v5    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    :catch_0
    move-exception v2

    .line 468
    .local v2, "e":Lorg/xmlpull/v1/XmlPullParserException;
    new-instance v6, Ljava/io/IOException;

    invoke-virtual {v2}, Lorg/xmlpull/v1/XmlPullParserException;->getCause()Ljava/lang/Throwable;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v6
.end method

.method private static getDeviceDensityScale()F
    .locals 8

    .prologue
    .line 703
    sget v4, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDeviceDensity:F

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-nez v4, :cond_0

    .line 705
    :try_start_0
    const-string v4, "android.os.SystemProperties"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 706
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v4, "getInt"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 707
    .local v3, "md":Ljava/lang/reflect/Method;
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "ro.sf.lcd_density"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/16 v6, 0xa0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 708
    .local v1, "density":I
    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "qemu.sf.lcd_density"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 710
    div-int/lit16 v4, v1, 0xa0

    int-to-float v4, v4

    sput v4, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDeviceDensity:F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 717
    .end local v1    # "density":I
    .end local v3    # "md":Ljava/lang/reflect/Method;
    :cond_0
    :goto_0
    sget v4, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDeviceDensity:F

    return v4

    .line 711
    :catch_0
    move-exception v2

    .line 712
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 713
    const/high16 v4, 0x3f800000    # 1.0f

    sput v4, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDeviceDensity:F

    goto :goto_0
.end method

.method private static getErrorDrawable(Ljava/lang/String;)Lcom/samsung/android/sdk/spr/drawable/SprDrawable;
    .locals 14
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    const/16 v11, 0xc8

    const/16 v10, 0xff

    .line 475
    const/4 v9, 0x5

    .line 477
    .local v9, "size":I
    new-instance v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    const/16 v1, 0x15e

    int-to-float v4, v1

    const/16 v1, 0x113

    int-to-float v5, v1

    move-object v1, p0

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;-><init>(Ljava/lang/String;FFFF)V

    .line 478
    .local v0, "document":Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    const/16 v1, 0x32

    int-to-float v1, v1

    int-to-float v3, v11

    invoke-direct {v8, v2, v2, v1, v3}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(FFFF)V

    .line 479
    .local v8, "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    invoke-static {v10, v11, v11, v11}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v1, v13, v3}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(BI)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V

    .line 480
    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 482
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .end local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    const/16 v1, 0x32

    int-to-float v1, v1

    const/16 v3, 0x64

    int-to-float v3, v3

    int-to-float v4, v11

    invoke-direct {v8, v1, v2, v3, v4}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(FFFF)V

    .line 483
    .restart local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    const/16 v3, -0x100

    invoke-direct {v1, v13, v3}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(BI)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V

    .line 484
    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 486
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .end local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    const/16 v1, 0x64

    int-to-float v1, v1

    const/16 v3, 0x96

    int-to-float v3, v3

    int-to-float v4, v11

    invoke-direct {v8, v1, v2, v3, v4}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(FFFF)V

    .line 487
    .restart local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    invoke-static {v10, v12, v10, v10}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v1, v13, v3}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(BI)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V

    .line 488
    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 490
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .end local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    const/16 v1, 0x96

    int-to-float v1, v1

    int-to-float v3, v11

    int-to-float v4, v11

    invoke-direct {v8, v1, v2, v3, v4}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(FFFF)V

    .line 491
    .restart local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    invoke-static {v10, v12, v10, v12}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v1, v13, v3}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(BI)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V

    .line 492
    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 494
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .end local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    int-to-float v1, v11

    const/16 v3, 0xfa

    int-to-float v3, v3

    int-to-float v4, v11

    invoke-direct {v8, v1, v2, v3, v4}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(FFFF)V

    .line 495
    .restart local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    invoke-static {v10, v10, v12, v10}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v1, v13, v3}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(BI)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V

    .line 496
    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 498
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .end local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    const/16 v1, 0xfa

    int-to-float v1, v1

    const/16 v3, 0x12c

    int-to-float v3, v3

    int-to-float v4, v11

    invoke-direct {v8, v1, v2, v3, v4}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(FFFF)V

    .line 499
    .restart local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    invoke-static {v10, v10, v12, v12}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v1, v13, v3}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(BI)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V

    .line 500
    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 502
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .end local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    const/16 v1, 0x12c

    int-to-float v1, v1

    const/16 v3, 0x15e

    int-to-float v3, v3

    int-to-float v4, v11

    invoke-direct {v8, v1, v2, v3, v4}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(FFFF)V

    .line 503
    .restart local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    invoke-static {v10, v12, v12, v10}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v1, v13, v3}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(BI)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V

    .line 504
    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 506
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .end local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    int-to-float v1, v11

    const/16 v3, 0x32

    int-to-float v3, v3

    const/16 v4, 0xe1

    int-to-float v4, v4

    invoke-direct {v8, v2, v1, v3, v4}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(FFFF)V

    .line 507
    .restart local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    invoke-static {v10, v12, v12, v10}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v1, v13, v3}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(BI)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V

    .line 508
    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 510
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .end local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    const/16 v1, 0x32

    int-to-float v1, v1

    int-to-float v3, v11

    const/16 v4, 0x64

    int-to-float v4, v4

    const/16 v5, 0xe1

    int-to-float v5, v5

    invoke-direct {v8, v1, v3, v4, v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(FFFF)V

    .line 511
    .restart local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    invoke-static {v10, v12, v12, v12}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v1, v13, v3}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(BI)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V

    .line 512
    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 514
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .end local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    const/16 v1, 0x64

    int-to-float v1, v1

    int-to-float v3, v11

    const/16 v4, 0x96

    int-to-float v4, v4

    const/16 v5, 0xe1

    int-to-float v5, v5

    invoke-direct {v8, v1, v3, v4, v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(FFFF)V

    .line 515
    .restart local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    invoke-static {v10, v10, v12, v10}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v1, v13, v3}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(BI)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V

    .line 516
    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 518
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .end local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    const/16 v1, 0x96

    int-to-float v1, v1

    int-to-float v3, v11

    int-to-float v4, v11

    const/16 v5, 0xe1

    int-to-float v5, v5

    invoke-direct {v8, v1, v3, v4, v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(FFFF)V

    .line 519
    .restart local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    invoke-static {v10, v12, v12, v12}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v1, v13, v3}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(BI)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V

    .line 520
    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 522
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .end local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    int-to-float v1, v11

    int-to-float v3, v11

    const/16 v4, 0xfa

    int-to-float v4, v4

    const/16 v5, 0xe1

    int-to-float v5, v5

    invoke-direct {v8, v1, v3, v4, v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(FFFF)V

    .line 523
    .restart local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    invoke-static {v10, v12, v10, v10}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v1, v13, v3}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(BI)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V

    .line 524
    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 526
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .end local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    const/16 v1, 0xfa

    int-to-float v1, v1

    int-to-float v3, v11

    const/16 v4, 0x12c

    int-to-float v4, v4

    const/16 v5, 0xe1

    int-to-float v5, v5

    invoke-direct {v8, v1, v3, v4, v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(FFFF)V

    .line 527
    .restart local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    invoke-static {v10, v12, v12, v12}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v1, v13, v3}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(BI)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V

    .line 528
    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 530
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .end local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    const/16 v1, 0x12c

    int-to-float v1, v1

    int-to-float v3, v11

    const/16 v4, 0x15e

    int-to-float v4, v4

    const/16 v5, 0xe1

    int-to-float v5, v5

    invoke-direct {v8, v1, v3, v4, v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(FFFF)V

    .line 531
    .restart local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    invoke-static {v10, v11, v11, v11}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-direct {v1, v13, v3}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(BI)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V

    .line 532
    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 534
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .end local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    const/16 v1, 0xe1

    int-to-float v1, v1

    const/16 v3, 0x15e

    int-to-float v3, v3

    const/16 v4, 0x113

    int-to-float v4, v4

    invoke-direct {v8, v2, v1, v3, v4}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(FFFF)V

    .line 535
    .restart local v8    # "object1":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
    new-instance v7, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;

    invoke-direct {v7}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;-><init>()V

    .line 536
    .local v7, "l":Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;
    iput-byte v13, v7, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->spreadMode:B

    .line 537
    iput v2, v7, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x1:F

    .line 538
    const/16 v1, 0xe1

    int-to-float v1, v1

    iput v1, v7, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y1:F

    .line 539
    const/16 v1, 0x15e

    int-to-float v1, v1

    iput v1, v7, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x2:F

    .line 540
    const/16 v1, 0xe1

    int-to-float v1, v1

    iput v1, v7, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y2:F

    .line 541
    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    iput-object v1, v7, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->colors:[I

    .line 542
    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_1

    iput-object v1, v7, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->positions:[F

    .line 543
    invoke-virtual {v7}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->updateGradient()V

    .line 544
    new-instance v6, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    const/4 v1, 0x3

    invoke-direct {v6, v1, v7}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(BLcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;)V

    .line 545
    .local v6, "fill":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;
    invoke-virtual {v8, v6}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V

    .line 546
    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 548
    new-instance v1, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$1;

    invoke-direct {v1, v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$1;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;)V

    return-object v1

    .line 541
    nop

    :array_0
    .array-data 4
        -0x1
        -0x1000000
    .end array-data

    .line 542
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static getVersion()I
    .locals 1

    .prologue
    .line 893
    const/4 v0, 0x1

    return v0
.end method

.method private isIntersect(FFFFFFFF)Z
    .locals 7
    .param p1, "ax"    # F
    .param p2, "ay"    # F
    .param p3, "bx"    # F
    .param p4, "by"    # F
    .param p5, "cx"    # F
    .param p6, "cy"    # F
    .param p7, "dx"    # F
    .param p8, "dy"    # F

    .prologue
    .line 132
    invoke-static {p1, p3}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-static {p5, p7}, Ljava/lang/Math;->min(FF)F

    move-result v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_0

    .line 133
    const/4 v4, 0x0

    .line 159
    :goto_0
    return v4

    .line 135
    :cond_0
    invoke-static {p1, p3}, Ljava/lang/Math;->min(FF)F

    move-result v4

    invoke-static {p5, p7}, Ljava/lang/Math;->max(FF)F

    move-result v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_1

    .line 136
    const/4 v4, 0x0

    goto :goto_0

    .line 138
    :cond_1
    invoke-static {p2, p4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-static {p6, p8}, Ljava/lang/Math;->min(FF)F

    move-result v5

    cmpg-float v4, v4, v5

    if-gez v4, :cond_2

    .line 139
    const/4 v4, 0x0

    goto :goto_0

    .line 141
    :cond_2
    invoke-static {p2, p4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    invoke-static {p6, p8}, Ljava/lang/Math;->max(FF)F

    move-result v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_3

    .line 142
    const/4 v4, 0x0

    goto :goto_0

    .line 145
    :cond_3
    sub-float v4, p6, p2

    sub-float v5, p3, p1

    mul-float/2addr v4, v5

    sub-float v5, p5, p1

    sub-float v6, p4, p2

    mul-float/2addr v5, v6

    sub-float v0, v4, v5

    .line 146
    .local v0, "temp1":F
    sub-float v4, p8, p2

    sub-float v5, p3, p1

    mul-float/2addr v4, v5

    sub-float v5, p7, p1

    sub-float v6, p4, p2

    mul-float/2addr v5, v6

    sub-float v1, v4, v5

    .line 148
    .local v1, "temp2":F
    mul-float v4, v0, v1

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_4

    .line 149
    const/4 v4, 0x0

    goto :goto_0

    .line 152
    :cond_4
    sub-float v4, p2, p6

    sub-float v5, p7, p5

    mul-float/2addr v4, v5

    sub-float v5, p1, p5

    sub-float v6, p8, p6

    mul-float/2addr v5, v6

    sub-float v2, v4, v5

    .line 153
    .local v2, "temp3":F
    sub-float v4, p4, p6

    sub-float v5, p7, p5

    mul-float/2addr v4, v5

    sub-float v5, p3, p5

    sub-float v6, p8, p6

    mul-float/2addr v5, v6

    sub-float v3, v4, v5

    .line 155
    .local v3, "temp4":F
    mul-float v4, v2, v3

    const/4 v5, 0x0

    cmpl-float v4, v4, v5

    if-lez v4, :cond_5

    .line 156
    const/4 v4, 0x0

    goto :goto_0

    .line 159
    :cond_5
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private needMirroring()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 899
    :try_start_0
    sget-object v2, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mGetLayoutDirection:Ljava/lang/reflect/Method;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2, p0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 903
    .local v0, "direction":I
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->isAutoMirrored()Z

    move-result v2

    if-eqz v2, :cond_0

    if-ne v0, v3, :cond_0

    move v2, v3

    .end local v0    # "direction":I
    :goto_0
    return v2

    .line 900
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/lang/Exception;
    move v2, v4

    .line 901
    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "direction":I
    :cond_0
    move v2, v4

    .line 903
    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 16
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 164
    invoke-static {}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererFactory;->getInstance()Lcom/samsung/android/sdk/spr/drawable/engine/SprRenderer;

    move-result-object v1

    .line 166
    .local v1, "renderer":Lcom/samsung/android/sdk/spr/drawable/engine/SprRenderer;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v11

    .line 168
    .local v11, "bounds":Landroid/graphics/Rect;
    invoke-virtual {v11}, Landroid/graphics/Rect;->width()I

    move-result v3

    if-lez v3, :cond_0

    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-gtz v3, :cond_1

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 173
    iget v3, v11, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget v4, v11, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 175
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->needMirroring()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 176
    invoke-virtual {v11}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 177
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->scale(FF)V

    .line 180
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$300(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Paint;

    move-result-object v15

    .line 182
    .local v15, "paint":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mColorFilter:Landroid/graphics/ColorFilter;

    if-nez v3, :cond_d

    .line 183
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mTintFilter:Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {v15, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 188
    :goto_1
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    monitor-enter v8

    .line 189
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$400(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 190
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$000(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 191
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->isIntrinsic()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_3

    .line 193
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->clone()Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 199
    :cond_3
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z
    invoke-static {v4}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$000(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Z

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v5}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v5

    iget v6, v11, Landroid/graphics/Rect;->right:I

    iget v7, v11, Landroid/graphics/Rect;->left:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    iget v7, v11, Landroid/graphics/Rect;->bottom:I

    iget v9, v11, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v9

    int-to-float v7, v7

    invoke-virtual {v3, v4, v5, v6, v7}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->setDisplayLayout(ZFFF)V

    .line 202
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v4, v11, Landroid/graphics/Rect;->right:I

    iget v5, v11, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v5

    iget v5, v11, Landroid/graphics/Rect;->bottom:I

    iget v6, v11, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v6

    invoke-virtual {v1, v3, v15, v4, v5}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRenderer;->preDraw(Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;Landroid/graphics/Paint;II)V

    .line 204
    invoke-virtual {v15}, Landroid/graphics/Paint;->getAlpha()I

    move-result v3

    const/16 v4, 0xff

    if-lt v3, v4, :cond_4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mAllowCaching:Z

    if-eqz v3, :cond_8

    .line 205
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$500(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Bitmap;

    move-result-object v10

    .line 206
    .local v10, "bm":Landroid/graphics/Bitmap;
    const/4 v13, 0x0

    .line 207
    .local v13, "cacheWidth":I
    const/4 v12, 0x0

    .line 208
    .local v12, "cacheHeight":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$600(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Rect;

    move-result-object v3

    if-nez v3, :cond_e

    .line 209
    invoke-virtual {v11}, Landroid/graphics/Rect;->width()I

    move-result v13

    .line 210
    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v12

    .line 216
    :goto_2
    const/4 v2, 0x0

    .line 218
    .local v2, "tmpCanvas":Landroid/graphics/Canvas;
    if-eqz v10, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->canReuseCache()Z

    move-result v3

    if-nez v3, :cond_10

    .line 219
    :cond_5
    if-lez v12, :cond_6

    if-gtz v12, :cond_f

    .line 220
    :cond_6
    const/4 v10, 0x0

    .line 224
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3, v10}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$502(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 225
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->updateCacheStates()V

    .line 231
    :goto_4
    if-eqz v10, :cond_8

    .line 232
    new-instance v2, Landroid/graphics/Canvas;

    .end local v2    # "tmpCanvas":Landroid/graphics/Canvas;
    invoke-direct {v2, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 233
    .restart local v2    # "tmpCanvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$600(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Rect;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 234
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$600(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    neg-int v3, v3

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$600(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->top:I

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 237
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z
    invoke-static {v4}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$000(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Z

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v5}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v5

    iget v6, v11, Landroid/graphics/Rect;->right:I

    iget v7, v11, Landroid/graphics/Rect;->left:I

    sub-int/2addr v6, v7

    iget v7, v11, Landroid/graphics/Rect;->bottom:I

    iget v9, v11, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v9

    invoke-virtual/range {v1 .. v7}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRenderer;->draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;ZFII)V

    .line 242
    .end local v2    # "tmpCanvas":Landroid/graphics/Canvas;
    .end local v10    # "bm":Landroid/graphics/Bitmap;
    .end local v12    # "cacheHeight":I
    .end local v13    # "cacheWidth":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    const/4 v4, 0x0

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$402(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z

    .line 244
    :cond_9
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 246
    invoke-virtual {v15}, Landroid/graphics/Paint;->getAlpha()I

    move-result v3

    const/16 v4, 0xff

    if-lt v3, v4, :cond_a

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mAllowCaching:Z

    if-eqz v3, :cond_11

    .line 247
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$500(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_c

    .line 248
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$600(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Rect;

    move-result-object v3

    if-eqz v3, :cond_b

    .line 249
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$600(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$600(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 251
    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$500(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Bitmap;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v15}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 257
    :cond_c
    :goto_5
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0

    .line 185
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mColorFilter:Landroid/graphics/ColorFilter;

    invoke-virtual {v15, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto/16 :goto_1

    .line 194
    :catch_0
    move-exception v14

    .line 195
    .local v14, "e":Ljava/lang/CloneNotSupportedException;
    :try_start_3
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v14}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 244
    .end local v14    # "e":Ljava/lang/CloneNotSupportedException;
    :catchall_0
    move-exception v3

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3

    .line 212
    .restart local v10    # "bm":Landroid/graphics/Bitmap;
    .restart local v12    # "cacheHeight":I
    .restart local v13    # "cacheWidth":I
    :cond_e
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$600(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v13

    .line 213
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$600(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v12

    goto/16 :goto_2

    .line 222
    .restart local v2    # "tmpCanvas":Landroid/graphics/Canvas;
    :cond_f
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v13, v12, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v10

    goto/16 :goto_3

    .line 227
    :cond_10
    new-instance v2, Landroid/graphics/Canvas;

    .end local v2    # "tmpCanvas":Landroid/graphics/Canvas;
    invoke-direct {v2, v10}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 228
    .restart local v2    # "tmpCanvas":Landroid/graphics/Canvas;
    const/4 v3, 0x0

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_4

    .line 254
    .end local v2    # "tmpCanvas":Landroid/graphics/Canvas;
    .end local v10    # "bm":Landroid/graphics/Bitmap;
    .end local v12    # "cacheHeight":I
    .end local v13    # "cacheWidth":I
    :cond_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$000(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Z

    move-result v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v7

    iget v3, v11, Landroid/graphics/Rect;->right:I

    iget v4, v11, Landroid/graphics/Rect;->left:I

    sub-int v8, v3, v4

    iget v3, v11, Landroid/graphics/Rect;->bottom:I

    iget v4, v11, Landroid/graphics/Rect;->top:I

    sub-int v9, v3, v4

    move-object v3, v1

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRenderer;->draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;ZFII)V

    goto :goto_5
.end method

.method public getAlpha()I
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$300(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    return v0
.end method

.method public getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 1

    .prologue
    .line 627
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    return-object v0
.end method

.method public getDocument()Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    .locals 1

    .prologue
    .line 886
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    return-object v0
.end method

.method public getIntrinsicHeight()I
    .locals 3

    .prologue
    .line 262
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mBottom:F

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v1, v1, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mTop:F

    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v2}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 3

    .prologue
    .line 268
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mRight:F

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v1, v1, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLeft:F

    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v2}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 330
    const/4 v0, -0x3

    return v0
.end method

.method public getPadding(Landroid/graphics/Rect;)Z
    .locals 6
    .param p1, "padding"    # Landroid/graphics/Rect;

    .prologue
    const/4 v5, 0x0

    .line 319
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingLeft:F

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v1, v1, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingTop:F

    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v2}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v2, v2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingRight:F

    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v3}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v3

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v3, v3, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingBottom:F

    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v4}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v4

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 324
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingLeft:F

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingTop:F

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingRight:F

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingBottom:F

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getState()[I
    .locals 1

    .prologue
    .line 403
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->getState()[I

    move-result-object v0

    .line 405
    .local v0, "result":[I
    return-object v0
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "r"    # Landroid/content/res/Resources;
    .param p2, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p3, "attrs"    # Landroid/util/AttributeSet;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 633
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V

    .line 634
    return-void
.end method

.method public inflate(Landroid/content/res/Resources;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/res/Resources$Theme;)V
    .locals 15
    .param p1, "r"    # Landroid/content/res/Resources;
    .param p2, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p3, "attrs"    # Landroid/util/AttributeSet;
    .param p4, "theme"    # Landroid/content/res/Resources$Theme;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 639
    const-string v9, "http://schemas.android.com/apk/res/android"

    const-string v10, "src"

    const/4 v11, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v9, v10, v11}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v4

    .line 640
    .local v4, "id":I
    if-nez v4, :cond_0

    .line 641
    new-instance v9, Lorg/xmlpull/v1/XmlPullParserException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface/range {p2 .. p2}, Lorg/xmlpull/v1/XmlPullParser;->getPositionDescription()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ": <spr> requires a valid src attribute"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 643
    :cond_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v5

    .line 644
    .local v5, "is":Ljava/io/InputStream;
    new-instance v9, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v5}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->createFromStreamInternal(Ljava/lang/String;Ljava/io/InputStream;)Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;)V

    iput-object v9, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .line 645
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 646
    iget-object v9, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z
    invoke-static {v9}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$000(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 648
    :try_start_0
    iget-object v9, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    invoke-static {v9}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$100(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->clone()Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 656
    :goto_0
    const-string v9, "http://schemas.android.com/apk/res/android"

    const-string v10, "tintMode"

    const/4 v11, -0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v9, v10, v11}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v7

    .line 657
    .local v7, "tintMode":I
    const/4 v9, -0x1

    if-eq v7, v9, :cond_1

    .line 659
    :try_start_1
    iget-object v10, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    const-class v9, Landroid/graphics/drawable/Drawable;

    const-string v11, "parseTintMode"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Class;

    const/4 v13, 0x0

    sget-object v14, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    const-class v14, Landroid/graphics/PorterDuff$Mode;

    aput-object v14, v12, v13

    invoke-virtual {v9, v11, v12}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v9

    const/4 v11, 0x0

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    sget-object v14, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    aput-object v14, v12, v13

    invoke-virtual {v9, v11, v12}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/PorterDuff$Mode;

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTintMode:Landroid/graphics/PorterDuff$Mode;
    invoke-static {v10, v9}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$802(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 666
    :cond_1
    :goto_1
    const/4 v2, 0x0

    .line 668
    .local v2, "csl":Landroid/content/res/ColorStateList;
    const-string v9, "http://schemas.android.com/apk/res/android"

    const-string v10, "tint"

    const/4 v11, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v9, v10, v11}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v8

    .line 669
    .local v8, "tintResId":I
    if-nez v8, :cond_4

    .line 670
    const-string v9, "http://schemas.android.com/apk/res/android"

    const-string v10, "tint"

    move-object/from16 v0, p3

    invoke-interface {v0, v9, v10}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 672
    .local v6, "tint":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 673
    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v9

    invoke-static {v9}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    .line 679
    .end local v6    # "tint":Ljava/lang/String;
    :cond_2
    :goto_2
    iget-object v9, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTint:Landroid/content/res/ColorStateList;
    invoke-static {v9, v2}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$702(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Landroid/content/res/ColorStateList;)Landroid/content/res/ColorStateList;

    .line 681
    iget-object v9, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    const-string v10, "http://schemas.android.com/apk/res/android"

    const-string v11, "autoMirrored"

    iget-object v12, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mAutoMirrored:Z
    invoke-static {v12}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$900(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Z

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v10, v11, v12}, Landroid/util/AttributeSet;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v10

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mAutoMirrored:Z
    invoke-static {v9, v10}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$902(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z

    .line 684
    const-string v9, "http://schemas.android.com/apk/res/android"

    const-string v10, "alpha"

    iget-object v11, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;
    invoke-static {v11}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$300(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Paint;

    move-result-object v11

    invoke-virtual {v11}, Landroid/graphics/Paint;->getAlpha()I

    move-result v11

    int-to-float v11, v11

    move-object/from16 v0, p3

    invoke-interface {v0, v9, v10, v11}, Landroid/util/AttributeSet;->getAttributeFloatValue(Ljava/lang/String;Ljava/lang/String;F)F

    move-result v9

    const/high16 v10, 0x437f0000    # 255.0f

    mul-float/2addr v9, v10

    float-to-int v1, v9

    .line 686
    .local v1, "alpha":I
    iget-object v9, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;
    invoke-static {v9}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$300(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Paint;

    move-result-object v9

    invoke-virtual {v9, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 687
    iget-object v9, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    const/4 v10, 0x1

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheDirty:Z
    invoke-static {v9, v10}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$1002(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z

    .line 689
    iget-object v9, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v9, v9, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLeft:F

    iget-object v10, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v10}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v10

    mul-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    iget-object v10, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v10, v10, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mTop:F

    iget-object v11, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v11}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v11

    mul-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    iget-object v11, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v11, v11, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mRight:F

    iget-object v12, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v12}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v12

    mul-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    iget-object v12, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v12, v12, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mBottom:F

    iget-object v13, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F
    invoke-static {v13}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F

    move-result v13

    mul-float/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v12

    invoke-super {p0, v9, v10, v11, v12}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 693
    return-void

    .line 649
    .end local v1    # "alpha":I
    .end local v2    # "csl":Landroid/content/res/ColorStateList;
    .end local v7    # "tintMode":I
    .end local v8    # "tintResId":I
    :catch_0
    move-exception v3

    .line 650
    .local v3, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v9, Ljava/lang/RuntimeException;

    invoke-direct {v9, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v9

    .line 653
    .end local v3    # "e":Ljava/lang/CloneNotSupportedException;
    :cond_3
    iget-object v9, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    invoke-static {v9}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$100(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    goto/16 :goto_0

    .line 661
    .restart local v7    # "tintMode":I
    :catch_1
    move-exception v3

    .line 662
    .local v3, "e":Ljava/lang/Exception;
    iget-object v9, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    sget-object v10, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTintMode:Landroid/graphics/PorterDuff$Mode;
    invoke-static {v9, v10}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$802(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;

    goto/16 :goto_1

    .line 676
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v2    # "csl":Landroid/content/res/ColorStateList;
    .restart local v8    # "tintResId":I
    :cond_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    goto/16 :goto_2
.end method

.method public isAllowCaching()Z
    .locals 1

    .prologue
    .line 440
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mAllowCaching:Z

    return v0
.end method

.method public isAutoMirrored()Z
    .locals 1

    .prologue
    .line 916
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mAutoMirrored:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$900(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Z

    move-result v0

    return v0
.end method

.method public isCacheEnabled()Z
    .locals 1

    .prologue
    .line 308
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mAllowCaching:Z

    return v0
.end method

.method public isDrawable(Landroid/graphics/Matrix;)Z
    .locals 14
    .param p1, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x4

    const/4 v2, 0x0

    .line 122
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v13

    .line 123
    .local v13, "bounds":Landroid/graphics/Rect;
    const/16 v0, 0x8

    new-array v3, v0, [F

    iget v0, v13, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    aput v0, v3, v2

    iget v0, v13, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    aput v0, v3, v6

    iget v0, v13, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    aput v0, v3, v7

    iget v0, v13, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    aput v0, v3, v8

    iget v0, v13, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    aput v0, v3, v5

    const/4 v0, 0x5

    iget v4, v13, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    aput v4, v3, v0

    const/4 v0, 0x6

    iget v4, v13, Landroid/graphics/Rect;->left:I

    int-to-float v4, v4

    aput v4, v3, v0

    const/4 v0, 0x7

    iget v4, v13, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    aput v4, v3, v0

    .line 125
    .local v3, "src":[F
    const/16 v0, 0x8

    new-array v1, v0, [F

    .local v1, "dst":[F
    move-object v0, p1

    move v4, v2

    .line 126
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Matrix;->mapPoints([FI[FII)V

    .line 128
    aget v0, v1, v2

    aget v6, v1, v6

    aget v7, v1, v7

    aget v8, v1, v8

    aget v9, v1, v5

    const/4 v2, 0x5

    aget v10, v1, v2

    const/4 v2, 0x6

    aget v11, v1, v2

    const/4 v2, 0x7

    aget v12, v1, v2

    move-object v4, p0

    move v5, v0

    invoke-direct/range {v4 .. v12}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->isIntersect(FFFFFFFF)Z

    move-result v0

    return v0
.end method

.method public isNinePatchEnabled()Z
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$000(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Z

    move-result v0

    return v0
.end method

.method public isStateful()Z
    .locals 1

    .prologue
    .line 381
    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTint:Landroid/content/res/ColorStateList;
    invoke-static {v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$700(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/content/res/ColorStateList;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTint:Landroid/content/res/ColorStateList;
    invoke-static {v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$700(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public mutate()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 735
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mMutated:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 736
    new-instance v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;-><init>(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .line 737
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mMutated:Z

    .line 739
    :cond_0
    return-object p0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .prologue
    .line 313
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    .line 314
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$402(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z

    .line 315
    return-void
.end method

.method protected onStateChange([I)Z
    .locals 5
    .param p1, "stateSet"    # [I

    .prologue
    const/4 v1, 0x1

    .line 386
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .line 387
    .local v0, "state":Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;
    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTint:Landroid/content/res/ColorStateList;
    invoke-static {v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$700(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/content/res/ColorStateList;

    move-result-object v2

    if-eqz v2, :cond_0

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTintMode:Landroid/graphics/PorterDuff$Mode;
    invoke-static {v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$800(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 388
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mTintFilter:Landroid/graphics/PorterDuffColorFilter;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTint:Landroid/content/res/ColorStateList;
    invoke-static {v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$700(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/content/res/ColorStateList;

    move-result-object v3

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTintMode:Landroid/graphics/PorterDuff$Mode;
    invoke-static {v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$800(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v4

    invoke-virtual {p0, v2, v3, v4}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->updateTintFilterInternal(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mTintFilter:Landroid/graphics/PorterDuffColorFilter;

    .line 389
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z
    invoke-static {v2, v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$402(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z

    .line 390
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->invalidateSelf()V

    .line 393
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAllowCaching(Z)V
    .locals 2
    .param p1, "allowCaching"    # Z

    .prologue
    .line 430
    iput-boolean p1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mAllowCaching:Z

    .line 431
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;
    invoke-static {v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$300(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    const/16 v1, 0xff

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mAllowCaching:Z

    if-nez v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$502(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 434
    :cond_0
    return-void
.end method

.method public setAlpha(I)V
    .locals 3
    .param p1, "alpha"    # I

    .prologue
    .line 335
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$300(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    .line 336
    .local v0, "oldAlpha":I
    if-eq p1, v0, :cond_1

    .line 337
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$300(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 338
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;
    invoke-static {v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$300(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v1

    const/16 v2, 0xff

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mAllowCaching:Z

    if-nez v1, :cond_0

    .line 339
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$502(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 341
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$402(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z

    .line 343
    :cond_1
    return-void
.end method

.method public setAutoMirrored(Z)V
    .locals 1
    .param p1, "mirrored"    # Z

    .prologue
    .line 908
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mAutoMirrored:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$900(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 909
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mAutoMirrored:Z
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$902(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z

    .line 910
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->invalidateSelf()V

    .line 912
    :cond_0
    return-void
.end method

.method public setCacheEnabled(ZLandroid/graphics/Rect;)V
    .locals 2
    .param p1, "enable"    # Z
    .param p2, "cacheBounds"    # Landroid/graphics/Rect;

    .prologue
    .line 297
    iput-boolean p1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mAllowCaching:Z

    .line 298
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;
    invoke-static {v0, p2}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$602(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Landroid/graphics/Rect;)Landroid/graphics/Rect;

    .line 299
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$402(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z

    .line 300
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 2
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 352
    iput-object p1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mColorFilter:Landroid/graphics/ColorFilter;

    .line 353
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$402(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z

    .line 354
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->invalidateSelf()V

    .line 355
    return-void
.end method

.method public setNinePatchEnabled(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 276
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$002(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z

    .line 277
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$402(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z

    .line 278
    return-void
.end method

.method public setState([I)Z
    .locals 1
    .param p1, "stateSet"    # [I

    .prologue
    .line 398
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    move-result v0

    return v0
.end method

.method public setTintList(Landroid/content/res/ColorStateList;)V
    .locals 3
    .param p1, "tint"    # Landroid/content/res/ColorStateList;

    .prologue
    .line 359
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .line 360
    .local v0, "state":Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;
    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTint:Landroid/content/res/ColorStateList;
    invoke-static {v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$700(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/content/res/ColorStateList;

    move-result-object v1

    if-eq v1, p1, :cond_0

    .line 361
    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTint:Landroid/content/res/ColorStateList;
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$702(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Landroid/content/res/ColorStateList;)Landroid/content/res/ColorStateList;

    .line 362
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mTintFilter:Landroid/graphics/PorterDuffColorFilter;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTintMode:Landroid/graphics/PorterDuff$Mode;
    invoke-static {v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$800(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v2

    invoke-virtual {p0, v1, p1, v2}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->updateTintFilterInternal(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mTintFilter:Landroid/graphics/PorterDuffColorFilter;

    .line 363
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$402(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z

    .line 364
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->invalidateSelf()V

    .line 366
    :cond_0
    return-void
.end method

.method public setTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 3
    .param p1, "tintMode"    # Landroid/graphics/PorterDuff$Mode;

    .prologue
    .line 370
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .line 371
    .local v0, "state":Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;
    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTintMode:Landroid/graphics/PorterDuff$Mode;
    invoke-static {v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$800(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v1

    if-eq v1, p1, :cond_0

    .line 372
    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTintMode:Landroid/graphics/PorterDuff$Mode;
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$802(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;

    .line 373
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mTintFilter:Landroid/graphics/PorterDuffColorFilter;

    # getter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTint:Landroid/content/res/ColorStateList;
    invoke-static {v0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$700(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {p0, v1, v2, p1}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->updateTintFilterInternal(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mTintFilter:Landroid/graphics/PorterDuffColorFilter;

    .line 374
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mState:Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->access$402(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z

    .line 375
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->invalidateSelf()V

    .line 377
    :cond_0
    return-void
.end method

.method public toSPR(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 724
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->toSPR(Ljava/io/OutputStream;)Z

    .line 725
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 697
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v1, v1, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLeft:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v1, v1, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mTop:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v1, v1, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mRight:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v1, v1, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mBottom:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nLoading:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->getLoadingTime()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms\nElement:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->getTotalElementCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nSegment:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->getTotalSegmentCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nAttribute:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->getTotalAttributeCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method updateTintFilterInternal(Landroid/graphics/PorterDuffColorFilter;Landroid/content/res/ColorStateList;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuffColorFilter;
    .locals 6
    .param p1, "tintFilter"    # Landroid/graphics/PorterDuffColorFilter;
    .param p2, "tint"    # Landroid/content/res/ColorStateList;
    .param p3, "tintMode"    # Landroid/graphics/PorterDuff$Mode;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 409
    sget-object v2, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mUpdateTintFilter:Ljava/lang/reflect/Method;

    if-nez v2, :cond_0

    .line 410
    const/4 v1, 0x0

    .line 423
    :goto_0
    return-object v1

    .line 413
    :cond_0
    const/4 v1, 0x0

    .line 415
    .local v1, "result":Landroid/graphics/PorterDuffColorFilter;
    sget-object v2, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mUpdateTintFilter:Ljava/lang/reflect/Method;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 417
    :try_start_0
    sget-object v2, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mUpdateTintFilter:Ljava/lang/reflect/Method;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    aput-object p3, v3, v4

    invoke-virtual {v2, p0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/graphics/PorterDuffColorFilter;

    move-object v1, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 421
    :goto_1
    sget-object v2, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->mUpdateTintFilter:Ljava/lang/reflect/Method;

    invoke-virtual {v2, v5}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    goto :goto_0

    .line 418
    :catch_0
    move-exception v2

    goto :goto_1
.end method
