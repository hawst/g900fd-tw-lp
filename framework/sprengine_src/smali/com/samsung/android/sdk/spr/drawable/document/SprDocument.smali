.class public Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
.super Ljava/lang/Object;
.source "SprDocument.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final DEFAULT_DENSITY_SCALE:F = 3.0f

.field public static final HEADER_SIZE:I = 0x90

.field public static final MAJOR_VERSION:S = 0x3030s

.field public static final MINOR_VERSION:S = 0x3031s

.field public static final RESERVED_SIZE:I = 0x40

.field public static final SPRTAG:I = 0x53505200

.field public static final SVFTAG:I = 0x53564600

.field private static final TAG:Ljava/lang/String; = "SPRDocument"


# instance fields
.field public mBottom:F

.field public final mDensity:F

.field protected final mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

.field private mIsInitialized:Z

.field public mLeft:F

.field private mLoadingTime:J

.field public final mName:Ljava/lang/String;

.field public mNinePatchBottom:F

.field public mNinePatchLeft:F

.field public mNinePatchRight:F

.field public mNinePatchTop:F

.field private mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

.field private mObjectUniqueId:I

.field public mPaddingBottom:F

.field public mPaddingLeft:F

.field public mPaddingRight:F

.field public mPaddingTop:F

.field private mReferenceMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;",
            ">;"
        }
    .end annotation
.end field

.field public mRight:F

.field public mTop:F


# direct methods
.method public constructor <init>(Ljava/lang/String;FFFF)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "left"    # F
    .param p3, "top"    # F
    .param p4, "right"    # F
    .param p5, "bottom"    # F

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    .line 51
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    .line 52
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    .line 54
    iput v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObjectUniqueId:I

    .line 57
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLoadingTime:J

    .line 64
    iput-object p0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .line 65
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mName:Ljava/lang/String;

    .line 67
    iput p2, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLeft:F

    .line 68
    iput p3, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mTop:F

    .line 69
    iput p4, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mRight:F

    .line 70
    iput p5, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mBottom:F

    .line 72
    iput v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchBottom:F

    iput v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchRight:F

    iput v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchTop:F

    iput v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchLeft:F

    .line 73
    iput v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingBottom:F

    iput v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingRight:F

    iput v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingTop:F

    iput v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingLeft:F

    .line 75
    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mDensity:F

    .line 77
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    .line 78
    new-instance v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;-><init>(Z)V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    .line 80
    iput-boolean v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    .line 81
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/InputStream;)V
    .locals 17
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    .line 51
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    .line 52
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    .line 54
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObjectUniqueId:I

    .line 57
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLoadingTime:J

    .line 85
    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .line 86
    const-string v14, "/"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v14

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mName:Ljava/lang/String;

    .line 88
    new-instance v6, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;

    move-object/from16 v0, p2

    invoke-direct {v6, v0}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;-><init>(Ljava/io/InputStream;)V

    .line 91
    .local v6, "in":Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;
    const/16 v14, 0x40

    new-array v9, v14, [B

    .line 93
    .local v9, "reserved":[B
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 95
    .local v12, "start":J
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readInt()I

    move-result v2

    .line 96
    .local v2, "fileTag":I
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readShort()S

    move-result v14

    iput-short v14, v6, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->mMajorVersion:S

    .line 97
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readShort()S

    move-result v14

    iput-short v14, v6, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->mMinorVersion:S

    .line 98
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readInt()I

    move-result v3

    .line 99
    .local v3, "headerSize":I
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readInt()I

    .line 100
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readInt()I

    .line 101
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readInt()I

    .line 102
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readInt()I

    .line 103
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLeft:F

    .line 104
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mTop:F

    .line 105
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mRight:F

    .line 106
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mBottom:F

    .line 107
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchLeft:F

    .line 108
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchTop:F

    .line 109
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchRight:F

    .line 110
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchBottom:F

    .line 111
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingLeft:F

    .line 112
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingTop:F

    .line 113
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingRight:F

    .line 114
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingBottom:F

    .line 115
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mDensity:F

    .line 116
    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->read([B)I

    move-result v14

    const/16 v15, 0x40

    if-ge v14, v15, :cond_0

    .line 117
    new-instance v14, Ljava/io/IOException;

    const-string v15, "file is too short!!!"

    invoke-direct {v14, v15}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 120
    :cond_0
    const v14, 0x53505200

    if-eq v2, v14, :cond_1

    const v14, 0x53564600

    if-eq v2, v14, :cond_1

    .line 121
    new-instance v14, Ljava/lang/RuntimeException;

    const-string v15, "wrong file format"

    invoke-direct {v14, v15}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 125
    :cond_1
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    add-int/lit16 v14, v3, -0x90

    if-ge v4, v14, :cond_2

    .line 126
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readByte()B

    .line 125
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 129
    :cond_2
    new-instance v14, Landroid/util/SparseArray;

    invoke-direct {v14}, Landroid/util/SparseArray;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    .line 131
    const/4 v4, 0x0

    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readInt()I

    move-result v7

    .local v7, "n":I
    :goto_1
    if-ge v4, v7, :cond_6

    .line 132
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readInt()I

    move-result v5

    .line 135
    .local v5, "id":I
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readByte()B

    move-result v11

    .line 137
    .local v11, "type":B
    const/4 v10, 0x0

    .line 138
    .local v10, "size":I
    iget-short v14, v6, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->mMajorVersion:S

    const/16 v15, 0x3030

    if-lt v14, v15, :cond_3

    iget-short v14, v6, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->mMinorVersion:S

    const/16 v15, 0x3032

    if-lt v14, v15, :cond_3

    .line 139
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readInt()I

    move-result v10

    .line 141
    :cond_3
    sparse-switch v11, :sswitch_data_0

    .line 171
    const/4 v8, 0x0

    .line 172
    .local v8, "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    const-string v14, "SPRDocument"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "unknown element type:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :goto_2
    if-lez v10, :cond_4

    .line 174
    invoke-virtual {v6}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readByte()B

    .line 175
    add-int/lit8 v10, v10, -0x1

    goto :goto_2

    .line 143
    .end local v8    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :sswitch_0
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    const/4 v14, 0x0

    invoke-direct {v8, v14, v6}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;-><init>(ZLcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 180
    .restart local v8    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :cond_4
    :goto_3
    if-eqz v8, :cond_5

    .line 181
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    invoke-virtual {v14, v5, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 131
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 147
    .end local v8    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :sswitch_1
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;

    invoke-direct {v8, v6}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 148
    .restart local v8    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    goto :goto_3

    .line 151
    .end local v8    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :sswitch_2
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    invoke-direct {v8, v6}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 152
    .restart local v8    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    goto :goto_3

    .line 155
    .end local v8    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :sswitch_3
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    invoke-direct {v8, v6}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 156
    .restart local v8    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    goto :goto_3

    .line 159
    .end local v8    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :sswitch_4
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;

    invoke-direct {v8, v6}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 160
    .restart local v8    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    goto :goto_3

    .line 163
    .end local v8    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :sswitch_5
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    invoke-direct {v8, v6}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 164
    .restart local v8    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    goto :goto_3

    .line 167
    .end local v8    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :sswitch_6
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeUse;

    invoke-direct {v8, v6}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeUse;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 168
    .restart local v8    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    goto :goto_3

    .line 185
    .end local v5    # "id":I
    .end local v8    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    .end local v10    # "size":I
    .end local v11    # "type":B
    :cond_6
    new-instance v14, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    const/4 v15, 0x1

    invoke-direct {v14, v15, v6}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;-><init>(ZLcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    .line 187
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    sub-long/2addr v14, v12

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLoadingTime:J

    .line 189
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    .line 190
    return-void

    .line 141
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x10 -> :sswitch_0
        0x11 -> :sswitch_6
    .end sparse-switch
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 11
    .param p1, "docName"    # Ljava/lang/String;
    .param p2, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    .line 51
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    .line 52
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    .line 54
    const/4 v8, 0x1

    iput v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObjectUniqueId:I

    .line 57
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLoadingTime:J

    .line 193
    iput-object p0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .line 194
    const-string v8, "/"

    invoke-virtual {p1, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mName:Ljava/lang/String;

    .line 197
    :cond_0
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    .local v1, "eventType":I
    const/4 v8, 0x2

    if-eq v1, v8, :cond_1

    const/4 v8, 0x1

    if-ne v1, v8, :cond_0

    .line 200
    :cond_1
    const/4 v8, 0x2

    if-eq v1, v8, :cond_2

    .line 201
    new-instance v8, Lorg/xmlpull/v1/XmlPullParserException;

    const-string v9, "No start tag found"

    invoke-direct {v8, v9}, Lorg/xmlpull/v1/XmlPullParserException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 204
    :cond_2
    const/4 v5, 0x0

    .local v5, "right":F
    const/4 v0, 0x0

    .line 205
    .local v0, "bottom":F
    const/4 v7, 0x0

    .line 207
    .local v7, "width":F
    const/4 v2, 0x0

    .local v2, "i":I
    invoke-interface {p2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v2, v3, :cond_7

    .line 208
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v4

    .line 209
    .local v4, "name":Ljava/lang/String;
    invoke-interface {p2, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v6

    .line 211
    .local v6, "value":Ljava/lang/String;
    const-string v8, "width"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 212
    const-string v8, "dp"

    invoke-virtual {v6, v8}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 213
    const/4 v8, 0x0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x2

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 207
    :cond_3
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 215
    :cond_4
    const-string v8, "height"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 217
    const-string v8, "viewportHeight"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 218
    invoke-static {v6}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_1

    .line 219
    :cond_5
    const-string v8, "viewportWidth"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 220
    invoke-static {v6}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v5

    goto :goto_1

    .line 221
    :cond_6
    const-string v8, "autoMirrored"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 223
    const-string v8, "tintMode"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 225
    const-string v8, "tint"

    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    goto :goto_1

    .line 230
    .end local v4    # "name":Ljava/lang/String;
    .end local v6    # "value":Ljava/lang/String;
    :cond_7
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mTop:F

    iput v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLeft:F

    .line 231
    iput v5, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mRight:F

    .line 232
    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mBottom:F

    .line 233
    iget v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mRight:F

    div-float/2addr v8, v7

    iput v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mDensity:F

    .line 235
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchBottom:F

    iput v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchRight:F

    iput v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchTop:F

    iput v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchLeft:F

    .line 236
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingBottom:F

    iput v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingRight:F

    iput v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingTop:F

    iput v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingLeft:F

    .line 238
    new-instance v8, Landroid/util/SparseArray;

    invoke-direct {v8}, Landroid/util/SparseArray;-><init>()V

    iput-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    .line 239
    new-instance v8, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    const/4 v9, 0x1

    invoke-direct {v8, v9}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;-><init>(Z)V

    iput-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    .line 240
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    new-instance v9, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    const/4 v10, 0x0

    invoke-direct {v9, v10, p2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;-><init>(ZLorg/xmlpull/v1/XmlPullParser;)V

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 242
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    .line 243
    return-void
.end method


# virtual methods
.method public appendObject(ILcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .prologue
    .line 370
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 371
    const-string v0, "SPRDocument"

    const-string v1, "Already closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :goto_0
    return-void

    .line 374
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->appendObject(ILcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    goto :goto_0
.end method

.method public appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V
    .locals 2
    .param p1, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .prologue
    .line 362
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 363
    const-string v0, "SPRDocument"

    const-string v1, "Already closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    :goto_0
    return-void

    .line 366
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    goto :goto_0
.end method

.method public appendReference(ILcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .prologue
    .line 329
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 330
    const-string v0, "SPRDocument"

    const-string v1, "Already closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    :goto_0
    return-void

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public clone()Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 410
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .line 411
    .local v0, "document":Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clone()Landroid/util/SparseArray;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    .line 412
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->clone()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    .line 414
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->clone()Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 252
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 253
    const-string v0, "SPRDocument"

    const-string v1, "Already closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :goto_0
    return-void

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 258
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    .line 260
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    .line 262
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    goto :goto_0
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->close()V

    .line 268
    return-void
.end method

.method public getLoadingTime()I
    .locals 2

    .prologue
    .line 418
    iget-wide v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLoadingTime:J

    long-to-int v0, v0

    return v0
.end method

.method public getObject()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    return-object v0
.end method

.method public getObjectUniqueId()I
    .locals 2

    .prologue
    .line 398
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObjectUniqueId:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObjectUniqueId:I

    return v0
.end method

.method public getReference(I)Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 353
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 354
    const-string v0, "SPRDocument"

    const-string v1, "Already closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    const/4 v0, 0x0

    .line 357
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    goto :goto_0
.end method

.method public getReferenceSize()I
    .locals 2

    .prologue
    .line 345
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 346
    const-string v0, "SPRDocument"

    const-string v1, "Already closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    const/4 v0, 0x0

    .line 349
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getTotalAttributeCount()I
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->getTotalAttributeCount()I

    move-result v0

    return v0
.end method

.method public getTotalElementCount()I
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->getTotalElementCount()I

    move-result v0

    return v0
.end method

.method public getTotalSegmentCount()I
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->getTotalSegmentCount()I

    move-result v0

    return v0
.end method

.method public isIntrinsic()Z
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNinePatch()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 405
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchLeft:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchTop:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchRight:F

    cmpl-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchBottom:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeObject(I)Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 386
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 387
    const-string v0, "SPRDocument"

    const-string v1, "Already closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    const/4 v0, 0x0

    .line 390
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->removeObject(I)Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    move-result-object v0

    goto :goto_0
.end method

.method public removeObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)Z
    .locals 2
    .param p1, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .prologue
    .line 378
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 379
    const-string v0, "SPRDocument"

    const-string v1, "Already closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    const/4 v0, 0x0

    .line 382
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->removeObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)Z

    move-result v0

    goto :goto_0
.end method

.method public removeReference(ILcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .prologue
    .line 337
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 338
    const-string v0, "SPRDocument"

    const-string v1, "Already closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    :goto_0
    return-void

    .line 341
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    goto :goto_0
.end method

.method public setDisplayLayout(ZFFF)V
    .locals 11
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 246
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    iget v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchLeft:F

    iget v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchTop:F

    iget v5, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchRight:F

    iget v6, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchBottom:F

    iget v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mRight:F

    iget v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLeft:F

    sub-float v7, v1, v2

    iget v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mBottom:F

    iget v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mTop:F

    sub-float v8, v1, v2

    move v1, p1

    move v2, p2

    move v9, p3

    move v10, p4

    invoke-virtual/range {v0 .. v10}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->setDisplayLayout(ZFFFFFFFFF)V

    .line 248
    return-void
.end method

.method public toSPR(Ljava/io/OutputStream;)Z
    .locals 10
    .param p1, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 271
    new-instance v6, Ljava/io/DataOutputStream;

    invoke-direct {v6, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 273
    .local v6, "out":Ljava/io/DataOutputStream;
    iget-boolean v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mIsInitialized:Z

    if-nez v8, :cond_0

    .line 274
    const-string v8, "SPRDocument"

    const-string v9, "Already closed"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    const/4 v8, 0x0

    .line 324
    :goto_0
    return v8

    .line 278
    :cond_0
    const/4 v0, 0x4

    .line 279
    .local v0, "filterSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v3

    .local v3, "n":I
    :goto_1
    if-ge v1, v3, :cond_1

    .line 280
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    invoke-virtual {v8, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .line 281
    .local v4, "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    invoke-virtual {v4}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->getSPRSize()I

    move-result v8

    add-int/2addr v0, v8

    .line 279
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 284
    .end local v4    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :cond_1
    const/4 v5, 0x4

    .line 285
    .local v5, "objectSize":I
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->getSPRSize()I

    move-result v8

    add-int/2addr v5, v8

    .line 287
    const v8, 0x53505200

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 288
    const/16 v8, 0x3030

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 289
    const/16 v8, 0x3031

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 290
    const/16 v8, 0x90

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 291
    add-int/lit16 v8, v0, 0x90

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 292
    add-int/lit16 v8, v0, 0x90

    add-int/2addr v8, v5

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 293
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 294
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->getObjectCount()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 295
    iget v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLeft:F

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 296
    iget v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mTop:F

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 297
    iget v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mRight:F

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 298
    iget v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mBottom:F

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 299
    iget v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchLeft:F

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 300
    iget v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchTop:F

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 301
    iget v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchRight:F

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 302
    iget v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchBottom:F

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 303
    iget v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingLeft:F

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 304
    iget v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingTop:F

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 305
    iget v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingRight:F

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 306
    iget v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mPaddingBottom:F

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 307
    iget v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mDensity:F

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 308
    const/16 v8, 0x40

    new-array v7, v8, [B

    .line 310
    .local v7, "reserved":[B
    invoke-virtual {v6, v7}, Ljava/io/DataOutputStream;->write([B)V

    .line 312
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v8

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 314
    const/4 v1, 0x0

    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v3

    :goto_2
    if-ge v1, v3, :cond_2

    .line 315
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    invoke-virtual {v8, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    .line 316
    .local v2, "key":I
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mReferenceMap:Landroid/util/SparseArray;

    invoke-virtual {v8, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .line 317
    .restart local v4    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    invoke-virtual {v6, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 318
    iget-byte v8, v4, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mType:B

    invoke-virtual {v6, v8}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 319
    invoke-virtual {v4, v6}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->toSPR(Ljava/io/DataOutputStream;)V

    .line 314
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 322
    .end local v2    # "key":I
    .end local v4    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :cond_2
    iget-object v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mObject:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-virtual {v8, v6}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->toSPR(Ljava/io/DataOutputStream;)V

    .line 324
    const/4 v8, 0x1

    goto/16 :goto_0
.end method
