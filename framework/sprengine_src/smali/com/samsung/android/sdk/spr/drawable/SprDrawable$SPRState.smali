.class final Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;
.super Landroid/graphics/drawable/Drawable$ConstantState;
.source "SprDrawable.java"

# interfaces
.implements Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sdk/spr/drawable/SprDrawable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SPRState"
.end annotation


# instance fields
.field private mAutoMirrored:Z

.field private mBitmapPaint:Landroid/graphics/Paint;

.field private mCacheBounds:Landroid/graphics/Rect;

.field private mCacheDirty:Z

.field private mCachedAutoMirrored:Z

.field private mCachedBitmap:Landroid/graphics/Bitmap;

.field private mCachedBitmapBounds:Landroid/graphics/Rect;

.field private mCachedBitmapPaint:Landroid/graphics/Paint;

.field private mCachedTint:Landroid/content/res/ColorStateList;

.field private mCachedTintMode:Landroid/graphics/PorterDuff$Mode;

.field private mDensityScale:F

.field private mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

.field private mNinePatch:Z

.field private mPaint:Landroid/graphics/Paint;

.field private mTint:Landroid/content/res/ColorStateList;

.field private mTintMode:Landroid/graphics/PorterDuff$Mode;

.field private mViewDirty:Z


# direct methods
.method constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 763
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    .line 743
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .line 744
    iput-boolean v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z

    .line 745
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F

    .line 746
    iput-boolean v3, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z

    .line 747
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mPaint:Landroid/graphics/Paint;

    .line 748
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;

    .line 750
    iput-boolean v3, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheDirty:Z

    .line 751
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmap:Landroid/graphics/Bitmap;

    .line 752
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmapPaint:Landroid/graphics/Paint;

    .line 753
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmapBounds:Landroid/graphics/Rect;

    .line 754
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedTint:Landroid/content/res/ColorStateList;

    .line 755
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedTintMode:Landroid/graphics/PorterDuff$Mode;

    .line 756
    iput-boolean v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedAutoMirrored:Z

    .line 758
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;

    .line 759
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTint:Landroid/content/res/ColorStateList;

    .line 760
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTintMode:Landroid/graphics/PorterDuff$Mode;

    .line 761
    iput-boolean v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mAutoMirrored:Z

    .line 764
    invoke-static {p0}, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->registerThemeChangeListener(Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;)V

    .line 765
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mPaint:Landroid/graphics/Paint;

    .line 766
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;

    .line 767
    return-void
.end method

.method constructor <init>(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)V
    .locals 4
    .param p1, "state"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 779
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    .line 743
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .line 744
    iput-boolean v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z

    .line 745
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F

    .line 746
    iput-boolean v3, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z

    .line 747
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mPaint:Landroid/graphics/Paint;

    .line 748
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;

    .line 750
    iput-boolean v3, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheDirty:Z

    .line 751
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmap:Landroid/graphics/Bitmap;

    .line 752
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmapPaint:Landroid/graphics/Paint;

    .line 753
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmapBounds:Landroid/graphics/Rect;

    .line 754
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedTint:Landroid/content/res/ColorStateList;

    .line 755
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedTintMode:Landroid/graphics/PorterDuff$Mode;

    .line 756
    iput-boolean v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedAutoMirrored:Z

    .line 758
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;

    .line 759
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTint:Landroid/content/res/ColorStateList;

    .line 760
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTintMode:Landroid/graphics/PorterDuff$Mode;

    .line 761
    iput-boolean v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mAutoMirrored:Z

    .line 780
    invoke-static {p0}, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->registerThemeChangeListener(Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;)V

    .line 781
    iget-object v0, p1, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .line 782
    iget-boolean v0, p1, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z

    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z

    .line 783
    iget v0, p1, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F

    .line 784
    iput-boolean v3, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z

    .line 785
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p1, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mPaint:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mPaint:Landroid/graphics/Paint;

    .line 786
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p1, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;

    .line 787
    return-void
.end method

.method constructor <init>(Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;)V
    .locals 4
    .param p1, "document"    # Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 769
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable$ConstantState;-><init>()V

    .line 743
    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .line 744
    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z

    .line 745
    iput v3, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F

    .line 746
    iput-boolean v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z

    .line 747
    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mPaint:Landroid/graphics/Paint;

    .line 748
    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;

    .line 750
    iput-boolean v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheDirty:Z

    .line 751
    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmap:Landroid/graphics/Bitmap;

    .line 752
    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmapPaint:Landroid/graphics/Paint;

    .line 753
    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmapBounds:Landroid/graphics/Rect;

    .line 754
    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedTint:Landroid/content/res/ColorStateList;

    .line 755
    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedTintMode:Landroid/graphics/PorterDuff$Mode;

    .line 756
    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedAutoMirrored:Z

    .line 758
    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;

    .line 759
    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTint:Landroid/content/res/ColorStateList;

    .line 760
    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    iput-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTintMode:Landroid/graphics/PorterDuff$Mode;

    .line 761
    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mAutoMirrored:Z

    .line 770
    invoke-static {p0}, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->registerThemeChangeListener(Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;)V

    .line 771
    iput-object p1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .line 772
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v2, v2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchLeft:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v2, v2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchTop:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v2, v2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchRight:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v2, v2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mNinePatchBottom:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z

    .line 774
    # invokes: Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->getDeviceDensityScale()F
    invoke-static {}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;->access$1100()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v1, v1, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mDensity:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F

    .line 775
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mPaint:Landroid/graphics/Paint;

    .line 776
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;

    .line 777
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .prologue
    .line 742
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;
    .param p1, "x1"    # Z

    .prologue
    .line 742
    iput-boolean p1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mNinePatch:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .prologue
    .line 742
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;
    .param p1, "x1"    # Z

    .prologue
    .line 742
    iput-boolean p1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheDirty:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .prologue
    .line 742
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mDensityScale:F

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Paint;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .prologue
    .line 742
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .prologue
    .line 742
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;
    .param p1, "x1"    # Z

    .prologue
    .line 742
    iput-boolean p1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .prologue
    .line 742
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 742
    iput-object p1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmap:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$600(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .prologue
    .line 742
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$602(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;
    .param p1, "x1"    # Landroid/graphics/Rect;

    .prologue
    .line 742
    iput-object p1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;

    return-object p1
.end method

.method static synthetic access$700(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/content/res/ColorStateList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .prologue
    .line 742
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTint:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method static synthetic access$702(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Landroid/content/res/ColorStateList;)Landroid/content/res/ColorStateList;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;
    .param p1, "x1"    # Landroid/content/res/ColorStateList;

    .prologue
    .line 742
    iput-object p1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTint:Landroid/content/res/ColorStateList;

    return-object p1
.end method

.method static synthetic access$800(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Landroid/graphics/PorterDuff$Mode;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .prologue
    .line 742
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTintMode:Landroid/graphics/PorterDuff$Mode;

    return-object v0
.end method

.method static synthetic access$802(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Landroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;
    .param p1, "x1"    # Landroid/graphics/PorterDuff$Mode;

    .prologue
    .line 742
    iput-object p1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTintMode:Landroid/graphics/PorterDuff$Mode;

    return-object p1
.end method

.method static synthetic access$900(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;

    .prologue
    .line 742
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mAutoMirrored:Z

    return v0
.end method

.method static synthetic access$902(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;
    .param p1, "x1"    # Z

    .prologue
    .line 742
    iput-boolean p1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mAutoMirrored:Z

    return p1
.end method


# virtual methods
.method canReuseCache()Z
    .locals 2

    .prologue
    .line 790
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheDirty:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmapBounds:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedTint:Landroid/content/res/ColorStateList;

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTint:Landroid/content/res/ColorStateList;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedTintMode:Landroid/graphics/PorterDuff$Mode;

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTintMode:Landroid/graphics/PorterDuff$Mode;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedAutoMirrored:Z

    iget-boolean v1, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mAutoMirrored:Z

    if-ne v0, v1, :cond_0

    .line 793
    const/4 v0, 0x1

    .line 795
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 830
    invoke-static {p0}, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->deregisterThemeChangeListener(Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;)V

    .line 831
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 832
    return-void
.end method

.method public getChangingConfigurations()I
    .locals 1

    .prologue
    .line 810
    const/4 v0, 0x0

    return v0
.end method

.method public newDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 815
    new-instance v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;-><init>(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)V

    return-object v0
.end method

.method public newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 820
    new-instance v0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/spr/drawable/SprDrawable;-><init>(Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;)V

    return-object v0
.end method

.method public onChange()V
    .locals 1

    .prologue
    .line 825
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mViewDirty:Z

    .line 826
    return-void
.end method

.method public updateCacheStates()V
    .locals 1

    .prologue
    .line 799
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheDirty:Z

    .line 800
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mBitmapPaint:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmapPaint:Landroid/graphics/Paint;

    .line 801
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCacheBounds:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedBitmapBounds:Landroid/graphics/Rect;

    .line 802
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTint:Landroid/content/res/ColorStateList;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedTint:Landroid/content/res/ColorStateList;

    .line 803
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mTintMode:Landroid/graphics/PorterDuff$Mode;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedTintMode:Landroid/graphics/PorterDuff$Mode;

    .line 804
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mAutoMirrored:Z

    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/SprDrawable$SPRState;->mCachedAutoMirrored:Z

    .line 805
    return-void
.end method
