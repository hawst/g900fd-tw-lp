.class public Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererFactory;
.super Ljava/lang/Object;
.source "SprRendererFactory.java"


# static fields
.field protected static mDebugLevel:Ljava/lang/Integer;

.field private static mInstance:Lcom/samsung/android/sdk/spr/drawable/engine/SprRenderer;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 7
    sput-object v3, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererFactory;->mInstance:Lcom/samsung/android/sdk/spr/drawable/engine/SprRenderer;

    .line 8
    sput-object v3, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererFactory;->mDebugLevel:Ljava/lang/Integer;

    .line 11
    const-string v3, "eng"

    sget-object v4, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 13
    :try_start_0
    const-string v3, "android.os.SystemProperties"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 14
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v3, "getInt"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 15
    .local v2, "md":Ljava/lang/reflect/Method;
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "persist.sys.spr.debug"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    sput-object v3, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererFactory;->mDebugLevel:Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    .end local v2    # "md":Ljava/lang/reflect/Method;
    .local v1, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 16
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v1

    .line 17
    .restart local v1    # "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    .line 18
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererFactory;->mDebugLevel:Ljava/lang/Integer;

    goto :goto_0

    .line 21
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sput-object v3, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererFactory;->mDebugLevel:Ljava/lang/Integer;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/sdk/spr/drawable/engine/SprRenderer;
    .locals 3

    .prologue
    .line 26
    const-class v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererFactory;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererFactory;->mInstance:Lcom/samsung/android/sdk/spr/drawable/engine/SprRenderer;

    if-nez v0, :cond_0

    .line 27
    sget-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererFactory;->mDebugLevel:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    .line 28
    new-instance v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;

    invoke-direct {v0}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererFactory;->mInstance:Lcom/samsung/android/sdk/spr/drawable/engine/SprRenderer;

    .line 34
    :cond_0
    :goto_0
    sget-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererFactory;->mInstance:Lcom/samsung/android/sdk/spr/drawable/engine/SprRenderer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 30
    :cond_1
    :try_start_1
    new-instance v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;

    sget-object v2, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererFactory;->mDebugLevel:Ljava/lang/Integer;

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;-><init>(Ljava/lang/Integer;)V

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererFactory;->mInstance:Lcom/samsung/android/sdk/spr/drawable/engine/SprRenderer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 26
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
