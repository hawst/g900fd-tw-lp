.class public Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;
.super Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
.source "SprObjectShapeRectangle.java"


# instance fields
.field public bottom:F

.field public left:F

.field private final mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

.field public right:F

.field public rx:F

.field public ry:F

.field public top:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;-><init>(B)V

    .line 10
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->left:F

    .line 11
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->top:F

    .line 12
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->right:F

    .line 13
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->bottom:F

    .line 14
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->rx:F

    .line 15
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->ry:F

    .line 21
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .line 22
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 2
    .param p1, "l"    # F
    .param p2, "t"    # F
    .param p3, "r"    # F
    .param p4, "b"    # F

    .prologue
    const/4 v1, 0x0

    .line 25
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;-><init>(B)V

    .line 10
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->left:F

    .line 11
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->top:F

    .line 12
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->right:F

    .line 13
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->bottom:F

    .line 14
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->rx:F

    .line 15
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->ry:F

    .line 26
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .line 27
    iput p1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->left:F

    .line 28
    iput p2, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->top:F

    .line 29
    iput p3, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->right:F

    .line 30
    iput p4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->bottom:F

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V
    .locals 2
    .param p1, "in"    # Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 34
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;-><init>(B)V

    .line 10
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->left:F

    .line 11
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->top:F

    .line 12
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->right:F

    .line 13
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->bottom:F

    .line 14
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->rx:F

    .line 15
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->ry:F

    .line 35
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .line 36
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 37
    return-void
.end method


# virtual methods
.method public fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->left:F

    .line 68
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->top:F

    .line 69
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->right:F

    .line 70
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->bottom:F

    .line 71
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->rx:F

    .line 72
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->ry:F

    .line 74
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 75
    return-void
.end method

.method public getSPRSize()I
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->getSPRSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x10

    return v0
.end method

.method public getTotalElementCount()I
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x1

    return v0
.end method

.method public getTotalSegmentCount()I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 96
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->rx:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->ry:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 97
    const/4 v0, 0x4

    .line 99
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setDisplayLayout(ZFFFFFFFFF)V
    .locals 6
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "npLeft"    # F
    .param p4, "npTop"    # F
    .param p5, "npRight"    # F
    .param p6, "npBottom"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "displayWidth"    # F
    .param p10, "displayHeight"    # F

    .prologue
    .line 42
    invoke-super/range {p0 .. p10}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->setDisplayLayout(ZFFFFFFFFF)V

    .line 45
    if-eqz p1, :cond_0

    .line 46
    div-float/2addr p9, p2

    .line 47
    div-float p10, p10, p2

    .line 49
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->left:F

    move-object v0, p0

    move v2, p3

    move v3, p5

    move v4, p7

    move v5, p9

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->left:F

    .line 50
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->top:F

    move-object v0, p0

    move v2, p4

    move v3, p6

    move v4, p8

    move/from16 v5, p10

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->top:F

    .line 51
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->right:F

    move-object v0, p0

    move v2, p3

    move v3, p5

    move v4, p7

    move v5, p9

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->right:F

    .line 52
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->bottom:F

    move-object v0, p0

    move v2, p4

    move v3, p6

    move v4, p8

    move/from16 v5, p10

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->bottom:F

    .line 53
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->rx:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->rx:F

    .line 54
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->ry:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->ry:F

    .line 63
    :goto_0
    return-void

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->left:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->left:F

    .line 57
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->top:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->top:F

    .line 58
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->right:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->right:F

    .line 59
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->bottom:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->bottom:F

    .line 60
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->rx:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->rx:F

    .line 61
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->ry:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->ry:F

    goto :goto_0
.end method

.method public toSPR(Ljava/io/DataOutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->left:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 80
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->top:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 81
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->right:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 82
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->bottom:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 83
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->rx:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 84
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->ry:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 86
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->toSPR(Ljava/io/DataOutputStream;)V

    .line 87
    return-void
.end method
