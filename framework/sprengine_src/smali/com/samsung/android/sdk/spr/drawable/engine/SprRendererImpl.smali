.class Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;
.super Lcom/samsung/android/sdk/spr/drawable/engine/SprRenderer;
.source "SprRendererImpl.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SPRRenderer"

.field private static final sCapArray:[Landroid/graphics/Paint$Cap;

.field private static final sJoinArray:[Landroid/graphics/Paint$Join;


# instance fields
.field private mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 33
    new-array v0, v5, [Landroid/graphics/Paint$Cap;

    sget-object v1, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    aput-object v1, v0, v2

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    aput-object v1, v0, v3

    sget-object v1, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->sCapArray:[Landroid/graphics/Paint$Cap;

    .line 34
    new-array v0, v5, [Landroid/graphics/Paint$Join;

    sget-object v1, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    aput-object v1, v0, v2

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    aput-object v1, v0, v3

    sget-object v1, Landroid/graphics/Paint$Join;->BEVEL:Landroid/graphics/Paint$Join;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->sJoinArray:[Landroid/graphics/Paint$Join;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRenderer;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .line 38
    return-void
.end method

.method private applyAttribute(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .prologue
    .line 170
    const/4 v9, 0x0

    .line 171
    .local v9, "reference":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    iget-object v0, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    :sswitch_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    .line 172
    .local v6, "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    iget-byte v0, v6, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mType:B

    sparse-switch v0, :sswitch_data_0

    .line 224
    const-string v0, "SPRRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Attribute type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v2, v6, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mType:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "is not supported type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_1
    move-object v7, v6

    .line 174
    check-cast v7, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;

    .line 175
    .local v7, "clip":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;
    iget v1, v7, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->left:F

    iget v2, v7, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->top:F

    iget v3, v7, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->right:F

    iget v4, v7, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->bottom:F

    sget-object v5, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    goto :goto_0

    .line 180
    .end local v7    # "clip":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;
    :sswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    check-cast v6, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClipPath;

    .end local v6    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    iget v1, v6, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClipPath;->link:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->getReference(I)Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    move-result-object v9

    .line 181
    if-eqz v9, :cond_0

    .line 182
    iget-byte v0, v9, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mType:B

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_3
    move-object v0, v9

    .line 191
    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;

    iget-object v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    goto :goto_0

    :sswitch_4
    move-object v0, v9

    .line 195
    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->left:F

    move-object v0, v9

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v2, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->top:F

    move-object v0, v9

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v3, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->right:F

    move-object v0, v9

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    iget v4, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->bottom:F

    sget-object v5, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    goto :goto_0

    .line 220
    .restart local v6    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    :sswitch_5
    check-cast v6, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;

    .end local v6    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    iget-object v0, v6, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    goto/16 :goto_0

    .line 228
    :cond_1
    return-void

    .line 172
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x3 -> :sswitch_2
        0x20 -> :sswitch_0
        0x23 -> :sswitch_0
        0x25 -> :sswitch_0
        0x26 -> :sswitch_0
        0x28 -> :sswitch_0
        0x29 -> :sswitch_0
        0x40 -> :sswitch_5
    .end sparse-switch

    .line 182
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x10 -> :sswitch_0
        0x11 -> :sswitch_0
    .end sparse-switch
.end method

.method private applyPreAttribute(Landroid/graphics/Paint;Landroid/graphics/Paint;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V
    .locals 12
    .param p1, "strokePaint"    # Landroid/graphics/Paint;
    .param p2, "fillPaint"    # Landroid/graphics/Paint;
    .param p3, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .prologue
    const/16 v11, 0xff

    const/4 v10, 0x0

    const/high16 v9, -0x1000000

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 304
    iget-object v4, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    :sswitch_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    .line 305
    .local v0, "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    iget-byte v4, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mType:B

    sparse-switch v4, :sswitch_data_0

    .line 391
    const-string v4, "SPRRenderer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Attribute type = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-byte v6, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mType:B

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "is not supported type"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_1
    move-object v1, v0

    .line 312
    check-cast v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    .line 313
    .local v1, "fill":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;
    iget-byte v4, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;->colorType:B

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 315
    :pswitch_0
    iput-boolean v10, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleFill:Z

    goto :goto_0

    .line 319
    :pswitch_1
    iput-boolean v7, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleFill:Z

    .line 320
    invoke-virtual {p2, v8}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 321
    iget v4, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;->color:I

    invoke-virtual {p2, v4}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 325
    :pswitch_2
    iput-boolean v7, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleFill:Z

    .line 326
    invoke-virtual {p2, v8}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 327
    sget-object v4, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->colorThemeTable:[I

    iget v5, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;->color:I

    aget v4, v4, v5

    or-int/2addr v4, v9

    invoke-virtual {p2, v4}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 332
    :pswitch_3
    iput-boolean v7, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleFill:Z

    .line 333
    invoke-virtual {p2, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 334
    iget-object v4, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;->gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    iget-object v4, v4, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->shader:Landroid/graphics/Shader;

    invoke-virtual {p2, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto :goto_0

    .end local v1    # "fill":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;
    :sswitch_2
    move-object v3, v0

    .line 341
    check-cast v3, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;

    .line 342
    .local v3, "stroke":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;
    iget-byte v4, v3, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;->colorType:B

    packed-switch v4, :pswitch_data_1

    goto :goto_0

    .line 344
    :pswitch_4
    iput-boolean v10, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleStroke:Z

    goto :goto_0

    .line 348
    :pswitch_5
    iput-boolean v7, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleStroke:Z

    .line 349
    invoke-virtual {p1, v8}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 350
    iget v4, v3, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;->color:I

    invoke-virtual {p1, v4}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 354
    :pswitch_6
    iput-boolean v7, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleStroke:Z

    .line 355
    invoke-virtual {p1, v8}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 356
    sget-object v4, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->colorThemeTable:[I

    iget v5, v3, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;->color:I

    aget v4, v4, v5

    or-int/2addr v4, v9

    invoke-virtual {p1, v4}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_0

    .line 361
    :pswitch_7
    iput-boolean v7, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleStroke:Z

    .line 362
    invoke-virtual {p1, v11}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 363
    iget-object v4, v3, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;->gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    iget-object v4, v4, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->shader:Landroid/graphics/Shader;

    invoke-virtual {p1, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto/16 :goto_0

    .line 370
    .end local v3    # "stroke":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;
    :sswitch_3
    sget-object v4, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->sCapArray:[Landroid/graphics/Paint$Cap;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinecap;

    .end local v0    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    iget-byte v5, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinecap;->linecap:B

    add-int/lit8 v5, v5, -0x1

    aget-object v4, v4, v5

    invoke-virtual {p1, v4}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    goto/16 :goto_0

    .line 374
    .restart local v0    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    :sswitch_4
    sget-object v4, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->sJoinArray:[Landroid/graphics/Paint$Join;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;

    .end local v0    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    iget-byte v5, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->linejoin:B

    add-int/lit8 v5, v5, -0x1

    aget-object v4, v4, v5

    invoke-virtual {p1, v4}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    goto/16 :goto_0

    .line 378
    .restart local v0    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    :sswitch_5
    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;

    .end local v0    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    iget v4, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->strokeWidth:F

    invoke-virtual {p1, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto/16 :goto_0

    .line 382
    .restart local v0    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    :sswitch_6
    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeMiterlimit;

    .end local v0    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    iget v4, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeMiterlimit;->miterLimit:F

    invoke-virtual {p1, v4}, Landroid/graphics/Paint;->setStrokeMiter(F)V

    goto/16 :goto_0

    .line 395
    :cond_0
    return-void

    .line 305
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_0
        0x20 -> :sswitch_1
        0x23 -> :sswitch_2
        0x25 -> :sswitch_3
        0x26 -> :sswitch_4
        0x28 -> :sswitch_5
        0x29 -> :sswitch_6
        0x40 -> :sswitch_0
    .end sparse-switch

    .line 313
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch

    .line 342
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method private draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;FF)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    .param p3, "sx"    # F
    .param p4, "sy"    # F

    .prologue
    .line 66
    const/16 v3, 0x1f

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->save(I)I

    .line 68
    iget-object v3, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 69
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->applyAttribute(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 72
    :cond_0
    iget-byte v3, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mType:B

    sparse-switch v3, :sswitch_data_0

    .line 107
    const-string v3, "SPRRenderer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Object type = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-byte v5, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mType:B

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "is not supported type"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    .end local p2    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :cond_1
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 112
    return-void

    .line 74
    .restart local p2    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :sswitch_0
    const/4 v0, 0x0

    .local v0, "i":I
    move-object v3, p2

    check-cast v3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->getObjectCount()I

    move-result v2

    .local v2, "n":I
    :goto_1
    if-ge v0, v2, :cond_1

    move-object v3, p2

    .line 75
    check-cast v3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->getObject(I)Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    move-result-object v3

    invoke-direct {p0, p1, v3, p3, p4}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;FF)V

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 80
    .end local v0    # "i":I
    .end local v2    # "n":I
    :sswitch_1
    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    check-cast p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeUse;

    .end local p2    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    iget v4, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeUse;->link:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->getReference(I)Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    move-result-object v1

    .line 81
    .local v1, "link":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    if-eqz v1, :cond_1

    .line 82
    invoke-direct {p0, p1, v1, p3, p4}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;FF)V

    goto :goto_0

    .line 87
    .end local v1    # "link":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    .restart local p2    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :sswitch_2
    check-cast p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;

    .end local p2    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;)V

    goto :goto_0

    .line 91
    .restart local p2    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :sswitch_3
    check-cast p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    .end local p2    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;)V

    goto :goto_0

    .line 95
    .restart local p2    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :sswitch_4
    check-cast p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    .end local p2    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;)V

    goto :goto_0

    .line 99
    .restart local p2    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :sswitch_5
    check-cast p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;

    .end local p2    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;FF)V

    goto :goto_0

    .line 103
    .restart local p2    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :sswitch_6
    check-cast p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .end local p2    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;)V

    goto :goto_0

    .line 72
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_5
        0x5 -> :sswitch_6
        0x10 -> :sswitch_0
        0x11 -> :sswitch_1
    .end sparse-switch
.end method

.method private draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;

    .prologue
    .line 115
    iget-boolean v0, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->isVisibleFill:Z

    if-eqz v0, :cond_0

    .line 116
    iget v0, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cx:F

    iget v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cy:F

    iget v2, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cr:F

    iget-object v3, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 118
    :cond_0
    iget-boolean v0, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->isVisibleStroke:Z

    if-eqz v0, :cond_1

    .line 119
    iget v0, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cx:F

    iget v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cy:F

    iget v2, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cr:F

    iget-object v3, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->strokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 121
    :cond_1
    return-void
.end method

.method private draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    .prologue
    .line 124
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->left:F

    iget v2, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->top:F

    iget v3, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->right:F

    iget v4, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 126
    .local v0, "rect":Landroid/graphics/RectF;
    iget-boolean v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->isVisibleFill:Z

    if-eqz v1, :cond_0

    .line 127
    iget-object v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 129
    :cond_0
    iget-boolean v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->isVisibleStroke:Z

    if-eqz v1, :cond_1

    .line 130
    iget-object v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->strokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 132
    :cond_1
    return-void
.end method

.method private draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    .prologue
    .line 135
    iget-boolean v0, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->isVisibleStroke:Z

    if-eqz v0, :cond_0

    .line 136
    iget v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x1:F

    iget v2, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y1:F

    iget v3, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x2:F

    iget v4, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y2:F

    iget-object v5, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->strokePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 138
    :cond_0
    return-void
.end method

.method private draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;FF)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;
    .param p3, "sx"    # F
    .param p4, "sy"    # F

    .prologue
    .line 141
    iget-boolean v0, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->isVisibleFill:Z

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    iget-object v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 144
    :cond_0
    iget-boolean v0, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->isVisibleStroke:Z

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->path:Landroid/graphics/Path;

    iget-object v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;->strokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 147
    :cond_1
    return-void
.end method

.method private draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    .prologue
    const/4 v5, 0x0

    .line 150
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->left:F

    iget v2, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->top:F

    iget v3, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->right:F

    iget v4, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->bottom:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 152
    .local v0, "rect":Landroid/graphics/RectF;
    iget v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->rx:F

    cmpl-float v1, v1, v5

    if-nez v1, :cond_0

    iget v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->ry:F

    cmpl-float v1, v1, v5

    if-eqz v1, :cond_3

    .line 153
    :cond_0
    iget-boolean v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->isVisibleFill:Z

    if-eqz v1, :cond_1

    .line 154
    iget v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->rx:F

    iget v2, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->ry:F

    iget-object v3, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 156
    :cond_1
    iget-boolean v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->isVisibleStroke:Z

    if-eqz v1, :cond_2

    .line 157
    iget v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->rx:F

    iget v2, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->ry:F

    iget-object v3, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->strokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 167
    :cond_2
    :goto_0
    return-void

    .line 160
    :cond_3
    iget-boolean v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->isVisibleFill:Z

    if-eqz v1, :cond_4

    .line 161
    iget-object v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 163
    :cond_4
    iget-boolean v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->isVisibleStroke:Z

    if-eqz v1, :cond_2

    .line 164
    iget-object v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;->strokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;ZFII)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "document"    # Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    .param p3, "ninePatch"    # Z
    .param p4, "densityScale"    # F
    .param p5, "displayWidth"    # I
    .param p6, "displayHeight"    # I

    .prologue
    .line 43
    iput-object p2, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .line 47
    if-eqz p3, :cond_0

    .line 48
    move v7, p4

    .local v7, "sy":F
    move v6, p4

    .line 54
    .local v6, "sx":F
    :goto_0
    const/16 v0, 0x1f

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    .line 55
    iget v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLeft:F

    iget v2, p2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mTop:F

    iget v0, p2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLeft:F

    int-to-float v3, p5

    add-float/2addr v3, v0

    iget v0, p2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mTop:F

    int-to-float v4, p6

    add-float/2addr v4, v0

    sget-object v5, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->clipRect(FFFFLandroid/graphics/Region$Op;)Z

    .line 58
    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->scale(FF)V

    .line 60
    invoke-virtual {p2}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->getObject()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    move-result-object v0

    invoke-direct {p0, p1, v0, v6, v7}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;FF)V

    .line 62
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 63
    return-void

    .line 50
    .end local v6    # "sx":F
    .end local v7    # "sy":F
    :cond_0
    int-to-float v0, p5

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v1, v1, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mRight:F

    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v2, v2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLeft:F

    sub-float/2addr v1, v2

    div-float v6, v0, v1

    .line 51
    .restart local v6    # "sx":F
    int-to-float v0, p6

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v1, v1, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mBottom:F

    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    iget v2, v2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mTop:F

    sub-float/2addr v1, v2

    div-float v7, v0, v1

    .restart local v7    # "sy":F
    goto :goto_0
.end method

.method protected preDraw(Landroid/graphics/Paint;Landroid/graphics/Paint;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;ZZ)V
    .locals 8
    .param p1, "strokePaint"    # Landroid/graphics/Paint;
    .param p2, "fillPaint"    # Landroid/graphics/Paint;
    .param p3, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    .param p4, "isVisibleStroke"    # Z
    .param p5, "isVisibleFill"    # Z

    .prologue
    .line 257
    move-object v1, p1

    .line 258
    .local v1, "newStrokePaint":Landroid/graphics/Paint;
    move-object v2, p2

    .line 260
    .local v2, "newFillPaint":Landroid/graphics/Paint;
    iput-boolean p4, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleStroke:Z

    .line 261
    iput-boolean p5, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleFill:Z

    .line 263
    iget-object v0, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 264
    new-instance v1, Landroid/graphics/Paint;

    .end local v1    # "newStrokePaint":Landroid/graphics/Paint;
    invoke-direct {v1, p1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 265
    .restart local v1    # "newStrokePaint":Landroid/graphics/Paint;
    new-instance v2, Landroid/graphics/Paint;

    .end local v2    # "newFillPaint":Landroid/graphics/Paint;
    invoke-direct {v2, p2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 266
    .restart local v2    # "newFillPaint":Landroid/graphics/Paint;
    invoke-direct {p0, v1, v2, p3}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->applyPreAttribute(Landroid/graphics/Paint;Landroid/graphics/Paint;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V

    .line 269
    :cond_0
    iget-byte v0, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mType:B

    sparse-switch v0, :sswitch_data_0

    .line 297
    const-string v0, "SPRRenderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Object type = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-byte v4, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mType:B

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "is not supported type"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    :cond_1
    :goto_0
    return-void

    .line 271
    :sswitch_0
    const/4 v6, 0x0

    .local v6, "i":I
    move-object v0, p3

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->getObjectCount()I

    move-result v7

    .local v7, "n":I
    :goto_1
    if-ge v6, v7, :cond_1

    move-object v0, p3

    .line 272
    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->getObject(I)Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    move-result-object v3

    iget-boolean v4, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleStroke:Z

    iget-boolean v5, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleFill:Z

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->preDraw(Landroid/graphics/Paint;Landroid/graphics/Paint;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;ZZ)V

    .line 271
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 278
    .end local v6    # "i":I
    .end local v7    # "n":I
    :sswitch_1
    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-object v0, p3

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeUse;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeUse;->link:I

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->getReference(I)Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    move-result-object v0

    if-nez v0, :cond_2

    .line 279
    const-string v0, "SPRRenderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "can\'t find link object!!!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 283
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    move-object v0, p3

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeUse;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeUse;->link:I

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->getReference(I)Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    move-result-object v3

    iget-boolean v4, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleStroke:Z

    iget-boolean v5, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleFill:Z

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->preDraw(Landroid/graphics/Paint;Landroid/graphics/Paint;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;ZZ)V

    goto :goto_0

    .line 292
    :sswitch_2
    iput-object v2, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->fillPaint:Landroid/graphics/Paint;

    .line 293
    iput-object v1, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->strokePaint:Landroid/graphics/Paint;

    goto :goto_0

    .line 269
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_2
        0x10 -> :sswitch_0
        0x11 -> :sswitch_1
    .end sparse-switch
.end method

.method public preDraw(Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;Landroid/graphics/Paint;II)V
    .locals 6
    .param p1, "document"    # Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    .param p2, "paint"    # Landroid/graphics/Paint;
    .param p3, "displayWidth"    # I
    .param p4, "displayHeight"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 232
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, p2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 233
    .local v1, "strokePaint":Landroid/graphics/Paint;
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, p2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 235
    .local v2, "fillPaint":Landroid/graphics/Paint;
    iput-object p1, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->mDocument:Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;

    .line 237
    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 238
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 239
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 241
    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 242
    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 244
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->getObject()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    move-result-object v3

    move-object v0, p0

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->preDraw(Landroid/graphics/Paint;Landroid/graphics/Paint;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;ZZ)V

    .line 245
    return-void
.end method

.method public preDraw(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V
    .locals 6
    .param p1, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .prologue
    .line 249
    iget-object v0, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->strokePaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->fillPaint:Landroid/graphics/Paint;

    if-nez v0, :cond_1

    .line 253
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    iget-object v1, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->strokePaint:Landroid/graphics/Paint;

    iget-object v2, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->fillPaint:Landroid/graphics/Paint;

    iget-boolean v4, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleStroke:Z

    iget-boolean v5, p1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleStroke:Z

    move-object v0, p0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->preDraw(Landroid/graphics/Paint;Landroid/graphics/Paint;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;ZZ)V

    goto :goto_0
.end method
