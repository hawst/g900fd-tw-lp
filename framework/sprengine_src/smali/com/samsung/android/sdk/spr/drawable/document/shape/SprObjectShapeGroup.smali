.class public Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;
.super Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
.source "SprObjectShapeGroup.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SPRObjectShapeGroup"


# instance fields
.field private mIsInitialized:Z

.field private final mIsRoot:Z

.field private mObjectList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1, "root"    # Z

    .prologue
    .line 21
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;-><init>(B)V

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsInitialized:Z

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsInitialized:Z

    .line 26
    iput-boolean p1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsRoot:Z

    .line 27
    return-void
.end method

.method public constructor <init>(ZLcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V
    .locals 1
    .param p1, "root"    # Z
    .param p2, "in"    # Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 30
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;-><init>(B)V

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsInitialized:Z

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsInitialized:Z

    .line 35
    iput-boolean p1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsRoot:Z

    .line 37
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 38
    return-void
.end method

.method public constructor <init>(ZLorg/xmlpull/v1/XmlPullParser;)V
    .locals 1
    .param p1, "root"    # Z
    .param p2, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 41
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;-><init>(B)V

    .line 15
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsInitialized:Z

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsInitialized:Z

    .line 46
    iput-boolean p1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsRoot:Z

    .line 48
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->fromXml(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 49
    return-void
.end method


# virtual methods
.method public appendObject(ILcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 199
    const-string v0, "SPRObjectShapeGroup"

    const-string v1, "Already finalize"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :goto_0
    return-void

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public appendObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)V
    .locals 2
    .param p1, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .prologue
    .line 190
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 191
    const-string v0, "SPRObjectShapeGroup"

    const-string v1, "Already finalize"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public bridge synthetic clone()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->clone()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 254
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->clone()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    .line 255
    .local v2, "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    .line 257
    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .line 258
    .local v0, "child":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    iget-object v3, v2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->clone()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 261
    .end local v0    # "child":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :cond_0
    return-object v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->clone()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    move-result-object v0

    return-object v0
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 54
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->finalize()V

    .line 55
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsInitialized:Z

    .line 58
    return-void
.end method

.method public fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V
    .locals 7
    .param p1, "in"    # Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readInt()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 75
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readByte()B

    move-result v3

    .line 76
    .local v3, "type":B
    const/4 v2, 0x0

    .line 78
    .local v2, "size":I
    iget-short v4, p1, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->mMajorVersion:S

    const/16 v5, 0x3030

    if-lt v4, v5, :cond_0

    iget-short v4, p1, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->mMinorVersion:S

    const/16 v5, 0x3032

    if-lt v4, v5, :cond_0

    .line 79
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readInt()I

    move-result v2

    .line 82
    :cond_0
    sparse-switch v3, :sswitch_data_0

    .line 112
    const-string v4, "SPRObjectShapeGroup"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unknown element type:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :goto_1
    if-lez v2, :cond_1

    .line 114
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readByte()B

    .line 115
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 84
    :sswitch_0
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    const/4 v6, 0x0

    invoke-direct {v5, v6, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;-><init>(ZLcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 88
    :sswitch_1
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;

    invoke-direct {v5, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 92
    :sswitch_2
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    invoke-direct {v5, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 96
    :sswitch_3
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    invoke-direct {v5, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 100
    :sswitch_4
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;

    invoke-direct {v5, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 104
    :sswitch_5
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;

    invoke-direct {v5, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeRectangle;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 108
    :sswitch_6
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeUse;

    invoke-direct {v5, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeUse;-><init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 121
    .end local v2    # "size":I
    .end local v3    # "type":B
    :cond_2
    iget-boolean v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsRoot:Z

    if-nez v4, :cond_3

    .line 122
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 124
    :cond_3
    return-void

    .line 82
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x10 -> :sswitch_0
        0x11 -> :sswitch_6
    .end sparse-switch
.end method

.method public fromXml(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 8
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 142
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 143
    invoke-interface {p1, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v3

    .line 145
    .local v3, "name":Ljava/lang/String;
    const-string v5, "name"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 142
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 147
    :cond_1
    const-string v5, "rotation"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 149
    const-string v5, "pivotX"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 151
    const-string v5, "pivotY"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 153
    const-string v5, "translateX"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 155
    const-string v5, "translateX"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 157
    const-string v5, "scaleX"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 159
    const-string v5, "scaleX"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 161
    const-string v5, "alpha"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    goto :goto_1

    .line 166
    .end local v3    # "name":Ljava/lang/String;
    :cond_2
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 168
    .local v0, "eventType":I
    :goto_2
    const/4 v5, 0x1

    if-eq v0, v5, :cond_7

    .line 169
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v4

    .line 170
    .local v4, "tag":Ljava/lang/String;
    const/4 v5, 0x2

    if-ne v0, v5, :cond_6

    .line 171
    const-string v5, "group"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 172
    iget-object v5, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    new-instance v6, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    const/4 v7, 0x0

    invoke-direct {v6, v7, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;-><init>(ZLorg/xmlpull/v1/XmlPullParser;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    :cond_3
    :goto_3
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 184
    goto :goto_2

    .line 173
    :cond_4
    const-string v5, "path"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 174
    iget-object v5, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    new-instance v6, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;

    invoke-direct {v6, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapePath;-><init>(Lorg/xmlpull/v1/XmlPullParser;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 175
    :cond_5
    const-string v5, "clip-path"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    goto :goto_3

    .line 177
    :cond_6
    const/4 v5, 0x3

    if-ne v0, v5, :cond_3

    .line 178
    const-string v5, "group"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 186
    .end local v4    # "tag":Ljava/lang/String;
    :cond_7
    return-void
.end method

.method public getObject(I)Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 239
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 240
    const-string v0, "SPRObjectShapeGroup"

    const-string v1, "Already finalize"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    const/4 v0, 0x0

    .line 243
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    goto :goto_0
.end method

.method public getObjectCount()I
    .locals 2

    .prologue
    .line 231
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 232
    const-string v0, "SPRObjectShapeGroup"

    const-string v1, "Already finalize"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    const/4 v0, 0x0

    .line 235
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getSPRSize()I
    .locals 1

    .prologue
    .line 249
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->getSPRSize()I

    move-result v0

    return v0
.end method

.method public getTotalAttributeCount()I
    .locals 4

    .prologue
    .line 286
    const/4 v2, 0x0

    .line 287
    .local v2, "total":I
    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .line 288
    .local v0, "child":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->getTotalAttributeCount()I

    move-result v3

    add-int/2addr v2, v3

    .line 289
    goto :goto_0

    .line 291
    .end local v0    # "child":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v3, v2

    return v3
.end method

.method public getTotalElementCount()I
    .locals 4

    .prologue
    .line 276
    const/4 v2, 0x0

    .line 277
    .local v2, "total":I
    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .line 278
    .local v0, "child":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->getTotalElementCount()I

    move-result v3

    add-int/2addr v2, v3

    .line 279
    goto :goto_0

    .line 281
    .end local v0    # "child":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :cond_0
    return v2
.end method

.method public getTotalSegmentCount()I
    .locals 4

    .prologue
    .line 266
    const/4 v2, 0x0

    .line 267
    .local v2, "total":I
    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .line 268
    .local v0, "child":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->getTotalSegmentCount()I

    move-result v3

    add-int/2addr v2, v3

    .line 269
    goto :goto_0

    .line 271
    .end local v0    # "child":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :cond_0
    return v2
.end method

.method public removeObject(I)Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 222
    iget-boolean v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsInitialized:Z

    if-nez v0, :cond_0

    .line 223
    const-string v0, "SPRObjectShapeGroup"

    const-string v1, "Already finalize"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    const/4 v0, 0x0

    .line 227
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    goto :goto_0
.end method

.method public removeObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)Z
    .locals 4
    .param p1, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .prologue
    .line 206
    iget-boolean v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsInitialized:Z

    if-nez v2, :cond_0

    .line 207
    const-string v2, "SPRObjectShapeGroup"

    const-string v3, "Already finalize"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    const/4 v2, 0x0

    .line 218
    :goto_0
    return v2

    .line 211
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .line 212
    .local v1, "obj":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    iget-byte v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mType:B

    const/16 v3, 0x10

    if-ne v2, v3, :cond_1

    .line 213
    check-cast v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;

    .end local v1    # "obj":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->removeObject(Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 214
    const/4 v2, 0x1

    goto :goto_0

    .line 218
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method

.method public setDisplayLayout(ZFFFFFFFFF)V
    .locals 12
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "npLeft"    # F
    .param p4, "npTop"    # F
    .param p5, "npRight"    # F
    .param p6, "npBottom"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "displayWidth"    # F
    .param p10, "displayHeight"    # F

    .prologue
    .line 63
    invoke-super/range {p0 .. p10}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->setDisplayLayout(ZFFFFFFFFF)V

    .line 66
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .local v0, "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    move v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    .line 67
    invoke-virtual/range {v0 .. v10}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->setDisplayLayout(ZFFFFFFFFF)V

    goto :goto_0

    .line 70
    .end local v0    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :cond_0
    return-void
.end method

.method public toSPR(Ljava/io/DataOutputStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 128
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 130
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .line 131
    .local v1, "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    iget-byte v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mType:B

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 132
    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->toSPR(Ljava/io/DataOutputStream;)V

    goto :goto_0

    .line 135
    .end local v1    # "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeGroup;->mIsRoot:Z

    if-nez v2, :cond_1

    .line 136
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->toSPR(Ljava/io/DataOutputStream;)V

    .line 138
    :cond_1
    return-void
.end method
