.class public Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;
.super Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;
.source "SprRadialGradient.java"


# instance fields
.field public cx:F

.field public cy:F

.field private final mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;

.field public r:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;-><init>()V

    .line 18
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/io/DataInputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;-><init>(Ljava/io/DataInputStream;)V

    .line 23
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;

    .line 24
    return-void
.end method


# virtual methods
.method public readBody(Ljava/io/DataInputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->cx:F

    .line 29
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->cy:F

    .line 30
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->r:F

    .line 31
    return-void
.end method

.method public setDisplayLayoutBody(ZFFFFFFFFF)V
    .locals 1
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "npLeft"    # F
    .param p4, "npTop"    # F
    .param p5, "npRight"    # F
    .param p6, "npBottom"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "displayWidth"    # F
    .param p10, "displayHeight"    # F

    .prologue
    .line 83
    if-eqz p1, :cond_0

    .line 84
    div-float/2addr p9, p2

    .line 85
    div-float/2addr p10, p2

    .line 87
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->cx:F

    invoke-static {v0, p3, p5, p7, p9}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->cx:F

    .line 88
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->cy:F

    invoke-static {v0, p4, p6, p8, p10}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->cy:F

    .line 89
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->r:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->r:F

    .line 95
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->cx:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->cx:F

    .line 92
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->cy:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->cy:F

    .line 93
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->r:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->r:F

    goto :goto_0
.end method

.method public updateGradient()V
    .locals 11

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 42
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->positions:[F

    array-length v9, v0

    .line 43
    .local v9, "size":I
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->positions:[F

    add-int/lit8 v1, v9, -0x1

    aget v0, v0, v1

    cmpl-float v0, v0, v6

    if-eqz v0, :cond_0

    .line 44
    add-int/lit8 v9, v9, 0x1

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->positions:[F

    aget v0, v0, v2

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_1

    .line 47
    add-int/lit8 v9, v9, 0x1

    .line 49
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->colors:[I

    .line 50
    .local v4, "lcolors":[I
    iget-object v5, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->positions:[F

    .line 52
    .local v5, "lpositions":[F
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->positions:[F

    array-length v0, v0

    if-eq v9, v0, :cond_4

    .line 53
    new-array v4, v9, [I

    .line 54
    new-array v5, v9, [F

    .line 55
    const/4 v8, 0x0

    .line 56
    .local v8, "index":I
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->positions:[F

    aget v0, v0, v2

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_2

    .line 57
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->colors:[I

    aget v0, v0, v2

    aput v0, v4, v2

    .line 58
    aput v3, v5, v2

    .line 59
    add-int/lit8 v8, v8, 0x1

    .line 62
    :cond_2
    const/4 v7, 0x0

    .local v7, "cnt":I
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->colors:[I

    array-length v0, v0

    if-ge v7, v0, :cond_3

    .line 63
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->colors:[I

    aget v0, v0, v7

    aput v0, v4, v8

    .line 64
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->positions:[F

    aget v0, v0, v7

    aput v0, v5, v8

    .line 62
    add-int/lit8 v7, v7, 0x1

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 67
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->positions:[F

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->positions:[F

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    cmpl-float v0, v0, v6

    if-eqz v0, :cond_4

    .line 68
    add-int/lit8 v0, v9, -0x1

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->colors:[I

    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->positions:[F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    aput v1, v4, v0

    .line 69
    add-int/lit8 v0, v9, -0x1

    aput v6, v5, v0

    .line 73
    .end local v7    # "cnt":I
    .end local v8    # "index":I
    :cond_4
    new-instance v0, Landroid/graphics/RadialGradient;

    iget v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->cx:F

    iget v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->cy:F

    iget v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->r:F

    sget-object v6, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->sTileModeArray:[Landroid/graphics/Shader$TileMode;

    iget-byte v10, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->spreadMode:B

    add-int/lit8 v10, v10, -0x1

    aget-object v6, v6, v10

    invoke-direct/range {v0 .. v6}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->shader:Landroid/graphics/Shader;

    .line 74
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->matrix:Landroid/graphics/Matrix;

    if-eqz v0, :cond_5

    .line 75
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->shader:Landroid/graphics/Shader;

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 77
    :cond_5
    return-void
.end method

.method public writeBody(Ljava/io/DataOutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->cx:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 36
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->cy:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 37
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;->r:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 38
    return-void
.end method
