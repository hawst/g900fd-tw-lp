.class public abstract Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
.super Ljava/lang/Object;
.source "SprObjectBase.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static final TAG:Ljava/lang/String;

.field public static final TYPE_CIRCLE:B = 0x1t

.field public static final TYPE_ELLIPSE:B = 0x2t

.field public static final TYPE_GROUP:B = 0x10t

.field public static final TYPE_LINE:B = 0x3t

.field public static final TYPE_NONE:B = 0x0t

.field public static final TYPE_PATH:B = 0x4t

.field public static final TYPE_RECTANGLE:B = 0x5t

.field public static final TYPE_USE:B = 0x11t


# instance fields
.field public fillPaint:Landroid/graphics/Paint;

.field public isVisibleFill:Z

.field public isVisibleStroke:Z

.field public mAttributeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;",
            ">;"
        }
    .end annotation
.end field

.field protected final mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

.field public final mType:B

.field public strokePaint:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(B)V
    .locals 2
    .param p1, "type"    # B

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    .line 49
    iput-boolean v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleStroke:Z

    .line 51
    iput-boolean v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleFill:Z

    .line 57
    iput-object p0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .line 58
    iput-byte p1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mType:B

    .line 59
    return-void
.end method

.method private getAttributeSize()I
    .locals 4

    .prologue
    .line 169
    const/4 v2, 0x4

    .line 170
    .local v2, "size":I
    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    .line 171
    .local v1, "object":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    invoke-virtual {v1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->getSPRSize()I

    move-result v3

    add-int/2addr v2, v3

    .line 172
    goto :goto_0

    .line 174
    .end local v1    # "object":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    :cond_0
    return v2
.end method

.method private loadAttributeFromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V
    .locals 7
    .param p1, "in"    # Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 100
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readInt()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 101
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readByte()B

    move-result v1

    .line 102
    .local v1, "id":I
    const/4 v3, 0x0

    .line 104
    .local v3, "size":I
    iget-short v4, p1, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->mMajorVersion:S

    const/16 v5, 0x3030

    if-lt v4, v5, :cond_0

    iget-short v4, p1, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->mMinorVersion:S

    const/16 v5, 0x3032

    if-lt v4, v5, :cond_0

    .line 105
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readInt()I

    move-result v3

    .line 108
    :cond_0
    sparse-switch v1, :sswitch_data_0

    .line 149
    sget-object v4, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown attribute id:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    :goto_1
    if-lez v3, :cond_1

    .line 151
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readByte()B

    .line 152
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 113
    :sswitch_0
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;

    invoke-direct {v5, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;-><init>(Ljava/io/DataInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    :cond_1
    :goto_2
    :sswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117
    :sswitch_2
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClipPath;

    invoke-direct {v5, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClipPath;-><init>(Ljava/io/DataInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 121
    :sswitch_3
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;

    invoke-direct {v5, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;-><init>(Ljava/io/DataInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 125
    :sswitch_4
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;

    invoke-direct {v5, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStroke;-><init>(Ljava/io/DataInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 129
    :sswitch_5
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinecap;

    invoke-direct {v5, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinecap;-><init>(Ljava/io/DataInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 133
    :sswitch_6
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;

    invoke-direct {v5, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;-><init>(Ljava/io/DataInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 137
    :sswitch_7
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;

    invoke-direct {v5, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;-><init>(Ljava/io/DataInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 141
    :sswitch_8
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeMiterlimit;

    invoke-direct {v5, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeMiterlimit;-><init>(Ljava/io/DataInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 145
    :sswitch_9
    iget-object v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    new-instance v5, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;

    invoke-direct {v5, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeMatrix;-><init>(Ljava/io/DataInputStream;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 157
    .end local v1    # "id":I
    .end local v3    # "size":I
    :cond_2
    return-void

    .line 108
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_0
        0x3 -> :sswitch_2
        0x20 -> :sswitch_3
        0x23 -> :sswitch_4
        0x25 -> :sswitch_5
        0x26 -> :sswitch_6
        0x28 -> :sswitch_7
        0x29 -> :sswitch_8
        0x40 -> :sswitch_9
    .end sparse-switch
.end method

.method private saveAttributeToSPR(Ljava/io/DataOutputStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 160
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 162
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    .line 163
    .local v1, "object":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    iget-byte v2, v1, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mType:B

    invoke-virtual {p1, v2}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 164
    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->toSPR(Ljava/io/DataOutputStream;)V

    goto :goto_0

    .line 166
    .end local v1    # "object":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    :cond_0
    return-void
.end method


# virtual methods
.method protected adjustNinePatchPosition(FFFFF)F
    .locals 2
    .param p1, "position"    # F
    .param p2, "ninePatchBegin"    # F
    .param p3, "ninePatchEnd"    # F
    .param p4, "sourceSize"    # F
    .param p5, "targetSize"    # F

    .prologue
    .line 179
    cmpl-float v0, p1, p2

    if-lez v0, :cond_1

    sub-float v0, p4, p3

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 180
    sub-float v0, p5, p2

    sub-float/2addr v0, p3

    sub-float v1, p1, p2

    mul-float/2addr v0, v1

    sub-float v1, p4, p3

    sub-float/2addr v1, p2

    div-float/2addr v0, v1

    add-float p1, p2, v0

    .line 186
    :cond_0
    :goto_0
    return p1

    .line 182
    :cond_1
    sub-float v0, p4, p3

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    .line 183
    sub-float v0, p5, p3

    sub-float v1, p4, p3

    sub-float v1, p1, v1

    add-float p1, v0, v1

    goto :goto_0
.end method

.method public appendAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V
    .locals 1
    .param p1, "attribute"    # Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    return-void
.end method

.method public clone()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 191
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    .line 193
    .local v2, "object":Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    .line 195
    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    .line 196
    .local v0, "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    iget-object v3, v2, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 199
    .end local v0    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    :cond_0
    return-object v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->clone()Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    move-result-object v0

    return-object v0
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 63
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 65
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 66
    return-void
.end method

.method public fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V
    .locals 0
    .param p1, "in"    # Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->loadAttributeFromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 86
    return-void
.end method

.method public getSPRSize()I
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->getAttributeSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getTotalAttributeCount()I
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public abstract getTotalElementCount()I
.end method

.method public abstract getTotalSegmentCount()I
.end method

.method public removeAttribute(Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;)V
    .locals 1
    .param p1, "attribute"    # Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method

.method public setDisplayLayout(ZFFFFFFFFF)V
    .locals 12
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "npLeft"    # F
    .param p4, "npTop"    # F
    .param p5, "npRight"    # F
    .param p6, "npBottom"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "displayWidth"    # F
    .param p10, "displayHeight"    # F

    .prologue
    .line 78
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mAttributeList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    .local v0, "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    move v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    .line 79
    invoke-virtual/range {v0 .. v10}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->setDisplayLayout(ZFFFFFFFFF)V

    goto :goto_0

    .line 82
    .end local v0    # "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    :cond_0
    return-void
.end method

.method public toSPR(Ljava/io/DataOutputStream;)V
    .locals 0
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->saveAttributeToSPR(Ljava/io/DataOutputStream;)V

    .line 90
    return-void
.end method
