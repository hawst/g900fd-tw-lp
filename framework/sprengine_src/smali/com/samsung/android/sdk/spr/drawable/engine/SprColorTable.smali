.class public Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;
.super Ljava/lang/Object;
.source "SprColorTable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;
    }
.end annotation


# static fields
.field public static final COLOR_THEME_SIZE:I = 0xa

.field public static final THEME_TYPE_AMBER:I = 0xe

.field public static final THEME_TYPE_BLUE:I = 0x6

.field public static final THEME_TYPE_BLUE_GREY:I = 0x13

.field public static final THEME_TYPE_BROWN:I = 0x11

.field public static final THEME_TYPE_CYAN:I = 0x8

.field public static final THEME_TYPE_DEEP_ORANGE:I = 0x10

.field public static final THEME_TYPE_DEEP_PURPLE:I = 0x4

.field public static final THEME_TYPE_GREEN:I = 0xa

.field public static final THEME_TYPE_GREY:I = 0x12

.field public static final THEME_TYPE_INDIGO:I = 0x5

.field public static final THEME_TYPE_LIGHT_BLUE:I = 0x7

.field public static final THEME_TYPE_LIGHT_GREEN:I = 0xb

.field public static final THEME_TYPE_LIME:I = 0xc

.field public static final THEME_TYPE_ORANGE:I = 0xf

.field public static final THEME_TYPE_PINK:I = 0x2

.field public static final THEME_TYPE_PURPLE:I = 0x3

.field public static final THEME_TYPE_RED:I = 0x1

.field public static final THEME_TYPE_TEAL:I = 0x9

.field public static final THEME_TYPE_USER_THEME:I = 0x0

.field public static final THEME_TYPE_YELLOW:I = 0xd

.field private static final amberColorTable:[I

.field private static final blueColorTable:[I

.field private static final blueGreyColorTable:[I

.field private static final brownColorTable:[I

.field public static colorThemeTable:[I

.field private static final cyanColorTable:[I

.field private static final deepOrangeColorTable:[I

.field private static final deepPurpleColorTable:[I

.field private static final greenColorTable:[I

.field private static final greyColorTable:[I

.field private static final indigoColorTable:[I

.field private static final lightBlueColorTable:[I

.field private static final lightGreenColorTable:[I

.field private static final limeColorTable:[I

.field private static mThemeChangeListenerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private static mThemeType:I

.field private static final orangeColorTable:[I

.field private static final pinkColorTable:[I

.field private static final purpleColorTable:[I

.field private static final redColorTable:[I

.field private static final tealColorTable:[I

.field private static final yellowColorTable:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 30
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->redColorTable:[I

    .line 32
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->pinkColorTable:[I

    .line 34
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->purpleColorTable:[I

    .line 36
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->deepPurpleColorTable:[I

    .line 38
    new-array v0, v1, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->indigoColorTable:[I

    .line 40
    new-array v0, v1, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->blueColorTable:[I

    .line 42
    new-array v0, v1, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->lightBlueColorTable:[I

    .line 44
    new-array v0, v1, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->cyanColorTable:[I

    .line 46
    new-array v0, v1, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->tealColorTable:[I

    .line 48
    new-array v0, v1, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->greenColorTable:[I

    .line 50
    new-array v0, v1, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->lightGreenColorTable:[I

    .line 52
    new-array v0, v1, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->limeColorTable:[I

    .line 54
    new-array v0, v1, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->yellowColorTable:[I

    .line 56
    new-array v0, v1, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->amberColorTable:[I

    .line 58
    new-array v0, v1, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->orangeColorTable:[I

    .line 60
    new-array v0, v1, [I

    fill-array-data v0, :array_f

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->deepOrangeColorTable:[I

    .line 62
    new-array v0, v1, [I

    fill-array-data v0, :array_10

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->brownColorTable:[I

    .line 64
    new-array v0, v1, [I

    fill-array-data v0, :array_11

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->greyColorTable:[I

    .line 66
    new-array v0, v1, [I

    fill-array-data v0, :array_12

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->blueGreyColorTable:[I

    .line 70
    const/16 v0, 0x12

    sput v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeType:I

    .line 71
    new-array v0, v1, [I

    fill-array-data v0, :array_13

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->colorThemeTable:[I

    .line 74
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeChangeListenerList:Ljava/util/ArrayList;

    return-void

    .line 30
    :array_0
    .array-data 4
        0xfde0dc
        0xf9bdbb
        0xf69988
        0xf36c60
        0xe84e40
        0xe51c23
        0xdd191d
        0xd01716
        0xc41411
        0xb0120a
    .end array-data

    .line 32
    :array_1
    .array-data 4
        0xfce4ce
        0xf8bbd0
        0xf48fb1
        0xf06292
        0xec407a
        0xe91e63
        0xd81b60
        0xc2185b
        0xad1457
        0x880e4f
    .end array-data

    .line 34
    :array_2
    .array-data 4
        0xf3e5f5
        0xe1bee7
        0xce93d8
        0xba68c8
        0xab47bc
        0x9c27b0
        0x8e24aa
        0x7b1fa2
        0x6a1b9a
        0x4a148c
    .end array-data

    .line 36
    :array_3
    .array-data 4
        0xede7f6
        0xd1c4e9
        0xb39ddb
        0x9575cd
        0x7e57c2
        0x673ab7
        0x5e35b1
        0x512da8
        0x4527a0
        0x311b92
    .end array-data

    .line 38
    :array_4
    .array-data 4
        0xe8eaf6
        0xc5cae9
        0x9fa8da
        0x7986cb
        0x5c6bc0
        0x3f51b5
        0x3949ab
        0x303f9f
        0x283593
        0x1a237e
    .end array-data

    .line 40
    :array_5
    .array-data 4
        0xe7e9fd
        0xd0d9ff
        0xafbfff
        0x91a7ff
        0x738ffe
        0x5677fc
        0x4e6cef
        0x455ede
        0x3b50ce
        0x2a36b1
    .end array-data

    .line 42
    :array_6
    .array-data 4
        0xe1f5fe
        0xb3e5fc
        0x81d4fa
        0x4fc3f7
        0x29b6f6
        0x3a9f4
        0x39be5
        0x288d1
        0x277bd
        0x1579b
    .end array-data

    .line 44
    :array_7
    .array-data 4
        0xe0f7fa
        0xb2ebf2
        0x80deea
        0x4dd0e1
        0x26c6da
        0xbcd4
        0xacc1
        0x97a7
        0x838f
        0x6064
    .end array-data

    .line 46
    :array_8
    .array-data 4
        0xe0f2f1
        0xb2dfdb
        0x80cbc4
        0x4db6ac
        0x26a69a
        0x9688    # 5.4E-41f
        0x897b
        0x796b
        0x695c
        0x4d40
    .end array-data

    .line 48
    :array_9
    .array-data 4
        0xd0f8ce
        0xa3e9a4
        0x72d572
        0x42bd41
        0x2baf2b
        0x259b24
        0xa8f08
        0xa7e07
        0x56f00
        0xd5302
    .end array-data

    .line 50
    :array_a
    .array-data 4
        0xf1f8e9
        0xdcedc8
        0xc5e1a5
        0xaed581
        0x9ccc65
        0x8bc34a
        0x7cb342
        0x689f38
        0x558b2f
        0x33691e
    .end array-data

    .line 52
    :array_b
    .array-data 4
        0xf9fbe7
        0xf0f4c3
        0xe6ee9c
        0xdce775
        0xd4e157
        0xcddc39
        0xc0ca33
        0xafb42b
        0x9e9d24
        0x827717
    .end array-data

    .line 54
    :array_c
    .array-data 4
        0xfffde7
        0xfff9c4
        0xfff59d
        0xfff176
        0xffee58
        0xffeb3b
        0xfdd835
        0xfbc02d
        0xf9a825
        0xf57f17
    .end array-data

    .line 56
    :array_d
    .array-data 4
        0xfff8e1
        0xffecb3
        0xffe082
        0xffd54f
        0xffca28
        0xffc107
        0xffb300
        0xffa000
        0xff8f00
        0xff6f00
    .end array-data

    .line 58
    :array_e
    .array-data 4
        0xfff3e0
        0xffe0b2
        0xffcc80
        0xffb74d
        0xffa726
        0xff9800
        0xfb8c00
        0xf57c00
        0xef6c00
        0xe65100
    .end array-data

    .line 60
    :array_f
    .array-data 4
        0xfbe9e7
        0xffccbc
        0xffab91
        0xff8a65
        0xff7043
        0xff5722
        0xf4511e
        0xe64a19
        0xd84315
        0xbf360c
    .end array-data

    .line 62
    :array_10
    .array-data 4
        0xefebe9
        0xd7ccc8
        0xbcaaa4
        0xa1887f
        0x8d6e63
        0x795548
        0x6d4c41
        0x5d4037
        0x4e342e
        0x3e2723
    .end array-data

    .line 64
    :array_11
    .array-data 4
        0xfafafa
        0xf5f5f5
        0xeeeeee
        0xe0e0e0
        0xbdbdbd
        0x9e9e9e
        0x757575
        0x616161
        0x424242
        0x212121
    .end array-data

    .line 66
    :array_12
    .array-data 4
        0xeceff1
        0xcfd8dc
        0xb0bec5
        0x90a4ae
        0x78909c
        0x607d8b
        0x546e7a
        0x455a64
        0x37474f
        0x263238
    .end array-data

    .line 71
    :array_13
    .array-data 4
        0xfafafa
        0xf5f5f5
        0xeeeeee
        0xe0e0e0
        0xbdbdbd
        0x9e9e9e
        0x757575
        0x616161
        0x424242
        0x212121
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    return-void
.end method

.method public static deregisterThemeChangeListener(Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;)V
    .locals 4
    .param p0, "listener"    # Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;

    .prologue
    .line 96
    sget-object v3, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->colorThemeTable:[I

    monitor-enter v3

    .line 97
    if-eqz p0, :cond_0

    :try_start_0
    sget-object v2, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeChangeListenerList:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 98
    :cond_0
    monitor-exit v3

    .line 112
    :goto_0
    return-void

    .line 101
    :cond_1
    sget-object v2, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeChangeListenerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 102
    .local v1, "l":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p0, :cond_2

    .line 103
    sget-object v2, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeChangeListenerList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 108
    .end local v1    # "l":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;>;"
    :cond_3
    sget-object v2, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeChangeListenerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_4

    .line 109
    const/4 v2, 0x0

    sput-object v2, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeChangeListenerList:Ljava/util/ArrayList;

    .line 111
    :cond_4
    monitor-exit v3

    goto :goto_0

    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static getThemeType()I
    .locals 1

    .prologue
    .line 127
    sget v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeType:I

    return v0
.end method

.method private static onThemeChange()V
    .locals 4

    .prologue
    .line 115
    sget-object v3, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->colorThemeTable:[I

    monitor-enter v3

    .line 116
    :try_start_0
    sget-object v2, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeChangeListenerList:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 117
    sget-object v2, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeChangeListenerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 118
    .local v1, "listener":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 119
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;

    invoke-interface {v2}, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;->onChange()V

    goto :goto_0

    .line 123
    .end local v1    # "listener":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    :cond_1
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 124
    return-void
.end method

.method public static registerThemeChangeListener(Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;)V
    .locals 3
    .param p0, "listener"    # Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;

    .prologue
    .line 81
    if-nez p0, :cond_0

    .line 93
    :goto_0
    return-void

    .line 85
    :cond_0
    sget-object v2, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->colorThemeTable:[I

    monitor-enter v2

    .line 86
    :try_start_0
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeChangeListenerList:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 87
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sput-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeChangeListenerList:Ljava/util/ArrayList;

    .line 89
    :cond_1
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 91
    .local v0, "weakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;>;"
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeChangeListenerList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    monitor-exit v2

    goto :goto_0

    .end local v0    # "weakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable$SprThemeChangeListener;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static setThemeType(I)V
    .locals 5
    .param p0, "themeType"    # I

    .prologue
    .line 131
    sget-object v3, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->colorThemeTable:[I

    monitor-enter v3

    .line 132
    :try_start_0
    sget v2, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeType:I

    if-ne v2, p0, :cond_0

    .line 133
    monitor-exit v3

    .line 228
    :goto_0
    return-void

    .line 136
    :cond_0
    const/4 v1, 0x0

    .line 138
    .local v1, "themeTable":[I
    packed-switch p0, :pswitch_data_0

    .line 219
    :goto_1
    if-eqz v1, :cond_2

    .line 220
    sput p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeType:I

    .line 221
    const/4 v0, 0x0

    .local v0, "cnt":I
    :goto_2
    const/16 v2, 0xa

    if-ge v0, v2, :cond_1

    .line 222
    sget-object v2, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->colorThemeTable:[I

    aget v4, v1, v0

    aput v4, v2, v0

    .line 221
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 140
    .end local v0    # "cnt":I
    :pswitch_0
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->redColorTable:[I

    .line 141
    goto :goto_1

    .line 144
    :pswitch_1
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->pinkColorTable:[I

    .line 145
    goto :goto_1

    .line 148
    :pswitch_2
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->purpleColorTable:[I

    .line 149
    goto :goto_1

    .line 152
    :pswitch_3
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->deepPurpleColorTable:[I

    .line 153
    goto :goto_1

    .line 156
    :pswitch_4
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->indigoColorTable:[I

    .line 157
    goto :goto_1

    .line 160
    :pswitch_5
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->blueColorTable:[I

    .line 161
    goto :goto_1

    .line 164
    :pswitch_6
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->lightBlueColorTable:[I

    .line 165
    goto :goto_1

    .line 168
    :pswitch_7
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->cyanColorTable:[I

    .line 169
    goto :goto_1

    .line 172
    :pswitch_8
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->tealColorTable:[I

    .line 173
    goto :goto_1

    .line 176
    :pswitch_9
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->greenColorTable:[I

    .line 177
    goto :goto_1

    .line 180
    :pswitch_a
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->lightGreenColorTable:[I

    .line 181
    goto :goto_1

    .line 184
    :pswitch_b
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->limeColorTable:[I

    .line 185
    goto :goto_1

    .line 188
    :pswitch_c
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->yellowColorTable:[I

    .line 189
    goto :goto_1

    .line 192
    :pswitch_d
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->amberColorTable:[I

    .line 193
    goto :goto_1

    .line 196
    :pswitch_e
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->orangeColorTable:[I

    .line 197
    goto :goto_1

    .line 200
    :pswitch_f
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->deepOrangeColorTable:[I

    .line 201
    goto :goto_1

    .line 204
    :pswitch_10
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->brownColorTable:[I

    .line 205
    goto :goto_1

    .line 208
    :pswitch_11
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->greyColorTable:[I

    .line 209
    goto :goto_1

    .line 212
    :pswitch_12
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->blueGreyColorTable:[I

    .line 213
    goto :goto_1

    .line 225
    .restart local v0    # "cnt":I
    :cond_1
    invoke-static {}, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->onThemeChange()V

    .line 227
    .end local v0    # "cnt":I
    :cond_2
    monitor-exit v3

    goto :goto_0

    .end local v1    # "themeTable":[I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 138
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method

.method public static setUserTheme([I)Z
    .locals 5
    .param p0, "userColorTheme"    # [I

    .prologue
    const/16 v4, 0xa

    const/4 v1, 0x0

    .line 231
    sget-object v2, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->colorThemeTable:[I

    monitor-enter v2

    .line 232
    :try_start_0
    array-length v3, p0

    if-eq v3, v4, :cond_0

    .line 233
    monitor-exit v2

    .line 241
    :goto_0
    return v1

    .line 235
    :cond_0
    const/4 v1, 0x0

    sput v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->mThemeType:I

    .line 236
    const/4 v0, 0x0

    .local v0, "cnt":I
    :goto_1
    if-ge v0, v4, :cond_1

    .line 237
    sget-object v1, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->colorThemeTable:[I

    aget v3, p0, v0

    aput v3, v1, v0

    .line 236
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 239
    :cond_1
    invoke-static {}, Lcom/samsung/android/sdk/spr/drawable/engine/SprColorTable;->onThemeChange()V

    .line 240
    monitor-exit v2

    .line 241
    const/4 v1, 0x1

    goto :goto_0

    .line 240
    .end local v0    # "cnt":I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
