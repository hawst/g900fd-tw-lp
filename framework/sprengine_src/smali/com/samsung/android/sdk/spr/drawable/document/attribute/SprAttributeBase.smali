.class public abstract Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
.super Ljava/lang/Object;
.source "SprAttributeBase.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final TYPE_CLIP:B = 0x1t

.field public static final TYPE_CLIP_PATH:B = 0x3t

.field public static final TYPE_FILL:B = 0x20t

.field public static final TYPE_MATRIX:B = 0x40t

.field public static final TYPE_NONE:B = 0x0t

.field public static final TYPE_STROKE:B = 0x23t

.field public static final TYPE_STROKE_LINECAP:B = 0x25t

.field public static final TYPE_STROKE_LINEJOIN:B = 0x26t

.field public static final TYPE_STROKE_MITERLIMIT:B = 0x29t

.field public static final TYPE_STROKE_WIDTH:B = 0x28t


# instance fields
.field protected final mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

.field public final mType:B


# direct methods
.method protected constructor <init>(B)V
    .locals 0
    .param p1, "type"    # B

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-byte p1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mType:B

    .line 42
    iput-object p0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    .line 43
    return-void
.end method

.method public static adjustNinePatchPosition(FFFFF)F
    .locals 2
    .param p0, "position"    # F
    .param p1, "ninePatchBegin"    # F
    .param p2, "ninePatchEnd"    # F
    .param p3, "sourceSize"    # F
    .param p4, "targetSize"    # F

    .prologue
    .line 64
    cmpl-float v0, p0, p1

    if-lez v0, :cond_1

    sub-float v0, p3, p2

    cmpg-float v0, p0, v0

    if-gez v0, :cond_1

    .line 65
    sub-float v0, p4, p1

    sub-float/2addr v0, p2

    sub-float v1, p0, p1

    mul-float/2addr v0, v1

    sub-float v1, p3, p2

    sub-float/2addr v1, p1

    div-float/2addr v0, v1

    add-float p0, p1, v0

    .line 71
    :cond_0
    :goto_0
    return p0

    .line 67
    :cond_1
    sub-float v0, p3, p2

    cmpl-float v0, p0, v0

    if-ltz v0, :cond_0

    .line 68
    sub-float v0, p4, p2

    sub-float v1, p3, p2

    sub-float v1, p0, v1

    add-float p0, v0, v1

    goto :goto_0
.end method


# virtual methods
.method public clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 7
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    move-result-object v0

    return-object v0
.end method

.method public abstract fromSPR(Ljava/io/DataInputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getSPRSize()I
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    return v0
.end method

.method public setDisplayLayout(ZFFFFFFFFF)V
    .locals 0
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "npLeft"    # F
    .param p4, "npTop"    # F
    .param p5, "npRight"    # F
    .param p6, "npBottom"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "displayWidth"    # F
    .param p10, "displayHeight"    # F

    .prologue
    .line 60
    return-void
.end method

.method public abstract toSPR(Ljava/io/DataOutputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
