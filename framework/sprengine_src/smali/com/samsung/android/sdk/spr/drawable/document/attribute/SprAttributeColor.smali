.class public abstract Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;
.super Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
.source "SprAttributeColor.java"


# static fields
.field public static final TYPE_ARGB:B = 0x1t

.field public static final TYPE_LINEAR_GRADIENT:B = 0x3t

.field public static final TYPE_LINK:B = 0x2t

.field public static final TYPE_NONE:B = 0x0t

.field public static final TYPE_RADIAL_GRADIENT:B = 0x4t


# instance fields
.field public color:I

.field public colorType:B

.field public gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;


# direct methods
.method public constructor <init>(B)V
    .locals 2
    .param p1, "type"    # B

    .prologue
    const/4 v1, 0x1

    .line 29
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 23
    iput-byte v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->colorType:B

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    .line 30
    iput-byte v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->colorType:B

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->color:I

    .line 32
    return-void
.end method

.method public constructor <init>(BBI)V
    .locals 3
    .param p1, "type"    # B
    .param p2, "colorType"    # B
    .param p3, "value"    # I

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 23
    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->colorType:B

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    .line 37
    packed-switch p2, :pswitch_data_0

    .line 47
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unexpected stroke type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :pswitch_0
    iput p3, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->color:I

    .line 50
    :pswitch_1
    iput-byte p2, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->colorType:B

    .line 51
    return-void

    .line 37
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public constructor <init>(BBLcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;)V
    .locals 3
    .param p1, "type"    # B
    .param p2, "colorType"    # B
    .param p3, "gradient"    # Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 23
    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->colorType:B

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    .line 56
    packed-switch p2, :pswitch_data_0

    .line 66
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unexpected stroke type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :pswitch_1
    iput-object p3, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    .line 69
    :pswitch_2
    iput-byte p2, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->colorType:B

    .line 70
    return-void

    .line 56
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public constructor <init>(BLjava/io/DataInputStream;)V
    .locals 1
    .param p1, "type"    # B
    .param p2, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 23
    const/4 v0, 0x1

    iput-byte v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->colorType:B

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    .line 74
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->fromSPR(Ljava/io/DataInputStream;)V

    .line 75
    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 145
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;

    .line 147
    .local v0, "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    if-eqz v1, :cond_0

    .line 148
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    .line 151
    :cond_0
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;

    move-result-object v0

    return-object v0
.end method

.method public fromSPR(Ljava/io/DataInputStream;)V
    .locals 3
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->colorType:B

    .line 81
    iget-byte v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->colorType:B

    packed-switch v0, :pswitch_data_0

    .line 100
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown fill type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->colorType:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :pswitch_0
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    .line 102
    :goto_0
    return-void

    .line 88
    :pswitch_1
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->color:I

    goto :goto_0

    .line 92
    :pswitch_2
    new-instance v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;-><init>(Ljava/io/DataInputStream;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    goto :goto_0

    .line 96
    :pswitch_3
    new-instance v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprRadialGradient;-><init>(Ljava/io/DataInputStream;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    goto :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getSPRSize()I
    .locals 1

    .prologue
    .line 130
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->getSPRSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x5

    return v0
.end method

.method public setDisplayLayout(ZFFFFFFFFF)V
    .locals 11
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "npLeft"    # F
    .param p4, "npTop"    # F
    .param p5, "npRight"    # F
    .param p6, "npBottom"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "displayWidth"    # F
    .param p10, "displayHeight"    # F

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    invoke-virtual/range {v0 .. v10}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->setDisplayLayout(ZFFFFFFFFF)V

    .line 141
    :cond_0
    return-void
.end method

.method public toSPR(Ljava/io/DataOutputStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 106
    iget-byte v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->colorType:B

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 108
    iget-byte v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->colorType:B

    packed-switch v0, :pswitch_data_0

    .line 124
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown fill type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-byte v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->colorType:B

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :pswitch_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 126
    :goto_0
    return-void

    .line 115
    :pswitch_1
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->color:I

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto :goto_0

    .line 120
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;->gradient:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->toSPR(Ljava/io/DataOutputStream;)V

    goto :goto_0

    .line 108
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
