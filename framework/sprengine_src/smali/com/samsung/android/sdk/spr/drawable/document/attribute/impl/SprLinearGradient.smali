.class public Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;
.super Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;
.source "SprLinearGradient.java"


# instance fields
.field private final mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;

.field public x1:F

.field public x2:F

.field public y1:F

.field public y2:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;-><init>()V

    .line 19
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;

    .line 20
    return-void
.end method

.method public constructor <init>(Ljava/io/DataInputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;-><init>(Ljava/io/DataInputStream;)V

    .line 24
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;

    .line 25
    return-void
.end method


# virtual methods
.method public readBody(Ljava/io/DataInputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x1:F

    .line 30
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y1:F

    .line 31
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x2:F

    .line 32
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y2:F

    .line 33
    return-void
.end method

.method public setDisplayLayoutBody(ZFFFFFFFFF)V
    .locals 1
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "npLeft"    # F
    .param p4, "npTop"    # F
    .param p5, "npRight"    # F
    .param p6, "npBottom"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "displayWidth"    # F
    .param p10, "displayHeight"    # F

    .prologue
    .line 55
    if-eqz p1, :cond_0

    .line 56
    div-float/2addr p9, p2

    .line 57
    div-float/2addr p10, p2

    .line 59
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x1:F

    invoke-static {v0, p3, p5, p7, p9}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x1:F

    .line 60
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y1:F

    invoke-static {v0, p4, p6, p8, p10}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y1:F

    .line 61
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x2:F

    invoke-static {v0, p3, p5, p7, p9}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x2:F

    .line 62
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y2:F

    invoke-static {v0, p4, p6, p8, p10}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y2:F

    .line 70
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x1:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x1:F

    .line 66
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y1:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y1:F

    .line 67
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x2:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x2:F

    .line 68
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y2:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y2:F

    goto :goto_0
.end method

.method public updateGradient()V
    .locals 9

    .prologue
    .line 45
    new-instance v0, Landroid/graphics/LinearGradient;

    iget v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x1:F

    iget v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y1:F

    iget v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x2:F

    iget v4, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y2:F

    iget-object v5, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->colors:[I

    iget-object v6, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->positions:[F

    sget-object v7, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->sTileModeArray:[Landroid/graphics/Shader$TileMode;

    iget-byte v8, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->spreadMode:B

    add-int/lit8 v8, v8, -0x1

    aget-object v7, v7, v8

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->shader:Landroid/graphics/Shader;

    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->matrix:Landroid/graphics/Matrix;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->shader:Landroid/graphics/Shader;

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->matrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 49
    :cond_0
    return-void
.end method

.method public writeBody(Ljava/io/DataOutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x1:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 38
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y1:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 39
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->x2:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 40
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprLinearGradient;->y2:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 41
    return-void
.end method
