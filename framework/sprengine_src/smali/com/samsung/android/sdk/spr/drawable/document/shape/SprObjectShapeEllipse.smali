.class public Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;
.super Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
.source "SprObjectShapeEllipse.java"


# instance fields
.field public bottom:F

.field public left:F

.field private final mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

.field public right:F

.field public top:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;-><init>(B)V

    .line 9
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->left:F

    .line 10
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->top:F

    .line 11
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->right:F

    .line 12
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->bottom:F

    .line 18
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    .line 19
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V
    .locals 2
    .param p1, "in"    # Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 22
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;-><init>(B)V

    .line 9
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->left:F

    .line 10
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->top:F

    .line 11
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->right:F

    .line 12
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->bottom:F

    .line 23
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    .line 24
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 25
    return-void
.end method


# virtual methods
.method public fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->left:F

    .line 52
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->top:F

    .line 53
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->right:F

    .line 54
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->bottom:F

    .line 56
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 57
    return-void
.end method

.method public getSPRSize()I
    .locals 1

    .prologue
    .line 71
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->getSPRSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x10

    return v0
.end method

.method public getTotalElementCount()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x1

    return v0
.end method

.method public getTotalSegmentCount()I
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x4

    return v0
.end method

.method public setDisplayLayout(ZFFFFFFFFF)V
    .locals 6
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "npLeft"    # F
    .param p4, "npTop"    # F
    .param p5, "npRight"    # F
    .param p6, "npBottom"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "displayWidth"    # F
    .param p10, "displayHeight"    # F

    .prologue
    .line 30
    invoke-super/range {p0 .. p10}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->setDisplayLayout(ZFFFFFFFFF)V

    .line 33
    if-eqz p1, :cond_0

    .line 34
    div-float/2addr p9, p2

    .line 35
    div-float p10, p10, p2

    .line 37
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    iget v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->left:F

    move-object v0, p0

    move v2, p3

    move v3, p5

    move v4, p7

    move v5, p9

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->left:F

    .line 38
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    iget v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->top:F

    move-object v0, p0

    move v2, p4

    move v3, p6

    move v4, p8

    move/from16 v5, p10

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->top:F

    .line 39
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    iget v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->right:F

    move-object v0, p0

    move v2, p3

    move v3, p5

    move v4, p7

    move v5, p9

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->right:F

    .line 40
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    iget v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->bottom:F

    move-object v0, p0

    move v2, p4

    move v3, p6

    move v4, p8

    move/from16 v5, p10

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->bottom:F

    .line 47
    :goto_0
    return-void

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->left:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->left:F

    .line 43
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->top:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->top:F

    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->right:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->right:F

    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->bottom:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->bottom:F

    goto :goto_0
.end method

.method public toSPR(Ljava/io/DataOutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->left:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 62
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->top:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 63
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->right:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 64
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeEllipse;->bottom:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 66
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->toSPR(Ljava/io/DataOutputStream;)V

    .line 67
    return-void
.end method
