.class public abstract Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;
.super Ljava/lang/Object;
.source "SprGradient.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final SPREAD_TYPE_NONE:B = 0x0t

.field public static final SPREAD_TYPE_PAD:B = 0x1t

.field public static final SPREAD_TYPE_REFLECT:B = 0x2t

.field public static final SPREAD_TYPE_REPEAT:B = 0x3t

.field static final sTileModeArray:[Landroid/graphics/Shader$TileMode;


# instance fields
.field public colors:[I

.field protected final mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

.field public matrix:Landroid/graphics/Matrix;

.field public positions:[F

.field public shader:Landroid/graphics/Shader;

.field public spreadMode:B


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    const/4 v0, 0x3

    new-array v0, v0, [Landroid/graphics/Shader$TileMode;

    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->sTileModeArray:[Landroid/graphics/Shader$TileMode;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-byte v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->spreadMode:B

    .line 24
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->matrix:Landroid/graphics/Matrix;

    .line 25
    iput-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->shader:Landroid/graphics/Shader;

    .line 30
    iput-object p0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    .line 31
    return-void
.end method

.method public constructor <init>(Ljava/io/DataInputStream;)V
    .locals 8
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v5, 0x0

    iput-byte v5, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->spreadMode:B

    .line 24
    iput-object v7, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->matrix:Landroid/graphics/Matrix;

    .line 25
    iput-object v7, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->shader:Landroid/graphics/Shader;

    .line 34
    iput-object p0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    .line 35
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->readBody(Ljava/io/DataInputStream;)V

    .line 37
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readByte()B

    move-result v5

    iput-byte v5, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->spreadMode:B

    .line 39
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v5

    new-array v5, v5, [I

    iput-object v5, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->colors:[I

    .line 40
    iget-object v5, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->colors:[I

    array-length v5, v5

    new-array v5, v5, [F

    iput-object v5, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->positions:[F

    .line 42
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->colors:[I

    array-length v5, v5

    if-ge v1, v5, :cond_0

    .line 43
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readFloat()F

    move-result v2

    .line 44
    .local v2, "offset":F
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    .line 45
    .local v0, "color":I
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readFloat()F

    move-result v3

    .line 47
    .local v3, "opacity":F
    iget-object v5, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->colors:[I

    const/high16 v6, 0x437f0000    # 255.0f

    mul-float/2addr v6, v3

    float-to-int v6, v6

    shl-int/lit8 v6, v6, 0x18

    or-int/2addr v6, v0

    aput v6, v5, v1

    .line 48
    iget-object v5, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->positions:[F

    aput v2, v5, v1

    .line 42
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 51
    .end local v0    # "color":I
    .end local v2    # "offset":F
    .end local v3    # "opacity":F
    :cond_0
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readByte()B

    move-result v4

    .line 52
    .local v4, "useMatrix":B
    invoke-static {p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprMatrix;->fromSPR(Ljava/io/DataInputStream;)Landroid/graphics/Matrix;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->matrix:Landroid/graphics/Matrix;

    .line 53
    if-nez v4, :cond_1

    .line 54
    iput-object v7, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->matrix:Landroid/graphics/Matrix;

    .line 57
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->updateGradient()V

    .line 58
    return-void
.end method


# virtual methods
.method public clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 92
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    .line 94
    .local v0, "attribute":Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->colors:[I

    array-length v2, v2

    new-array v2, v2, [I

    iput-object v2, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->colors:[I

    .line 95
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->colors:[I

    array-length v2, v2

    new-array v2, v2, [F

    iput-object v2, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->positions:[F

    .line 97
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->colors:[I

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 98
    iget-object v2, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->colors:[I

    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->colors:[I

    aget v3, v3, v1

    aput v3, v2, v1

    .line 99
    iget-object v2, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->positions:[F

    iget-object v3, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->positions:[F

    aget v3, v3, v1

    aput v3, v2, v1

    .line 97
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 102
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->updateGradient()V

    .line 104
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->clone()Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    move-result-object v0

    return-object v0
.end method

.method public abstract readBody(Ljava/io/DataInputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public setDisplayLayout(ZFFFFFFFFF)V
    .locals 11
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "npLeft"    # F
    .param p4, "npTop"    # F
    .param p5, "npRight"    # F
    .param p6, "npBottom"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "displayWidth"    # F
    .param p10, "displayHeight"    # F

    .prologue
    .line 79
    invoke-virtual/range {p0 .. p10}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->setDisplayLayoutBody(ZFFFFFFFFF)V

    .line 82
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    iget-object v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->matrix:Landroid/graphics/Matrix;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    iget-object v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->matrix:Landroid/graphics/Matrix;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    move/from16 v10, p10

    invoke-static/range {v0 .. v10}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprMatrix;->setDisplayLayout(Landroid/graphics/Matrix;ZFFFFFFFFF)Landroid/graphics/Matrix;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->matrix:Landroid/graphics/Matrix;

    .line 87
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->updateGradient()V

    .line 88
    return-void
.end method

.method public abstract setDisplayLayoutBody(ZFFFFFFFFF)V
.end method

.method public toSPR(Ljava/io/DataOutputStream;)V
    .locals 3
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->writeBody(Ljava/io/DataOutputStream;)V

    .line 62
    iget-byte v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->spreadMode:B

    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 64
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->colors:[I

    array-length v1, v1

    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 66
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->colors:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->positions:[F

    aget v1, v1, v0

    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 68
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->colors:[I

    aget v1, v1, v0

    const v2, 0xffffff

    and-int/2addr v1, v2

    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 69
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->colors:[I

    aget v1, v1, v0

    shr-int/lit8 v1, v1, 0x18

    int-to-float v1, v1

    const/high16 v2, 0x437f0000    # 255.0f

    div-float/2addr v1, v2

    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->matrix:Landroid/graphics/Matrix;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p1, v1}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 73
    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;->matrix:Landroid/graphics/Matrix;

    invoke-static {p1, v1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprMatrix;->toSPR(Ljava/io/DataOutputStream;Landroid/graphics/Matrix;)V

    .line 74
    return-void

    .line 72
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public abstract updateGradient()V
.end method

.method public abstract writeBody(Ljava/io/DataOutputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
