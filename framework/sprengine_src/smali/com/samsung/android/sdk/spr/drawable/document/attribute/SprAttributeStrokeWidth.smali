.class public Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;
.super Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
.source "SprAttributeStrokeWidth.java"


# instance fields
.field public origStrokeWidth:F

.field public strokeWidth:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13
    const/16 v0, 0x28

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 9
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->origStrokeWidth:F

    .line 10
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->strokeWidth:F

    .line 14
    return-void
.end method

.method public constructor <init>(F)V
    .locals 2
    .param p1, "width"    # F

    .prologue
    const/4 v1, 0x0

    .line 17
    const/16 v0, 0x28

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 9
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->origStrokeWidth:F

    .line 10
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->strokeWidth:F

    .line 18
    iput p1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->strokeWidth:F

    iput p1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->origStrokeWidth:F

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/io/DataInputStream;)V
    .locals 2
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 22
    const/16 v0, 0x28

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 9
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->origStrokeWidth:F

    .line 10
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->strokeWidth:F

    .line 23
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->fromSPR(Ljava/io/DataInputStream;)V

    .line 24
    return-void
.end method


# virtual methods
.method public fromSPR(Ljava/io/DataInputStream;)V
    .locals 3
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const v2, 0x3e99999a    # 0.3f

    .line 28
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->origStrokeWidth:F

    .line 29
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->origStrokeWidth:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->strokeWidth:F

    .line 30
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->strokeWidth:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->strokeWidth:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    .line 31
    iput v2, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->strokeWidth:F

    .line 33
    :cond_0
    return-void
.end method

.method public getSPRSize()I
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->getSPRSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public toSPR(Ljava/io/DataOutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeWidth;->origStrokeWidth:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 38
    return-void
.end method
