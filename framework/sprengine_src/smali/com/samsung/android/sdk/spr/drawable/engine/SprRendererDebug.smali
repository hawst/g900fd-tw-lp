.class Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;
.super Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;
.source "SprRendererDebug.java"


# static fields
.field public static final DEBUG_HIGH:I = 0x3

.field public static final DEBUG_LOW:I = 0x1

.field public static final DEBUG_MID:I = 0x2

.field protected static mDebugLevel:Ljava/lang/Integer;


# instance fields
.field private mTextOutlinePaint:Landroid/graphics/Paint;

.field private mTextPaint:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mDebugLevel:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;)V
    .locals 4
    .param p1, "debugLevel"    # Ljava/lang/Integer;

    .prologue
    const/4 v3, 0x1

    const/high16 v2, 0x41a00000    # 20.0f

    .line 26
    invoke-direct {p0}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;-><init>()V

    .line 27
    sput-object p1, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mDebugLevel:Ljava/lang/Integer;

    .line 29
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mTextOutlinePaint:Landroid/graphics/Paint;

    .line 30
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mTextOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 31
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mTextOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 32
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mTextOutlinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 33
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mTextOutlinePaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 34
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mTextOutlinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40800000    # 4.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 36
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mTextPaint:Landroid/graphics/Paint;

    .line 37
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 38
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 39
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mTextPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 40
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mTextPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 41
    return-void
.end method

.method private debugInfo(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;II)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "document"    # Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    .param p3, "displayWidth"    # I
    .param p4, "displayHeight"    # I

    .prologue
    .line 96
    sget-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mDebugLevel:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 97
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->isNinePatch()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "N"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    rem-int/lit16 v1, v1, 0x2710

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x14

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;I)V

    .line 102
    iget-object v0, p2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mName:Ljava/lang/String;

    const/16 v1, 0x28

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;I)V

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x3c

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;I)V

    .line 105
    sget-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mDebugLevel:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->getLoadingTime()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms E:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->getTotalElementCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " S:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->getTotalSegmentCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " A:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->getTotalAttributeCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x50

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->drawText(Landroid/graphics/Canvas;Ljava/lang/String;I)V

    .line 110
    :cond_0
    return-void

    .line 97
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->isIntrinsic()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ""

    goto/16 :goto_0

    :cond_2
    const-string v0, "C"

    goto/16 :goto_0
.end method

.method private drawText(Landroid/graphics/Canvas;Ljava/lang/String;I)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "y"    # I

    .prologue
    const/high16 v2, 0x40a00000    # 5.0f

    .line 113
    int-to-float v0, p3

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mTextOutlinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v2, v0, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 114
    int-to-float v0, p3

    iget-object v1, p0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v2, v0, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 115
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;ZFII)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "document"    # Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    .param p3, "ninePatch"    # Z
    .param p4, "densityScale"    # F
    .param p5, "displayWidth"    # I
    .param p6, "displayHeight"    # I

    .prologue
    .line 46
    const-string v0, "SPRRenderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    rem-int/lit16 v2, v2, 0x2710

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ninepatch:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " width:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " height:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 49
    .local v5, "debugPaint":Landroid/graphics/Paint;
    const/4 v0, 0x1

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 50
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 51
    const/high16 v0, 0x40a00000    # 5.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 52
    const/high16 v0, -0x10000

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 53
    iget v1, p2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLeft:F

    iget v2, p2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mTop:F

    iget v0, p2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mLeft:F

    int-to-float v3, p5

    add-float/2addr v3, v0

    iget v0, p2, Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;->mTop:F

    int-to-float v4, p6

    add-float/2addr v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 56
    invoke-super/range {p0 .. p6}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;ZFII)V

    .line 58
    invoke-direct {p0, p1, p2, p5, p6}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->debugInfo(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;II)V

    .line 59
    return-void
.end method

.method public dumpPNG(Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;ZFII)V
    .locals 13
    .param p1, "document"    # Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;
    .param p2, "ninePatch"    # Z
    .param p3, "densityScale"    # F
    .param p4, "displayWidth"    # I
    .param p5, "displayHeight"    # I

    .prologue
    .line 76
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 77
    .local v9, "bm":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .local v3, "c":Landroid/graphics/Canvas;
    move-object v2, p0

    move-object v4, p1

    move v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    move/from16 v8, p5

    .line 78
    invoke-virtual/range {v2 .. v8}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->draw(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/spr/drawable/document/SprDocument;ZFII)V

    .line 81
    :try_start_0
    new-instance v10, Ljava/io/File;

    const-string v2, "/sdcard/spr_debug"

    invoke-direct {v10, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 82
    .local v10, "dir":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->mkdir()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v10}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 83
    :cond_0
    new-instance v12, Ljava/io/FileOutputStream;

    new-instance v2, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v5

    rem-int/lit16 v5, v5, 0x2710

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".png"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v10, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v12, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 84
    .local v12, "out":Ljava/io/OutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x5a

    invoke-virtual {v9, v2, v4, v12}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 85
    invoke-virtual {v12}, Ljava/io/OutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 93
    .end local v10    # "dir":Ljava/io/File;
    .end local v12    # "out":Ljava/io/OutputStream;
    :cond_1
    :goto_0
    return-void

    .line 87
    :catch_0
    move-exception v11

    .line 88
    .local v11, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v11}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 90
    .end local v11    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v11

    .line 91
    .local v11, "e":Ljava/io/IOException;
    invoke-virtual {v11}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method protected preDraw(Landroid/graphics/Paint;Landroid/graphics/Paint;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;ZZ)V
    .locals 4
    .param p1, "strokePaint"    # Landroid/graphics/Paint;
    .param p2, "fillPaint"    # Landroid/graphics/Paint;
    .param p3, "object"    # Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
    .param p4, "isVisibleStroke"    # Z
    .param p5, "isVisibleFill"    # Z

    .prologue
    const/16 v3, 0xff

    const/high16 v2, 0x40000000    # 2.0f

    .line 64
    invoke-super/range {p0 .. p5}, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererImpl;->preDraw(Landroid/graphics/Paint;Landroid/graphics/Paint;Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;ZZ)V

    .line 66
    sget-object v0, Lcom/samsung/android/sdk/spr/drawable/engine/SprRendererDebug;->mDebugLevel:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    iget-object v0, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->strokePaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->isVisibleStroke:Z

    .line 68
    iget-object v0, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->strokePaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-static {v3, v1, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 69
    iget-object v0, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->strokePaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    .line 70
    iget-object v0, p3, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->strokePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 73
    :cond_0
    return-void
.end method
