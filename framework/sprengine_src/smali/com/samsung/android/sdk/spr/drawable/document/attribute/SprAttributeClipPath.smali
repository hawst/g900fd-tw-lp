.class public Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClipPath;
.super Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
.source "SprAttributeClipPath.java"


# instance fields
.field public link:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 9
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClipPath;->link:I

    .line 13
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "l"    # I

    .prologue
    .line 16
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 9
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClipPath;->link:I

    .line 17
    iput p1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClipPath;->link:I

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/io/DataInputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 9
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClipPath;->link:I

    .line 22
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClipPath;->fromSPR(Ljava/io/DataInputStream;)V

    .line 23
    return-void
.end method


# virtual methods
.method public fromSPR(Ljava/io/DataInputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClipPath;->link:I

    .line 28
    return-void
.end method

.method public getSPRSize()I
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->getSPRSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    return v0
.end method

.method public toSPR(Ljava/io/DataOutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClipPath;->link:I

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 33
    return-void
.end method
