.class public Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;
.super Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
.source "SprAttributeStrokeLinejoin.java"


# static fields
.field public static STROKE_LINEJOIN_TYPE_BEVEL:B

.field public static STROKE_LINEJOIN_TYPE_MITER:B

.field public static STROKE_LINEJOIN_TYPE_NONE:B

.field public static STROKE_LINEJOIN_TYPE_ROUND:B


# instance fields
.field public linejoin:B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    sput-byte v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->STROKE_LINEJOIN_TYPE_NONE:B

    .line 11
    const/4 v0, 0x1

    sput-byte v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->STROKE_LINEJOIN_TYPE_MITER:B

    .line 13
    const/4 v0, 0x2

    sput-byte v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->STROKE_LINEJOIN_TYPE_ROUND:B

    .line 15
    const/4 v0, 0x3

    sput-byte v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->STROKE_LINEJOIN_TYPE_BEVEL:B

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    const/16 v0, 0x26

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 17
    sget-byte v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->STROKE_LINEJOIN_TYPE_MITER:B

    iput-byte v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->linejoin:B

    .line 21
    return-void
.end method

.method public constructor <init>(B)V
    .locals 1
    .param p1, "linejoin"    # B

    .prologue
    .line 24
    const/16 v0, 0x26

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 17
    sget-byte v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->STROKE_LINEJOIN_TYPE_MITER:B

    iput-byte v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->linejoin:B

    .line 25
    iput-byte p1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->linejoin:B

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/io/DataInputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    const/16 v0, 0x26

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 17
    sget-byte v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->STROKE_LINEJOIN_TYPE_MITER:B

    iput-byte v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->linejoin:B

    .line 30
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->fromSPR(Ljava/io/DataInputStream;)V

    .line 31
    return-void
.end method


# virtual methods
.method public fromSPR(Ljava/io/DataInputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readByte()B

    move-result v0

    iput-byte v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->linejoin:B

    .line 36
    return-void
.end method

.method public getSPRSize()I
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->getSPRSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public toSPR(Ljava/io/DataOutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    iget-byte v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeStrokeLinejoin;->linejoin:B

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 41
    return-void
.end method
