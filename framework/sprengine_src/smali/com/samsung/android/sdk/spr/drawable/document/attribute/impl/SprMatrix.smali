.class public Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprMatrix;
.super Ljava/lang/Object;
.source "SprMatrix.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromSPR(Ljava/io/DataInputStream;)Landroid/graphics/Matrix;
    .locals 10
    .param p0, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 11
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 12
    .local v6, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readFloat()F

    move-result v0

    .line 13
    .local v0, "A":F
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readFloat()F

    move-result v1

    .line 14
    .local v1, "B":F
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readFloat()F

    move-result v2

    .line 15
    .local v2, "C":F
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readFloat()F

    move-result v3

    .line 16
    .local v3, "D":F
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readFloat()F

    move-result v4

    .line 17
    .local v4, "E":F
    invoke-virtual {p0}, Ljava/io/DataInputStream;->readFloat()F

    move-result v5

    .line 18
    .local v5, "F":F
    const/16 v7, 0x9

    new-array v7, v7, [F

    const/4 v8, 0x0

    aput v0, v7, v8

    const/4 v8, 0x1

    aput v1, v7, v8

    const/4 v8, 0x2

    aput v2, v7, v8

    const/4 v8, 0x3

    aput v3, v7, v8

    const/4 v8, 0x4

    aput v4, v7, v8

    const/4 v8, 0x5

    aput v5, v7, v8

    const/4 v8, 0x6

    aput v9, v7, v8

    const/4 v8, 0x7

    aput v9, v7, v8

    const/16 v8, 0x8

    const/high16 v9, 0x3f800000    # 1.0f

    aput v9, v7, v8

    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->setValues([F)V

    .line 20
    return-object v6
.end method

.method public static setDisplayLayout(Landroid/graphics/Matrix;ZFFFFFFFFF)Landroid/graphics/Matrix;
    .locals 5
    .param p0, "intrinsic"    # Landroid/graphics/Matrix;
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "npLeft"    # F
    .param p4, "npTop"    # F
    .param p5, "npRight"    # F
    .param p6, "npBottom"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "displayWidth"    # F
    .param p10, "displayHeight"    # F

    .prologue
    .line 46
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1, p0}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 48
    .local v1, "matrix":Landroid/graphics/Matrix;
    if-eqz p1, :cond_0

    .line 49
    div-float/2addr p9, p2

    .line 50
    div-float/2addr p10, p2

    .line 52
    const/16 v3, 0x9

    new-array v0, v3, [F

    .line 53
    .local v0, "intrinsicValues":[F
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 55
    const/16 v3, 0x9

    new-array v2, v3, [F

    .line 56
    .local v2, "values":[F
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->getValues([F)V

    .line 58
    const/4 v3, 0x2

    const/4 v4, 0x2

    aget v4, v0, v4

    invoke-static {v4, p3, p5, p7, p9}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->adjustNinePatchPosition(FFFFF)F

    move-result v4

    aput v4, v2, v3

    .line 60
    const/4 v3, 0x5

    const/4 v4, 0x5

    aget v4, v0, v4

    invoke-static {v4, p4, p6, p8, p10}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->adjustNinePatchPosition(FFFFF)F

    move-result v4

    aput v4, v2, v3

    .line 63
    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->setValues([F)V

    .line 66
    .end local v0    # "intrinsicValues":[F
    .end local v2    # "values":[F
    :cond_0
    return-object v1
.end method

.method public static toSPR(Ljava/io/DataOutputStream;Landroid/graphics/Matrix;)V
    .locals 2
    .param p0, "out"    # Ljava/io/DataOutputStream;
    .param p1, "matrix"    # Landroid/graphics/Matrix;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 24
    if-nez p1, :cond_0

    .line 25
    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 26
    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 27
    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 28
    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 29
    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 30
    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 41
    :goto_0
    return-void

    .line 32
    :cond_0
    const/16 v1, 0x9

    new-array v0, v1, [F

    .line 33
    .local v0, "values":[F
    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 34
    const/4 v1, 0x0

    aget v1, v0, v1

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 35
    const/4 v1, 0x1

    aget v1, v0, v1

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 36
    const/4 v1, 0x2

    aget v1, v0, v1

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 37
    const/4 v1, 0x3

    aget v1, v0, v1

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 38
    const/4 v1, 0x4

    aget v1, v0, v1

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 39
    const/4 v1, 0x5

    aget v1, v0, v1

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    goto :goto_0
.end method
