.class public Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;
.super Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
.source "SprObjectShapeCircle.java"


# instance fields
.field public cr:F

.field public cx:F

.field public cy:F

.field private final mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 16
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;-><init>(B)V

    .line 9
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cx:F

    .line 10
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cy:F

    .line 11
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cr:F

    .line 17
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V
    .locals 2
    .param p1, "in"    # Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 21
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;-><init>(B)V

    .line 9
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cx:F

    .line 10
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cy:F

    .line 11
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cr:F

    .line 22
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;

    .line 23
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 24
    return-void
.end method


# virtual methods
.method public fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cx:F

    .line 49
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cy:F

    .line 50
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cr:F

    .line 52
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 53
    return-void
.end method

.method public getSPRSize()I
    .locals 1

    .prologue
    .line 66
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->getSPRSize()I

    move-result v0

    add-int/lit8 v0, v0, 0xc

    return v0
.end method

.method public getTotalElementCount()I
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    return v0
.end method

.method public getTotalSegmentCount()I
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x4

    return v0
.end method

.method public setDisplayLayout(ZFFFFFFFFF)V
    .locals 6
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "npLeft"    # F
    .param p4, "npTop"    # F
    .param p5, "npRight"    # F
    .param p6, "npBottom"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "displayWidth"    # F
    .param p10, "displayHeight"    # F

    .prologue
    .line 29
    invoke-super/range {p0 .. p10}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->setDisplayLayout(ZFFFFFFFFF)V

    .line 32
    if-eqz p1, :cond_0

    .line 33
    div-float/2addr p9, p2

    .line 34
    div-float p10, p10, p2

    .line 36
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;

    iget v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cx:F

    move-object v0, p0

    move v2, p3

    move v3, p5

    move v4, p7

    move v5, p9

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cx:F

    .line 37
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;

    iget v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cy:F

    move-object v0, p0

    move v2, p4

    move v3, p6

    move v4, p8

    move/from16 v5, p10

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cy:F

    .line 38
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cr:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cr:F

    .line 44
    :goto_0
    return-void

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cx:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cx:F

    .line 41
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cy:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cy:F

    .line 42
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cr:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cr:F

    goto :goto_0
.end method

.method public toSPR(Ljava/io/DataOutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cx:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 58
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cy:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 59
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeCircle;->cr:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 61
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->toSPR(Ljava/io/DataOutputStream;)V

    .line 62
    return-void
.end method
