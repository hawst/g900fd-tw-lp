.class public Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeFill;
.super Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;
.source "SprAttributeFill.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;-><init>(B)V

    .line 10
    return-void
.end method

.method public constructor <init>(BI)V
    .locals 1
    .param p1, "type"    # B
    .param p2, "value"    # I

    .prologue
    .line 13
    const/16 v0, 0x20

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;-><init>(BBI)V

    .line 14
    return-void
.end method

.method public constructor <init>(BLcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;)V
    .locals 1
    .param p1, "type"    # B
    .param p2, "gradient"    # Lcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;

    .prologue
    .line 17
    const/16 v0, 0x20

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;-><init>(BBLcom/samsung/android/sdk/spr/drawable/document/attribute/impl/SprGradient;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Ljava/io/DataInputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    const/16 v0, 0x20

    invoke-direct {p0, v0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeColor;-><init>(BLjava/io/DataInputStream;)V

    .line 22
    return-void
.end method
