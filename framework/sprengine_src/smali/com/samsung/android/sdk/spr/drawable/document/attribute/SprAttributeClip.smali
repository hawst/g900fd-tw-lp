.class public Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;
.super Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;
.source "SprAttributeClip.java"


# instance fields
.field public bottom:F

.field public left:F

.field private final mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;

.field public right:F

.field public top:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 9
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->left:F

    .line 10
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->top:F

    .line 11
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->right:F

    .line 12
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->bottom:F

    .line 18
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/io/DataInputStream;)V
    .locals 2
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 22
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;-><init>(B)V

    .line 9
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->left:F

    .line 10
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->top:F

    .line 11
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->right:F

    .line 12
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->bottom:F

    .line 23
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;

    .line 24
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->fromSPR(Ljava/io/DataInputStream;)V

    .line 25
    return-void
.end method


# virtual methods
.method public fromSPR(Ljava/io/DataInputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/DataInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->left:F

    .line 30
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->top:F

    .line 31
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->right:F

    .line 32
    invoke-virtual {p1}, Ljava/io/DataInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->bottom:F

    .line 33
    return-void
.end method

.method public getSPRSize()I
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeBase;->getSPRSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x10

    return v0
.end method

.method public setDisplayLayout(ZFFFFFFFFF)V
    .locals 1
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "npLeft"    # F
    .param p4, "npTop"    # F
    .param p5, "npRight"    # F
    .param p6, "npBottom"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "displayWidth"    # F
    .param p10, "displayHeight"    # F

    .prologue
    .line 51
    if-eqz p1, :cond_0

    .line 52
    div-float/2addr p9, p2

    .line 53
    div-float/2addr p10, p2

    .line 55
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->left:F

    invoke-static {v0, p3, p5, p7, p9}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->left:F

    .line 56
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->top:F

    invoke-static {v0, p4, p6, p8, p10}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->top:F

    .line 57
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->right:F

    invoke-static {v0, p3, p5, p7, p9}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->right:F

    .line 58
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->bottom:F

    invoke-static {v0, p4, p6, p8, p10}, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->bottom:F

    .line 66
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->left:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->left:F

    .line 62
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->top:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->top:F

    .line 63
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->right:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->right:F

    .line 64
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->bottom:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->bottom:F

    goto :goto_0
.end method

.method public toSPR(Ljava/io/DataOutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->left:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 38
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->top:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 39
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->right:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 40
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/attribute/SprAttributeClip;->bottom:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 41
    return-void
.end method
