.class public Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;
.super Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;
.source "SprObjectShapeLine.java"


# instance fields
.field private final mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

.field public x1:F

.field public x2:F

.field public y1:F

.field public y2:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;-><init>(B)V

    .line 10
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x1:F

    .line 11
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x2:F

    .line 12
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y1:F

    .line 13
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y2:F

    .line 19
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V
    .locals 2
    .param p1, "in"    # Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 23
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;-><init>(B)V

    .line 10
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x1:F

    .line 11
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x2:F

    .line 12
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y1:F

    .line 13
    iput v1, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y2:F

    .line 24
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;

    check-cast v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    iput-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    .line 25
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 26
    return-void
.end method


# virtual methods
.method public fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x1:F

    .line 53
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y1:F

    .line 54
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x2:F

    .line 55
    invoke-virtual {p1}, Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y2:F

    .line 57
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->fromSPR(Lcom/samsung/android/sdk/spr/drawable/document/SprInputStream;)V

    .line 58
    return-void
.end method

.method public getSPRSize()I
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->getSPRSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x10

    return v0
.end method

.method public getTotalElementCount()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    return v0
.end method

.method public getTotalSegmentCount()I
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x2

    return v0
.end method

.method public setDisplayLayout(ZFFFFFFFFF)V
    .locals 6
    .param p1, "ninePatch"    # Z
    .param p2, "densityScale"    # F
    .param p3, "npLeft"    # F
    .param p4, "npTop"    # F
    .param p5, "npRight"    # F
    .param p6, "npBottom"    # F
    .param p7, "width"    # F
    .param p8, "height"    # F
    .param p9, "displayWidth"    # F
    .param p10, "displayHeight"    # F

    .prologue
    .line 31
    invoke-super/range {p0 .. p10}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->setDisplayLayout(ZFFFFFFFFF)V

    .line 34
    if-eqz p1, :cond_0

    .line 35
    div-float/2addr p9, p2

    .line 36
    div-float p10, p10, p2

    .line 38
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    iget v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x1:F

    move-object v0, p0

    move v2, p3

    move v3, p5

    move v4, p7

    move v5, p9

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x1:F

    .line 39
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    iget v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y1:F

    move-object v0, p0

    move v2, p4

    move v3, p6

    move v4, p8

    move/from16 v5, p10

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y1:F

    .line 40
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    iget v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x2:F

    move-object v0, p0

    move v2, p3

    move v3, p5

    move v4, p7

    move v5, p9

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x2:F

    .line 41
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    iget v1, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y2:F

    move-object v0, p0

    move v2, p4

    move v3, p6

    move v4, p8

    move/from16 v5, p10

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->adjustNinePatchPosition(FFFFF)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y2:F

    .line 48
    :goto_0
    return-void

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x1:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x1:F

    .line 44
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y1:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y1:F

    .line 45
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x2:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x2:F

    .line 46
    iget-object v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->mIntrinsic:Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;

    iget v0, v0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y2:F

    iput v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y2:F

    goto :goto_0
.end method

.method public toSPR(Ljava/io/DataOutputStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/DataOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x1:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 63
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y1:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 64
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->x2:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 65
    iget v0, p0, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectShapeLine;->y2:F

    invoke-virtual {p1, v0}, Ljava/io/DataOutputStream;->writeFloat(F)V

    .line 67
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/spr/drawable/document/shape/SprObjectBase;->toSPR(Ljava/io/DataOutputStream;)V

    .line 68
    return-void
.end method
