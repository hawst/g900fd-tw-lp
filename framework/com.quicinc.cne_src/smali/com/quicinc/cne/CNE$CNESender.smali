.class public Lcom/quicinc/cne/CNE$CNESender;
.super Landroid/os/Handler;
.source "CNE.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quicinc/cne/CNE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CNESender"
.end annotation


# static fields
.field private static final SUB_TYPE:Ljava/lang/String; = "CORE:COM:SNDR"


# instance fields
.field dataLength:[B

.field final synthetic this$0:Lcom/quicinc/cne/CNE;


# direct methods
.method public constructor <init>(Lcom/quicinc/cne/CNE;Landroid/os/Looper;)V
    .locals 1
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 837
    iput-object p1, p0, Lcom/quicinc/cne/CNE$CNESender;->this$0:Lcom/quicinc/cne/CNE;

    .line 838
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 842
    const/4 v0, 0x4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/quicinc/cne/CNE$CNESender;->dataLength:[B

    .line 839
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 851
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Lcom/quicinc/cne/CNERequest;

    move-object v5, v7

    check-cast v5, Lcom/quicinc/cne/CNERequest;

    .line 852
    .local v5, "rr":Lcom/quicinc/cne/CNERequest;
    const/4 v4, 0x0

    .line 854
    .local v4, "req":Lcom/quicinc/cne/CNERequest;
    iget v7, p1, Landroid/os/Message;->what:I

    packed-switch v7, :pswitch_data_0

    .line 927
    :cond_0
    :goto_0
    return-void

    .line 862
    :pswitch_0
    const/4 v0, 0x0

    .line 865
    .local v0, "alreadySubtracted":Z
    :try_start_0
    iget-object v7, p0, Lcom/quicinc/cne/CNE$CNESender;->this$0:Lcom/quicinc/cne/CNE;

    iget-object v6, v7, Lcom/quicinc/cne/CNE;->mSocket:Landroid/net/LocalSocket;

    .line 866
    .local v6, "s":Landroid/net/LocalSocket;
    if-nez v6, :cond_1

    .line 867
    invoke-virtual {v5}, Lcom/quicinc/cne/CNERequest;->release()V

    .line 868
    iget-object v7, p0, Lcom/quicinc/cne/CNE$CNESender;->this$0:Lcom/quicinc/cne/CNE;

    iget v8, v7, Lcom/quicinc/cne/CNE;->mRequestMessagesPending:I

    add-int/lit8 v8, v8, -0x1

    iput v8, v7, Lcom/quicinc/cne/CNE;->mRequestMessagesPending:I

    .line 869
    const/4 v0, 0x1

    goto :goto_0

    .line 872
    :cond_1
    iget-object v7, p0, Lcom/quicinc/cne/CNE$CNESender;->this$0:Lcom/quicinc/cne/CNE;

    iget-object v8, v7, Lcom/quicinc/cne/CNE;->mRequestsList:Ljava/util/ArrayList;

    monitor-enter v8
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 873
    :try_start_1
    iget-object v7, p0, Lcom/quicinc/cne/CNE$CNESender;->this$0:Lcom/quicinc/cne/CNE;

    iget-object v7, v7, Lcom/quicinc/cne/CNE;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 874
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 875
    :try_start_2
    iget-object v7, p0, Lcom/quicinc/cne/CNE$CNESender;->this$0:Lcom/quicinc/cne/CNE;

    iget v8, v7, Lcom/quicinc/cne/CNE;->mRequestMessagesPending:I

    add-int/lit8 v8, v8, -0x1

    iput v8, v7, Lcom/quicinc/cne/CNE;->mRequestMessagesPending:I

    .line 876
    const/4 v0, 0x1

    .line 878
    iget-object v7, v5, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v7}, Landroid/os/Parcel;->marshall()[B

    move-result-object v1

    .line 879
    .local v1, "data":[B
    iget-object v7, v5, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v7}, Landroid/os/Parcel;->recycle()V

    .line 880
    const/4 v7, 0x0

    iput-object v7, v5, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    .line 881
    array-length v7, v1

    const/16 v8, 0x2000

    if-le v7, v8, :cond_5

    .line 882
    new-instance v7, Ljava/lang/RuntimeException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Parcel larger than max bytes allowed! "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v9, v1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    .line 895
    .end local v1    # "data":[B
    .end local v6    # "s":Landroid/net/LocalSocket;
    :catch_0
    move-exception v2

    .line 896
    .local v2, "ex":Ljava/io/IOException;
    const-string v7, "CORE:COM:SNDR"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "IOException "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v7, v8}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    iget-object v7, p0, Lcom/quicinc/cne/CNE$CNESender;->this$0:Lcom/quicinc/cne/CNE;

    iget v8, v5, Lcom/quicinc/cne/CNERequest;->mSerial:I

    # invokes: Lcom/quicinc/cne/CNE;->findAndRemoveRequestFromList(I)Lcom/quicinc/cne/CNERequest;
    invoke-static {v7, v8}, Lcom/quicinc/cne/CNE;->access$2500(Lcom/quicinc/cne/CNE;I)Lcom/quicinc/cne/CNERequest;

    move-result-object v4

    .line 900
    if-nez v4, :cond_2

    if-nez v0, :cond_3

    .line 901
    :cond_2
    invoke-virtual {v5}, Lcom/quicinc/cne/CNERequest;->release()V

    .line 914
    .end local v2    # "ex":Ljava/io/IOException;
    :cond_3
    :goto_1
    if-nez v0, :cond_0

    .line 915
    iget-object v7, p0, Lcom/quicinc/cne/CNE$CNESender;->this$0:Lcom/quicinc/cne/CNE;

    iget v8, v7, Lcom/quicinc/cne/CNE;->mRequestMessagesPending:I

    add-int/lit8 v8, v8, -0x1

    iput v8, v7, Lcom/quicinc/cne/CNE;->mRequestMessagesPending:I

    goto/16 :goto_0

    .line 874
    .restart local v6    # "s":Landroid/net/LocalSocket;
    :catchall_0
    move-exception v7

    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v7
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1

    .line 903
    .end local v6    # "s":Landroid/net/LocalSocket;
    :catch_1
    move-exception v3

    .line 904
    .local v3, "exc":Ljava/lang/RuntimeException;
    const-string v7, "CORE:COM:SNDR"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Uncaught exception "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v7, v8}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    .line 905
    iget-object v7, p0, Lcom/quicinc/cne/CNE$CNESender;->this$0:Lcom/quicinc/cne/CNE;

    iget v8, v5, Lcom/quicinc/cne/CNERequest;->mSerial:I

    # invokes: Lcom/quicinc/cne/CNE;->findAndRemoveRequestFromList(I)Lcom/quicinc/cne/CNERequest;
    invoke-static {v7, v8}, Lcom/quicinc/cne/CNE;->access$2500(Lcom/quicinc/cne/CNE;I)Lcom/quicinc/cne/CNERequest;

    move-result-object v4

    .line 908
    if-nez v4, :cond_4

    if-nez v0, :cond_3

    .line 910
    :cond_4
    invoke-virtual {v5}, Lcom/quicinc/cne/CNERequest;->release()V

    goto :goto_1

    .line 887
    .end local v3    # "exc":Ljava/lang/RuntimeException;
    .restart local v1    # "data":[B
    .restart local v6    # "s":Landroid/net/LocalSocket;
    :cond_5
    :try_start_5
    iget-object v7, p0, Lcom/quicinc/cne/CNE$CNESender;->dataLength:[B

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/quicinc/cne/CNE$CNESender;->dataLength:[B

    const/4 v10, 0x1

    const/4 v11, 0x0

    aput-byte v11, v9, v10

    aput-byte v11, v7, v8

    .line 888
    iget-object v7, p0, Lcom/quicinc/cne/CNE$CNESender;->dataLength:[B

    const/4 v8, 0x2

    array-length v9, v1

    shr-int/lit8 v9, v9, 0x8

    and-int/lit16 v9, v9, 0xff

    int-to-byte v9, v9

    aput-byte v9, v7, v8

    .line 889
    iget-object v7, p0, Lcom/quicinc/cne/CNE$CNESender;->dataLength:[B

    const/4 v8, 0x3

    array-length v9, v1

    and-int/lit16 v9, v9, 0xff

    int-to-byte v9, v9

    aput-byte v9, v7, v8

    .line 891
    invoke-virtual {v6}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    iget-object v8, p0, Lcom/quicinc/cne/CNE$CNESender;->dataLength:[B

    invoke-virtual {v7, v8}, Ljava/io/OutputStream;->write([B)V

    .line 892
    invoke-virtual {v6}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/io/OutputStream;->write([B)V

    .line 894
    iget-object v7, p0, Lcom/quicinc/cne/CNE$CNESender;->this$0:Lcom/quicinc/cne/CNE;

    iget v8, v5, Lcom/quicinc/cne/CNERequest;->mSerial:I

    # invokes: Lcom/quicinc/cne/CNE;->findAndRemoveRequestFromList(I)Lcom/quicinc/cne/CNERequest;
    invoke-static {v7, v8}, Lcom/quicinc/cne/CNE;->access$2500(Lcom/quicinc/cne/CNE;I)Lcom/quicinc/cne/CNERequest;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 919
    .end local v0    # "alreadySubtracted":Z
    .end local v1    # "data":[B
    .end local v6    # "s":Landroid/net/LocalSocket;
    :pswitch_1
    const-string v7, "CORE:COM:SNDR"

    const-string v8, "event_toggle_wifi ...reassociating"

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v7, v8}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    .line 920
    iget-object v7, p0, Lcom/quicinc/cne/CNE$CNESender;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v7}, Lcom/quicinc/cne/CNE;->access$200(Lcom/quicinc/cne/CNE;)Landroid/net/wifi/WifiManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->disconnect()Z

    .line 921
    iget-object v7, p0, Lcom/quicinc/cne/CNE$CNESender;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v7}, Lcom/quicinc/cne/CNE;->access$200(Lcom/quicinc/cne/CNE;)Landroid/net/wifi/WifiManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->reassociate()Z

    goto/16 :goto_0

    .line 924
    :pswitch_2
    iget-object v7, p0, Lcom/quicinc/cne/CNE$CNESender;->this$0:Lcom/quicinc/cne/CNE;

    # invokes: Lcom/quicinc/cne/CNE;->postCndUpInit()V
    invoke-static {v7}, Lcom/quicinc/cne/CNE;->access$2600(Lcom/quicinc/cne/CNE;)V

    goto/16 :goto_0

    .line 854
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public run()V
    .locals 0

    .prologue
    .line 847
    return-void
.end method
