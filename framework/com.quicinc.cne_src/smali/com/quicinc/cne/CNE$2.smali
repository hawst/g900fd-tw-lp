.class Lcom/quicinc/cne/CNE$2;
.super Landroid/os/HandlerThread;
.source "CNE.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/quicinc/cne/CNE;->registerCNENetworkFactory()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/quicinc/cne/CNE;


# direct methods
.method constructor <init>(Lcom/quicinc/cne/CNE;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;

    .prologue
    .line 1337
    iput-object p1, p0, Lcom/quicinc/cne/CNE$2;->this$0:Lcom/quicinc/cne/CNE;

    invoke-direct {p0, p2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onLooperPrepared()V
    .locals 6

    .prologue
    .line 1340
    new-instance v0, Landroid/net/NetworkCapabilities;

    invoke-direct {v0}, Landroid/net/NetworkCapabilities;-><init>()V

    .line 1341
    .local v0, "netcap":Landroid/net/NetworkCapabilities;
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/net/NetworkCapabilities;->addCapability(I)Landroid/net/NetworkCapabilities;

    .line 1342
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/NetworkCapabilities;->addTransportType(I)Landroid/net/NetworkCapabilities;

    .line 1343
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/NetworkCapabilities;->addTransportType(I)Landroid/net/NetworkCapabilities;

    .line 1345
    iget-object v1, p0, Lcom/quicinc/cne/CNE$2;->this$0:Lcom/quicinc/cne/CNE;

    new-instance v2, Lcom/quicinc/cne/CNENetworkFactory;

    invoke-virtual {p0}, Lcom/quicinc/cne/CNE$2;->getLooper()Landroid/os/Looper;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/CNE$2;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/quicinc/cne/CNE;->access$3400(Lcom/quicinc/cne/CNE;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "CORE"

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/quicinc/cne/CNENetworkFactory;-><init>(Landroid/os/Looper;Landroid/content/Context;Ljava/lang/String;Landroid/net/NetworkCapabilities;)V

    # setter for: Lcom/quicinc/cne/CNE;->mNetworkFactory:Landroid/net/NetworkFactory;
    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->access$3302(Lcom/quicinc/cne/CNE;Landroid/net/NetworkFactory;)Landroid/net/NetworkFactory;

    .line 1346
    iget-object v1, p0, Lcom/quicinc/cne/CNE$2;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->mNetworkFactory:Landroid/net/NetworkFactory;
    invoke-static {v1}, Lcom/quicinc/cne/CNE;->access$3300(Lcom/quicinc/cne/CNE;)Landroid/net/NetworkFactory;

    move-result-object v1

    const/16 v2, 0x270f

    invoke-virtual {v1, v2}, Landroid/net/NetworkFactory;->setScoreFilter(I)V

    .line 1348
    iget-object v1, p0, Lcom/quicinc/cne/CNE$2;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->mNetworkFactory:Landroid/net/NetworkFactory;
    invoke-static {v1}, Lcom/quicinc/cne/CNE;->access$3300(Lcom/quicinc/cne/CNE;)Landroid/net/NetworkFactory;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkFactory;->register()V

    .line 1349
    iget-object v1, p0, Lcom/quicinc/cne/CNE$2;->this$0:Lcom/quicinc/cne/CNE;

    new-instance v2, Lcom/quicinc/cne/CNE$CNENetworkFactoryHandler;

    iget-object v3, p0, Lcom/quicinc/cne/CNE$2;->this$0:Lcom/quicinc/cne/CNE;

    invoke-virtual {p0}, Lcom/quicinc/cne/CNE$2;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/quicinc/cne/CNE$CNENetworkFactoryHandler;-><init>(Lcom/quicinc/cne/CNE;Landroid/os/Looper;)V

    # setter for: Lcom/quicinc/cne/CNE;->mFactoryHandler:Landroid/os/Handler;
    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->access$3502(Lcom/quicinc/cne/CNE;Landroid/os/Handler;)Landroid/os/Handler;

    .line 1350
    return-void
.end method
