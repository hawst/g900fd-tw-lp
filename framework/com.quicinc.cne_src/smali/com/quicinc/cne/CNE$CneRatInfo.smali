.class Lcom/quicinc/cne/CNE$CneRatInfo;
.super Ljava/lang/Object;
.source "CNE.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quicinc/cne/CNE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CneRatInfo"
.end annotation


# instance fields
.field ifNameV4:Ljava/lang/String;

.field ifNameV6:Ljava/lang/String;

.field ipAddrV4:Ljava/lang/String;

.field ipAddrV6:Ljava/lang/String;

.field networkState:I

.field networkType:I

.field subType:I

.field final synthetic this$0:Lcom/quicinc/cne/CNE;

.field timeStamp:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/quicinc/cne/CNE;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 459
    iput-object p1, p0, Lcom/quicinc/cne/CNE$CneRatInfo;->this$0:Lcom/quicinc/cne/CNE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 460
    const-string v0, ""

    iput-object v0, p0, Lcom/quicinc/cne/CNE$CneRatInfo;->ipAddrV4:Ljava/lang/String;

    .line 461
    const-string v0, ""

    iput-object v0, p0, Lcom/quicinc/cne/CNE$CneRatInfo;->ipAddrV6:Ljava/lang/String;

    .line 462
    const-string v0, ""

    iput-object v0, p0, Lcom/quicinc/cne/CNE$CneRatInfo;->ifNameV4:Ljava/lang/String;

    .line 463
    const-string v0, ""

    iput-object v0, p0, Lcom/quicinc/cne/CNE$CneRatInfo;->ifNameV6:Ljava/lang/String;

    .line 464
    iput v1, p0, Lcom/quicinc/cne/CNE$CneRatInfo;->networkType:I

    .line 465
    sget-object v0, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    # invokes: Lcom/quicinc/cne/CNE;->NetworkStateToInt(Landroid/net/NetworkInfo$State;)I
    invoke-static {p1, v0}, Lcom/quicinc/cne/CNE;->access$000(Lcom/quicinc/cne/CNE;Landroid/net/NetworkInfo$State;)I

    move-result v0

    iput v0, p0, Lcom/quicinc/cne/CNE$CneRatInfo;->networkState:I

    .line 466
    iput v1, p0, Lcom/quicinc/cne/CNE$CneRatInfo;->subType:I

    .line 467
    const-string v0, ""

    iput-object v0, p0, Lcom/quicinc/cne/CNE$CneRatInfo;->timeStamp:Ljava/lang/String;

    .line 468
    return-void
.end method


# virtual methods
.method public resetRatInfo()V
    .locals 1

    .prologue
    .line 471
    const-string v0, ""

    iput-object v0, p0, Lcom/quicinc/cne/CNE$CneRatInfo;->ipAddrV4:Ljava/lang/String;

    .line 472
    const-string v0, ""

    iput-object v0, p0, Lcom/quicinc/cne/CNE$CneRatInfo;->ipAddrV6:Ljava/lang/String;

    .line 473
    const-string v0, ""

    iput-object v0, p0, Lcom/quicinc/cne/CNE$CneRatInfo;->ifNameV4:Ljava/lang/String;

    .line 474
    const-string v0, ""

    iput-object v0, p0, Lcom/quicinc/cne/CNE$CneRatInfo;->ifNameV6:Ljava/lang/String;

    .line 475
    return-void
.end method

.method public setTimeStamp(Ljava/lang/String;)V
    .locals 0
    .param p1, "ts"    # Ljava/lang/String;

    .prologue
    .line 478
    iput-object p1, p0, Lcom/quicinc/cne/CNE$CneRatInfo;->timeStamp:Ljava/lang/String;

    .line 479
    return-void
.end method
