.class Lcom/quicinc/cne/CNE$CneWwanInfo;
.super Lcom/quicinc/cne/CNE$CneRatInfo;
.source "CNE.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quicinc/cne/CNE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CneWwanInfo"
.end annotation


# instance fields
.field mccMnc:Ljava/lang/String;

.field roaming:I

.field signalStrength:I

.field final synthetic this$0:Lcom/quicinc/cne/CNE;


# direct methods
.method public constructor <init>(Lcom/quicinc/cne/CNE;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 603
    iput-object p1, p0, Lcom/quicinc/cne/CNE$CneWwanInfo;->this$0:Lcom/quicinc/cne/CNE;

    .line 604
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE$CneRatInfo;-><init>(Lcom/quicinc/cne/CNE;)V

    .line 605
    iput v1, p0, Lcom/quicinc/cne/CNE$CneWwanInfo;->signalStrength:I

    .line 606
    iput v1, p0, Lcom/quicinc/cne/CNE$CneWwanInfo;->roaming:I

    .line 607
    const-string v0, ""

    iput-object v0, p0, Lcom/quicinc/cne/CNE$CneWwanInfo;->mccMnc:Ljava/lang/String;

    .line 608
    iput v1, p0, Lcom/quicinc/cne/CNE$CneWwanInfo;->networkType:I

    .line 609
    return-void
.end method

.method static synthetic access$2100(Lcom/quicinc/cne/CNE$CneWwanInfo;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE$CneWwanInfo;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 598
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE$CneWwanInfo;->setWwanInfo(Landroid/content/Intent;)V

    return-void
.end method

.method private resetWwanInfo()V
    .locals 1

    .prologue
    .line 612
    invoke-super {p0}, Lcom/quicinc/cne/CNE$CneRatInfo;->resetRatInfo()V

    .line 613
    const/4 v0, 0x0

    iput v0, p0, Lcom/quicinc/cne/CNE$CneWwanInfo;->signalStrength:I

    .line 614
    const-string v0, ""

    iput-object v0, p0, Lcom/quicinc/cne/CNE$CneWwanInfo;->mccMnc:Ljava/lang/String;

    .line 615
    return-void
.end method

.method private setWwanInfo(Landroid/content/Intent;)V
    .locals 17
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 618
    if-nez p1, :cond_1

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 622
    :cond_1
    :try_start_0
    const-string v14, "state"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 623
    .local v13, "state":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->this$0:Lcom/quicinc/cne/CNE;

    # invokes: Lcom/quicinc/cne/CNE;->NetworkStateStringToInt(Ljava/lang/String;)I
    invoke-static {v14, v13}, Lcom/quicinc/cne/CNE;->access$400(Lcom/quicinc/cne/CNE;Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->networkState:I

    .line 625
    move-object/from16 v0, p0

    iget v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->networkState:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->this$0:Lcom/quicinc/cne/CNE;

    const-string v16, "DISCONNECTED"

    # invokes: Lcom/quicinc/cne/CNE;->NetworkStateStringToInt(Ljava/lang/String;)I
    invoke-static/range {v15 .. v16}, Lcom/quicinc/cne/CNE;->access$400(Lcom/quicinc/cne/CNE;Ljava/lang/String;)I

    move-result v15

    if-eq v14, v15, :cond_2

    move-object/from16 v0, p0

    iget v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->networkState:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->this$0:Lcom/quicinc/cne/CNE;

    const-string v16, "UNKNOWN"

    # invokes: Lcom/quicinc/cne/CNE;->NetworkStateStringToInt(Ljava/lang/String;)I
    invoke-static/range {v15 .. v16}, Lcom/quicinc/cne/CNE;->access$400(Lcom/quicinc/cne/CNE;Ljava/lang/String;)I

    move-result v15

    if-ne v14, v15, :cond_3

    .line 628
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/quicinc/cne/CNE$CneWwanInfo;->resetWwanInfo()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 679
    .end local v13    # "state":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 680
    .local v10, "nexp":Ljava/lang/NullPointerException;
    const-string v14, "CORE"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "setWwanInfo(): Null Pointer Exception"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v14, v15}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 631
    .end local v10    # "nexp":Ljava/lang/NullPointerException;
    .restart local v13    # "state":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v14, "linkProperties"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v9

    check-cast v9, Landroid/net/LinkProperties;

    .line 633
    .local v9, "lp":Landroid/net/LinkProperties;
    if-eqz v9, :cond_a

    .line 634
    invoke-virtual {v9}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/List;

    move-result-object v8

    .line 635
    .local v8, "linkAddresses":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/LinkAddress;

    .line 636
    .local v7, "linkAddress":Landroid/net/LinkAddress;
    invoke-virtual {v7}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v1

    .line 637
    .local v1, "addr":Ljava/net/InetAddress;
    instance-of v14, v1, Ljava/net/Inet4Address;

    if-eqz v14, :cond_5

    .line 638
    invoke-virtual {v1}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v14

    if-nez v14, :cond_4

    invoke-virtual {v1}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v14

    if-nez v14, :cond_4

    .line 639
    invoke-virtual {v9}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->ifNameV4:Ljava/lang/String;

    .line 640
    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->ipAddrV4:Ljava/lang/String;

    goto :goto_1

    .line 643
    :cond_5
    instance-of v14, v1, Ljava/net/Inet6Address;

    if-eqz v14, :cond_4

    .line 644
    invoke-virtual {v1}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v14

    if-nez v14, :cond_6

    invoke-virtual {v1}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v14

    if-nez v14, :cond_6

    .line 645
    invoke-virtual {v9}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->ifNameV6:Ljava/lang/String;

    .line 646
    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->ipAddrV6:Ljava/lang/String;

    .line 648
    :cond_6
    invoke-virtual {v9}, Landroid/net/LinkProperties;->getStackedLinks()Ljava/util/List;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/LinkProperties;

    .line 649
    .local v6, "link":Landroid/net/LinkProperties;
    invoke-virtual {v6}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v14

    const-string v15, "clat4"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 650
    invoke-virtual {v6}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/List;

    move-result-object v12

    .line 652
    .local v12, "stackedLinkAddresses":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    invoke-interface {v12}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_8
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/net/LinkAddress;

    .line 653
    .local v11, "stackedLinkAddress":Landroid/net/LinkAddress;
    invoke-virtual {v11}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v2

    .line 654
    .local v2, "address":Ljava/net/InetAddress;
    instance-of v14, v2, Ljava/net/Inet4Address;

    if-eqz v14, :cond_8

    .line 655
    invoke-virtual {v2}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v14

    if-nez v14, :cond_8

    invoke-virtual {v2}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v14

    if-nez v14, :cond_8

    .line 657
    invoke-virtual {v6}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->ifNameV4:Ljava/lang/String;

    .line 658
    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->ipAddrV4:Ljava/lang/String;

    goto :goto_3

    .line 665
    .end local v2    # "address":Ljava/net/InetAddress;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v11    # "stackedLinkAddress":Landroid/net/LinkAddress;
    .end local v12    # "stackedLinkAddresses":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    :cond_9
    const-string v14, "CORE"

    const-string v15, "no clat4 interface present for ipv6 address"

    # invokes: Lcom/quicinc/cne/CNE;->logd(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v14, v15}, Lcom/quicinc/cne/CNE;->access$500(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 673
    .end local v1    # "addr":Ljava/net/InetAddress;
    .end local v6    # "link":Landroid/net/LinkProperties;
    .end local v7    # "linkAddress":Landroid/net/LinkAddress;
    .end local v8    # "linkAddresses":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->mTelephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {v14}, Lcom/quicinc/cne/CNE;->access$600(Lcom/quicinc/cne/CNE;)Landroid/telephony/TelephonyManager;

    move-result-object v14

    if-eqz v14, :cond_0

    .line 674
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->mTelephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {v14}, Lcom/quicinc/cne/CNE;->access$600(Lcom/quicinc/cne/CNE;)Landroid/telephony/TelephonyManager;

    move-result-object v14

    invoke-virtual {v14}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v14

    if-eqz v14, :cond_b

    const/4 v14, 0x1

    :goto_4
    move-object/from16 v0, p0

    iput v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->roaming:I

    .line 675
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v0, p0

    iget v15, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->networkType:I

    # invokes: Lcom/quicinc/cne/CNE;->getSignalStrength(I)I
    invoke-static {v14, v15}, Lcom/quicinc/cne/CNE;->access$700(Lcom/quicinc/cne/CNE;I)I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->signalStrength:I

    .line 676
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->mTelephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {v14}, Lcom/quicinc/cne/CNE;->access$600(Lcom/quicinc/cne/CNE;)Landroid/telephony/TelephonyManager;

    move-result-object v14

    invoke-virtual {v14}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->mccMnc:Ljava/lang/String;

    .line 677
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->mTelephonyManager:Landroid/telephony/TelephonyManager;
    invoke-static {v14}, Lcom/quicinc/cne/CNE;->access$600(Lcom/quicinc/cne/CNE;)Landroid/telephony/TelephonyManager;

    move-result-object v14

    invoke-virtual {v14}, Landroid/telephony/TelephonyManager;->getDataNetworkType()I

    move-result v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->subType:I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 674
    :cond_b
    const/4 v14, 0x0

    goto :goto_4
.end method
