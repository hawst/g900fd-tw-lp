.class Lcom/quicinc/cne/CNE$CneWifiInfo;
.super Lcom/quicinc/cne/CNE$CneRatInfo;
.source "CNE.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quicinc/cne/CNE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CneWifiInfo"
.end annotation


# instance fields
.field bssid:Ljava/lang/String;

.field dnsInfo:[Ljava/lang/String;

.field rssi:I

.field softApState:I

.field ssid:Ljava/lang/String;

.field final synthetic this$0:Lcom/quicinc/cne/CNE;


# direct methods
.method public constructor <init>(Lcom/quicinc/cne/CNE;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 490
    iput-object p1, p0, Lcom/quicinc/cne/CNE$CneWifiInfo;->this$0:Lcom/quicinc/cne/CNE;

    .line 491
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE$CneRatInfo;-><init>(Lcom/quicinc/cne/CNE;)V

    .line 492
    const/16 v1, 0xb

    iput v1, p0, Lcom/quicinc/cne/CNE$CneWifiInfo;->softApState:I

    .line 493
    const/4 v1, 0x0

    iput v1, p0, Lcom/quicinc/cne/CNE$CneWifiInfo;->rssi:I

    .line 494
    const-string v1, ""

    iput-object v1, p0, Lcom/quicinc/cne/CNE$CneWifiInfo;->ssid:Ljava/lang/String;

    .line 495
    const-string v1, "00:00:00:00:00:00"

    iput-object v1, p0, Lcom/quicinc/cne/CNE$CneWifiInfo;->bssid:Ljava/lang/String;

    .line 496
    new-array v1, v3, [Ljava/lang/String;

    iput-object v1, p0, Lcom/quicinc/cne/CNE$CneWifiInfo;->dnsInfo:[Ljava/lang/String;

    .line 497
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 498
    iget-object v1, p0, Lcom/quicinc/cne/CNE$CneWifiInfo;->dnsInfo:[Ljava/lang/String;

    const-string v2, "0.0.0.0"

    aput-object v2, v1, v0

    .line 497
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 500
    :cond_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/quicinc/cne/CNE$CneWifiInfo;->networkType:I

    .line 501
    return-void
.end method

.method static synthetic access$1700(Lcom/quicinc/cne/CNE$CneWifiInfo;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE$CneWifiInfo;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 484
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE$CneWifiInfo;->setWifiInfo(Landroid/content/Intent;)V

    return-void
.end method

.method private resetWifiInfo()V
    .locals 3

    .prologue
    .line 504
    invoke-super {p0}, Lcom/quicinc/cne/CNE$CneRatInfo;->resetRatInfo()V

    .line 505
    const/4 v1, 0x0

    iput v1, p0, Lcom/quicinc/cne/CNE$CneWifiInfo;->rssi:I

    .line 506
    const-string v1, ""

    iput-object v1, p0, Lcom/quicinc/cne/CNE$CneWifiInfo;->ssid:Ljava/lang/String;

    .line 507
    const-string v1, "00:00:00:00:00:00"

    iput-object v1, p0, Lcom/quicinc/cne/CNE$CneWifiInfo;->bssid:Ljava/lang/String;

    .line 508
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 509
    iget-object v1, p0, Lcom/quicinc/cne/CNE$CneWifiInfo;->dnsInfo:[Ljava/lang/String;

    const-string v2, "0.0.0.0"

    aput-object v2, v1, v0

    .line 508
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 511
    :cond_0
    return-void
.end method

.method private setWifiInfo(Landroid/content/Intent;)V
    .locals 16
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 514
    if-nez p1, :cond_1

    .line 595
    :cond_0
    :goto_0
    return-void

    .line 518
    :cond_1
    :try_start_0
    const-string v13, "networkInfo"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/net/NetworkInfo;

    .line 520
    .local v3, "cneNi":Landroid/net/NetworkInfo;
    const-string v13, "linkProperties"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/LinkProperties;

    .line 522
    .local v2, "cneLp":Landroid/net/LinkProperties;
    const-string v13, "wifiInfo"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/WifiInfo;

    .line 524
    .local v4, "cneWi":Landroid/net/wifi/WifiInfo;
    const-string v13, "newRssi"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 525
    .local v11, "newRssi":I
    const/16 v13, 0x15

    move-object/from16 v0, p0

    iput v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->subType:I

    .line 526
    if-eqz v3, :cond_2

    .line 527
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->this$0:Lcom/quicinc/cne/CNE;

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v14

    # invokes: Lcom/quicinc/cne/CNE;->NetworkStateToInt(Landroid/net/NetworkInfo$State;)I
    invoke-static {v13, v14}, Lcom/quicinc/cne/CNE;->access$000(Lcom/quicinc/cne/CNE;Landroid/net/NetworkInfo$State;)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->networkState:I

    .line 530
    :cond_2
    move-object/from16 v0, p0

    iget v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->networkState:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->this$0:Lcom/quicinc/cne/CNE;

    sget-object v15, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    # invokes: Lcom/quicinc/cne/CNE;->NetworkStateToInt(Landroid/net/NetworkInfo$State;)I
    invoke-static {v14, v15}, Lcom/quicinc/cne/CNE;->access$000(Lcom/quicinc/cne/CNE;Landroid/net/NetworkInfo$State;)I

    move-result v14

    if-eq v13, v14, :cond_3

    move-object/from16 v0, p0

    iget v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->networkState:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->this$0:Lcom/quicinc/cne/CNE;

    sget-object v15, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    # invokes: Lcom/quicinc/cne/CNE;->NetworkStateToInt(Landroid/net/NetworkInfo$State;)I
    invoke-static {v14, v15}, Lcom/quicinc/cne/CNE;->access$000(Lcom/quicinc/cne/CNE;Landroid/net/NetworkInfo$State;)I

    move-result v14

    if-ne v13, v14, :cond_4

    .line 533
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/quicinc/cne/CNE$CneWifiInfo;->resetWifiInfo()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 587
    .end local v2    # "cneLp":Landroid/net/LinkProperties;
    .end local v3    # "cneNi":Landroid/net/NetworkInfo;
    .end local v4    # "cneWi":Landroid/net/wifi/WifiInfo;
    .end local v11    # "newRssi":I
    :catch_0
    move-exception v12

    .line 588
    .local v12, "nexp":Ljava/lang/NullPointerException;
    const-string v13, "CORE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "setWifiInfo(): Null Pointer Exception"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v13, v14}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 537
    .end local v12    # "nexp":Ljava/lang/NullPointerException;
    .restart local v2    # "cneLp":Landroid/net/LinkProperties;
    .restart local v3    # "cneNi":Landroid/net/NetworkInfo;
    .restart local v4    # "cneWi":Landroid/net/wifi/WifiInfo;
    .restart local v11    # "newRssi":I
    :cond_4
    if-eqz v2, :cond_9

    .line 538
    :try_start_1
    invoke-virtual {v2}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/List;

    move-result-object v10

    .line 539
    .local v10, "linkAddresses":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    invoke-interface {v10}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/LinkAddress;

    .line 540
    .local v9, "linkAddress":Landroid/net/LinkAddress;
    invoke-virtual {v9}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v1

    .line 541
    .local v1, "addr":Ljava/net/InetAddress;
    instance-of v13, v1, Ljava/net/Inet4Address;

    if-eqz v13, :cond_6

    .line 542
    invoke-virtual {v1}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v13

    if-nez v13, :cond_5

    invoke-virtual {v1}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v13

    if-nez v13, :cond_5

    .line 543
    invoke-virtual {v2}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->ifNameV4:Ljava/lang/String;

    .line 544
    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->ipAddrV4:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 589
    .end local v1    # "addr":Ljava/net/InetAddress;
    .end local v2    # "cneLp":Landroid/net/LinkProperties;
    .end local v3    # "cneNi":Landroid/net/NetworkInfo;
    .end local v4    # "cneWi":Landroid/net/wifi/WifiInfo;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v9    # "linkAddress":Landroid/net/LinkAddress;
    .end local v10    # "linkAddresses":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    .end local v11    # "newRssi":I
    :catch_1
    move-exception v6

    .line 590
    .local v6, "ex":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v13, "CORE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "setWifiInfo(): array out of bound exception: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v13, v14}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 547
    .end local v6    # "ex":Ljava/lang/ArrayIndexOutOfBoundsException;
    .restart local v1    # "addr":Ljava/net/InetAddress;
    .restart local v2    # "cneLp":Landroid/net/LinkProperties;
    .restart local v3    # "cneNi":Landroid/net/NetworkInfo;
    .restart local v4    # "cneWi":Landroid/net/wifi/WifiInfo;
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v9    # "linkAddress":Landroid/net/LinkAddress;
    .restart local v10    # "linkAddresses":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    .restart local v11    # "newRssi":I
    :cond_6
    :try_start_2
    instance-of v13, v1, Ljava/net/Inet6Address;

    if-eqz v13, :cond_5

    .line 548
    invoke-virtual {v1}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v13

    if-nez v13, :cond_5

    invoke-virtual {v1}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v13

    if-nez v13, :cond_5

    .line 549
    invoke-virtual {v2}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->ifNameV6:Ljava/lang/String;

    .line 550
    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->ipAddrV6:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 591
    .end local v1    # "addr":Ljava/net/InetAddress;
    .end local v2    # "cneLp":Landroid/net/LinkProperties;
    .end local v3    # "cneNi":Landroid/net/NetworkInfo;
    .end local v4    # "cneWi":Landroid/net/wifi/WifiInfo;
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v9    # "linkAddress":Landroid/net/LinkAddress;
    .end local v10    # "linkAddresses":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    .end local v11    # "newRssi":I
    :catch_2
    move-exception v6

    .line 592
    .local v6, "ex":Ljava/lang/Exception;
    const-string v13, "CORE"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "setWifiInfo(): caught unexpected exception: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v13, v14}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 556
    .end local v6    # "ex":Ljava/lang/Exception;
    .restart local v2    # "cneLp":Landroid/net/LinkProperties;
    .restart local v3    # "cneNi":Landroid/net/NetworkInfo;
    .restart local v4    # "cneWi":Landroid/net/wifi/WifiInfo;
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v10    # "linkAddresses":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    .restart local v11    # "newRssi":I
    :cond_7
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    const/4 v13, 0x2

    if-ge v7, v13, :cond_8

    .line 557
    :try_start_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->dnsInfo:[Ljava/lang/String;

    const-string v14, "0.0.0.0"

    aput-object v14, v13, v7

    .line 556
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 559
    :cond_8
    invoke-virtual {v2}, Landroid/net/LinkProperties;->getDnsServers()Ljava/util/List;

    move-result-object v5

    .line 560
    .local v5, "dnsAddress":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/net/InetAddress;>;"
    const/4 v7, 0x0

    .line 561
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/net/InetAddress;

    .line 562
    .restart local v1    # "addr":Ljava/net/InetAddress;
    const/4 v13, 0x2

    if-lt v7, v13, :cond_c

    .line 564
    const-string v13, "CORE"

    const-string v14, "getDns: Max dns addrs reached"

    # invokes: Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v13, v14}, Lcom/quicinc/cne/CNE;->access$100(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    .end local v1    # "addr":Ljava/net/InetAddress;
    .end local v5    # "dnsAddress":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/net/InetAddress;>;"
    .end local v7    # "i":I
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "linkAddresses":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    :cond_9
    if-eqz v4, :cond_a

    .line 573
    invoke-virtual {v4}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->bssid:Ljava/lang/String;

    .line 574
    invoke-virtual {v4}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/net/wifi/WifiInfo;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->ssid:Ljava/lang/String;

    .line 575
    invoke-virtual {v4}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->rssi:I

    .line 579
    :cond_a
    if-eqz v11, :cond_b

    .line 580
    move-object/from16 v0, p0

    iput v11, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->rssi:I

    .line 583
    :cond_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v13}, Lcom/quicinc/cne/CNE;->access$200(Lcom/quicinc/cne/CNE;)Landroid/net/wifi/WifiManager;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 584
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v13}, Lcom/quicinc/cne/CNE;->access$200(Lcom/quicinc/cne/CNE;)Landroid/net/wifi/WifiManager;

    move-result-object v13

    invoke-virtual {v13}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->softApState:I

    goto/16 :goto_0

    .line 567
    .restart local v1    # "addr":Ljava/net/InetAddress;
    .restart local v5    # "dnsAddress":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/net/InetAddress;>;"
    .restart local v7    # "i":I
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v10    # "linkAddresses":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/net/LinkAddress;>;"
    :cond_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->dnsInfo:[Ljava/lang/String;

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v13, v7
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 568
    add-int/lit8 v7, v7, 0x1

    .line 569
    goto :goto_3
.end method
