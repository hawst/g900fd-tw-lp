.class Lcom/quicinc/cne/andsf/Extension$CqeThreshold;
.super Ljava/lang/Object;
.source "Extension.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quicinc/cne/andsf/Extension;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CqeThreshold"
.end annotation


# instance fields
.field private apid:Ljava/lang/String;

.field public data:Landroid/content/ContentValues;

.field private id:Ljava/lang/String;

.field private nodeName:Ljava/lang/String;

.field final synthetic this$0:Lcom/quicinc/cne/andsf/Extension;

.field private unit:Ljava/lang/String;

.field private valueStr:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/quicinc/cne/andsf/Extension;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)V
    .locals 4
    .param p2, "thresholdTypeNode"    # Lorg/w3c/dom/Node;
    .param p3, "idNode"    # Lorg/w3c/dom/Node;
    .param p4, "apidNode"    # Lorg/w3c/dom/Node;

    .prologue
    const/4 v3, 0x0

    .line 723
    iput-object p1, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 722
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    iput-object v2, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->data:Landroid/content/ContentValues;

    .line 724
    invoke-interface {p2}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    .line 725
    invoke-interface {p2}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    .line 726
    const-string v2, "CqeThreshold: Putting attributes..."

    invoke-static {v2}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 727
    check-cast p2, Lorg/w3c/dom/Element;

    .end local p2    # "thresholdTypeNode":Lorg/w3c/dom/Node;
    invoke-interface {p2}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v0

    .line 728
    .local v0, "attrs":Lorg/w3c/dom/NamedNodeMap;
    const-string v2, "units"

    invoke-interface {v0, v2}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 729
    .local v1, "threshUnits":Lorg/w3c/dom/Node;
    if-nez v1, :cond_0

    move-object v2, v3

    :goto_0
    iput-object v2, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->unit:Ljava/lang/String;

    .line 730
    invoke-interface {p3}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->id:Ljava/lang/String;

    .line 731
    if-nez p4, :cond_1

    :goto_1
    iput-object v3, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->apid:Ljava/lang/String;

    .line 732
    return-void

    .line 729
    :cond_0
    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 731
    :cond_1
    invoke-interface {p4}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method static synthetic access$200(Lcom/quicinc/cne/andsf/Extension$CqeThreshold;)I
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/andsf/Extension$CqeThreshold;

    .prologue
    .line 715
    invoke-direct {p0}, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->prontoParameterValidation()I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/quicinc/cne/andsf/Extension$CqeThreshold;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/andsf/Extension$CqeThreshold;

    .prologue
    .line 715
    invoke-direct {p0}, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->addToDatabase()V

    return-void
.end method

.method static synthetic access$400(Lcom/quicinc/cne/andsf/Extension$CqeThreshold;)I
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/andsf/Extension$CqeThreshold;

    .prologue
    .line 715
    invoke-direct {p0}, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->romeParameterValidation()I

    move-result v0

    return v0
.end method

.method private addToDatabase()V
    .locals 3

    .prologue
    .line 1358
    const-string v0, "CqeThreshold: adding CQE Thresholds to database."

    invoke-static {v0}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1359
    iget-object v0, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->data:Landroid/content/ContentValues;

    const-string v1, "Type"

    iget-object v2, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1361
    iget-object v0, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->data:Landroid/content/ContentValues;

    const-string v1, "Value"

    iget-object v2, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1363
    iget-object v0, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->data:Landroid/content/ContentValues;

    const-string v1, "Units"

    iget-object v2, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->unit:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1365
    iget-object v0, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->data:Landroid/content/ContentValues;

    const-string v1, "id"

    iget-object v2, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1367
    iget-object v0, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->data:Landroid/content/ContentValues;

    const-string v1, "apid"

    iget-object v2, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->apid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1368
    const-string v0, "CqeThreshold: finished"

    invoke-static {v0}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1369
    return-void
.end method

.method private prontoParameterValidation()I
    .locals 13

    .prologue
    const/16 v12, -0x63

    const/high16 v11, -0x40800000    # -1.0f

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v9, 0x1

    const/4 v5, -0x3

    .line 735
    const-string v6, "validating CQE Threshold for Pronto"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 739
    const/16 v1, -0x63

    .line 740
    .local v1, "dropValue":I
    const/16 v0, -0xa

    .line 743
    .local v0, "addValue":I
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "RSSIAddThreshold"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 744
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIAddThreshold"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 745
    const-string v6, "Adding DUPLICATE RSSIAddThreshold."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 1035
    :goto_0
    return v5

    .line 748
    :cond_0
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIAddThreshold"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 750
    :try_start_0
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 755
    if-lt v0, v12, :cond_1

    const/16 v6, -0xa

    if-le v0, v6, :cond_30

    .line 757
    :cond_1
    const-string v6, "RSSIAddThreshold value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 751
    :catch_0
    move-exception v2

    .line 752
    .local v2, "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: RSSIAddThreshold Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 761
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_2
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "RSSIDropThreshold"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 762
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIDropThreshold"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 763
    const-string v6, "Adding DUPLICATE RSSIDropThreshold."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 766
    :cond_3
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIDropThreshold"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 768
    :try_start_1
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 773
    if-lt v1, v12, :cond_4

    const/16 v6, -0xa

    if-le v1, v6, :cond_30

    .line 775
    :cond_4
    const-string v6, "RSSIDropThreshold value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 769
    :catch_1
    move-exception v2

    .line 770
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: RSSIDropThreshold Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 779
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_5
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "RSSIModelThreshold"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 780
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIModelThreshold"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 781
    const-string v6, "Adding DUPLICATE RSSIModelThreshold."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 784
    :cond_6
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIModelThreshold"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 786
    :try_start_2
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v4

    .line 791
    .local v4, "value":I
    if-lt v4, v12, :cond_7

    const/16 v6, -0xa

    if-le v4, v6, :cond_30

    .line 793
    :cond_7
    const-string v6, "RSSIModelThreshold value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 787
    .end local v4    # "value":I
    :catch_2
    move-exception v2

    .line 788
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: RSSIModelThreshold Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 797
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_8
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "RSSIAveragingInterval"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 798
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIAveragingInterval"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 799
    const-string v6, "Adding DUPLICATE RSSIAveragingInterval."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 802
    :cond_9
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIAveragingInterval"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 804
    :try_start_3
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    move-result v4

    .line 809
    .restart local v4    # "value":I
    if-lt v4, v9, :cond_a

    const/16 v6, 0xe10

    if-le v4, v6, :cond_30

    .line 811
    :cond_a
    const-string v6, "RSSIAveragingInterval value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 805
    .end local v4    # "value":I
    :catch_3
    move-exception v2

    .line 806
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: RSSIAveragingInterval Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 815
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_b
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "RSSIMacTimerThreshold"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 816
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIMacTimerThreshold"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 817
    const-string v6, "Adding DUPLICATE RSSIMacTimerThreshold."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 820
    :cond_c
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIMacTimerThreshold"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 822
    :try_start_4
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_4

    move-result v4

    .line 827
    .restart local v4    # "value":I
    if-lt v4, v12, :cond_d

    const/16 v6, -0xa

    if-gt v4, v6, :cond_d

    if-lt v4, v1, :cond_d

    if-le v4, v0, :cond_30

    .line 829
    :cond_d
    const-string v6, "RSSIMacTimerThreshold value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 823
    .end local v4    # "value":I
    :catch_4
    move-exception v2

    .line 824
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: RSSIMacTimerThreshold Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 833
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_e
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "CQETimer"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    .line 834
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "CQETimer"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 835
    const-string v6, "Adding DUPLICATE CQETimer."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 838
    :cond_f
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "CQETimer"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 840
    :try_start_5
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_5

    move-result v4

    .line 845
    .restart local v4    # "value":I
    if-ltz v4, :cond_10

    const/16 v6, 0xe10

    if-le v4, v6, :cond_30

    .line 847
    :cond_10
    const-string v6, "CQETimer value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 841
    .end local v4    # "value":I
    :catch_5
    move-exception v2

    .line 842
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: CQETimer Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 851
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_11
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "MACHysteresisTimer"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 852
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MACHysteresisTimer"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 853
    const-string v6, "Adding DUPLICATE MACHysteresisTimer."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 856
    :cond_12
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MACHysteresisTimer"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 858
    :try_start_6
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_6} :catch_6

    move-result v4

    .line 863
    .restart local v4    # "value":I
    const/16 v6, 0xa

    if-lt v4, v6, :cond_13

    const/16 v6, 0xe10

    if-le v4, v6, :cond_30

    .line 865
    :cond_13
    const-string v6, "MACHysteresisTimer value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 859
    .end local v4    # "value":I
    :catch_6
    move-exception v2

    .line 860
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: MACHysteresisTimer Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 869
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_14
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "MACStatsAveragingAlpha"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 870
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MACStatsAveragingAlpha"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 871
    const-string v6, "Adding DUPLICATE MACStatsAveragingAlpha."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 874
    :cond_15
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MACStatsAveragingAlpha"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 876
    :try_start_7
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_7} :catch_7

    move-result v3

    .line 881
    .local v3, "floatValue":F
    const/4 v6, 0x0

    cmpg-float v6, v3, v6

    if-ltz v6, :cond_16

    cmpl-float v6, v3, v10

    if-lez v6, :cond_30

    .line 883
    :cond_16
    const-string v6, "MACStatsAveragingAlpha value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 877
    .end local v3    # "floatValue":F
    :catch_7
    move-exception v2

    .line 878
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: MACStatsAveragingAlpha Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 887
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_17
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "FrameCntThreshold"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    .line 888
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "FrameCntThreshold"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 889
    const-string v6, "Adding DUPLICATE FrameCntThreshold."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 892
    :cond_18
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "FrameCntThreshold"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 894
    :try_start_8
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/lang/NumberFormatException; {:try_start_8 .. :try_end_8} :catch_8

    move-result v4

    .line 899
    .restart local v4    # "value":I
    if-ltz v4, :cond_19

    const/16 v6, 0x3e8

    if-le v4, v6, :cond_30

    .line 901
    :cond_19
    const-string v6, "FrameCntThreshold value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 895
    .end local v4    # "value":I
    :catch_8
    move-exception v2

    .line 896
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: FrameCntThreshold Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 905
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_1a
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "MACMibThreshold2a"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1d

    .line 906
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MACMibThreshold2a"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 907
    const-string v6, "Adding DUPLICATE MACMibThreshold2a."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 910
    :cond_1b
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MACMibThreshold2a"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 912
    :try_start_9
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_9 .. :try_end_9} :catch_9

    move-result v3

    .line 917
    .restart local v3    # "floatValue":F
    const/4 v6, 0x0

    cmpg-float v6, v3, v6

    if-ltz v6, :cond_1c

    const/high16 v6, 0x42c80000    # 100.0f

    cmpl-float v6, v3, v6

    if-lez v6, :cond_30

    .line 919
    :cond_1c
    const-string v6, "MACMibThreshold value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 913
    .end local v3    # "floatValue":F
    :catch_9
    move-exception v2

    .line 914
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: MACMibThreshold Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 923
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_1d
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "MACMibThreshold2b"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_20

    .line 924
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MACMibThreshold2b"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 925
    const-string v6, "Adding DUPLICATE MACMibThreshold2b."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 928
    :cond_1e
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MACMibThreshold2b"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 930
    :try_start_a
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_a
    .catch Ljava/lang/NumberFormatException; {:try_start_a .. :try_end_a} :catch_a

    move-result v3

    .line 935
    .restart local v3    # "floatValue":F
    const/4 v6, 0x0

    cmpg-float v6, v3, v6

    if-ltz v6, :cond_1f

    const/high16 v6, 0x42c80000    # 100.0f

    cmpl-float v6, v3, v6

    if-lez v6, :cond_30

    .line 937
    :cond_1f
    const-string v6, "MACMibThreshold value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 931
    .end local v3    # "floatValue":F
    :catch_a
    move-exception v2

    .line 932
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: MACMibThreshold Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 941
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_20
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "ColdStartThreshold"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_23

    .line 942
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "ColdStartThreshold"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_21

    .line 943
    const-string v6, "Adding DUPLICATE MACMibThreshold2b."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 946
    :cond_21
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "ColdStartThreshold"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 948
    :try_start_b
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_b
    .catch Ljava/lang/NumberFormatException; {:try_start_b .. :try_end_b} :catch_b

    move-result v4

    .line 953
    .restart local v4    # "value":I
    if-ltz v4, :cond_22

    const/16 v6, 0x3e8

    if-le v4, v6, :cond_30

    .line 954
    :cond_22
    const-string v6, "ColdStartThreshold value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 949
    .end local v4    # "value":I
    :catch_b
    move-exception v2

    .line 950
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: ColdStartThreshold Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 959
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_23
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "RetryMetricWeight2a"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_26

    .line 960
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RetryMetricWeight2a"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_24

    .line 961
    const-string v6, "Adding DUPLICATE RetryMetricWeight2a."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 964
    :cond_24
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RetryMetricWeight2a"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 966
    :try_start_c
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_c
    .catch Ljava/lang/NumberFormatException; {:try_start_c .. :try_end_c} :catch_c

    move-result v3

    .line 971
    .restart local v3    # "floatValue":F
    cmpg-float v6, v3, v11

    if-ltz v6, :cond_25

    cmpl-float v6, v3, v10

    if-lez v6, :cond_30

    .line 973
    :cond_25
    const-string v6, "RetryMetricWeight value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 967
    .end local v3    # "floatValue":F
    :catch_c
    move-exception v2

    .line 968
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: RetryMetricWeight Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 977
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_26
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "RetryMetricWeight2b"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_29

    .line 978
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RetryMetricWeight2b"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_27

    .line 979
    const-string v6, "Adding DUPLICATE RetryMetricWeight2b."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 982
    :cond_27
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RetryMetricWeight2b"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 984
    :try_start_d
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_d
    .catch Ljava/lang/NumberFormatException; {:try_start_d .. :try_end_d} :catch_d

    move-result v3

    .line 989
    .restart local v3    # "floatValue":F
    cmpg-float v6, v3, v11

    if-ltz v6, :cond_28

    cmpl-float v6, v3, v10

    if-lez v6, :cond_30

    .line 991
    :cond_28
    const-string v6, "RetryMetricWeight value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 985
    .end local v3    # "floatValue":F
    :catch_d
    move-exception v2

    .line 986
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: RetryMetricWeight Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 995
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_29
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "MultiRetryMetricWeight2a"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2c

    .line 996
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MultiRetryMetricWeight2a"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2a

    .line 997
    const-string v6, "Adding DUPLICATE MultiRetryMetricWeight2a."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1000
    :cond_2a
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MultiRetryMetricWeight2a"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1002
    :try_start_e
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_e
    .catch Ljava/lang/NumberFormatException; {:try_start_e .. :try_end_e} :catch_e

    move-result v3

    .line 1007
    .restart local v3    # "floatValue":F
    cmpg-float v6, v3, v11

    if-ltz v6, :cond_2b

    cmpl-float v6, v3, v10

    if-lez v6, :cond_30

    .line 1009
    :cond_2b
    const-string v6, "MultiRetryMetricWeight value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1003
    .end local v3    # "floatValue":F
    :catch_e
    move-exception v2

    .line 1004
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: MultiRetryMetricWeight Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1013
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_2c
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "MultiRetryMetricWeight2b"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2f

    .line 1014
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MultiRetryMetricWeight2b"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2d

    .line 1015
    const-string v6, "Adding DUPLICATE MultiRetryMetricWeight2b."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1018
    :cond_2d
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MultiRetryMetricWeight2b"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1020
    :try_start_f
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_f
    .catch Ljava/lang/NumberFormatException; {:try_start_f .. :try_end_f} :catch_f

    move-result v3

    .line 1025
    .restart local v3    # "floatValue":F
    cmpg-float v6, v3, v11

    if-ltz v6, :cond_2e

    cmpl-float v6, v3, v10

    if-lez v6, :cond_30

    .line 1027
    :cond_2e
    const-string v6, "MultiRetryMetricWeight value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1021
    .end local v3    # "floatValue":F
    :catch_f
    move-exception v2

    .line 1022
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: MultiRetryMetricWeight Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1032
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_2f
    const-string v6, "Unsupported CQE parameter."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1035
    :cond_30
    const/16 v5, 0x3e8

    goto/16 :goto_0
.end method

.method private romeParameterValidation()I
    .locals 13

    .prologue
    const/16 v12, -0x63

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v5, -0x3

    .line 1039
    const-string v6, "validating CQE Threshold for Rome"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1042
    const/16 v1, -0x63

    .line 1043
    .local v1, "dropValue":I
    const/16 v0, -0xa

    .line 1045
    .local v0, "addValue":I
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "RSSIAddThreshold"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1046
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIAddThreshold"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1047
    const-string v6, "Adding DUPLICATE RSSIAddThreshold."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 1355
    :cond_0
    :goto_0
    return v5

    .line 1050
    :cond_1
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIAddThreshold"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1052
    :try_start_0
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1057
    if-lt v0, v12, :cond_2

    const/16 v6, -0xa

    if-le v0, v6, :cond_1d

    .line 1059
    :cond_2
    const-string v6, "RSSIAddThreshold value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 1053
    :catch_0
    move-exception v2

    .line 1054
    .local v2, "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: RSSIAddThreshold Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 1063
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_3
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "RSSIDropThreshold"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1064
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIDropThreshold"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1065
    const-string v6, "Adding DUPLICATE RSSIDropThreshold."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 1068
    :cond_4
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIDropThreshold"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1070
    :try_start_1
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v1

    .line 1075
    if-lt v1, v12, :cond_5

    const/16 v6, -0xa

    if-le v1, v6, :cond_1d

    .line 1077
    :cond_5
    const-string v6, "RSSIDropThreshold value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 1071
    :catch_1
    move-exception v2

    .line 1072
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: RSSIDropThreshold Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 1081
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_6
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "RSSIAveragingInterval"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 1082
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIAveragingInterval"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1083
    const-string v6, "Adding DUPLICATE RSSIAveragingInterval."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1086
    :cond_7
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIAveragingInterval"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1088
    :try_start_2
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v4

    .line 1093
    .local v4, "value":I
    if-lt v4, v9, :cond_8

    const/16 v6, 0xe10

    if-le v4, v6, :cond_1d

    .line 1095
    :cond_8
    const-string v6, "RSSIAveragingInterval value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1089
    .end local v4    # "value":I
    :catch_2
    move-exception v2

    .line 1090
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: RSSIAveragingInterval Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1099
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_9
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "RSSIMacTimerThreshold"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 1100
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIMacTimerThreshold"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1101
    const-string v6, "Adding DUPLICATE RSSIMacTimerThreshold."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1104
    :cond_a
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RSSIMacTimerThreshold"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1106
    :try_start_3
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    move-result v4

    .line 1111
    .restart local v4    # "value":I
    if-lt v4, v12, :cond_b

    const/16 v6, -0xa

    if-gt v4, v6, :cond_b

    if-lt v4, v1, :cond_b

    if-le v4, v0, :cond_1d

    .line 1113
    :cond_b
    const-string v6, "RSSIMacTimerThreshold value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1107
    .end local v4    # "value":I
    :catch_3
    move-exception v2

    .line 1108
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: RSSIMacTimerThreshold Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1117
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_c
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "CQETimer"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 1118
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "CQETimer"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 1119
    const-string v6, "Adding DUPLICATE CQETimer."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1122
    :cond_d
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "CQETimer"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1124
    :try_start_4
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_4

    move-result v4

    .line 1129
    .restart local v4    # "value":I
    if-ltz v4, :cond_e

    const/16 v6, 0xe10

    if-le v4, v6, :cond_1d

    .line 1131
    :cond_e
    const-string v6, "CQETimer value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1125
    .end local v4    # "value":I
    :catch_4
    move-exception v2

    .line 1126
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: CQETimer Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1135
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_f
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "MACHysteresisTimer"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 1136
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MACHysteresisTimer"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 1137
    const-string v6, "Adding DUPLICATE MACHysteresisTimer."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1140
    :cond_10
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MACHysteresisTimer"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1142
    :try_start_5
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_5

    move-result v4

    .line 1147
    .restart local v4    # "value":I
    const/16 v6, 0xa

    if-lt v4, v6, :cond_11

    const/16 v6, 0xe10

    if-le v4, v6, :cond_1d

    .line 1149
    :cond_11
    const-string v6, "MACHysteresisTimer value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1143
    .end local v4    # "value":I
    :catch_5
    move-exception v2

    .line 1144
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: MACHysteresisTimer Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1153
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_12
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "MACStatsAveragingAlpha"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_15

    .line 1154
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MACStatsAveragingAlpha"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_13

    .line 1155
    const-string v6, "Adding DUPLICATE MACStatsAveragingAlpha."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1158
    :cond_13
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "MACStatsAveragingAlpha"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1160
    :try_start_6
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_6
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_6} :catch_6

    move-result v3

    .line 1165
    .local v3, "floatValue":F
    cmpg-float v6, v3, v10

    if-ltz v6, :cond_14

    cmpl-float v6, v3, v11

    if-lez v6, :cond_1d

    .line 1167
    :cond_14
    const-string v6, "MACStatsAveragingAlpha value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1161
    .end local v3    # "floatValue":F
    :catch_6
    move-exception v2

    .line 1162
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: MACStatsAveragingAlpha Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1171
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_15
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "RMP_THR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_18

    .line 1172
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RMP_THR"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_16

    .line 1173
    const-string v6, "Adding DUPLICATE RMP_THR."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1176
    :cond_16
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RMP_THR"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1178
    :try_start_7
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_7
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_7} :catch_7

    move-result v3

    .line 1183
    .restart local v3    # "floatValue":F
    cmpg-float v6, v3, v10

    if-ltz v6, :cond_17

    cmpl-float v6, v3, v11

    if-lez v6, :cond_1d

    .line 1185
    :cond_17
    const-string v6, "RMP_THR value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1179
    .end local v3    # "floatValue":F
    :catch_7
    move-exception v2

    .line 1180
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: RMP_THR Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1189
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_18
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "RMP_CNT_THR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1b

    .line 1190
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RMP_CNT_THR"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_19

    .line 1191
    const-string v6, "Adding DUPLICATE RMP_CNT_THR."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1194
    :cond_19
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RMP_CNT_THR"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1196
    :try_start_8
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_8
    .catch Ljava/lang/NumberFormatException; {:try_start_8 .. :try_end_8} :catch_8

    move-result v4

    .line 1201
    .restart local v4    # "value":I
    if-ltz v4, :cond_1a

    const v6, 0xfde8

    if-le v4, v6, :cond_1d

    .line 1203
    :cond_1a
    const-string v6, "RMP_CNT_THR value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1197
    .end local v4    # "value":I
    :catch_8
    move-exception v2

    .line 1198
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: RMP_CNT_THR Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1207
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_1b
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "RX_MCS_THR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1e

    .line 1208
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RX_MCS_THR"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1c

    .line 1209
    const-string v6, "Adding DUPLICATE RX_MCS_THR."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1212
    :cond_1c
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RX_MCS_THR"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1214
    :try_start_9
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 1215
    .restart local v4    # "value":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RX_MCS_THR: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/NumberFormatException; {:try_start_9 .. :try_end_9} :catch_9

    .line 1220
    if-ltz v4, :cond_0

    const/16 v6, 0xa

    if-gt v4, v6, :cond_0

    .line 1355
    .end local v4    # "value":I
    :cond_1d
    const/16 v5, 0x3e8

    goto/16 :goto_0

    .line 1216
    :catch_9
    move-exception v2

    .line 1217
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: RX_MCS_THR Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1225
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_1e
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "RX_BW_THR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_21

    .line 1226
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RX_BW_THR"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1f

    .line 1227
    const-string v6, "Adding DUPLICATE RX_BW_THR."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1230
    :cond_1f
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "RX_BW_THR"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1232
    :try_start_a
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_a
    .catch Ljava/lang/NumberFormatException; {:try_start_a .. :try_end_a} :catch_a

    move-result v4

    .line 1237
    .restart local v4    # "value":I
    if-ltz v4, :cond_20

    const/4 v6, 0x3

    if-le v4, v6, :cond_1d

    .line 1239
    :cond_20
    const-string v6, "RX_BW_THR value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1233
    .end local v4    # "value":I
    :catch_a
    move-exception v2

    .line 1234
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: RX_BW_THR Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1243
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_21
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "TMD_THR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_24

    .line 1244
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "TMD_THR"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_22

    .line 1245
    const-string v6, "Adding DUPLICATE TMD_THR."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1248
    :cond_22
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "TMD_THR"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1250
    :try_start_b
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_b
    .catch Ljava/lang/NumberFormatException; {:try_start_b .. :try_end_b} :catch_b

    move-result v3

    .line 1255
    .restart local v3    # "floatValue":F
    cmpg-float v6, v3, v10

    if-ltz v6, :cond_23

    cmpl-float v6, v3, v11

    if-lez v6, :cond_1d

    .line 1257
    :cond_23
    const-string v6, "TMD_THR value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1251
    .end local v3    # "floatValue":F
    :catch_b
    move-exception v2

    .line 1252
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: TMD_THR Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1261
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_24
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "TMD_CNT_THR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_27

    .line 1262
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "TMD_CNT_THR"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_25

    .line 1263
    const-string v6, "Adding DUPLICATE TMD_CNT_THR."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1266
    :cond_25
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "TMD_CNT_THR"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1268
    :try_start_c
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_c
    .catch Ljava/lang/NumberFormatException; {:try_start_c .. :try_end_c} :catch_c

    move-result v4

    .line 1273
    .restart local v4    # "value":I
    if-ltz v4, :cond_26

    const v6, 0xfde8

    if-le v4, v6, :cond_1d

    .line 1275
    :cond_26
    const-string v6, "RetryMetricWeight value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1269
    .end local v4    # "value":I
    :catch_c
    move-exception v2

    .line 1270
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: TMD_CNT_THR Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1279
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_27
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "TMR_THR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2a

    .line 1280
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "TMR_THR"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_28

    .line 1281
    const-string v6, "Adding DUPLICATE TMR_THR."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1284
    :cond_28
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "TMR_THR"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1286
    :try_start_d
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_d
    .catch Ljava/lang/NumberFormatException; {:try_start_d .. :try_end_d} :catch_d

    move-result v3

    .line 1291
    .restart local v3    # "floatValue":F
    cmpg-float v6, v3, v10

    if-ltz v6, :cond_29

    cmpl-float v6, v3, v11

    if-lez v6, :cond_1d

    .line 1293
    :cond_29
    const-string v6, "TMR_THR value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1287
    .end local v3    # "floatValue":F
    :catch_d
    move-exception v2

    .line 1288
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: TMR_THR Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1297
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_2a
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "TMR_CNT_THR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2d

    .line 1298
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "TMR_CNT_THR"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2b

    .line 1299
    const-string v6, "Adding DUPLICATE TMR_CNT_THR."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1302
    :cond_2b
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "TMR_CNT_THR"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1304
    :try_start_e
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_e
    .catch Ljava/lang/NumberFormatException; {:try_start_e .. :try_end_e} :catch_e

    move-result v4

    .line 1309
    .restart local v4    # "value":I
    if-ltz v4, :cond_2c

    const v6, 0xfde8

    if-le v4, v6, :cond_1d

    .line 1311
    :cond_2c
    const-string v6, "TMR_CNT_THR value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1305
    .end local v4    # "value":I
    :catch_e
    move-exception v2

    .line 1306
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: TMR_CNT_THR Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1315
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_2d
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "TX_MCS_THR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_30

    .line 1316
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "TX_MCS_THR"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2e

    .line 1317
    const-string v6, "Adding DUPLICATE TX_MCS_THR."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1320
    :cond_2e
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "TX_MCS_THR"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1322
    :try_start_f
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_f
    .catch Ljava/lang/NumberFormatException; {:try_start_f .. :try_end_f} :catch_f

    move-result v4

    .line 1327
    .restart local v4    # "value":I
    if-ltz v4, :cond_2f

    const/16 v6, 0xa

    if-le v4, v6, :cond_1d

    .line 1329
    :cond_2f
    const-string v6, "TX_MCS_THR value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1323
    .end local v4    # "value":I
    :catch_f
    move-exception v2

    .line 1324
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: TX_MCS_THR Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1333
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_30
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->nodeName:Ljava/lang/String;

    const-string v7, "TX_BW_THR"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_33

    .line 1334
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "TX_BW_THR"

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_31

    .line 1335
    const-string v6, "Adding DUPLICATE TX_BW_THR."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1338
    :cond_31
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    iget-object v6, v6, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    const-string v7, "TX_BW_THR"

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340
    :try_start_10
    iget-object v6, p0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_10
    .catch Ljava/lang/NumberFormatException; {:try_start_10 .. :try_end_10} :catch_10

    move-result v4

    .line 1345
    .restart local v4    # "value":I
    if-ltz v4, :cond_32

    const/4 v6, 0x3

    if-le v4, v6, :cond_1d

    .line 1347
    :cond_32
    const-string v6, "TX_BW_THR value out of range"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1341
    .end local v4    # "value":I
    :catch_10
    move-exception v2

    .line 1342
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v6, "CQE parameter: TX_BW_THR Number Format Exception"

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1352
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_33
    const-string v6, "Unsupported CQE parameter."

    invoke-static {v6}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
