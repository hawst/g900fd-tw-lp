.class Lcom/quicinc/cne/andsf/Extension$TqeThreshold;
.super Ljava/lang/Object;
.source "Extension.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quicinc/cne/andsf/Extension;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TqeThreshold"
.end annotation


# instance fields
.field private apid:Ljava/lang/String;

.field public data:Landroid/content/ContentValues;

.field private id:Ljava/lang/String;

.field private nodeName:Ljava/lang/String;

.field final synthetic this$0:Lcom/quicinc/cne/andsf/Extension;

.field private unit:Ljava/lang/String;

.field private valueStr:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/quicinc/cne/andsf/Extension;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)V
    .locals 4
    .param p2, "thresholdTypeNode"    # Lorg/w3c/dom/Node;
    .param p3, "idNode"    # Lorg/w3c/dom/Node;
    .param p4, "apidNode"    # Lorg/w3c/dom/Node;

    .prologue
    const/4 v3, 0x0

    .line 1475
    iput-object p1, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->this$0:Lcom/quicinc/cne/andsf/Extension;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1474
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    iput-object v2, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->data:Landroid/content/ContentValues;

    .line 1476
    invoke-interface {p2}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->nodeName:Ljava/lang/String;

    .line 1477
    invoke-interface {p2}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->valueStr:Ljava/lang/String;

    .line 1478
    const-string v2, "TqeThreshold: Putting attributes..."

    invoke-static {v2}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1479
    check-cast p2, Lorg/w3c/dom/Element;

    .end local p2    # "thresholdTypeNode":Lorg/w3c/dom/Node;
    invoke-interface {p2}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v0

    .line 1480
    .local v0, "attrs":Lorg/w3c/dom/NamedNodeMap;
    const-string v2, "units"

    invoke-interface {v0, v2}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v1

    .line 1481
    .local v1, "threshUnits":Lorg/w3c/dom/Node;
    if-nez v1, :cond_0

    move-object v2, v3

    :goto_0
    iput-object v2, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->unit:Ljava/lang/String;

    .line 1482
    invoke-interface {p3}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->id:Ljava/lang/String;

    .line 1483
    if-nez p4, :cond_1

    :goto_1
    iput-object v3, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->apid:Ljava/lang/String;

    .line 1484
    return-void

    .line 1481
    :cond_0
    invoke-interface {v1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1483
    :cond_1
    invoke-interface {p4}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method static synthetic access$500(Lcom/quicinc/cne/andsf/Extension$TqeThreshold;)I
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/andsf/Extension$TqeThreshold;

    .prologue
    .line 1467
    invoke-direct {p0}, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->parameterValidation()I

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/quicinc/cne/andsf/Extension$TqeThreshold;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/andsf/Extension$TqeThreshold;

    .prologue
    .line 1467
    invoke-direct {p0}, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->addToDatabase()V

    return-void
.end method

.method private addToDatabase()V
    .locals 3

    .prologue
    .line 1552
    const-string v0, "TqeThreshold: adding TQE Thresholds to database."

    invoke-static {v0}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1553
    iget-object v0, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->data:Landroid/content/ContentValues;

    const-string v1, "Type"

    iget-object v2, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->nodeName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1555
    iget-object v0, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->data:Landroid/content/ContentValues;

    const-string v1, "Value"

    iget-object v2, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->valueStr:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1557
    iget-object v0, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->data:Landroid/content/ContentValues;

    const-string v1, "Units"

    iget-object v2, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->unit:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1559
    iget-object v0, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->data:Landroid/content/ContentValues;

    const-string v1, "id"

    iget-object v2, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1561
    iget-object v0, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->data:Landroid/content/ContentValues;

    const-string v1, "apid"

    iget-object v2, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->apid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1562
    const-string v0, "TqeThreshold: finished"

    invoke-static {v0}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1563
    return-void
.end method

.method private parameterValidation()I
    .locals 8

    .prologue
    const v7, 0xffff

    const/4 v4, -0x3

    .line 1487
    const-string v5, "validating TQE Threshold"

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1491
    const/16 v1, -0x63

    .line 1492
    .local v1, "dropValue":I
    const/16 v0, -0xa

    .line 1495
    .local v0, "addValue":I
    iget-object v5, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->nodeName:Ljava/lang/String;

    const-string v6, "DGIMThresh"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1497
    :try_start_0
    iget-object v5, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1502
    if-ltz v0, :cond_0

    if-le v0, v7, :cond_7

    .line 1504
    :cond_0
    const-string v5, "DGIMThresh value out of range"

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 1548
    :goto_0
    return v4

    .line 1498
    :catch_0
    move-exception v2

    .line 1499
    .local v2, "e":Ljava/lang/NumberFormatException;
    const-string v5, "TQE parameter: DGIMThresh Number Format Exception"

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 1508
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_1
    iget-object v5, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->nodeName:Ljava/lang/String;

    const-string v6, "DBDTputThresh"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1510
    :try_start_1
    iget-object v5, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 1515
    const v5, 0x186a0

    if-lt v0, v5, :cond_2

    const v5, 0x5f5e100

    if-le v0, v5, :cond_7

    .line 1517
    :cond_2
    const-string v5, "DBDTputThresh value out of range"

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 1511
    :catch_1
    move-exception v2

    .line 1512
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v5, "TQE parameter: DBDTputThresh Number Format Exception"

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 1521
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_3
    iget-object v5, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->nodeName:Ljava/lang/String;

    const-string v6, "TQETimeWindow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1523
    :try_start_2
    iget-object v5, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v0

    .line 1528
    if-ltz v0, :cond_4

    if-le v0, v7, :cond_7

    .line 1530
    :cond_4
    const-string v5, "TQETimeWindow value out of range"

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 1524
    :catch_2
    move-exception v2

    .line 1525
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v5, "TQE parameter: TQETimeWindow Number Format Exception"

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 1534
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_5
    iget-object v5, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->nodeName:Ljava/lang/String;

    const-string v6, "RatioThresh"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1536
    :try_start_3
    iget-object v5, p0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->valueStr:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    move-result v3

    .line 1541
    .local v3, "floatValue":F
    const/4 v5, 0x0

    cmpg-float v5, v3, v5

    if-lez v5, :cond_6

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v5, v3, v5

    if-ltz v5, :cond_7

    .line 1543
    :cond_6
    const-string v5, "RatioThresh value out of range"

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 1537
    .end local v3    # "floatValue":F
    :catch_3
    move-exception v2

    .line 1538
    .restart local v2    # "e":Ljava/lang/NumberFormatException;
    const-string v5, "TQE parameter: RatioThresh Number Format Exception"

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto :goto_0

    .line 1548
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    :cond_7
    const/16 v4, 0x3e8

    goto :goto_0
.end method
