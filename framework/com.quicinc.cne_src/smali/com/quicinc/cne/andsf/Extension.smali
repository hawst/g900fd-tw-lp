.class public Lcom/quicinc/cne/andsf/Extension;
.super Lcom/quicinc/cne/andsf/AndsfNodeSet;
.source "Extension.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quicinc/cne/andsf/Extension$TqeThreshold;,
        Lcom/quicinc/cne/andsf/Extension$CqeThreshold;,
        Lcom/quicinc/cne/andsf/Extension$BqeThreshold;
    }
.end annotation


# static fields
.field static final MAX_NUMBER_APIDS:I = 0x80

.field static final MAX_NUMBER_BQE_THRESHOLDS:I = 0x2

.field static final MAX_NUMBER_ICD_CONFIG_SET:I = 0x1


# instance fields
.field authApps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation
.end field

.field bqeApIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation
.end field

.field bqeThresholds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/quicinc/cne/andsf/Extension$BqeThreshold;",
            ">;"
        }
    .end annotation
.end field

.field cqeThresholdTags:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field cqeThresholds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/quicinc/cne/andsf/Extension$CqeThreshold;",
            ">;"
        }
    .end annotation
.end field

.field icdApIdSet:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation
.end field

.field icdConfigSet:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation
.end field

.field productId:Ljava/lang/String;

.field tqeThresholds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/quicinc/cne/andsf/Extension$TqeThreshold;",
            ">;"
        }
    .end annotation
.end field

.field wlanChipsetMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 46
    invoke-direct {p0}, Lcom/quicinc/cne/andsf/AndsfNodeSet;-><init>()V

    .line 26
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->bqeThresholds:Ljava/util/ArrayList;

    .line 27
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->bqeApIds:Ljava/util/ArrayList;

    .line 29
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->icdConfigSet:Ljava/util/ArrayList;

    .line 30
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->icdApIdSet:Ljava/util/ArrayList;

    .line 32
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->cqeThresholds:Ljava/util/ArrayList;

    .line 33
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->tqeThresholds:Ljava/util/ArrayList;

    .line 34
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->authApps:Ljava/util/ArrayList;

    .line 40
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->wlanChipsetMap:Ljava/util/HashMap;

    .line 42
    iput-object v2, p0, Lcom/quicinc/cne/andsf/Extension;->productId:Ljava/lang/String;

    .line 47
    const-string v1, "ro.board.platform"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "productName":Ljava/lang/String;
    iget-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->wlanChipsetMap:Ljava/util/HashMap;

    const-string v2, "apq8084"

    const-string v3, "Rome"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    iget-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->wlanChipsetMap:Ljava/util/HashMap;

    const-string v2, "msm8994"

    const-string v3, "Rome"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    if-eqz v0, :cond_0

    .line 51
    iget-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->wlanChipsetMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->productId:Ljava/lang/String;

    .line 53
    :cond_0
    iget-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->productId:Ljava/lang/String;

    const-string v2, "Rome"

    if-eq v1, v2, :cond_1

    .line 54
    const-string v1, "Pronto"

    iput-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->productId:Ljava/lang/String;

    .line 55
    new-instance v1, Ljava/util/HashMap;

    const/16 v2, 0x10

    invoke-direct {v1, v2, v4}, Ljava/util/HashMap;-><init>(IF)V

    iput-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    .line 62
    :goto_0
    return-void

    .line 59
    :cond_1
    new-instance v1, Ljava/util/HashMap;

    const/16 v2, 0x11

    invoke-direct {v1, v2, v4}, Ljava/util/HashMap;-><init>(IF)V

    iput-object v1, p0, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    goto :goto_0
.end method

.method private handleBQEExtension(Lorg/w3c/dom/Element;)I
    .locals 26
    .param p1, "baseNode"    # Lorg/w3c/dom/Element;

    .prologue
    .line 79
    const-string v24, "BQEExtension: Handling InterfacManager "

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 81
    const-string v24, "InterfaceManager"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v23

    .line 83
    .local v23, "wqe":Lorg/w3c/dom/NodeList;
    const-string v24, "BQE_Disabled"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v24

    const/16 v25, 0x0

    invoke-interface/range {v24 .. v25}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 84
    .local v8, "bqeDisabled":Lorg/w3c/dom/Node;
    const-string v24, "ICD_Disabled"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v24

    const/16 v25, 0x0

    invoke-interface/range {v24 .. v25}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    .line 85
    .local v12, "icdDisabled":Lorg/w3c/dom/Node;
    const-string v24, "BQEExtension: checking BQE_Disabled ICD_Disabled tags"

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 87
    if-eqz v8, :cond_0

    if-nez v12, :cond_1

    .line 88
    :cond_0
    const-string v24, "BQEExtension: No BQE_Disabled or ICD_Disabled tag found."

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 89
    const/16 v24, -0x3

    .line 229
    :goto_0
    return v24

    .line 92
    :cond_1
    invoke-interface {v8}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v24

    const/16 v25, 0x0

    invoke-interface/range {v24 .. v25}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v21

    .line 94
    .local v21, "valBqe":Ljava/lang/String;
    invoke-interface {v12}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v24

    const/16 v25, 0x0

    invoke-interface/range {v24 .. v25}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v22

    .line 96
    .local v22, "valIcd":Ljava/lang/String;
    if-eqz v21, :cond_2

    if-nez v22, :cond_3

    .line 97
    :cond_2
    const-string v24, "BQEExtension: missing BQE_Disabled or ICD_Disabled value"

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 98
    const/16 v24, -0x3

    goto :goto_0

    .line 101
    :cond_3
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "BQEExtension: BQE_Disabled "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " ICD_Disabled "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 103
    const-string v24, "true"

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_4

    const-string v24, "false"

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_6

    :cond_4
    const-string v24, "true"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_5

    const-string v24, "false"

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_6

    .line 106
    :cond_5
    const/16 v24, 0x0

    invoke-interface/range {v23 .. v24}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/quicinc/cne/andsf/Extension;->inspectChildren(Lorg/w3c/dom/Node;)V

    .line 114
    const-string v24, "BQEExtension: Handling Authentication"

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 115
    const-string v24, "Authentication"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v23

    .line 116
    const/16 v24, 0x0

    invoke-interface/range {v23 .. v24}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/quicinc/cne/andsf/Extension;->inspectChildren(Lorg/w3c/dom/Node;)V

    .line 118
    const-string v24, "BQEExtension: Handling BQE_Thresholds"

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 119
    const-string v24, "BQE_Thresholds"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v19

    .line 120
    .local v19, "thresholds":Lorg/w3c/dom/NodeList;
    const/4 v15, 0x0

    .line 121
    .local v15, "isDefaultSet":Z
    if-nez v19, :cond_7

    .line 122
    const-string v24, "BQEExtension: missing BQE_Thresholds tag"

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 123
    const/16 v24, -0x3

    goto/16 :goto_0

    .line 109
    .end local v15    # "isDefaultSet":Z
    .end local v19    # "thresholds":Lorg/w3c/dom/NodeList;
    :cond_6
    const-string v24, "BQEExtension: BQE_Disabled or ICD_Disabled value is not true or false"

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 111
    const/16 v24, -0x3

    goto/16 :goto_0

    .line 125
    .restart local v15    # "isDefaultSet":Z
    .restart local v19    # "thresholds":Lorg/w3c/dom/NodeList;
    :cond_7
    invoke-interface/range {v19 .. v19}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v24

    const/16 v25, 0x2

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_8

    .line 126
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "BQEExtension: Num bqe threshold: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-interface/range {v19 .. v19}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " exceeds max, using only first "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const/16 v25, 0x2

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 131
    :cond_8
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    invoke-interface/range {v19 .. v19}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v24

    const/16 v25, 0x2

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->min(II)I

    move-result v24

    move/from16 v0, v24

    if-ge v11, v0, :cond_15

    .line 132
    move-object/from16 v0, v19

    invoke-interface {v0, v11}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    check-cast v24, Lorg/w3c/dom/Element;

    invoke-interface/range {v24 .. v24}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v5

    .line 133
    .local v5, "attrs":Lorg/w3c/dom/NamedNodeMap;
    const-string v24, "Id"

    move-object/from16 v0, v24

    invoke-interface {v5, v0}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v13

    .line 134
    .local v13, "id":Lorg/w3c/dom/Node;
    const-string v14, "0"

    .line 135
    .local v14, "idVal":Ljava/lang/String;
    if-eqz v13, :cond_b

    .line 136
    invoke-interface {v13}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v14

    .line 145
    move-object/from16 v0, v19

    invoke-interface {v0, v11}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    check-cast v24, Lorg/w3c/dom/Element;

    const-string v25, "apIds"

    invoke-interface/range {v24 .. v25}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 147
    .local v4, "apIds":Lorg/w3c/dom/NodeList;
    const/4 v3, 0x0

    .line 150
    .local v3, "apIdValue":Ljava/lang/String;
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v24

    if-lez v24, :cond_f

    .line 151
    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-interface {v4, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    check-cast v24, Lorg/w3c/dom/Element;

    invoke-interface/range {v24 .. v24}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v24

    const-string v25, "Type"

    invoke-interface/range {v24 .. v25}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v20

    .line 153
    .local v20, "type":Ljava/lang/String;
    if-eqz v20, :cond_9

    const-string v24, "SSID"

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_c

    .line 154
    :cond_9
    const-string v24, "BQEExtension: Attribute Type in apIds is null or not SSID ignoring this Node"

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 131
    .end local v3    # "apIdValue":Ljava/lang/String;
    .end local v4    # "apIds":Lorg/w3c/dom/NodeList;
    .end local v20    # "type":Ljava/lang/String;
    :cond_a
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 139
    :cond_b
    const-string v24, "BQEExtension: Attribute Id in BQE_Thresholds not defined ignoring this Node"

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    goto :goto_2

    .line 161
    .restart local v3    # "apIdValue":Ljava/lang/String;
    .restart local v4    # "apIds":Lorg/w3c/dom/NodeList;
    .restart local v20    # "type":Ljava/lang/String;
    :cond_c
    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-interface {v4, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    check-cast v24, Lorg/w3c/dom/Element;

    const-string v25, "apId"

    invoke-interface/range {v24 .. v25}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 163
    .local v2, "apIdNodeList":Lorg/w3c/dom/NodeList;
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v24

    const/16 v25, 0x80

    move/from16 v0, v24

    move/from16 v1, v25

    if-le v0, v1, :cond_d

    .line 164
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "BQEExtension: Num apIds: "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " exceeds max, using only first "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const/16 v25, 0x80

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 167
    :cond_d
    const/16 v16, 0x0

    .local v16, "j":I
    :goto_3
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v24

    const/16 v25, 0x80

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->min(II)I

    move-result v24

    move/from16 v0, v16

    move/from16 v1, v24

    if-ge v0, v1, :cond_10

    .line 168
    move/from16 v0, v16

    invoke-interface {v2, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v24

    const/16 v25, 0x0

    invoke-interface/range {v24 .. v25}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    .line 169
    if-eqz v3, :cond_e

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v24

    if-eqz v24, :cond_e

    .line 170
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 171
    .local v10, "data":Landroid/content/ContentValues;
    const-string v24, "Id"

    move-object/from16 v0, v24

    invoke-virtual {v10, v0, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v24, "apId"

    move-object/from16 v0, v24

    invoke-virtual {v10, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/andsf/Extension;->bqeApIds:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    .end local v10    # "data":Landroid/content/ContentValues;
    :goto_4
    add-int/lit8 v16, v16, 0x1

    goto :goto_3

    .line 175
    :cond_e
    const-string v24, "BQEExtension: ignoring this node apIdVal is null or empty"

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    goto :goto_4

    .line 180
    .end local v2    # "apIdNodeList":Lorg/w3c/dom/NodeList;
    .end local v16    # "j":I
    .end local v20    # "type":Ljava/lang/String;
    :cond_f
    if-nez v15, :cond_11

    .line 181
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 182
    .restart local v10    # "data":Landroid/content/ContentValues;
    const-string v24, "Id"

    move-object/from16 v0, v24

    invoke-virtual {v10, v0, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/andsf/Extension;->bqeApIds:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    const/4 v15, 0x1

    .line 194
    .end local v10    # "data":Landroid/content/ContentValues;
    :cond_10
    move-object/from16 v0, v19

    invoke-interface {v0, v11}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    check-cast v24, Lorg/w3c/dom/Element;

    const-string v25, "RadioTechnology"

    invoke-interface/range {v24 .. v25}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v18

    .line 196
    .local v18, "subRats":Lorg/w3c/dom/NodeList;
    if-nez v18, :cond_12

    .line 197
    const-string v24, "BQEExtension: missing RadioTechnology tags."

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 198
    const/16 v24, -0x3

    goto/16 :goto_0

    .line 186
    .end local v18    # "subRats":Lorg/w3c/dom/NodeList;
    :cond_11
    const-string v24, "BQEExtension: Multiple default nodes not allowed ignoring this Node"

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 200
    .restart local v18    # "subRats":Lorg/w3c/dom/NodeList;
    :cond_12
    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "BQEExtension: RadioTechnology getLength ="

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 201
    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v24

    const/16 v25, 0xc

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_13

    .line 202
    const-string v24, "BQE Extension: Total number of radio technology mismatches."

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 203
    const/16 v24, -0x3

    goto/16 :goto_0

    .line 205
    :cond_13
    const/16 v17, 0x0

    .local v17, "k":I
    :goto_5
    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v24

    move/from16 v0, v17

    move/from16 v1, v24

    if-ge v0, v1, :cond_a

    .line 207
    new-instance v9, Lcom/quicinc/cne/andsf/Extension$BqeThreshold;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/quicinc/cne/andsf/Extension$BqeThreshold;-><init>(Lcom/quicinc/cne/andsf/Extension;)V

    .line 208
    .local v9, "bt":Lcom/quicinc/cne/andsf/Extension$BqeThreshold;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/andsf/Extension;->bqeThresholds:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    move-object/from16 v0, v24

    # invokes: Lcom/quicinc/cne/andsf/Extension$BqeThreshold;->parameterValidation(Lorg/w3c/dom/Node;)I
    invoke-static {v9, v0}, Lcom/quicinc/cne/andsf/Extension$BqeThreshold;->access$000(Lcom/quicinc/cne/andsf/Extension$BqeThreshold;Lorg/w3c/dom/Node;)I

    move-result v24

    const/16 v25, -0x3

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_14

    .line 210
    const-string v24, "BQEExtension: BQE parameter validation failed."

    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 211
    const/16 v24, -0x3

    goto/16 :goto_0

    .line 213
    :cond_14
    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    move-object/from16 v0, v24

    # invokes: Lcom/quicinc/cne/andsf/Extension$BqeThreshold;->addToDatabase(Lorg/w3c/dom/Node;Ljava/lang/String;)V
    invoke-static {v9, v0, v14}, Lcom/quicinc/cne/andsf/Extension$BqeThreshold;->access$100(Lcom/quicinc/cne/andsf/Extension$BqeThreshold;Lorg/w3c/dom/Node;Ljava/lang/String;)V

    .line 205
    add-int/lit8 v17, v17, 0x1

    goto :goto_5

    .line 219
    .end local v3    # "apIdValue":Ljava/lang/String;
    .end local v4    # "apIds":Lorg/w3c/dom/NodeList;
    .end local v5    # "attrs":Lorg/w3c/dom/NamedNodeMap;
    .end local v9    # "bt":Lcom/quicinc/cne/andsf/Extension$BqeThreshold;
    .end local v13    # "id":Lorg/w3c/dom/Node;
    .end local v14    # "idVal":Ljava/lang/String;
    .end local v17    # "k":I
    .end local v18    # "subRats":Lorg/w3c/dom/NodeList;
    :cond_15
    const-string v24, "AuthApps"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    .line 220
    .local v6, "authAppNode":Lorg/w3c/dom/NodeList;
    const/16 v24, 0x0

    move/from16 v0, v24

    invoke-interface {v6, v0}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    check-cast v24, Lorg/w3c/dom/Element;

    const-string v25, "Appname"

    invoke-interface/range {v24 .. v25}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v7

    .line 222
    .local v7, "authAppNodeList":Lorg/w3c/dom/NodeList;
    const/4 v11, 0x0

    :goto_6
    invoke-interface {v7}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v24

    move/from16 v0, v24

    if-ge v11, v0, :cond_16

    .line 223
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 224
    .restart local v10    # "data":Landroid/content/ContentValues;
    const-string v24, "Appname"

    invoke-interface {v7, v11}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/andsf/Extension;->authApps:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    add-int/lit8 v11, v11, 0x1

    goto :goto_6

    .line 229
    .end local v10    # "data":Landroid/content/ContentValues;
    :cond_16
    const/16 v24, 0x3e8

    goto/16 :goto_0
.end method

.method private handleCQEExtension(Lorg/w3c/dom/Element;)I
    .locals 14
    .param p1, "baseNode"    # Lorg/w3c/dom/Element;

    .prologue
    .line 520
    const-string v12, "CQEExtension: Handling CQE extension"

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 521
    const-string v12, "CQE"

    invoke-interface {p1, v12}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 522
    .local v3, "cqeTag":Lorg/w3c/dom/NodeList;
    if-nez v3, :cond_0

    .line 523
    const-string v12, "CQEExtension: missing CQE tag"

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 524
    const/4 v12, -0x3

    .line 630
    :goto_0
    return v12

    .line 526
    :cond_0
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "CQEExtension: CQE tag entries "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 528
    const/4 v6, 0x0

    .local v6, "index":I
    :goto_1
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v12

    if-ge v6, v12, :cond_4

    .line 529
    invoke-interface {v3, v6}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    if-nez v12, :cond_2

    .line 530
    const-string v12, "CQEExtension: missing items under current CQE tag."

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 528
    :cond_1
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 533
    :cond_2
    invoke-interface {v3, v6}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    check-cast v12, Lorg/w3c/dom/Element;

    invoke-interface {v12}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v2

    .line 534
    .local v2, "attrs":Lorg/w3c/dom/NamedNodeMap;
    if-nez v2, :cond_3

    .line 535
    const-string v12, "CQEExtension: no attributes found for current CQE tag"

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    goto :goto_2

    .line 538
    :cond_3
    const-string v12, "prodId"

    invoke-interface {v2, v12}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    .line 539
    .local v8, "prodId":Lorg/w3c/dom/Node;
    if-eqz v8, :cond_1

    .line 542
    iget-object v12, p0, Lcom/quicinc/cne/andsf/Extension;->productId:Ljava/lang/String;

    invoke-interface {v8}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 546
    .end local v2    # "attrs":Lorg/w3c/dom/NamedNodeMap;
    .end local v8    # "prodId":Lorg/w3c/dom/Node;
    :cond_4
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v12

    if-ne v6, v12, :cond_5

    .line 547
    const-string v12, "CQEExtension: No matching CQE tag found"

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 548
    const/4 v12, -0x3

    goto :goto_0

    .line 550
    :cond_5
    invoke-interface {v3, v6}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    check-cast v12, Lorg/w3c/dom/Element;

    const-string v13, "CQE_Thresholds"

    invoke-interface {v12, v13}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v9

    .line 551
    .local v9, "thresholds":Lorg/w3c/dom/NodeList;
    if-nez v9, :cond_6

    .line 552
    const-string v12, "CQEExtension: missing CQE_Thresholds tag, using default."

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 553
    const/4 v12, -0x3

    goto :goto_0

    .line 555
    :cond_6
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "CQEExtension: thresholds length "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 556
    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v12

    const/4 v13, 0x2

    if-le v12, v13, :cond_7

    .line 557
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "CQEExtension: Number of CQE_Thresholds elements "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " is greater than 2"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 559
    const/4 v12, -0x3

    goto/16 :goto_0

    .line 561
    :cond_7
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3
    invoke-interface {v9}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v12

    if-ge v4, v12, :cond_12

    .line 565
    iget-object v12, p0, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    invoke-virtual {v12}, Ljava/util/HashMap;->clear()V

    .line 567
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "CQEExtension: Processing threshold item "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 568
    invoke-interface {v9, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    if-nez v12, :cond_8

    .line 569
    const-string v12, "CQEExtension: missing items under CQE_Thresholds tag."

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 570
    const/4 v12, -0x3

    goto/16 :goto_0

    .line 572
    :cond_8
    invoke-interface {v9, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    check-cast v12, Lorg/w3c/dom/Element;

    invoke-interface {v12}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v2

    .line 573
    .restart local v2    # "attrs":Lorg/w3c/dom/NamedNodeMap;
    const-string v12, "Id"

    invoke-interface {v2, v12}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 574
    .local v5, "id":Lorg/w3c/dom/Node;
    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    const/16 v13, 0x80

    if-le v12, v13, :cond_a

    .line 575
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "CQEExtension:  Id value "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " is greater than 128"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 561
    :cond_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 579
    :cond_a
    invoke-interface {v9, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    check-cast v12, Lorg/w3c/dom/Element;

    const-string v13, "apIds"

    invoke-interface {v12, v13}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 580
    .local v1, "apids":Lorg/w3c/dom/NodeList;
    if-nez v1, :cond_b

    .line 581
    const-string v12, "CQEExtension: missing tag apIds under CQE_Thresholds"

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 582
    const/4 v12, -0x3

    goto/16 :goto_0

    .line 584
    :cond_b
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v12

    if-nez v12, :cond_c

    .line 585
    const-string v12, "CQEExtension: Handling cqe thresholds for default apid"

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 586
    invoke-interface {v9, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    const/4 v13, 0x0

    invoke-direct {p0, v12, v5, v13}, Lcom/quicinc/cne/andsf/Extension;->handleCqeThresholds(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)I

    move-result v12

    const/4 v13, -0x3

    if-ne v12, v13, :cond_10

    .line 587
    const/4 v12, -0x3

    goto/16 :goto_0

    .line 592
    :cond_c
    const/4 v12, 0x0

    invoke-interface {v1, v12}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    check-cast v12, Lorg/w3c/dom/Element;

    invoke-interface {v12}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v12

    const-string v13, "Type"

    invoke-interface {v12, v13}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v12

    invoke-interface {v12}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v11

    .line 593
    .local v11, "type":Ljava/lang/String;
    const-string v12, "SSID"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_f

    .line 595
    const/4 v12, 0x0

    invoke-interface {v1, v12}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    check-cast v12, Lorg/w3c/dom/Element;

    const-string v13, "apId"

    invoke-interface {v12, v13}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 596
    .local v0, "apid":Lorg/w3c/dom/NodeList;
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v12

    const/16 v13, 0x80

    if-le v12, v13, :cond_d

    .line 598
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "CQEExtension: Apid length "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " exceeds max, handling first "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const/16 v13, 0x80

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 601
    :cond_d
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_4
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v12

    const/16 v13, 0x80

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v12

    if-ge v7, v12, :cond_10

    .line 602
    iget-object v12, p0, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    invoke-virtual {v12}, Ljava/util/HashMap;->clear()V

    .line 603
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "CQEExtension: Handling cqe thresholds for apid "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-interface {v0, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v13

    invoke-interface {v13}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v13

    invoke-interface {v13}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 605
    invoke-interface {v9, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    invoke-interface {v0, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v13

    invoke-interface {v13}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v13

    invoke-direct {p0, v12, v5, v13}, Lcom/quicinc/cne/andsf/Extension;->handleCqeThresholds(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)I

    move-result v12

    const/4 v13, -0x3

    if-ne v12, v13, :cond_e

    .line 607
    const/4 v12, -0x3

    goto/16 :goto_0

    .line 601
    :cond_e
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 613
    .end local v0    # "apid":Lorg/w3c/dom/NodeList;
    .end local v7    # "j":I
    :cond_f
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "CQEExtension: Apid type "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " is not supported"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 617
    .end local v11    # "type":Ljava/lang/String;
    :cond_10
    const/4 v10, 0x0

    .line 618
    .local v10, "totalTags":I
    const-string v12, "Rome"

    iget-object v13, p0, Lcom/quicinc/cne/andsf/Extension;->productId:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_11

    .line 619
    const/16 v10, 0x11

    .line 624
    :goto_5
    iget-object v12, p0, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    invoke-virtual {v12}, Ljava/util/HashMap;->size()I

    move-result v12

    if-eq v12, v10, :cond_9

    .line 625
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Missing at least one CQE parameter tag. count: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Lcom/quicinc/cne/andsf/Extension;->cqeThresholdTags:Ljava/util/HashMap;

    invoke-virtual {v13}, Ljava/util/HashMap;->size()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 627
    const/4 v12, -0x3

    goto/16 :goto_0

    .line 622
    :cond_11
    const/16 v10, 0x10

    goto :goto_5

    .line 630
    .end local v1    # "apids":Lorg/w3c/dom/NodeList;
    .end local v2    # "attrs":Lorg/w3c/dom/NamedNodeMap;
    .end local v5    # "id":Lorg/w3c/dom/Node;
    .end local v10    # "totalTags":I
    :cond_12
    const/16 v12, 0x3e8

    goto/16 :goto_0
.end method

.method private handleCqeThresholds(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)I
    .locals 7
    .param p1, "thresholdNode"    # Lorg/w3c/dom/Node;
    .param p2, "idNode"    # Lorg/w3c/dom/Node;
    .param p3, "apidNode"    # Lorg/w3c/dom/Node;

    .prologue
    const/4 v4, -0x3

    .line 635
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleCqeThresholds: thresholdNode "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p2}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 637
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 638
    .local v1, "details":Lorg/w3c/dom/NodeList;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CQE Thresholds details length "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 639
    iget-object v5, p0, Lcom/quicinc/cne/andsf/Extension;->productId:Ljava/lang/String;

    const-string v6, "Pronto"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 640
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-ge v2, v5, :cond_8

    .line 641
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 642
    .local v3, "thresholdTypeNode":Lorg/w3c/dom/Node;
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RSSIAddThreshold"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RSSIDropThreshold"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RSSIModelThreshold"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RSSIAveragingInterval"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RSSIMacTimerThreshold"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "CQETimer"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MACHysteresisTimer"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MACStatsAveragingAlpha"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "FrameCntThreshold"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ColdStartThreshold"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MACMibThreshold2a"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MACMibThreshold2b"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RetryMetricWeight2a"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RetryMetricWeight2b"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MultiRetryMetricWeight2a"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MultiRetryMetricWeight2b"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 660
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adding new CqeThreshold item "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 663
    new-instance v0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;

    invoke-direct {v0, p0, v3, p2, p3}, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;-><init>(Lcom/quicinc/cne/andsf/Extension;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)V

    .line 664
    .local v0, "ct":Lcom/quicinc/cne/andsf/Extension$CqeThreshold;
    iget-object v5, p0, Lcom/quicinc/cne/andsf/Extension;->cqeThresholds:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 665
    # invokes: Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->prontoParameterValidation()I
    invoke-static {v0}, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->access$200(Lcom/quicinc/cne/andsf/Extension$CqeThreshold;)I

    move-result v5

    if-ne v5, v4, :cond_1

    .line 666
    const-string v5, "CQE parameter validation failed."

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 712
    .end local v0    # "ct":Lcom/quicinc/cne/andsf/Extension$CqeThreshold;
    .end local v2    # "i":I
    .end local v3    # "thresholdTypeNode":Lorg/w3c/dom/Node;
    :goto_1
    return v4

    .line 669
    .restart local v0    # "ct":Lcom/quicinc/cne/andsf/Extension$CqeThreshold;
    .restart local v2    # "i":I
    .restart local v3    # "thresholdTypeNode":Lorg/w3c/dom/Node;
    :cond_1
    # invokes: Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->addToDatabase()V
    invoke-static {v0}, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->access$300(Lcom/quicinc/cne/andsf/Extension$CqeThreshold;)V

    .line 640
    .end local v0    # "ct":Lcom/quicinc/cne/andsf/Extension$CqeThreshold;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 673
    .end local v2    # "i":I
    .end local v3    # "thresholdTypeNode":Lorg/w3c/dom/Node;
    :cond_3
    iget-object v5, p0, Lcom/quicinc/cne/andsf/Extension;->productId:Ljava/lang/String;

    const-string v6, "Rome"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 674
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-ge v2, v5, :cond_8

    .line 675
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 676
    .restart local v3    # "thresholdTypeNode":Lorg/w3c/dom/Node;
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RSSIAddThreshold"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RSSIDropThreshold"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RSSIAveragingInterval"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RSSIMacTimerThreshold"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "CQETimer"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MACHysteresisTimer"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "MACStatsAveragingAlpha"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RMP_THR"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RMP_CNT_THR"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RX_MCS_THR"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RX_BW_THR"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "TMD_THR"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "TMD_CNT_THR"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "TMR_THR"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "TMR_CNT_THR"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "TX_MCS_THR"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "TX_BW_THR"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 695
    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adding new CqeThreshold item "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 697
    new-instance v0, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;

    invoke-direct {v0, p0, v3, p2, p3}, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;-><init>(Lcom/quicinc/cne/andsf/Extension;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)V

    .line 698
    .restart local v0    # "ct":Lcom/quicinc/cne/andsf/Extension$CqeThreshold;
    iget-object v5, p0, Lcom/quicinc/cne/andsf/Extension;->cqeThresholds:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 699
    # invokes: Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->romeParameterValidation()I
    invoke-static {v0}, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->access$400(Lcom/quicinc/cne/andsf/Extension$CqeThreshold;)I

    move-result v5

    if-ne v5, v4, :cond_5

    .line 700
    const-string v5, "CQE parameter validation failed."

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 703
    :cond_5
    # invokes: Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->addToDatabase()V
    invoke-static {v0}, Lcom/quicinc/cne/andsf/Extension$CqeThreshold;->access$300(Lcom/quicinc/cne/andsf/Extension$CqeThreshold;)V

    .line 674
    .end local v0    # "ct":Lcom/quicinc/cne/andsf/Extension$CqeThreshold;
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 708
    .end local v2    # "i":I
    .end local v3    # "thresholdTypeNode":Lorg/w3c/dom/Node;
    :cond_7
    const-string v5, "Unknown productId"

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 712
    .restart local v2    # "i":I
    :cond_8
    const/16 v4, 0x3e8

    goto/16 :goto_1
.end method

.method private handleICDExtension(Lorg/w3c/dom/Element;)I
    .locals 16
    .param p1, "baseNode"    # Lorg/w3c/dom/Element;

    .prologue
    .line 438
    const-string v14, "Handling ICD"

    invoke-static {v14}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 439
    const/4 v12, 0x0

    .line 440
    .local v12, "isIcdDefaultSet":Z
    const/4 v3, 0x0

    .line 441
    .local v3, "configTypeVal":Ljava/lang/String;
    const-string v14, "ICD"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v7

    .line 442
    .local v7, "icd":Lorg/w3c/dom/NodeList;
    if-nez v7, :cond_0

    .line 443
    const-string v14, "missing icd tag. using default."

    invoke-static {v14}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 444
    const/4 v14, -0x3

    .line 516
    :goto_0
    return v14

    .line 446
    :cond_0
    invoke-interface {v7}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v14

    const/4 v15, 0x1

    if-le v14, v15, :cond_1

    .line 447
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Num icd tags: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-interface {v7}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " exceeds max ICD tags allowed"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 449
    const/4 v14, -0x3

    goto :goto_0

    .line 451
    :cond_1
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    invoke-interface {v7}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v14

    const/4 v15, 0x1

    invoke-static {v14, v15}, Ljava/lang/Math;->min(II)I

    move-result v14

    if-ge v6, v14, :cond_c

    .line 452
    invoke-interface {v7, v6}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v14

    if-nez v14, :cond_2

    .line 453
    const-string v14, "missing items under icd tag."

    invoke-static {v14}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 454
    const/4 v14, -0x3

    goto :goto_0

    .line 456
    :cond_2
    invoke-interface {v7, v6}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v14

    check-cast v14, Lorg/w3c/dom/Element;

    invoke-interface {v14}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v1

    .line 457
    .local v1, "attrs":Lorg/w3c/dom/NamedNodeMap;
    const-string v14, "config-type"

    invoke-interface {v1, v14}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 458
    .local v2, "configType":Lorg/w3c/dom/Node;
    if-eqz v2, :cond_3

    .line 459
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v3

    .line 460
    const-string v14, "disabled"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_4

    const-string v14, "enabled"

    invoke-virtual {v3, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_4

    .line 461
    const-string v14, "Value for attribute config-type in ICD is not correct, setting to default"

    invoke-static {v14}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 463
    const/4 v14, -0x3

    goto :goto_0

    .line 467
    :cond_3
    const-string v14, "Attribute config-type in ICD not defined, setting to default"

    invoke-static {v14}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 468
    const/4 v14, -0x3

    goto :goto_0

    .line 470
    :cond_4
    invoke-interface {v7, v6}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v14

    check-cast v14, Lorg/w3c/dom/Element;

    const-string v15, "apIds"

    invoke-interface {v14, v15}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v10

    .line 472
    .local v10, "icdApIds":Lorg/w3c/dom/NodeList;
    const/4 v9, 0x0

    .line 473
    .local v9, "icdApIdValue":Ljava/lang/String;
    invoke-interface {v10}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v14

    if-lez v14, :cond_9

    .line 474
    const/4 v14, 0x0

    invoke-interface {v10, v14}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v14

    check-cast v14, Lorg/w3c/dom/Element;

    invoke-interface {v14}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v14

    const-string v15, "Type"

    invoke-interface {v14, v15}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v14

    invoke-interface {v14}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v11

    .line 476
    .local v11, "icdApiType":Ljava/lang/String;
    if-eqz v11, :cond_5

    const-string v14, "SSID"

    invoke-virtual {v11, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_6

    .line 477
    :cond_5
    const-string v14, "Attribute Type in ICD apIds is null or not SSID, reverting to default"

    invoke-static {v14}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 479
    const/4 v14, -0x3

    goto/16 :goto_0

    .line 482
    :cond_6
    const/4 v14, 0x0

    invoke-interface {v10, v14}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v14

    check-cast v14, Lorg/w3c/dom/Element;

    const-string v15, "apId"

    invoke-interface {v14, v15}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v8

    .line 484
    .local v8, "icdApIdNodeList":Lorg/w3c/dom/NodeList;
    invoke-interface {v8}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v14

    const/16 v15, 0x80

    if-le v14, v15, :cond_7

    .line 485
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Num apIds: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-interface {v8}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " exceeds max, using only first "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/16 v15, 0x80

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 489
    :cond_7
    const/4 v13, 0x0

    .local v13, "j":I
    :goto_2
    invoke-interface {v8}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v14

    const/16 v15, 0x80

    invoke-static {v14, v15}, Ljava/lang/Math;->min(II)I

    move-result v14

    if-ge v13, v14, :cond_a

    .line 490
    invoke-interface {v8, v13}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v14

    invoke-interface {v14}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v14

    const/4 v15, 0x0

    invoke-interface {v14, v15}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v14

    invoke-interface {v14}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v9

    .line 491
    if-eqz v9, :cond_8

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v14

    if-eqz v14, :cond_8

    .line 492
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 493
    .local v5, "data":Landroid/content/ContentValues;
    const-string v14, "apId"

    invoke-virtual {v5, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 494
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/quicinc/cne/andsf/Extension;->icdApIdSet:Ljava/util/ArrayList;

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 489
    .end local v5    # "data":Landroid/content/ContentValues;
    :goto_3
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 496
    :cond_8
    const-string v14, "ignoring this node apIdVal is null or empty"

    invoke-static {v14}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    goto :goto_3

    .line 501
    .end local v8    # "icdApIdNodeList":Lorg/w3c/dom/NodeList;
    .end local v11    # "icdApiType":Ljava/lang/String;
    .end local v13    # "j":I
    :cond_9
    if-nez v12, :cond_b

    .line 502
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 503
    .restart local v5    # "data":Landroid/content/ContentValues;
    const-string v14, "apId"

    invoke-virtual {v5, v14, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/quicinc/cne/andsf/Extension;->icdApIdSet:Ljava/util/ArrayList;

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 505
    const/4 v12, 0x1

    .line 451
    .end local v5    # "data":Landroid/content/ContentValues;
    :cond_a
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_1

    .line 507
    :cond_b
    const-string v14, "Multiple default nodes not allowed  ignoring this Node"

    invoke-static {v14}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    goto :goto_4

    .line 513
    .end local v1    # "attrs":Lorg/w3c/dom/NamedNodeMap;
    .end local v2    # "configType":Lorg/w3c/dom/Node;
    .end local v9    # "icdApIdValue":Ljava/lang/String;
    .end local v10    # "icdApIds":Lorg/w3c/dom/NodeList;
    :cond_c
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 514
    .local v4, "config_set":Landroid/content/ContentValues;
    const-string v14, "config_type"

    invoke-virtual {v4, v14, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/quicinc/cne/andsf/Extension;->icdConfigSet:Ljava/util/ArrayList;

    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 516
    const/16 v14, 0x3e8

    goto/16 :goto_0
.end method

.method private handleTQEExtension(Lorg/w3c/dom/Element;)I
    .locals 13
    .param p1, "baseNode"    # Lorg/w3c/dom/Element;

    .prologue
    const/4 v12, 0x0

    const/16 v11, 0x80

    const/4 v9, -0x3

    .line 1373
    const-string v8, "TQEExtension: Handling TQE extension"

    invoke-static {v8}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1374
    const-string v8, "TQE_Thresholds"

    invoke-interface {p1, v8}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v6

    .line 1375
    .local v6, "thresholds":Lorg/w3c/dom/NodeList;
    if-nez v6, :cond_0

    .line 1376
    const-string v8, "TQEExtension: missing TQE_Thresholds tag, using default."

    invoke-static {v8}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    move v8, v9

    .line 1435
    :goto_0
    return v8

    .line 1379
    :cond_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "TQEExtension: thresholds length "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1380
    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v8

    const/4 v10, 0x2

    if-le v8, v10, :cond_1

    .line 1381
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "TQEExtension: Number of TQE_Thresholds elements "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " is greater than 2"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    move v8, v9

    .line 1383
    goto :goto_0

    .line 1385
    :cond_1
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-interface {v6}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v8

    if-ge v3, v8, :cond_a

    .line 1386
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "TQEExtension: Processing threshold item "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1387
    invoke-interface {v6, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    if-nez v8, :cond_2

    .line 1388
    const-string v8, "TQEExtension: missing items under TQE_Thresholds tag."

    invoke-static {v8}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    move v8, v9

    .line 1389
    goto :goto_0

    .line 1391
    :cond_2
    invoke-interface {v6, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    check-cast v8, Lorg/w3c/dom/Element;

    invoke-interface {v8}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v2

    .line 1392
    .local v2, "attrs":Lorg/w3c/dom/NamedNodeMap;
    const-string v8, "Id"

    invoke-interface {v2, v8}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 1393
    .local v4, "id":Lorg/w3c/dom/Node;
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    if-le v8, v11, :cond_4

    .line 1394
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "TQEExtension:  Id value "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " is greater than 128"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1385
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1398
    :cond_4
    invoke-interface {v6, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    check-cast v8, Lorg/w3c/dom/Element;

    const-string v10, "apIds"

    invoke-interface {v8, v10}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 1399
    .local v1, "apids":Lorg/w3c/dom/NodeList;
    if-nez v1, :cond_5

    .line 1400
    const-string v8, "TQEExtension: missing tag apIds under TQE_Thresholds"

    invoke-static {v8}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    move v8, v9

    .line 1401
    goto/16 :goto_0

    .line 1403
    :cond_5
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v8

    if-nez v8, :cond_6

    .line 1404
    const-string v8, "TQEExtension: Handling tqe thresholds for default apid"

    invoke-static {v8}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1405
    invoke-interface {v6, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const/4 v10, 0x0

    invoke-direct {p0, v8, v4, v10}, Lcom/quicinc/cne/andsf/Extension;->handleTqeThresholds(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)I

    move-result v8

    if-ne v8, v9, :cond_3

    move v8, v9

    .line 1406
    goto/16 :goto_0

    .line 1411
    :cond_6
    invoke-interface {v1, v12}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    check-cast v8, Lorg/w3c/dom/Element;

    invoke-interface {v8}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v8

    const-string v10, "Type"

    invoke-interface {v8, v10}, Lorg/w3c/dom/NamedNodeMap;->getNamedItem(Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object v8

    invoke-interface {v8}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v7

    .line 1412
    .local v7, "type":Ljava/lang/String;
    const-string v8, "SSID"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1414
    invoke-interface {v1, v12}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    check-cast v8, Lorg/w3c/dom/Element;

    const-string v10, "apId"

    invoke-interface {v8, v10}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 1415
    .local v0, "apid":Lorg/w3c/dom/NodeList;
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v8

    if-le v8, v11, :cond_7

    .line 1417
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "TQEExtension: Apid length "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " exceeds max, handling first "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1420
    :cond_7
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_3
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v8

    invoke-static {v8, v11}, Ljava/lang/Math;->min(II)I

    move-result v8

    if-ge v5, v8, :cond_3

    .line 1421
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "TQEExtension: Handling tqe thresholds for apid "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v0, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v10

    invoke-interface {v10}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v10

    invoke-interface {v10}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1423
    invoke-interface {v6, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    invoke-interface {v0, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v10

    invoke-interface {v10}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v10

    invoke-direct {p0, v8, v4, v10}, Lcom/quicinc/cne/andsf/Extension;->handleTqeThresholds(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)I

    move-result v8

    if-ne v8, v9, :cond_8

    move v8, v9

    .line 1425
    goto/16 :goto_0

    .line 1420
    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 1431
    .end local v0    # "apid":Lorg/w3c/dom/NodeList;
    .end local v5    # "j":I
    :cond_9
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "TQEExtension: Apid type "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " is not supported"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1435
    .end local v1    # "apids":Lorg/w3c/dom/NodeList;
    .end local v2    # "attrs":Lorg/w3c/dom/NamedNodeMap;
    .end local v4    # "id":Lorg/w3c/dom/Node;
    .end local v7    # "type":Ljava/lang/String;
    :cond_a
    const/16 v8, 0x3e8

    goto/16 :goto_0
.end method

.method private handleTqeThresholds(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)I
    .locals 7
    .param p1, "thresholdNode"    # Lorg/w3c/dom/Node;
    .param p2, "idNode"    # Lorg/w3c/dom/Node;
    .param p3, "apidNode"    # Lorg/w3c/dom/Node;

    .prologue
    const/4 v4, -0x3

    .line 1440
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleTqeThresholds: thresholdNode "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {p2}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1441
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 1442
    .local v1, "details":Lorg/w3c/dom/NodeList;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "details length "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1443
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-ge v2, v5, :cond_3

    .line 1444
    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 1445
    .local v3, "thresholdTypeNode":Lorg/w3c/dom/Node;
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "BBD_Disabled"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "DBD_Disabled"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "DGIMThresh"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "DBDTputThresh"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "TQETimeWindow"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "RatioThresh"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1453
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Adding new TqeThreshold item "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 1455
    new-instance v0, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;

    invoke-direct {v0, p0, v3, p2, p3}, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;-><init>(Lcom/quicinc/cne/andsf/Extension;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)V

    .line 1456
    .local v0, "ct":Lcom/quicinc/cne/andsf/Extension$TqeThreshold;
    iget-object v5, p0, Lcom/quicinc/cne/andsf/Extension;->tqeThresholds:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1457
    # invokes: Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->parameterValidation()I
    invoke-static {v0}, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->access$500(Lcom/quicinc/cne/andsf/Extension$TqeThreshold;)I

    move-result v5

    if-ne v5, v4, :cond_1

    .line 1458
    const-string v5, "TQE parameter validation failed."

    invoke-static {v5}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 1464
    .end local v0    # "ct":Lcom/quicinc/cne/andsf/Extension$TqeThreshold;
    .end local v3    # "thresholdTypeNode":Lorg/w3c/dom/Node;
    :goto_1
    return v4

    .line 1461
    .restart local v0    # "ct":Lcom/quicinc/cne/andsf/Extension$TqeThreshold;
    .restart local v3    # "thresholdTypeNode":Lorg/w3c/dom/Node;
    :cond_1
    # invokes: Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->addToDatabase()V
    invoke-static {v0}, Lcom/quicinc/cne/andsf/Extension$TqeThreshold;->access$600(Lcom/quicinc/cne/andsf/Extension$TqeThreshold;)V

    .line 1443
    .end local v0    # "ct":Lcom/quicinc/cne/andsf/Extension$TqeThreshold;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 1464
    .end local v3    # "thresholdTypeNode":Lorg/w3c/dom/Node;
    :cond_3
    const/16 v4, 0x3e8

    goto :goto_1
.end method


# virtual methods
.method public assignValues(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1568
    const-string v0, "BQE_Disabled"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1569
    iget-object v0, p0, Lcom/quicinc/cne/andsf/Extension;->data:Landroid/content/ContentValues;

    const-string v1, "BQE_Disabled"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1580
    :cond_0
    :goto_0
    return-void

    .line 1571
    :cond_1
    const-string v0, "ICD_Disabled"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1572
    iget-object v0, p0, Lcom/quicinc/cne/andsf/Extension;->data:Landroid/content/ContentValues;

    const-string v1, "ICD_Disabled"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1574
    :cond_2
    const-string v0, "MaxAuthTime"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1575
    iget-object v0, p0, Lcom/quicinc/cne/andsf/Extension;->data:Landroid/content/ContentValues;

    const-string v1, "MaxAuthTime"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1577
    :cond_3
    const-string v0, "IcdBanRetest"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1578
    iget-object v0, p0, Lcom/quicinc/cne/andsf/Extension;->data:Landroid/content/ContentValues;

    const-string v1, "IcdBanRetest"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public handleExtension(Lorg/w3c/dom/Element;)I
    .locals 2
    .param p1, "baseNode"    # Lorg/w3c/dom/Element;

    .prologue
    const/16 v0, 0x3e8

    .line 65
    invoke-direct {p0, p1}, Lcom/quicinc/cne/andsf/Extension;->handleBQEExtension(Lorg/w3c/dom/Element;)I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-direct {p0, p1}, Lcom/quicinc/cne/andsf/Extension;->handleCQEExtension(Lorg/w3c/dom/Element;)I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-direct {p0, p1}, Lcom/quicinc/cne/andsf/Extension;->handleTQEExtension(Lorg/w3c/dom/Element;)I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-direct {p0, p1}, Lcom/quicinc/cne/andsf/Extension;->handleICDExtension(Lorg/w3c/dom/Element;)I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 69
    const-string v1, "Extension: successfully handle Extension node."

    invoke-static {v1}, Lcom/quicinc/cne/andsf/AndsfParser;->dlogd(Ljava/lang/String;)V

    .line 74
    :goto_0
    return v0

    .line 73
    :cond_0
    const-string v0, "Extension: error in handle Extension node."

    invoke-static {v0}, Lcom/quicinc/cne/andsf/AndsfParser;->loge(Ljava/lang/String;)V

    .line 74
    const/4 v0, -0x3

    goto :goto_0
.end method
