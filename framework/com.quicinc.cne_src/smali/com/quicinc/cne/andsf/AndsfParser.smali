.class public Lcom/quicinc/cne/andsf/AndsfParser;
.super Ljava/lang/Object;
.source "AndsfParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;,
        Lcom/quicinc/cne/andsf/AndsfParser$Version;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field static final SUB_TYPE:Ljava/lang/String; = "PLCY:ANDSF"

.field private static final TAG_ANDSF:Ljava/lang/String; = "Andsf"

.field private static final TAG_ROOT:Ljava/lang/String; = "AndsfPolicy"

.field private static final TAG_VERSION:Ljava/lang/String; = "Version"


# instance fields
.field andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

.field andsfNode:Lorg/w3c/dom/Element;

.field mContext:Landroid/content/Context;

.field mDb:Lcom/quicinc/cne/andsf/DbConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object v0, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    .line 89
    iput-object v0, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfNode:Lorg/w3c/dom/Element;

    .line 95
    iput-object p1, p0, Lcom/quicinc/cne/andsf/AndsfParser;->mContext:Landroid/content/Context;

    .line 96
    return-void
.end method

.method public static dlogd(Ljava/lang/String;)V
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 353
    const-string v0, "PLCY:ANDSF"

    invoke-static {v0, p0}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    return-void
.end method

.method public static loge(Ljava/lang/String;)V
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 357
    const-string v0, "PLCY:ANDSF"

    invoke-static {v0, p0}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    return-void
.end method

.method private parseAndsfConfig(Lorg/w3c/dom/Element;)I
    .locals 11
    .param p1, "elem"    # Lorg/w3c/dom/Element;

    .prologue
    const/4 v10, 0x1

    const/4 v5, -0x3

    .line 288
    iget-object v4, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    invoke-static {}, Lcom/quicinc/cne/andsf/AndsfParser$Version;->getString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;->version:Ljava/lang/String;

    .line 289
    const-string v4, "PLCY:ANDSF"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "version= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    iget-object v7, v7, Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;->version:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    iget-object v4, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    iput-wide v6, v4, Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;->time:J

    .line 292
    const-string v4, "PLCY:ANDSF"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "time= "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    iget-wide v8, v7, Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;->time:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    iget-object v4, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    iget-object v4, v4, Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;->data:Landroid/content/ContentValues;

    const-string v6, "Version"

    iget-object v7, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    iget-object v7, v7, Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;->version:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    iget-object v4, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    iget-object v4, v4, Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;->data:Landroid/content/ContentValues;

    const-string v6, "Timestamp"

    iget-object v7, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    iget-wide v8, v7, Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;->time:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 299
    const-string v4, "ISRP"

    invoke-interface {p1, v4}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 300
    .local v3, "isrpNodes":Lorg/w3c/dom/NodeList;
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    if-ge v4, v10, :cond_2

    .line 301
    const-string v4, "PLCY:ANDSF"

    const-string v6, "No IRSP nodes in ANDSF file"

    invoke-static {v4, v6}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    :cond_0
    const-string v4, "Ext"

    invoke-interface {p1, v4}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 313
    .local v0, "extensionNodes":Lorg/w3c/dom/NodeList;
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    if-ge v4, v10, :cond_3

    .line 314
    :cond_1
    const-string v4, "PLCY:ANDSF"

    const-string v6, "No Ext node in ANDSF file"

    invoke-static {v4, v6}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .line 331
    :goto_0
    return v4

    .line 304
    .end local v0    # "extensionNodes":Lorg/w3c/dom/NodeList;
    :cond_2
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 305
    new-instance v2, Lcom/quicinc/cne/andsf/IsrpDetails;

    invoke-interface {v3, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    check-cast v4, Lorg/w3c/dom/Element;

    invoke-direct {v2, v4}, Lcom/quicinc/cne/andsf/IsrpDetails;-><init>(Lorg/w3c/dom/Element;)V

    .line 307
    .local v2, "isrpDetails":Lcom/quicinc/cne/andsf/IsrpDetails;
    iget-object v4, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    iget-object v4, v4, Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;->isrps:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 318
    .end local v1    # "i":I
    .end local v2    # "isrpDetails":Lcom/quicinc/cne/andsf/IsrpDetails;
    .restart local v0    # "extensionNodes":Lorg/w3c/dom/NodeList;
    :cond_3
    iget-object v4, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    new-instance v6, Lcom/quicinc/cne/andsf/Extension;

    invoke-direct {v6}, Lcom/quicinc/cne/andsf/Extension;-><init>()V

    iput-object v6, v4, Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;->extension:Lcom/quicinc/cne/andsf/Extension;

    .line 319
    iget-object v4, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    iget-object v4, v4, Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;->extension:Lcom/quicinc/cne/andsf/Extension;

    if-nez v4, :cond_4

    .line 320
    const-string v4, "PLCY:ANDSF"

    const-string v6, "Failed to create Extension"

    invoke-static {v4, v6}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .line 321
    goto :goto_0

    .line 323
    :cond_4
    iget-object v4, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    iget-object v6, v4, Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;->extension:Lcom/quicinc/cne/andsf/Extension;

    const/4 v4, 0x0

    invoke-interface {v0, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    check-cast v4, Lorg/w3c/dom/Element;

    invoke-virtual {v6, v4}, Lcom/quicinc/cne/andsf/Extension;->handleExtension(Lorg/w3c/dom/Element;)I

    move-result v4

    if-ne v4, v5, :cond_5

    .line 325
    const-string v4, "PLCY:ANDSF"

    const-string v6, "Invalid args when parsing xml file Extension nodes."

    invoke-static {v4, v6}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v5

    .line 327
    goto :goto_0

    .line 331
    :cond_5
    invoke-direct {p0}, Lcom/quicinc/cne/andsf/AndsfParser;->updateAndsfDb()I

    move-result v4

    goto :goto_0
.end method

.method private parseFile(Ljava/lang/String;)I
    .locals 14
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v13, 0x1

    const/4 v10, -0x3

    .line 214
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v2

    .line 215
    .local v2, "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v1

    .line 216
    .local v1, "db":Ljavax/xml/parsers/DocumentBuilder;
    new-instance v9, Ljava/io/File;

    invoke-direct {v9, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v3

    .line 218
    .local v3, "doc":Lorg/w3c/dom/Document;
    if-nez v3, :cond_1

    .line 219
    const-string v9, "PLCY:ANDSF"

    const-string v11, "Malformed ANDSF xml file"

    invoke-static {v9, v11}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v10

    .line 278
    .end local v1    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v2    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v3    # "doc":Lorg/w3c/dom/Document;
    :cond_0
    :goto_0
    return v6

    .line 224
    .restart local v1    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v2    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v3    # "doc":Lorg/w3c/dom/Document;
    :cond_1
    invoke-direct {p0, v3}, Lcom/quicinc/cne/andsf/AndsfParser;->validatePolicyTree(Lorg/w3c/dom/Document;)I

    move-result v6

    .line 225
    .local v6, "ret":I
    const/16 v9, 0x3e8

    if-ne v9, v6, :cond_0

    .line 229
    const-string v9, "Andsf"

    invoke-interface {v3, v9}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 230
    .local v0, "andsfNodes":Lorg/w3c/dom/NodeList;
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    if-lt v9, v13, :cond_5

    .line 232
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    if-ge v5, v9, :cond_4

    .line 234
    invoke-interface {v0, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v9

    check-cast v9, Lorg/w3c/dom/Element;

    iput-object v9, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfNode:Lorg/w3c/dom/Element;

    .line 235
    iget-object v9, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfNode:Lorg/w3c/dom/Element;

    const-string v11, "Version"

    invoke-interface {v9, v11}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v8

    .line 236
    .local v8, "versionNode":Lorg/w3c/dom/NodeList;
    invoke-interface {v8}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    if-ne v9, v13, :cond_3

    .line 238
    const/4 v9, 0x0

    invoke-interface {v8, v9}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v9

    invoke-interface {v9}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v9

    invoke-interface {v9}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v7

    .line 244
    .local v7, "version":Ljava/lang/String;
    invoke-static {v7}, Lcom/quicinc/cne/andsf/AndsfParser$Version;->matchVersion(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 246
    const-string v9, "PLCY:ANDSF"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "parsing Andsf node "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", version "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lcom/quicinc/cne/andsf/AndsfParser$Version;->getString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget-object v9, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfNode:Lorg/w3c/dom/Element;

    invoke-direct {p0, v9}, Lcom/quicinc/cne/andsf/AndsfParser;->parseAndsfConfig(Lorg/w3c/dom/Element;)I

    move-result v6

    goto :goto_0

    .line 253
    :cond_2
    const-string v9, "PLCY:ANDSF"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Version "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " from Andsf node "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " does not match software version "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lcom/quicinc/cne/andsf/AndsfParser$Version;->getString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    .end local v7    # "version":Ljava/lang/String;
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_1

    .line 262
    :cond_3
    const-string v9, "PLCY:ANDSF"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "No/Many version tag from Andsf node "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 275
    .end local v0    # "andsfNodes":Lorg/w3c/dom/NodeList;
    .end local v1    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v2    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v3    # "doc":Lorg/w3c/dom/Document;
    .end local v5    # "i":I
    .end local v6    # "ret":I
    .end local v8    # "versionNode":Lorg/w3c/dom/NodeList;
    :catch_0
    move-exception v4

    .line 276
    .local v4, "e":Ljava/lang/Exception;
    const-string v9, "PLCY:ANDSF"

    const-string v11, "ANDSF Parser failed"

    invoke-static {v9, v11}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    move v6, v10

    .line 278
    goto/16 :goto_0

    .line 266
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v0    # "andsfNodes":Lorg/w3c/dom/NodeList;
    .restart local v1    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v2    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v3    # "doc":Lorg/w3c/dom/Document;
    .restart local v5    # "i":I
    .restart local v6    # "ret":I
    :cond_4
    :try_start_1
    const-string v9, "PLCY:ANDSF"

    const-string v11, "No version match from any Andsf node. Configuration not parsed."

    invoke-static {v9, v11}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const/4 v6, -0x8

    goto/16 :goto_0

    .line 272
    .end local v5    # "i":I
    :cond_5
    const-string v9, "PLCY:ANDSF"

    const-string v11, "No Andsf tag"

    invoke-static {v9, v11}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move v6, v10

    .line 273
    goto/16 :goto_0
.end method

.method private updateAndsfDb()I
    .locals 5

    .prologue
    .line 337
    :try_start_0
    new-instance v2, Lcom/quicinc/cne/andsf/DbConnection;

    iget-object v3, p0, Lcom/quicinc/cne/andsf/AndsfParser;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/quicinc/cne/andsf/DbConnection;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/quicinc/cne/andsf/AndsfParser;->mDb:Lcom/quicinc/cne/andsf/DbConnection;

    .line 338
    iget-object v2, p0, Lcom/quicinc/cne/andsf/AndsfParser;->mDb:Lcom/quicinc/cne/andsf/DbConnection;

    invoke-virtual {v2}, Lcom/quicinc/cne/andsf/DbConnection;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 340
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iget-object v2, p0, Lcom/quicinc/cne/andsf/AndsfParser;->mDb:Lcom/quicinc/cne/andsf/DbConnection;

    iget-object v3, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    invoke-virtual {v2, v0, v3}, Lcom/quicinc/cne/andsf/DbConnection;->commitAndsf(Landroid/database/sqlite/SQLiteDatabase;Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;)V

    .line 341
    iget-object v2, p0, Lcom/quicinc/cne/andsf/AndsfParser;->mDb:Lcom/quicinc/cne/andsf/DbConnection;

    invoke-virtual {v2}, Lcom/quicinc/cne/andsf/DbConnection;->close()V

    .line 342
    iget-object v2, p0, Lcom/quicinc/cne/andsf/AndsfParser;->mDb:Lcom/quicinc/cne/andsf/DbConnection;

    const-string v3, "/data/connectivity/"

    invoke-virtual {v2, v3}, Lcom/quicinc/cne/andsf/DbConnection;->copyDatabase(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    const/16 v2, 0x3e8

    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :goto_0
    return v2

    .line 343
    :catch_0
    move-exception v1

    .line 344
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "PLCY:ANDSF"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Update ANDSF db failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    const/4 v2, -0x1

    goto :goto_0
.end method

.method private updateDefaultConfigFile(Ljava/lang/String;)V
    .locals 12
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 123
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v2

    .line 124
    .local v2, "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Ljavax/xml/parsers/DocumentBuilderFactory;->setValidating(Z)V

    .line 125
    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v1

    .line 126
    .local v1, "db":Ljavax/xml/parsers/DocumentBuilder;
    new-instance v9, Ljava/io/File;

    const-string v10, "system/etc/cne/andsfCne.xml"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v3

    .line 127
    .local v3, "doc":Lorg/w3c/dom/Document;
    invoke-interface {v3}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v6

    .line 129
    .local v6, "root":Lorg/w3c/dom/Element;
    iget-object v9, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfNode:Lorg/w3c/dom/Element;

    if-eqz v9, :cond_1

    .line 132
    :goto_0
    invoke-interface {v6}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 133
    invoke-interface {v6}, Lorg/w3c/dom/Element;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v9

    invoke-interface {v6, v9}, Lorg/w3c/dom/Element;->removeChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/xml/transform/TransformerConfigurationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/xml/transform/TransformerException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 149
    .end local v1    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v2    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v3    # "doc":Lorg/w3c/dom/Document;
    .end local v6    # "root":Lorg/w3c/dom/Element;
    :catch_0
    move-exception v5

    .line 150
    .local v5, "e":Ljavax/xml/parsers/ParserConfigurationException;
    const-string v9, "PLCY:ANDSF"

    const-string v10, "ParserConfigurationException..."

    invoke-static {v9, v10}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    invoke-virtual {v5}, Ljavax/xml/parsers/ParserConfigurationException;->printStackTrace()V

    .line 165
    .end local v5    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    :goto_1
    return-void

    .line 136
    .restart local v1    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v2    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v3    # "doc":Lorg/w3c/dom/Document;
    .restart local v6    # "root":Lorg/w3c/dom/Element;
    :cond_0
    :try_start_1
    iget-object v9, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfNode:Lorg/w3c/dom/Element;

    const/4 v10, 0x1

    invoke-interface {v3, v9, v10}, Lorg/w3c/dom/Document;->importNode(Lorg/w3c/dom/Node;Z)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 137
    .local v0, "copied":Lorg/w3c/dom/Node;
    invoke-interface {v6, v0}, Lorg/w3c/dom/Element;->appendChild(Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    .line 139
    invoke-static {}, Ljavax/xml/transform/TransformerFactory;->newInstance()Ljavax/xml/transform/TransformerFactory;

    move-result-object v9

    invoke-virtual {v9}, Ljavax/xml/transform/TransformerFactory;->newTransformer()Ljavax/xml/transform/Transformer;

    move-result-object v8

    .line 140
    .local v8, "tf":Ljavax/xml/transform/Transformer;
    new-instance v4, Ljavax/xml/transform/dom/DOMSource;

    invoke-direct {v4, v3}, Ljavax/xml/transform/dom/DOMSource;-><init>(Lorg/w3c/dom/Node;)V

    .line 141
    .local v4, "ds":Ljavax/xml/transform/dom/DOMSource;
    new-instance v7, Ljavax/xml/transform/stream/StreamResult;

    const-string v9, "data/connectivity/andsfCne.xml"

    invoke-direct {v7, v9}, Ljavax/xml/transform/stream/StreamResult;-><init>(Ljava/lang/String;)V

    .line 142
    .local v7, "sr":Ljavax/xml/transform/stream/StreamResult;
    invoke-virtual {v8, v4, v7}, Ljavax/xml/transform/Transformer;->transform(Ljavax/xml/transform/Source;Ljavax/xml/transform/Result;)V

    .line 143
    const-string v9, "PLCY:ANDSF"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Restore "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " to "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "data/connectivity/andsfCne.xml"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljavax/xml/transform/TransformerConfigurationException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljavax/xml/transform/TransformerException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_1

    .line 152
    .end local v0    # "copied":Lorg/w3c/dom/Node;
    .end local v1    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v2    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v3    # "doc":Lorg/w3c/dom/Document;
    .end local v4    # "ds":Ljavax/xml/transform/dom/DOMSource;
    .end local v6    # "root":Lorg/w3c/dom/Element;
    .end local v7    # "sr":Ljavax/xml/transform/stream/StreamResult;
    .end local v8    # "tf":Ljavax/xml/transform/Transformer;
    :catch_1
    move-exception v5

    .line 153
    .local v5, "e":Lorg/xml/sax/SAXException;
    const-string v9, "PLCY:ANDSF"

    const-string v10, "SAXException..."

    invoke-static {v9, v10}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-virtual {v5}, Lorg/xml/sax/SAXException;->printStackTrace()V

    goto :goto_1

    .line 147
    .end local v5    # "e":Lorg/xml/sax/SAXException;
    .restart local v1    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .restart local v2    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .restart local v3    # "doc":Lorg/w3c/dom/Document;
    .restart local v6    # "root":Lorg/w3c/dom/Element;
    :cond_1
    :try_start_2
    const-string v9, "PLCY:ANDSF"

    const-string v10, "andsfNode is null. Not restoring default file"

    invoke-static {v9, v10}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljavax/xml/transform/TransformerConfigurationException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljavax/xml/transform/TransformerException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_1

    .line 155
    .end local v1    # "db":Ljavax/xml/parsers/DocumentBuilder;
    .end local v2    # "dbf":Ljavax/xml/parsers/DocumentBuilderFactory;
    .end local v3    # "doc":Lorg/w3c/dom/Document;
    .end local v6    # "root":Lorg/w3c/dom/Element;
    :catch_2
    move-exception v5

    .line 156
    .local v5, "e":Ljavax/xml/transform/TransformerConfigurationException;
    const-string v9, "PLCY:ANDSF"

    const-string v10, "TransformerConfigurationException..."

    invoke-static {v9, v10}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    invoke-virtual {v5}, Ljavax/xml/transform/TransformerConfigurationException;->printStackTrace()V

    goto :goto_1

    .line 158
    .end local v5    # "e":Ljavax/xml/transform/TransformerConfigurationException;
    :catch_3
    move-exception v5

    .line 159
    .local v5, "e":Ljavax/xml/transform/TransformerException;
    const-string v9, "PLCY:ANDSF"

    const-string v10, "TransformerException..."

    invoke-static {v9, v10}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {v5}, Ljavax/xml/transform/TransformerException;->printStackTrace()V

    goto :goto_1

    .line 161
    .end local v5    # "e":Ljavax/xml/transform/TransformerException;
    :catch_4
    move-exception v5

    .line 162
    .local v5, "e":Ljava/lang/Exception;
    const-string v9, "PLCY:ANDSF"

    const-string v10, "updateDefaultConfigFile failed"

    invoke-static {v9, v10}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private validatePolicyTree(Lorg/w3c/dom/Document;)I
    .locals 6
    .param p1, "doc"    # Lorg/w3c/dom/Document;

    .prologue
    const/4 v3, -0x3

    .line 173
    invoke-interface {p1}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v2

    .line 175
    .local v2, "root":Lorg/w3c/dom/Element;
    if-eqz v2, :cond_1

    const-string v4, "AndsfPolicy"

    invoke-interface {v2}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 177
    const-string v4, "Andsf"

    invoke-interface {v2, v4}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    .line 178
    .local v0, "andsf":Lorg/w3c/dom/Element;
    if-eqz v0, :cond_0

    .line 180
    const-string v4, "Version"

    invoke-interface {v0, v4}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 181
    .local v1, "list":Lorg/w3c/dom/NodeList;
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_2

    .line 182
    const-string v4, "PLCY:ANDSF"

    const-string v5, "There must be only one version node <Version> as a child of ANDSF node"

    invoke-static {v4, v5}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    .end local v0    # "andsf":Lorg/w3c/dom/Element;
    .end local v1    # "list":Lorg/w3c/dom/NodeList;
    :goto_0
    return v3

    .line 189
    .restart local v0    # "andsf":Lorg/w3c/dom/Element;
    :cond_0
    const-string v4, "PLCY:ANDSF"

    const-string v5, "ANDSF node must be <Andsf> as a child of <AndsfPolicy>"

    invoke-static {v4, v5}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 195
    .end local v0    # "andsf":Lorg/w3c/dom/Element;
    :cond_1
    const-string v4, "PLCY:ANDSF"

    const-string v5, "root node must be <AndsfPolicy>"

    invoke-static {v4, v5}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 198
    .restart local v0    # "andsf":Lorg/w3c/dom/Element;
    .restart local v1    # "list":Lorg/w3c/dom/NodeList;
    :cond_2
    const/16 v3, 0x3e8

    goto :goto_0
.end method


# virtual methods
.method public updateAndsf(Ljava/lang/String;)I
    .locals 4
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 103
    const-string v1, "PLCY:ANDSF"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Starting ANDSF parser, version: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/quicinc/cne/andsf/AndsfParser$Version;->getString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const/4 v0, -0x1

    .line 106
    .local v0, "retVal":I
    new-instance v1, Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    invoke-direct {v1, p0}, Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;-><init>(Lcom/quicinc/cne/andsf/AndsfParser;)V

    iput-object v1, p0, Lcom/quicinc/cne/andsf/AndsfParser;->andsfData:Lcom/quicinc/cne/andsf/AndsfParser$AndsfData;

    .line 107
    invoke-direct {p0, p1}, Lcom/quicinc/cne/andsf/AndsfParser;->parseFile(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_0

    .line 108
    const-string v1, "PLCY:ANDSF"

    const-string v2, "Finished parsing ANDSF file"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v1, "data/connectivity/andsfCne.xml"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 110
    invoke-direct {p0, p1}, Lcom/quicinc/cne/andsf/AndsfParser;->updateDefaultConfigFile(Ljava/lang/String;)V

    .line 113
    :cond_0
    return v0
.end method
