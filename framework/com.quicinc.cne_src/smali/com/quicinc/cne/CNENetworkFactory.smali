.class public Lcom/quicinc/cne/CNENetworkFactory;
.super Landroid/net/NetworkFactory;
.source "CNENetworkFactory.java"


# static fields
.field private static final DBG:Z = true

.field private static final EVENT_DEFAULT_NETWORK_SWITCH:I = 0x83ffe

.field private static final SUB_TYPE:Ljava/lang/String; = "CORE"


# instance fields
.field private mMobileScore:I

.field private mMobileScoreChanged:Z

.field private mWifiScore:I

.field private mWifiScoreChanged:Z


# direct methods
.method public constructor <init>(Landroid/os/Looper;Landroid/content/Context;Ljava/lang/String;Landroid/net/NetworkCapabilities;)V
    .locals 2
    .param p1, "l"    # Landroid/os/Looper;
    .param p2, "c"    # Landroid/content/Context;
    .param p3, "TAG"    # Ljava/lang/String;
    .param p4, "nc"    # Landroid/net/NetworkCapabilities;

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 29
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/net/NetworkFactory;-><init>(Landroid/os/Looper;Landroid/content/Context;Ljava/lang/String;Landroid/net/NetworkCapabilities;)V

    .line 30
    iput v0, p0, Lcom/quicinc/cne/CNENetworkFactory;->mWifiScore:I

    .line 31
    iput v0, p0, Lcom/quicinc/cne/CNENetworkFactory;->mMobileScore:I

    .line 32
    iput-boolean v1, p0, Lcom/quicinc/cne/CNENetworkFactory;->mWifiScoreChanged:Z

    .line 33
    iput-boolean v1, p0, Lcom/quicinc/cne/CNENetworkFactory;->mMobileScoreChanged:Z

    .line 34
    return-void
.end method

.method private static dlogd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 91
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method private static dloge(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 107
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    return-void
.end method

.method private static dlogi(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 99
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method private static dlogv(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 95
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    return-void
.end method

.method private static dlogw(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 103
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method private static logd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 71
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method private static loge(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    return-void
.end method

.method private static logi(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 79
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method private static logv(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method private static logw(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 83
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method private switchNetwork()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59
    iget v0, p0, Lcom/quicinc/cne/CNENetworkFactory;->mWifiScore:I

    if-lez v0, :cond_2

    iget v0, p0, Lcom/quicinc/cne/CNENetworkFactory;->mMobileScore:I

    if-lez v0, :cond_2

    iget-boolean v0, p0, Lcom/quicinc/cne/CNENetworkFactory;->mWifiScoreChanged:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/quicinc/cne/CNENetworkFactory;->mMobileScoreChanged:Z

    if-eqz v0, :cond_2

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/quicinc/cne/CNENetworkFactory;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    if-eqz v0, :cond_3

    .line 61
    iget-object v0, p0, Lcom/quicinc/cne/CNENetworkFactory;->mAsyncChannel:Lcom/android/internal/util/AsyncChannel;

    const v1, 0x83ffe

    invoke-virtual {v0, v1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(I)V

    .line 65
    :goto_0
    iget-boolean v0, p0, Lcom/quicinc/cne/CNENetworkFactory;->mWifiScoreChanged:Z

    if-eqz v0, :cond_1

    iput-boolean v2, p0, Lcom/quicinc/cne/CNENetworkFactory;->mWifiScoreChanged:Z

    .line 66
    :cond_1
    iget-boolean v0, p0, Lcom/quicinc/cne/CNENetworkFactory;->mMobileScoreChanged:Z

    if-eqz v0, :cond_2

    iput-boolean v2, p0, Lcom/quicinc/cne/CNENetworkFactory;->mMobileScoreChanged:Z

    .line 68
    :cond_2
    return-void

    .line 63
    :cond_3
    const-string v0, "CORE"

    const-string v1, "Async channel is null"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CNENetworkFactory;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public acceptRequest(Landroid/net/NetworkRequest;I)Z
    .locals 6
    .param p1, "nr"    # Landroid/net/NetworkRequest;
    .param p2, "score"    # I

    .prologue
    const/16 v5, 0xc

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 38
    const-string v0, "CORE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nr: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", score: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CNENetworkFactory;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    iget-object v0, p1, Landroid/net/NetworkRequest;->networkCapabilities:Landroid/net/NetworkCapabilities;

    invoke-virtual {v0, v3}, Landroid/net/NetworkCapabilities;->hasTransport(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/net/NetworkRequest;->networkCapabilities:Landroid/net/NetworkCapabilities;

    invoke-virtual {v0, v5}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    if-lez p2, :cond_0

    iget v0, p0, Lcom/quicinc/cne/CNENetworkFactory;->mWifiScore:I

    if-eq v0, p2, :cond_0

    .line 42
    iput p2, p0, Lcom/quicinc/cne/CNENetworkFactory;->mWifiScore:I

    .line 43
    iput-boolean v3, p0, Lcom/quicinc/cne/CNENetworkFactory;->mWifiScoreChanged:Z

    .line 46
    :cond_0
    iget-object v0, p1, Landroid/net/NetworkRequest;->networkCapabilities:Landroid/net/NetworkCapabilities;

    invoke-virtual {v0, v4}, Landroid/net/NetworkCapabilities;->hasTransport(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Landroid/net/NetworkRequest;->networkCapabilities:Landroid/net/NetworkCapabilities;

    invoke-virtual {v0, v5}, Landroid/net/NetworkCapabilities;->hasCapability(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    if-lez p2, :cond_1

    iget v0, p0, Lcom/quicinc/cne/CNENetworkFactory;->mMobileScore:I

    if-eq v0, p2, :cond_1

    .line 49
    iput p2, p0, Lcom/quicinc/cne/CNENetworkFactory;->mMobileScore:I

    .line 50
    iput-boolean v3, p0, Lcom/quicinc/cne/CNENetworkFactory;->mWifiScoreChanged:Z

    .line 53
    :cond_1
    const-string v0, "CORE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wifi score: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/quicinc/cne/CNENetworkFactory;->mWifiScore:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mobile score: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/quicinc/cne/CNENetworkFactory;->mMobileScore:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CNENetworkFactory;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Lcom/quicinc/cne/CNENetworkFactory;->switchNetwork()V

    .line 55
    return v4
.end method
