.class public final Lcom/quicinc/cne/CNE;
.super Lcom/quicinc/cne/ICNEManager$Stub;
.source "CNE.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quicinc/cne/CNE$5;,
        Lcom/quicinc/cne/CNE$PackageListener;,
        Lcom/quicinc/cne/CNE$WriteBrowserPkgApp;,
        Lcom/quicinc/cne/CNE$CNENetworkFactoryHandler;,
        Lcom/quicinc/cne/CNE$CNEReceiver;,
        Lcom/quicinc/cne/CNE$CNESender;,
        Lcom/quicinc/cne/CNE$IdGenerator;,
        Lcom/quicinc/cne/CNE$CneWwanInfo;,
        Lcom/quicinc/cne/CNE$CneWifiInfo;,
        Lcom/quicinc/cne/CNE$CneRatInfo;,
        Lcom/quicinc/cne/CNE$FeatureType;
    }
.end annotation


# static fields
.field private static final CLAT_INTERFACE_NAME:Ljava/lang/String; = "clat4"

.field public static final CND_RET_CODE_INVALID_DATA:I = -0x2

.field public static final CND_RET_CODE_OK:I = 0x0

.field public static final CND_RET_CODE_UNKNOWN_ERROR:I = -0x1

.field public static final CNE_EXTRA_TETHERED_IFACE:Ljava/lang/String; = "tetheredClientIface"

.field public static final CNE_EXTRA_UPSTREAM_IFACE:Ljava/lang/String; = "tetheringUpstreamIface"

.field public static final CNE_EXTRA_UPSTREAM_INFO_DEFAULT:I = -0x1

.field public static final CNE_EXTRA_UPSTREAM_IP_TYPE:Ljava/lang/String; = "tetheringUpstreamIpType"

.field public static final CNE_EXTRA_UPSTREAM_UPDATE_TYPE:Ljava/lang/String; = "tetheringUpstreamUpdateType"

.field private static final CNE_FEATURE_OFF:I = 0x1

.field private static final CNE_FEATURE_ON:I = 0x2

.field static final CNE_MAX_COMMAND_BYTES:I = 0x2000

.field static final CNE_NET_SUBTYPE_WLAN_B:I = 0x14

.field static final CNE_NET_SUBTYPE_WLAN_G:I = 0x15

.field static final CNE_NOTIFY_ACCESS_ALLOWED:I = 0x19

.field static final CNE_NOTIFY_ACCESS_DENIED:I = 0x15

.field static final CNE_NOTIFY_ANDSF_DATA_READY:I = 0x1c

.field static final CNE_NOTIFY_APP_INFO_LIST_CMD:I = 0x17

.field static final CNE_NOTIFY_BQE_POST_RESULT:I = 0x1a

.field static final CNE_NOTIFY_BROWSERS_INFO_LIST_CMD:I = 0x1d

.field static final CNE_NOTIFY_DEFAULT_NW_PREF:I = 0xb

.field static final CNE_NOTIFY_DISALLOWED_AP_MSG:I = 0xe

.field static final CNE_NOTIFY_DNS_PRIORITY_CMD:I = 0x13

.field static final CNE_NOTIFY_FEATURE_STATUS:I = 0x1a

.field static final CNE_NOTIFY_FMC_STATUS:I = 0xb

.field static final CNE_NOTIFY_FWMARK_INFO:I = 0x24

.field static final CNE_NOTIFY_GET_PARENT_APP_RESULT:I = 0x21

.field static final CNE_NOTIFY_HOST_ROUTING_IP:I = 0xc

.field static final CNE_NOTIFY_ICD_HTTP_RESULT:I = 0x1b

.field static final CNE_NOTIFY_ICD_RESULT:I = 0x15

.field static final CNE_NOTIFY_INFLIGHT_STATUS:I = 0xa

.field static final CNE_NOTIFY_JRTT_RESULT:I = 0x19

.field static final CNE_NOTIFY_MORE_PREFERED_RAT_AVAIL:I = 0x7

.field static final CNE_NOTIFY_POLICY_UPDATE_DONE:I = 0x1c

.field static final CNE_NOTIFY_RAT_CONNECT_STATUS:I = 0xa

.field static final CNE_NOTIFY_RAT_LOST:I = 0x8

.field static final CNE_NOTIFY_SCREEN_STATE_CMD:I = 0x22

.field static final CNE_NOTIFY_SENSOR_EVENT_CMD:I = 0xd

.field static final CNE_NOTIFY_SOCKET_CLOSED_CMD:I = 0x14

.field static final CNE_NOTIFY_TETHERING_UPSTREAM_INFO_CMD:I = 0x23

.field static final CNE_NOTIFY_TIMER_EXPIRED_CMD:I = 0xf

.field static final CNE_NOTIFY_VENDOR_MSG:I = 0xd

.field static final CNE_NOTIFY_WLAN_CONNECTIVITY_UP:I = 0x18

.field public static final CNE_PREFERENCE_CHANGED_ACTION:Ljava/lang/String; = "com.quicinc.cne.CNE_PREFERENCE_CHANGED"

.field static final CNE_RAT_ANY:I = 0x2

.field static final CNE_RAT_INVALID:I = 0x4

.field static final CNE_RAT_MAX:I = 0x4

.field static final CNE_RAT_MIN:I = 0x0

.field static final CNE_RAT_NONE:I = 0x3

.field static final CNE_RAT_WLAN:I = 0x1

.field static final CNE_RAT_WWAN:I = 0x0

.field static final CNE_REQUEST_BRING_RAT_DOWN:I = 0x5

.field static final CNE_REQUEST_BRING_RAT_UP:I = 0x6

.field static final CNE_REQUEST_CONFIG_IPROUTE2_CMD:I = 0xe

.field static final CNE_REQUEST_CONF_NW:I = 0x4

.field static final CNE_REQUEST_DEREG_ROLE:I = 0x5

.field static final CNE_REQUEST_GET_APP_INFO_LIST:I = 0x12

.field static final CNE_REQUEST_GET_BROWSERS_INFO_LIST:I = 0x18

.field static final CNE_REQUEST_GET_COMPATIBLE_NWS:I = 0x3

.field static final CNE_REQUEST_GET_PARENT_APP:I = 0x1d

.field static final CNE_REQUEST_INIT:I = 0x1

.field static final CNE_REQUEST_POST_BQE_RESULTS:I = 0x16

.field static final CNE_REQUEST_REG_NOTIFICATIONS:I = 0x6

.field static final CNE_REQUEST_REG_ROLE:I = 0x2

.field static final CNE_REQUEST_SET_DEFAULT_ROUTE_MSG:I = 0x10

.field static final CNE_REQUEST_START_ACTIVE_BQE:I = 0xf

.field static final CNE_REQUEST_START_FMC_CMD:I = 0x10

.field static final CNE_REQUEST_START_ICD:I = 0x11

.field static final CNE_REQUEST_START_SCAN_WLAN:I = 0x9

.field static final CNE_REQUEST_STOP_ACTIVE_BQE:I = 0x14

.field static final CNE_REQUEST_STOP_FMC_CMD:I = 0x11

.field static final CNE_REQUEST_UPDATE_BATTERY_INFO:I = 0x7

.field static final CNE_REQUEST_UPDATE_DEFAULT_NETWORK_INFO:I = 0x13

.field static final CNE_REQUEST_UPDATE_WLAN_INFO:I = 0x8

.field static final CNE_REQUEST_UPDATE_WLAN_SCAN_RESULTS:I = 0xc

.field static final CNE_REQUEST_UPDATE_WWAN_DORMANCY_INFO:I = 0x12

.field static final CNE_REQUEST_UPDATE_WWAN_INFO:I = 0x9

.field static final CNE_REQ_GET_FEATURE_STATUS:I = 0x1e

.field static final CNE_REQ_SET_FEATURE_PREF:I = 0x1f

.field static final CNE_RESPONSE_CONFIRM_NW:I = 0x3

.field static final CNE_RESPONSE_DEREG_ROLE:I = 0x4

.field static final CNE_RESPONSE_GET_BEST_NW:I = 0x2

.field static final CNE_RESPONSE_REG_ROLE:I = 0x1

.field static final CNE_RESP_SET_FEATURE_PREF:I = 0x1b

.field public static final CNE_RET_BUSY:I = -0x2

.field public static final CNE_RET_ERROR:I = -0x1

.field public static final CNE_RET_FEATURE_UNSUPPORTED:I = -0x4

.field public static final CNE_RET_FILE_SIZE_TOO_LARGE:I = -0x5

.field public static final CNE_RET_INVALID_ARGS:I = -0x3

.field public static final CNE_RET_INVALID_VERSION:I = -0x8

.field public static final CNE_RET_PATH_ACCESS_DENIED:I = -0x6

.field public static final CNE_RET_PATH_NAME_TOO_LONG:I = -0x7

.field public static final CNE_RET_SUCCESS:I = 0x3e8

.field static final CNE_ROLE_INVALID:I = -0x1

.field public static final CNE_SCREEN_STATE_EVT:I = 0x1

.field static final CNE_SWIM_RSSI_POLL_PERIOD:I = 0x1388

.field public static final CNE_UPSTREAM_IFACE_CHANGED_ACTION:Ljava/lang/String; = "com.android.server.connectivity.UPSTREAM_IFACE_CHANGED"

.field private static final DBG:Z = true

.field private static final EVENT_DEFAULT_NETWORK_SWITCH:I = 0x83ffe

.field public static final EVENT_POSTCNDINIT:I = 0x3

.field public static final EVENT_SEND:I = 0x1

.field public static final EVENT_TOGGLE_WIFI:I = 0x2

.field public static final EXTRA_FEATURE_ID:Ljava/lang/String; = "cneFeatureId"

.field public static final EXTRA_FEATURE_PARAMETER:Ljava/lang/String; = "cneFeatureParameter"

.field private static final EXTRA_IS_DEFAULT:Ljava/lang/String; = "isDefault"

.field private static final EXTRA_NETID:Ljava/lang/String; = "netID"

.field public static final EXTRA_NETWORK_TYPE:Ljava/lang/String; = "netType"

.field public static final EXTRA_PARAMETER_VALUE:Ljava/lang/String; = "cneParameterValue"

.field public static final IWLAN_FEATURE:I = 0x2

.field public static final IWLAN_FEATURE_ENABLED:I = 0x1

.field public static final IWLAN_FEATURE_OFF:I = 0x1

.field public static final IWLAN_FEATURE_ON:I = 0x2

.field private static final MAX_ANDSF_FILE_SIZE:J = 0x19000L

.field private static final MAX_DNS_ADDRS:I = 0x2

.field private static final MAX_FILE_PATH_LENGTH:I = 0x400

.field private static final NETID_UPDATE:Ljava/lang/String; = "org.codeaurora.NETID_UPDATE"

.field private static final NETWORK_STATE_CONNECTED:Ljava/lang/String; = "CONNECTED"

.field private static final NETWORK_STATE_CONNECTING:Ljava/lang/String; = "CONNECTING"

.field private static final NETWORK_STATE_DISCONNECTED:Ljava/lang/String; = "DISCONNECTED"

.field private static final NETWORK_STATE_DISCONNECTING:Ljava/lang/String; = "DISCONNECTING"

.field private static final NETWORK_STATE_SUSPENDED:Ljava/lang/String; = "SUSPENDED"

.field private static final NETWORK_STATE_UNKNOWN:Ljava/lang/String; = "UNKNOWN"

.field private static final POLICY_TYPE_ANDSF:I = 0x1

.field private static final QXDM_LOGGING:I = 0xf86

.field static final RESPONSE_SOLICITED:I = 0x0

.field static final RESPONSE_UNSOLICITED:I = 0x1

.field public static final SCREEN_STATE_OFF:Z = false

.field public static final SCREEN_STATE_ON:Z = true

.field static final SOCKET_NAME_CNE:Ljava/lang/String; = "cnd"

.field static final SOCKET_OPEN_RETRY_MILLIS:I = 0xfa0

.field public static final STATUS_FAILURE:I = 0x0

.field public static final STATUS_INFLIGHT:I = 0x1

.field public static final STATUS_NOT_INFLIGHT:I = 0x0

.field public static final STATUS_SUCCESS:I = 0x1

.field private static final SUB_TYPE:Ljava/lang/String; = "CORE"

.field private static final WIFI_NO_FAM_CONNECTED:I = 0x0

.field private static final WIFI_V4_CONNECTED:I = 0x1

.field private static final WIFI_V4_V6_CONNECTED:I = 0x3

.field private static final WIFI_V6_CONNECTED:I = 0x2

.field public static final WQE_FEATURE:I = 0x1

.field public static final WQE_FEATURE_ENABLED:I = 0x1

.field public static final WQE_FEATURE_OFF:I = 0x1

.field public static final WQE_FEATURE_ON:I = 0x2

.field public static final andsfCneFbFileLoc:Ljava/lang/String; = "system/etc/cne/andsfCne.xml"

.field public static final andsfCneFileLoc:Ljava/lang/String; = "data/connectivity/andsfCne.xml"

.field public static final dataPath:Ljava/lang/String; = "/data/connectivity/"

.field private static isAndsfConfigUpdateBusy:Z = false

.field static isCndDisconnected:Z = false

.field static isCndUp:Z = false

.field static isDispatched:Z = false

.field private static mRemoveHostEntry:Z = false

.field private static mRequestHandlers:Ljava/util/concurrent/ConcurrentHashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation
.end field

.field private static mRoleRegId:I = 0x0

.field private static mSocketId:I = 0x0

.field public static final systemPath:Ljava/lang/String; = "/system/etc/cne/"

.field private static updateOpPolicy:Ljava/lang/Object; = null

.field private static final wifiBadReason:Ljava/lang/String; = " Wifi quality is poor "

.field private static final wifiGoodReason:Ljava/lang/String; = " Wifi quality is better "


# instance fields
.field private final INVALID_MSG_ARG:I

.field private IPV4Available:Z

.field private IPV6Available:Z

.field private _cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

.field private _cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

.field private activeWlanIfName:Ljava/lang/String;

.field private activeWwanV4IfName:Ljava/lang/String;

.field private activeWwanV6IfName:Ljava/lang/String;

.field private andsfHasBeenInit:Z

.field private andsfParser:Lcom/quicinc/cne/andsf/AndsfParser;

.field private cm:Landroid/net/ConnectivityManager;

.field private curLp:Landroid/net/LinkProperties;

.field private getRequestUrl:Ljava/lang/String;

.field private hostRoutingIpAddr:Ljava/lang/String;

.field private isWifiConnected:Z

.field isWlanConnected:Z

.field private lastFamilyType:I

.field mBrowserInfoThread:Ljava/lang/Thread;

.field private mContext:Landroid/content/Context;

.field private mCsHandler:Landroid/os/Handler;

.field private mDefaultNetwork:I

.field private mFactoryHandler:Landroid/os/Handler;

.field private mFactoryThread:Landroid/os/HandlerThread;

.field private mIPFamilyLock:Ljava/lang/Object;

.field private mIWLANFeatureEnabled:Z

.field private mIWLANFeatureRequestedState:Z

.field private mIdGen:Lcom/quicinc/cne/CNE$IdGenerator;

.field mIntentReceiver:Landroid/content/BroadcastReceiver;

.field private mLastWQEFeatureEnabled:Z

.field private mMobileNetwork:Landroid/net/Network;

.field private mMobileNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

.field private mMobileRequest:Landroid/net/NetworkRequest;

.field private mNetworkFactory:Landroid/net/NetworkFactory;

.field private mNetworkPreference:I

.field mPackageListener:Lcom/quicinc/cne/CNE$PackageListener;

.field mReceiver:Lcom/quicinc/cne/CNE$CNEReceiver;

.field mReceiverThread:Ljava/lang/Thread;

.field mRequestMessagesPending:I

.field mRequestsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/quicinc/cne/CNERequest;",
            ">;"
        }
    .end annotation
.end field

.field private mScreenOn:Z

.field mSender:Lcom/quicinc/cne/CNE$CNESender;

.field mSenderThread:Landroid/os/HandlerThread;

.field mServiceState:Landroid/telephony/ServiceState;

.field private mSignalStrength:Landroid/telephony/SignalStrength;

.field mSocket:Landroid/net/LocalSocket;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field private mWQEFeatureEnabled:Z

.field private mWQEFeatureRequestedState:Z

.field private mWifiManager:Landroid/net/wifi/WifiManager;

.field private mWifiNetwork:Landroid/net/Network;

.field private mWifiNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

.field private mWifiRequest:Landroid/net/NetworkRequest;

.field private netId:[I

.field prevRSSI:I

.field private wqeConfigured:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 215
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/quicinc/cne/CNE;->updateOpPolicy:Ljava/lang/Object;

    .line 219
    sput-boolean v1, Lcom/quicinc/cne/CNE;->isAndsfConfigUpdateBusy:Z

    .line 243
    sput v1, Lcom/quicinc/cne/CNE;->mSocketId:I

    .line 268
    sput-boolean v1, Lcom/quicinc/cne/CNE;->isCndUp:Z

    .line 271
    sput-boolean v1, Lcom/quicinc/cne/CNE;->isCndDisconnected:Z

    .line 274
    sput-boolean v1, Lcom/quicinc/cne/CNE;->isDispatched:Z

    .line 398
    sput v1, Lcom/quicinc/cne/CNE;->mRoleRegId:I

    .line 418
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/quicinc/cne/CNE;->mRequestHandlers:Ljava/util/concurrent/ConcurrentHashMap;

    .line 427
    sput-boolean v1, Lcom/quicinc/cne/CNE;->mRemoveHostEntry:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v9, 0x3

    const/4 v12, -0x1

    const/4 v11, 0x1

    const/4 v8, 0x0

    const/4 v10, 0x0

    .line 1232
    invoke-direct {p0}, Lcom/quicinc/cne/ICNEManager$Stub;-><init>()V

    .line 199
    iput-boolean v10, p0, Lcom/quicinc/cne/CNE;->IPV4Available:Z

    .line 200
    iput-boolean v10, p0, Lcom/quicinc/cne/CNE;->IPV6Available:Z

    .line 201
    iput v10, p0, Lcom/quicinc/cne/CNE;->lastFamilyType:I

    .line 205
    new-instance v7, Ljava/lang/Object;

    invoke-direct {v7}, Ljava/lang/Object;-><init>()V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->mIPFamilyLock:Ljava/lang/Object;

    .line 207
    iput-boolean v10, p0, Lcom/quicinc/cne/CNE;->isWifiConnected:Z

    .line 208
    iput-boolean v10, p0, Lcom/quicinc/cne/CNE;->mLastWQEFeatureEnabled:Z

    .line 210
    iput-boolean v11, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    .line 211
    iput-boolean v10, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureEnabled:Z

    .line 213
    iput-boolean v11, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureRequestedState:Z

    .line 214
    iput-boolean v10, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureRequestedState:Z

    .line 235
    iput-object v8, p0, Lcom/quicinc/cne/CNE;->mCsHandler:Landroid/os/Handler;

    .line 241
    iput-object v8, p0, Lcom/quicinc/cne/CNE;->mPackageListener:Lcom/quicinc/cne/CNE$PackageListener;

    .line 244
    iput v12, p0, Lcom/quicinc/cne/CNE;->INVALID_MSG_ARG:I

    .line 254
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->mRequestsList:Ljava/util/ArrayList;

    .line 404
    const/16 v7, 0x11

    iput v7, p0, Lcom/quicinc/cne/CNE;->mDefaultNetwork:I

    .line 421
    new-instance v7, Landroid/telephony/SignalStrength;

    invoke-direct {v7}, Landroid/telephony/SignalStrength;-><init>()V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->mSignalStrength:Landroid/telephony/SignalStrength;

    .line 423
    iput-object v8, p0, Lcom/quicinc/cne/CNE;->activeWlanIfName:Ljava/lang/String;

    .line 424
    iput-object v8, p0, Lcom/quicinc/cne/CNE;->activeWwanV4IfName:Ljava/lang/String;

    .line 425
    iput-object v8, p0, Lcom/quicinc/cne/CNE;->activeWwanV6IfName:Ljava/lang/String;

    .line 426
    iput-object v8, p0, Lcom/quicinc/cne/CNE;->hostRoutingIpAddr:Ljava/lang/String;

    .line 428
    iput-object v8, p0, Lcom/quicinc/cne/CNE;->getRequestUrl:Ljava/lang/String;

    .line 430
    iput-object v8, p0, Lcom/quicinc/cne/CNE;->andsfParser:Lcom/quicinc/cne/andsf/AndsfParser;

    .line 431
    iput-boolean v10, p0, Lcom/quicinc/cne/CNE;->andsfHasBeenInit:Z

    .line 433
    iput-boolean v10, p0, Lcom/quicinc/cne/CNE;->wqeConfigured:Z

    .line 686
    iput-boolean v10, p0, Lcom/quicinc/cne/CNE;->isWlanConnected:Z

    .line 687
    iput v10, p0, Lcom/quicinc/cne/CNE;->prevRSSI:I

    .line 689
    new-instance v7, Lcom/quicinc/cne/CNE$1;

    invoke-direct {v7, p0}, Lcom/quicinc/cne/CNE$1;-><init>(Lcom/quicinc/cne/CNE;)V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 1407
    new-instance v7, Lcom/quicinc/cne/CNE$3;

    invoke-direct {v7, p0}, Lcom/quicinc/cne/CNE$3;-><init>(Lcom/quicinc/cne/CNE;)V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->mWifiNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 1460
    new-instance v7, Lcom/quicinc/cne/CNE$4;

    invoke-direct {v7, p0}, Lcom/quicinc/cne/CNE$4;-><init>(Lcom/quicinc/cne/CNE;)V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->mMobileNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 1235
    new-instance v7, Landroid/net/LinkProperties;

    invoke-direct {v7}, Landroid/net/LinkProperties;-><init>()V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    .line 1236
    iput-object p2, p0, Lcom/quicinc/cne/CNE;->mCsHandler:Landroid/os/Handler;

    .line 1237
    iput v10, p0, Lcom/quicinc/cne/CNE;->mRequestMessagesPending:I

    .line 1238
    iput-object p1, p0, Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;

    .line 1239
    new-instance v7, Landroid/os/HandlerThread;

    const-string v8, "CNESender"

    invoke-direct {v7, v8}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->mSenderThread:Landroid/os/HandlerThread;

    .line 1240
    iget-object v7, p0, Lcom/quicinc/cne/CNE;->mSenderThread:Landroid/os/HandlerThread;

    invoke-virtual {v7}, Landroid/os/HandlerThread;->start()V

    .line 1243
    new-instance v7, Lcom/quicinc/cne/CNE$CneWifiInfo;

    invoke-direct {v7, p0}, Lcom/quicinc/cne/CNE$CneWifiInfo;-><init>(Lcom/quicinc/cne/CNE;)V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    .line 1244
    new-instance v7, Lcom/quicinc/cne/CNE$CneWwanInfo;

    invoke-direct {v7, p0}, Lcom/quicinc/cne/CNE$CneWwanInfo;-><init>(Lcom/quicinc/cne/CNE;)V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    .line 1246
    const-string v7, "persist.cne.feature"

    invoke-static {v7, v10}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 1247
    .local v4, "val":I
    if-ne v4, v9, :cond_0

    .line 1248
    iput-boolean v11, p0, Lcom/quicinc/cne/CNE;->wqeConfigured:Z

    .line 1249
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->registerCNENetworkFactory()V

    .line 1250
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->startNetworks()V

    .line 1253
    :cond_0
    const-string v7, "persist.cne.logging.qxdm"

    invoke-static {v7, v10}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 1254
    const/16 v7, 0xf86

    if-ne v4, v7, :cond_1

    .line 1255
    sput-boolean v11, Lcom/quicinc/cne/CneMsg;->ADDTL_MSG:Z

    .line 1257
    :cond_1
    iget-object v7, p0, Lcom/quicinc/cne/CNE;->mSenderThread:Landroid/os/HandlerThread;

    invoke-virtual {v7}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    .line 1258
    .local v3, "looper":Landroid/os/Looper;
    if-nez v3, :cond_3

    .line 1259
    const-string v7, "CORE"

    const-string v8, "Looper obj is NULL."

    invoke-static {v7, v8}, Lcom/quicinc/cne/CNE;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 1321
    :cond_2
    return-void

    .line 1262
    :cond_3
    new-instance v7, Lcom/quicinc/cne/CNE$CNESender;

    invoke-direct {v7, p0, v3}, Lcom/quicinc/cne/CNE$CNESender;-><init>(Lcom/quicinc/cne/CNE;Landroid/os/Looper;)V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->mSender:Lcom/quicinc/cne/CNE$CNESender;

    .line 1264
    new-instance v6, Lcom/quicinc/cne/CNE$WriteBrowserPkgApp;

    invoke-direct {v6, p0}, Lcom/quicinc/cne/CNE$WriteBrowserPkgApp;-><init>(Lcom/quicinc/cne/CNE;)V

    .line 1265
    .local v6, "writeBrowserPkgAppRunnable":Ljava/lang/Runnable;
    new-instance v7, Ljava/lang/Thread;

    invoke-direct {v7, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->mBrowserInfoThread:Ljava/lang/Thread;

    .line 1267
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1268
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v7, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1269
    const-string v7, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1270
    const-string v7, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1271
    const-string v7, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1272
    const-string v7, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1273
    const-string v7, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1274
    const-string v7, "android.net.wifi.LINK_CONFIGURATION_CHANGED"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1275
    const-string v7, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1276
    const-string v7, "android.intent.action.ANY_DATA_STATE"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1277
    const-string v7, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1278
    const-string v7, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1279
    const-string v7, "com.android.server.connectivity.UPSTREAM_IFACE_CHANGED"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1282
    const-string v7, "persist.cne.feature"

    invoke-static {v7, v10}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 1283
    .local v5, "value":I
    if-ne v5, v9, :cond_4

    .line 1284
    const-string v7, "org.codeaurora.NETID_UPDATE"

    invoke-virtual {v0, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1287
    :cond_4
    iget-object v7, p0, Lcom/quicinc/cne/CNE;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v7, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1289
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 1290
    .local v1, "filter2":Landroid/content/IntentFilter;
    const-string v7, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1291
    const-string v7, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1292
    const-string v7, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1293
    const-string v7, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1294
    const-string v7, "package"

    invoke-virtual {v1, v7}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1296
    iget-object v7, p0, Lcom/quicinc/cne/CNE;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v7, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1298
    iget-object v7, p0, Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;

    const-string v8, "wifi"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/wifi/WifiManager;

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 1299
    iget-object v7, p0, Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;

    const-string v8, "phone"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telephony/TelephonyManager;

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 1301
    new-instance v7, Lcom/quicinc/cne/CNE$CNEReceiver;

    invoke-direct {v7, p0}, Lcom/quicinc/cne/CNE$CNEReceiver;-><init>(Lcom/quicinc/cne/CNE;)V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->mReceiver:Lcom/quicinc/cne/CNE$CNEReceiver;

    .line 1302
    new-instance v7, Ljava/lang/Thread;

    iget-object v8, p0, Lcom/quicinc/cne/CNE;->mReceiver:Lcom/quicinc/cne/CNE$CNEReceiver;

    const-string v9, "CNEReceiver"

    invoke-direct {v7, v8, v9}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->mReceiverThread:Ljava/lang/Thread;

    .line 1303
    iget-object v7, p0, Lcom/quicinc/cne/CNE;->mReceiverThread:Ljava/lang/Thread;

    invoke-virtual {v7}, Ljava/lang/Thread;->start()V

    .line 1305
    new-instance v7, Lcom/quicinc/cne/andsf/AndsfParser;

    iget-object v8, p0, Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Lcom/quicinc/cne/andsf/AndsfParser;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->andsfParser:Lcom/quicinc/cne/andsf/AndsfParser;

    .line 1306
    const/4 v7, 0x7

    if-eq v5, v7, :cond_5

    const/16 v7, 0x8

    if-eq v5, v7, :cond_5

    const/16 v7, 0x9

    if-ne v5, v7, :cond_6

    .line 1307
    :cond_5
    new-instance v7, Lcom/quicinc/cne/CNE$PackageListener;

    invoke-direct {v7, p0}, Lcom/quicinc/cne/CNE$PackageListener;-><init>(Lcom/quicinc/cne/CNE;)V

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->mPackageListener:Lcom/quicinc/cne/CNE$PackageListener;

    .line 1311
    :cond_6
    const-string v7, "persist.sys.cnd.wqe"

    invoke-static {v7, v10}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 1312
    const/4 v7, 0x2

    if-ne v7, v5, :cond_7

    .line 1313
    const-string v7, "CORE"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "sendPrefChangedBroadcast wqe status = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/quicinc/cne/CNE;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 1314
    invoke-direct {p0, v11, v11, v5}, Lcom/quicinc/cne/CNE;->sendPrefChangedBroadcast(III)V

    .line 1317
    :cond_7
    const/4 v7, 0x4

    new-array v7, v7, [I

    iput-object v7, p0, Lcom/quicinc/cne/CNE;->netId:[I

    .line 1318
    const/4 v2, 0x0

    .local v2, "iface":I
    :goto_0
    const/4 v7, 0x4

    if-ge v2, v7, :cond_2

    .line 1319
    iget-object v7, p0, Lcom/quicinc/cne/CNE;->netId:[I

    aput v12, v7, v2

    .line 1318
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private CneRatTypetoNetworkType(I)I
    .locals 4
    .param p1, "x"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 2194
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    move v0, v2

    .line 2212
    :goto_0
    return v0

    .line 2196
    :pswitch_1
    const-string v1, "CORE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CNE_RAT_WWAN("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") transformed into"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ConnectivityManager.TYPE_MOBILE("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2201
    :pswitch_2
    const-string v0, "CORE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "CNE_RAT_WLAN("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") transformed into"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ConnectivityManager.TYPE_WIFI("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 2204
    goto :goto_0

    :pswitch_3
    move v0, v2

    .line 2206
    goto :goto_0

    .line 2194
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private DetailedNetworkStateToFineNetworkState(Landroid/net/NetworkInfo$DetailedState;)Landroid/net/NetworkInfo$State;
    .locals 2
    .param p1, "state"    # Landroid/net/NetworkInfo$DetailedState;

    .prologue
    .line 1690
    sget-object v0, Lcom/quicinc/cne/CNE$5;->$SwitchMap$android$net$NetworkInfo$DetailedState:[I

    invoke-virtual {p1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1702
    sget-object v0, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    :goto_0
    return-object v0

    .line 1692
    :pswitch_0
    sget-object v0, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    goto :goto_0

    .line 1693
    :pswitch_1
    sget-object v0, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    goto :goto_0

    .line 1694
    :pswitch_2
    sget-object v0, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    goto :goto_0

    .line 1695
    :pswitch_3
    sget-object v0, Landroid/net/NetworkInfo$State;->DISCONNECTING:Landroid/net/NetworkInfo$State;

    goto :goto_0

    .line 1696
    :pswitch_4
    sget-object v0, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    goto :goto_0

    .line 1697
    :pswitch_5
    sget-object v0, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    goto :goto_0

    .line 1698
    :pswitch_6
    sget-object v0, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    goto :goto_0

    .line 1699
    :pswitch_7
    sget-object v0, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    goto :goto_0

    .line 1700
    :pswitch_8
    sget-object v0, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    goto :goto_0

    .line 1701
    :pswitch_9
    sget-object v0, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    goto :goto_0

    .line 1690
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private NetworkStateStringToInt(Ljava/lang/String;)I
    .locals 7
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1642
    const/4 v5, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v5, :pswitch_data_0

    .line 1659
    const/4 v0, 0x5

    :goto_1
    :pswitch_0
    return v0

    .line 1642
    :sswitch_0
    const-string v6, "CONNECTING"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v5, v0

    goto :goto_0

    :sswitch_1
    const-string v6, "CONNECTED"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v5, v1

    goto :goto_0

    :sswitch_2
    const-string v6, "SUSPENDED"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v5, v2

    goto :goto_0

    :sswitch_3
    const-string v6, "DISCONNECTING"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v5, v3

    goto :goto_0

    :sswitch_4
    const-string v6, "DISCONNECTED"

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v5, v4

    goto :goto_0

    :pswitch_1
    move v0, v1

    .line 1647
    goto :goto_1

    :pswitch_2
    move v0, v2

    .line 1650
    goto :goto_1

    :pswitch_3
    move v0, v3

    .line 1653
    goto :goto_1

    :pswitch_4
    move v0, v4

    .line 1656
    goto :goto_1

    .line 1642
    :sswitch_data_0
    .sparse-switch
        -0x7c6dfd17 -> :sswitch_1
        -0x3eb5be5a -> :sswitch_3
        -0x11519548 -> :sswitch_0
        0x37c8963b -> :sswitch_4
        0x430d9dbb -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private NetworkStateToInt(Landroid/net/NetworkInfo$State;)I
    .locals 2
    .param p1, "state"    # Landroid/net/NetworkInfo$State;

    .prologue
    .line 1664
    sget-object v0, Lcom/quicinc/cne/CNE$5;->$SwitchMap$android$net$NetworkInfo$State:[I

    invoke-virtual {p1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1684
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 1666
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1669
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1672
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 1675
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 1678
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 1681
    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    .line 1664
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private NetworkTypetoCneRatType(I)I
    .locals 1
    .param p1, "x"    # I

    .prologue
    .line 2181
    packed-switch p1, :pswitch_data_0

    .line 2189
    const/4 v0, 0x4

    :goto_0
    return v0

    .line 2183
    :pswitch_0
    const/4 v0, 0x3

    goto :goto_0

    .line 2185
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2187
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 2181
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic access$000(Lcom/quicinc/cne/CNE;Landroid/net/NetworkInfo$State;)I
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Landroid/net/NetworkInfo$State;

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->NetworkStateToInt(Landroid/net/NetworkInfo$State;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 141
    invoke-static {p0, p1}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/quicinc/cne/CNE;)Z
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/quicinc/cne/CNE;->isWifiConnected:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/quicinc/cne/CNE;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/quicinc/cne/CNE;->isWifiConnected:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/quicinc/cne/CNE;)I
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->getInetFamily()I

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/quicinc/cne/CNE;)Z
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->updateDefaultNetwork()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/quicinc/cne/CNE;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Lcom/quicinc/cne/CNE;->notifyNetIdInfo(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Lcom/quicinc/cne/CNE;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->sendScreenState(Z)V

    return-void
.end method

.method static synthetic access$1502(Lcom/quicinc/cne/CNE;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/quicinc/cne/CNE;->mScreenOn:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/quicinc/cne/CNE;)Lcom/quicinc/cne/CNE$CneWifiInfo;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/quicinc/cne/CNE;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->sendWifiStatus()V

    return-void
.end method

.method static synthetic access$1900(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 141
    invoke-static {p0, p1}, Lcom/quicinc/cne/CNE;->loge(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/quicinc/cne/CNE;)Landroid/net/wifi/WifiManager;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/quicinc/cne/CNE;)Lcom/quicinc/cne/CNE$CneWwanInfo;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/quicinc/cne/CNE;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->sendWwanStatus()V

    return-void
.end method

.method static synthetic access$2300(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 141
    invoke-static {p0, p1}, Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2400(Lcom/quicinc/cne/CNE;)Z
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->sendBrowserInfoList()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2500(Lcom/quicinc/cne/CNE;I)Lcom/quicinc/cne/CNERequest;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # I

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->findAndRemoveRequestFromList(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2600(Lcom/quicinc/cne/CNE;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->postCndUpInit()V

    return-void
.end method

.method static synthetic access$2700(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 141
    invoke-static {p0, p1}, Lcom/quicinc/cne/CNE;->rlog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/quicinc/cne/CNE;)Z
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->sendInitReq()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2900(Lcom/quicinc/cne/CNE;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->requestFeaturesSettings()V

    return-void
.end method

.method static synthetic access$300(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 141
    invoke-static {p0, p1}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$3000(Ljava/io/InputStream;[B)I
    .locals 1
    .param p0, "x0"    # Ljava/io/InputStream;
    .param p1, "x1"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    invoke-static {p0, p1}, Lcom/quicinc/cne/CNE;->readCneMessage(Ljava/io/InputStream;[B)I

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/quicinc/cne/CNE;Landroid/os/Parcel;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Landroid/os/Parcel;

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->processResponse(Landroid/os/Parcel;)V

    return-void
.end method

.method static synthetic access$3202(Lcom/quicinc/cne/CNE;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/quicinc/cne/CNE;->andsfHasBeenInit:Z

    return p1
.end method

.method static synthetic access$3300(Lcom/quicinc/cne/CNE;)Landroid/net/NetworkFactory;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->mNetworkFactory:Landroid/net/NetworkFactory;

    return-object v0
.end method

.method static synthetic access$3302(Lcom/quicinc/cne/CNE;Landroid/net/NetworkFactory;)Landroid/net/NetworkFactory;
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Landroid/net/NetworkFactory;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/quicinc/cne/CNE;->mNetworkFactory:Landroid/net/NetworkFactory;

    return-object p1
.end method

.method static synthetic access$3400(Lcom/quicinc/cne/CNE;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/quicinc/cne/CNE;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/quicinc/cne/CNE;->mFactoryHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$3602(Lcom/quicinc/cne/CNE;Landroid/net/Network;)Landroid/net/Network;
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Landroid/net/Network;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/quicinc/cne/CNE;->mWifiNetwork:Landroid/net/Network;

    return-object p1
.end method

.method static synthetic access$3700(Lcom/quicinc/cne/CNE;)Landroid/net/ConnectivityManager;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->cm:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/quicinc/cne/CNE;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->mIPFamilyLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$3902(Lcom/quicinc/cne/CNE;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/quicinc/cne/CNE;->IPV4Available:Z

    return p1
.end method

.method static synthetic access$400(Lcom/quicinc/cne/CNE;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->NetworkStateStringToInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$4002(Lcom/quicinc/cne/CNE;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Z

    .prologue
    .line 141
    iput-boolean p1, p0, Lcom/quicinc/cne/CNE;->IPV6Available:Z

    return p1
.end method

.method static synthetic access$4100(Lcom/quicinc/cne/CNE;Landroid/net/LinkProperties;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Landroid/net/LinkProperties;

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->checkIPFamilyAvailability(Landroid/net/LinkProperties;)V

    return-void
.end method

.method static synthetic access$4200(Lcom/quicinc/cne/CNE;Landroid/net/LinkProperties;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Landroid/net/LinkProperties;

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->updateLinkProperties(Landroid/net/LinkProperties;)V

    return-void
.end method

.method static synthetic access$4302(Lcom/quicinc/cne/CNE;I)I
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # I

    .prologue
    .line 141
    iput p1, p0, Lcom/quicinc/cne/CNE;->lastFamilyType:I

    return p1
.end method

.method static synthetic access$4400(Lcom/quicinc/cne/CNE;)[I
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->netId:[I

    return-object v0
.end method

.method static synthetic access$4500(Lcom/quicinc/cne/CNE;)Landroid/net/LinkProperties;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/quicinc/cne/CNE;Landroid/net/LinkProperties;)Z
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Landroid/net/LinkProperties;

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->compareLinkProperties(Landroid/net/LinkProperties;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4702(Lcom/quicinc/cne/CNE;Landroid/net/Network;)Landroid/net/Network;
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Landroid/net/Network;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/quicinc/cne/CNE;->mMobileNetwork:Landroid/net/Network;

    return-object p1
.end method

.method static synthetic access$4900(Lcom/quicinc/cne/CNE;Lcom/quicinc/cne/CNERequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # Lcom/quicinc/cne/CNERequest;

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->sendAppInfoList(Lcom/quicinc/cne/CNERequest;)V

    return-void
.end method

.method static synthetic access$500(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 141
    invoke-static {p0, p1}, Lcom/quicinc/cne/CNE;->logd(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/quicinc/cne/CNE;)Landroid/telephony/TelephonyManager;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/quicinc/cne/CNE;I)I
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE;
    .param p1, "x1"    # I

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->getSignalStrength(I)I

    move-result v0

    return v0
.end method

.method private broadcastWqeStateChange(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    .line 1501
    const-string v1, "CORE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "send wqe state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1502
    new-instance v0, Landroid/content/Intent;

    const-string v1, "prop_state_change"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1503
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "state"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1504
    iget-object v1, p0, Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1505
    return-void
.end method

.method private checkFeatureEnabled(Lcom/quicinc/cne/CNE$FeatureType;)Z
    .locals 6
    .param p1, "feature"    # Lcom/quicinc/cne/CNE$FeatureType;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2619
    const-string v4, "persist.cne.feature"

    invoke-static {v4, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 2620
    .local v1, "value":I
    const/4 v0, 0x0

    .line 2621
    .local v0, "enabled":Z
    sget-object v4, Lcom/quicinc/cne/CNE$5;->$SwitchMap$com$quicinc$cne$CNE$FeatureType:[I

    invoke-virtual {p1}, Lcom/quicinc/cne/CNE$FeatureType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2623
    const/4 v0, 0x0

    .line 2625
    :goto_0
    if-nez v0, :cond_2

    .line 2626
    const-string v3, "CORE"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Feature "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is not enabled"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/quicinc/cne/CNE;->dloge(Ljava/lang/String;Ljava/lang/String;)V

    .line 2629
    :goto_1
    return v2

    .line 2622
    :pswitch_0
    const/4 v4, 0x3

    if-eq v1, v4, :cond_0

    const/4 v4, 0x6

    if-ne v1, v4, :cond_1

    :cond_0
    move v0, v3

    :goto_2
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_2

    :cond_2
    move v2, v3

    .line 2629
    goto :goto_1

    .line 2621
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private checkIPFamilyAvailability(Landroid/net/LinkProperties;)V
    .locals 2
    .param p1, "lp"    # Landroid/net/LinkProperties;

    .prologue
    .line 1383
    iget-object v1, p0, Lcom/quicinc/cne/CNE;->mIPFamilyLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1384
    if-nez p1, :cond_0

    .line 1385
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/quicinc/cne/CNE;->IPV4Available:Z

    .line 1386
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/quicinc/cne/CNE;->IPV6Available:Z

    .line 1387
    monitor-exit v1

    .line 1405
    :goto_0
    return-void

    .line 1389
    :cond_0
    invoke-virtual {p1}, Landroid/net/LinkProperties;->hasIPv4Address()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/net/LinkProperties;->hasIPv4DefaultRoute()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/net/LinkProperties;->hasIPv4DnsServer()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1391
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quicinc/cne/CNE;->IPV4Available:Z

    .line 1392
    invoke-virtual {p1}, Landroid/net/LinkProperties;->hasGlobalIPv6Address()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/net/LinkProperties;->hasIPv6DefaultRoute()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1393
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quicinc/cne/CNE;->IPV6Available:Z

    .line 1404
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1395
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/quicinc/cne/CNE;->IPV6Available:Z

    goto :goto_1

    .line 1397
    :cond_2
    invoke-virtual {p1}, Landroid/net/LinkProperties;->hasGlobalIPv6Address()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/net/LinkProperties;->hasIPv6DefaultRoute()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/net/LinkProperties;->hasIPv6DnsServer()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1399
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quicinc/cne/CNE;->IPV6Available:Z

    .line 1400
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/quicinc/cne/CNE;->IPV4Available:Z

    goto :goto_1

    .line 1402
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/quicinc/cne/CNE;->IPV6Available:Z

    iput-boolean v0, p0, Lcom/quicinc/cne/CNE;->IPV4Available:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private compareLinkProperties(Landroid/net/LinkProperties;)Z
    .locals 1
    .param p1, "newLp"    # Landroid/net/LinkProperties;

    .prologue
    .line 1356
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    invoke-virtual {v0, p1}, Landroid/net/LinkProperties;->isIdenticalDnses(Landroid/net/LinkProperties;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    invoke-virtual {v0, p1}, Landroid/net/LinkProperties;->isIdenticalRoutes(Landroid/net/LinkProperties;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    invoke-virtual {v0, p1}, Landroid/net/LinkProperties;->isIdenticalAddresses(Landroid/net/LinkProperties;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    invoke-virtual {v0, p1}, Landroid/net/LinkProperties;->isIdenticalStackedLinks(Landroid/net/LinkProperties;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static configureSsid(Ljava/lang/String;)Z
    .locals 17
    .param p0, "newStr"    # Ljava/lang/String;

    .prologue
    .line 2480
    const/4 v12, 0x0

    .line 2481
    .local v12, "strMatched":Z
    :try_start_0
    new-instance v1, Ljava/io/File;

    const-string v14, "/data/ssidconfig.txt"

    invoke-direct {v1, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2484
    .local v1, "file":Ljava/io/File;
    new-instance v11, Ljava/io/BufferedReader;

    new-instance v14, Ljava/io/FileReader;

    invoke-direct {v14, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v11, v14}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 2485
    .local v11, "reader":Ljava/io/BufferedReader;
    const-string v3, ""

    .line 2486
    .local v3, "line":Ljava/lang/String;
    const-string v10, ""

    .line 2487
    .local v10, "oldtext":Ljava/lang/String;
    const-string v7, ""

    .line 2489
    .local v7, "oldStr":Ljava/lang/String;
    new-instance v5, Ljava/util/StringTokenizer;

    const-string v14, ":"

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v14}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2490
    .local v5, "newst":Ljava/util/StringTokenizer;
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    .line 2491
    .local v4, "newToken":Ljava/lang/String;
    const-string v14, "CORE"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "configureSsid: newToken: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2492
    :cond_0
    invoke-virtual {v11}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2493
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\r\n"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 2494
    new-instance v9, Ljava/util/StringTokenizer;

    const-string v14, ":"

    invoke-direct {v9, v3, v14}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2495
    .local v9, "oldst":Ljava/util/StringTokenizer;
    :cond_1
    :goto_0
    invoke-virtual {v9}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 2496
    invoke-virtual {v9}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    .line 2497
    .local v8, "oldToken":Ljava/lang/String;
    const-string v14, "CORE"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "configureSsid: oldToken: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2498
    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 2499
    const-string v14, "CORE"

    const-string v15, "configSsid entry matched"

    invoke-static {v14, v15}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2501
    move-object v7, v3

    .line 2502
    const/4 v12, 0x1

    goto :goto_0

    .line 2506
    .end local v8    # "oldToken":Ljava/lang/String;
    .end local v9    # "oldst":Ljava/util/StringTokenizer;
    :cond_2
    if-nez v12, :cond_3

    .line 2507
    const-string v14, "CORE"

    const-string v15, "configSsid entry not matched"

    invoke-static {v14, v15}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2508
    const/4 v14, 0x0

    .line 2520
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "line":Ljava/lang/String;
    .end local v4    # "newToken":Ljava/lang/String;
    .end local v5    # "newst":Ljava/util/StringTokenizer;
    .end local v7    # "oldStr":Ljava/lang/String;
    .end local v10    # "oldtext":Ljava/lang/String;
    .end local v11    # "reader":Ljava/io/BufferedReader;
    :goto_1
    return v14

    .line 2511
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "line":Ljava/lang/String;
    .restart local v4    # "newToken":Ljava/lang/String;
    .restart local v5    # "newst":Ljava/util/StringTokenizer;
    .restart local v7    # "oldStr":Ljava/lang/String;
    .restart local v10    # "oldtext":Ljava/lang/String;
    .restart local v11    # "reader":Ljava/io/BufferedReader;
    :cond_3
    move-object/from16 v0, p0

    invoke-virtual {v10, v7, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2512
    .local v6, "newtext":Ljava/lang/String;
    invoke-virtual {v11}, Ljava/io/BufferedReader;->close()V

    .line 2513
    new-instance v13, Ljava/io/FileWriter;

    const-string v14, "/data/ssidconfig.txt"

    invoke-direct {v13, v14}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    .line 2514
    .local v13, "writer":Ljava/io/FileWriter;
    invoke-virtual {v13, v6}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 2515
    invoke-virtual {v13}, Ljava/io/FileWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2516
    const/4 v14, 0x1

    goto :goto_1

    .line 2517
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "line":Ljava/lang/String;
    .end local v4    # "newToken":Ljava/lang/String;
    .end local v5    # "newst":Ljava/util/StringTokenizer;
    .end local v6    # "newtext":Ljava/lang/String;
    .end local v7    # "oldStr":Ljava/lang/String;
    .end local v10    # "oldtext":Ljava/lang/String;
    .end local v11    # "reader":Ljava/io/BufferedReader;
    .end local v13    # "writer":Ljava/io/FileWriter;
    :catch_0
    move-exception v2

    .line 2518
    .local v2, "ioe":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 2520
    const/4 v14, 0x1

    goto :goto_1
.end method

.method private convertToNetworkState(Lcom/android/internal/telephony/PhoneConstants$DataState;)Landroid/net/NetworkInfo$State;
    .locals 2
    .param p1, "dataState"    # Lcom/android/internal/telephony/PhoneConstants$DataState;

    .prologue
    .line 1708
    sget-object v0, Lcom/quicinc/cne/CNE$5;->$SwitchMap$com$android$internal$telephony$PhoneConstants$DataState:[I

    invoke-virtual {p1}, Lcom/android/internal/telephony/PhoneConstants$DataState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1718
    sget-object v0, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    :goto_0
    return-object v0

    .line 1710
    :pswitch_0
    sget-object v0, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    goto :goto_0

    .line 1712
    :pswitch_1
    sget-object v0, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    goto :goto_0

    .line 1714
    :pswitch_2
    sget-object v0, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    goto :goto_0

    .line 1716
    :pswitch_3
    sget-object v0, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    goto :goto_0

    .line 1708
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static dlogd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 3081
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 3082
    return-void
.end method

.method private static dloge(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 3093
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 3094
    return-void
.end method

.method private static dlogi(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 3087
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 3088
    return-void
.end method

.method private static dlogv(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 3084
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 3085
    return-void
.end method

.method private static dlogw(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 3090
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 3091
    return-void
.end method

.method private findAndRemoveRequestFromList(I)Lcom/quicinc/cne/CNERequest;
    .locals 5
    .param p1, "serial"    # I

    .prologue
    .line 1537
    iget-object v4, p0, Lcom/quicinc/cne/CNE;->mRequestsList:Ljava/util/ArrayList;

    monitor-enter v4

    .line 1538
    const/4 v0, 0x0

    .local v0, "i":I
    :try_start_0
    iget-object v3, p0, Lcom/quicinc/cne/CNE;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "s":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 1539
    iget-object v3, p0, Lcom/quicinc/cne/CNE;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/quicinc/cne/CNERequest;

    .line 1540
    .local v1, "rr":Lcom/quicinc/cne/CNERequest;
    iget v3, v1, Lcom/quicinc/cne/CNERequest;->mSerial:I

    if-ne v3, p1, :cond_0

    .line 1541
    iget-object v3, p0, Lcom/quicinc/cne/CNE;->mRequestsList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1542
    monitor-exit v4

    .line 1546
    .end local v1    # "rr":Lcom/quicinc/cne/CNERequest;
    :goto_1
    return-object v1

    .line 1538
    .restart local v1    # "rr":Lcom/quicinc/cne/CNERequest;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1545
    .end local v1    # "rr":Lcom/quicinc/cne/CNERequest;
    :cond_1
    monitor-exit v4

    .line 1546
    const/4 v1, 0x0

    goto :goto_1

    .line 1545
    .end local v2    # "s":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private getInetFamily()I
    .locals 3

    .prologue
    .line 1864
    iget-object v1, p0, Lcom/quicinc/cne/CNE;->mIPFamilyLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1865
    :try_start_0
    iget-boolean v0, p0, Lcom/quicinc/cne/CNE;->IPV4Available:Z

    if-eqz v0, :cond_1

    .line 1866
    iget-boolean v0, p0, Lcom/quicinc/cne/CNE;->IPV6Available:Z

    if-eqz v0, :cond_0

    .line 1867
    const-string v0, "CORE"

    const-string v2, "V4_V6 connected"

    invoke-static {v0, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1868
    const/4 v0, 0x3

    monitor-exit v1

    .line 1877
    :goto_0
    return v0

    .line 1870
    :cond_0
    const-string v0, "CORE"

    const-string v2, "V4 connected"

    invoke-static {v0, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1871
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    .line 1878
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1872
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/quicinc/cne/CNE;->IPV6Available:Z

    if-eqz v0, :cond_2

    .line 1873
    const-string v0, "CORE"

    const-string v2, "V6 connected"

    invoke-static {v0, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1874
    const/4 v0, 0x2

    monitor-exit v1

    goto :goto_0

    .line 1876
    :cond_2
    const-string v0, "CORE"

    const-string v2, "No family connected"

    invoke-static {v0, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1877
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private getPhone()Lcom/android/internal/telephony/ITelephony;
    .locals 1

    .prologue
    .line 1193
    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    return-object v0
.end method

.method private getSignalStrength(I)I
    .locals 4
    .param p1, "networkType"    # I

    .prologue
    const/4 v0, -0x1

    .line 1723
    iget-object v1, p0, Lcom/quicinc/cne/CNE;->mSignalStrength:Landroid/telephony/SignalStrength;

    if-nez v1, :cond_0

    .line 1724
    const-string v1, "CORE"

    const-string v2, "getSignalStrength mSignalStrength in null"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1746
    :goto_0
    return v0

    .line 1727
    :cond_0
    const-string v1, "CORE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSignalStrength networkType= "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 1728
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1736
    :pswitch_0
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->mSignalStrength:Landroid/telephony/SignalStrength;

    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x71

    goto :goto_0

    .line 1739
    :pswitch_1
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->mSignalStrength:Landroid/telephony/SignalStrength;

    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v0

    goto :goto_0

    .line 1743
    :pswitch_2
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->mSignalStrength:Landroid/telephony/SignalStrength;

    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getEvdoDbm()I

    move-result v0

    goto :goto_0

    .line 1728
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private handleDnsPriorityCmd(Landroid/os/Parcel;)V
    .locals 10
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    const/4 v9, 0x1

    .line 2341
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 2342
    .local v2, "size":I
    const/4 v3, 0x0

    .line 2343
    .local v3, "temp":I
    new-array v1, v2, [I

    .line 2345
    .local v1, "priority":[I
    const-string v6, "CORE"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleDnsPriorityCmd: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2347
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 2348
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 2349
    invoke-direct {p0, v3}, Lcom/quicinc/cne/CNE;->CneRatTypetoNetworkType(I)I

    move-result v6

    aput v6, v1, v0

    .line 2350
    const-string v6, "CORE"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "priority["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget v8, v1, v0

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2347
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2352
    :cond_0
    const/4 v6, 0x0

    aget v4, v1, v6

    .line 2353
    .local v4, "type":I
    if-le v2, v9, :cond_2

    aget v5, v1, v9

    .line 2354
    .local v5, "type2":I
    :goto_1
    invoke-static {v4}, Landroid/net/ConnectivityManager;->isNetworkTypeValid(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2361
    :cond_1
    return-void

    .line 2353
    .end local v5    # "type2":I
    :cond_2
    const/4 v5, -0x1

    goto :goto_1
.end method

.method private handleFeatureStatusNotification(Landroid/os/Parcel;)V
    .locals 7
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 2288
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 2289
    .local v0, "featureId":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 2291
    .local v1, "featureStatus":I
    const-string v2, "CORE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleFeatureStatusNotification(): feature id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " feature status: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2294
    if-ne v0, v5, :cond_4

    .line 2295
    monitor-enter p0

    .line 2296
    if-ne v1, v6, :cond_1

    .line 2297
    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    .line 2298
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureRequestedState:Z

    .line 2307
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2308
    iget-boolean v2, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    if-nez v2, :cond_0

    .line 2309
    iget-boolean v2, p0, Lcom/quicinc/cne/CNE;->wqeConfigured:Z

    if-eqz v2, :cond_3

    .line 2310
    const-string v2, "CORE"

    const-string v3, "WQE is configured"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2311
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->stopNetworks()V

    .line 2313
    iget-object v2, p0, Lcom/quicinc/cne/CNE;->mSender:Lcom/quicinc/cne/CNE$CNESender;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v6, v4, v5}, Lcom/quicinc/cne/CNE$CNESender;->sendEmptyMessageDelayed(IJ)Z

    .line 2337
    :cond_0
    :goto_1
    return-void

    .line 2299
    :cond_1
    if-ne v1, v5, :cond_2

    .line 2300
    const/4 v2, 0x0

    :try_start_1
    iput-boolean v2, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    .line 2301
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureRequestedState:Z

    goto :goto_0

    .line 2307
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 2303
    :cond_2
    const/4 v2, 0x0

    :try_start_2
    iput-boolean v2, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    .line 2304
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureRequestedState:Z

    .line 2305
    const-string v2, "CORE"

    const-string v3, "handleFeatureStatusNotification():unknown feature status."

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->dloge(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2316
    :cond_3
    const-string v2, "CORE"

    const-string v3, "WQE is not configured."

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2320
    :cond_4
    if-ne v0, v6, :cond_7

    .line 2321
    monitor-enter p0

    .line 2322
    if-ne v1, v6, :cond_5

    .line 2323
    const/4 v2, 0x1

    :try_start_3
    iput-boolean v2, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureEnabled:Z

    .line 2324
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureRequestedState:Z

    .line 2333
    :goto_2
    monitor-exit p0

    goto :goto_1

    :catchall_1
    move-exception v2

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    .line 2325
    :cond_5
    if-ne v1, v5, :cond_6

    .line 2326
    const/4 v2, 0x0

    :try_start_4
    iput-boolean v2, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureEnabled:Z

    .line 2327
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureRequestedState:Z

    goto :goto_2

    .line 2329
    :cond_6
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureEnabled:Z

    .line 2330
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureRequestedState:Z

    .line 2331
    const-string v2, "CORE"

    const-string v3, "handleFeatureStatusNotification():unknown feature status."

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->dloge(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_2

    .line 2335
    :cond_7
    const-string v2, "CORE"

    const-string v3, "handleFeatureStatusNotification(): unknown feature id."

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->dloge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private handleFeatureStatusSetResponse(Landroid/os/Parcel;)V
    .locals 9
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x1

    .line 2228
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 2229
    .local v1, "featureId":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 2230
    .local v2, "featureStatus":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 2232
    .local v0, "error":I
    const-string v6, "CORE"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleFeatureStatusSetResponse(): feature id: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " feature status: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " error code: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2235
    if-nez v0, :cond_a

    .line 2236
    if-ne v1, v5, :cond_5

    .line 2237
    monitor-enter p0

    .line 2238
    if-ne v2, v4, :cond_0

    .line 2239
    const/4 v6, 0x1

    :try_start_0
    iput-boolean v6, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    .line 2247
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2248
    iget-boolean v6, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    if-eqz v6, :cond_2

    move v3, v4

    .line 2249
    .local v3, "state":I
    :goto_1
    invoke-direct {p0, v5, v5, v3}, Lcom/quicinc/cne/CNE;->sendPrefChangedBroadcast(III)V

    .line 2250
    iget-boolean v5, p0, Lcom/quicinc/cne/CNE;->wqeConfigured:Z

    if-eqz v5, :cond_4

    .line 2251
    const-string v5, "CORE"

    const-string v6, "WQE is configured"

    invoke-static {v5, v6}, Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2253
    iget-boolean v5, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    if-eqz v5, :cond_3

    .line 2254
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->startNetworks()V

    .line 2259
    :goto_2
    iget-object v5, p0, Lcom/quicinc/cne/CNE;->mSender:Lcom/quicinc/cne/CNE$CNESender;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v5, v4, v6, v7}, Lcom/quicinc/cne/CNE$CNESender;->sendEmptyMessageDelayed(IJ)Z

    .line 2284
    .end local v3    # "state":I
    :goto_3
    return-void

    .line 2240
    :cond_0
    if-ne v2, v5, :cond_1

    .line 2241
    const/4 v6, 0x0

    :try_start_1
    iput-boolean v6, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    goto :goto_0

    .line 2247
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 2243
    :cond_1
    const/4 v6, 0x0

    :try_start_2
    iput-boolean v6, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    .line 2244
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureRequestedState:Z

    .line 2245
    const-string v6, "CORE"

    const-string v7, "handleFeatureStatusSetResponse():unknown feature status."

    invoke-static {v6, v7}, Lcom/quicinc/cne/CNE;->dloge(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :cond_2
    move v3, v5

    .line 2248
    goto :goto_1

    .line 2256
    .restart local v3    # "state":I
    :cond_3
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->stopNetworks()V

    goto :goto_2

    .line 2262
    :cond_4
    const-string v4, "CORE"

    const-string v5, "WQE is not configured."

    invoke-static {v4, v5}, Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 2264
    .end local v3    # "state":I
    :cond_5
    if-ne v1, v4, :cond_9

    .line 2265
    monitor-enter p0

    .line 2266
    if-ne v2, v4, :cond_6

    .line 2267
    const/4 v6, 0x1

    :try_start_3
    iput-boolean v6, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureEnabled:Z

    .line 2275
    :goto_4
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2276
    iget-boolean v6, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureEnabled:Z

    if-eqz v6, :cond_8

    move v3, v4

    .line 2277
    .restart local v3    # "state":I
    :goto_5
    invoke-direct {p0, v4, v5, v3}, Lcom/quicinc/cne/CNE;->sendPrefChangedBroadcast(III)V

    goto :goto_3

    .line 2268
    .end local v3    # "state":I
    :cond_6
    if-ne v2, v5, :cond_7

    .line 2269
    const/4 v6, 0x0

    :try_start_4
    iput-boolean v6, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureEnabled:Z

    goto :goto_4

    .line 2275
    :catchall_1
    move-exception v4

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v4

    .line 2271
    :cond_7
    const/4 v6, 0x0

    :try_start_5
    iput-boolean v6, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureEnabled:Z

    .line 2272
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureRequestedState:Z

    .line 2273
    const-string v6, "CORE"

    const-string v7, "handleFeatureStatusSetResponse():unknown feature status."

    invoke-static {v6, v7}, Lcom/quicinc/cne/CNE;->dloge(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_4

    :cond_8
    move v3, v5

    .line 2276
    goto :goto_5

    .line 2279
    :cond_9
    const-string v4, "CORE"

    const-string v5, "handleFeatureStatusSetResponse(): unknown feature id."

    invoke-static {v4, v5}, Lcom/quicinc/cne/CNE;->dloge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 2282
    :cond_a
    const-string v4, "CORE"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleFeatureStatusSetResponse(): response error code: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/quicinc/cne/CNE;->dloge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3
.end method

.method private handleGetAppInfoMsg(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 2158
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->mPackageListener:Lcom/quicinc/cne/CNE$PackageListener;

    invoke-virtual {v0}, Lcom/quicinc/cne/CNE$PackageListener;->sendInstalledPackageInfo()Z

    .line 2159
    return-void
.end method

.method private handleGetBrowsersInfoMsg(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 2162
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->sendBrowserInfoList()Z

    .line 2163
    return-void
.end method

.method private handleGetParentApp(Landroid/os/Parcel;)V
    .locals 15
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 2720
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 2721
    .local v3, "cookie":I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 2722
    .local v5, "nestedApp":Ljava/lang/String;
    const/4 v7, 0x0

    .line 2724
    .local v7, "parentUid":I
    :try_start_0
    iget-object v12, p0, Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;

    const-string v13, "activity"

    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 2725
    .local v0, "am":Landroid/app/ActivityManager;
    const/4 v12, 0x1

    invoke-virtual {v0, v12}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v9

    .line 2726
    .local v9, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_0

    .line 2727
    const/4 v12, 0x0

    invoke-interface {v9, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v10, v12, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 2728
    .local v10, "topActivity":Landroid/content/ComponentName;
    const/4 v12, 0x0

    invoke-interface {v9, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v12, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    .line 2729
    .local v1, "baseActivity":Landroid/content/ComponentName;
    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v11

    .line 2730
    .local v11, "topPackage":Ljava/lang/String;
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 2731
    .local v2, "basePackage":Ljava/lang/String;
    const-string v12, "CORE"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "topPackage:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " basePackage:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2732
    invoke-virtual {v11, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 2733
    iget-object v12, p0, Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v2, v13}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v6

    .line 2735
    .local v6, "parentAppInfo":Landroid/content/pm/ApplicationInfo;
    iget v7, v6, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2742
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v1    # "baseActivity":Landroid/content/ComponentName;
    .end local v2    # "basePackage":Ljava/lang/String;
    .end local v6    # "parentAppInfo":Landroid/content/pm/ApplicationInfo;
    .end local v9    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v10    # "topActivity":Landroid/content/ComponentName;
    .end local v11    # "topPackage":Ljava/lang/String;
    :cond_0
    :goto_0
    const/16 v12, 0x21

    invoke-static {v12}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v8

    .line 2743
    .local v8, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v8, :cond_1

    .line 2744
    const-string v12, "CORE"

    const-string v13, "handleGetParentApp: rr=NULL"

    invoke-static {v12, v13}, Lcom/quicinc/cne/CNE;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 2750
    :goto_1
    return-void

    .line 2738
    .end local v8    # "rr":Lcom/quicinc/cne/CNERequest;
    :catch_0
    move-exception v4

    .line 2739
    .local v4, "e":Ljava/lang/Exception;
    const-string v12, "CORE"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "handleGetParentApp Exception: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/quicinc/cne/CNE;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2747
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v8    # "rr":Lcom/quicinc/cne/CNERequest;
    :cond_1
    iget-object v12, v8, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v12, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2748
    iget-object v12, v8, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v12, v7}, Landroid/os/Parcel;->writeInt(I)V

    .line 2749
    invoke-virtual {p0, v8}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    goto :goto_1
.end method

.method private handlePolicyUpdateResponse(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    const/4 v4, 0x1

    .line 2138
    const-string v2, "CORE"

    const-string v3, "handlePolicyUpdateResponse called"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->dlogv(Ljava/lang/String;Ljava/lang/String;)V

    .line 2139
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 2140
    .local v0, "policy":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 2141
    .local v1, "result":I
    if-ne v0, v4, :cond_0

    .line 2144
    const/4 v2, 0x0

    invoke-direct {p0, v4, v2}, Lcom/quicinc/cne/CNE;->setPolicyConfigUpdateBusy(IZ)V

    .line 2146
    :cond_0
    return-void
.end method

.method private handlePostBqeResult(Landroid/os/Parcel;)V
    .locals 11
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 2116
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 2117
    .local v5, "ssid":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 2118
    .local v3, "uri":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    .line 2119
    .local v9, "tputKiloBitsPerSec":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 2121
    .local v8, "timestampSec":I
    const-string v0, "CORE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handlePostBqeResult called, ssid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " URI="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tput="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " timestampSec= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE;->dlogv(Ljava/lang/String;Ljava/lang/String;)V

    .line 2124
    new-instance v10, Ljava/lang/Thread;

    new-instance v0, Lcom/quicinc/cne/BQEClient;

    iget-object v2, p0, Lcom/quicinc/cne/CNE;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget-object v4, p0, Lcom/quicinc/cne/CNE;->getRequestUrl:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/quicinc/cne/BQEClient;-><init>(Lcom/quicinc/cne/CNE;Landroid/net/wifi/WifiManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v10, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    .line 2128
    return-void
.end method

.method private handleSetDefaultRouteMsg(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    .line 2150
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 2151
    .local v0, "ratType":I
    const-string v3, "CORE"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "handleSetDefaultRouteMsg for ratType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/quicinc/cne/CNE;->dlogd(Ljava/lang/String;Ljava/lang/String;)V

    .line 2152
    const/4 v1, 0x0

    .line 2153
    .local v1, "state":I
    if-ne v0, v2, :cond_0

    move v1, v2

    .line 2154
    :goto_0
    invoke-direct {p0, v1}, Lcom/quicinc/cne/CNE;->broadcastWqeStateChange(I)V

    .line 2155
    return-void

    .line 2153
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private handleStartActiveBQEMsg(Landroid/os/Parcel;)V
    .locals 7
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 2105
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 2106
    .local v4, "bssid":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 2107
    .local v3, "uri":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 2108
    .local v5, "fileSize":Ljava/lang/String;
    iput-object v3, p0, Lcom/quicinc/cne/CNE;->getRequestUrl:Ljava/lang/String;

    .line 2109
    const-string v0, "CORE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleStartActiveBQEMsg called, bssid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " URI="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fileSize="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE;->dlogv(Ljava/lang/String;Ljava/lang/String;)V

    .line 2111
    new-instance v6, Ljava/lang/Thread;

    new-instance v0, Lcom/quicinc/cne/BQEClient;

    iget-object v2, p0, Lcom/quicinc/cne/CNE;->mWifiManager:Landroid/net/wifi/WifiManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/quicinc/cne/BQEClient;-><init>(Lcom/quicinc/cne/CNE;Landroid/net/wifi/WifiManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v6, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 2112
    return-void
.end method

.method private handleStartICDMsg(Landroid/os/Parcel;)V
    .locals 9
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 2166
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 2167
    .local v3, "uri":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 2168
    .local v4, "httpuri":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 2169
    .local v5, "ssid":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 2170
    .local v6, "timeout":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 2171
    .local v7, "tid":I
    const-string v0, "CORE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleStartICDMsg called with uri= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " httpuri= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ssid= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " timeout= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " tid= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE;->dlogv(Ljava/lang/String;Ljava/lang/String;)V

    .line 2174
    new-instance v8, Ljava/lang/Thread;

    new-instance v0, Lcom/quicinc/cne/ICDClient;

    iget-object v2, p0, Lcom/quicinc/cne/CNE;->mWifiManager:Landroid/net/wifi/WifiManager;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/quicinc/cne/ICDClient;-><init>(Lcom/quicinc/cne/CNE;Landroid/net/wifi/WifiManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-direct {v8, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    .line 2176
    return-void
.end method

.method private handleStopActiveBQEMsg(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 2132
    const-string v0, "CORE"

    const-string v1, "handleStopActiveBQEMsg called"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE;->dlogv(Ljava/lang/String;Ljava/lang/String;)V

    .line 2133
    invoke-static {}, Lcom/quicinc/cne/BQEClient;->stop()V

    .line 2134
    return-void
.end method

.method private static logd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 3066
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 3067
    return-void
.end method

.method private static loge(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 3078
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 3079
    return-void
.end method

.method private static logi(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 3072
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 3073
    return-void
.end method

.method private static logv(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 3069
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 3070
    return-void
.end method

.method private static logw(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 3075
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 3076
    return-void
.end method

.method private notifyNetIdInfo(II)Z
    .locals 7
    .param p1, "netType"    # I
    .param p2, "netId"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1817
    const-string v4, "CORE"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "notifyNetIdInfo netType="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " netId="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 1818
    const/16 v4, 0x24

    invoke-static {v4}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v1

    .line 1819
    .local v1, "rr":Lcom/quicinc/cne/CNERequest;
    const/4 v0, -0x1

    .line 1820
    .local v0, "netTp":I
    if-nez v1, :cond_0

    .line 1821
    const-string v3, "CORE"

    const-string v4, "notifyNetIdInfo: rr=NULL"

    invoke-static {v3, v4}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1839
    :goto_0
    return v2

    .line 1824
    :cond_0
    if-ne p1, v3, :cond_1

    .line 1825
    const/4 v0, 0x1

    .line 1834
    :goto_1
    iget-object v2, p0, Lcom/quicinc/cne/CNE;->netId:[I

    aput p2, v2, v0

    .line 1835
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1836
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1837
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1838
    invoke-virtual {p0, v1}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    move v2, v3

    .line 1839
    goto :goto_0

    .line 1827
    :cond_1
    if-nez p1, :cond_2

    .line 1828
    const/4 v0, 0x0

    goto :goto_1

    .line 1831
    :cond_2
    const-string v3, "CORE"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "notifyNetIdInfo invalid netType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static parseBwString(Ljava/lang/String;)I
    .locals 6
    .param p0, "rate"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 2449
    if-nez p0, :cond_0

    .line 2474
    :goto_0
    return v3

    .line 2451
    :cond_0
    const/4 v1, 0x1

    .line 2452
    .local v1, "rateMultiple":I
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "kbps"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "kbit/s"

    invoke-virtual {p0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "kb/s"

    invoke-virtual {p0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2454
    :cond_1
    const/16 v1, 0x3e8

    .line 2464
    :cond_2
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    .line 2465
    .local v2, "trimPosition":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v0, v4, :cond_4

    .line 2466
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x30

    if-le v4, v5, :cond_3

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x39

    if-lt v4, v5, :cond_a

    .line 2467
    :cond_3
    move v2, v0

    .line 2471
    :cond_4
    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 2472
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_5

    const-string p0, "0"

    .line 2474
    :cond_5
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    mul-int/2addr v3, v1

    goto :goto_0

    .line 2455
    .end local v0    # "i":I
    .end local v2    # "trimPosition":I
    :cond_6
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "mbps"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_7

    const-string v4, "Mbit/s"

    invoke-virtual {p0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_7

    const-string v4, "Mb/s"

    invoke-virtual {p0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2457
    :cond_7
    const v1, 0xf4240

    goto :goto_1

    .line 2458
    :cond_8
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "gbps"

    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_9

    const-string v4, "Gbit/s"

    invoke-virtual {p0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_9

    const-string v4, "Gb/s"

    invoke-virtual {p0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2460
    :cond_9
    const v1, 0x3b9aca00

    goto :goto_1

    .line 2465
    .restart local v0    # "i":I
    .restart local v2    # "trimPosition":I
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method private postCndUpInit()V
    .locals 10

    .prologue
    const/16 v9, 0x3e8

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1115
    const-string v5, "CORE"

    const-string v6, "starting initialization of components that require cnd to have started"

    invoke-static {v5, v6}, Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 1119
    const/4 v2, 0x0

    .local v2, "iface":I
    :goto_0
    const/4 v5, 0x4

    if-ge v2, v5, :cond_1

    .line 1120
    iget-object v5, p0, Lcom/quicinc/cne/CNE;->netId:[I

    aget v5, v5, v2

    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 1121
    iget-object v5, p0, Lcom/quicinc/cne/CNE;->netId:[I

    aget v5, v5, v2

    invoke-direct {p0, v2, v5}, Lcom/quicinc/cne/CNE;->notifyNetIdInfo(II)Z

    .line 1119
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1128
    :cond_1
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->sendWifiStatus()V

    .line 1131
    iput v8, p0, Lcom/quicinc/cne/CNE;->lastFamilyType:I

    .line 1132
    iget-object v5, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    if-eqz v5, :cond_2

    .line 1133
    iget-object v5, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget v5, v5, Lcom/quicinc/cne/CNE$CneWifiInfo;->networkState:I

    sget-object v6, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    invoke-direct {p0, v6}, Lcom/quicinc/cne/CNE;->NetworkStateToInt(Landroid/net/NetworkInfo$State;)I

    move-result v6

    if-ne v5, v6, :cond_8

    .line 1134
    iput-boolean v7, p0, Lcom/quicinc/cne/CNE;->isWifiConnected:Z

    .line 1135
    iget-boolean v5, p0, Lcom/quicinc/cne/CNE;->isWifiConnected:Z

    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->getInetFamily()I

    move-result v6

    invoke-virtual {p0, v5, v6}, Lcom/quicinc/cne/CNE;->notifyWlanConnectivityUp(ZI)Z

    .line 1142
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->sendWwanStatus()V

    .line 1146
    iget-object v5, p0, Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;

    const-string v6, "power"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    .line 1147
    .local v3, "pm":Landroid/os/PowerManager;
    if-eqz v3, :cond_3

    .line 1148
    invoke-virtual {v3}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v5

    iput-boolean v5, p0, Lcom/quicinc/cne/CNE;->mScreenOn:Z

    .line 1149
    iget-boolean v5, p0, Lcom/quicinc/cne/CNE;->mScreenOn:Z

    invoke-direct {p0, v5}, Lcom/quicinc/cne/CNE;->sendScreenState(Z)V

    .line 1154
    :cond_3
    iget v5, p0, Lcom/quicinc/cne/CNE;->mDefaultNetwork:I

    invoke-direct {p0, v5}, Lcom/quicinc/cne/CNE;->sendDefaultNwMsg(I)Z

    .line 1158
    invoke-direct {p0, v7, v8}, Lcom/quicinc/cne/CNE;->setPolicyConfigUpdateBusy(IZ)V

    .line 1159
    iget-boolean v5, p0, Lcom/quicinc/cne/CNE;->andsfHasBeenInit:Z

    if-nez v5, :cond_6

    .line 1160
    const/4 v4, 0x0

    .line 1161
    .local v4, "success":Z
    new-instance v1, Ljava/io/File;

    const-string v5, "data/connectivity/andsfCne.xml"

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1162
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1164
    const/4 v5, 0x1

    :try_start_0
    const-string v6, "data/connectivity/andsfCne.xml"

    invoke-virtual {p0, v5, v6}, Lcom/quicinc/cne/CNE;->updatePolicy(ILjava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-ne v5, v9, :cond_4

    .line 1165
    const/4 v4, 0x1

    .line 1171
    :cond_4
    :goto_2
    if-nez v4, :cond_5

    .line 1172
    const-string v5, "CORE"

    const-string v6, "Using Fallback andsfCne.xml"

    invoke-static {v5, v6}, Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 1174
    const/4 v5, 0x1

    :try_start_1
    const-string v6, "system/etc/cne/andsfCne.xml"

    invoke-virtual {p0, v5, v6}, Lcom/quicinc/cne/CNE;->updatePolicy(ILjava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    if-ne v5, v9, :cond_5

    .line 1175
    const/4 v4, 0x1

    .line 1181
    :cond_5
    :goto_3
    iput-boolean v4, p0, Lcom/quicinc/cne/CNE;->andsfHasBeenInit:Z

    .line 1183
    .end local v1    # "f":Ljava/io/File;
    .end local v4    # "success":Z
    :cond_6
    sget-boolean v5, Lcom/quicinc/cne/CNE;->isCndDisconnected:Z

    if-eqz v5, :cond_7

    .line 1184
    const-string v5, "CORE"

    const-string v6, "Recovering from cnd crashed"

    invoke-static {v5, v6}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1185
    iget-boolean v5, p0, Lcom/quicinc/cne/CNE;->mScreenOn:Z

    invoke-direct {p0, v5}, Lcom/quicinc/cne/CNE;->sendScreenState(Z)V

    .line 1186
    iget-object v5, p0, Lcom/quicinc/cne/CNE;->mPackageListener:Lcom/quicinc/cne/CNE$PackageListener;

    if-eqz v5, :cond_7

    .line 1187
    iget-object v5, p0, Lcom/quicinc/cne/CNE;->mPackageListener:Lcom/quicinc/cne/CNE$PackageListener;

    invoke-virtual {v5}, Lcom/quicinc/cne/CNE$PackageListener;->sendInstalledPackageInfo()Z

    .line 1190
    :cond_7
    return-void

    .line 1137
    .end local v3    # "pm":Landroid/os/PowerManager;
    :cond_8
    iput-boolean v8, p0, Lcom/quicinc/cne/CNE;->isWifiConnected:Z

    .line 1138
    iget-boolean v5, p0, Lcom/quicinc/cne/CNE;->isWifiConnected:Z

    invoke-direct {p0}, Lcom/quicinc/cne/CNE;->getInetFamily()I

    move-result v6

    invoke-virtual {p0, v5, v6}, Lcom/quicinc/cne/CNE;->notifyWlanConnectivityUp(ZI)Z

    goto :goto_1

    .line 1167
    .restart local v1    # "f":Ljava/io/File;
    .restart local v3    # "pm":Landroid/os/PowerManager;
    .restart local v4    # "success":Z
    :catch_0
    move-exception v0

    .line 1168
    .local v0, "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 1177
    .end local v0    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 1178
    .restart local v0    # "ex":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method

.method private processResponse(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 1525
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1526
    .local v0, "type":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1527
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->processUnsolicited(Landroid/os/Parcel;)V

    .line 1534
    :goto_0
    return-void

    .line 1528
    :cond_0
    if-nez v0, :cond_1

    .line 1529
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->processSolicited(Landroid/os/Parcel;)V

    goto :goto_0

    .line 1531
    :cond_1
    const-string v1, "CORE"

    const-string v2, "malformed message, expected RESPONSE_UNSOLICITED or RESPONSE_SOLICITED, rejecting"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->dlogw(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private processSolicited(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 1551
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1552
    .local v2, "serial":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1554
    .local v0, "error":I
    invoke-direct {p0, v2}, Lcom/quicinc/cne/CNE;->findAndRemoveRequestFromList(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v1

    .line 1555
    .local v1, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v1, :cond_0

    .line 1556
    const-string v3, "CORE"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected solicited response! sn: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1566
    :goto_0
    return-void

    .line 1560
    :cond_0
    if-eqz v0, :cond_1

    .line 1561
    invoke-virtual {v1, v0}, Lcom/quicinc/cne/CNERequest;->onError(I)V

    .line 1562
    invoke-virtual {v1}, Lcom/quicinc/cne/CNERequest;->release()V

    goto :goto_0

    .line 1565
    :cond_1
    invoke-virtual {v1}, Lcom/quicinc/cne/CNERequest;->release()V

    goto :goto_0
.end method

.method private processUnsolicited(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    .line 1569
    const-string v1, "CORE"

    const-string v2, "processUnsolicited called"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 1572
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1573
    .local v0, "response":I
    packed-switch v0, :pswitch_data_0

    .line 1635
    :pswitch_0
    const-string v1, "CORE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UNKOWN Unsolicited Event "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1638
    :goto_0
    return-void

    .line 1575
    :pswitch_1
    const-string v1, "CORE"

    const-string v2, "CNE_REQUEST_START_ACTIVE_BQE received"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->rlog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1576
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->handleStartActiveBQEMsg(Landroid/os/Parcel;)V

    goto :goto_0

    .line 1580
    :pswitch_2
    const-string v1, "CORE"

    const-string v2, "CNE_REQUEST_STOP_ACTIVE_BQE received"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->rlog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1581
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->handleStopActiveBQEMsg(Landroid/os/Parcel;)V

    goto :goto_0

    .line 1585
    :pswitch_3
    const-string v1, "CORE"

    const-string v2, "CNE_REQUEST_SET_DEFAULT_ROUTE_MSG received"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->rlog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1586
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->handleSetDefaultRouteMsg(Landroid/os/Parcel;)V

    goto :goto_0

    .line 1590
    :pswitch_4
    const-string v1, "CORE"

    const-string v2, "CNE_REQUEST_GET_APP_INFO_LIST received"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->rlog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1591
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->handleGetAppInfoMsg(Landroid/os/Parcel;)V

    goto :goto_0

    .line 1595
    :pswitch_5
    const-string v1, "CORE"

    const-string v2, "CNE_REQUEST_GET_BROWSERS_INFO_LIST received"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->rlog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1596
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->handleGetBrowsersInfoMsg(Landroid/os/Parcel;)V

    goto :goto_0

    .line 1600
    :pswitch_6
    const-string v1, "CORE"

    const-string v2, "CNE_REQUEST_START_ICD received"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->rlog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1601
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->handleStartICDMsg(Landroid/os/Parcel;)V

    goto :goto_0

    .line 1605
    :pswitch_7
    const-string v1, "CORE"

    const-string v2, "CNE_NOTIFY_DNS_PRIORITY_CMD received"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->rlog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1606
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->handleDnsPriorityCmd(Landroid/os/Parcel;)V

    goto :goto_0

    .line 1610
    :pswitch_8
    const-string v1, "CORE"

    const-string v2, "CNE_REQUEST_POST_BQE_RESULTS received"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->rlog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1611
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->handlePostBqeResult(Landroid/os/Parcel;)V

    goto :goto_0

    .line 1615
    :pswitch_9
    const-string v1, "CORE"

    const-string v2, " CNE_REQUEST_GET_PARENT_APP received"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->rlog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1616
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->handleGetParentApp(Landroid/os/Parcel;)V

    goto :goto_0

    .line 1620
    :pswitch_a
    const-string v1, "CORE"

    const-string v2, "CNE_NOTIFY_FEATURE_STATUS received"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->rlog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1621
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->handleFeatureStatusNotification(Landroid/os/Parcel;)V

    goto :goto_0

    .line 1625
    :pswitch_b
    const-string v1, "CORE"

    const-string v2, "CNE_RESP_SET_FEATURE_PREF received"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->rlog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1626
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->handleFeatureStatusSetResponse(Landroid/os/Parcel;)V

    goto :goto_0

    .line 1630
    :pswitch_c
    const-string v1, "CORE"

    const-string v2, " CNE_NOTIFY_POLICY_UPDATE_DONE received"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->rlog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1631
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->handlePolicyUpdateResponse(Landroid/os/Parcel;)V

    goto/16 :goto_0

    .line 1573
    :pswitch_data_0
    .packed-switch 0xf
        :pswitch_1
        :pswitch_3
        :pswitch_6
        :pswitch_4
        :pswitch_7
        :pswitch_2
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_9
    .end packed-switch
.end method

.method private static readCneMessage(Ljava/io/InputStream;[B)I
    .locals 8
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "buffer"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    .line 955
    const/4 v2, 0x0

    .line 956
    .local v2, "offset":I
    const/4 v3, 0x4

    .line 958
    .local v3, "remaining":I
    :cond_0
    invoke-virtual {p0, p1, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 960
    .local v0, "countRead":I
    if-gez v0, :cond_1

    .line 961
    const-string v5, "CORE"

    const-string v6, "Hit EOS reading message length"

    invoke-static {v5, v6}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v4

    .line 989
    :goto_0
    return v1

    .line 965
    :cond_1
    add-int/2addr v2, v0

    .line 966
    sub-int/2addr v3, v0

    .line 967
    if-gtz v3, :cond_0

    .line 969
    const/4 v5, 0x0

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x18

    const/4 v6, 0x1

    aget-byte v6, p1, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x10

    or-int/2addr v5, v6

    const/4 v6, 0x2

    aget-byte v6, p1, v6

    and-int/lit16 v6, v6, 0xff

    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v5, v6

    const/4 v6, 0x3

    aget-byte v6, p1, v6

    and-int/lit16 v6, v6, 0xff

    or-int v1, v5, v6

    .line 974
    .local v1, "messageLength":I
    const/4 v2, 0x0

    .line 975
    move v3, v1

    .line 977
    :cond_2
    invoke-virtual {p0, p1, v2, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 979
    if-gez v0, :cond_3

    .line 980
    const-string v5, "CORE"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Hit EOS reading message.  messageLength="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " remaining="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v4

    .line 982
    goto :goto_0

    .line 985
    :cond_3
    add-int/2addr v2, v0

    .line 986
    sub-int/2addr v3, v0

    .line 987
    if-gtz v3, :cond_2

    goto :goto_0
.end method

.method private registerCNENetworkFactory()V
    .locals 2

    .prologue
    .line 1337
    new-instance v0, Lcom/quicinc/cne/CNE$2;

    const-string v1, "cnenetworkfactory"

    invoke-direct {v0, p0, v1}, Lcom/quicinc/cne/CNE$2;-><init>(Lcom/quicinc/cne/CNE;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/quicinc/cne/CNE;->mFactoryThread:Landroid/os/HandlerThread;

    .line 1352
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->mFactoryThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 1353
    return-void
.end method

.method public static registerRequestHandler(ILandroid/os/Handler;)V
    .locals 3
    .param p0, "request"    # I
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 1515
    sget-object v1, Lcom/quicinc/cne/CNE;->mRequestHandlers:Ljava/util/concurrent/ConcurrentHashMap;

    monitor-enter v1

    .line 1516
    :try_start_0
    sget-object v0, Lcom/quicinc/cne/CNE;->mRequestHandlers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1517
    const-string v0, "CORE"

    const-string v2, "Handler already registered overriding with new handler."

    invoke-static {v0, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1519
    :cond_0
    sget-object v0, Lcom/quicinc/cne/CNE;->mRequestHandlers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1520
    monitor-exit v1

    .line 1521
    return-void

    .line 1520
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private requestFeatureSettings(I)Z
    .locals 5
    .param p1, "featureId"    # I

    .prologue
    const/4 v1, 0x1

    .line 1218
    const/16 v2, 0x1e

    invoke-static {v2}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v0

    .line 1219
    .local v0, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v0, :cond_0

    .line 1220
    const-string v1, "CORE"

    const-string v2, "requestFeatureSettings: rr=NULL"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1221
    const/4 v1, 0x0

    .line 1228
    :goto_0
    return v1

    .line 1223
    :cond_0
    iget-object v2, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224
    iget-object v2, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1226
    const-string v2, "CORE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "requestFeatureSettings: featureId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 1227
    invoke-virtual {p0, v0}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    goto :goto_0
.end method

.method private requestFeatureSettingsChange(II)Z
    .locals 4
    .param p1, "featureId"    # I
    .param p2, "newValue"    # I

    .prologue
    .line 1202
    const/16 v1, 0x1f

    invoke-static {v1}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v0

    .line 1203
    .local v0, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v0, :cond_0

    .line 1204
    const-string v1, "CORE"

    const-string v2, "requestFeatureSettingsChange: rr=NULL"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1205
    const/4 v1, 0x0

    .line 1214
    :goto_0
    return v1

    .line 1207
    :cond_0
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1208
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1209
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1211
    const-string v1, "CORE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestFeatureSettingsChange: feature id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " new value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 1213
    invoke-virtual {p0, v0}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    .line 1214
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private requestFeaturesSettings()V
    .locals 1

    .prologue
    .line 1197
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/quicinc/cne/CNE;->requestFeatureSettings(I)Z

    .line 1198
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/quicinc/cne/CNE;->requestFeatureSettings(I)Z

    .line 1199
    return-void
.end method

.method private static rlog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 3100
    invoke-static {p0, p1}, Lcom/quicinc/cne/CneMsg;->rlog(Ljava/lang/String;Ljava/lang/String;)V

    .line 3101
    return-void
.end method

.method private sendAppInfoList(Lcom/quicinc/cne/CNERequest;)V
    .locals 2
    .param p1, "rr"    # Lcom/quicinc/cne/CNERequest;

    .prologue
    .line 2100
    const-string v0, "CORE"

    const-string v1, "sendAppInfoList"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2101
    invoke-virtual {p0, p1}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    .line 2102
    return-void
.end method

.method private declared-synchronized sendBrowserInfoList()Z
    .locals 15

    .prologue
    const/4 v10, 0x0

    .line 2754
    monitor-enter p0

    :try_start_0
    new-instance v2, Landroid/content/Intent;

    const-string v11, "android.intent.action.VIEW"

    const-string v12, "http://www.google.com"

    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-direct {v2, v11, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2755
    .local v2, "i":Landroid/content/Intent;
    iget-object v11, p0, Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v11, v12, v13, v2, v14}, Landroid/content/pm/PackageManager;->queryIntentActivityOptions(Landroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 2758
    .local v0, "activityList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v4, Landroid/content/Intent;

    const-string v11, "android.intent.action.VIEW"

    const-string v12, "https://www.google.com"

    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-direct {v4, v11, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2759
    .local v4, "i1":Landroid/content/Intent;
    iget-object v11, p0, Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v11, v12, v13, v4, v14}, Landroid/content/pm/PackageManager;->queryIntentActivityOptions(Landroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 2762
    .local v1, "activityList1":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 2764
    .local v6, "packageNamesSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/ResolveInfo;

    .line 2765
    .local v8, "someActivityResolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v11, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2754
    .end local v0    # "activityList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v1    # "activityList1":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v2    # "i":Landroid/content/Intent;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "i1":Landroid/content/Intent;
    .end local v6    # "packageNamesSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v8    # "someActivityResolveInfo":Landroid/content/pm/ResolveInfo;
    :catchall_0
    move-exception v10

    monitor-exit p0

    throw v10

    .line 2768
    .restart local v0    # "activityList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v1    # "activityList1":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v2    # "i":Landroid/content/Intent;
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "i1":Landroid/content/Intent;
    .restart local v6    # "packageNamesSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_0
    :try_start_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/ResolveInfo;

    .line 2769
    .restart local v8    # "someActivityResolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v11, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v11}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2772
    .end local v8    # "someActivityResolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_1
    const/16 v11, 0x1d

    invoke-static {v11}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v7

    .line 2774
    .local v7, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v7, :cond_3

    .line 2775
    const-string v11, "CORE"

    const-string v12, "sendBrowserInfoList: rr=NULL"

    invoke-static {v11, v12}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2792
    :cond_2
    :goto_2
    monitor-exit p0

    return v10

    .line 2779
    :cond_3
    :try_start_2
    invoke-virtual {v6}, Ljava/util/HashSet;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_2

    .line 2780
    const-string v10, "CORE"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "CNE- sendBrowserInfoList: browsers list size = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2781
    iget-object v10, v7, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v6}, Ljava/util/HashSet;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Landroid/os/Parcel;->writeInt(I)V

    .line 2783
    invoke-virtual {v6}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 2784
    .local v5, "it":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2786
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 2787
    .local v9, "value":Ljava/lang/String;
    iget-object v10, v7, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v10, v9}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3

    .line 2789
    .end local v9    # "value":Ljava/lang/String;
    :cond_4
    invoke-virtual {p0, v7}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2790
    const/4 v10, 0x1

    goto :goto_2
.end method

.method private sendDefaultNwMsg(I)Z
    .locals 5
    .param p1, "defNw"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1780
    const/4 v3, -0x1

    if-le p1, v3, :cond_0

    const/16 v3, 0x11

    if-le p1, v3, :cond_1

    .line 1782
    :cond_0
    const-string v2, "CORE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendDefaultNwMsg: Default network msg not being sent to CND.Value out of range: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->dlogw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1795
    :goto_0
    return v1

    .line 1786
    :cond_1
    const/16 v3, 0x13

    invoke-static {v3}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v0

    .line 1787
    .local v0, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v0, :cond_2

    .line 1788
    const-string v2, "CORE"

    const-string v3, "sendDefaultNwMsg: rr=NULL - not updated"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1791
    :cond_2
    const-string v1, "CORE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendDefaultNwMsg: default = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 1792
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1793
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE;->NetworkTypetoCneRatType(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1794
    invoke-virtual {p0, v0}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    move v1, v2

    .line 1795
    goto :goto_0
.end method

.method private sendInitReq()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2090
    invoke-static {v1}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v0

    .line 2091
    .local v0, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v0, :cond_0

    .line 2092
    const-string v1, "CORE"

    const-string v2, "sendinitReq: rr=NULL"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 2093
    const/4 v1, 0x0

    .line 2096
    :goto_0
    return v1

    .line 2095
    :cond_0
    invoke-virtual {p0, v0}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    goto :goto_0
.end method

.method private declared-synchronized sendPrefChangedBroadcast(III)V
    .locals 5
    .param p1, "featureId"    # I
    .param p2, "featureParameter"    # I
    .param p3, "value"    # I

    .prologue
    .line 2705
    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.quicinc.cne.CNE_PREFERENCE_CHANGED"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2706
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "cneFeatureId"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2707
    const-string v2, "cneFeatureParameter"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2708
    const-string v2, "cneParameterValue"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2711
    :try_start_1
    iget-object v2, p0, Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;

    const-string v3, "android.permission.BROADCAST_STICKY"

    const-string v4, "CNE sendPrefChangedBroadcast()"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 2713
    iget-object v2, p0, Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2717
    :goto_0
    monitor-exit p0

    return-void

    .line 2714
    :catch_0
    move-exception v1

    .line 2715
    .local v1, "se":Ljava/lang/SecurityException;
    :try_start_2
    const-string v2, "CORE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendPrefChangedBroadcast() SecurityException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->loge(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 2705
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "se":Ljava/lang/SecurityException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private sendScreenState(Z)V
    .locals 4
    .param p1, "state"    # Z

    .prologue
    const/4 v1, 0x1

    .line 2076
    const/16 v2, 0x22

    invoke-static {v2}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v0

    .line 2077
    .local v0, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v0, :cond_0

    .line 2078
    const-string v1, "CORE"

    const-string v2, "sendScreenState: rr=NULL"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 2087
    :goto_0
    return-void

    .line 2081
    :cond_0
    iget-object v2, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2082
    iget-object v2, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2083
    iget-object v2, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    if-ne p1, v1, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2085
    const-string v1, "CORE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendScreenState: state:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2086
    invoke-virtual {p0, v0}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    goto :goto_0

    .line 2083
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private sendWifiStatus()V
    .locals 14

    .prologue
    .line 1983
    iget-object v9, p0, Lcom/quicinc/cne/CNE;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    if-eqz v9, :cond_3

    .line 1985
    :try_start_0
    iget-object v9, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    new-instance v10, Ljava/sql/Timestamp;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-direct {v10, v12, v13}, Ljava/sql/Timestamp;-><init>(J)V

    invoke-virtual {v10}, Ljava/sql/Timestamp;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/quicinc/cne/CNE$CneWifiInfo;->setTimeStamp(Ljava/lang/String;)V

    .line 1986
    iget-object v9, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v2, v9, Lcom/quicinc/cne/CNE$CneWifiInfo;->dnsInfo:[Ljava/lang/String;

    .line 1988
    .local v2, "dnsInfo":[Ljava/lang/String;
    new-instance v1, Ljava/lang/String;

    const-string v9, " "

    invoke-direct {v1, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 1989
    .local v1, "dnsAddrs":Ljava/lang/String;
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v3, v0, v5

    .line 1990
    .local v3, "dnsStr":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1989
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1993
    .end local v3    # "dnsStr":Ljava/lang/String;
    :cond_0
    const-string v9, "CORE"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "sendWifiStatus - subType: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget v11, v11, Lcom/quicinc/cne/CNE$CneWifiInfo;->subType:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " networkState: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget v11, v11, Lcom/quicinc/cne/CNE$CneWifiInfo;->networkState:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " softApState: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget v11, v11, Lcom/quicinc/cne/CNE$CneWifiInfo;->softApState:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " rssi="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget v11, v11, Lcom/quicinc/cne/CNE$CneWifiInfo;->rssi:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " ssid="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v11, v11, Lcom/quicinc/cne/CNE$CneWifiInfo;->ssid:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " bssid="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v11, v11, Lcom/quicinc/cne/CNE$CneWifiInfo;->bssid:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " ipV4Addr="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v11, v11, Lcom/quicinc/cne/CNE$CneWifiInfo;->ipAddrV4:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " ifNameV4="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v11, v11, Lcom/quicinc/cne/CNE$CneWifiInfo;->ifNameV4:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " ipAddrV6="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v11, v11, Lcom/quicinc/cne/CNE$CneWifiInfo;->ipAddrV6:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " ifNameV6="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v11, v11, Lcom/quicinc/cne/CNE$CneWifiInfo;->ifNameV6:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " timeStamp:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v11, v11, Lcom/quicinc/cne/CNE$CneWifiInfo;->timeStamp:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " DNS addrs="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2007
    const/16 v9, 0x8

    invoke-static {v9}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v8

    .line 2008
    .local v8, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v8, :cond_1

    .line 2009
    const-string v9, "CORE"

    const-string v10, "updateWlanStatus: rr=NULL - no updated"

    invoke-static {v9, v10}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 2034
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "dnsAddrs":Ljava/lang/String;
    .end local v2    # "dnsInfo":[Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v8    # "rr":Lcom/quicinc/cne/CNERequest;
    :goto_1
    return-void

    .line 2013
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v1    # "dnsAddrs":Ljava/lang/String;
    .restart local v2    # "dnsInfo":[Ljava/lang/String;
    .restart local v5    # "i$":I
    .restart local v6    # "len$":I
    .restart local v8    # "rr":Lcom/quicinc/cne/CNERequest;
    :cond_1
    iget-object v9, v8, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v10, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget v10, v10, Lcom/quicinc/cne/CNE$CneWifiInfo;->subType:I

    invoke-virtual {v9, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 2014
    iget-object v9, v8, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v10, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget v10, v10, Lcom/quicinc/cne/CNE$CneWifiInfo;->networkState:I

    invoke-virtual {v9, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 2015
    iget-object v9, v8, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v10, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget v10, v10, Lcom/quicinc/cne/CNE$CneWifiInfo;->softApState:I

    invoke-virtual {v9, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 2016
    iget-object v9, v8, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v10, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget v10, v10, Lcom/quicinc/cne/CNE$CneWifiInfo;->rssi:I

    invoke-virtual {v9, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 2017
    iget-object v9, v8, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v10, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v10, v10, Lcom/quicinc/cne/CNE$CneWifiInfo;->ssid:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2018
    iget-object v9, v8, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v10, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v10, v10, Lcom/quicinc/cne/CNE$CneWifiInfo;->bssid:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2019
    iget-object v9, v8, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v10, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v10, v10, Lcom/quicinc/cne/CNE$CneWifiInfo;->ipAddrV4:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2020
    iget-object v9, v8, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v10, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v10, v10, Lcom/quicinc/cne/CNE$CneWifiInfo;->ifNameV4:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2021
    iget-object v9, v8, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v10, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v10, v10, Lcom/quicinc/cne/CNE$CneWifiInfo;->ipAddrV6:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2022
    iget-object v9, v8, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v10, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v10, v10, Lcom/quicinc/cne/CNE$CneWifiInfo;->ifNameV6:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2023
    iget-object v9, v8, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v10, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v10, v10, Lcom/quicinc/cne/CNE$CneWifiInfo;->timeStamp:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2024
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    const/4 v9, 0x2

    if-ge v4, v9, :cond_2

    .line 2025
    iget-object v9, v8, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v10, p0, Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;

    iget-object v10, v10, Lcom/quicinc/cne/CNE$CneWifiInfo;->dnsInfo:[Ljava/lang/String;

    aget-object v10, v10, v4

    invoke-virtual {v9, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2024
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2027
    :cond_2
    invoke-virtual {p0, v8}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2028
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "dnsAddrs":Ljava/lang/String;
    .end local v2    # "dnsInfo":[Ljava/lang/String;
    .end local v4    # "i":I
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v8    # "rr":Lcom/quicinc/cne/CNERequest;
    :catch_0
    move-exception v7

    .line 2029
    .local v7, "npe":Ljava/lang/NullPointerException;
    const-string v9, "CORE"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "sendWifiStatus: null pointer"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2032
    .end local v7    # "npe":Ljava/lang/NullPointerException;
    :cond_3
    const-string v9, "CORE"

    const-string v10, "sendWlanStatus: null mWifiManager or CneWifiInfo"

    invoke-static {v9, v10}, Lcom/quicinc/cne/CNE;->dlogw(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private sendWwanStatus()V
    .locals 6

    .prologue
    .line 2037
    iget-object v2, p0, Lcom/quicinc/cne/CNE;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    if-eqz v2, :cond_1

    .line 2039
    :try_start_0
    iget-object v2, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    new-instance v3, Ljava/sql/Timestamp;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/sql/Timestamp;-><init>(J)V

    invoke-virtual {v3}, Ljava/sql/Timestamp;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/quicinc/cne/CNE$CneWwanInfo;->setTimeStamp(Ljava/lang/String;)V

    .line 2040
    const/16 v2, 0x9

    invoke-static {v2}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v1

    .line 2041
    .local v1, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v1, :cond_0

    .line 2042
    const-string v2, "CORE"

    const-string v3, "sendWwanStatus: rr=NULL - no updated"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 2044
    :cond_0
    const-string v2, "CORE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendWwanStatus type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget v4, v4, Lcom/quicinc/cne/CNE$CneWwanInfo;->subType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " state="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget v4, v4, Lcom/quicinc/cne/CNE$CneWwanInfo;->networkState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " strength="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget v4, v4, Lcom/quicinc/cne/CNE$CneWwanInfo;->signalStrength:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " roaming="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget v4, v4, Lcom/quicinc/cne/CNE$CneWwanInfo;->roaming:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ipV4Addr="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget-object v4, v4, Lcom/quicinc/cne/CNE$CneWwanInfo;->ipAddrV4:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ifNameV4="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget-object v4, v4, Lcom/quicinc/cne/CNE$CneWwanInfo;->ifNameV4:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ipV6Addr="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget-object v4, v4, Lcom/quicinc/cne/CNE$CneWwanInfo;->ipAddrV6:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ifNameV6="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget-object v4, v4, Lcom/quicinc/cne/CNE$CneWwanInfo;->ifNameV6:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " timeStamp="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget-object v4, v4, Lcom/quicinc/cne/CNE$CneWwanInfo;->timeStamp:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mccMnc="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget-object v4, v4, Lcom/quicinc/cne/CNE$CneWwanInfo;->mccMnc:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2054
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v3, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget v3, v3, Lcom/quicinc/cne/CNE$CneWwanInfo;->subType:I

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2055
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v3, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget v3, v3, Lcom/quicinc/cne/CNE$CneWwanInfo;->networkState:I

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2056
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v3, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget v3, v3, Lcom/quicinc/cne/CNE$CneWwanInfo;->signalStrength:I

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2057
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v3, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget v3, v3, Lcom/quicinc/cne/CNE$CneWwanInfo;->roaming:I

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2058
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v3, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget-object v3, v3, Lcom/quicinc/cne/CNE$CneWwanInfo;->ipAddrV4:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2059
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v3, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget-object v3, v3, Lcom/quicinc/cne/CNE$CneWwanInfo;->ifNameV4:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2060
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v3, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget-object v3, v3, Lcom/quicinc/cne/CNE$CneWwanInfo;->ipAddrV6:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2061
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v3, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget-object v3, v3, Lcom/quicinc/cne/CNE$CneWwanInfo;->ifNameV6:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2062
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v3, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget-object v3, v3, Lcom/quicinc/cne/CNE$CneWwanInfo;->timeStamp:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2063
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget-object v3, p0, Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;

    iget-object v3, v3, Lcom/quicinc/cne/CNE$CneWwanInfo;->mccMnc:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2064
    invoke-virtual {p0, v1}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2072
    .end local v1    # "rr":Lcom/quicinc/cne/CNERequest;
    :goto_0
    return-void

    .line 2066
    :catch_0
    move-exception v0

    .line 2067
    .local v0, "npe":Ljava/lang/NullPointerException;
    const-string v2, "CORE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendWwanStatus: null pointer "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2070
    .end local v0    # "npe":Ljava/lang/NullPointerException;
    :cond_1
    const-string v2, "CORE"

    const-string v3, "sendWwanStatus: null TelephonyManager or CneWwanInfo"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->dlogw(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setPolicyConfigUpdateBusy(IZ)V
    .locals 3
    .param p1, "policyType"    # I
    .param p2, "tryLater"    # Z

    .prologue
    .line 2551
    packed-switch p1, :pswitch_data_0

    .line 2562
    :goto_0
    return-void

    .line 2553
    :pswitch_0
    monitor-enter p0

    .line 2554
    :try_start_0
    sput-boolean p2, Lcom/quicinc/cne/CNE;->isAndsfConfigUpdateBusy:Z

    .line 2555
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2556
    const-string v0, "CORE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isAndsfConfigUpdateBusy: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/quicinc/cne/CNE;->isAndsfConfigUpdateBusy:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE;->dlogv(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2555
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2551
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private startNetworks()V
    .locals 3

    .prologue
    const/16 v2, 0xc

    .line 1475
    iget-boolean v0, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    iget-boolean v1, p0, Lcom/quicinc/cne/CNE;->mLastWQEFeatureEnabled:Z

    if-eq v0, v1, :cond_0

    .line 1476
    iget-boolean v0, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    iput-boolean v0, p0, Lcom/quicinc/cne/CNE;->mLastWQEFeatureEnabled:Z

    .line 1477
    new-instance v0, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V

    invoke-virtual {v0, v2}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/quicinc/cne/CNE;->mWifiRequest:Landroid/net/NetworkRequest;

    .line 1481
    new-instance v0, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v0}, Landroid/net/NetworkRequest$Builder;-><init>()V

    invoke-virtual {v0, v2}, Landroid/net/NetworkRequest$Builder;->addCapability(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/quicinc/cne/CNE;->mMobileRequest:Landroid/net/NetworkRequest;

    .line 1485
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/quicinc/cne/CNE;->cm:Landroid/net/ConnectivityManager;

    .line 1486
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->cm:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/quicinc/cne/CNE;->mWifiRequest:Landroid/net/NetworkRequest;

    iget-object v2, p0, Lcom/quicinc/cne/CNE;->mWifiNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1, v2}, Landroid/net/ConnectivityManager;->requestNetwork(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 1487
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->cm:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/quicinc/cne/CNE;->mMobileRequest:Landroid/net/NetworkRequest;

    iget-object v2, p0, Lcom/quicinc/cne/CNE;->mMobileNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1, v2}, Landroid/net/ConnectivityManager;->requestNetwork(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 1489
    :cond_0
    return-void
.end method

.method private stopNetworks()V
    .locals 2

    .prologue
    .line 1492
    iget-boolean v0, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/quicinc/cne/CNE;->mLastWQEFeatureEnabled:Z

    if-eqz v0, :cond_0

    .line 1493
    iget-boolean v0, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    iput-boolean v0, p0, Lcom/quicinc/cne/CNE;->mLastWQEFeatureEnabled:Z

    .line 1494
    const-string v0, "CORE"

    const-string v1, "Unregister the network callbacks"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1495
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->cm:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/quicinc/cne/CNE;->mWifiNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 1496
    iget-object v0, p0, Lcom/quicinc/cne/CNE;->cm:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/quicinc/cne/CNE;->mMobileNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 1498
    :cond_0
    return-void
.end method

.method private updateDefaultNetwork()Z
    .locals 5

    .prologue
    .line 1801
    const/4 v1, -0x1

    .line 1802
    .local v1, "defaultNw":I
    iget-object v3, p0, Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1804
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 1805
    .local v2, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1806
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    .line 1809
    :cond_0
    iget v3, p0, Lcom/quicinc/cne/CNE;->mDefaultNetwork:I

    if-eq v1, v3, :cond_1

    .line 1810
    iput v1, p0, Lcom/quicinc/cne/CNE;->mDefaultNetwork:I

    .line 1811
    iget v3, p0, Lcom/quicinc/cne/CNE;->mDefaultNetwork:I

    invoke-direct {p0, v3}, Lcom/quicinc/cne/CNE;->sendDefaultNwMsg(I)Z

    move-result v3

    .line 1813
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private updateLinkProperties(Landroid/net/LinkProperties;)V
    .locals 6
    .param p1, "newLp"    # Landroid/net/LinkProperties;

    .prologue
    .line 1363
    const-string v3, "CORE"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "newLp: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1364
    iget-object v3, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    if-eqz v3, :cond_2

    .line 1365
    iget-object v3, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    invoke-virtual {v3}, Landroid/net/LinkProperties;->clear()V

    .line 1366
    iget-object v3, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    invoke-virtual {p1}, Landroid/net/LinkProperties;->getInterfaceName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/LinkProperties;->setInterfaceName(Ljava/lang/String;)V

    .line 1367
    iget-object v3, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    invoke-virtual {p1}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/LinkProperties;->setLinkAddresses(Ljava/util/Collection;)V

    .line 1368
    iget-object v3, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    invoke-virtual {p1}, Landroid/net/LinkProperties;->getDnsServers()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/LinkProperties;->setDnsServers(Ljava/util/Collection;)V

    .line 1369
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/RouteInfo;

    .line 1370
    .local v2, "route":Landroid/net/RouteInfo;
    iget-object v3, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    invoke-virtual {v3, v2}, Landroid/net/LinkProperties;->addRoute(Landroid/net/RouteInfo;)Z

    goto :goto_0

    .line 1372
    .end local v2    # "route":Landroid/net/RouteInfo;
    :cond_0
    iget-object v3, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    invoke-virtual {p1}, Landroid/net/LinkProperties;->getDomains()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/LinkProperties;->setDomains(Ljava/lang/String;)V

    .line 1373
    iget-object v3, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    invoke-virtual {p1}, Landroid/net/LinkProperties;->getMtu()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/net/LinkProperties;->setMtu(I)V

    .line 1374
    iget-object v3, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    invoke-virtual {p1}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyInfo;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/LinkProperties;->setHttpProxy(Landroid/net/ProxyInfo;)V

    .line 1375
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getStackedLinks()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/LinkProperties;

    .line 1376
    .local v1, "lp":Landroid/net/LinkProperties;
    iget-object v3, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    invoke-virtual {v3, v1}, Landroid/net/LinkProperties;->addStackedLink(Landroid/net/LinkProperties;)Z

    goto :goto_1

    .line 1378
    .end local v1    # "lp":Landroid/net/LinkProperties;
    :cond_1
    const-string v3, "CORE"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "curLp: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1380
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method

.method private updateOperatorPolicy(Ljava/lang/String;)I
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2525
    const/4 v0, -0x1

    .line 2526
    .local v0, "retVal":I
    sget-boolean v1, Lcom/quicinc/cne/CNE;->isAndsfConfigUpdateBusy:Z

    if-nez v1, :cond_3

    .line 2527
    const-string v1, "CORE"

    const-string v2, "Updating Operator Policy"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2528
    iget-object v1, p0, Lcom/quicinc/cne/CNE;->andsfParser:Lcom/quicinc/cne/andsf/AndsfParser;

    if-nez v1, :cond_0

    .line 2529
    const-string v1, "CORE"

    const-string v2, "andsfParser object is null"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 2530
    const/4 v1, -0x1

    .line 2547
    :goto_0
    return v1

    .line 2532
    :cond_0
    invoke-direct {p0, v3, v3}, Lcom/quicinc/cne/CNE;->setPolicyConfigUpdateBusy(IZ)V

    .line 2533
    iget-object v1, p0, Lcom/quicinc/cne/CNE;->andsfParser:Lcom/quicinc/cne/andsf/AndsfParser;

    invoke-virtual {v1, p1}, Lcom/quicinc/cne/andsf/AndsfParser;->updateAndsf(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_2

    .line 2534
    invoke-virtual {p0}, Lcom/quicinc/cne/CNE;->andsfDataReady()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2535
    const/4 v0, -0x1

    .line 2536
    invoke-direct {p0, v3, v4}, Lcom/quicinc/cne/CNE;->setPolicyConfigUpdateBusy(IZ)V

    :cond_1
    :goto_1
    move v1, v0

    .line 2547
    goto :goto_0

    .line 2539
    :cond_2
    const-string v1, "CORE"

    const-string v2, "failed to parse configuration file...skip sending message to native layer"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 2541
    invoke-direct {p0, v3, v4}, Lcom/quicinc/cne/CNE;->setPolicyConfigUpdateBusy(IZ)V

    goto :goto_1

    .line 2544
    :cond_3
    const-string v1, "CORE"

    const-string v2, "Previous request in process try later..."

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2545
    const/4 v1, -0x2

    goto :goto_0
.end method


# virtual methods
.method public andsfDataReady()Z
    .locals 3

    .prologue
    .line 1751
    const/16 v1, 0x1c

    invoke-static {v1}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v0

    .line 1752
    .local v0, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v0, :cond_0

    .line 1753
    const-string v1, "CORE"

    const-string v2, "andsfDataReady: rr=NULL"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1754
    const/4 v1, 0x0

    .line 1757
    :goto_0
    return v1

    .line 1756
    :cond_0
    invoke-virtual {p0, v0}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    .line 1757
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getIWLANEnabled()Z
    .locals 2

    .prologue
    .line 2670
    const-string v0, "CORE"

    const-string v1, "getIWLANEnabled()"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE;->dlogd(Ljava/lang/String;Ljava/lang/String;)V

    .line 2671
    monitor-enter p0

    .line 2672
    :try_start_0
    iget-boolean v0, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureEnabled:Z

    monitor-exit p0

    return v0

    .line 2673
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getNSRMEnabled()I
    .locals 1

    .prologue
    .line 3105
    const/4 v0, -0x4

    return v0
.end method

.method public getPolicyVersion(I)I
    .locals 3
    .param p1, "policyType"    # I

    .prologue
    .line 2604
    packed-switch p1, :pswitch_data_0

    .line 2612
    const-string v0, "CORE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid PolicyType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " passed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE;->dlogw(Ljava/lang/String;Ljava/lang/String;)V

    .line 2613
    const/4 v0, -0x3

    :goto_0
    return v0

    .line 2606
    :pswitch_0
    sget-object v0, Lcom/quicinc/cne/CNE$FeatureType;->WQE:Lcom/quicinc/cne/CNE$FeatureType;

    invoke-direct {p0, v0}, Lcom/quicinc/cne/CNE;->checkFeatureEnabled(Lcom/quicinc/cne/CNE$FeatureType;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2607
    const/4 v0, -0x4

    goto :goto_0

    .line 2609
    :cond_0
    invoke-static {}, Lcom/quicinc/cne/andsf/AndsfParser$Version;->getInt()I

    move-result v0

    goto :goto_0

    .line 2604
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getWQEEnabled()I
    .locals 2

    .prologue
    .line 2634
    const-string v0, "CORE"

    const-string v1, "getWQEEnabled()"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE;->dlogd(Ljava/lang/String;Ljava/lang/String;)V

    .line 2635
    monitor-enter p0

    .line 2636
    :try_start_0
    sget-object v0, Lcom/quicinc/cne/CNE$FeatureType;->WQE:Lcom/quicinc/cne/CNE$FeatureType;

    invoke-direct {p0, v0}, Lcom/quicinc/cne/CNE;->checkFeatureEnabled(Lcom/quicinc/cne/CNE$FeatureType;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2637
    const/4 v0, -0x4

    monitor-exit p0

    .line 2639
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureEnabled:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    :goto_1
    monitor-exit p0

    goto :goto_0

    .line 2640
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2639
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public notifyRatConnectStatus(IILjava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "type"    # I
    .param p2, "status"    # I
    .param p3, "ipV4Addr"    # Ljava/lang/String;
    .param p4, "ipV6Addr"    # Ljava/lang/String;

    .prologue
    .line 1846
    const/16 v1, 0xa

    invoke-static {v1}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v0

    .line 1848
    .local v0, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v0, :cond_0

    .line 1849
    const-string v1, "CORE"

    const-string v2, "notifyRatConnectStatus: rr=NULL"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1850
    const/4 v1, 0x0

    .line 1860
    :goto_0
    return v1

    .line 1852
    :cond_0
    const-string v1, "CORE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifyRatConnectStatus ratType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " status="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ipV4Addr="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ipV6Addr="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 1855
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1856
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1857
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1858
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1859
    invoke-virtual {p0, v0}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    .line 1860
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public notifyWlanConnectivityUp(ZI)Z
    .locals 4
    .param p1, "isConnected"    # Z
    .param p2, "familyType"    # I

    .prologue
    .line 1882
    if-eqz p1, :cond_1

    iget v1, p0, Lcom/quicinc/cne/CNE;->lastFamilyType:I

    if-eq v1, p2, :cond_1

    .line 1883
    const/16 v1, 0x18

    invoke-static {v1}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v0

    .line 1884
    .local v0, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v0, :cond_0

    .line 1885
    const-string v1, "CORE"

    const-string v2, "notifyWlanConnectivityUp: rr=NULL"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1886
    const/4 v1, 0x0

    .line 1893
    .end local v0    # "rr":Lcom/quicinc/cne/CNERequest;
    :goto_0
    return v1

    .line 1888
    .restart local v0    # "rr":Lcom/quicinc/cne/CNERequest;
    :cond_0
    const-string v1, "CORE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifyWlanConnectivityUp familyType = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 1889
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1890
    invoke-virtual {p0, v0}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    .line 1891
    iput p2, p0, Lcom/quicinc/cne/CNE;->lastFamilyType:I

    .line 1893
    .end local v0    # "rr":Lcom/quicinc/cne/CNERequest;
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method declared-synchronized send(Lcom/quicinc/cne/CNERequest;)V
    .locals 3
    .param p1, "rr"    # Lcom/quicinc/cne/CNERequest;

    .prologue
    .line 1509
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/quicinc/cne/CNE;->mSender:Lcom/quicinc/cne/CNE$CNESender;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Lcom/quicinc/cne/CNE$CNESender;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1511
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1512
    monitor-exit p0

    return-void

    .line 1509
    .end local v0    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public sendBQEResponse(I)V
    .locals 4
    .param p1, "result"    # I

    .prologue
    .line 2430
    const/16 v1, 0x1a

    invoke-static {v1}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v0

    .line 2431
    .local v0, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v0, :cond_0

    .line 2432
    const-string v1, "CORE"

    const-string v2, "notifyBQEPostResult: rr=NULL"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 2438
    :goto_0
    return-void

    .line 2435
    :cond_0
    const-string v1, "CORE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifyBQEPostResult result="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2436
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2437
    invoke-virtual {p0, v0}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    goto :goto_0
.end method

.method public sendBQEResponse(IIII)V
    .locals 4
    .param p1, "result"    # I
    .param p2, "rtt"    # I
    .param p3, "tSec"    # I
    .param p4, "tMs"    # I

    .prologue
    .line 2413
    const/16 v1, 0x19

    invoke-static {v1}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v0

    .line 2414
    .local v0, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v0, :cond_0

    .line 2415
    const-string v1, "CORE"

    const-string v2, "notifyJRTTResult: rr=NULL"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 2426
    :goto_0
    return-void

    .line 2418
    :cond_0
    const-string v1, "CORE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifyJRTTResult result="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " BQE params "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2421
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2422
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2423
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2424
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 2425
    invoke-virtual {p0, v0}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    goto :goto_0
.end method

.method public sendDefaultNwPref(I)V
    .locals 4
    .param p1, "preference"    # I

    .prologue
    .line 1933
    const/16 v2, 0xb

    invoke-static {v2}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v1

    .line 1934
    .local v1, "rr":Lcom/quicinc/cne/CNERequest;
    if-eqz v1, :cond_1

    .line 1935
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1936
    const/4 v0, 0x1

    .line 1937
    .local v0, "rat":I
    if-nez p1, :cond_0

    .line 1938
    const/4 v0, 0x0

    .line 1940
    :cond_0
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1941
    invoke-virtual {p0, v1}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    .line 1945
    .end local v0    # "rat":I
    :goto_0
    return-void

    .line 1943
    :cond_1
    const-string v2, "CORE"

    const-string v3, "sendDefaultNwPref2Cne: rr=NULL"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public sendDefaultNwPref2Cne(I)V
    .locals 0
    .param p1, "preference"    # I

    .prologue
    .line 1928
    iput p1, p0, Lcom/quicinc/cne/CNE;->mNetworkPreference:I

    .line 1929
    return-void
.end method

.method public sendICDResponse(ILjava/lang/String;IIIIIIIII)V
    .locals 7
    .param p1, "result"    # I
    .param p2, "bssid"    # Ljava/lang/String;
    .param p3, "flags"    # I
    .param p4, "tid"    # I
    .param p5, "icdQuota"    # I
    .param p6, "icdProb"    # I
    .param p7, "bqeQuota"    # I
    .param p8, "bqeProb"    # I
    .param p9, "mbw"    # I
    .param p10, "dl"    # I
    .param p11, "sdev"    # I

    .prologue
    .line 2385
    const/16 v2, 0x15

    invoke-static {v2}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v1

    .line 2386
    .local v1, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v1, :cond_0

    .line 2387
    const-string v2, "CORE"

    const-string v3, "notifyICDResult: rr=NULL"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 2409
    :goto_0
    return-void

    .line 2390
    :cond_0
    const-string v2, "CORE"

    const-string v3, "notifyICDResult result=%d bssid=%s flags 0x%x tid %d ICD params %d %d BQE params %d %d mbw %d tput params %d %d"

    const/16 v4, 0xb

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    const/4 v5, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x6

    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x7

    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x8

    invoke-static/range {p9 .. p9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0x9

    invoke-static/range {p10 .. p10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/16 v5, 0xa

    invoke-static/range {p11 .. p11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2397
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2398
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2399
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2400
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 2401
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2, p5}, Landroid/os/Parcel;->writeInt(I)V

    .line 2402
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2, p6}, Landroid/os/Parcel;->writeInt(I)V

    .line 2403
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2, p7}, Landroid/os/Parcel;->writeInt(I)V

    .line 2404
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v2, p8}, Landroid/os/Parcel;->writeInt(I)V

    .line 2405
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    move/from16 v0, p9

    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2406
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    move/from16 v0, p10

    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2407
    iget-object v2, v1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    move/from16 v0, p11

    invoke-virtual {v2, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 2408
    invoke-virtual {p0, v1}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    goto/16 :goto_0
.end method

.method public sendIcdHttpResponse(ILjava/lang/String;II)V
    .locals 6
    .param p1, "result"    # I
    .param p2, "bssid"    # Ljava/lang/String;
    .param p3, "tid"    # I
    .param p4, "family"    # I

    .prologue
    .line 2364
    const/16 v1, 0x1b

    invoke-static {v1}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v0

    .line 2365
    .local v0, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v0, :cond_0

    .line 2366
    const-string v1, "CORE"

    const-string v2, "notifyIcdHttpResult: rr=NULL"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 2378
    :goto_0
    return-void

    .line 2369
    :cond_0
    const-string v1, "CORE"

    const-string v2, "notifyIcdHttpResult result=%d bssid=%s tid=%d family=%d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2373
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2374
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2375
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2376
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 2377
    invoke-virtual {p0, v0}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    goto :goto_0
.end method

.method public sendTetheringUpstreamInfo(Ljava/lang/String;Ljava/lang/String;II)Z
    .locals 4
    .param p1, "upstreamIface"    # Ljava/lang/String;
    .param p2, "tetheredIface"    # Ljava/lang/String;
    .param p3, "ipType"    # I
    .param p4, "updateType"    # I

    .prologue
    .line 1898
    const/16 v1, 0x23

    invoke-static {v1}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v0

    .line 1900
    .local v0, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v0, :cond_0

    .line 1901
    const-string v1, "CORE"

    const-string v2, "sendTetheringUpstreamInfo: rr=NULL"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1902
    const/4 v1, 0x0

    .line 1923
    :goto_0
    return v1

    .line 1905
    :cond_0
    if-nez p1, :cond_1

    .line 1906
    const-string p1, ""

    .line 1909
    :cond_1
    if-nez p2, :cond_2

    .line 1910
    const-string p2, ""

    .line 1913
    :cond_2
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1914
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1915
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1916
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1918
    const-string v1, "CORE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendTetheringUpstreamInfo: upstreamIface:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Tethered Iface:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ipType(IPV4=0, IPV6=1):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " updateType(del=0, add=1):"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 1922
    invoke-virtual {p0, v0}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    .line 1923
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setIWLANEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 2678
    const-string v1, "CORE"

    const-string v2, "setIWLANEnabled()"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->dlogd(Ljava/lang/String;Ljava/lang/String;)V

    .line 2679
    monitor-enter p0

    .line 2680
    :try_start_0
    iget-boolean v1, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureRequestedState:Z

    if-eq v1, p1, :cond_0

    .line 2681
    iput-boolean p1, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureRequestedState:Z

    .line 2686
    iget-boolean v1, p0, Lcom/quicinc/cne/CNE;->mIWLANFeatureRequestedState:Z

    if-eqz v1, :cond_1

    .line 2687
    const/4 v0, 0x2

    .line 2692
    .local v0, "state":I
    :goto_0
    const/4 v1, 0x2

    invoke-direct {p0, v1, v0}, Lcom/quicinc/cne/CNE;->requestFeatureSettingsChange(II)Z

    .line 2693
    monitor-exit p0

    .line 2694
    .end local v0    # "state":I
    :goto_1
    return-void

    .line 2683
    :cond_0
    monitor-exit p0

    goto :goto_1

    .line 2693
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 2689
    :cond_1
    const/4 v0, 0x1

    .restart local v0    # "state":I
    goto :goto_0
.end method

.method public setNSRMEnabled(I)I
    .locals 1
    .param p1, "nsrmSetType"    # I

    .prologue
    .line 3110
    const/4 v0, -0x4

    return v0
.end method

.method public setWQEEnabled(Z)I
    .locals 5
    .param p1, "enabled"    # Z

    .prologue
    const/16 v1, 0x3e8

    .line 2645
    const-string v2, "CORE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setWQEEnabled() "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->dlogd(Ljava/lang/String;Ljava/lang/String;)V

    .line 2646
    monitor-enter p0

    .line 2647
    :try_start_0
    sget-object v2, Lcom/quicinc/cne/CNE$FeatureType;->WQE:Lcom/quicinc/cne/CNE$FeatureType;

    invoke-direct {p0, v2}, Lcom/quicinc/cne/CNE;->checkFeatureEnabled(Lcom/quicinc/cne/CNE$FeatureType;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2648
    const/4 v1, -0x4

    monitor-exit p0

    .line 2664
    :goto_0
    return v1

    .line 2650
    :cond_0
    iget-boolean v2, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureRequestedState:Z

    if-eq v2, p1, :cond_1

    .line 2651
    iput-boolean p1, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureRequestedState:Z

    .line 2657
    iget-boolean v2, p0, Lcom/quicinc/cne/CNE;->mWQEFeatureRequestedState:Z

    if-eqz v2, :cond_2

    .line 2658
    const/4 v0, 0x2

    .line 2663
    .local v0, "state":I
    :goto_1
    const/4 v2, 0x1

    invoke-direct {p0, v2, v0}, Lcom/quicinc/cne/CNE;->requestFeatureSettingsChange(II)Z

    .line 2664
    monitor-exit p0

    goto :goto_0

    .line 2665
    .end local v0    # "state":I
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 2653
    :cond_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2660
    :cond_2
    const/4 v0, 0x1

    .restart local v0    # "state":I
    goto :goto_1
.end method

.method public updateBatteryStatus(III)Z
    .locals 4
    .param p1, "status"    # I
    .param p2, "pluginType"    # I
    .param p3, "level"    # I

    .prologue
    .line 1762
    const/4 v1, 0x7

    invoke-static {v1}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v0

    .line 1763
    .local v0, "rr":Lcom/quicinc/cne/CNERequest;
    if-nez v0, :cond_0

    .line 1764
    const-string v1, "CORE"

    const-string v2, "updateBatteryStatus: rr=NULL"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 1765
    const/4 v1, 0x0

    .line 1774
    :goto_0
    return v1

    .line 1767
    :cond_0
    const-string v1, "CORE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UpdateBatteryStatus status="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pluginType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " level="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V

    .line 1769
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1770
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1771
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1772
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 1773
    invoke-virtual {p0, v0}, Lcom/quicinc/cne/CNE;->send(Lcom/quicinc/cne/CNERequest;)V

    .line 1774
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public updatePolicy(ILjava/lang/String;)I
    .locals 6
    .param p1, "policyType"    # I
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 2567
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x400

    if-le v2, v3, :cond_0

    .line 2568
    const-string v2, "CORE"

    const-string v3, "Path length too long"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->dloge(Ljava/lang/String;Ljava/lang/String;)V

    .line 2569
    const/4 v2, -0x7

    .line 2597
    :goto_0
    return v2

    .line 2572
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 2574
    .local v1, "uid":I
    const-string v2, "/system/etc/cne/"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "/data/connectivity/"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    const/16 v2, 0x3e8

    if-le v1, v2, :cond_2

    .line 2576
    const-string v2, "CORE"

    const-string v3, "This path is not allowed to access"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->dloge(Ljava/lang/String;Ljava/lang/String;)V

    .line 2577
    const/4 v2, -0x6

    goto :goto_0

    .line 2580
    :cond_2
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2582
    .local v0, "file":Ljava/io/File;
    packed-switch p1, :pswitch_data_0

    .line 2596
    const-string v2, "CORE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Invalid PolicyType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " passed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->dlogw(Ljava/lang/String;Ljava/lang/String;)V

    .line 2597
    const/4 v2, -0x3

    goto :goto_0

    .line 2584
    :pswitch_0
    sget-object v2, Lcom/quicinc/cne/CNE$FeatureType;->WQE:Lcom/quicinc/cne/CNE$FeatureType;

    invoke-direct {p0, v2}, Lcom/quicinc/cne/CNE;->checkFeatureEnabled(Lcom/quicinc/cne/CNE$FeatureType;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2585
    const/4 v2, -0x4

    goto :goto_0

    .line 2588
    :cond_3
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/32 v4, 0x19000

    cmp-long v2, v2, v4

    if-lez v2, :cond_4

    .line 2589
    const-string v2, "CORE"

    const-string v3, "File size not supported"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->dloge(Ljava/lang/String;Ljava/lang/String;)V

    .line 2590
    const/4 v2, -0x5

    goto :goto_0

    .line 2593
    :cond_4
    invoke-direct {p0, p2}, Lcom/quicinc/cne/CNE;->updateOperatorPolicy(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 2582
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
