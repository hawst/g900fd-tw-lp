.class Lcom/quicinc/cne/CNE$1;
.super Landroid/content/BroadcastReceiver;
.source "CNE.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quicinc/cne/CNE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/quicinc/cne/CNE;


# direct methods
.method constructor <init>(Lcom/quicinc/cne/CNE;)V
    .locals 0

    .prologue
    .line 689
    iput-object p1, p0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 26
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 691
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 693
    .local v4, "action":Ljava/lang/String;
    const-string v21, "android.intent.action.BOOT_COMPLETED"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 694
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/quicinc/cne/CNE;->mBrowserInfoThread:Ljava/lang/Thread;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Thread;->getState()Ljava/lang/Thread$State;

    move-result-object v21

    sget-object v22, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_0

    .line 695
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/quicinc/cne/CNE;->mBrowserInfoThread:Ljava/lang/Thread;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/lang/Thread;->start()V

    .line 697
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/quicinc/cne/CNE;->mPackageListener:Lcom/quicinc/cne/CNE$PackageListener;

    move-object/from16 v21, v0

    if-eqz v21, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/quicinc/cne/CNE;->mPackageListener:Lcom/quicinc/cne/CNE$PackageListener;

    move-object/from16 v21, v0

    # getter for: Lcom/quicinc/cne/CNE$PackageListener;->isInitialized:Z
    invoke-static/range {v21 .. v21}, Lcom/quicinc/cne/CNE$PackageListener;->access$800(Lcom/quicinc/cne/CNE$PackageListener;)Z

    move-result v21

    if-nez v21, :cond_1

    .line 698
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/quicinc/cne/CNE;->mPackageListener:Lcom/quicinc/cne/CNE$PackageListener;

    move-object/from16 v21, v0

    # invokes: Lcom/quicinc/cne/CNE$PackageListener;->initialize()V
    invoke-static/range {v21 .. v21}, Lcom/quicinc/cne/CNE$PackageListener;->access$900(Lcom/quicinc/cne/CNE$PackageListener;)V

    .line 798
    :cond_1
    :goto_0
    return-void

    .line 700
    :cond_2
    const-string v21, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 701
    const-string v21, "networkInfo"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Landroid/net/NetworkInfo;

    .line 702
    .local v12, "ni":Landroid/net/NetworkInfo;
    if-eqz v12, :cond_3

    invoke-virtual {v12}, Landroid/net/NetworkInfo;->getType()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_3

    .line 703
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    invoke-virtual {v12}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v22

    # setter for: Lcom/quicinc/cne/CNE;->isWifiConnected:Z
    invoke-static/range {v21 .. v22}, Lcom/quicinc/cne/CNE;->access$1002(Lcom/quicinc/cne/CNE;Z)Z

    .line 704
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v22, v0

    # getter for: Lcom/quicinc/cne/CNE;->isWifiConnected:Z
    invoke-static/range {v22 .. v22}, Lcom/quicinc/cne/CNE;->access$1000(Lcom/quicinc/cne/CNE;)Z

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v23, v0

    # invokes: Lcom/quicinc/cne/CNE;->getInetFamily()I
    invoke-static/range {v23 .. v23}, Lcom/quicinc/cne/CNE;->access$1100(Lcom/quicinc/cne/CNE;)I

    move-result v23

    invoke-virtual/range {v21 .. v23}, Lcom/quicinc/cne/CNE;->notifyWlanConnectivityUp(ZI)Z

    .line 706
    :cond_3
    const-string v21, "isDefault"

    const/16 v22, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 707
    .local v7, "isDefault":Z
    if-eqz v7, :cond_1

    .line 708
    const-string v21, "CORE"

    const-string v22, "Got CONNECTIVITY_ACTION_IMMEDIATE on default nw"

    # invokes: Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/quicinc/cne/CNE;->access$100(Ljava/lang/String;Ljava/lang/String;)V

    .line 709
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    # invokes: Lcom/quicinc/cne/CNE;->updateDefaultNetwork()Z
    invoke-static/range {v21 .. v21}, Lcom/quicinc/cne/CNE;->access$1200(Lcom/quicinc/cne/CNE;)Z

    goto :goto_0

    .line 712
    .end local v7    # "isDefault":Z
    .end local v12    # "ni":Landroid/net/NetworkInfo;
    :cond_4
    const-string v21, "org.codeaurora.NETID_UPDATE"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 713
    const-string v21, "CORE"

    const-string v22, "Got NET_ID_UPDATE"

    # invokes: Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/quicinc/cne/CNE;->access$100(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    const-string v21, "netType"

    const/16 v22, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 715
    .local v11, "netType":I
    const-string v21, "netID"

    const/16 v22, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v10

    .line 716
    .local v10, "netId":I
    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v11, v0, :cond_1

    const/16 v21, -0x1

    move/from16 v0, v21

    if-eq v10, v0, :cond_1

    .line 717
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    # invokes: Lcom/quicinc/cne/CNE;->notifyNetIdInfo(II)Z
    invoke-static {v0, v11, v10}, Lcom/quicinc/cne/CNE;->access$1300(Lcom/quicinc/cne/CNE;II)Z

    goto/16 :goto_0

    .line 718
    .end local v10    # "netId":I
    .end local v11    # "netType":I
    :cond_5
    const-string v21, "android.intent.action.BATTERY_CHANGED"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 719
    const-string v21, "CORE"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "CNE received action: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/quicinc/cne/CNE;->access$100(Ljava/lang/String;Ljava/lang/String;)V

    .line 720
    const-string v21, "level"

    const/16 v22, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v21

    move/from16 v0, v21

    int-to-double v8, v0

    .line 721
    .local v8, "level":D
    const-string v21, "scale"

    const/16 v22, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v21

    move/from16 v0, v21

    int-to-double v0, v0

    move-wide/from16 v16, v0

    .line 722
    .local v16, "scale":D
    const-string v21, "plugged"

    const/16 v22, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v14

    .line 723
    .local v14, "pluginType":I
    const-string v21, "status"

    const/16 v22, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    .line 724
    .local v15, "status":I
    div-double v22, v8, v16

    const-wide/high16 v24, 0x4059000000000000L    # 100.0

    mul-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-int v13, v0

    .line 725
    .local v13, "normalizedLevel":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v15, v14, v13}, Lcom/quicinc/cne/CNE;->updateBatteryStatus(III)Z

    goto/16 :goto_0

    .line 727
    .end local v8    # "level":D
    .end local v13    # "normalizedLevel":I
    .end local v14    # "pluginType":I
    .end local v15    # "status":I
    .end local v16    # "scale":D
    :cond_6
    const-string v21, "android.intent.action.SCREEN_ON"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 728
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    # invokes: Lcom/quicinc/cne/CNE;->sendScreenState(Z)V
    invoke-static/range {v21 .. v22}, Lcom/quicinc/cne/CNE;->access$1400(Lcom/quicinc/cne/CNE;Z)V

    .line 729
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    # setter for: Lcom/quicinc/cne/CNE;->mScreenOn:Z
    invoke-static/range {v21 .. v22}, Lcom/quicinc/cne/CNE;->access$1502(Lcom/quicinc/cne/CNE;Z)Z

    goto/16 :goto_0

    .line 731
    :cond_7
    const-string v21, "android.intent.action.SCREEN_OFF"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 732
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    # invokes: Lcom/quicinc/cne/CNE;->sendScreenState(Z)V
    invoke-static/range {v21 .. v22}, Lcom/quicinc/cne/CNE;->access$1400(Lcom/quicinc/cne/CNE;Z)V

    .line 733
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    # setter for: Lcom/quicinc/cne/CNE;->mScreenOn:Z
    invoke-static/range {v21 .. v22}, Lcom/quicinc/cne/CNE;->access$1502(Lcom/quicinc/cne/CNE;Z)Z

    goto/16 :goto_0

    .line 735
    :cond_8
    const-string v21, "android.net.wifi.RSSI_CHANGED"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_9

    const-string v21, "android.net.wifi.LINK_CONFIGURATION_CHANGED"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 737
    :cond_9
    const-string v21, "CORE"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "CNE received action RSSI/Link Changed events: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/quicinc/cne/CNE;->access$100(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    # getter for: Lcom/quicinc/cne/CNE;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static/range {v21 .. v21}, Lcom/quicinc/cne/CNE;->access$200(Lcom/quicinc/cne/CNE;)Landroid/net/wifi/WifiManager;

    move-result-object v21

    if-eqz v21, :cond_a

    .line 740
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    # getter for: Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;
    invoke-static/range {v21 .. v21}, Lcom/quicinc/cne/CNE;->access$1600(Lcom/quicinc/cne/CNE;)Lcom/quicinc/cne/CNE$CneWifiInfo;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    # invokes: Lcom/quicinc/cne/CNE$CneWifiInfo;->setWifiInfo(Landroid/content/Intent;)V
    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE$CneWifiInfo;->access$1700(Lcom/quicinc/cne/CNE$CneWifiInfo;Landroid/content/Intent;)V

    .line 742
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    # invokes: Lcom/quicinc/cne/CNE;->sendWifiStatus()V
    invoke-static/range {v21 .. v21}, Lcom/quicinc/cne/CNE;->access$1800(Lcom/quicinc/cne/CNE;)V

    goto/16 :goto_0

    .line 744
    :cond_a
    const-string v21, "CORE"

    const-string v22, "CNE received action RSSI/Link Changed events, null WifiManager"

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 747
    :cond_b
    const-string v21, "android.net.wifi.STATE_CHANGE"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_c

    const-string v21, "android.net.wifi.WIFI_STATE_CHANGED"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_e

    .line 749
    :cond_c
    const-string v21, "CORE"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "CNE received action Network/Wifi State Changed: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/quicinc/cne/CNE;->access$100(Ljava/lang/String;Ljava/lang/String;)V

    .line 750
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    # getter for: Lcom/quicinc/cne/CNE;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static/range {v21 .. v21}, Lcom/quicinc/cne/CNE;->access$200(Lcom/quicinc/cne/CNE;)Landroid/net/wifi/WifiManager;

    move-result-object v21

    if-eqz v21, :cond_d

    .line 751
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    # getter for: Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;
    invoke-static/range {v21 .. v21}, Lcom/quicinc/cne/CNE;->access$1600(Lcom/quicinc/cne/CNE;)Lcom/quicinc/cne/CNE$CneWifiInfo;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    # invokes: Lcom/quicinc/cne/CNE$CneWifiInfo;->setWifiInfo(Landroid/content/Intent;)V
    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE$CneWifiInfo;->access$1700(Lcom/quicinc/cne/CNE$CneWifiInfo;Landroid/content/Intent;)V

    .line 753
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    # invokes: Lcom/quicinc/cne/CNE;->sendWifiStatus()V
    invoke-static/range {v21 .. v21}, Lcom/quicinc/cne/CNE;->access$1800(Lcom/quicinc/cne/CNE;)V

    .line 754
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    const/16 v22, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v23, v0

    # getter for: Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;
    invoke-static/range {v23 .. v23}, Lcom/quicinc/cne/CNE;->access$1600(Lcom/quicinc/cne/CNE;)Lcom/quicinc/cne/CNE$CneWifiInfo;

    move-result-object v23

    move-object/from16 v0, v23

    iget v0, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->networkState:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v24, v0

    # getter for: Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;
    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/CNE;->access$1600(Lcom/quicinc/cne/CNE;)Lcom/quicinc/cne/CNE$CneWifiInfo;

    move-result-object v24

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->ipAddrV4:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v25, v0

    # getter for: Lcom/quicinc/cne/CNE;->_cneWifiInfo:Lcom/quicinc/cne/CNE$CneWifiInfo;
    invoke-static/range {v25 .. v25}, Lcom/quicinc/cne/CNE;->access$1600(Lcom/quicinc/cne/CNE;)Lcom/quicinc/cne/CNE$CneWifiInfo;

    move-result-object v25

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/quicinc/cne/CNE$CneWifiInfo;->ipAddrV6:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v21 .. v25}, Lcom/quicinc/cne/CNE;->notifyRatConnectStatus(IILjava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 760
    :cond_d
    const-string v21, "CORE"

    const-string v22, "CNE received action Network State Changed, null WifiManager"

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 762
    :cond_e
    const-string v21, "android.intent.action.ANY_DATA_STATE"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_11

    .line 763
    const-string v21, "apnType"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 764
    .local v5, "apnType":Ljava/lang/String;
    if-nez v5, :cond_f

    .line 765
    const-string v21, "CORE"

    const-string v22, "CNE error getting apnType"

    # invokes: Lcom/quicinc/cne/CNE;->loge(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/quicinc/cne/CNE;->access$1900(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 768
    :cond_f
    const-string v21, "default"

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_10

    .line 769
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    # getter for: Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;
    invoke-static/range {v21 .. v21}, Lcom/quicinc/cne/CNE;->access$2000(Lcom/quicinc/cne/CNE;)Lcom/quicinc/cne/CNE$CneWwanInfo;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, p2

    # invokes: Lcom/quicinc/cne/CNE$CneWwanInfo;->setWwanInfo(Landroid/content/Intent;)V
    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE$CneWwanInfo;->access$2100(Lcom/quicinc/cne/CNE$CneWwanInfo;Landroid/content/Intent;)V

    .line 771
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    # invokes: Lcom/quicinc/cne/CNE;->sendWwanStatus()V
    invoke-static/range {v21 .. v21}, Lcom/quicinc/cne/CNE;->access$2200(Lcom/quicinc/cne/CNE;)V

    .line 772
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v23, v0

    # getter for: Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;
    invoke-static/range {v23 .. v23}, Lcom/quicinc/cne/CNE;->access$2000(Lcom/quicinc/cne/CNE;)Lcom/quicinc/cne/CNE$CneWwanInfo;

    move-result-object v23

    move-object/from16 v0, v23

    iget v0, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->networkState:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v24, v0

    # getter for: Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;
    invoke-static/range {v24 .. v24}, Lcom/quicinc/cne/CNE;->access$2000(Lcom/quicinc/cne/CNE;)Lcom/quicinc/cne/CNE$CneWwanInfo;

    move-result-object v24

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->ipAddrV4:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v25, v0

    # getter for: Lcom/quicinc/cne/CNE;->_cneWwanInfo:Lcom/quicinc/cne/CNE$CneWwanInfo;
    invoke-static/range {v25 .. v25}, Lcom/quicinc/cne/CNE;->access$2000(Lcom/quicinc/cne/CNE;)Lcom/quicinc/cne/CNE$CneWwanInfo;

    move-result-object v25

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/quicinc/cne/CNE$CneWwanInfo;->ipAddrV6:Ljava/lang/String;

    move-object/from16 v25, v0

    invoke-virtual/range {v21 .. v25}, Lcom/quicinc/cne/CNE;->notifyRatConnectStatus(IILjava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 776
    :cond_10
    const-string v21, "CORE"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "CNE currently does not support this apnType="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/quicinc/cne/CNE;->logi(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/quicinc/cne/CNE;->access$2300(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 778
    .end local v5    # "apnType":Ljava/lang/String;
    :cond_11
    const-string v21, "android.intent.action.PACKAGE_ADDED"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_12

    const-string v21, "android.intent.action.PACKAGE_REMOVED"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_12

    const-string v21, "android.intent.action.PACKAGE_REPLACED"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_12

    const-string v21, "android.intent.action.PACKAGE_CHANGED"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_13

    .line 782
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    # invokes: Lcom/quicinc/cne/CNE;->sendBrowserInfoList()Z
    invoke-static/range {v21 .. v21}, Lcom/quicinc/cne/CNE;->access$2400(Lcom/quicinc/cne/CNE;)Z

    goto/16 :goto_0

    .line 787
    :cond_13
    const-string v21, "com.android.server.connectivity.UPSTREAM_IFACE_CHANGED"

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_14

    .line 788
    const-string v21, "tetheringUpstreamIface"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 789
    .local v20, "upstreamIface":Ljava/lang/String;
    const-string v21, "tetheredClientIface"

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 790
    .local v18, "tetheredIface":Ljava/lang/String;
    const-string v21, "tetheringUpstreamIpType"

    const/16 v22, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 792
    .local v6, "ipType":I
    const-string v21, "tetheringUpstreamUpdateType"

    const/16 v22, -0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    .line 794
    .local v19, "updateType":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$1;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    move-object/from16 v2, v18

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v6, v3}, Lcom/quicinc/cne/CNE;->sendTetheringUpstreamInfo(Ljava/lang/String;Ljava/lang/String;II)Z

    goto/16 :goto_0

    .line 796
    .end local v6    # "ipType":I
    .end local v18    # "tetheredIface":Ljava/lang/String;
    .end local v19    # "updateType":I
    .end local v20    # "upstreamIface":Ljava/lang/String;
    :cond_14
    const-string v21, "CORE"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "CNE received unexpected action: "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v21 .. v22}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
