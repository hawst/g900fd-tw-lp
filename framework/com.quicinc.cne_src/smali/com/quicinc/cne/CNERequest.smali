.class public Lcom/quicinc/cne/CNERequest;
.super Ljava/lang/Object;
.source "CNERequest.java"


# static fields
.field private static final MAX_POOL_SIZE:I = 0x4

.field private static final SUB_TYPE:Ljava/lang/String; = "CORE:COM"

.field static sNextSerial:I

.field private static sPool:Lcom/quicinc/cne/CNERequest;

.field private static sPoolSize:I

.field private static sPoolSync:Ljava/lang/Object;

.field static sSerialMonitor:Ljava/lang/Object;


# instance fields
.field mNext:Lcom/quicinc/cne/CNERequest;

.field mRequest:I

.field mResult:Landroid/os/Message;

.field mSerial:I

.field mp:Landroid/os/Parcel;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    sput v1, Lcom/quicinc/cne/CNERequest;->sNextSerial:I

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/quicinc/cne/CNERequest;->sSerialMonitor:Ljava/lang/Object;

    .line 41
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/quicinc/cne/CNERequest;->sPoolSync:Ljava/lang/Object;

    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/quicinc/cne/CNERequest;->sPool:Lcom/quicinc/cne/CNERequest;

    .line 43
    sput v1, Lcom/quicinc/cne/CNERequest;->sPoolSize:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    return-void
.end method

.method static obtain(I)Lcom/quicinc/cne/CNERequest;
    .locals 4
    .param p0, "request"    # I

    .prologue
    .line 63
    const/4 v0, 0x0

    .line 65
    .local v0, "rr":Lcom/quicinc/cne/CNERequest;
    sget-object v2, Lcom/quicinc/cne/CNERequest;->sPoolSync:Ljava/lang/Object;

    monitor-enter v2

    .line 66
    :try_start_0
    sget-object v1, Lcom/quicinc/cne/CNERequest;->sPool:Lcom/quicinc/cne/CNERequest;

    if-eqz v1, :cond_0

    .line 67
    sget-object v0, Lcom/quicinc/cne/CNERequest;->sPool:Lcom/quicinc/cne/CNERequest;

    .line 68
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mNext:Lcom/quicinc/cne/CNERequest;

    sput-object v1, Lcom/quicinc/cne/CNERequest;->sPool:Lcom/quicinc/cne/CNERequest;

    .line 69
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/quicinc/cne/CNERequest;->mNext:Lcom/quicinc/cne/CNERequest;

    .line 70
    sget v1, Lcom/quicinc/cne/CNERequest;->sPoolSize:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/quicinc/cne/CNERequest;->sPoolSize:I

    .line 72
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    if-nez v0, :cond_1

    .line 75
    new-instance v0, Lcom/quicinc/cne/CNERequest;

    .end local v0    # "rr":Lcom/quicinc/cne/CNERequest;
    invoke-direct {v0}, Lcom/quicinc/cne/CNERequest;-><init>()V

    .line 78
    .restart local v0    # "rr":Lcom/quicinc/cne/CNERequest;
    :cond_1
    sget-object v2, Lcom/quicinc/cne/CNERequest;->sSerialMonitor:Ljava/lang/Object;

    monitor-enter v2

    .line 79
    :try_start_1
    sget v1, Lcom/quicinc/cne/CNERequest;->sNextSerial:I

    add-int/lit8 v3, v1, 0x1

    sput v3, Lcom/quicinc/cne/CNERequest;->sNextSerial:I

    iput v1, v0, Lcom/quicinc/cne/CNERequest;->mSerial:I

    .line 80
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 81
    iput p0, v0, Lcom/quicinc/cne/CNERequest;->mRequest:I

    .line 82
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    iput-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    .line 84
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v1, p0}, Landroid/os/Parcel;->writeInt(I)V

    .line 85
    iget-object v1, v0, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    iget v2, v0, Lcom/quicinc/cne/CNERequest;->mSerial:I

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    return-object v0

    .line 72
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 80
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method static resetSerial()V
    .locals 2

    .prologue
    .line 109
    sget-object v1, Lcom/quicinc/cne/CNERequest;->sSerialMonitor:Ljava/lang/Object;

    monitor-enter v1

    .line 110
    const/4 v0, 0x0

    :try_start_0
    sput v0, Lcom/quicinc/cne/CNERequest;->sNextSerial:I

    .line 111
    monitor-exit v1

    .line 112
    return-void

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method onError(I)V
    .locals 0
    .param p1, "error"    # I

    .prologue
    .line 133
    return-void
.end method

.method release()V
    .locals 3

    .prologue
    .line 95
    sget-object v1, Lcom/quicinc/cne/CNERequest;->sPoolSync:Ljava/lang/Object;

    monitor-enter v1

    .line 96
    :try_start_0
    sget v0, Lcom/quicinc/cne/CNERequest;->sPoolSize:I

    const/4 v2, 0x4

    if-ge v0, v2, :cond_0

    .line 97
    sget-object v0, Lcom/quicinc/cne/CNERequest;->sPool:Lcom/quicinc/cne/CNERequest;

    iput-object v0, p0, Lcom/quicinc/cne/CNERequest;->mNext:Lcom/quicinc/cne/CNERequest;

    .line 98
    sput-object p0, Lcom/quicinc/cne/CNERequest;->sPool:Lcom/quicinc/cne/CNERequest;

    .line 99
    sget v0, Lcom/quicinc/cne/CNERequest;->sPoolSize:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/quicinc/cne/CNERequest;->sPoolSize:I

    .line 101
    :cond_0
    monitor-exit v1

    .line 102
    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method serialString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 116
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x8

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 119
    .local v2, "sb":Ljava/lang/StringBuilder;
    iget v4, p0, Lcom/quicinc/cne/CNERequest;->mSerial:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    .line 122
    .local v3, "sn":Ljava/lang/String;
    const/16 v4, 0x5b

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 123
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    .local v1, "s":I
    :goto_0
    rsub-int/lit8 v4, v1, 0x4

    if-ge v0, v4, :cond_0

    .line 124
    const/16 v4, 0x30

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 127
    :cond_0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    const/16 v4, 0x5d

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 129
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method
