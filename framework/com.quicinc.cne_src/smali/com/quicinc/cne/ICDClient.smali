.class public Lcom/quicinc/cne/ICDClient;
.super Ljava/lang/Object;
.source "ICDClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quicinc/cne/ICDClient$icdHttp;,
        Lcom/quicinc/cne/ICDClient$IcdResult;
    }
.end annotation


# static fields
.field public static final FLAG_BQE_PROB_PRESENT:I = 0x8

.field public static final FLAG_BQE_QUOTA_PRESENT:I = 0x4

.field public static final FLAG_ICD_PROB_PRESENT:I = 0x2

.field public static final FLAG_ICD_QUOTA_PRESENT:I = 0x1

.field public static final FLAG_MBW_PRESENT:I = 0x10

.field public static final FLAG_TPUT_DL_PRESENT:I = 0x20

.field public static final FLAG_TPUT_SDEV_PRESENT:I = 0x40

.field static final ICD_REQ:I = 0x0

.field static final PARAMETER_REQ:I = 0x1

.field private static final SUB_TYPE:Ljava/lang/String; = "WQE:ICD"


# instance fields
.field private bqeProb:I

.field private bqeQuota:I

.field private bssid:Ljava/lang/String;

.field private bssidList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private bssidPassed:Ljava/lang/String;

.field private bssidURI:Ljava/net/URI;

.field private cneHandle:Lcom/quicinc/cne/CNE;

.field private currentBSSID:Ljava/lang/String;

.field private dl:I

.field private family:I

.field private flags:I

.field private httpBssidURI:Ljava/net/URI;

.field private httpURI:Ljava/net/URI;

.field private icdHttpClient:Lorg/apache/http/client/HttpClient;

.field private icdHttpGet:Lorg/apache/http/client/methods/HttpGet;

.field private icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

.field private icdHttpRsp:Lorg/apache/http/HttpResponse;

.field private icdParamClient:Lorg/apache/http/client/HttpClient;

.field private icdParamGet:Lorg/apache/http/client/methods/HttpGet;

.field private icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

.field private icdParamRsp:Lorg/apache/http/HttpResponse;

.field private icdProb:I

.field private icdQuota:I

.field private ipAddr:I

.field private mbw:I

.field private postingProb:I

.field private rspLock:Ljava/util/concurrent/locks/Lock;

.field private sdev:I

.field private seconds:I

.field private sentHttpRsp:Ljava/lang/Boolean;

.field private sentParamRsp:Ljava/lang/Boolean;

.field private tid:I

.field private timeout:Ljava/lang/Boolean;

.field private timer:Ljava/util/Timer;

.field private uri:Ljava/net/URI;

.field private wifiMgr:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Lcom/quicinc/cne/CNE;Landroid/net/wifi/WifiManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 4
    .param p1, "handle"    # Lcom/quicinc/cne/CNE;
    .param p2, "wifi"    # Landroid/net/wifi/WifiManager;
    .param p3, "uri"    # Ljava/lang/String;
    .param p4, "httpuri"    # Ljava/lang/String;
    .param p5, "bssidPassed"    # Ljava/lang/String;
    .param p6, "seconds"    # I
    .param p7, "tid"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput v2, p0, Lcom/quicinc/cne/ICDClient;->icdQuota:I

    .line 79
    iput v2, p0, Lcom/quicinc/cne/ICDClient;->icdProb:I

    .line 80
    iput v2, p0, Lcom/quicinc/cne/ICDClient;->bqeQuota:I

    .line 81
    iput v2, p0, Lcom/quicinc/cne/ICDClient;->bqeProb:I

    .line 82
    iput v2, p0, Lcom/quicinc/cne/ICDClient;->mbw:I

    .line 83
    iput v2, p0, Lcom/quicinc/cne/ICDClient;->postingProb:I

    .line 84
    iput v2, p0, Lcom/quicinc/cne/ICDClient;->dl:I

    .line 85
    iput v2, p0, Lcom/quicinc/cne/ICDClient;->sdev:I

    .line 86
    iput v2, p0, Lcom/quicinc/cne/ICDClient;->family:I

    .line 116
    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v1, p0, Lcom/quicinc/cne/ICDClient;->rspLock:Ljava/util/concurrent/locks/Lock;

    .line 117
    iput-object v3, p0, Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 118
    iput-object v3, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 119
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/quicinc/cne/ICDClient;->sentParamRsp:Ljava/lang/Boolean;

    .line 120
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/quicinc/cne/ICDClient;->sentHttpRsp:Ljava/lang/Boolean;

    .line 121
    iput v2, p0, Lcom/quicinc/cne/ICDClient;->flags:I

    .line 122
    iput v2, p0, Lcom/quicinc/cne/ICDClient;->family:I

    .line 123
    iput p6, p0, Lcom/quicinc/cne/ICDClient;->seconds:I

    .line 124
    iput p7, p0, Lcom/quicinc/cne/ICDClient;->tid:I

    .line 125
    iput-object p2, p0, Lcom/quicinc/cne/ICDClient;->wifiMgr:Landroid/net/wifi/WifiManager;

    .line 128
    :try_start_0
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, p3}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/quicinc/cne/ICDClient;->uri:Ljava/net/URI;

    .line 129
    iput-object p5, p0, Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;

    .line 130
    invoke-direct {p0}, Lcom/quicinc/cne/ICDClient;->setBssid()V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 144
    :goto_0
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    iput-object v1, p0, Lcom/quicinc/cne/ICDClient;->icdParamClient:Lorg/apache/http/client/HttpClient;

    .line 145
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    iput-object v1, p0, Lcom/quicinc/cne/ICDClient;->icdHttpClient:Lorg/apache/http/client/HttpClient;

    .line 146
    iput-object p1, p0, Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;

    .line 148
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/quicinc/cne/ICDClient$icdHttp;

    invoke-direct {v2, p0, p4}, Lcom/quicinc/cne/ICDClient$icdHttp;-><init>(Lcom/quicinc/cne/ICDClient;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 150
    const-string v1, "WQE:ICD"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ICDClient() constructor created with URI = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " httpURI = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bssidPassed = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " tid = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    return-void

    .line 132
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Ljava/net/URISyntaxException;
    invoke-virtual {v0}, Ljava/net/URISyntaxException;->printStackTrace()V

    .line 135
    const-string v1, "WQE:ICD"

    const-string v2, "Received Invalid URI , continuing with null URI"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iput-object v3, p0, Lcom/quicinc/cne/ICDClient;->uri:Ljava/net/URI;

    .line 137
    iput-object v3, p0, Lcom/quicinc/cne/ICDClient;->httpURI:Ljava/net/URI;

    goto :goto_0

    .line 139
    .end local v0    # "e":Ljava/net/URISyntaxException;
    :catch_1
    move-exception v0

    .line 141
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 142
    const-string v1, "WQE:ICD"

    const-string v2, "Received an Exception while creating ICDClient.Continuing"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/quicinc/cne/ICDClient;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->timeout:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$002(Lcom/quicinc/cne/ICDClient;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/quicinc/cne/ICDClient;->timeout:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$100(Lcom/quicinc/cne/ICDClient;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->timer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/quicinc/cne/ICDClient;)Ljava/net/URI;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->httpBssidURI:Ljava/net/URI;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/quicinc/cne/ICDClient;Ljava/net/URI;)Ljava/net/URI;
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;
    .param p1, "x1"    # Ljava/net/URI;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/quicinc/cne/ICDClient;->httpBssidURI:Ljava/net/URI;

    return-object p1
.end method

.method static synthetic access$102(Lcom/quicinc/cne/ICDClient;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;
    .param p1, "x1"    # Ljava/util/Timer;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/quicinc/cne/ICDClient;->timer:Ljava/util/Timer;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/quicinc/cne/ICDClient;)Lorg/apache/http/client/HttpClient;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdHttpClient:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/quicinc/cne/ICDClient;)Lorg/apache/http/client/methods/HttpGet;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdHttpGet:Lorg/apache/http/client/methods/HttpGet;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/quicinc/cne/ICDClient;Lorg/apache/http/client/methods/HttpGet;)Lorg/apache/http/client/methods/HttpGet;
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;
    .param p1, "x1"    # Lorg/apache/http/client/methods/HttpGet;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/quicinc/cne/ICDClient;->icdHttpGet:Lorg/apache/http/client/methods/HttpGet;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/quicinc/cne/ICDClient;)Lorg/apache/http/HttpResponse;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdHttpRsp:Lorg/apache/http/HttpResponse;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/quicinc/cne/ICDClient;Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpResponse;
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;
    .param p1, "x1"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/quicinc/cne/ICDClient;->icdHttpRsp:Lorg/apache/http/HttpResponse;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/quicinc/cne/ICDClient;)Ljava/util/concurrent/locks/Lock;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->rspLock:Ljava/util/concurrent/locks/Lock;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/quicinc/cne/ICDClient;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->sentHttpRsp:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/quicinc/cne/ICDClient;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/quicinc/cne/ICDClient;->sentHttpRsp:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/quicinc/cne/ICDClient;)I
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget v0, p0, Lcom/quicinc/cne/ICDClient;->tid:I

    return v0
.end method

.method static synthetic access$1700(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/CNE;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;

    return-object v0
.end method

.method static synthetic access$200(Lcom/quicinc/cne/ICDClient;)I
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget v0, p0, Lcom/quicinc/cne/ICDClient;->seconds:I

    return v0
.end method

.method static synthetic access$400(Lcom/quicinc/cne/ICDClient;)Ljava/net/URI;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->httpURI:Ljava/net/URI;

    return-object v0
.end method

.method static synthetic access$402(Lcom/quicinc/cne/ICDClient;Ljava/net/URI;)Ljava/net/URI;
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;
    .param p1, "x1"    # Ljava/net/URI;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/quicinc/cne/ICDClient;->httpURI:Ljava/net/URI;

    return-object p1
.end method

.method static synthetic access$500(Lcom/quicinc/cne/ICDClient;)I
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget v0, p0, Lcom/quicinc/cne/ICDClient;->family:I

    return v0
.end method

.method static synthetic access$502(Lcom/quicinc/cne/ICDClient;I)I
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;
    .param p1, "x1"    # I

    .prologue
    .line 48
    iput p1, p0, Lcom/quicinc/cne/ICDClient;->family:I

    return p1
.end method

.method static synthetic access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    return-object v0
.end method

.method static synthetic access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;
    .param p1, "x1"    # Lcom/quicinc/cne/ICDClient$IcdResult;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    return-object p1
.end method

.method static synthetic access$700(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->bssid:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->currentBSSID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;

    return-object v0
.end method

.method private parseIcdParamClientRsp()V
    .locals 14

    .prologue
    .line 804
    const-string v0, "WQE:ICD"

    const-string v1, "parseIcdParamClientRsp()"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->rspLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 806
    const-string v0, "WQE:ICD"

    const-string v1, "parseIcdParamClientRsp() Locked"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->sentParamRsp:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 810
    const-string v0, "WQE:ICD"

    const-string v1, "ICD Parameter Response sent already, doing nothing"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    :goto_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/quicinc/cne/ICDClient;->sentParamRsp:Ljava/lang/Boolean;

    .line 840
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->rspLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 841
    const-string v0, "WQE:ICD"

    const-string v1, "parseIcdParamClientRsp() UnLocked"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    return-void

    .line 814
    :cond_0
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    if-eqz v0, :cond_1

    .line 816
    const-string v0, "WQE:ICD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ICDRequest failure.Reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    invoke-virtual {v2}, Lcom/quicinc/cne/ICDClient$IcdResult;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 820
    :cond_1
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdParamRsp:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v13

    .line 821
    .local v13, "statusCode":I
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdParamRsp:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v12

    .line 822
    .local v12, "reasonPhrase":Ljava/lang/String;
    const-string v0, "WQE:ICD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ICDResponse http Status code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    const/16 v0, 0xc8

    if-ne v13, v0, :cond_2

    .line 825
    const-string v0, "WQE:ICD"

    const-string v1, "ICD Parameter Request successful"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    sget-object v0, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_SUCCESS:Lcom/quicinc/cne/ICDClient$IcdResult;

    iput-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 827
    invoke-direct {p0}, Lcom/quicinc/cne/ICDClient;->parseIcdParamJsonRsp()V

    .line 828
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;

    iget-object v1, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    invoke-virtual {v1}, Lcom/quicinc/cne/ICDClient$IcdResult;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;

    iget v3, p0, Lcom/quicinc/cne/ICDClient;->flags:I

    iget v4, p0, Lcom/quicinc/cne/ICDClient;->tid:I

    iget v5, p0, Lcom/quicinc/cne/ICDClient;->icdQuota:I

    iget v6, p0, Lcom/quicinc/cne/ICDClient;->icdProb:I

    iget v7, p0, Lcom/quicinc/cne/ICDClient;->bqeQuota:I

    iget v8, p0, Lcom/quicinc/cne/ICDClient;->bqeProb:I

    iget v9, p0, Lcom/quicinc/cne/ICDClient;->mbw:I

    iget v10, p0, Lcom/quicinc/cne/ICDClient;->dl:I

    iget v11, p0, Lcom/quicinc/cne/ICDClient;->sdev:I

    invoke-virtual/range {v0 .. v11}, Lcom/quicinc/cne/CNE;->sendICDResponse(ILjava/lang/String;IIIIIIIII)V

    goto/16 :goto_0

    .line 835
    :cond_2
    const-string v0, "WQE:ICD"

    const-string v1, "ICD Parameter Request failed"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private parseIcdParamJsonRsp()V
    .locals 12

    .prologue
    .line 700
    const-string v10, "WQE:ICD"

    const-string v11, "parseIcdParamJsonRsp()"

    invoke-static {v10, v11}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    const/4 v4, 0x0

    .line 702
    .local v4, "jsonString":Ljava/lang/String;
    iget-object v10, p0, Lcom/quicinc/cne/ICDClient;->icdParamRsp:Lorg/apache/http/HttpResponse;

    invoke-interface {v10}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 703
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v1, :cond_5

    .line 705
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v6

    .line 706
    .local v6, "len":J
    const-wide/16 v10, -0x1

    cmp-long v10, v6, v10

    if-eqz v10, :cond_4

    const-wide/32 v10, 0x7fffffff

    cmp-long v10, v6, v10

    if-gez v10, :cond_4

    .line 710
    :try_start_0
    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v4

    .line 711
    const-string v10, "WQE:ICD"

    invoke-static {v10, v4}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/net/ParseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    .line 712
    const/4 v2, 0x0

    .line 715
    .local v2, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Landroid/net/ParseException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    .line 716
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    .local v3, "jsonObject":Lorg/json/JSONObject;
    :try_start_2
    iget-object v10, p0, Lcom/quicinc/cne/ICDClient;->bssid:Ljava/lang/String;

    const-string v11, "bssid"

    invoke-virtual {v3, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 718
    const-string v10, "WQE:ICD"

    const-string v11, "Bssids match, ICD PASS"

    invoke-static {v10, v11}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/net/ParseException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 735
    :try_start_3
    const-string v10, "mbw"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    iput v10, p0, Lcom/quicinc/cne/ICDClient;->mbw:I

    .line 736
    iget v10, p0, Lcom/quicinc/cne/ICDClient;->flags:I

    or-int/lit8 v10, v10, 0x10

    iput v10, p0, Lcom/quicinc/cne/ICDClient;->flags:I

    .line 737
    const-string v10, "tput"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 738
    .local v9, "tput":Lorg/json/JSONObject;
    if-eqz v9, :cond_0

    .line 740
    const-string v10, "dl"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    iput v10, p0, Lcom/quicinc/cne/ICDClient;->dl:I

    .line 741
    iget v10, p0, Lcom/quicinc/cne/ICDClient;->flags:I

    or-int/lit8 v10, v10, 0x20

    iput v10, p0, Lcom/quicinc/cne/ICDClient;->flags:I

    .line 742
    const-string v10, "sdev"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    iput v10, p0, Lcom/quicinc/cne/ICDClient;->sdev:I

    .line 743
    iget v10, p0, Lcom/quicinc/cne/ICDClient;->flags:I

    or-int/lit8 v10, v10, 0x40

    iput v10, p0, Lcom/quicinc/cne/ICDClient;->flags:I

    .line 745
    :cond_0
    const-string v10, "prob"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 746
    .local v5, "prob":Lorg/json/JSONObject;
    if-eqz v5, :cond_1

    .line 748
    const-string v10, "icd"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    iput v10, p0, Lcom/quicinc/cne/ICDClient;->icdProb:I

    .line 749
    iget v10, p0, Lcom/quicinc/cne/ICDClient;->flags:I

    or-int/lit8 v10, v10, 0x2

    iput v10, p0, Lcom/quicinc/cne/ICDClient;->flags:I

    .line 750
    const-string v10, "bqe"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    iput v10, p0, Lcom/quicinc/cne/ICDClient;->bqeProb:I

    .line 751
    iget v10, p0, Lcom/quicinc/cne/ICDClient;->flags:I

    or-int/lit8 v10, v10, 0x8

    iput v10, p0, Lcom/quicinc/cne/ICDClient;->flags:I

    .line 753
    :cond_1
    const-string v10, "quota"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 754
    .local v8, "quota":Lorg/json/JSONObject;
    if-eqz v8, :cond_2

    .line 756
    const-string v10, "icd"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    iput v10, p0, Lcom/quicinc/cne/ICDClient;->icdQuota:I

    .line 757
    iget v10, p0, Lcom/quicinc/cne/ICDClient;->flags:I

    or-int/lit8 v10, v10, 0x1

    iput v10, p0, Lcom/quicinc/cne/ICDClient;->flags:I

    .line 758
    const-string v10, "bqe"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v10

    iput v10, p0, Lcom/quicinc/cne/ICDClient;->bqeQuota:I

    .line 759
    iget v10, p0, Lcom/quicinc/cne/ICDClient;->flags:I

    or-int/lit8 v10, v10, 0x4

    iput v10, p0, Lcom/quicinc/cne/ICDClient;->flags:I
    :try_end_3
    .catch Landroid/net/ParseException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    .line 802
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    .end local v5    # "prob":Lorg/json/JSONObject;
    .end local v6    # "len":J
    .end local v8    # "quota":Lorg/json/JSONObject;
    .end local v9    # "tput":Lorg/json/JSONObject;
    :cond_2
    :goto_0
    return-void

    .line 722
    .restart local v3    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "len":J
    :cond_3
    :try_start_4
    const-string v10, "WQE:ICD"

    const-string v11, "Received a mismatched bssid from the server in JSON response."

    invoke-static {v10, v11}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    const-string v10, "WQE:ICD"

    const-string v11, "Interpreting as ICD FAILURE"

    invoke-static {v10, v11}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    sget-object v10, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_FAILURE:Lcom/quicinc/cne/ICDClient$IcdResult;

    iput-object v10, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 725
    new-instance v10, Ljava/lang/Exception;

    invoke-direct {v10}, Ljava/lang/Exception;-><init>()V

    throw v10
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Landroid/net/ParseException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    .line 728
    :catch_0
    move-exception v0

    move-object v2, v3

    .line 730
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    .local v0, "e":Lorg/json/JSONException;
    .restart local v2    # "jsonObject":Lorg/json/JSONObject;
    :goto_1
    :try_start_5
    const-string v10, "WQE:ICD"

    const-string v11, "Didn\'t receive a JSON Object/bssid not present, possible captive portal"

    invoke-static {v10, v11}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    const-string v10, "WQE:ICD"

    const-string v11, "Interpreting as ICD FAILURE"

    invoke-static {v10, v11}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 732
    sget-object v10, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_FAILURE:Lcom/quicinc/cne/ICDClient$IcdResult;

    iput-object v10, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 733
    new-instance v10, Ljava/lang/Exception;

    invoke-direct {v10}, Ljava/lang/Exception;-><init>()V

    throw v10
    :try_end_5
    .catch Landroid/net/ParseException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    .line 762
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    .line 764
    .local v0, "e":Landroid/net/ParseException;
    const-string v10, "WQE:ICD"

    const-string v11, "Ignoring Parse  Exception"

    invoke-static {v10, v11}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    invoke-virtual {v0}, Landroid/net/ParseException;->printStackTrace()V

    goto :goto_0

    .line 768
    .end local v0    # "e":Landroid/net/ParseException;
    :catch_2
    move-exception v0

    .line 770
    .local v0, "e":Ljava/io/IOException;
    const-string v10, "WQE:ICD"

    const-string v11, "IO Exception"

    invoke-static {v10, v11}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 772
    const-string v10, "WQE:ICD"

    const-string v11, "Interpreting as ICD TIMEOUT"

    invoke-static {v10, v11}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 773
    sget-object v10, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_TIMEOUT:Lcom/quicinc/cne/ICDClient$IcdResult;

    iput-object v10, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    goto :goto_0

    .line 776
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 778
    .local v0, "e":Lorg/json/JSONException;
    const-string v10, "WQE:ICD"

    const-string v11, "Ignoring JSON Exception"

    invoke-static {v10, v11}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 779
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 782
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_4
    move-exception v0

    .line 784
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0

    .line 789
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    const-string v10, "WQE:ICD"

    const-string v11, "Invalid content length, bssid not present"

    invoke-static {v10, v11}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    const-string v10, "WQE:ICD"

    const-string v11, "Interpreting as ICD FAILURE"

    invoke-static {v10, v11}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    sget-object v10, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_FAILURE:Lcom/quicinc/cne/ICDClient$IcdResult;

    iput-object v10, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    goto :goto_0

    .line 797
    .end local v6    # "len":J
    :cond_5
    const-string v10, "WQE:ICD"

    const-string v11, "HTTP entity is null, bssid not present"

    invoke-static {v10, v11}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 798
    const-string v10, "WQE:ICD"

    const-string v11, "Interpreting as ICD FAILURE"

    invoke-static {v10, v11}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    sget-object v10, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_FAILURE:Lcom/quicinc/cne/ICDClient$IcdResult;

    iput-object v10, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    goto/16 :goto_0

    .line 728
    .restart local v2    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "len":J
    :catch_5
    move-exception v0

    goto :goto_1
.end method

.method private sendIcdParamClientReq()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 658
    const-string v1, "WQE:ICD"

    const-string v2, "sendIcdParamClientReq()"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    :try_start_0
    iget-object v1, p0, Lcom/quicinc/cne/ICDClient;->icdParamClient:Lorg/apache/http/client/HttpClient;

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient;->icdParamGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-interface {v1, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    iput-object v1, p0, Lcom/quicinc/cne/ICDClient;->icdParamRsp:Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 696
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :goto_0
    return-object v1

    .line 664
    :catch_0
    move-exception v0

    .line 666
    .local v0, "e":Lorg/apache/http/client/ClientProtocolException;
    const-string v1, "WQE:ICD"

    const-string v2, "Client protocol Exception Exception"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 668
    const-string v1, "WQE:ICD"

    const-string v2, "Interpreting as ICD TIMEOUT"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 669
    sget-object v1, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_TIMEOUT:Lcom/quicinc/cne/ICDClient$IcdResult;

    iput-object v1, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 670
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 672
    .end local v0    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_1
    move-exception v0

    .line 674
    .local v0, "e":Ljava/net/UnknownHostException;
    const-string v1, "WQE:ICD"

    const-string v2, "UnknownHost Exception"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    invoke-virtual {v0}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 676
    const-string v1, "WQE:ICD"

    const-string v2, "Interpreting as ICD TIMEOUT"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 677
    sget-object v1, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_TIMEOUT:Lcom/quicinc/cne/ICDClient$IcdResult;

    iput-object v1, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 678
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 680
    .end local v0    # "e":Ljava/net/UnknownHostException;
    :catch_2
    move-exception v0

    .line 682
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "WQE:ICD"

    const-string v2, "IO Exception"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 684
    const-string v1, "WQE:ICD"

    const-string v2, "Interpreting as ICD TIMEOUT"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    sget-object v1, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_TIMEOUT:Lcom/quicinc/cne/ICDClient$IcdResult;

    iput-object v1, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 686
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 688
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 690
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "WQE:ICD"

    const-string v2, " Server Exception"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 692
    const-string v1, "WQE:ICD"

    const-string v2, "Interpreting as ICD TIMEOUT"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    sget-object v1, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_TIMEOUT:Lcom/quicinc/cne/ICDClient$IcdResult;

    iput-object v1, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 694
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method private setBssid()V
    .locals 8

    .prologue
    .line 549
    const-string v4, "WQE:ICD"

    const-string v5, "setBssid()"

    invoke-static {v4, v5}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    iget-object v4, p0, Lcom/quicinc/cne/ICDClient;->wifiMgr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    .line 552
    .local v2, "wifiInfo":Landroid/net/wifi/WifiInfo;
    if-eqz v2, :cond_0

    .line 554
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v4

    const-string v5, ":"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/quicinc/cne/ICDClient;->bssid:Ljava/lang/String;

    .line 555
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/quicinc/cne/ICDClient;->currentBSSID:Ljava/lang/String;

    .line 556
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v4

    iput v4, p0, Lcom/quicinc/cne/ICDClient;->ipAddr:I

    .line 557
    const-string v4, "WQE:ICD"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bssid="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient;->bssid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " currentBSSID="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient;->currentBSSID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    const-string v4, "WQE:ICD"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Passed BSSID="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and IPAddr ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/Integer;

    iget v7, p0, Lcom/quicinc/cne/ICDClient;->ipAddr:I

    invoke-direct {v6, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    :goto_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/quicinc/cne/ICDClient;->bssidList:Ljava/util/ArrayList;

    .line 566
    iget-object v4, p0, Lcom/quicinc/cne/ICDClient;->wifiMgr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v3

    .line 567
    .local v3, "wifiScanResults":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    if-nez v3, :cond_1

    .line 568
    const-string v4, "WQE:ICD"

    const-string v5, "wifi scan result is null"

    invoke-static {v4, v5}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    :goto_1
    return-void

    .line 562
    .end local v3    # "wifiScanResults":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :cond_0
    const-string v4, "WQE:ICD"

    const-string v5, "wifiMgr RemoteException returned NULL"

    invoke-static {v4, v5}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 572
    .restart local v3    # "wifiScanResults":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/ScanResult;

    .line 573
    .local v1, "result":Landroid/net/wifi/ScanResult;
    if-eqz v1, :cond_2

    iget-object v4, v1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    if-nez v4, :cond_3

    .line 574
    :cond_2
    const-string v4, "WQE:ICD"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "@@@Received invalid scan result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 577
    :cond_3
    iget-object v4, p0, Lcom/quicinc/cne/ICDClient;->bssidList:Ljava/util/ArrayList;

    iget-object v5, v1, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    const-string v6, ":"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 579
    .end local v1    # "result":Landroid/net/wifi/ScanResult;
    :cond_4
    iget-object v4, p0, Lcom/quicinc/cne/ICDClient;->bssidList:Ljava/util/ArrayList;

    sget-object v5, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_1
.end method

.method private setIcdParamClientReq()Ljava/lang/Boolean;
    .locals 15

    .prologue
    .line 583
    const-string v0, "WQE:ICD"

    const-string v1, "setIcdParamClientReq()"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->bssid:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->currentBSSID:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 587
    :cond_0
    const-string v0, "WQE:ICD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failure :Bssid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient;->bssid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " currentBSSID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient;->currentBSSID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " passed BSSID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    const-string v0, "WQE:ICD"

    const-string v1, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    sget-object v0, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    iput-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 590
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 654
    :goto_0
    return-object v0

    .line 592
    :cond_1
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->uri:Ljava/net/URI;

    if-nez v0, :cond_2

    .line 594
    const-string v0, "WQE:ICD"

    const-string v1, "Failure :uri=null"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    const-string v0, "WQE:ICD"

    const-string v1, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    sget-object v0, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    iput-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 597
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 599
    :cond_2
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->currentBSSID:Ljava/lang/String;

    iget-object v1, p0, Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 601
    const-string v0, "WQE:ICD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failure :currentBSSID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient;->currentBSSID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " passed BSSID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 602
    const-string v0, "WQE:ICD"

    const-string v1, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    sget-object v0, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    iput-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 604
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 606
    :cond_3
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 607
    .local v11, "qparams":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "bssid"

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient;->bssid:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 608
    const/4 v10, 0x0

    .line 610
    .local v10, "numBssidAdded":I
    new-instance v13, Ljava/lang/StringBuilder;

    const/16 v0, 0x80

    invoke-direct {v13, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 611
    .local v13, "tmp1":Ljava/lang/StringBuilder;
    const/4 v9, 0x0

    .line 612
    .local v9, "len":I
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->bssidList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 613
    .local v12, "str":Ljava/lang/String;
    const-string v0, "WQE:ICD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "currentbssid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient;->bssid:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",nbssid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 614
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->bssid:Ljava/lang/String;

    invoke-virtual {v12, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 618
    const/4 v0, 0x1

    if-lt v10, v0, :cond_6

    const/4 v0, 0x4

    if-ge v10, v0, :cond_6

    .line 619
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v9, v0

    .line 620
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 621
    const/4 v0, 0x3

    if-eq v10, v0, :cond_5

    .line 622
    const-string v0, ","

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 623
    add-int/lit8 v9, v9, 0x1

    .line 625
    :cond_5
    add-int/lit8 v10, v10, 0x1

    .line 628
    :cond_6
    if-nez v10, :cond_4

    .line 629
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "np"

    invoke-direct {v0, v1, v12}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 630
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 634
    .end local v12    # "str":Ljava/lang/String;
    :cond_7
    const/4 v0, 0x0

    invoke-virtual {v13, v0, v9}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 635
    .local v14, "tmp2":Ljava/lang/String;
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "nx"

    invoke-direct {v0, v1, v14}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 638
    :try_start_0
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->uri:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/quicinc/cne/ICDClient;->uri:Ljava/net/URI;

    invoke-virtual {v1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient;->uri:Ljava/net/URI;

    invoke-virtual {v2}, Ljava/net/URI;->getPort()I

    move-result v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient;->uri:Ljava/net/URI;

    invoke-virtual {v3}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {v11, v4}, Lorg/apache/http/client/utils/URLEncodedUtils;->format(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lorg/apache/http/client/utils/URIUtils;->createURI(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    iput-object v0, p0, Lcom/quicinc/cne/ICDClient;->bssidURI:Ljava/net/URI;

    .line 640
    const-string v0, "WQE:ICD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "URI="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient;->bssidURI:Ljava/net/URI;

    invoke-virtual {v2}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 650
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdParamClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v7

    .line 651
    .local v7, "httpParams":Lorg/apache/http/params/HttpParams;
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    iget-object v1, p0, Lcom/quicinc/cne/ICDClient;->bssidURI:Ljava/net/URI;

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    iput-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdParamGet:Lorg/apache/http/client/methods/HttpGet;

    .line 652
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdParamGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v1, "Accept-Encoding"

    const-string v2, "gzip;q=0,deflate;q=0"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdParamGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v1, "Cache-Control"

    const-string v2, "no-cache"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    .line 642
    .end local v7    # "httpParams":Lorg/apache/http/params/HttpParams;
    :catch_0
    move-exception v6

    .line 644
    .local v6, "e1":Ljava/net/URISyntaxException;
    const-string v0, "WQE:ICD"

    const-string v1, "URI Syntax Exception"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    invoke-virtual {v6}, Ljava/net/URISyntaxException;->printStackTrace()V

    .line 646
    const-string v0, "WQE:ICD"

    const-string v1, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 647
    sget-object v0, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    iput-object v0, p0, Lcom/quicinc/cne/ICDClient;->icdParamReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 648
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 845
    const-string v0, "WQE:ICD"

    const-string v1, "ICDClient thread started"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    invoke-direct {p0}, Lcom/quicinc/cne/ICDClient;->setIcdParamClientReq()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 848
    invoke-direct {p0}, Lcom/quicinc/cne/ICDClient;->sendIcdParamClientReq()Ljava/lang/Boolean;

    .line 850
    :cond_0
    invoke-direct {p0}, Lcom/quicinc/cne/ICDClient;->parseIcdParamClientRsp()V

    .line 851
    return-void
.end method
