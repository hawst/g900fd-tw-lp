.class Lcom/quicinc/cne/CNE$3;
.super Landroid/net/ConnectivityManager$NetworkCallback;
.source "CNE.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quicinc/cne/CNE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/quicinc/cne/CNE;


# direct methods
.method constructor <init>(Lcom/quicinc/cne/CNE;)V
    .locals 0

    .prologue
    .line 1407
    iput-object p1, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    invoke-direct {p0}, Landroid/net/ConnectivityManager$NetworkCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onAvailable(Landroid/net/Network;)V
    .locals 6
    .param p1, "network"    # Landroid/net/Network;

    .prologue
    .line 1410
    const-string v2, "CORE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "network available: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    .line 1411
    iget-object v2, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # setter for: Lcom/quicinc/cne/CNE;->mWifiNetwork:Landroid/net/Network;
    invoke-static {v2, p1}, Lcom/quicinc/cne/CNE;->access$3602(Lcom/quicinc/cne/CNE;Landroid/net/Network;)Landroid/net/Network;

    .line 1412
    invoke-static {p1}, Landroid/net/ConnectivityManager;->setProcessDefaultNetwork(Landroid/net/Network;)Z

    move-result v0

    .line 1413
    .local v0, "flag":Z
    if-eqz v0, :cond_0

    .line 1414
    const-string v2, "CORE"

    const-string v3, "onAvailable: bind the process to WIFI"

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    .line 1416
    :cond_0
    iget-object v2, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->cm:Landroid/net/ConnectivityManager;
    invoke-static {v2}, Lcom/quicinc/cne/CNE;->access$3700(Lcom/quicinc/cne/CNE;)Landroid/net/ConnectivityManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/net/ConnectivityManager;->getLinkProperties(Landroid/net/Network;)Landroid/net/LinkProperties;

    move-result-object v1

    .line 1417
    .local v1, "lp":Landroid/net/LinkProperties;
    if-nez v1, :cond_1

    .line 1418
    const-string v2, "CORE"

    const-string v3, "Lp is null"

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    .line 1419
    iget-object v2, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->mIPFamilyLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/quicinc/cne/CNE;->access$3800(Lcom/quicinc/cne/CNE;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 1420
    :try_start_0
    iget-object v2, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    iget-object v4, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    const/4 v5, 0x0

    # setter for: Lcom/quicinc/cne/CNE;->IPV6Available:Z
    invoke-static {v4, v5}, Lcom/quicinc/cne/CNE;->access$4002(Lcom/quicinc/cne/CNE;Z)Z

    move-result v4

    # setter for: Lcom/quicinc/cne/CNE;->IPV4Available:Z
    invoke-static {v2, v4}, Lcom/quicinc/cne/CNE;->access$3902(Lcom/quicinc/cne/CNE;Z)Z

    .line 1421
    monitor-exit v3

    .line 1428
    :goto_0
    return-void

    .line 1421
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1424
    :cond_1
    iget-object v2, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # invokes: Lcom/quicinc/cne/CNE;->checkIPFamilyAvailability(Landroid/net/LinkProperties;)V
    invoke-static {v2, v1}, Lcom/quicinc/cne/CNE;->access$4100(Lcom/quicinc/cne/CNE;Landroid/net/LinkProperties;)V

    .line 1425
    iget-object v2, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    const/4 v3, 0x1

    # setter for: Lcom/quicinc/cne/CNE;->isWifiConnected:Z
    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->access$1002(Lcom/quicinc/cne/CNE;Z)Z

    .line 1426
    iget-object v2, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    iget-object v3, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->isWifiConnected:Z
    invoke-static {v3}, Lcom/quicinc/cne/CNE;->access$1000(Lcom/quicinc/cne/CNE;)Z

    move-result v3

    iget-object v4, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # invokes: Lcom/quicinc/cne/CNE;->getInetFamily()I
    invoke-static {v4}, Lcom/quicinc/cne/CNE;->access$1100(Lcom/quicinc/cne/CNE;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/quicinc/cne/CNE;->notifyWlanConnectivityUp(ZI)Z

    .line 1427
    iget-object v2, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # invokes: Lcom/quicinc/cne/CNE;->updateLinkProperties(Landroid/net/LinkProperties;)V
    invoke-static {v2, v1}, Lcom/quicinc/cne/CNE;->access$4200(Lcom/quicinc/cne/CNE;Landroid/net/LinkProperties;)V

    goto :goto_0
.end method

.method public onLinkPropertiesChanged(Landroid/net/Network;Landroid/net/LinkProperties;)V
    .locals 4
    .param p1, "network"    # Landroid/net/Network;
    .param p2, "lp"    # Landroid/net/LinkProperties;

    .prologue
    .line 1445
    if-nez p2, :cond_1

    .line 1446
    const-string v0, "CORE"

    const-string v1, "Lp is null"

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    .line 1447
    iget-object v0, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->mIPFamilyLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/quicinc/cne/CNE;->access$3800(Lcom/quicinc/cne/CNE;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1448
    :try_start_0
    iget-object v0, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    iget-object v2, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    const/4 v3, 0x0

    # setter for: Lcom/quicinc/cne/CNE;->IPV6Available:Z
    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->access$4002(Lcom/quicinc/cne/CNE;Z)Z

    move-result v2

    # setter for: Lcom/quicinc/cne/CNE;->IPV4Available:Z
    invoke-static {v0, v2}, Lcom/quicinc/cne/CNE;->access$3902(Lcom/quicinc/cne/CNE;Z)Z

    .line 1449
    monitor-exit v1

    .line 1457
    :cond_0
    :goto_0
    return-void

    .line 1449
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1452
    :cond_1
    iget-object v0, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # invokes: Lcom/quicinc/cne/CNE;->checkIPFamilyAvailability(Landroid/net/LinkProperties;)V
    invoke-static {v0, p2}, Lcom/quicinc/cne/CNE;->access$4100(Lcom/quicinc/cne/CNE;Landroid/net/LinkProperties;)V

    .line 1453
    iget-object v0, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->curLp:Landroid/net/LinkProperties;
    invoke-static {v0}, Lcom/quicinc/cne/CNE;->access$4500(Lcom/quicinc/cne/CNE;)Landroid/net/LinkProperties;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # invokes: Lcom/quicinc/cne/CNE;->compareLinkProperties(Landroid/net/LinkProperties;)Z
    invoke-static {v0, p2}, Lcom/quicinc/cne/CNE;->access$4600(Lcom/quicinc/cne/CNE;Landroid/net/LinkProperties;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1454
    :cond_2
    iget-object v0, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    iget-object v1, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->isWifiConnected:Z
    invoke-static {v1}, Lcom/quicinc/cne/CNE;->access$1000(Lcom/quicinc/cne/CNE;)Z

    move-result v1

    iget-object v2, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # invokes: Lcom/quicinc/cne/CNE;->getInetFamily()I
    invoke-static {v2}, Lcom/quicinc/cne/CNE;->access$1100(Lcom/quicinc/cne/CNE;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/quicinc/cne/CNE;->notifyWlanConnectivityUp(ZI)Z

    .line 1455
    iget-object v0, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # invokes: Lcom/quicinc/cne/CNE;->updateLinkProperties(Landroid/net/LinkProperties;)V
    invoke-static {v0, p2}, Lcom/quicinc/cne/CNE;->access$4200(Lcom/quicinc/cne/CNE;Landroid/net/LinkProperties;)V

    goto :goto_0
.end method

.method public onLost(Landroid/net/Network;)V
    .locals 4
    .param p1, "network"    # Landroid/net/Network;

    .prologue
    .line 1432
    const-string v0, "CORE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "network lost: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    .line 1433
    const/4 v0, 0x0

    invoke-static {v0}, Landroid/net/ConnectivityManager;->setProcessDefaultNetwork(Landroid/net/Network;)Z

    .line 1434
    const-string v0, "CORE"

    const-string v1, "onLost: unbind the process to WIFI"

    # invokes: Lcom/quicinc/cne/CNE;->logw(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE;->access$300(Ljava/lang/String;Ljava/lang/String;)V

    .line 1435
    iget-object v0, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->mIPFamilyLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/quicinc/cne/CNE;->access$3800(Lcom/quicinc/cne/CNE;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1436
    :try_start_0
    iget-object v0, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    iget-object v2, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    const/4 v3, 0x0

    # setter for: Lcom/quicinc/cne/CNE;->IPV6Available:Z
    invoke-static {v2, v3}, Lcom/quicinc/cne/CNE;->access$4002(Lcom/quicinc/cne/CNE;Z)Z

    move-result v2

    # setter for: Lcom/quicinc/cne/CNE;->IPV4Available:Z
    invoke-static {v0, v2}, Lcom/quicinc/cne/CNE;->access$3902(Lcom/quicinc/cne/CNE;Z)Z

    .line 1437
    iget-object v0, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    const/4 v2, 0x0

    # setter for: Lcom/quicinc/cne/CNE;->lastFamilyType:I
    invoke-static {v0, v2}, Lcom/quicinc/cne/CNE;->access$4302(Lcom/quicinc/cne/CNE;I)I

    .line 1438
    iget-object v0, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    const/4 v2, 0x0

    # setter for: Lcom/quicinc/cne/CNE;->isWifiConnected:Z
    invoke-static {v0, v2}, Lcom/quicinc/cne/CNE;->access$1002(Lcom/quicinc/cne/CNE;Z)Z

    .line 1439
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1440
    iget-object v0, p0, Lcom/quicinc/cne/CNE$3;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->netId:[I
    invoke-static {v0}, Lcom/quicinc/cne/CNE;->access$4400(Lcom/quicinc/cne/CNE;)[I

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, -0x1

    aput v2, v0, v1

    .line 1441
    return-void

    .line 1439
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
