.class public Lcom/quicinc/cne/ICDClient$icdHttp;
.super Ljava/lang/Object;
.source "ICDClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quicinc/cne/ICDClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "icdHttp"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quicinc/cne/ICDClient$icdHttp$ICDTimerTask;
    }
.end annotation


# instance fields
.field private hostName:Ljava/lang/String;

.field private httpURL:Ljava/lang/String;

.field private httpUriAddress:Ljava/lang/String;

.field private httpuri:Ljava/lang/String;

.field final synthetic this$0:Lcom/quicinc/cne/ICDClient;


# direct methods
.method public constructor <init>(Lcom/quicinc/cne/ICDClient;Ljava/lang/String;)V
    .locals 4
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    const-string v0, "WQE:ICD"

    const-string v1, "icdHttp - constructor"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    iput-object p2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->httpuri:Ljava/lang/String;

    .line 167
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    # setter for: Lcom/quicinc/cne/ICDClient;->timeout:Ljava/lang/Boolean;
    invoke-static {p1, v0}, Lcom/quicinc/cne/ICDClient;->access$002(Lcom/quicinc/cne/ICDClient;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 168
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    # setter for: Lcom/quicinc/cne/ICDClient;->timer:Ljava/util/Timer;
    invoke-static {p1, v0}, Lcom/quicinc/cne/ICDClient;->access$102(Lcom/quicinc/cne/ICDClient;Ljava/util/Timer;)Ljava/util/Timer;

    .line 169
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->httpuri:Ljava/lang/String;

    iput-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->httpURL:Ljava/lang/String;

    .line 170
    # getter for: Lcom/quicinc/cne/ICDClient;->timer:Ljava/util/Timer;
    invoke-static {p1}, Lcom/quicinc/cne/ICDClient;->access$100(Lcom/quicinc/cne/ICDClient;)Ljava/util/Timer;

    move-result-object v0

    new-instance v1, Lcom/quicinc/cne/ICDClient$icdHttp$ICDTimerTask;

    invoke-direct {v1, p0}, Lcom/quicinc/cne/ICDClient$icdHttp$ICDTimerTask;-><init>(Lcom/quicinc/cne/ICDClient$icdHttp;)V

    # getter for: Lcom/quicinc/cne/ICDClient;->seconds:I
    invoke-static {p1}, Lcom/quicinc/cne/ICDClient;->access$200(Lcom/quicinc/cne/ICDClient;)I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 171
    return-void
.end method

.method static synthetic access$300(Lcom/quicinc/cne/ICDClient$icdHttp;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/ICDClient$icdHttp;

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/quicinc/cne/ICDClient$icdHttp;->parseIcdHttpClientRsp()V

    return-void
.end method

.method private getHostFromURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 185
    const/4 v1, 0x0

    .line 186
    .local v1, "startPos":I
    const-string v2, "http://"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 187
    const-string v2, "WQE:ICD"

    const-string v3, "The URL doesn\'t start with http. Returning null"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const/4 v2, 0x0

    .line 195
    :goto_0
    return-object v2

    .line 190
    :cond_0
    const/4 v1, 0x7

    .line 191
    const-string v2, "/"

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 192
    .local v0, "endPos":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 193
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    .line 195
    :cond_1
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method private parseIcdHttpClientRsp()V
    .locals 7

    .prologue
    .line 320
    const-string v2, "WQE:ICD"

    const-string v3, "parseIcdHttpClientRsp()"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->rspLock:Ljava/util/concurrent/locks/Lock;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1400(Lcom/quicinc/cne/ICDClient;)Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 322
    const-string v2, "WQE:ICD"

    const-string v3, "parseIcdHttpClientRsp() Locked"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->sentHttpRsp:Ljava/lang/Boolean;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1500(Lcom/quicinc/cne/ICDClient;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 326
    const-string v2, "WQE:ICD"

    const-string v3, "ICD Http Response sent already, doing nothing"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    :goto_0
    const-string v2, "WQE:ICD"

    const-string v3, "Cancelling the ICD timer thread"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->timer:Ljava/util/Timer;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$100(Lcom/quicinc/cne/ICDClient;)Ljava/util/Timer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Timer;->cancel()V

    .line 453
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    # setter for: Lcom/quicinc/cne/ICDClient;->sentHttpRsp:Ljava/lang/Boolean;
    invoke-static {v2, v3}, Lcom/quicinc/cne/ICDClient;->access$1502(Lcom/quicinc/cne/ICDClient;Ljava/lang/Boolean;)Ljava/lang/Boolean;

    .line 454
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->rspLock:Ljava/util/concurrent/locks/Lock;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1400(Lcom/quicinc/cne/ICDClient;)Ljava/util/concurrent/locks/Lock;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 455
    const-string v2, "WQE:ICD"

    const-string v3, "parseIcdHttpClientRsp() UnLocked"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    return-void

    .line 330
    :cond_0
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->timeout:Ljava/lang/Boolean;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$000(Lcom/quicinc/cne/ICDClient;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 332
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v3, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_TIMEOUT:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2, v3}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 333
    const-string v2, "WQE:ICD"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Http ICDRequest failure.Reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/quicinc/cne/ICDClient$IcdResult;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1700(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/CNE;

    move-result-object v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quicinc/cne/ICDClient$IcdResult;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->tid:I
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$1600(Lcom/quicinc/cne/ICDClient;)I

    move-result v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->family:I
    invoke-static {v6}, Lcom/quicinc/cne/ICDClient;->access$500(Lcom/quicinc/cne/ICDClient;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/quicinc/cne/CNE;->sendIcdHttpResponse(ILjava/lang/String;II)V

    goto/16 :goto_0

    .line 339
    :cond_1
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 341
    const-string v2, "WQE:ICD"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ICD Http Request failure.Reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v4

    invoke-virtual {v4}, Lcom/quicinc/cne/ICDClient$IcdResult;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1700(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/CNE;

    move-result-object v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quicinc/cne/ICDClient$IcdResult;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->tid:I
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$1600(Lcom/quicinc/cne/ICDClient;)I

    move-result v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->family:I
    invoke-static {v6}, Lcom/quicinc/cne/ICDClient;->access$500(Lcom/quicinc/cne/ICDClient;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/quicinc/cne/CNE;->sendIcdHttpResponse(ILjava/lang/String;II)V

    goto/16 :goto_0

    .line 347
    :cond_2
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpRsp:Lorg/apache/http/HttpResponse;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1300(Lcom/quicinc/cne/ICDClient;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    .line 348
    .local v1, "statusCode":I
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpRsp:Lorg/apache/http/HttpResponse;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1300(Lcom/quicinc/cne/ICDClient;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v0

    .line 349
    .local v0, "reasonPhrase":Ljava/lang/String;
    const-string v2, "WQE:ICD"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ICD Http Response status code: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 350
    const/16 v2, 0xc8

    if-ne v1, v2, :cond_4

    .line 352
    const-string v2, "WQE:ICD"

    const-string v3, "ICD Http Request successful"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v3, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_SUCCESS:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2, v3}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 354
    invoke-direct {p0}, Lcom/quicinc/cne/ICDClient$icdHttp;->parseIcdHttpJsonRsp()V

    .line 355
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v2

    sget-object v3, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_SUCCESS:Lcom/quicinc/cne/ICDClient$IcdResult;

    if-eq v2, v3, :cond_3

    .line 357
    const-string v2, "WQE:ICD"

    const-string v3, "ICD Http Json Parsing not successful"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    :cond_3
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1700(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/CNE;

    move-result-object v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quicinc/cne/ICDClient$IcdResult;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->tid:I
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$1600(Lcom/quicinc/cne/ICDClient;)I

    move-result v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->family:I
    invoke-static {v6}, Lcom/quicinc/cne/ICDClient;->access$500(Lcom/quicinc/cne/ICDClient;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/quicinc/cne/CNE;->sendIcdHttpResponse(ILjava/lang/String;II)V

    goto/16 :goto_0

    .line 362
    :cond_4
    const/16 v2, 0x12e

    if-ne v1, v2, :cond_5

    .line 364
    const-string v2, "WQE:ICD"

    const-string v3, "Interpreting as ICD FAILURE"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v3, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_FAILURE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2, v3}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 366
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1700(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/CNE;

    move-result-object v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quicinc/cne/ICDClient$IcdResult;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->tid:I
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$1600(Lcom/quicinc/cne/ICDClient;)I

    move-result v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->family:I
    invoke-static {v6}, Lcom/quicinc/cne/ICDClient;->access$500(Lcom/quicinc/cne/ICDClient;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/quicinc/cne/CNE;->sendIcdHttpResponse(ILjava/lang/String;II)V

    goto/16 :goto_0

    .line 369
    :cond_5
    const/16 v2, 0x190

    if-ne v1, v2, :cond_6

    .line 371
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v3, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2, v3}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 372
    const-string v2, "WQE:ICD"

    const-string v3, "Assuming ICDRequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    const-string v2, "WQE:ICD"

    const-string v3, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1700(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/CNE;

    move-result-object v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quicinc/cne/ICDClient$IcdResult;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->tid:I
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$1600(Lcom/quicinc/cne/ICDClient;)I

    move-result v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->family:I
    invoke-static {v6}, Lcom/quicinc/cne/ICDClient;->access$500(Lcom/quicinc/cne/ICDClient;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/quicinc/cne/CNE;->sendIcdHttpResponse(ILjava/lang/String;II)V

    goto/16 :goto_0

    .line 377
    :cond_6
    const/16 v2, 0x194

    if-ne v1, v2, :cond_7

    .line 379
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v3, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2, v3}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 380
    const-string v2, "WQE:ICD"

    const-string v3, "Assuming ICDRequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    const-string v2, "WQE:ICD"

    const-string v3, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1700(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/CNE;

    move-result-object v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quicinc/cne/ICDClient$IcdResult;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->tid:I
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$1600(Lcom/quicinc/cne/ICDClient;)I

    move-result v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->family:I
    invoke-static {v6}, Lcom/quicinc/cne/ICDClient;->access$500(Lcom/quicinc/cne/ICDClient;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/quicinc/cne/CNE;->sendIcdHttpResponse(ILjava/lang/String;II)V

    goto/16 :goto_0

    .line 385
    :cond_7
    const/16 v2, 0x195

    if-ne v1, v2, :cond_8

    .line 387
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v3, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2, v3}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 388
    const-string v2, "WQE:ICD"

    const-string v3, "Assuming ICDRequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    const-string v2, "WQE:ICD"

    const-string v3, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1700(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/CNE;

    move-result-object v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quicinc/cne/ICDClient$IcdResult;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->tid:I
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$1600(Lcom/quicinc/cne/ICDClient;)I

    move-result v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->family:I
    invoke-static {v6}, Lcom/quicinc/cne/ICDClient;->access$500(Lcom/quicinc/cne/ICDClient;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/quicinc/cne/CNE;->sendIcdHttpResponse(ILjava/lang/String;II)V

    goto/16 :goto_0

    .line 393
    :cond_8
    const/16 v2, 0x196

    if-ne v1, v2, :cond_9

    .line 395
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v3, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2, v3}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 396
    const-string v2, "WQE:ICD"

    const-string v3, "Assuming ICDRequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    const-string v2, "WQE:ICD"

    const-string v3, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 398
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1700(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/CNE;

    move-result-object v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quicinc/cne/ICDClient$IcdResult;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->tid:I
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$1600(Lcom/quicinc/cne/ICDClient;)I

    move-result v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->family:I
    invoke-static {v6}, Lcom/quicinc/cne/ICDClient;->access$500(Lcom/quicinc/cne/ICDClient;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/quicinc/cne/CNE;->sendIcdHttpResponse(ILjava/lang/String;II)V

    goto/16 :goto_0

    .line 401
    :cond_9
    const/16 v2, 0x1f4

    if-ne v1, v2, :cond_a

    .line 403
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v3, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2, v3}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 404
    const-string v2, "WQE:ICD"

    const-string v3, "Assuming ICDRequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    const-string v2, "WQE:ICD"

    const-string v3, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1700(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/CNE;

    move-result-object v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quicinc/cne/ICDClient$IcdResult;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->tid:I
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$1600(Lcom/quicinc/cne/ICDClient;)I

    move-result v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->family:I
    invoke-static {v6}, Lcom/quicinc/cne/ICDClient;->access$500(Lcom/quicinc/cne/ICDClient;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/quicinc/cne/CNE;->sendIcdHttpResponse(ILjava/lang/String;II)V

    goto/16 :goto_0

    .line 409
    :cond_a
    const/16 v2, 0x1f5

    if-ne v1, v2, :cond_b

    .line 411
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v3, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2, v3}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 412
    const-string v2, "WQE:ICD"

    const-string v3, "Assuming ICDRequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    const-string v2, "WQE:ICD"

    const-string v3, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1700(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/CNE;

    move-result-object v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quicinc/cne/ICDClient$IcdResult;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->tid:I
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$1600(Lcom/quicinc/cne/ICDClient;)I

    move-result v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->family:I
    invoke-static {v6}, Lcom/quicinc/cne/ICDClient;->access$500(Lcom/quicinc/cne/ICDClient;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/quicinc/cne/CNE;->sendIcdHttpResponse(ILjava/lang/String;II)V

    goto/16 :goto_0

    .line 417
    :cond_b
    const/16 v2, 0x1f7

    if-ne v1, v2, :cond_c

    .line 419
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v3, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2, v3}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 420
    const-string v2, "WQE:ICD"

    const-string v3, "Assuming ICDRequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const-string v2, "WQE:ICD"

    const-string v3, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1700(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/CNE;

    move-result-object v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quicinc/cne/ICDClient$IcdResult;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->tid:I
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$1600(Lcom/quicinc/cne/ICDClient;)I

    move-result v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->family:I
    invoke-static {v6}, Lcom/quicinc/cne/ICDClient;->access$500(Lcom/quicinc/cne/ICDClient;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/quicinc/cne/CNE;->sendIcdHttpResponse(ILjava/lang/String;II)V

    goto/16 :goto_0

    .line 425
    :cond_c
    const/16 v2, 0x1f9

    if-ne v1, v2, :cond_d

    .line 427
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v3, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2, v3}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 428
    const-string v2, "WQE:ICD"

    const-string v3, "Assuming ICDRequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    const-string v2, "WQE:ICD"

    const-string v3, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1700(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/CNE;

    move-result-object v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quicinc/cne/ICDClient$IcdResult;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->tid:I
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$1600(Lcom/quicinc/cne/ICDClient;)I

    move-result v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->family:I
    invoke-static {v6}, Lcom/quicinc/cne/ICDClient;->access$500(Lcom/quicinc/cne/ICDClient;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/quicinc/cne/CNE;->sendIcdHttpResponse(ILjava/lang/String;II)V

    goto/16 :goto_0

    .line 433
    :cond_d
    const/16 v2, 0x64

    if-ge v1, v2, :cond_e

    if-ltz v1, :cond_e

    .line 435
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v3, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2, v3}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 436
    const-string v2, "WQE:ICD"

    const-string v3, "Assuming ICDRequest successful, as Origin servers are down"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const-string v2, "WQE:ICD"

    const-string v3, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1700(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/CNE;

    move-result-object v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quicinc/cne/ICDClient$IcdResult;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->tid:I
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$1600(Lcom/quicinc/cne/ICDClient;)I

    move-result v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->family:I
    invoke-static {v6}, Lcom/quicinc/cne/ICDClient;->access$500(Lcom/quicinc/cne/ICDClient;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/quicinc/cne/CNE;->sendIcdHttpResponse(ILjava/lang/String;II)V

    goto/16 :goto_0

    .line 443
    :cond_e
    const-string v2, "WQE:ICD"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Interpreting as ICD FAILURE - statusCode:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v3, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_FAILURE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2, v3}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 445
    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->cneHandle:Lcom/quicinc/cne/CNE;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1700(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/CNE;

    move-result-object v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/quicinc/cne/ICDClient$IcdResult;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v4}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->tid:I
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$1600(Lcom/quicinc/cne/ICDClient;)I

    move-result v5

    iget-object v6, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->family:I
    invoke-static {v6}, Lcom/quicinc/cne/ICDClient;->access$500(Lcom/quicinc/cne/ICDClient;)I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/quicinc/cne/CNE;->sendIcdHttpResponse(ILjava/lang/String;II)V

    goto/16 :goto_0
.end method

.method private parseIcdHttpJsonRsp()V
    .locals 10

    .prologue
    .line 459
    const-string v5, "WQE:ICD"

    const-string v8, "parseIcdHttpJsonRsp()"

    invoke-static {v5, v8}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    const/4 v4, 0x0

    .line 461
    .local v4, "jsonString":Ljava/lang/String;
    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpRsp:Lorg/apache/http/HttpResponse;
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$1300(Lcom/quicinc/cne/ICDClient;)Lorg/apache/http/HttpResponse;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 462
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v1, :cond_2

    .line 464
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v6

    .line 465
    .local v6, "len":J
    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_1

    const-wide/32 v8, 0x7fffffff

    cmp-long v5, v6, v8

    if-gez v5, :cond_1

    .line 469
    :try_start_0
    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v4

    .line 470
    const-string v5, "WQE:ICD"

    invoke-static {v5, v4}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/net/ParseException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4

    .line 471
    const/4 v2, 0x0

    .line 474
    .local v2, "jsonObject":Lorg/json/JSONObject;
    :try_start_1
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Landroid/net/ParseException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    .line 475
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    .local v3, "jsonObject":Lorg/json/JSONObject;
    :try_start_2
    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssid:Ljava/lang/String;
    invoke-static {v5}, Lcom/quicinc/cne/ICDClient;->access$700(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v5

    const-string v8, "bssid"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 477
    const-string v5, "WQE:ICD"

    const-string v8, "Bssids match, Http ICD PASS"

    invoke-static {v5, v8}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v8, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_SUCCESS:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v5, v8}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 536
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    .end local v6    # "len":J
    :goto_0
    return-void

    .line 482
    .restart local v3    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "len":J
    :cond_0
    const-string v5, "WQE:ICD"

    const-string v8, "Received a mismatched bssid from the server in JSON response."

    invoke-static {v5, v8}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    const-string v5, "WQE:ICD"

    const-string v8, "Interpreting as ICD FAILURE"

    invoke-static {v5, v8}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v8, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_FAILURE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v5, v8}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 485
    new-instance v5, Ljava/lang/Exception;

    invoke-direct {v5}, Ljava/lang/Exception;-><init>()V

    throw v5
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/net/ParseException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_4

    .line 488
    :catch_0
    move-exception v0

    move-object v2, v3

    .line 490
    .end local v3    # "jsonObject":Lorg/json/JSONObject;
    .local v0, "e":Lorg/json/JSONException;
    .restart local v2    # "jsonObject":Lorg/json/JSONObject;
    :goto_1
    :try_start_3
    const-string v5, "WQE:ICD"

    const-string v8, "Didn\'t receive a JSON Object/bssid not present, possible captive portal"

    invoke-static {v5, v8}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    const-string v5, "WQE:ICD"

    const-string v8, "Interpreting as ICD FAILURE"

    invoke-static {v5, v8}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v8, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_FAILURE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v5, v8}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 493
    new-instance v5, Ljava/lang/Exception;

    invoke-direct {v5}, Ljava/lang/Exception;-><init>()V

    throw v5
    :try_end_3
    .catch Landroid/net/ParseException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    .line 496
    .end local v0    # "e":Lorg/json/JSONException;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :catch_1
    move-exception v0

    .line 498
    .local v0, "e":Landroid/net/ParseException;
    const-string v5, "WQE:ICD"

    const-string v8, "Ignoring Parse  Exception"

    invoke-static {v5, v8}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    invoke-virtual {v0}, Landroid/net/ParseException;->printStackTrace()V

    goto :goto_0

    .line 502
    .end local v0    # "e":Landroid/net/ParseException;
    :catch_2
    move-exception v0

    .line 504
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "WQE:ICD"

    const-string v8, "IO Exception"

    invoke-static {v5, v8}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 506
    const-string v5, "WQE:ICD"

    const-string v8, "Interpreting as ICD TIMEOUT"

    invoke-static {v5, v8}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v8, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_TIMEOUT:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v5, v8}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    goto :goto_0

    .line 510
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 512
    .local v0, "e":Lorg/json/JSONException;
    const-string v5, "WQE:ICD"

    const-string v8, "Ignoring JSON Exception"

    invoke-static {v5, v8}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0

    .line 516
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_4
    move-exception v0

    .line 518
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0

    .line 523
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v5, "WQE:ICD"

    const-string v8, "Invalid content length, bssid not present"

    invoke-static {v5, v8}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 524
    const-string v5, "WQE:ICD"

    const-string v8, "Interpreting as ICD FAILURE"

    invoke-static {v5, v8}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v8, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_FAILURE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v5, v8}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    goto/16 :goto_0

    .line 531
    .end local v6    # "len":J
    :cond_2
    const-string v5, "WQE:ICD"

    const-string v8, "HTTP entity is null, bssid not present"

    invoke-static {v5, v8}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    const-string v5, "WQE:ICD"

    const-string v8, "Interpreting as ICD FAILURE"

    invoke-static {v5, v8}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    iget-object v5, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v8, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_FAILURE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v5, v8}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    goto/16 :goto_0

    .line 488
    .restart local v2    # "jsonObject":Lorg/json/JSONObject;
    .restart local v6    # "len":J
    :catch_5
    move-exception v0

    goto :goto_1
.end method

.method private sendIcdHttpClientReq()Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 279
    const-string v1, "WQE:ICD"

    const-string v2, "sendIcdHttpClientReq()"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    :try_start_0
    iget-object v1, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpClient:Lorg/apache/http/client/HttpClient;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1100(Lcom/quicinc/cne/ICDClient;)Lorg/apache/http/client/HttpClient;

    move-result-object v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpGet:Lorg/apache/http/client/methods/HttpGet;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$1200(Lcom/quicinc/cne/ICDClient;)Lorg/apache/http/client/methods/HttpGet;

    move-result-object v3

    invoke-interface {v2, v3}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v2

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpRsp:Lorg/apache/http/HttpResponse;
    invoke-static {v1, v2}, Lcom/quicinc/cne/ICDClient;->access$1302(Lcom/quicinc/cne/ICDClient;Lorg/apache/http/HttpResponse;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 316
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :goto_0
    return-object v1

    .line 284
    :catch_0
    move-exception v0

    .line 286
    .local v0, "e":Lorg/apache/http/client/ClientProtocolException;
    const-string v1, "WQE:ICD"

    const-string v2, "Client protocol Exception Exception"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 288
    const-string v1, "WQE:ICD"

    const-string v2, "Interpreting as ICD TIMEOUT"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    iget-object v1, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v2, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_TIMEOUT:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v1, v2}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 290
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 292
    .end local v0    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_1
    move-exception v0

    .line 294
    .local v0, "e":Ljava/net/UnknownHostException;
    const-string v1, "WQE:ICD"

    const-string v2, "UnknownHost Exception"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-virtual {v0}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 296
    const-string v1, "WQE:ICD"

    const-string v2, "Interpreting as ICD TIMEOUT"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    iget-object v1, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v2, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_TIMEOUT:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v1, v2}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 298
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 300
    .end local v0    # "e":Ljava/net/UnknownHostException;
    :catch_2
    move-exception v0

    .line 302
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "WQE:ICD"

    const-string v2, "IO Exception"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 303
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 304
    const-string v1, "WQE:ICD"

    const-string v2, "Interpreting as ICD TIMEOUT"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    iget-object v1, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v2, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_TIMEOUT:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v1, v2}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 306
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0

    .line 308
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 310
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "WQE:ICD"

    const-string v2, " Server Exception"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 312
    const-string v1, "WQE:ICD"

    const-string v2, "Interpreting as ICD TIMEOUT"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    iget-object v1, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v2, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_TIMEOUT:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v1, v2}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 314
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method private setIcdHttpClientReq()Ljava/lang/Boolean;
    .locals 14

    .prologue
    const/4 v11, 0x1

    const/4 v13, 0x0

    .line 199
    const-string v0, "WQE:ICD"

    const-string v1, "setIcdHttpClientReq()"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    :try_start_0
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->httpuri:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/quicinc/cne/ICDClient$icdHttp;->getHostFromURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->hostName:Ljava/lang/String;

    .line 202
    const-string v0, "WQE:ICD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HostName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->hostName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->hostName:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 204
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->hostName:Ljava/lang/String;

    invoke-static {v0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v10

    .line 205
    .local v10, "serverAddr":Ljava/net/InetAddress;
    if-eqz v10, :cond_0

    .line 206
    invoke-virtual {v10}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->httpUriAddress:Ljava/lang/String;

    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->httpUriAddress:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/cne/v1/icd"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->httpURL:Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1

    .line 209
    :try_start_1
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    new-instance v1, Ljava/net/URI;

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->httpURL:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    # setter for: Lcom/quicinc/cne/ICDClient;->httpURI:Ljava/net/URI;
    invoke-static {v0, v1}, Lcom/quicinc/cne/ICDClient;->access$402(Lcom/quicinc/cne/ICDClient;Ljava/net/URI;)Ljava/net/URI;
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1

    .line 216
    :goto_0
    :try_start_2
    iget-object v1, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    instance-of v0, v10, Ljava/net/Inet4Address;

    if-eqz v0, :cond_2

    move v0, v11

    :goto_1
    # setter for: Lcom/quicinc/cne/ICDClient;->family:I
    invoke-static {v1, v0}, Lcom/quicinc/cne/ICDClient;->access$502(Lcom/quicinc/cne/ICDClient;I)I
    :try_end_2
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_1

    .line 229
    .end local v10    # "serverAddr":Ljava/net/InetAddress;
    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssid:Ljava/lang/String;
    invoke-static {v0}, Lcom/quicinc/cne/ICDClient;->access$700(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->currentBSSID:Ljava/lang/String;
    invoke-static {v0}, Lcom/quicinc/cne/ICDClient;->access$800(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v0}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 231
    :cond_1
    const-string v0, "WQE:ICD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failure :Bssid="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssid:Ljava/lang/String;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$700(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " currentBSSID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->currentBSSID:Ljava/lang/String;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$800(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " passed BSSID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const-string v0, "WQE:ICD"

    const-string v1, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v1, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v0, v1}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 234
    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 275
    :goto_3
    return-object v0

    .line 211
    .restart local v10    # "serverAddr":Ljava/net/InetAddress;
    :catch_0
    move-exception v6

    .line 213
    .local v6, "e":Ljava/net/URISyntaxException;
    :try_start_3
    const-string v0, "WQE:ICD"

    const-string v1, "Received Invalid URI , continuing with null URI"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    const/4 v1, 0x0

    # setter for: Lcom/quicinc/cne/ICDClient;->httpURI:Ljava/net/URI;
    invoke-static {v0, v1}, Lcom/quicinc/cne/ICDClient;->access$402(Lcom/quicinc/cne/ICDClient;Ljava/net/URI;)Ljava/net/URI;
    :try_end_3
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    .line 222
    .end local v6    # "e":Ljava/net/URISyntaxException;
    .end local v10    # "serverAddr":Ljava/net/InetAddress;
    :catch_1
    move-exception v6

    .line 223
    .local v6, "e":Ljava/net/UnknownHostException;
    const-string v0, "WQE:ICD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t resolve DNS for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->hostName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v0, "WQE:ICD"

    const-string v1, "Interpreting as ICD_TIMEOUT"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v1, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_TIMEOUT:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v0, v1}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 226
    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_3

    .line 216
    .end local v6    # "e":Ljava/net/UnknownHostException;
    .restart local v10    # "serverAddr":Ljava/net/InetAddress;
    :cond_2
    const/4 v0, 0x2

    goto/16 :goto_1

    .line 219
    .end local v10    # "serverAddr":Ljava/net/InetAddress;
    :cond_3
    :try_start_4
    const-string v0, "WQE:ICD"

    const-string v1, "Couldn\'t get host name, continuing with null URI"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    const/4 v1, 0x0

    # setter for: Lcom/quicinc/cne/ICDClient;->httpURI:Ljava/net/URI;
    invoke-static {v0, v1}, Lcom/quicinc/cne/ICDClient;->access$402(Lcom/quicinc/cne/ICDClient;Ljava/net/URI;)Ljava/net/URI;
    :try_end_4
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_2

    .line 236
    :cond_4
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->httpURI:Ljava/net/URI;
    invoke-static {v0}, Lcom/quicinc/cne/ICDClient;->access$400(Lcom/quicinc/cne/ICDClient;)Ljava/net/URI;

    move-result-object v0

    if-nez v0, :cond_5

    .line 238
    const-string v0, "WQE:ICD"

    const-string v1, "Failure :httpURI=null"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    const-string v0, "WQE:ICD"

    const-string v1, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v1, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v0, v1}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 241
    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_3

    .line 243
    :cond_5
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->currentBSSID:Ljava/lang/String;
    invoke-static {v0}, Lcom/quicinc/cne/ICDClient;->access$800(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v1}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 245
    const-string v0, "WQE:ICD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failure :currentBSSID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->currentBSSID:Ljava/lang/String;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$800(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " passed BSSID="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssidPassed:Ljava/lang/String;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$900(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const-string v0, "WQE:ICD"

    const-string v1, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v1, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v0, v1}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 248
    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_3

    .line 251
    :cond_6
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 252
    .local v9, "qparams":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "bssid"

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->bssid:Ljava/lang/String;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$700(Lcom/quicinc/cne/ICDClient;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    :try_start_5
    iget-object v12, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->httpURI:Ljava/net/URI;
    invoke-static {v0}, Lcom/quicinc/cne/ICDClient;->access$400(Lcom/quicinc/cne/ICDClient;)Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->httpURI:Ljava/net/URI;
    invoke-static {v1}, Lcom/quicinc/cne/ICDClient;->access$400(Lcom/quicinc/cne/ICDClient;)Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->httpURI:Ljava/net/URI;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$400(Lcom/quicinc/cne/ICDClient;)Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URI;->getPort()I

    move-result v2

    iget-object v3, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->httpURI:Ljava/net/URI;
    invoke-static {v3}, Lcom/quicinc/cne/ICDClient;->access$400(Lcom/quicinc/cne/ICDClient;)Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {v9, v4}, Lorg/apache/http/client/utils/URLEncodedUtils;->format(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lorg/apache/http/client/utils/URIUtils;->createURI(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/net/URI;

    move-result-object v0

    # setter for: Lcom/quicinc/cne/ICDClient;->httpBssidURI:Ljava/net/URI;
    invoke-static {v12, v0}, Lcom/quicinc/cne/ICDClient;->access$1002(Lcom/quicinc/cne/ICDClient;Ljava/net/URI;)Ljava/net/URI;

    .line 258
    const-string v0, "WQE:ICD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "URI="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->httpBssidURI:Ljava/net/URI;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1000(Lcom/quicinc/cne/ICDClient;)Ljava/net/URI;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/net/URISyntaxException; {:try_start_5 .. :try_end_5} :catch_2

    .line 268
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpClient:Lorg/apache/http/client/HttpClient;
    invoke-static {v0}, Lcom/quicinc/cne/ICDClient;->access$1100(Lcom/quicinc/cne/ICDClient;)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v8

    .line 269
    .local v8, "httpParams":Lorg/apache/http/params/HttpParams;
    const-string v0, "WQE:ICD"

    const-string v1, "disabling http param redirect"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const-string v0, "http.protocol.handle-redirects"

    invoke-interface {v8, v0, v13}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    .line 271
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    new-instance v1, Lorg/apache/http/client/methods/HttpGet;

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->httpBssidURI:Ljava/net/URI;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$1000(Lcom/quicinc/cne/ICDClient;)Ljava/net/URI;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpGet:Lorg/apache/http/client/methods/HttpGet;
    invoke-static {v0, v1}, Lcom/quicinc/cne/ICDClient;->access$1202(Lcom/quicinc/cne/ICDClient;Lorg/apache/http/client/methods/HttpGet;)Lorg/apache/http/client/methods/HttpGet;

    .line 272
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpGet:Lorg/apache/http/client/methods/HttpGet;
    invoke-static {v0}, Lcom/quicinc/cne/ICDClient;->access$1200(Lcom/quicinc/cne/ICDClient;)Lorg/apache/http/client/methods/HttpGet;

    move-result-object v0

    const-string v1, "Accept-Encoding"

    const-string v2, "gzip;q=0,deflate;q=0"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpGet:Lorg/apache/http/client/methods/HttpGet;
    invoke-static {v0}, Lcom/quicinc/cne/ICDClient;->access$1200(Lcom/quicinc/cne/ICDClient;)Lorg/apache/http/client/methods/HttpGet;

    move-result-object v0

    const-string v1, "Cache-Control"

    const-string v2, "no-cache"

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpGet:Lorg/apache/http/client/methods/HttpGet;
    invoke-static {v0}, Lcom/quicinc/cne/ICDClient;->access$1200(Lcom/quicinc/cne/ICDClient;)Lorg/apache/http/client/methods/HttpGet;

    move-result-object v0

    const-string v1, "Host"

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->hostName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_3

    .line 260
    .end local v8    # "httpParams":Lorg/apache/http/params/HttpParams;
    :catch_2
    move-exception v7

    .line 262
    .local v7, "e1":Ljava/net/URISyntaxException;
    const-string v0, "WQE:ICD"

    const-string v1, "httpURI Syntax Exception"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    invoke-virtual {v7}, Ljava/net/URISyntaxException;->printStackTrace()V

    .line 264
    const-string v0, "WQE:ICD"

    const-string v1, "Interpreting as ICD_RESULT_PASS_NOT_STORE"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    sget-object v1, Lcom/quicinc/cne/ICDClient$IcdResult;->ICD_RESULT_PASS_NOT_STORE:Lcom/quicinc/cne/ICDClient$IcdResult;

    # setter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v0, v1}, Lcom/quicinc/cne/ICDClient;->access$602(Lcom/quicinc/cne/ICDClient;Lcom/quicinc/cne/ICDClient$IcdResult;)Lcom/quicinc/cne/ICDClient$IcdResult;

    .line 266
    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_3
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 538
    const-string v0, "WQE:ICD"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "icdHttp - start:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/quicinc/cne/ICDClient$icdHttp;->this$0:Lcom/quicinc/cne/ICDClient;

    # getter for: Lcom/quicinc/cne/ICDClient;->icdHttpReqResult:Lcom/quicinc/cne/ICDClient$IcdResult;
    invoke-static {v2}, Lcom/quicinc/cne/ICDClient;->access$600(Lcom/quicinc/cne/ICDClient;)Lcom/quicinc/cne/ICDClient$IcdResult;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    invoke-direct {p0}, Lcom/quicinc/cne/ICDClient$icdHttp;->setIcdHttpClientReq()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 541
    invoke-direct {p0}, Lcom/quicinc/cne/ICDClient$icdHttp;->sendIcdHttpClientReq()Ljava/lang/Boolean;

    .line 543
    :cond_0
    invoke-direct {p0}, Lcom/quicinc/cne/ICDClient$icdHttp;->parseIcdHttpClientRsp()V

    .line 545
    return-void
.end method
