.class public Lcom/quicinc/cne/BQEClient;
.super Ljava/lang/Object;
.source "BQEClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quicinc/cne/BQEClient$BqeResult;
    }
.end annotation


# static fields
.field private static final SUB_TYPE:Ljava/lang/String; = "WQE:BQE"

.field private static connMgr:Lorg/apache/http/conn/ClientConnectionManager;

.field private static inProgress:Ljava/lang/Boolean;


# instance fields
.field private bssid:Ljava/lang/String;

.field private bssidList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private bssidPassed:Ljava/lang/String;

.field private bssidURI:Ljava/net/URI;

.field private cneHandle:Lcom/quicinc/cne/CNE;

.field private currentBSSID:Ljava/lang/String;

.field private filesize:Ljava/lang/String;

.field private getRequestUrl:Ljava/lang/String;

.field private httpClient:Lorg/apache/http/client/HttpClient;

.field private httpGet:Lorg/apache/http/client/methods/HttpGet;

.field private httpPost:Lorg/apache/http/client/methods/HttpPost;

.field private httpRsp:Lorg/apache/http/HttpResponse;

.field private ipAddr:I

.field private postingThread:Ljava/lang/Boolean;

.field private result:Lcom/quicinc/cne/BQEClient$BqeResult;

.field private rspLock:Ljava/util/concurrent/locks/Lock;

.field private rtt:I

.field private rttEnd:J

.field private rttStart:J

.field private sentRsp:Ljava/lang/Boolean;

.field private serverInetAddr:Ljava/net/InetAddress;

.field private tMs:I

.field private tSec:I

.field private tput:I

.field private ts:I

.field private uri:Ljava/net/URI;

.field private wifiMgr:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Lcom/quicinc/cne/CNE;Landroid/net/wifi/WifiManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "handle"    # Lcom/quicinc/cne/CNE;
    .param p2, "wifi"    # Landroid/net/wifi/WifiManager;
    .param p3, "uri"    # Ljava/lang/String;
    .param p4, "bssidPassed"    # Ljava/lang/String;
    .param p5, "filesize"    # Ljava/lang/String;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->rspLock:Ljava/util/concurrent/locks/Lock;

    .line 103
    iput-object v3, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 104
    iput-object v3, p0, Lcom/quicinc/cne/BQEClient;->getRequestUrl:Ljava/lang/String;

    .line 105
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->sentRsp:Ljava/lang/Boolean;

    .line 106
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/quicinc/cne/BQEClient;->inProgress:Ljava/lang/Boolean;

    .line 107
    iput v2, p0, Lcom/quicinc/cne/BQEClient;->rtt:I

    .line 108
    iput-wide v4, p0, Lcom/quicinc/cne/BQEClient;->rttStart:J

    .line 109
    iput-wide v4, p0, Lcom/quicinc/cne/BQEClient;->rttEnd:J

    .line 110
    iput v2, p0, Lcom/quicinc/cne/BQEClient;->tSec:I

    .line 111
    iput v2, p0, Lcom/quicinc/cne/BQEClient;->tMs:I

    .line 112
    iput-object p2, p0, Lcom/quicinc/cne/BQEClient;->wifiMgr:Landroid/net/wifi/WifiManager;

    .line 113
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->postingThread:Ljava/lang/Boolean;

    .line 116
    :try_start_0
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, p3}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->uri:Ljava/net/URI;

    .line 117
    iput-object p4, p0, Lcom/quicinc/cne/BQEClient;->bssidPassed:Ljava/lang/String;

    .line 118
    iput-object p5, p0, Lcom/quicinc/cne/BQEClient;->filesize:Ljava/lang/String;

    .line 119
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->setBssid()V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 132
    :goto_0
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 133
    iget-object v1, p0, Lcom/quicinc/cne/BQEClient;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v1}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    sput-object v1, Lcom/quicinc/cne/BQEClient;->connMgr:Lorg/apache/http/conn/ClientConnectionManager;

    .line 134
    iput-object p1, p0, Lcom/quicinc/cne/BQEClient;->cneHandle:Lcom/quicinc/cne/CNE;

    .line 135
    const-string v1, "WQE:BQE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BQEClient() constructor created with GET URI ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and bssidPassed="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    return-void

    .line 121
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Ljava/net/URISyntaxException;
    invoke-virtual {v0}, Ljava/net/URISyntaxException;->printStackTrace()V

    .line 124
    const-string v1, "WQE:BQE"

    const-string v2, "Received Invalid URI , continuing with null URI"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iput-object v3, p0, Lcom/quicinc/cne/BQEClient;->uri:Ljava/net/URI;

    goto :goto_0

    .line 127
    .end local v0    # "e":Ljava/net/URISyntaxException;
    :catch_1
    move-exception v0

    .line 129
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 130
    const-string v1, "WQE:BQE"

    const-string v2, "Received an Exception while creating BQEClient.Continuing"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Lcom/quicinc/cne/CNE;Landroid/net/wifi/WifiManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "handle"    # Lcom/quicinc/cne/CNE;
    .param p2, "wifi"    # Landroid/net/wifi/WifiManager;
    .param p3, "uri"    # Ljava/lang/String;
    .param p4, "getRequestUrl"    # Ljava/lang/String;
    .param p5, "bssidPassed"    # Ljava/lang/String;
    .param p6, "tput"    # Ljava/lang/String;
    .param p7, "ts"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->rspLock:Ljava/util/concurrent/locks/Lock;

    .line 141
    iput-object p4, p0, Lcom/quicinc/cne/BQEClient;->getRequestUrl:Ljava/lang/String;

    .line 142
    iput-object v3, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 143
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->sentRsp:Ljava/lang/Boolean;

    .line 144
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/quicinc/cne/BQEClient;->ts:I

    .line 145
    invoke-static {p6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/quicinc/cne/BQEClient;->tput:I

    .line 146
    iput-object p2, p0, Lcom/quicinc/cne/BQEClient;->wifiMgr:Landroid/net/wifi/WifiManager;

    .line 147
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->postingThread:Ljava/lang/Boolean;

    .line 150
    :try_start_0
    new-instance v1, Ljava/net/URI;

    invoke-direct {v1, p3}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->uri:Ljava/net/URI;

    .line 151
    iput-object p5, p0, Lcom/quicinc/cne/BQEClient;->bssidPassed:Ljava/lang/String;

    .line 152
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->setBssid()V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 165
    :goto_0
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->httpClient:Lorg/apache/http/client/HttpClient;

    .line 166
    iget-object v1, p0, Lcom/quicinc/cne/BQEClient;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v1}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    sput-object v1, Lcom/quicinc/cne/BQEClient;->connMgr:Lorg/apache/http/conn/ClientConnectionManager;

    .line 167
    iput-object p1, p0, Lcom/quicinc/cne/BQEClient;->cneHandle:Lcom/quicinc/cne/CNE;

    .line 168
    const-string v1, "WQE:BQE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BQEClient() constructor created with POST URI ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " and bssidPassed="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    return-void

    .line 154
    :catch_0
    move-exception v0

    .line 156
    .local v0, "e":Ljava/net/URISyntaxException;
    invoke-virtual {v0}, Ljava/net/URISyntaxException;->printStackTrace()V

    .line 157
    const-string v1, "WQE:BQE"

    const-string v2, "Received Invalid URI , continuing with null URI"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iput-object v3, p0, Lcom/quicinc/cne/BQEClient;->uri:Ljava/net/URI;

    goto :goto_0

    .line 160
    .end local v0    # "e":Ljava/net/URISyntaxException;
    :catch_1
    move-exception v0

    .line 162
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 163
    const-string v1, "WQE:BQE"

    const-string v2, "Received an Exception while creating BQEClient.Continuing"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private consumeBQERspData()V
    .locals 6

    .prologue
    .line 463
    const-string v3, "WQE:BQE"

    const-string v4, "consumeBQERspData()"

    invoke-static {v3, v4}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 464
    const/4 v2, 0x0

    .line 465
    .local v2, "jsonString":Ljava/lang/String;
    iget-object v3, p0, Lcom/quicinc/cne/BQEClient;->httpRsp:Lorg/apache/http/HttpResponse;

    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 468
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    :try_start_0
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 474
    :goto_0
    const-string v3, "WQE:BQE"

    const-string v4, "Done Consuming the BQE data"

    invoke-static {v3, v4}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    sput-object v3, Lcom/quicinc/cne/BQEClient;->inProgress:Ljava/lang/Boolean;

    .line 476
    invoke-static {}, Lcom/quicinc/cne/BQEClient;->stop()V

    .line 477
    return-void

    .line 470
    :catch_0
    move-exception v0

    .line 472
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "WQE:BQE"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Got IO exception while Consuming the BQE data :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private parseBQEClientRsp()V
    .locals 5

    .prologue
    .line 484
    const-string v2, "WQE:BQE"

    const-string v3, "parseBQEClientRsp()"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    iget-object v2, p0, Lcom/quicinc/cne/BQEClient;->rspLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 486
    const-string v2, "WQE:BQE"

    const-string v3, "parseBQEClientRsp() Locked"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    iget-object v2, p0, Lcom/quicinc/cne/BQEClient;->sentRsp:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 489
    const-string v2, "WQE:BQE"

    const-string v3, "Response sent already, doing nothing"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    :goto_0
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/quicinc/cne/BQEClient;->sentRsp:Ljava/lang/Boolean;

    .line 579
    iget-object v2, p0, Lcom/quicinc/cne/BQEClient;->rspLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 580
    const-string v2, "WQE:BQE"

    const-string v3, "parseBQEClientRsp() UnLocked"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    return-void

    .line 493
    :cond_0
    iget-object v2, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    if-eqz v2, :cond_1

    .line 495
    const-string v2, "WQE:BQE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BQERequest failure.Reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    invoke-virtual {v4}, Lcom/quicinc/cne/BQEClient$BqeResult;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->sendBQEResponse()V

    goto :goto_0

    .line 500
    :cond_1
    iget-object v2, p0, Lcom/quicinc/cne/BQEClient;->httpRsp:Lorg/apache/http/HttpResponse;

    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    .line 501
    .local v1, "statusCode":I
    iget-object v2, p0, Lcom/quicinc/cne/BQEClient;->httpRsp:Lorg/apache/http/HttpResponse;

    invoke-interface {v2}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v0

    .line 502
    .local v0, "reasonPhrase":Ljava/lang/String;
    const-string v2, "WQE:BQE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BQEResponse http Status code: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    const/16 v2, 0xc8

    if-ne v1, v2, :cond_2

    .line 505
    const-string v2, "WQE:BQE"

    const-string v3, "BQERequest successful"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_CONTINUE:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v2, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 507
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->sendBQEResponse()V

    .line 508
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->consumeBQERspData()V

    goto/16 :goto_0

    .line 510
    :cond_2
    const/16 v2, 0x12e

    if-ne v1, v2, :cond_3

    .line 512
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_FAILURE:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v2, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 513
    const-string v2, "WQE:BQE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BQERequest failure.Reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    invoke-virtual {v4}, Lcom/quicinc/cne/BQEClient$BqeResult;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->sendBQEResponse()V

    goto/16 :goto_0

    .line 516
    :cond_3
    const/16 v2, 0x190

    if-ne v1, v2, :cond_4

    .line 518
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_SUCCESS:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v2, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 519
    const-string v2, "WQE:BQE"

    const-string v3, "Assuming BQERequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->sendBQEResponse()V

    goto/16 :goto_0

    .line 522
    :cond_4
    const/16 v2, 0x194

    if-ne v1, v2, :cond_5

    .line 524
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_SUCCESS:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v2, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 525
    const-string v2, "WQE:BQE"

    const-string v3, "Assuming BQERequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->sendBQEResponse()V

    goto/16 :goto_0

    .line 528
    :cond_5
    const/16 v2, 0x195

    if-ne v1, v2, :cond_6

    .line 530
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_SUCCESS:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v2, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 531
    const-string v2, "WQE:BQE"

    const-string v3, "Assuming BQERequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->sendBQEResponse()V

    goto/16 :goto_0

    .line 534
    :cond_6
    const/16 v2, 0x196

    if-ne v1, v2, :cond_7

    .line 536
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_SUCCESS:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v2, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 537
    const-string v2, "WQE:BQE"

    const-string v3, "Assuming BQERequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->sendBQEResponse()V

    goto/16 :goto_0

    .line 540
    :cond_7
    const/16 v2, 0x1f4

    if-ne v1, v2, :cond_8

    .line 542
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_SUCCESS:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v2, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 543
    const-string v2, "WQE:BQE"

    const-string v3, "Assuming BQERequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->sendBQEResponse()V

    goto/16 :goto_0

    .line 546
    :cond_8
    const/16 v2, 0x1f5

    if-ne v1, v2, :cond_9

    .line 548
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_SUCCESS:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v2, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 549
    const-string v2, "WQE:BQE"

    const-string v3, "Assuming BQERequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 550
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->sendBQEResponse()V

    goto/16 :goto_0

    .line 552
    :cond_9
    const/16 v2, 0x1f7

    if-ne v1, v2, :cond_a

    .line 554
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_SUCCESS:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v2, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 555
    const-string v2, "WQE:BQE"

    const-string v3, "Assuming BQERequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->sendBQEResponse()V

    goto/16 :goto_0

    .line 558
    :cond_a
    const/16 v2, 0x1f9

    if-ne v1, v2, :cond_b

    .line 560
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_SUCCESS:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v2, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 561
    const-string v2, "WQE:BQE"

    const-string v3, "Assuming BQERequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->sendBQEResponse()V

    goto/16 :goto_0

    .line 564
    :cond_b
    const/16 v2, 0x64

    if-ge v1, v2, :cond_c

    if-ltz v1, :cond_c

    .line 566
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_SUCCESS:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v2, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 567
    const-string v2, "WQE:BQE"

    const-string v3, "Assuming BQERequest successful, as Origin servers sent error"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->sendBQEResponse()V

    goto/16 :goto_0

    .line 572
    :cond_c
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_FAILURE:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v2, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 573
    const-string v2, "WQE:BQE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BQERequest failure.Reason: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    invoke-virtual {v4}, Lcom/quicinc/cne/BQEClient$BqeResult;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->sendBQEResponse()V

    goto/16 :goto_0
.end method

.method private sendBQEClientReq()Ljava/lang/Boolean;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 413
    const-string v1, "WQE:BQE"

    const-string v2, "sendBQEClientReq()"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    :try_start_0
    iget-object v1, p0, Lcom/quicinc/cne/BQEClient;->postingThread:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 418
    iget-object v1, p0, Lcom/quicinc/cne/BQEClient;->httpClient:Lorg/apache/http/client/HttpClient;

    iget-object v2, p0, Lcom/quicinc/cne/BQEClient;->httpPost:Lorg/apache/http/client/methods/HttpPost;

    invoke-interface {v1, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->httpRsp:Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 459
    :goto_0
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :goto_1
    return-object v1

    .line 423
    :cond_0
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/quicinc/cne/BQEClient;->rttStart:J

    .line 424
    iget-object v1, p0, Lcom/quicinc/cne/BQEClient;->httpClient:Lorg/apache/http/client/HttpClient;

    iget-object v2, p0, Lcom/quicinc/cne/BQEClient;->httpGet:Lorg/apache/http/client/methods/HttpGet;

    invoke-interface {v1, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->httpRsp:Lorg/apache/http/HttpResponse;

    .line 425
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/quicinc/cne/BQEClient;->rttEnd:J

    .line 426
    new-instance v1, Ljava/lang/Long;

    iget-wide v2, p0, Lcom/quicinc/cne/BQEClient;->rttEnd:J

    iget-wide v4, p0, Lcom/quicinc/cne/BQEClient;->rttStart:J

    sub-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v1}, Ljava/lang/Long;->intValue()I

    move-result v1

    iput v1, p0, Lcom/quicinc/cne/BQEClient;->rtt:I

    .line 427
    new-instance v1, Ljava/lang/Long;

    iget-wide v2, p0, Lcom/quicinc/cne/BQEClient;->rttEnd:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v1}, Ljava/lang/Long;->intValue()I

    move-result v1

    iput v1, p0, Lcom/quicinc/cne/BQEClient;->tSec:I

    .line 428
    new-instance v1, Ljava/lang/Long;

    iget-wide v2, p0, Lcom/quicinc/cne/BQEClient;->rttEnd:J

    const-wide/16 v4, 0x3e8

    rem-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v1}, Ljava/lang/Long;->intValue()I

    move-result v1

    iput v1, p0, Lcom/quicinc/cne/BQEClient;->tMs:I
    :try_end_1
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 431
    :catch_0
    move-exception v0

    .line 433
    .local v0, "e":Lorg/apache/http/client/ClientProtocolException;
    const-string v1, "WQE:BQE"

    const-string v2, "Client protocol Exception Exception"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    .line 435
    sget-object v1, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_FAILURE:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 436
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_1

    .line 438
    .end local v0    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_1
    move-exception v0

    .line 440
    .local v0, "e":Ljava/net/UnknownHostException;
    const-string v1, "WQE:BQE"

    const-string v2, "UnknownHost Exception"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    invoke-virtual {v0}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 442
    sget-object v1, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_FAILURE:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 443
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_1

    .line 445
    .end local v0    # "e":Ljava/net/UnknownHostException;
    :catch_2
    move-exception v0

    .line 447
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "WQE:BQE"

    const-string v2, "IO Exception"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 448
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 449
    sget-object v1, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_FAILURE:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 450
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_1

    .line 452
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 454
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "WQE:BQE"

    const-string v2, " Server Exception"

    invoke-static {v1, v2}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 456
    sget-object v1, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_FAILURE:Lcom/quicinc/cne/BQEClient$BqeResult;

    iput-object v1, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 457
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto/16 :goto_1
.end method

.method private sendBQEResponse()V
    .locals 5

    .prologue
    .line 480
    iget-object v0, p0, Lcom/quicinc/cne/BQEClient;->postingThread:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/quicinc/cne/BQEClient;->cneHandle:Lcom/quicinc/cne/CNE;

    iget-object v1, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    invoke-virtual {v1}, Lcom/quicinc/cne/BQEClient$BqeResult;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/quicinc/cne/CNE;->sendBQEResponse(I)V

    .line 482
    :goto_0
    return-void

    .line 481
    :cond_0
    iget-object v0, p0, Lcom/quicinc/cne/BQEClient;->cneHandle:Lcom/quicinc/cne/CNE;

    iget-object v1, p0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    invoke-virtual {v1}, Lcom/quicinc/cne/BQEClient$BqeResult;->ordinal()I

    move-result v1

    iget v2, p0, Lcom/quicinc/cne/BQEClient;->rtt:I

    iget v3, p0, Lcom/quicinc/cne/BQEClient;->tSec:I

    iget v4, p0, Lcom/quicinc/cne/BQEClient;->tMs:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/quicinc/cne/CNE;->sendBQEResponse(IIII)V

    goto :goto_0
.end method

.method private setBQEClientReq()Ljava/lang/Boolean;
    .locals 31

    .prologue
    .line 207
    const-string v2, "WQE:BQE"

    const-string v3, "setBQEClientReq()"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->bssid:Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->currentBSSID:Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->bssidPassed:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 211
    :cond_0
    const-string v2, "WQE:BQE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failure :Bssid="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/quicinc/cne/BQEClient;->bssid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " currentBSSID="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/quicinc/cne/BQEClient;->currentBSSID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " passed BSSID="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/quicinc/cne/BQEClient;->bssidPassed:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_FAILURE:Lcom/quicinc/cne/BQEClient$BqeResult;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 213
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 409
    :goto_0
    return-object v2

    .line 215
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->uri:Ljava/net/URI;

    if-nez v2, :cond_2

    .line 217
    const-string v2, "WQE:BQE"

    const-string v3, "Failure :uri==NULL"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_FAILURE:Lcom/quicinc/cne/BQEClient$BqeResult;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 219
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 221
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->currentBSSID:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quicinc/cne/BQEClient;->bssidPassed:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 223
    const-string v2, "WQE:BQE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failure :currentBSSID="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/quicinc/cne/BQEClient;->currentBSSID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " passed BSSID="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/quicinc/cne/BQEClient;->bssidPassed:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_FAILURE:Lcom/quicinc/cne/BQEClient$BqeResult;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 225
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 227
    :cond_3
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 228
    .local v23, "qparams":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->postingThread:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_8

    .line 230
    const-string v2, "WQE:BQE"

    const-string v3, "Set non-Posting Request"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "bssid"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/quicinc/cne/BQEClient;->bssid:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "size"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/quicinc/cne/BQEClient;->filesize:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    const/16 v20, 0x0

    .line 234
    .local v20, "numBssidAdded":I
    new-instance v28, Ljava/lang/StringBuilder;

    const/16 v2, 0x80

    move-object/from16 v0, v28

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 235
    .local v28, "tmp1":Ljava/lang/StringBuilder;
    const/16 v17, 0x0

    .line 236
    .local v17, "len":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->bssidList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    .line 237
    .local v27, "str":Ljava/lang/String;
    const-string v2, "WQE:BQE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "@@@currentbssid:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/quicinc/cne/BQEClient;->bssid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",nbssid:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->bssid:Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 242
    const/4 v2, 0x1

    move/from16 v0, v20

    if-lt v0, v2, :cond_6

    const/4 v2, 0x4

    move/from16 v0, v20

    if-ge v0, v2, :cond_6

    .line 243
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    move-result v2

    add-int v17, v17, v2

    .line 244
    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    const/4 v2, 0x3

    move/from16 v0, v20

    if-eq v0, v2, :cond_5

    .line 246
    const-string v2, ","

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 247
    add-int/lit8 v17, v17, 0x1

    .line 249
    :cond_5
    add-int/lit8 v20, v20, 0x1

    .line 252
    :cond_6
    if-nez v20, :cond_4

    .line 253
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "np"

    move-object/from16 v0, v27

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    add-int/lit8 v20, v20, 0x1

    goto :goto_1

    .line 258
    .end local v27    # "str":Ljava/lang/String;
    :cond_7
    const/4 v2, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v29

    .line 259
    .local v29, "tmp2":Ljava/lang/String;
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "xp"

    move-object/from16 v0, v29

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->uri:Ljava/net/URI;

    invoke-virtual {v2}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quicinc/cne/BQEClient;->uri:Ljava/net/URI;

    invoke-virtual {v3}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/quicinc/cne/BQEClient;->uri:Ljava/net/URI;

    invoke-virtual {v4}, Ljava/net/URI;->getPort()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/quicinc/cne/BQEClient;->uri:Ljava/net/URI;

    invoke-virtual {v5}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v5

    const-string v6, "UTF-8"

    move-object/from16 v0, v23

    invoke-static {v0, v6}, Lorg/apache/http/client/utils/URLEncodedUtils;->format(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lorg/apache/http/client/utils/URIUtils;->createURI(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/net/URI;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/quicinc/cne/BQEClient;->bssidURI:Ljava/net/URI;

    .line 265
    const-string v2, "WQE:BQE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "URI="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/quicinc/cne/BQEClient;->bssidURI:Ljava/net/URI;

    invoke-virtual {v4}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 275
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v17    # "len":I
    .end local v20    # "numBssidAdded":I
    .end local v28    # "tmp1":Ljava/lang/StringBuilder;
    .end local v29    # "tmp2":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->httpClient:Lorg/apache/http/client/HttpClient;

    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v13

    .line 284
    .local v13, "httpParams":Lorg/apache/http/params/HttpParams;
    const/16 v2, 0x10

    :try_start_1
    new-array v15, v2, [B

    .line 285
    .local v15, "ipAddrArray":[B
    const/4 v2, 0x0

    const/4 v3, 0x0

    aput-byte v3, v15, v2

    .line 286
    const/4 v2, 0x1

    const/4 v3, 0x0

    aput-byte v3, v15, v2

    .line 287
    const/4 v2, 0x2

    const/4 v3, 0x0

    aput-byte v3, v15, v2

    .line 288
    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-byte v3, v15, v2

    .line 289
    const/4 v2, 0x4

    const/4 v3, 0x0

    aput-byte v3, v15, v2

    .line 290
    const/4 v2, 0x5

    const/4 v3, 0x0

    aput-byte v3, v15, v2

    .line 291
    const/4 v2, 0x6

    const/4 v3, 0x0

    aput-byte v3, v15, v2

    .line 292
    const/4 v2, 0x7

    const/4 v3, 0x0

    aput-byte v3, v15, v2

    .line 293
    const/16 v2, 0x8

    const/4 v3, 0x0

    aput-byte v3, v15, v2

    .line 294
    const/16 v2, 0x9

    const/4 v3, 0x0

    aput-byte v3, v15, v2

    .line 295
    const/16 v2, 0xa

    const/4 v3, -0x1

    aput-byte v3, v15, v2

    .line 296
    const/16 v2, 0xb

    const/4 v3, -0x1

    aput-byte v3, v15, v2

    .line 297
    const/16 v2, 0xc

    move-object/from16 v0, p0

    iget v3, v0, Lcom/quicinc/cne/BQEClient;->ipAddr:I

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v15, v2

    .line 298
    const/16 v2, 0xd

    move-object/from16 v0, p0

    iget v3, v0, Lcom/quicinc/cne/BQEClient;->ipAddr:I

    shr-int/lit8 v3, v3, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v15, v2

    .line 299
    const/16 v2, 0xe

    move-object/from16 v0, p0

    iget v3, v0, Lcom/quicinc/cne/BQEClient;->ipAddr:I

    shr-int/lit8 v3, v3, 0x10

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v15, v2

    .line 300
    const/16 v2, 0xf

    move-object/from16 v0, p0

    iget v3, v0, Lcom/quicinc/cne/BQEClient;->ipAddr:I

    shr-int/lit8 v3, v3, 0x18

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v15, v2

    .line 303
    const/4 v2, 0x4

    new-array v8, v2, [B

    .line 304
    .local v8, "addressBytes":[B
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/quicinc/cne/BQEClient;->ipAddr:I

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v8, v2

    .line 305
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/quicinc/cne/BQEClient;->ipAddr:I

    shr-int/lit8 v3, v3, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v8, v2

    .line 306
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/quicinc/cne/BQEClient;->ipAddr:I

    shr-int/lit8 v3, v3, 0x10

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v8, v2

    .line 307
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/quicinc/cne/BQEClient;->ipAddr:I

    shr-int/lit8 v3, v3, 0x18

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v8, v2
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_2

    .line 308
    const/16 v19, 0x0

    .line 310
    .local v19, "nif":Ljava/net/NetworkInterface;
    :try_start_2
    invoke-static {v8}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v2

    invoke-static {v2}, Ljava/net/NetworkInterface;->getByInetAddress(Ljava/net/InetAddress;)Ljava/net/NetworkInterface;
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v19

    .line 317
    :goto_2
    if-eqz v19, :cond_9

    .line 318
    :try_start_3
    const-string v2, "WQE:BQE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "nif:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v19 .. v19}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    :cond_9
    const-string v2, "localhost"

    move-object/from16 v0, v19

    invoke-static {v2, v15, v0}, Ljava/net/Inet6Address;->getByAddress(Ljava/lang/String;[BLjava/net/NetworkInterface;)Ljava/net/Inet6Address;

    move-result-object v16

    .line 322
    .local v16, "ipAddrObj":Ljava/net/Inet6Address;
    const-string v2, "WQE:BQE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Source address "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v16 .. v16}, Ljava/net/Inet6Address;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    const-string v2, "http.route.local-address"

    move-object/from16 v0, v16

    invoke-interface {v13, v2, v0}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 325
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->uri:Ljava/net/URI;

    invoke-virtual {v2}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/quicinc/cne/BQEClient;->serverInetAddr:Ljava/net/InetAddress;
    :try_end_3
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_2

    .line 335
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->postingThread:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 338
    const-string v2, "WQE:BQE"

    const-string v3, "Set Posting Request"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const/16 v21, 0x0

    .line 340
    .local v21, "params":Lorg/apache/http/entity/StringEntity;
    const/4 v11, 0x0

    .line 342
    .local v11, "getServerIP":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->getRequestUrl:Ljava/lang/String;

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->getRequestUrl:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 344
    :try_start_4
    new-instance v30, Ljava/net/URI;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->getRequestUrl:Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-direct {v0, v2}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 345
    .local v30, "uriGetReq":Ljava/net/URI;
    invoke-virtual/range {v30 .. v30}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v12

    .line 346
    .local v12, "getServerInetAddr":Ljava/net/InetAddress;
    invoke-virtual {v12}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
    :try_end_4
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/net/URISyntaxException; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v11

    .line 360
    .end local v12    # "getServerInetAddr":Ljava/net/InetAddress;
    .end local v30    # "uriGetReq":Ljava/net/URI;
    :cond_a
    :goto_3
    :try_start_5
    new-instance v26, Lorg/json/JSONObject;

    invoke-direct/range {v26 .. v26}, Lorg/json/JSONObject;-><init>()V

    .line 361
    .local v26, "rspitems":Lorg/json/JSONObject;
    const-string v2, "bssid"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quicinc/cne/BQEClient;->bssid:Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 362
    const-string v2, "tput"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/quicinc/cne/BQEClient;->tput:I

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 363
    const-string v2, "ts"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/quicinc/cne/BQEClient;->ts:I

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 364
    if-eqz v11, :cond_b

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    .line 365
    const-string v2, "edgeIP"

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 368
    :cond_b
    new-instance v18, Lorg/json/JSONArray;

    invoke-direct/range {v18 .. v18}, Lorg/json/JSONArray;-><init>()V

    .line 369
    .local v18, "ngbhs":Lorg/json/JSONArray;
    const/16 v20, 0x1

    .line 370
    .restart local v20    # "numBssidAdded":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->bssidList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .restart local v14    # "i$":Ljava/util/Iterator;
    :cond_c
    :goto_4
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Ljava/lang/String;

    .line 371
    .restart local v27    # "str":Ljava/lang/String;
    const-string v2, "WQE:BQE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PostFindings-currentbssid:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/quicinc/cne/BQEClient;->bssid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",nbssid:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->bssid:Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 376
    const/4 v2, 0x1

    move/from16 v0, v20

    if-lt v0, v2, :cond_c

    const/4 v2, 0x4

    move/from16 v0, v20

    if-ge v0, v2, :cond_c

    .line 377
    move-object/from16 v0, v18

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_5
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5 .. :try_end_5} :catch_7
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_5

    .line 378
    add-int/lit8 v20, v20, 0x1

    goto :goto_4

    .line 267
    .end local v8    # "addressBytes":[B
    .end local v11    # "getServerIP":Ljava/lang/String;
    .end local v13    # "httpParams":Lorg/apache/http/params/HttpParams;
    .end local v15    # "ipAddrArray":[B
    .end local v16    # "ipAddrObj":Ljava/net/Inet6Address;
    .end local v18    # "ngbhs":Lorg/json/JSONArray;
    .end local v19    # "nif":Ljava/net/NetworkInterface;
    .end local v21    # "params":Lorg/apache/http/entity/StringEntity;
    .end local v26    # "rspitems":Lorg/json/JSONObject;
    .end local v27    # "str":Ljava/lang/String;
    .restart local v17    # "len":I
    .restart local v28    # "tmp1":Ljava/lang/StringBuilder;
    .restart local v29    # "tmp2":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 269
    .local v10, "e1":Ljava/net/URISyntaxException;
    const-string v2, "WQE:BQE"

    const-string v3, "URI Syntax Exception"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    invoke-virtual {v10}, Ljava/net/URISyntaxException;->printStackTrace()V

    .line 271
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_FAILURE:Lcom/quicinc/cne/BQEClient$BqeResult;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 272
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 312
    .end local v10    # "e1":Ljava/net/URISyntaxException;
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v17    # "len":I
    .end local v20    # "numBssidAdded":I
    .end local v28    # "tmp1":Ljava/lang/StringBuilder;
    .end local v29    # "tmp2":Ljava/lang/String;
    .restart local v8    # "addressBytes":[B
    .restart local v13    # "httpParams":Lorg/apache/http/params/HttpParams;
    .restart local v15    # "ipAddrArray":[B
    .restart local v19    # "nif":Ljava/net/NetworkInterface;
    :catch_1
    move-exception v9

    .line 313
    .local v9, "e":Ljava/net/SocketException;
    :try_start_6
    const-string v2, "WQE:BQE"

    const-string v3, "Unknown Interface Exception"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    invoke-virtual {v9}, Ljava/net/SocketException;->printStackTrace()V
    :try_end_6
    .catch Ljava/net/UnknownHostException; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_2

    .line 327
    .end local v8    # "addressBytes":[B
    .end local v9    # "e":Ljava/net/SocketException;
    .end local v15    # "ipAddrArray":[B
    .end local v19    # "nif":Ljava/net/NetworkInterface;
    :catch_2
    move-exception v9

    .line 329
    .local v9, "e":Ljava/net/UnknownHostException;
    const-string v2, "WQE:BQE"

    const-string v3, "Unknown Host Exception Exception"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    invoke-virtual {v9}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 331
    sget-object v2, Lcom/quicinc/cne/BQEClient$BqeResult;->BQE_RESULT_FAILURE:Lcom/quicinc/cne/BQEClient$BqeResult;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/quicinc/cne/BQEClient;->result:Lcom/quicinc/cne/BQEClient$BqeResult;

    .line 332
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 347
    .end local v9    # "e":Ljava/net/UnknownHostException;
    .restart local v8    # "addressBytes":[B
    .restart local v11    # "getServerIP":Ljava/lang/String;
    .restart local v15    # "ipAddrArray":[B
    .restart local v16    # "ipAddrObj":Ljava/net/Inet6Address;
    .restart local v19    # "nif":Ljava/net/NetworkInterface;
    .restart local v21    # "params":Lorg/apache/http/entity/StringEntity;
    :catch_3
    move-exception v9

    .line 348
    .restart local v9    # "e":Ljava/net/UnknownHostException;
    const-string v2, "WQE:BQE"

    const-string v3, "Unknown Host Exception Exception"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    invoke-virtual {v9}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 350
    const/4 v11, 0x0

    .line 355
    goto/16 :goto_3

    .line 351
    .end local v9    # "e":Ljava/net/UnknownHostException;
    :catch_4
    move-exception v9

    .line 352
    .local v9, "e":Ljava/net/URISyntaxException;
    const-string v2, "WQE:BQE"

    const-string v3, "URI Syntax Exception Exception"

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    invoke-virtual {v9}, Ljava/net/URISyntaxException;->printStackTrace()V

    .line 354
    const/4 v11, 0x0

    goto/16 :goto_3

    .line 381
    .end local v9    # "e":Ljava/net/URISyntaxException;
    .restart local v14    # "i$":Ljava/util/Iterator;
    .restart local v18    # "ngbhs":Lorg/json/JSONArray;
    .restart local v20    # "numBssidAdded":I
    .restart local v26    # "rspitems":Lorg/json/JSONObject;
    :cond_d
    :try_start_7
    const-string v2, "ngbh.bssids"

    move-object/from16 v0, v26

    move-object/from16 v1, v18

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 383
    new-instance v25, Lorg/json/JSONArray;

    invoke-direct/range {v25 .. v25}, Lorg/json/JSONArray;-><init>()V

    .line 384
    .local v25, "rsparray":Lorg/json/JSONArray;
    const/4 v2, 0x0

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;

    .line 387
    new-instance v24, Lorg/json/JSONObject;

    invoke-direct/range {v24 .. v24}, Lorg/json/JSONObject;-><init>()V

    .line 388
    .local v24, "rsp":Lorg/json/JSONObject;
    const-string v2, "acs"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 389
    new-instance v22, Lorg/apache/http/entity/StringEntity;

    invoke-virtual/range {v24 .. v24}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_7 .. :try_end_7} :catch_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_5

    .line 391
    .end local v21    # "params":Lorg/apache/http/entity/StringEntity;
    .local v22, "params":Lorg/apache/http/entity/StringEntity;
    :try_start_8
    const-string v2, "WQE:BQE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PostFinding="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {v24 .. v24}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_8 .. :try_end_8} :catch_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_6

    move-object/from16 v21, v22

    .line 399
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v18    # "ngbhs":Lorg/json/JSONArray;
    .end local v20    # "numBssidAdded":I
    .end local v22    # "params":Lorg/apache/http/entity/StringEntity;
    .end local v24    # "rsp":Lorg/json/JSONObject;
    .end local v25    # "rsparray":Lorg/json/JSONArray;
    .end local v26    # "rspitems":Lorg/json/JSONObject;
    .restart local v21    # "params":Lorg/apache/http/entity/StringEntity;
    :goto_5
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quicinc/cne/BQEClient;->uri:Ljava/net/URI;

    invoke-direct {v2, v3}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/net/URI;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/quicinc/cne/BQEClient;->httpPost:Lorg/apache/http/client/methods/HttpPost;

    .line 400
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->httpPost:Lorg/apache/http/client/methods/HttpPost;

    const-string v3, "content-type"

    const-string v4, "application/json"

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpPost;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->httpPost:Lorg/apache/http/client/methods/HttpPost;

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 409
    .end local v11    # "getServerIP":Ljava/lang/String;
    .end local v21    # "params":Lorg/apache/http/entity/StringEntity;
    :goto_6
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto/16 :goto_0

    .line 405
    :cond_e
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/quicinc/cne/BQEClient;->bssidURI:Ljava/net/URI;

    invoke-direct {v2, v3}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/net/URI;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/quicinc/cne/BQEClient;->httpGet:Lorg/apache/http/client/methods/HttpGet;

    .line 406
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->httpGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v3, "Accept-Encoding"

    const-string v4, "gzip;q=0,deflate;q=0"

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/quicinc/cne/BQEClient;->httpGet:Lorg/apache/http/client/methods/HttpGet;

    const-string v3, "Cache-Control"

    const-string v4, "no-cache"

    invoke-virtual {v2, v3, v4}, Lorg/apache/http/client/methods/HttpGet;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    .line 396
    .restart local v11    # "getServerIP":Ljava/lang/String;
    .restart local v21    # "params":Lorg/apache/http/entity/StringEntity;
    :catch_5
    move-exception v2

    goto :goto_5

    .end local v21    # "params":Lorg/apache/http/entity/StringEntity;
    .restart local v14    # "i$":Ljava/util/Iterator;
    .restart local v18    # "ngbhs":Lorg/json/JSONArray;
    .restart local v20    # "numBssidAdded":I
    .restart local v22    # "params":Lorg/apache/http/entity/StringEntity;
    .restart local v24    # "rsp":Lorg/json/JSONObject;
    .restart local v25    # "rsparray":Lorg/json/JSONArray;
    .restart local v26    # "rspitems":Lorg/json/JSONObject;
    :catch_6
    move-exception v2

    move-object/from16 v21, v22

    .end local v22    # "params":Lorg/apache/http/entity/StringEntity;
    .restart local v21    # "params":Lorg/apache/http/entity/StringEntity;
    goto :goto_5

    .line 393
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v18    # "ngbhs":Lorg/json/JSONArray;
    .end local v20    # "numBssidAdded":I
    .end local v24    # "rsp":Lorg/json/JSONObject;
    .end local v25    # "rsparray":Lorg/json/JSONArray;
    .end local v26    # "rspitems":Lorg/json/JSONObject;
    :catch_7
    move-exception v2

    goto :goto_5

    .end local v21    # "params":Lorg/apache/http/entity/StringEntity;
    .restart local v14    # "i$":Ljava/util/Iterator;
    .restart local v18    # "ngbhs":Lorg/json/JSONArray;
    .restart local v20    # "numBssidAdded":I
    .restart local v22    # "params":Lorg/apache/http/entity/StringEntity;
    .restart local v24    # "rsp":Lorg/json/JSONObject;
    .restart local v25    # "rsparray":Lorg/json/JSONArray;
    .restart local v26    # "rspitems":Lorg/json/JSONObject;
    :catch_8
    move-exception v2

    move-object/from16 v21, v22

    .end local v22    # "params":Lorg/apache/http/entity/StringEntity;
    .restart local v21    # "params":Lorg/apache/http/entity/StringEntity;
    goto :goto_5
.end method

.method private setBssid()V
    .locals 8

    .prologue
    .line 178
    const-string v4, "WQE:BQE"

    const-string v5, "setBssid()"

    invoke-static {v4, v5}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v4, p0, Lcom/quicinc/cne/BQEClient;->wifiMgr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    .line 180
    .local v2, "wifiInfo":Landroid/net/wifi/WifiInfo;
    if-eqz v2, :cond_1

    .line 182
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v4

    const-string v5, ":"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/quicinc/cne/BQEClient;->bssid:Ljava/lang/String;

    .line 183
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/quicinc/cne/BQEClient;->currentBSSID:Ljava/lang/String;

    .line 184
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v4

    iput v4, p0, Lcom/quicinc/cne/BQEClient;->ipAddr:I

    .line 185
    const-string v4, "WQE:BQE"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bssid="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/quicinc/cne/BQEClient;->bssid:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " currentBSSID="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/quicinc/cne/BQEClient;->currentBSSID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const-string v4, "WQE:BQE"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Passed BSSID="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/quicinc/cne/BQEClient;->bssidPassed:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and IPAddr ="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/lang/Integer;

    iget v7, p0, Lcom/quicinc/cne/BQEClient;->ipAddr:I

    invoke-direct {v6, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :goto_0
    iget-object v4, p0, Lcom/quicinc/cne/BQEClient;->wifiMgr:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v3

    .line 194
    .local v3, "wifiScanResults":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/quicinc/cne/BQEClient;->bssidList:Ljava/util/ArrayList;

    .line 196
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/ScanResult;

    .line 197
    .local v1, "result":Landroid/net/wifi/ScanResult;
    if-eqz v1, :cond_0

    iget-object v4, v1, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    if-nez v4, :cond_2

    .line 198
    :cond_0
    const-string v4, "WQE:BQE"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "@@@Received invalid scan result: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 190
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "result":Landroid/net/wifi/ScanResult;
    .end local v3    # "wifiScanResults":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :cond_1
    const-string v4, "WQE:BQE"

    const-string v5, "wifiMgr RemoteException returned NULL"

    invoke-static {v4, v5}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 201
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v1    # "result":Landroid/net/wifi/ScanResult;
    .restart local v3    # "wifiScanResults":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    :cond_2
    iget-object v4, p0, Lcom/quicinc/cne/BQEClient;->bssidList:Ljava/util/ArrayList;

    iget-object v5, v1, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    const-string v6, ":"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 203
    .end local v1    # "result":Landroid/net/wifi/ScanResult;
    :cond_3
    iget-object v4, p0, Lcom/quicinc/cne/BQEClient;->bssidList:Ljava/util/ArrayList;

    sget-object v5, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-static {v4, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 204
    return-void
.end method

.method public static stop()V
    .locals 2

    .prologue
    .line 172
    sget-object v0, Lcom/quicinc/cne/BQEClient;->inProgress:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/quicinc/cne/BQEClient;->connMgr:Lorg/apache/http/conn/ClientConnectionManager;

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->shutdown()V

    .line 174
    :goto_0
    const-string v0, "WQE:BQE"

    const-string v1, "BQE active probing is now stopped"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    return-void

    .line 173
    :cond_0
    const-string v0, "WQE:BQE"

    const-string v1, "BQE active probing is already stopped"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 586
    const-string v0, "WQE:BQE"

    const-string v1, "BQEClient thread started"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->setBQEClientReq()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 589
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->sendBQEClientReq()Ljava/lang/Boolean;

    .line 591
    :cond_0
    invoke-direct {p0}, Lcom/quicinc/cne/BQEClient;->parseBQEClientRsp()V

    .line 592
    return-void
.end method
