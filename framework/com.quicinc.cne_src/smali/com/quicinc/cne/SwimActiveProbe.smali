.class Lcom/quicinc/cne/SwimActiveProbe;
.super Landroid/os/Handler;
.source "SwimActiveProbe.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quicinc/cne/SwimActiveProbe$SwimActiveProbeMsgHdlr;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final SUB_TYPE:Ljava/lang/String; = "WQE"

.field private static final URI:Ljava/lang/String; = "http://www.qualcomm.com"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mSender:Lcom/quicinc/cne/CNE$CNESender;

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/quicinc/cne/CNE$CNESender;)V
    .locals 5
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "sender"    # Lcom/quicinc/cne/CNE$CNESender;

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/quicinc/cne/SwimActiveProbe;->mContext:Landroid/content/Context;

    .line 41
    iget-object v3, p0, Lcom/quicinc/cne/SwimActiveProbe;->mContext:Landroid/content/Context;

    const-string v4, "wifi"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    iput-object v3, p0, Lcom/quicinc/cne/SwimActiveProbe;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 42
    iput-object p2, p0, Lcom/quicinc/cne/SwimActiveProbe;->mSender:Lcom/quicinc/cne/CNE$CNESender;

    .line 43
    new-instance v0, Landroid/os/HandlerThread;

    const-string v3, "SwimActiveProbe"

    invoke-direct {v0, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 44
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 45
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    .line 46
    .local v1, "looper":Landroid/os/Looper;
    if-nez v1, :cond_0

    .line 48
    const-string v3, "WQE"

    const-string v4, "SwimActiveProbe could not get Looper"

    invoke-static {v3, v4}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    :cond_0
    new-instance v2, Lcom/quicinc/cne/SwimActiveProbe$SwimActiveProbeMsgHdlr;

    invoke-direct {v2, p0, v1}, Lcom/quicinc/cne/SwimActiveProbe$SwimActiveProbeMsgHdlr;-><init>(Lcom/quicinc/cne/SwimActiveProbe;Landroid/os/Looper;)V

    .line 53
    .local v2, "msghdlr":Lcom/quicinc/cne/SwimActiveProbe$SwimActiveProbeMsgHdlr;
    const/16 v3, 0xf

    invoke-static {v3, v2}, Lcom/quicinc/cne/CNE;->registerRequestHandler(ILandroid/os/Handler;)V

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/quicinc/cne/SwimActiveProbe;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/SwimActiveProbe;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/quicinc/cne/SwimActiveProbe;->doActiveProbing(Ljava/lang/String;)V

    return-void
.end method

.method private doActiveProbing(Ljava/lang/String;)V
    .locals 14
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 57
    const-string v9, "WQE"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "startingActiveProbe on thread: ["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Thread;->getId()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "] with uri "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    :try_start_0
    new-instance v3, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 62
    .local v3, "hc":Lorg/apache/http/client/HttpClient;
    invoke-interface {v3}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v4

    .line 64
    .local v4, "hp":Lorg/apache/http/params/HttpParams;
    iget-object v9, p0, Lcom/quicinc/cne/SwimActiveProbe;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-nez v9, :cond_0

    .line 66
    const-string v9, "WQE"

    const-string v10, "invalid pointer to mWifiManager"

    invoke-static {v9, v10}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    .end local v3    # "hc":Lorg/apache/http/client/HttpClient;
    .end local v4    # "hp":Lorg/apache/http/params/HttpParams;
    .end local p1    # "uri":Ljava/lang/String;
    :goto_0
    return-void

    .line 69
    .restart local v3    # "hc":Lorg/apache/http/client/HttpClient;
    .restart local v4    # "hp":Lorg/apache/http/params/HttpParams;
    .restart local p1    # "uri":Ljava/lang/String;
    :cond_0
    iget-object v9, p0, Lcom/quicinc/cne/SwimActiveProbe;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v9}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v9

    if-nez v9, :cond_1

    .line 71
    const-string v9, "WQE"

    const-string v10, "invalid connectionInfo"

    invoke-static {v9, v10}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 104
    .end local v3    # "hc":Lorg/apache/http/client/HttpClient;
    .end local v4    # "hp":Lorg/apache/http/params/HttpParams;
    .end local p1    # "uri":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Ljava/io/IOException;
    const-string v9, "WQE"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "sendActiveProbe GET exception"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/quicinc/cne/CneMsg;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 74
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v3    # "hc":Lorg/apache/http/client/HttpClient;
    .restart local v4    # "hp":Lorg/apache/http/params/HttpParams;
    .restart local p1    # "uri":Ljava/lang/String;
    :cond_1
    :try_start_1
    iget-object v9, p0, Lcom/quicinc/cne/SwimActiveProbe;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v9}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v5

    .line 75
    .local v5, "ii":I
    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v9, 0x10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 76
    .local v8, "sb":Ljava/lang/StringBuilder;
    and-int/lit16 v9, v5, 0xff

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 77
    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    shr-int/lit8 v9, v5, 0x8

    and-int/lit16 v9, v9, 0xff

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 79
    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    shr-int/lit8 v9, v5, 0x10

    and-int/lit16 v9, v9, 0xff

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 81
    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    shr-int/lit8 v9, v5, 0x18

    and-int/lit16 v9, v9, 0xff

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 83
    const-string v9, "http.route.local-address"

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v10

    invoke-interface {v4, v9, v10}, Lorg/apache/http/params/HttpParams;->setParameter(Ljava/lang/String;Ljava/lang/Object;)Lorg/apache/http/params/HttpParams;

    .line 87
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_2

    .end local p1    # "uri":Ljava/lang/String;
    :goto_1
    invoke-direct {v2, p1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 88
    .local v2, "get":Lorg/apache/http/client/methods/HttpGet;
    invoke-interface {v3, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v7

    .line 89
    .local v7, "rp":Lorg/apache/http/HttpResponse;
    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v9

    invoke-interface {v9}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v6

    .line 90
    .local v6, "rc":I
    const/16 v9, 0xc8

    if-ne v6, v9, :cond_3

    .line 92
    const-string v9, "WQE"

    const-string v10, "sendActiveProbe GET successful"

    invoke-static {v9, v10}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 95
    .local v1, "ent":Lorg/apache/http/HttpEntity;
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 96
    const-string v9, "WQE"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "sendActiveProbe finished on thread ["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Thread;->getId()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/quicinc/cne/CneMsg;->logd(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 87
    .end local v1    # "ent":Lorg/apache/http/HttpEntity;
    .end local v2    # "get":Lorg/apache/http/client/methods/HttpGet;
    .end local v6    # "rc":I
    .end local v7    # "rp":Lorg/apache/http/HttpResponse;
    .restart local p1    # "uri":Ljava/lang/String;
    :cond_2
    const-string p1, "http://www.qualcomm.com"

    goto :goto_1

    .line 101
    .end local p1    # "uri":Ljava/lang/String;
    .restart local v2    # "get":Lorg/apache/http/client/methods/HttpGet;
    .restart local v6    # "rc":I
    .restart local v7    # "rp":Lorg/apache/http/HttpResponse;
    :cond_3
    const-string v9, "WQE"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "sendActiveProbe GET failed: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private send(Lcom/quicinc/cne/CNERequest;)V
    .locals 3
    .param p1, "req"    # Lcom/quicinc/cne/CNERequest;

    .prologue
    .line 112
    iget-object v1, p0, Lcom/quicinc/cne/SwimActiveProbe;->mSender:Lcom/quicinc/cne/CNE$CNESender;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Lcom/quicinc/cne/CNE$CNESender;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 113
    .local v0, "msg":Landroid/os/Message;
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 114
    return-void
.end method
