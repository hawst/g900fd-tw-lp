.class Lcom/quicinc/cne/SwimActiveProbe$SwimActiveProbeMsgHdlr;
.super Landroid/os/Handler;
.source "SwimActiveProbe.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quicinc/cne/SwimActiveProbe;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SwimActiveProbeMsgHdlr"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/quicinc/cne/SwimActiveProbe;


# direct methods
.method constructor <init>(Lcom/quicinc/cne/SwimActiveProbe;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/quicinc/cne/SwimActiveProbe$SwimActiveProbeMsgHdlr;->this$0:Lcom/quicinc/cne/SwimActiveProbe;

    .line 120
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 121
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 124
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 133
    const-string v2, "WQE"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unhandled Message msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/quicinc/cne/CneMsg;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :goto_0
    return-void

    .line 127
    :pswitch_0
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/os/Parcel;

    move-object v0, v2

    check-cast v0, Landroid/os/Parcel;

    .line 128
    .local v0, "p":Landroid/os/Parcel;
    invoke-virtual {v0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 129
    .local v1, "uri":Ljava/lang/String;
    iget-object v2, p0, Lcom/quicinc/cne/SwimActiveProbe$SwimActiveProbeMsgHdlr;->this$0:Lcom/quicinc/cne/SwimActiveProbe;

    # invokes: Lcom/quicinc/cne/SwimActiveProbe;->doActiveProbing(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/quicinc/cne/SwimActiveProbe;->access$000(Lcom/quicinc/cne/SwimActiveProbe;Ljava/lang/String;)V

    goto :goto_0

    .line 124
    nop

    :pswitch_data_0
    .packed-switch 0xf
        :pswitch_0
    .end packed-switch
.end method
