.class Lcom/quicinc/cne/CNE$PackageListener;
.super Ljava/lang/Object;
.source "CNE.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quicinc/cne/CNE;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PackageListener"
.end annotation


# static fields
.field static final CNE_PKG_ACTION_ADD:I = 0x0

.field static final CNE_PKG_ACTION_REMOVE:I = 0x1


# instance fields
.field private dataPosNumOfApps:I

.field private isInitialized:Z

.field private mFilter:Landroid/content/IntentFilter;

.field mIntentReceiver:Landroid/content/BroadcastReceiver;

.field final synthetic this$0:Lcom/quicinc/cne/CNE;


# direct methods
.method constructor <init>(Lcom/quicinc/cne/CNE;)V
    .locals 2

    .prologue
    .line 2812
    iput-object p1, p0, Lcom/quicinc/cne/CNE$PackageListener;->this$0:Lcom/quicinc/cne/CNE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2798
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/quicinc/cne/CNE$PackageListener;->isInitialized:Z

    .line 2816
    new-instance v0, Lcom/quicinc/cne/CNE$PackageListener$1;

    invoke-direct {v0, p0}, Lcom/quicinc/cne/CNE$PackageListener$1;-><init>(Lcom/quicinc/cne/CNE$PackageListener;)V

    iput-object v0, p0, Lcom/quicinc/cne/CNE$PackageListener;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    .line 2813
    const-string v0, "CORE"

    const-string v1, "PackageListener constructor"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2814
    return-void
.end method

.method static synthetic access$4800(Lcom/quicinc/cne/CNE$PackageListener;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE$PackageListener;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 2795
    invoke-direct {p0, p1}, Lcom/quicinc/cne/CNE$PackageListener;->sendUpdatedPackageInfo(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$800(Lcom/quicinc/cne/CNE$PackageListener;)Z
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cne/CNE$PackageListener;

    .prologue
    .line 2795
    iget-boolean v0, p0, Lcom/quicinc/cne/CNE$PackageListener;->isInitialized:Z

    return v0
.end method

.method static synthetic access$900(Lcom/quicinc/cne/CNE$PackageListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/quicinc/cne/CNE$PackageListener;

    .prologue
    .line 2795
    invoke-direct {p0}, Lcom/quicinc/cne/CNE$PackageListener;->initialize()V

    return-void
.end method

.method private addPackageInfo(Lcom/quicinc/cne/CNERequest;Ljava/lang/String;ILjava/lang/String;)Z
    .locals 3
    .param p1, "rr"    # Lcom/quicinc/cne/CNERequest;
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "uid"    # I
    .param p4, "hashes"    # Ljava/lang/String;

    .prologue
    .line 2834
    if-nez p1, :cond_0

    .line 2835
    const-string v0, "CORE"

    const-string v1, "addPackageInfo: rr is Nullnot adding package info"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logw(Ljava/lang/String;Ljava/lang/String;)V

    .line 2838
    const/4 v0, 0x0

    .line 2858
    :goto_0
    return v0

    .line 2842
    :cond_0
    const-string v0, "CORE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addPackageInfo: appname:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " uid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "hashes:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2855
    iget-object v0, p1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2856
    iget-object v0, p1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2857
    iget-object v0, p1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2858
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private initRequest(Lcom/quicinc/cne/CNERequest;II)Z
    .locals 3
    .param p1, "rr"    # Lcom/quicinc/cne/CNERequest;
    .param p2, "action"    # I
    .param p3, "numOfApps"    # I

    .prologue
    .line 2862
    if-nez p1, :cond_0

    .line 2863
    const/4 v0, 0x0

    .line 2875
    :goto_0
    return v0

    .line 2870
    :cond_0
    iget-object v0, p1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2872
    iget-object v0, p1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    iput v0, p0, Lcom/quicinc/cne/CNE$PackageListener;->dataPosNumOfApps:I

    .line 2873
    iget-object v0, p1, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    .line 2874
    const-string v0, "CORE"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initRequest: numApps:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/quicinc/cne/CNE;->access$100(Ljava/lang/String;Ljava/lang/String;)V

    .line 2875
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private initialize()V
    .locals 3

    .prologue
    .line 2964
    const-string v0, "CORE"

    const-string v1, "PackageListener initialize"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2966
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/quicinc/cne/CNE$PackageListener;->mFilter:Landroid/content/IntentFilter;

    .line 2967
    iget-object v0, p0, Lcom/quicinc/cne/CNE$PackageListener;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2968
    iget-object v0, p0, Lcom/quicinc/cne/CNE$PackageListener;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2969
    iget-object v0, p0, Lcom/quicinc/cne/CNE$PackageListener;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 2970
    iget-object v0, p0, Lcom/quicinc/cne/CNE$PackageListener;->this$0:Lcom/quicinc/cne/CNE;

    # getter for: Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/quicinc/cne/CNE;->access$3400(Lcom/quicinc/cne/CNE;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/quicinc/cne/CNE$PackageListener;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/quicinc/cne/CNE$PackageListener;->mFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2972
    invoke-virtual {p0}, Lcom/quicinc/cne/CNE$PackageListener;->sendInstalledPackageInfo()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2973
    const-string v0, "CORE"

    const-string v1, "PackageListener: send installed package info to CND"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2979
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/quicinc/cne/CNE$PackageListener;->isInitialized:Z

    .line 2980
    return-void

    .line 2976
    :cond_0
    const-string v0, "CORE"

    const-string v1, "PackageListener: Failed to send installed package info to CND"

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private sendUpdatedPackageInfo(Landroid/content/Intent;)V
    .locals 20
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2983
    const-string v17, "CORE"

    const-string v18, "PackageListener sendUpdatedPackageInfo"

    invoke-static/range {v17 .. v18}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2984
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 2986
    .local v3, "action":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    .line 2987
    .local v8, "extras":Landroid/os/Bundle;
    if-nez v8, :cond_1

    .line 2988
    const-string v17, "CORE"

    const-string v18, "sendUpdatedPackageInfo: no extra info "

    invoke-static/range {v17 .. v18}, Lcom/quicinc/cne/CneMsg;->loge(Ljava/lang/String;Ljava/lang/String;)V

    .line 3054
    .end local v8    # "extras":Landroid/os/Bundle;
    :cond_0
    :goto_0
    return-void

    .line 2994
    .restart local v8    # "extras":Landroid/os/Bundle;
    :cond_1
    const-string v17, "android.intent.extra.UID"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v16

    .line 2995
    .local v16, "uid":I
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v5

    .line 2996
    .local v5, "appNameUri":Landroid/net/Uri;
    invoke-virtual {v5}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v4

    .line 2997
    .local v4, "appName":Ljava/lang/String;
    const/4 v10, 0x0

    .line 2999
    .local v10, "hashes":Ljava/lang/String;
    const-string v17, "CORE"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Intent sendUpdatedPackageInfo: uid:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "  appName:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 3003
    const/16 v17, 0x17

    invoke-static/range {v17 .. v17}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v13

    .line 3005
    .local v13, "rr":Lcom/quicinc/cne/CNERequest;
    const-string v17, "android.intent.action.PACKAGE_REMOVED"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 3006
    const/16 v17, 0x1

    const/16 v18, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v13, v1, v2}, Lcom/quicinc/cne/CNE$PackageListener;->initRequest(Lcom/quicinc/cne/CNERequest;II)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 3042
    :cond_2
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v13, v4, v1, v10}, Lcom/quicinc/cne/CNE$PackageListener;->addPackageInfo(Lcom/quicinc/cne/CNERequest;Ljava/lang/String;ILjava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_7

    .line 3043
    const-string v17, "CORE"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "sendUpdatedPackageInfo: appname:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " uid:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "hashes:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " failed"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 3049
    .end local v4    # "appName":Ljava/lang/String;
    .end local v5    # "appNameUri":Landroid/net/Uri;
    .end local v8    # "extras":Landroid/os/Bundle;
    .end local v10    # "hashes":Ljava/lang/String;
    .end local v13    # "rr":Lcom/quicinc/cne/CNERequest;
    .end local v16    # "uid":I
    :catch_0
    move-exception v7

    .line 3050
    .local v7, "ex":Ljava/lang/NullPointerException;
    const-string v17, "CORE"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Can\'t update package Exception: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v7}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/quicinc/cne/CneMsg;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3010
    .end local v7    # "ex":Ljava/lang/NullPointerException;
    .restart local v4    # "appName":Ljava/lang/String;
    .restart local v5    # "appNameUri":Landroid/net/Uri;
    .restart local v8    # "extras":Landroid/os/Bundle;
    .restart local v10    # "hashes":Ljava/lang/String;
    .restart local v13    # "rr":Lcom/quicinc/cne/CNERequest;
    .restart local v16    # "uid":I
    :cond_3
    :try_start_1
    const-string v17, "android.intent.action.PACKAGE_ADDED"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v17

    if-eqz v17, :cond_2

    .line 3012
    const/16 v17, 0x0

    const/16 v18, 0x1

    :try_start_2
    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v13, v1, v2}, Lcom/quicinc/cne/CNE$PackageListener;->initRequest(Lcom/quicinc/cne/CNERequest;II)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 3015
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$PackageListener;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v17, v0

    # getter for: Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;
    invoke-static/range {v17 .. v17}, Lcom/quicinc/cne/CNE;->access$3400(Lcom/quicinc/cne/CNE;)Landroid/content/Context;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    const/16 v18, 0x40

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v15, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 3017
    .local v15, "sigs":[Landroid/content/pm/Signature;
    if-eqz v15, :cond_2

    .line 3018
    move-object v6, v15

    .local v6, "arr$":[Landroid/content/pm/Signature;
    array-length v12, v6

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_1
    if-ge v11, v12, :cond_2

    aget-object v14, v6, v11

    .line 3019
    .local v14, "sig":Landroid/content/pm/Signature;
    invoke-virtual {v14}, Landroid/content/pm/Signature;->hashCode()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v9

    .line 3020
    .local v9, "hashCode":Ljava/lang/String;
    const-string v17, "CORE"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "sig available hashcode: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 3021
    if-nez v10, :cond_5

    .line 3022
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_4

    .line 3023
    move-object v10, v9

    .line 3018
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 3025
    :cond_4
    const/16 v17, 0x0

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    goto :goto_2

    .line 3028
    :cond_5
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v17

    const/16 v18, 0x8

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_6

    .line 3029
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ":"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto :goto_2

    .line 3031
    :cond_6
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, ":"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const/16 v18, 0x0

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v10

    goto :goto_2

    .line 3037
    .end local v6    # "arr$":[Landroid/content/pm/Signature;
    .end local v9    # "hashCode":Ljava/lang/String;
    .end local v11    # "i$":I
    .end local v12    # "len$":I
    .end local v14    # "sig":Landroid/content/pm/Signature;
    .end local v15    # "sigs":[Landroid/content/pm/Signature;
    :catch_1
    move-exception v7

    .line 3038
    .local v7, "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_3
    const-string v17, "CORE"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Can\'t update packages. Exception: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual {v7}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/quicinc/cne/CneMsg;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3048
    .end local v7    # "ex":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$PackageListener;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    # invokes: Lcom/quicinc/cne/CNE;->sendAppInfoList(Lcom/quicinc/cne/CNERequest;)V
    invoke-static {v0, v13}, Lcom/quicinc/cne/CNE;->access$4900(Lcom/quicinc/cne/CNE;Lcom/quicinc/cne/CNERequest;)V
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method


# virtual methods
.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 3058
    :try_start_0
    const-string v0, "CORE"

    const-string v1, "Finalize...."

    invoke-static {v0, v1}, Lcom/quicinc/cne/CneMsg;->logw(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3061
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 3063
    return-void

    .line 3061
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method protected sendInstalledPackageInfo()Z
    .locals 22

    .prologue
    .line 2879
    const-string v18, "CORE"

    const-string v19, "initPackageInfo"

    invoke-static/range {v18 .. v19}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2881
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$PackageListener;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v18, v0

    # getter for: Lcom/quicinc/cne/CNE;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/quicinc/cne/CNE;->access$3400(Lcom/quicinc/cne/CNE;)Landroid/content/Context;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v18

    const/16 v19, 0x40

    invoke-virtual/range {v18 .. v19}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v12

    .line 2882
    .local v12, "pkgList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    const/16 v18, 0x17

    invoke-static/range {v18 .. v18}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v14

    .line 2883
    .local v14, "rr":Lcom/quicinc/cne/CNERequest;
    const/4 v11, 0x0

    .line 2884
    .local v11, "numPkgsCopied":I
    const/16 v18, 0x0

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v19

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v14, v1, v2}, Lcom/quicinc/cne/CNE$PackageListener;->initRequest(Lcom/quicinc/cne/CNERequest;II)Z

    move-result v18

    if-nez v18, :cond_0

    .line 2885
    const/16 v18, 0x0

    .line 2961
    :goto_0
    return v18

    .line 2890
    :cond_0
    :try_start_0
    const-string v18, "CORE"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "initPackageInfo: size:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/quicinc/cne/CneMsg;->logv(Ljava/lang/String;Ljava/lang/String;)V

    .line 2891
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v8, v0, :cond_7

    .line 2892
    invoke-interface {v12, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/content/pm/PackageInfo;

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    invoke-interface {v12, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/content/pm/PackageInfo;

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-boolean v0, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    move/from16 v18, v0

    if-eqz v18, :cond_5

    .line 2896
    invoke-interface {v12, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/content/pm/PackageInfo;

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    move-object/from16 v16, v0

    .line 2897
    .local v16, "sigs":[Landroid/content/pm/Signature;
    const/4 v7, 0x0

    .line 2898
    .local v7, "hashes":Ljava/lang/String;
    invoke-interface {v12, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/content/pm/PackageInfo;

    move-object/from16 v0, v18

    iget-object v13, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 2899
    .local v13, "pkgName":Ljava/lang/String;
    invoke-interface {v12, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/content/pm/PackageInfo;

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    move/from16 v17, v0

    .line 2901
    .local v17, "uid":I
    if-eqz v16, :cond_4

    .line 2902
    move-object/from16 v4, v16

    .local v4, "arr$":[Landroid/content/pm/Signature;
    array-length v10, v4

    .local v10, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_2
    if-ge v9, v10, :cond_4

    aget-object v15, v4, v9

    .line 2903
    .local v15, "sig":Landroid/content/pm/Signature;
    invoke-virtual {v15}, Landroid/content/pm/Signature;->hashCode()I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    .line 2904
    .local v6, "hashCode":Ljava/lang/String;
    const-string v18, "CORE"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "sig available hashcode: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2905
    if-nez v7, :cond_2

    .line 2906
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v18

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_1

    .line 2907
    move-object v7, v6

    .line 2902
    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 2909
    :cond_1
    const/16 v18, 0x0

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    .line 2912
    :cond_2
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v18

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_3

    .line 2913
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ":"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    .line 2915
    :cond_3
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ":"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const/16 v19, 0x0

    const/16 v20, 0x8

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v6, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_3

    .line 2921
    .end local v4    # "arr$":[Landroid/content/pm/Signature;
    .end local v6    # "hashCode":Ljava/lang/String;
    .end local v9    # "i$":I
    .end local v10    # "len$":I
    .end local v15    # "sig":Landroid/content/pm/Signature;
    :cond_4
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v14, v13, v1, v7}, Lcom/quicinc/cne/CNE$PackageListener;->addPackageInfo(Lcom/quicinc/cne/CNERequest;Ljava/lang/String;ILjava/lang/String;)Z

    move-result v18

    if-nez v18, :cond_6

    .line 2922
    const-string v18, "CORE"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "initPackageInfo: appname:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " uid:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "hashes:"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " failed"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/quicinc/cne/CneMsg;->logi(Ljava/lang/String;Ljava/lang/String;)V

    .line 2891
    .end local v7    # "hashes":Ljava/lang/String;
    .end local v13    # "pkgName":Ljava/lang/String;
    .end local v16    # "sigs":[Landroid/content/pm/Signature;
    .end local v17    # "uid":I
    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 2928
    .restart local v7    # "hashes":Ljava/lang/String;
    .restart local v13    # "pkgName":Ljava/lang/String;
    .restart local v16    # "sigs":[Landroid/content/pm/Signature;
    .restart local v17    # "uid":I
    :cond_6
    add-int/lit8 v11, v11, 0x1

    .line 2939
    iget-object v0, v14, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/os/Parcel;->dataSize()I

    move-result v18

    move/from16 v0, v18

    int-to-double v0, v0

    move-wide/from16 v18, v0

    const-wide v20, 0x40b999999999999aL    # 6553.6

    cmpl-double v18, v18, v20

    if-ltz v18, :cond_5

    .line 2942
    iget-object v0, v14, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/quicinc/cne/CNE$PackageListener;->dataPosNumOfApps:I

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 2943
    iget-object v0, v14, Lcom/quicinc/cne/CNERequest;->mp:Landroid/os/Parcel;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Landroid/os/Parcel;->writeInt(I)V

    .line 2944
    const-string v18, "CORE"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "numPkgsCopied ="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    # invokes: Lcom/quicinc/cne/CNE;->dlogi(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v18 .. v19}, Lcom/quicinc/cne/CNE;->access$100(Ljava/lang/String;Ljava/lang/String;)V

    .line 2945
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$PackageListener;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # invokes: Lcom/quicinc/cne/CNE;->sendAppInfoList(Lcom/quicinc/cne/CNERequest;)V
    invoke-static {v0, v14}, Lcom/quicinc/cne/CNE;->access$4900(Lcom/quicinc/cne/CNE;Lcom/quicinc/cne/CNERequest;)V

    .line 2947
    const/4 v11, 0x0

    .line 2948
    const/16 v18, 0x17

    invoke-static/range {v18 .. v18}, Lcom/quicinc/cne/CNERequest;->obtain(I)Lcom/quicinc/cne/CNERequest;

    move-result-object v14

    .line 2950
    const/16 v18, 0x0

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v19

    add-int/lit8 v20, v8, 0x1

    sub-int v19, v19, v20

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v14, v1, v2}, Lcom/quicinc/cne/CNE$PackageListener;->initRequest(Lcom/quicinc/cne/CNERequest;II)Z

    move-result v18

    if-nez v18, :cond_5

    .line 2951
    const/16 v18, 0x0

    goto/16 :goto_0

    .line 2957
    .end local v7    # "hashes":Ljava/lang/String;
    .end local v13    # "pkgName":Ljava/lang/String;
    .end local v16    # "sigs":[Landroid/content/pm/Signature;
    .end local v17    # "uid":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/quicinc/cne/CNE$PackageListener;->this$0:Lcom/quicinc/cne/CNE;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # invokes: Lcom/quicinc/cne/CNE;->sendAppInfoList(Lcom/quicinc/cne/CNERequest;)V
    invoke-static {v0, v14}, Lcom/quicinc/cne/CNE;->access$4900(Lcom/quicinc/cne/CNE;Lcom/quicinc/cne/CNERequest;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2961
    .end local v8    # "i":I
    :goto_4
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 2958
    :catch_0
    move-exception v5

    .line 2959
    .local v5, "ex":Ljava/lang/NullPointerException;
    const-string v18, "CORE"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "initPackageInfo got exception: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v5}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/quicinc/cne/CneMsg;->logw(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method
