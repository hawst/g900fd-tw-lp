.class public Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;
.super Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;
.source "QmiPrimitiveTypes.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "QmiArray"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;",
        ">",
        "Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;"
    }
.end annotation


# instance fields
.field private mArrayLength:S

.field private mVal:[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private vLenSize:S


# direct methods
.method public constructor <init>([Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Ljava/lang/Class;S)V
    .locals 3
    .param p3, "valueSize"    # S
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;",
            "Ljava/lang/Class",
            "<TT;>;S)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 197
    .local p0, "this":Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;, "Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray<TT;>;"
    .local p1, "arr":[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;, "[TT;"
    .local p2, "c":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-direct {p0}, Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;-><init>()V

    .line 199
    :try_start_0
    iput-object p1, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->mVal:[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    .line 200
    array-length v1, p1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->mArrayLength:S

    .line 201
    iput-short p3, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->vLenSize:S
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    return-void

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/NumberFormatException;
    new-instance v1, Ljava/security/InvalidParameterException;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public constructor <init>([Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;SLjava/lang/Class;)V
    .locals 3
    .param p2, "maxArraySize"    # S
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;S",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 179
    .local p0, "this":Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;, "Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray<TT;>;"
    .local p1, "arr":[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;, "[TT;"
    .local p3, "c":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    invoke-direct {p0}, Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;-><init>()V

    .line 181
    :try_start_0
    iput-object p1, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->mVal:[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    .line 182
    array-length v1, p1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->mArrayLength:S

    .line 183
    const/16 v1, 0xff

    if-le p2, v1, :cond_0

    .line 184
    const/4 v1, 0x2

    iput-short v1, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->vLenSize:S

    .line 191
    :goto_0
    return-void

    .line 187
    :cond_0
    const/4 v1, 0x1

    iput-short v1, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->vLenSize:S
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 188
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Ljava/lang/NumberFormatException;
    new-instance v1, Ljava/security/InvalidParameterException;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public getSize()I
    .locals 3

    .prologue
    .line 208
    .local p0, "this":Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;, "Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray<TT;>;"
    const/4 v0, 0x0

    .line 209
    .local v0, "actualArrayBytesSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-short v2, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->mArrayLength:S

    if-ge v1, v2, :cond_0

    .line 210
    iget-object v2, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->mVal:[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;->getSize()I

    move-result v2

    add-int/2addr v0, v2

    .line 209
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 211
    :cond_0
    iget-short v2, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->vLenSize:S

    add-int/2addr v2, v0

    return v2
.end method

.method public toByteArray()[B
    .locals 4

    .prologue
    .line 222
    .local p0, "this":Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;, "Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray<TT;>;"
    invoke-virtual {p0}, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->getSize()I

    move-result v2

    invoke-static {v2}, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 225
    .local v0, "buf":Ljava/nio/ByteBuffer;
    iget-short v2, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->vLenSize:S

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 226
    iget-short v2, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->mArrayLength:S

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 230
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-short v2, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->mArrayLength:S

    if-ge v1, v2, :cond_1

    .line 231
    iget-object v2, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->mVal:[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 230
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 228
    .end local v1    # "i":I
    :cond_0
    iget-short v2, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->mArrayLength:S

    invoke-static {v2}, Lcom/qualcomm/qcrilhook/PrimitiveParser;->parseByte(S)B

    move-result v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 234
    .restart local v1    # "i":I
    :cond_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    return-object v2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 215
    .local p0, "this":Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;, "Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray<TT;>;"
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 216
    .local v1, "s":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-short v2, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->mArrayLength:S

    if-ge v0, v2, :cond_0

    .line 217
    iget-object v2, p0, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;->mVal:[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 216
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 218
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
