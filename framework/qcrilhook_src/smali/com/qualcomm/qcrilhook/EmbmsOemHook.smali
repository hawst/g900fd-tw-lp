.class public Lcom/qualcomm/qcrilhook/EmbmsOemHook;
.super Landroid/os/Handler;
.source "EmbmsOemHook.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActiveLogPacketIDsResponse;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$TimeResponse;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActDeactResponse;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$SigStrengthResponse;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiResponse;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$DeliverLogPacketRequest;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActiveLogPacketIDsRequest;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$SetTimeRequest;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiDeActivateRequest;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActDeactRequest;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiActivateRequest;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$DisableResponse;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$EnableResponse;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$E911StateIndication;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$Sib16Coverage;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$CoverageState;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$SaiIndication;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$RadioStateIndication;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$CellIdIndication;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$OosState;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiListIndication;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$StateChangeInfo;,
        Lcom/qualcomm/qcrilhook/EmbmsOemHook$UnsolObject;
    }
.end annotation


# static fields
.field private static final EMBMSHOOK_MSG_ID_ACTDEACT:S = 0x11s

.field private static final EMBMSHOOK_MSG_ID_ACTIVATE:S = 0x2s

.field private static final EMBMSHOOK_MSG_ID_DEACTIVATE:S = 0x3s

.field private static final EMBMSHOOK_MSG_ID_DELIVER_LOG_PACKET:S = 0x16s

.field private static final EMBMSHOOK_MSG_ID_DISABLE:S = 0x1s

.field private static final EMBMSHOOK_MSG_ID_ENABLE:S = 0x0s

.field private static final EMBMSHOOK_MSG_ID_GET_ACTIVE:S = 0x5s

.field private static final EMBMSHOOK_MSG_ID_GET_ACTIVE_LOG_PACKET_IDS:S = 0x15s

.field private static final EMBMSHOOK_MSG_ID_GET_AVAILABLE:S = 0x4s

.field private static final EMBMSHOOK_MSG_ID_GET_COVERAGE:S = 0x8s

.field private static final EMBMSHOOK_MSG_ID_GET_E911_STATE:S = 0x1bs

.field private static final EMBMSHOOK_MSG_ID_GET_SIB16_COVERAGE:S = 0x18s

.field private static final EMBMSHOOK_MSG_ID_GET_SIG_STRENGTH:S = 0x9s

.field private static final EMBMSHOOK_MSG_ID_GET_TIME:S = 0x1as

.field private static final EMBMSHOOK_MSG_ID_SET_TIME:S = 0x17s

.field private static final EMBMSHOOK_MSG_ID_UNSOL_ACTIVE_TMGI_LIST:S = 0xcs

.field private static final EMBMSHOOK_MSG_ID_UNSOL_AVAILABLE_TMGI_LIST:S = 0xfs

.field private static final EMBMSHOOK_MSG_ID_UNSOL_CELL_ID:S = 0x12s

.field private static final EMBMSHOOK_MSG_ID_UNSOL_COVERAGE_STATE:S = 0xds

.field private static final EMBMSHOOK_MSG_ID_UNSOL_E911_STATE:S = 0x1cs

.field private static final EMBMSHOOK_MSG_ID_UNSOL_OOS_STATE:S = 0x10s

.field private static final EMBMSHOOK_MSG_ID_UNSOL_RADIO_STATE:S = 0x13s

.field private static final EMBMSHOOK_MSG_ID_UNSOL_SAI_LIST:S = 0x14s

.field private static final EMBMSHOOK_MSG_ID_UNSOL_SIB16:S = 0x19s

.field private static final EMBMSHOOK_MSG_ID_UNSOL_STATE_CHANGE:S = 0xbs

.field private static final EMBMS_SERVICE_ID:S = 0x2s

.field private static final FAILURE:I = -0x1

.field private static LOG_TAG:Ljava/lang/String; = null

.field private static final OEM_HOOK_RESPONSE:I = 0x1

.field private static final ONE_BYTE:S = 0x1s

.field private static final QCRILHOOK_READY_CALLBACK:I = 0x2

.field private static final SIZE_OF_TMGI:I = 0x6

.field private static final SUCCESS:I = 0x0

.field private static final TLV_TYPE_ACTDEACTIVATE_REQ_ACT_TMGI:B = 0x3t

.field private static final TLV_TYPE_ACTDEACTIVATE_REQ_DEACT_TMGI:B = 0x4t

.field private static final TLV_TYPE_ACTDEACTIVATE_REQ_EARFCN_LIST:B = 0x6t

.field private static final TLV_TYPE_ACTDEACTIVATE_REQ_PRIORITY:B = 0x5t

.field private static final TLV_TYPE_ACTDEACTIVATE_REQ_SAI_LIST:B = 0x10t

.field private static final TLV_TYPE_ACTDEACTIVATE_RESP_ACTTMGI:B = 0x11t

.field private static final TLV_TYPE_ACTDEACTIVATE_RESP_ACT_CODE:B = 0x2t

.field private static final TLV_TYPE_ACTDEACTIVATE_RESP_DEACTTMGI:B = 0x12t

.field private static final TLV_TYPE_ACTDEACTIVATE_RESP_DEACT_CODE:B = 0x3t

.field private static final TLV_TYPE_ACTIVATE_REQ_EARFCN_LIST:B = 0x5t

.field private static final TLV_TYPE_ACTIVATE_REQ_PRIORITY:B = 0x4t

.field private static final TLV_TYPE_ACTIVATE_REQ_SAI_LIST:B = 0x10t

.field private static final TLV_TYPE_ACTIVATE_REQ_TMGI:B = 0x3t

.field private static final TLV_TYPE_ACTIVATE_RESP_TMGI:B = 0x11t

.field private static final TLV_TYPE_ACTIVELOGPACKETID_REQ_PACKET_ID_LIST:S = 0x2s

.field private static final TLV_TYPE_ACTIVELOGPACKETID_RESP_PACKET_ID_LIST:S = 0x2s

.field private static final TLV_TYPE_COMMON_REQ_CALL_ID:B = 0x2t

.field private static final TLV_TYPE_COMMON_REQ_TRACE_ID:B = 0x1t

.field private static final TLV_TYPE_COMMON_RESP_CALL_ID:B = 0x10t

.field private static final TLV_TYPE_COMMON_RESP_CODE:B = 0x2t

.field private static final TLV_TYPE_COMMON_RESP_TRACE_ID:B = 0x1t

.field private static final TLV_TYPE_DEACTIVATE_REQ_TMGI:B = 0x3t

.field private static final TLV_TYPE_DEACTIVATE_RESP_TMGI:B = 0x11t

.field private static final TLV_TYPE_DELIVERLOGPACKET_REQ_LOG_PACKET:S = 0x3s

.field private static final TLV_TYPE_DELIVERLOGPACKET_REQ_PACKET_ID:S = 0x2s

.field private static final TLV_TYPE_ENABLE_RESP_IFNAME:B = 0x11t

.field private static final TLV_TYPE_ENABLE_RESP_IF_INDEX:B = 0x12t

.field private static final TLV_TYPE_GET_ACTIVE_RESP_TMGI_ARRAY:B = 0x10t

.field private static final TLV_TYPE_GET_AVAILABLE_RESP_TMGI_ARRAY:B = 0x10t

.field private static final TLV_TYPE_GET_COVERAGE_STATE_RESP_STATE:B = 0x10t

.field private static final TLV_TYPE_GET_E911_RESP_STATE:S = 0x10s

.field private static final TLV_TYPE_GET_SIG_STRENGTH_RESP_ACTIVE_TMGI_LIST:B = 0x14t

.field private static final TLV_TYPE_GET_SIG_STRENGTH_RESP_EXCESS_SNR:B = 0x12t

.field private static final TLV_TYPE_GET_SIG_STRENGTH_RESP_MBSFN_AREA_ID:B = 0x10t

.field private static final TLV_TYPE_GET_SIG_STRENGTH_RESP_NUMBER_OF_TMGI_PER_MBSFN:B = 0x13t

.field private static final TLV_TYPE_GET_SIG_STRENGTH_RESP_SNR:B = 0x11t

.field private static final TLV_TYPE_GET_TIME_RESP_DAY_LIGHT_SAVING:B = 0x10t

.field private static final TLV_TYPE_GET_TIME_RESP_LEAP_SECONDS:B = 0x11t

.field private static final TLV_TYPE_GET_TIME_RESP_LOCAL_TIME_OFFSET:B = 0x12t

.field private static final TLV_TYPE_GET_TIME_RESP_TIME_MSECONDS:B = 0x3t

.field private static final TLV_TYPE_SET_TIME_REQ_SNTP_SUCCESS:B = 0x1t

.field private static final TLV_TYPE_SET_TIME_REQ_TIME_MSECONDS:B = 0x10t

.field private static final TLV_TYPE_SET_TIME_REQ_TIME_STAMP:B = 0x11t

.field private static final TLV_TYPE_UNSOL_ACTIVE_IND_TMGI_ARRAY:S = 0x2s

.field private static final TLV_TYPE_UNSOL_AVAILABLE_IND_TMGI_ARRAY_OR_RESPONSE_CODE:S = 0x2s

.field private static final TLV_TYPE_UNSOL_CELL_ID_IND_CID:S = 0x4s

.field private static final TLV_TYPE_UNSOL_CELL_ID_IND_MCC:S = 0x2s

.field private static final TLV_TYPE_UNSOL_CELL_ID_IND_MNC:S = 0x3s

.field private static final TLV_TYPE_UNSOL_COVERAGE_IND_STATE_OR_RESPONSE_CODE:S = 0x2s

.field private static final TLV_TYPE_UNSOL_E911_STATE_OR_RESPONSE_CODE:S = 0x2s

.field private static final TLV_TYPE_UNSOL_OOS_IND_STATE:S = 0x2s

.field private static final TLV_TYPE_UNSOL_OOS_IND_TMGI_ARRAY:S = 0x3s

.field private static final TLV_TYPE_UNSOL_RADIO_STATE:S = 0x2s

.field private static final TLV_TYPE_UNSOL_SAI_IND_AVAILABLE_SAI_LIST:S = 0x4s

.field private static final TLV_TYPE_UNSOL_SAI_IND_CAMPED_SAI_LIST:S = 0x2s

.field private static final TLV_TYPE_UNSOL_SAI_IND_SAI_PER_GROUP_LIST:S = 0x3s

.field private static final TLV_TYPE_UNSOL_SIB16:S = 0x1s

.field private static final TLV_TYPE_UNSOL_STATE_IND_IF_INDEX:S = 0x3s

.field private static final TLV_TYPE_UNSOL_STATE_IND_IP_ADDRESS:S = 0x2s

.field private static final TLV_TYPE_UNSOL_STATE_IND_STATE:S = 0x1s

.field private static final TWO_BYTES:S = 0x2s

.field private static final UNSOL_BASE_QCRILHOOK:I = 0x1000

.field public static final UNSOL_TYPE_ACTIVE_TMGI_LIST:I = 0x2

.field public static final UNSOL_TYPE_AVAILABLE_TMGI_LIST:I = 0x4

.field public static final UNSOL_TYPE_BROADCAST_COVERAGE:I = 0x3

.field public static final UNSOL_TYPE_CELL_ID:I = 0x6

.field public static final UNSOL_TYPE_E911_STATE:I = 0xa

.field public static final UNSOL_TYPE_EMBMSOEMHOOK_READY_CALLBACK:I = 0x1001

.field public static final UNSOL_TYPE_OOS_STATE:I = 0x5

.field public static final UNSOL_TYPE_RADIO_STATE:I = 0x7

.field public static final UNSOL_TYPE_SAI_LIST:I = 0x8

.field public static final UNSOL_TYPE_SIB16_COVERAGE:I = 0x9

.field public static final UNSOL_TYPE_STATE_CHANGE:I = 0x1

.field private static mRefCount:I

.field private static sInstance:Lcom/qualcomm/qcrilhook/EmbmsOemHook;


# instance fields
.field private mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

.field private mRegistrants:Landroid/os/RegistrantList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-string v0, "EmbmsOemHook"

    sput-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    .line 211
    const/4 v0, 0x0

    sput v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mRefCount:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x2

    .line 217
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 218
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v1, "EmbmsOemHook ()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    invoke-static {p1}, Lcom/qualcomm/qcrilhook/QmiOemHook;->getInstance(Landroid/content/Context;)Lcom/qualcomm/qcrilhook/QmiOemHook;

    move-result-object v0

    iput-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    .line 220
    const/4 v0, 0x1

    invoke-static {v2, p0, v0}, Lcom/qualcomm/qcrilhook/QmiOemHook;->registerService(SLandroid/os/Handler;I)V

    .line 222
    const/4 v0, 0x0

    invoke-static {p0, v2, v0}, Lcom/qualcomm/qcrilhook/QmiOemHook;->registerOnReadyCb(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 223
    new-instance v0, Landroid/os/RegistrantList;

    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mRegistrants:Landroid/os/RegistrantList;

    .line 224
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/qualcomm/qcrilhook/EmbmsOemHook;Ljava/nio/ByteBuffer;)[B
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qcrilhook/EmbmsOemHook;
    .param p1, "x1"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->parseTmgi(Ljava/nio/ByteBuffer;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/qualcomm/qcrilhook/EmbmsOemHook;S[B)Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qcrilhook/EmbmsOemHook;
    .param p1, "x1"    # S
    .param p2, "x2"    # [B

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->byteArrayToQmiArray(S[B)Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/qualcomm/qcrilhook/EmbmsOemHook;S[I)Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qcrilhook/EmbmsOemHook;
    .param p1, "x1"    # S
    .param p2, "x2"    # [I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->intArrayToQmiArray(S[I)Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/qualcomm/qcrilhook/EmbmsOemHook;Ljava/nio/ByteBuffer;)[B
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qcrilhook/EmbmsOemHook;
    .param p1, "x1"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->parseActiveTmgi(Ljava/nio/ByteBuffer;)[B

    move-result-object v0

    return-object v0
.end method

.method private byteArrayToQmiArray(S[B)Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;
    .locals 4
    .param p1, "vSize"    # S
    .param p2, "arr"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S[B)",
            "Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray",
            "<",
            "Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiByte;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1375
    array-length v2, p2

    new-array v1, v2, [Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiByte;

    .line 1376
    .local v1, "qmiByteArray":[Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiByte;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    .line 1377
    new-instance v2, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiByte;

    aget-byte v3, p2, v0

    invoke-direct {v2, v3}, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiByte;-><init>(B)V

    aput-object v2, v1, v0

    .line 1376
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1379
    :cond_0
    new-instance v2, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;

    const-class v3, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiByte;

    invoke-direct {v2, v1, v3, p1}, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;-><init>([Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Ljava/lang/Class;S)V

    return-object v2
.end method

.method public static bytesToHexString([B)Ljava/lang/String;
    .locals 4
    .param p0, "bytes"    # [B

    .prologue
    .line 689
    if-nez p0, :cond_0

    const/4 v3, 0x0

    .line 701
    :goto_0
    return-object v3

    .line 691
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    array-length v3, p0

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 693
    .local v2, "ret":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, p0

    if-ge v1, v3, :cond_1

    .line 695
    aget-byte v3, p0, v1

    shr-int/lit8 v3, v3, 0x4

    and-int/lit8 v0, v3, 0xf

    .line 696
    .local v0, "b":I
    const-string v3, "0123456789abcdef"

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 697
    aget-byte v3, p0, v1

    and-int/lit8 v0, v3, 0xf

    .line 698
    const-string v3, "0123456789abcdef"

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 693
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 701
    .end local v0    # "b":I
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/qualcomm/qcrilhook/EmbmsOemHook;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 228
    const-class v1, Lcom/qualcomm/qcrilhook/EmbmsOemHook;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->sInstance:Lcom/qualcomm/qcrilhook/EmbmsOemHook;

    if-nez v0, :cond_0

    .line 229
    new-instance v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;

    invoke-direct {v0, p0}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->sInstance:Lcom/qualcomm/qcrilhook/EmbmsOemHook;

    .line 230
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Singleton Instance of Embms created."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    :cond_0
    sget v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mRefCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mRefCount:I

    .line 233
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->sInstance:Lcom/qualcomm/qcrilhook/EmbmsOemHook;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 228
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private handleResponse(Ljava/util/HashMap;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 296
    .local p1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    const/16 v10, 0x8

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Short;

    invoke-virtual {v10}, Ljava/lang/Short;->shortValue()S

    move-result v5

    .line 297
    .local v5, "msgId":S
    const/4 v10, 0x2

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 298
    .local v7, "responseSize":I
    const/4 v10, 0x3

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 299
    .local v9, "successStatus":I
    const/4 v10, 0x4

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Message;

    .line 300
    .local v4, "msg":Landroid/os/Message;
    const/4 v10, 0x6

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/nio/ByteBuffer;

    .line 303
    .local v6, "respByteBuf":Ljava/nio/ByteBuffer;
    sget-object v10, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, " responseSize="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " successStatus="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    packed-switch v5, :pswitch_data_0

    .line 445
    :pswitch_0
    sget-object v10, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "received unexpected msgId "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    :goto_0
    return-void

    .line 308
    :pswitch_1
    new-instance v10, Lcom/qualcomm/qcrilhook/EmbmsOemHook$EnableResponse;

    invoke-direct {v10, p0, v9, v6}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$EnableResponse;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;ILjava/nio/ByteBuffer;)V

    iput-object v10, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 309
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 313
    :pswitch_2
    new-instance v10, Lcom/qualcomm/qcrilhook/EmbmsOemHook$DisableResponse;

    invoke-direct {v10, p0, v9, v6}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$DisableResponse;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;ILjava/nio/ByteBuffer;)V

    iput-object v10, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 314
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 319
    :pswitch_3
    new-instance v10, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiResponse;

    invoke-direct {v10, p0, v9, v6}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiResponse;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;ILjava/nio/ByteBuffer;)V

    iput-object v10, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 320
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 324
    :pswitch_4
    new-instance v10, Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActDeactResponse;

    invoke-direct {v10, p0, v9, v6}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActDeactResponse;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;ILjava/nio/ByteBuffer;)V

    iput-object v10, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 325
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 329
    :pswitch_5
    new-instance v2, Lcom/qualcomm/qcrilhook/EmbmsOemHook$StateChangeInfo;

    invoke-direct {v2, p0, v6}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$StateChangeInfo;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;Ljava/nio/ByteBuffer;)V

    .line 330
    .local v2, "info":Lcom/qualcomm/qcrilhook/EmbmsOemHook$StateChangeInfo;
    const/4 v10, 0x1

    invoke-direct {p0, v10, v2}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->notifyUnsol(ILjava/lang/Object;)V

    goto :goto_0

    .line 339
    .end local v2    # "info":Lcom/qualcomm/qcrilhook/EmbmsOemHook$StateChangeInfo;
    :pswitch_6
    const/4 v10, 0x4

    if-ne v5, v10, :cond_0

    if-eqz v9, :cond_0

    .line 340
    sget-object v10, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Error received in EMBMSHOOK_MSG_ID_GET_AVAILABLE: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 344
    :cond_0
    new-instance v3, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiListIndication;

    invoke-direct {v3, p0, v6, v5}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiListIndication;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;Ljava/nio/ByteBuffer;S)V

    .line 345
    .local v3, "list":Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiListIndication;
    const/4 v10, 0x4

    invoke-direct {p0, v10, v3}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->notifyUnsol(ILjava/lang/Object;)V

    goto :goto_0

    .line 354
    .end local v3    # "list":Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiListIndication;
    :pswitch_7
    const/4 v10, 0x5

    if-ne v5, v10, :cond_1

    if-eqz v9, :cond_1

    .line 355
    sget-object v10, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Error received in EMBMSHOOK_MSG_ID_GET_ACTIVE: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 359
    :cond_1
    new-instance v3, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiListIndication;

    invoke-direct {v3, p0, v6, v5}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiListIndication;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;Ljava/nio/ByteBuffer;S)V

    .line 360
    .restart local v3    # "list":Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiListIndication;
    const/4 v10, 0x2

    invoke-direct {p0, v10, v3}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->notifyUnsol(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 369
    .end local v3    # "list":Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiListIndication;
    :pswitch_8
    const/16 v10, 0x8

    if-ne v5, v10, :cond_2

    if-eqz v9, :cond_2

    .line 370
    sget-object v10, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Error received in EMBMSHOOK_MSG_ID_GET_COVERAGE: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 375
    :cond_2
    new-instance v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook$CoverageState;

    invoke-direct {v0, p0, v6, v5}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$CoverageState;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;Ljava/nio/ByteBuffer;S)V

    .line 376
    .local v0, "cs":Lcom/qualcomm/qcrilhook/EmbmsOemHook$CoverageState;
    const/4 v10, 0x3

    invoke-direct {p0, v10, v0}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->notifyUnsol(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 380
    .end local v0    # "cs":Lcom/qualcomm/qcrilhook/EmbmsOemHook$CoverageState;
    :pswitch_9
    new-instance v10, Lcom/qualcomm/qcrilhook/EmbmsOemHook$SigStrengthResponse;

    invoke-direct {v10, p0, v9, v6}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$SigStrengthResponse;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;ILjava/nio/ByteBuffer;)V

    iput-object v10, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 381
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 385
    :pswitch_a
    iput v9, v4, Landroid/os/Message;->arg1:I

    .line 386
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 390
    :pswitch_b
    new-instance v10, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TimeResponse;

    invoke-direct {v10, p0, v9, v6}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TimeResponse;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;ILjava/nio/ByteBuffer;)V

    iput-object v10, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 391
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 397
    :pswitch_c
    const/16 v10, 0x18

    if-ne v5, v10, :cond_3

    if-eqz v9, :cond_3

    .line 398
    sget-object v10, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Error received in EMBMSHOOK_MSG_ID_GET_SIB16_COVERAGE: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 403
    :cond_3
    new-instance v1, Lcom/qualcomm/qcrilhook/EmbmsOemHook$Sib16Coverage;

    invoke-direct {v1, p0, v6}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$Sib16Coverage;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;Ljava/nio/ByteBuffer;)V

    .line 404
    .local v1, "ind":Lcom/qualcomm/qcrilhook/EmbmsOemHook$Sib16Coverage;
    const/16 v10, 0x9

    invoke-direct {p0, v10, v1}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->notifyUnsol(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 408
    .end local v1    # "ind":Lcom/qualcomm/qcrilhook/EmbmsOemHook$Sib16Coverage;
    :pswitch_d
    new-instance v10, Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActiveLogPacketIDsResponse;

    invoke-direct {v10, p0, v9, v6}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActiveLogPacketIDsResponse;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;ILjava/nio/ByteBuffer;)V

    iput-object v10, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 409
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V

    goto/16 :goto_0

    .line 414
    :pswitch_e
    sget-object v10, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, " deliverLogPacket response successStatus="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 420
    :pswitch_f
    new-instance v1, Lcom/qualcomm/qcrilhook/EmbmsOemHook$E911StateIndication;

    invoke-direct {v1, p0, v6, v5}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$E911StateIndication;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;Ljava/nio/ByteBuffer;S)V

    .line 421
    .local v1, "ind":Lcom/qualcomm/qcrilhook/EmbmsOemHook$E911StateIndication;
    const/16 v10, 0xa

    invoke-direct {p0, v10, v1}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->notifyUnsol(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 425
    .end local v1    # "ind":Lcom/qualcomm/qcrilhook/EmbmsOemHook$E911StateIndication;
    :pswitch_10
    new-instance v8, Lcom/qualcomm/qcrilhook/EmbmsOemHook$OosState;

    invoke-direct {v8, p0, v6}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$OosState;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;Ljava/nio/ByteBuffer;)V

    .line 426
    .local v8, "state":Lcom/qualcomm/qcrilhook/EmbmsOemHook$OosState;
    const/4 v10, 0x5

    invoke-direct {p0, v10, v8}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->notifyUnsol(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 430
    .end local v8    # "state":Lcom/qualcomm/qcrilhook/EmbmsOemHook$OosState;
    :pswitch_11
    new-instance v1, Lcom/qualcomm/qcrilhook/EmbmsOemHook$CellIdIndication;

    invoke-direct {v1, p0, v6}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$CellIdIndication;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;Ljava/nio/ByteBuffer;)V

    .line 431
    .local v1, "ind":Lcom/qualcomm/qcrilhook/EmbmsOemHook$CellIdIndication;
    const/4 v10, 0x6

    invoke-direct {p0, v10, v1}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->notifyUnsol(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 435
    .end local v1    # "ind":Lcom/qualcomm/qcrilhook/EmbmsOemHook$CellIdIndication;
    :pswitch_12
    new-instance v1, Lcom/qualcomm/qcrilhook/EmbmsOemHook$RadioStateIndication;

    invoke-direct {v1, p0, v6}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$RadioStateIndication;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;Ljava/nio/ByteBuffer;)V

    .line 436
    .local v1, "ind":Lcom/qualcomm/qcrilhook/EmbmsOemHook$RadioStateIndication;
    const/4 v10, 0x7

    invoke-direct {p0, v10, v1}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->notifyUnsol(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 440
    .end local v1    # "ind":Lcom/qualcomm/qcrilhook/EmbmsOemHook$RadioStateIndication;
    :pswitch_13
    new-instance v1, Lcom/qualcomm/qcrilhook/EmbmsOemHook$SaiIndication;

    invoke-direct {v1, p0, v6}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$SaiIndication;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;Ljava/nio/ByteBuffer;)V

    .line 441
    .local v1, "ind":Lcom/qualcomm/qcrilhook/EmbmsOemHook$SaiIndication;
    const/16 v10, 0x8

    invoke-direct {p0, v10, v1}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->notifyUnsol(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 306
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_6
        :pswitch_10
        :pswitch_4
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_d
        :pswitch_e
        :pswitch_a
        :pswitch_c
        :pswitch_c
        :pswitch_b
        :pswitch_f
        :pswitch_f
    .end packed-switch
.end method

.method private intArrayToQmiArray(S[I)Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;
    .locals 6
    .param p1, "vSize"    # S
    .param p2, "arr"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S[I)",
            "Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray",
            "<",
            "Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiInteger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1388
    if-nez p2, :cond_0

    const/4 v1, 0x0

    .line 1390
    .local v1, "length":I
    :goto_0
    new-array v2, v1, [Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiInteger;

    .line 1391
    .local v2, "qmiIntArray":[Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiInteger;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 1392
    new-instance v3, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiInteger;

    aget v4, p2, v0

    int-to-long v4, v4

    invoke-direct {v3, v4, v5}, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiInteger;-><init>(J)V

    aput-object v3, v2, v0

    .line 1391
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1388
    .end local v0    # "i":I
    .end local v1    # "length":I
    .end local v2    # "qmiIntArray":[Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiInteger;
    :cond_0
    array-length v1, p2

    goto :goto_0

    .line 1394
    .restart local v0    # "i":I
    .restart local v1    # "length":I
    .restart local v2    # "qmiIntArray":[Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiInteger;
    :cond_1
    new-instance v3, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;

    const-class v4, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiInteger;

    invoke-direct {v3, v2, v4, p1}, Lcom/qualcomm/qcrilhook/QmiPrimitiveTypes$QmiArray;-><init>([Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Ljava/lang/Class;S)V

    return-object v3
.end method

.method private notifyUnsol(ILjava/lang/Object;)V
    .locals 5
    .param p1, "type"    # I
    .param p2, "payload"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 454
    new-instance v1, Lcom/qualcomm/qcrilhook/EmbmsOemHook$UnsolObject;

    invoke-direct {v1, p0, p1, p2}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$UnsolObject;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;ILjava/lang/Object;)V

    .line 455
    .local v1, "obj":Lcom/qualcomm/qcrilhook/EmbmsOemHook$UnsolObject;
    new-instance v0, Landroid/os/AsyncResult;

    invoke-direct {v0, v2, v1, v2}, Landroid/os/AsyncResult;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Throwable;)V

    .line 456
    .local v0, "ar":Landroid/os/AsyncResult;
    sget-object v2, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Notifying registrants type = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    iget-object v2, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mRegistrants:Landroid/os/RegistrantList;

    invoke-virtual {v2, v0}, Landroid/os/RegistrantList;->notifyRegistrants(Landroid/os/AsyncResult;)V

    .line 458
    return-void
.end method

.method private parseActiveTmgi(Ljava/nio/ByteBuffer;)[B
    .locals 8
    .param p1, "buf"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 855
    const/4 v1, 0x0

    .line 856
    .local v1, "index":I
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v6

    .line 857
    .local v6, "totalTmgis":S
    mul-int/lit8 v7, v6, 0x6

    new-array v4, v7, [B

    .line 859
    .local v4, "tmgi":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v6, :cond_1

    .line 860
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v5

    .line 861
    .local v5, "tmgiLength":B
    const/4 v3, 0x0

    .local v3, "j":I
    move v2, v1

    .end local v1    # "index":I
    .local v2, "index":I
    :goto_1
    if-ge v3, v5, :cond_0

    .line 862
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v7

    aput-byte v7, v4, v2

    .line 861
    add-int/lit8 v3, v3, 0x1

    move v2, v1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    goto :goto_1

    .line 859
    :cond_0
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    goto :goto_0

    .line 865
    .end local v3    # "j":I
    .end local v5    # "tmgiLength":B
    :cond_1
    return-object v4
.end method

.method private parseTmgi(Ljava/nio/ByteBuffer;)[B
    .locals 8
    .param p1, "buf"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 841
    const/4 v1, 0x0

    .line 842
    .local v1, "index":I
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v6

    .line 843
    .local v6, "totalTmgis":B
    mul-int/lit8 v7, v6, 0x6

    new-array v4, v7, [B

    .line 845
    .local v4, "tmgi":[B
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v6, :cond_1

    .line 846
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v5

    .line 847
    .local v5, "tmgiLength":B
    const/4 v3, 0x0

    .local v3, "j":I
    move v2, v1

    .end local v1    # "index":I
    .local v2, "index":I
    :goto_1
    if-ge v3, v5, :cond_0

    .line 848
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v7

    aput-byte v7, v4, v2

    .line 847
    add-int/lit8 v3, v3, 0x1

    move v2, v1

    .end local v1    # "index":I
    .restart local v2    # "index":I
    goto :goto_1

    .line 845
    :cond_0
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    .end local v2    # "index":I
    .restart local v1    # "index":I
    goto :goto_0

    .line 851
    .end local v3    # "j":I
    .end local v5    # "tmgiLength":B
    :cond_1
    return-object v4
.end method


# virtual methods
.method public actDeactTmgi(IB[B[BI[I[ILandroid/os/Message;)I
    .locals 10
    .param p1, "traceId"    # I
    .param p2, "callId"    # B
    .param p3, "actTmgi"    # [B
    .param p4, "deActTmgi"    # [B
    .param p5, "priority"    # I
    .param p6, "saiList"    # [I
    .param p7, "earfcnList"    # [I
    .param p8, "msg"    # Landroid/os/Message;

    .prologue
    .line 541
    new-instance v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActDeactRequest;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActDeactRequest;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;IB[B[BI[I[I)V

    .line 545
    .local v0, "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActDeactRequest;
    :try_start_0
    iget-object v1, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    const/4 v2, 0x2

    const/16 v3, 0x11

    invoke-virtual {v0}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActDeactRequest;->getTypes()[S

    move-result-object v4

    invoke-virtual {v0}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActDeactRequest;->getItems()[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    move-result-object v5

    move-object/from16 v6, p8

    invoke-virtual/range {v1 .. v6}, Lcom/qualcomm/qcrilhook/QmiOemHook;->sendQmiMessageAsync(SS[S[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 552
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 548
    :catch_0
    move-exception v9

    .line 549
    .local v9, "e":Ljava/io/IOException;
    sget-object v1, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v2, "IOException occurred during activate-deactivate !!!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public activateTmgi(IB[BI[I[ILandroid/os/Message;)I
    .locals 9
    .param p1, "traceId"    # I
    .param p2, "callId"    # B
    .param p3, "tmgi"    # [B
    .param p4, "priority"    # I
    .param p5, "saiList"    # [I
    .param p6, "earfcnList"    # [I
    .param p7, "msg"    # Landroid/os/Message;

    .prologue
    .line 504
    new-instance v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiActivateRequest;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiActivateRequest;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;IB[BI[I[I)V

    .line 508
    .local v0, "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiActivateRequest;
    :try_start_0
    iget-object v1, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    const/4 v2, 0x2

    const/4 v3, 0x2

    invoke-virtual {v0}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiActivateRequest;->getTypes()[S

    move-result-object v4

    invoke-virtual {v0}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiActivateRequest;->getItems()[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    move-result-object v5

    move-object/from16 v6, p7

    invoke-virtual/range {v1 .. v6}, Lcom/qualcomm/qcrilhook/QmiOemHook;->sendQmiMessageAsync(SS[S[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 515
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 511
    :catch_0
    move-exception v8

    .line 512
    .local v8, "e":Ljava/io/IOException;
    sget-object v1, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v2, "IOException occurred during activate !!!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public deactivateTmgi(IB[BLandroid/os/Message;)I
    .locals 8
    .param p1, "traceId"    # I
    .param p2, "callId"    # B
    .param p3, "tmgi"    # [B
    .param p4, "msg"    # Landroid/os/Message;

    .prologue
    .line 522
    new-instance v7, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiDeActivateRequest;

    invoke-direct {v7, p0, p1, p3, p2}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiDeActivateRequest;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;I[BB)V

    .line 525
    .local v7, "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiDeActivateRequest;
    :try_start_0
    iget-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    const/4 v1, 0x2

    const/4 v2, 0x3

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiDeActivateRequest;->getTypes()[S

    move-result-object v3

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$TmgiDeActivateRequest;->getItems()[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    move-result-object v4

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qcrilhook/QmiOemHook;->sendQmiMessageAsync(SS[S[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 532
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 528
    :catch_0
    move-exception v6

    .line 529
    .local v6, "e":Ljava/io/IOException;
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v1, "IOException occurred during deactivate !!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public deliverLogPacket(II[B)I
    .locals 8
    .param p1, "traceId"    # I
    .param p2, "logPacketId"    # I
    .param p3, "logPacket"    # [B

    .prologue
    .line 669
    new-instance v7, Lcom/qualcomm/qcrilhook/EmbmsOemHook$DeliverLogPacketRequest;

    invoke-direct {v7, p0, p1, p2, p3}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$DeliverLogPacketRequest;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;II[B)V

    .line 672
    .local v7, "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$DeliverLogPacketRequest;
    :try_start_0
    iget-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    const/4 v1, 0x2

    const/16 v2, 0x16

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$DeliverLogPacketRequest;->getTypes()[S

    move-result-object v3

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$DeliverLogPacketRequest;->getItems()[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qcrilhook/QmiOemHook;->sendQmiMessageAsync(SS[S[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 679
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 675
    :catch_0
    move-exception v6

    .line 676
    .local v6, "e":Ljava/io/IOException;
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v1, "IOException occurred during deliver logPacket !!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 677
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public disable(IBLandroid/os/Message;)I
    .locals 8
    .param p1, "traceId"    # I
    .param p2, "callId"    # B
    .param p3, "msg"    # Landroid/os/Message;

    .prologue
    .line 633
    new-instance v7, Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;

    invoke-direct {v7, p0, p1, p2}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;IB)V

    .line 635
    .local v7, "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;
    :try_start_0
    iget-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;->getTypes()[S

    move-result-object v3

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;->getItems()[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    move-result-object v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qcrilhook/QmiOemHook;->sendQmiMessageAsync(SS[S[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 642
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 638
    :catch_0
    move-exception v6

    .line 639
    .local v6, "e":Ljava/io/IOException;
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v1, "IOException occurred during disable !!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 640
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public declared-synchronized dispose()V
    .locals 3

    .prologue
    .line 242
    monitor-enter p0

    :try_start_0
    sget v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mRefCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mRefCount:I

    if-nez v0, :cond_0

    .line 243
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v1, "dispose(): Unregistering receiver"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/qualcomm/qcrilhook/QmiOemHook;->unregisterService(I)V

    .line 245
    invoke-static {p0}, Lcom/qualcomm/qcrilhook/QmiOemHook;->unregisterOnReadyCb(Landroid/os/Handler;)V

    .line 246
    iget-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    invoke-virtual {v0}, Lcom/qualcomm/qcrilhook/QmiOemHook;->dispose()V

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    .line 248
    const/4 v0, 0x0

    sput-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->sInstance:Lcom/qualcomm/qcrilhook/EmbmsOemHook;

    .line 249
    iget-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mRegistrants:Landroid/os/RegistrantList;

    invoke-virtual {v0}, Landroid/os/RegistrantList;->removeCleared()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 253
    :goto_0
    monitor-exit p0

    return-void

    .line 251
    :cond_0
    :try_start_1
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dispose mRefCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mRefCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public enable(ILandroid/os/Message;)I
    .locals 9
    .param p1, "traceId"    # I
    .param p2, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v8, 0x0

    .line 487
    :try_start_0
    new-instance v7, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;

    invoke-direct {v7, p0, p1}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;I)V

    .line 489
    .local v7, "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;
    iget-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;->getTypes()[S

    move-result-object v3

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;->getItems()[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    move-result-object v4

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qcrilhook/QmiOemHook;->sendQmiMessageAsync(SS[S[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v8

    .line 495
    .end local v7    # "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;
    :goto_0
    return v0

    .line 491
    :catch_0
    move-exception v6

    .line 492
    .local v6, "e":Ljava/io/IOException;
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v1, "IOException occurred during enable !!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getActiveLogPacketIDs(I[ILandroid/os/Message;)I
    .locals 8
    .param p1, "traceId"    # I
    .param p2, "supportedLogPacketIdList"    # [I
    .param p3, "msg"    # Landroid/os/Message;

    .prologue
    .line 650
    new-instance v7, Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActiveLogPacketIDsRequest;

    invoke-direct {v7, p0, p1, p2}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActiveLogPacketIDsRequest;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;I[I)V

    .line 654
    .local v7, "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActiveLogPacketIDsRequest;
    :try_start_0
    iget-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    const/4 v1, 0x2

    const/16 v2, 0x15

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActiveLogPacketIDsRequest;->getTypes()[S

    move-result-object v3

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$ActiveLogPacketIDsRequest;->getItems()[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    move-result-object v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qcrilhook/QmiOemHook;->sendQmiMessageAsync(SS[S[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 661
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 657
    :catch_0
    move-exception v6

    .line 658
    .local v6, "e":Ljava/io/IOException;
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v1, "IOException occurred during activate log packet ID\'s !!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getActiveTMGIList(IB)I
    .locals 8
    .param p1, "traceId"    # I
    .param p2, "callId"    # B

    .prologue
    .line 580
    new-instance v7, Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;

    invoke-direct {v7, p0, p1, p2}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;IB)V

    .line 583
    .local v7, "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;
    :try_start_0
    iget-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    const/4 v1, 0x2

    const/4 v2, 0x5

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;->getTypes()[S

    move-result-object v3

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;->getItems()[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qcrilhook/QmiOemHook;->sendQmiMessageAsync(SS[S[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 590
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 586
    :catch_0
    move-exception v6

    .line 587
    .local v6, "e":Ljava/io/IOException;
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v1, "IOException occurred during getActiveTMGIList !!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getAvailableTMGIList(IB)I
    .locals 8
    .param p1, "traceId"    # I
    .param p2, "callId"    # B

    .prologue
    .line 561
    new-instance v7, Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;

    invoke-direct {v7, p0, p1, p2}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;IB)V

    .line 564
    .local v7, "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;
    :try_start_0
    iget-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    const/4 v1, 0x2

    const/4 v2, 0x4

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;->getTypes()[S

    move-result-object v3

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$GenericRequest;->getItems()[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qcrilhook/QmiOemHook;->sendQmiMessageAsync(SS[S[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 571
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 567
    :catch_0
    move-exception v6

    .line 568
    .local v6, "e":Ljava/io/IOException;
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v1, "IOException occurred during getAvailableTMGIList !!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getCoverageState(I)I
    .locals 8
    .param p1, "traceId"    # I

    .prologue
    .line 601
    :try_start_0
    new-instance v7, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;

    invoke-direct {v7, p0, p1}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;I)V

    .line 603
    .local v7, "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;
    iget-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    const/4 v1, 0x2

    const/16 v2, 0x8

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;->getTypes()[S

    move-result-object v3

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;->getItems()[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qcrilhook/QmiOemHook;->sendQmiMessageAsync(SS[S[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 609
    const/4 v0, 0x0

    .end local v7    # "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;
    :goto_0
    return v0

    .line 605
    :catch_0
    move-exception v6

    .line 606
    .local v6, "e":Ljava/io/IOException;
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v1, "IOException occurred during getActiveTMGIList !!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getE911State(ILandroid/os/Message;)I
    .locals 8
    .param p1, "traceId"    # I
    .param p2, "msg"    # Landroid/os/Message;

    .prologue
    .line 762
    :try_start_0
    new-instance v7, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;

    invoke-direct {v7, p0, p1}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;I)V

    .line 764
    .local v7, "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;
    iget-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    const/4 v1, 0x2

    const/16 v2, 0x1b

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;->getTypes()[S

    move-result-object v3

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;->getItems()[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    move-result-object v4

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qcrilhook/QmiOemHook;->sendQmiMessageAsync(SS[S[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 771
    const/4 v0, 0x0

    .end local v7    # "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;
    :goto_0
    return v0

    .line 766
    :catch_0
    move-exception v6

    .line 767
    .local v6, "e":Ljava/io/IOException;
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v1, "IOException occurred during getE911State !!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getSib16CoverageStatus(Landroid/os/Message;)I
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 725
    :try_start_0
    iget-object v1, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    const/4 v2, 0x2

    const/16 v3, 0x18

    invoke-virtual {v1, v2, v3, p1}, Lcom/qualcomm/qcrilhook/QmiOemHook;->sendQmiMessageAsync(SSLandroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 731
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 727
    :catch_0
    move-exception v0

    .line 728
    .local v0, "e":Ljava/io/IOException;
    sget-object v1, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v2, "IOException occurred during getSIB16 !!!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getSignalStrength(ILandroid/os/Message;)I
    .locals 8
    .param p1, "traceId"    # I
    .param p2, "msg"    # Landroid/os/Message;

    .prologue
    .line 618
    :try_start_0
    new-instance v7, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;

    invoke-direct {v7, p0, p1}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;I)V

    .line 620
    .local v7, "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;
    iget-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    const/4 v1, 0x2

    const/16 v2, 0x9

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;->getTypes()[S

    move-result-object v3

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;->getItems()[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    move-result-object v4

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qcrilhook/QmiOemHook;->sendQmiMessageAsync(SS[S[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 626
    const/4 v0, 0x0

    .end local v7    # "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;
    :goto_0
    return v0

    .line 622
    :catch_0
    move-exception v6

    .line 623
    .local v6, "e":Ljava/io/IOException;
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v1, "IOException occurred during enable !!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 624
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getTime(ILandroid/os/Message;)I
    .locals 8
    .param p1, "traceId"    # I
    .param p2, "msg"    # Landroid/os/Message;

    .prologue
    .line 709
    :try_start_0
    new-instance v7, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;

    invoke-direct {v7, p0, p1}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;I)V

    .line 711
    .local v7, "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;
    iget-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    const/4 v1, 0x2

    const/16 v2, 0x1a

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;->getTypes()[S

    move-result-object v3

    invoke-virtual {v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;->getItems()[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    move-result-object v4

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/qualcomm/qcrilhook/QmiOemHook;->sendQmiMessageAsync(SS[S[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 717
    const/4 v0, 0x0

    .end local v7    # "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$BasicRequest;
    :goto_0
    return v0

    .line 713
    :catch_0
    move-exception v6

    .line 714
    .local v6, "e":Ljava/io/IOException;
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v1, "IOException occurred during getTime !!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 260
    sget-object v3, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "received message : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/AsyncResult;

    .line 263
    .local v0, "ar":Landroid/os/AsyncResult;
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 278
    sget-object v3, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unexpected message received from QmiOemHook what = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    :goto_0
    return-void

    .line 265
    :pswitch_0
    iget-object v1, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    check-cast v1, Ljava/util/HashMap;

    .line 266
    .local v1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    if-nez v1, :cond_0

    .line 267
    sget-object v3, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v4, "Hashmap async userobj is NULL"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 271
    :cond_0
    invoke-direct {p0, v1}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->handleResponse(Ljava/util/HashMap;)V

    goto :goto_0

    .line 274
    .end local v1    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Object;>;"
    :pswitch_1
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    .line 275
    .local v2, "payload":Ljava/lang/Object;
    const/16 v3, 0x1001

    invoke-direct {p0, v3, v2}, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->notifyUnsol(ILjava/lang/Object;)V

    goto :goto_0

    .line 263
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public registerForNotifications(Landroid/os/Handler;ILjava/lang/Object;)V
    .locals 4
    .param p1, "h"    # Landroid/os/Handler;
    .param p2, "what"    # I
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    .line 464
    new-instance v0, Landroid/os/Registrant;

    invoke-direct {v0, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    .line 465
    .local v0, "r":Landroid/os/Registrant;
    iget-object v2, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mRegistrants:Landroid/os/RegistrantList;

    monitor-enter v2

    .line 466
    :try_start_0
    sget-object v1, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Adding a registrant"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    iget-object v1, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mRegistrants:Landroid/os/RegistrantList;

    invoke-virtual {v1, v0}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    .line 468
    monitor-exit v2

    .line 469
    return-void

    .line 468
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public setTime(ZJJLandroid/os/Message;)I
    .locals 10
    .param p1, "sntpSuccess"    # Z
    .param p2, "timeMseconds"    # J
    .param p4, "timeStamp"    # J
    .param p6, "msg"    # Landroid/os/Message;

    .prologue
    .line 738
    const/4 v3, 0x0

    .line 739
    .local v3, "success":B
    if-eqz p1, :cond_0

    .line 740
    const/4 v3, 0x1

    .line 742
    :cond_0
    sget-object v2, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setTime success = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " timeMseconds = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " timeStamp = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 744
    new-instance v1, Lcom/qualcomm/qcrilhook/EmbmsOemHook$SetTimeRequest;

    move-object v2, p0

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$SetTimeRequest;-><init>(Lcom/qualcomm/qcrilhook/EmbmsOemHook;BJJ)V

    .line 746
    .local v1, "req":Lcom/qualcomm/qcrilhook/EmbmsOemHook$SetTimeRequest;
    :try_start_0
    iget-object v4, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mQmiOemHook:Lcom/qualcomm/qcrilhook/QmiOemHook;

    const/4 v5, 0x2

    const/16 v6, 0x17

    invoke-virtual {v1}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$SetTimeRequest;->getTypes()[S

    move-result-object v7

    invoke-virtual {v1}, Lcom/qualcomm/qcrilhook/EmbmsOemHook$SetTimeRequest;->getItems()[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;

    move-result-object v8

    move-object/from16 v9, p6

    invoke-virtual/range {v4 .. v9}, Lcom/qualcomm/qcrilhook/QmiOemHook;->sendQmiMessageAsync(SS[S[Lcom/qualcomm/qcrilhook/BaseQmiTypes$BaseQmiItemType;Landroid/os/Message;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 753
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 748
    :catch_0
    move-exception v0

    .line 749
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v4, "IOException occured during setTime !!!!!!"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public unregisterForNotifications(Landroid/os/Handler;)V
    .locals 3
    .param p1, "h"    # Landroid/os/Handler;

    .prologue
    .line 476
    iget-object v1, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mRegistrants:Landroid/os/RegistrantList;

    monitor-enter v1

    .line 477
    :try_start_0
    sget-object v0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Removing a registrant"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    iget-object v0, p0, Lcom/qualcomm/qcrilhook/EmbmsOemHook;->mRegistrants:Landroid/os/RegistrantList;

    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    .line 479
    monitor-exit v1

    .line 480
    return-void

    .line 479
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
