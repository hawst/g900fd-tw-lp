.class public Lcom/samsung/android/smartface/SmartFaceService;
.super Lcom/samsung/android/smartface/ISmartFaceService$Stub;
.source "SmartFaceService.java"

# interfaces
.implements Lcom/samsung/android/smartface/CameraController$CameraListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/smartface/SmartFaceService$Logger;,
        Lcom/samsung/android/smartface/SmartFaceService$Client;,
        Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;,
        Lcom/samsung/android/smartface/SmartFaceService$SensorListener;
    }
.end annotation


# static fields
.field private static final DB_FACE_SCROLL:Ljava/lang/String; = "face_smart_scroll"

.field private static final DB_MUTE_PAUSE:Ljava/lang/String; = "motion_merged_mute_pause"

.field private static final DB_SCREEN_ROTATION:Ljava/lang/String; = "accelerometer_rotation"

.field private static final DB_SMART_PAUSE:Ljava/lang/String; = "smart_pause"

.field private static final DB_SMART_ROTATION:Ljava/lang/String; = "intelligent_rotation_mode"

.field private static final DB_SMART_SCREEN:Ljava/lang/String; = "intelligent_screen_mode"

.field private static final DB_SMART_SCROLL:Ljava/lang/String; = "smart_scroll"

.field private static final DB_SMART_SLEEP:Ljava/lang/String; = "intelligent_sleep_mode"

.field public static final DEBUG:Z = true

.field private static final LOW_LIGHT_THRESHOLD:F = 10.0f

.field public static final UPDATE_COVER:I = 0x1

.field public static mDumpPreview:Z

.field private static mSensor:Landroid/hardware/Sensor;

.field private static mSensorManager:Landroid/hardware/SensorManager;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mActiveClientNumber:I

.field private mActiveServiceType:I

.field mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field mCameraController:Lcom/samsung/android/smartface/CameraController;

.field private mChangedByDock:Z

.field private mClearCoverOpened:Z

.field private mClientList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/os/IBinder;",
            "Lcom/samsung/android/smartface/SmartFaceService$Client;",
            ">;"
        }
    .end annotation
.end field

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mCoverManager:Lcom/samsung/android/cover/CoverManager;

.field private mCoverStateListener:Lcom/samsung/android/cover/CoverManager$StateListener;

.field private mFaceScrollState:Z

.field private mFolderCoverOpened:Z

.field private mHasSmartPause:Z

.field private mHasSmartRotation:Z

.field private mHasSmartScroll:Z

.field private mHasSmartStay:Z

.field private mLastClient:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Lcom/samsung/android/smartface/SmartFaceService$Client;",
            ">;"
        }
    .end annotation
.end field

.field private mLastServiecTypeFromClient:I

.field private mLightIntensityEnough:Z

.field private mListenerHandler:Landroid/os/Handler;

.field private mListenerThread:Landroid/os/HandlerThread;

.field private final mLock:Ljava/lang/Object;

.field private mLux:F

.field private mMultiWindowEnabled:Z

.field private mPauseEnabled:Z

.field private mRotationEnabled:Z

.field private mScrollEnabled:Z

.field mScrollPauseWorker:Lcom/samsung/android/smartface/ScrollPauseWorker;

.field private mSensorListener:Lcom/samsung/android/smartface/SmartFaceService$SensorListener;

.field mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

.field private mSmartPauseClientNumber:I

.field private mSmartScreenObserver:Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;

.field private mStayEnabled:Z

.field mStayRotationWorker:Lcom/samsung/android/smartface/StayRotationWorker;

.field private mSystemReady:Z

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field protected mWorkers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/smartface/Worker;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/smartface/SmartFaceService;->mDumpPreview:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 465
    invoke-direct {p0}, Lcom/samsung/android/smartface/ISmartFaceService$Stub;-><init>()V

    .line 93
    const-string v1, "SmartFaceService"

    iput-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->TAG:Ljava/lang/String;

    .line 141
    iput-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mStayRotationWorker:Lcom/samsung/android/smartface/StayRotationWorker;

    .line 142
    iput-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mScrollPauseWorker:Lcom/samsung/android/smartface/ScrollPauseWorker;

    .line 147
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;

    .line 158
    iput-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    .line 159
    iput-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCoverStateListener:Lcom/samsung/android/cover/CoverManager$StateListener;

    .line 354
    new-instance v1, Lcom/samsung/android/smartface/SmartFaceService$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/smartface/SmartFaceService$1;-><init>(Lcom/samsung/android/smartface/SmartFaceService;)V

    iput-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 467
    iput-object p1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    .line 468
    iput v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveClientNumber:I

    .line 469
    iput v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartPauseClientNumber:I

    .line 470
    iput v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastServiecTypeFromClient:I

    .line 471
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClientList:Ljava/util/HashMap;

    .line 472
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWorkers:Ljava/util/ArrayList;

    .line 473
    new-instance v1, Ljava/util/ArrayDeque;

    invoke-direct {v1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastClient:Ljava/util/ArrayDeque;

    .line 475
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "SmartFaceService Listener Handler"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mListenerThread:Landroid/os/HandlerThread;

    .line 476
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mListenerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 477
    new-instance v1, Lcom/samsung/android/smartface/SmartFaceService$2;

    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mListenerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/smartface/SmartFaceService$2;-><init>(Lcom/samsung/android/smartface/SmartFaceService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mListenerHandler:Landroid/os/Handler;

    .line 495
    new-instance v1, Lcom/samsung/android/smartface/CameraController;

    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p0}, Lcom/samsung/android/smartface/CameraController;-><init>(Landroid/content/Context;Lcom/samsung/android/smartface/CameraController$CameraListener;)V

    iput-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCameraController:Lcom/samsung/android/smartface/CameraController;

    .line 497
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 498
    .local v0, "pm":Landroid/os/PowerManager;
    const/4 v1, 0x1

    const-string v2, "SmartFaceService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 499
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContentResolver:Landroid/content/ContentResolver;

    .line 501
    new-instance v1, Lcom/samsung/android/smartface/SmartFaceIndicator;

    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/smartface/SmartFaceIndicator;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    .line 503
    new-instance v1, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;

    invoke-direct {v1, p0}, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;-><init>(Lcom/samsung/android/smartface/SmartFaceService;)V

    iput-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartScreenObserver:Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;

    .line 504
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "smart_scroll"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartScreenObserver:Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 506
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "motion_merged_mute_pause"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartScreenObserver:Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 508
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "smart_pause"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartScreenObserver:Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 510
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "intelligent_sleep_mode"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartScreenObserver:Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 512
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "intelligent_rotation_mode"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartScreenObserver:Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 514
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accelerometer_rotation"

    invoke-static {v2}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartScreenObserver:Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;

    invoke-virtual {v1, v2, v4, v3, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 517
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    const-string v2, "sensor"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    sput-object v1, Lcom/samsung/android/smartface/SmartFaceService;->mSensorManager:Landroid/hardware/SensorManager;

    .line 518
    sget-object v1, Lcom/samsung/android/smartface/SmartFaceService;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/smartface/SmartFaceService;->mSensor:Landroid/hardware/Sensor;

    .line 520
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.smartface.smart_stay"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mHasSmartStay:Z

    .line 521
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.smartface.smart_rotation"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mHasSmartRotation:Z

    .line 522
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.smartface.smart_scroll"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mHasSmartScroll:Z

    .line 523
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.android.smartface.smart_pause"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mHasSmartPause:Z

    .line 525
    invoke-direct {p0}, Lcom/samsung/android/smartface/SmartFaceService;->initialize()V

    .line 527
    const-string v1, "SmartFaceService"

    const-string v2, "SmartFaceService Started."

    invoke-static {v1, v2, v4}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 528
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/smartface/SmartFaceService;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    iget v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLux:F

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/smartface/SmartFaceService;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;
    .param p1, "x1"    # F

    .prologue
    .line 92
    iput p1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLux:F

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/smartface/SmartFaceService;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/smartface/SmartFaceService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/android/smartface/SmartFaceService;->updateActiveServiceTypeLocked()V

    return-void
.end method

.method static synthetic access$1102(Lcom/samsung/android/smartface/SmartFaceService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSystemReady:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/samsung/android/smartface/SmartFaceService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mMultiWindowEnabled:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/samsung/android/smartface/SmartFaceService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClearCoverOpened:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/samsung/android/smartface/SmartFaceService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClearCoverOpened:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/samsung/android/smartface/SmartFaceService;)Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartScreenObserver:Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/smartface/SmartFaceService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mFaceScrollState:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/samsung/android/smartface/SmartFaceService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mFaceScrollState:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/samsung/android/smartface/SmartFaceService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mChangedByDock:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/samsung/android/smartface/SmartFaceService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mChangedByDock:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/samsung/android/smartface/SmartFaceService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mFolderCoverOpened:Z

    return v0
.end method

.method static synthetic access$1702(Lcom/samsung/android/smartface/SmartFaceService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mFolderCoverOpened:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/smartface/SmartFaceService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLightIntensityEnough:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/android/smartface/SmartFaceService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLightIntensityEnough:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/smartface/SmartFaceService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/android/smartface/SmartFaceService;->updateClientStatusLocked()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/smartface/SmartFaceService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mListenerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/smartface/SmartFaceService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mScrollEnabled:Z

    return v0
.end method

.method static synthetic access$502(Lcom/samsung/android/smartface/SmartFaceService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mScrollEnabled:Z

    return p1
.end method

.method static synthetic access$600(Lcom/samsung/android/smartface/SmartFaceService;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/smartface/SmartFaceService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mPauseEnabled:Z

    return v0
.end method

.method static synthetic access$702(Lcom/samsung/android/smartface/SmartFaceService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mPauseEnabled:Z

    return p1
.end method

.method static synthetic access$800(Lcom/samsung/android/smartface/SmartFaceService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mStayEnabled:Z

    return v0
.end method

.method static synthetic access$802(Lcom/samsung/android/smartface/SmartFaceService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mStayEnabled:Z

    return p1
.end method

.method static synthetic access$900(Lcom/samsung/android/smartface/SmartFaceService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mRotationEnabled:Z

    return v0
.end method

.method static synthetic access$902(Lcom/samsung/android/smartface/SmartFaceService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mRotationEnabled:Z

    return p1
.end method

.method private getAppName(I)Ljava/lang/String;
    .locals 7
    .param p1, "pID"    # I

    .prologue
    .line 1013
    const-string v4, ""

    .line 1014
    .local v4, "processName":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    const-string v6, "activity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1015
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v3

    .line 1017
    .local v3, "l":Ljava/util/List;
    if-eqz v3, :cond_1

    .line 1019
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1021
    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1023
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningAppProcessInfo;

    move-object v2, v5

    check-cast v2, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 1024
    .local v2, "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v5, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v5, p1, :cond_0

    .line 1026
    iget-object v4, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    goto :goto_0

    .line 1030
    .end local v1    # "i":Ljava/util/Iterator;
    .end local v2    # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    return-object v4
.end method

.method private initialize()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 532
    new-instance v2, Lcom/samsung/android/smartface/SmartFaceService$SensorListener;

    invoke-direct {v2, p0, v7}, Lcom/samsung/android/smartface/SmartFaceService$SensorListener;-><init>(Lcom/samsung/android/smartface/SmartFaceService;Lcom/samsung/android/smartface/SmartFaceService$1;)V

    iput-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSensorListener:Lcom/samsung/android/smartface/SmartFaceService$SensorListener;

    .line 534
    iput-boolean v6, p0, Lcom/samsung/android/smartface/SmartFaceService;->mMultiWindowEnabled:Z

    .line 535
    iput-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClearCoverOpened:Z

    .line 536
    iput-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mFolderCoverOpened:Z

    .line 537
    iput-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLightIntensityEnough:Z

    .line 538
    iput-boolean v6, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSystemReady:Z

    .line 540
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 541
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v2, "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 542
    const-string v2, "com.samsung.cover.OPEN"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 543
    const-string v2, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 544
    const-string v2, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 545
    const-string v2, "android.intent.action.DOCK_EVENT"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 546
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 548
    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mListenerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3, v0, v7, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 551
    new-instance v2, Lcom/samsung/android/cover/CoverManager;

    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/samsung/android/cover/CoverManager;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    .line 552
    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    if-eqz v2, :cond_0

    .line 553
    new-instance v2, Lcom/samsung/android/smartface/SmartFaceService$3;

    invoke-direct {v2, p0}, Lcom/samsung/android/smartface/SmartFaceService$3;-><init>(Lcom/samsung/android/smartface/SmartFaceService;)V

    iput-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCoverStateListener:Lcom/samsung/android/cover/CoverManager$StateListener;

    .line 569
    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCoverStateListener:Lcom/samsung/android/cover/CoverManager$StateListener;

    invoke-virtual {v2, v3}, Lcom/samsung/android/cover/CoverManager;->registerListener(Lcom/samsung/android/cover/CoverManager$StateListener;)V

    .line 571
    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    invoke-virtual {v2}, Lcom/samsung/android/cover/CoverManager;->getCoverState()Lcom/samsung/android/cover/CoverState;

    move-result-object v1

    .line 572
    .local v1, "state":Lcom/samsung/android/cover/CoverState;
    if-eqz v1, :cond_0

    .line 574
    invoke-virtual {v1}, Lcom/samsung/android/cover/CoverState;->getSwitchState()Z

    move-result v2

    if-ne v2, v5, :cond_1

    .line 575
    iput-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClearCoverOpened:Z

    .line 580
    :goto_0
    const-string v2, "SmartFaceService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "from initialize -> mClearCoverOpened: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClearCoverOpened:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v5}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 585
    .end local v1    # "state":Lcom/samsung/android/cover/CoverState;
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/smartface/SmartFaceService;->loadWorkers()V

    .line 587
    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartScreenObserver:Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;

    invoke-virtual {v2, v5}, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->onChange(Z)V

    .line 588
    return-void

    .line 578
    .restart local v1    # "state":Lcom/samsung/android/cover/CoverState;
    :cond_1
    iput-boolean v6, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClearCoverOpened:Z

    goto :goto_0
.end method

.method private loadWorkers()V
    .locals 7

    .prologue
    .line 591
    iget-boolean v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mHasSmartPause:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mHasSmartScroll:Z

    if-eqz v1, :cond_1

    .line 593
    :cond_0
    new-instance v0, Lcom/samsung/android/smartface/ScrollPauseWorker;

    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    const-string v2, "Scroll/Pause Worker"

    const/16 v3, 0x33

    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    iget-object v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCameraController:Lcom/samsung/android/smartface/CameraController;

    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService;->mListenerHandler:Landroid/os/Handler;

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/smartface/ScrollPauseWorker;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/samsung/android/smartface/SmartFaceIndicator;Lcom/samsung/android/smartface/CameraController;Landroid/os/Handler;)V

    .line 594
    .local v0, "w1":Lcom/samsung/android/smartface/ScrollPauseWorker;
    iput-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mScrollPauseWorker:Lcom/samsung/android/smartface/ScrollPauseWorker;

    .line 595
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWorkers:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 598
    .end local v0    # "w1":Lcom/samsung/android/smartface/ScrollPauseWorker;
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mHasSmartStay:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mHasSmartRotation:Z

    if-eqz v1, :cond_3

    .line 600
    :cond_2
    new-instance v0, Lcom/samsung/android/smartface/StayRotationWorker;

    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    const-string v2, "Stay/Rotation Worker"

    const/16 v3, 0xc

    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    iget-object v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCameraController:Lcom/samsung/android/smartface/CameraController;

    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService;->mListenerHandler:Landroid/os/Handler;

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/smartface/StayRotationWorker;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/samsung/android/smartface/SmartFaceIndicator;Lcom/samsung/android/smartface/CameraController;Landroid/os/Handler;)V

    .line 601
    .local v0, "w1":Lcom/samsung/android/smartface/StayRotationWorker;
    iput-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mStayRotationWorker:Lcom/samsung/android/smartface/StayRotationWorker;

    .line 602
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWorkers:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 605
    .end local v0    # "w1":Lcom/samsung/android/smartface/StayRotationWorker;
    :cond_3
    return-void
.end method

.method private updateActiveServiceType()V
    .locals 2

    .prologue
    .line 608
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 610
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/smartface/SmartFaceService;->updateActiveServiceTypeLocked()V

    .line 611
    monitor-exit v1

    .line 612
    return-void

    .line 611
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private updateActiveServiceTypeLocked()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 616
    iput v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    .line 618
    iget-boolean v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClearCoverOpened:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mFolderCoverOpened:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSystemReady:Z

    if-eqz v2, :cond_3

    .line 619
    iget-boolean v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mScrollEnabled:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mMultiWindowEnabled:Z

    if-nez v2, :cond_0

    .line 621
    iget v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    .line 622
    iget v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    .line 623
    iget v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    .line 625
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mPauseEnabled:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mMultiWindowEnabled:Z

    if-nez v2, :cond_1

    iget v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    .line 626
    :cond_1
    iget-boolean v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mStayEnabled:Z

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    .line 627
    :cond_2
    iget-boolean v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mRotationEnabled:Z

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    .line 630
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWorkers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/smartface/Worker;

    .line 631
    .local v1, "worker":Lcom/samsung/android/smartface/Worker;
    iget v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/smartface/Worker;->setActiveServiceType(I)V

    goto :goto_0

    .line 634
    .end local v1    # "worker":Lcom/samsung/android/smartface/Worker;
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    iget v3, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    invoke-virtual {v2, v3}, Lcom/samsung/android/smartface/SmartFaceIndicator;->onSettingChanged(I)V

    .line 636
    const-string v2, "SmartFaceService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mActiveServiceType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    invoke-static {v4}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v5}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 638
    return-void
.end method

.method private updateClientStatus()V
    .locals 2

    .prologue
    .line 641
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 642
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/smartface/SmartFaceService;->updateClientStatusLocked()V

    .line 643
    monitor-exit v1

    .line 644
    return-void

    .line 643
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private updateClientStatusLocked()V
    .locals 14

    .prologue
    .line 648
    const/4 v0, 0x0

    .line 649
    .local v0, "ActiveClients":I
    const/4 v1, 0x0

    .line 650
    .local v1, "SmartPauseClients":I
    const/4 v6, 0x0

    .line 651
    .local v6, "service_type":I
    const/4 v7, -0x1

    .line 653
    .local v7, "service_type_mask":I
    iget-boolean v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLightIntensityEnough:Z

    if-nez v9, :cond_0

    const/4 v7, -0x3

    .line 655
    :cond_0
    const-string v9, "SmartFaceService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mLightIntensityEnough: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-boolean v11, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLightIntensityEnough:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " mLux: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLux:F

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 658
    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWorkers:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/smartface/Worker;

    .line 659
    .local v8, "worker":Lcom/samsung/android/smartface/Worker;
    invoke-virtual {v8}, Lcom/samsung/android/smartface/Worker;->clearMessages()V

    .line 660
    invoke-virtual {v8}, Lcom/samsung/android/smartface/Worker;->clearClients()V

    goto :goto_0

    .line 663
    .end local v8    # "worker":Lcom/samsung/android/smartface/Worker;
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClientList:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/IBinder;

    .line 665
    .local v3, "client_key":Landroid/os/IBinder;
    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClientList:Ljava/util/HashMap;

    invoke-virtual {v9, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/smartface/SmartFaceService$Client;

    .line 668
    .local v2, "client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    invoke-virtual {v2}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v9

    and-int/2addr v9, v7

    iget v10, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    and-int/2addr v9, v10

    if-eqz v9, :cond_3

    .line 670
    add-int/lit8 v0, v0, 0x1

    .line 671
    invoke-virtual {v2}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v9

    iget v10, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    and-int/2addr v9, v10

    or-int/2addr v6, v9

    .line 673
    :cond_3
    invoke-virtual {v2}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v9

    and-int/lit8 v9, v9, 0x2

    if-eqz v9, :cond_4

    .line 675
    add-int/lit8 v1, v1, 0x1

    .line 678
    :cond_4
    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWorkers:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/smartface/Worker;

    .line 679
    .restart local v8    # "worker":Lcom/samsung/android/smartface/Worker;
    invoke-virtual {v8}, Lcom/samsung/android/smartface/Worker;->getSupportedServiceType()I

    move-result v9

    iget v10, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    and-int/2addr v9, v10

    invoke-virtual {v2}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v10

    and-int/2addr v9, v10

    if-eqz v9, :cond_5

    .line 680
    invoke-virtual {v8, v2}, Lcom/samsung/android/smartface/Worker;->addClient(Lcom/samsung/android/smartface/SmartFaceService$Client;)V

    goto :goto_1

    .line 686
    .end local v2    # "client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    .end local v3    # "client_key":Landroid/os/IBinder;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v8    # "worker":Lcom/samsung/android/smartface/Worker;
    :cond_6
    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWorkers:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/smartface/Worker;

    .line 687
    .restart local v8    # "worker":Lcom/samsung/android/smartface/Worker;
    invoke-virtual {v8}, Lcom/samsung/android/smartface/Worker;->updateClientsDone()V

    goto :goto_2

    .line 690
    .end local v8    # "worker":Lcom/samsung/android/smartface/Worker;
    :cond_7
    const-string v9, "SmartFaceService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Service Type to Worker: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v6}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 691
    const-string v9, "SmartFaceService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Last Active clients:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveClientNumber:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " Current Active clients: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 692
    const-string v9, "SmartFaceService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Last Smart Pause clients: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartPauseClientNumber:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " Current Smart Pause clients: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 694
    iget v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartPauseClientNumber:I

    if-nez v9, :cond_b

    if-lez v1, :cond_b

    .line 696
    const/high16 v9, 0x7fc00000    # NaNf

    iput v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLux:F

    .line 697
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLightIntensityEnough:Z

    .line 698
    sget-object v9, Lcom/samsung/android/smartface/SmartFaceService;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v10, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSensorListener:Lcom/samsung/android/smartface/SmartFaceService$SensorListener;

    sget-object v11, Lcom/samsung/android/smartface/SmartFaceService;->mSensor:Landroid/hardware/Sensor;

    const/4 v12, 0x3

    iget-object v13, p0, Lcom/samsung/android/smartface/SmartFaceService;->mListenerHandler:Landroid/os/Handler;

    invoke-virtual {v9, v10, v11, v12, v13}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 705
    :cond_8
    :goto_3
    iget v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveClientNumber:I

    if-nez v9, :cond_c

    if-lez v0, :cond_c

    .line 707
    const-string v9, "SmartFaceService"

    const-string v10, "We have active clients. open camera"

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 709
    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v9}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v9

    if-nez v9, :cond_9

    .line 710
    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v9}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 711
    const-string v9, "SmartFaceService"

    const-string v10, "mWakeLock.acquire() in FD"

    const/4 v11, 0x1

    invoke-static {v9, v10, v11}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 713
    :cond_9
    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/samsung/android/smartface/SmartFaceIndicator;->onCameraStatusChanged(Z)V

    .line 714
    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCameraController:Lcom/samsung/android/smartface/CameraController;

    invoke-virtual {v9, v6}, Lcom/samsung/android/smartface/CameraController;->openCamera(I)V

    .line 733
    :cond_a
    :goto_4
    iput v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveClientNumber:I

    .line 734
    iput v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartPauseClientNumber:I

    .line 736
    return-void

    .line 700
    :cond_b
    iget v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartPauseClientNumber:I

    if-lez v9, :cond_8

    if-nez v1, :cond_8

    .line 702
    sget-object v9, Lcom/samsung/android/smartface/SmartFaceService;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v10, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSensorListener:Lcom/samsung/android/smartface/SmartFaceService$SensorListener;

    invoke-virtual {v9, v10}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    goto :goto_3

    .line 716
    :cond_c
    iget v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveClientNumber:I

    if-lez v9, :cond_d

    if-nez v0, :cond_d

    .line 718
    const-string v9, "SmartFaceService"

    const-string v10, "No active clients. close camera"

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 720
    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCameraController:Lcom/samsung/android/smartface/CameraController;

    invoke-virtual {v9}, Lcom/samsung/android/smartface/CameraController;->closeCamera()V

    .line 721
    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/samsung/android/smartface/SmartFaceIndicator;->onCameraStatusChanged(Z)V

    .line 722
    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/samsung/android/smartface/SmartFaceIndicator;->setWakeLock(Z)V

    .line 723
    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v9}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v9

    if-eqz v9, :cond_a

    .line 724
    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v9}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 725
    const-string v9, "SmartFaceService"

    const-string v10, "mWakeLock.release() in FD"

    const/4 v11, 0x1

    invoke-static {v9, v10, v11}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_4

    .line 728
    :cond_d
    if-lez v0, :cond_a

    .line 730
    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCameraController:Lcom/samsung/android/smartface/CameraController;

    invoke-virtual {v9, v6}, Lcom/samsung/android/smartface/CameraController;->updateCameraMode(I)V

    goto :goto_4
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 7
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 946
    const-string v4, "SmartFaceService"

    const-string v5, "DUMP SmartFaceService."

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 948
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mContext:Landroid/content/Context;

    const-string v5, "android.permission.DUMP"

    invoke-virtual {v4, v5}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_0

    .line 949
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Permission Denial: can\'t dump SmartFaceService from from pid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", uid="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " without permission "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "android.permission.DUMP"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1007
    :goto_0
    return-void

    .line 955
    :cond_0
    const-string v4, "SmartFace service is up and running!"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 956
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " # of clients: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClientList:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 957
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " # of active clients: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveClientNumber:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 959
    const-string v4, "\n Vision Camera: true"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 962
    const-string v4, " Method: qualcomm: intelligent-mode and no-display"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 963
    const-string v4, " Size: 320x180"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 964
    const-string v4, " Size2: 480x272"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 967
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "\n # of Workers: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWorkers:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 968
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWorkers:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/smartface/Worker;

    .line 970
    .local v3, "worker":Lcom/samsung/android/smartface/Worker;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") supports "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3}, Lcom/samsung/android/smartface/Worker;->getSupportedServiceType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 971
    invoke-virtual {v3, p2}, Lcom/samsung/android/smartface/Worker;->dump(Ljava/io/PrintWriter;)V

    .line 972
    const-string v4, ""

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 976
    .end local v3    # "worker":Lcom/samsung/android/smartface/Worker;
    :cond_1
    const-string v4, "SmartFaceService State:"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 977
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mScrollEnabled: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mScrollEnabled:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 978
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mPauseEnabled: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mPauseEnabled:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 979
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mStayEnabled: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mStayEnabled:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 980
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mRotationEnabled: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mRotationEnabled:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 981
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mMultiWindowEnabled: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mMultiWindowEnabled:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 982
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mClearCoverOpened: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClearCoverOpened:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 983
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mFolderCoverOpened: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mFolderCoverOpened:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 984
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mChangedByDock: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mChangedByDock:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 985
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mFaceScrollState: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mFaceScrollState:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 986
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mLux: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLux:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 988
    const-string v4, "\nSmartFaceService System Feature:"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 989
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " has Smart Stay: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mHasSmartStay:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 990
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " has Smart Rotation: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mHasSmartRotation:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 991
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " has Smart Scroll: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mHasSmartScroll:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 992
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " has Smart Pause: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mHasSmartPause:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 994
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClientList:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    .line 995
    .local v0, "client":Landroid/os/IBinder;
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClientList:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/smartface/SmartFaceService$Client;

    .line 996
    .local v2, "new_client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    if-eqz v2, :cond_2

    invoke-virtual {v2, p2}, Lcom/samsung/android/smartface/SmartFaceService$Client;->dump(Ljava/io/PrintWriter;)V

    goto :goto_2

    .line 999
    .end local v0    # "client":Landroid/os/IBinder;
    .end local v2    # "new_client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCameraController:Lcom/samsung/android/smartface/CameraController;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCameraController:Lcom/samsung/android/smartface/CameraController;

    invoke-virtual {v4}, Lcom/samsung/android/smartface/CameraController;->isOpened()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1000
    const-string v4, "\n Camera is opened"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1004
    :goto_3
    const-string v4, "\n"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1005
    invoke-static {p2}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->dumpLog(Ljava/io/PrintWriter;)V

    goto/16 :goto_0

    .line 1002
    :cond_4
    const-string v4, "\n Camera is not opened"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public getSupportedServices()I
    .locals 4

    .prologue
    .line 936
    const/4 v1, 0x0

    .line 938
    .local v1, "services":I
    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWorkers:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/smartface/Worker;

    .line 939
    .local v2, "worker":Lcom/samsung/android/smartface/Worker;
    invoke-virtual {v2}, Lcom/samsung/android/smartface/Worker;->getSupportedServiceType()I

    move-result v3

    or-int/2addr v1, v3

    goto :goto_0

    .line 941
    .end local v2    # "worker":Lcom/samsung/android/smartface/Worker;
    :cond_0
    return v1
.end method

.method public onCallbackReset()V
    .locals 5

    .prologue
    .line 776
    const-string v2, "SmartFaceService"

    const-string v3, "Clear worker messages"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 778
    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWorkers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/smartface/Worker;

    .line 779
    .local v1, "worker":Lcom/samsung/android/smartface/Worker;
    invoke-virtual {v1}, Lcom/samsung/android/smartface/Worker;->clearMessages()V

    goto :goto_0

    .line 781
    .end local v1    # "worker":Lcom/samsung/android/smartface/Worker;
    :cond_0
    return-void
.end method

.method public onError(I)V
    .locals 4
    .param p1, "error"    # I

    .prologue
    .line 766
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 767
    :try_start_0
    iget v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveClientNumber:I

    if-eqz v0, :cond_0

    .line 768
    const-string v0, "SmartFaceService"

    const-string v2, "Whatever happened to camera service, I will try to re-open camera! I have an active client waiting for my service."

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 769
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService;->mCameraController:Lcom/samsung/android/smartface/CameraController;

    invoke-virtual {v0}, Lcom/samsung/android/smartface/CameraController;->openCameraAsync()V

    .line 771
    :cond_0
    monitor-exit v1

    .line 772
    return-void

    .line 771
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onPreviewFrame([B)Z
    .locals 9
    .param p1, "data"    # [B

    .prologue
    const/4 v8, 0x0

    .line 743
    invoke-static {}, Lcom/android/internal/policy/impl/WindowOrientationListener;->getCurrentRotation()I

    move-result v0

    .line 744
    .local v0, "UI_orientation":I
    const/4 v2, 0x0

    .line 746
    .local v2, "needToAddBuffer":Z
    const-string v4, "SmartFaceService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Orientation: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    iget-boolean v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mScrollEnabled:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mPauseEnabled:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mStayEnabled:Z

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mRotationEnabled:Z

    if-eqz v4, :cond_2

    .line 749
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService;->mWorkers:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/smartface/Worker;

    .line 750
    .local v3, "worker":Lcom/samsung/android/smartface/Worker;
    const-string v4, "SmartFaceService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Calling onPreviewFrame: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 754
    invoke-virtual {v3, p1, v0}, Lcom/samsung/android/smartface/Worker;->onPreviewFrame([BI)V

    goto :goto_0

    .line 756
    .end local v3    # "worker":Lcom/samsung/android/smartface/Worker;
    :cond_1
    const/4 v2, 0x1

    .line 761
    .end local v1    # "i$":Ljava/util/Iterator;
    :goto_1
    return v2

    .line 758
    :cond_2
    const-string v4, "SmartFaceService"

    const-string v5, "smart face service is disabled."

    invoke-static {v4, v5, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method public register(Lcom/samsung/android/smartface/ISmartFaceClient;I)Z
    .locals 12
    .param p1, "client"    # Lcom/samsung/android/smartface/ISmartFaceClient;
    .param p2, "serviceType"    # I

    .prologue
    .line 791
    const-string v1, "SmartFaceService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "register:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    invoke-virtual {v2, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 793
    const/4 v7, 0x0

    .line 795
    .local v7, "ret":Z
    invoke-static {}, Lcom/samsung/android/smartface/SmartFaceService;->getCallingPid()I

    move-result v5

    .line 796
    .local v5, "calling_pid":I
    invoke-static {}, Lcom/samsung/android/smartface/SmartFaceService;->clearCallingIdentity()J

    move-result-wide v8

    .line 798
    .local v8, "identityToken":J
    const-string v1, "SmartFaceService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "register, serviceType("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") pid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v5}, Lcom/samsung/android/smartface/SmartFaceService;->getAppName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 800
    iget-object v10, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;

    monitor-enter v10

    .line 802
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClientList:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/samsung/android/smartface/ISmartFaceClient;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/smartface/SmartFaceService$Client;

    .line 804
    .local v0, "new_client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    if-nez v0, :cond_5

    .line 805
    new-instance v0, Lcom/samsung/android/smartface/SmartFaceService$Client;

    .end local v0    # "new_client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    invoke-direct {p0, v5}, Lcom/samsung/android/smartface/SmartFaceService;->getAppName(I)Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p0

    move v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/smartface/SmartFaceService$Client;-><init>(Lcom/samsung/android/smartface/SmartFaceService;Lcom/samsung/android/smartface/ISmartFaceClient;Lcom/samsung/android/smartface/SmartFaceService;IILjava/lang/String;)V

    .line 806
    .restart local v0    # "new_client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mScrollPauseWorker:Lcom/samsung/android/smartface/ScrollPauseWorker;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mScrollPauseWorker:Lcom/samsung/android/smartface/ScrollPauseWorker;

    invoke-virtual {v1}, Lcom/samsung/android/smartface/ScrollPauseWorker;->getSupportedServiceType()I

    move-result v1

    and-int/2addr v1, p2

    if-eqz v1, :cond_1

    .line 808
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastClient:Ljava/util/ArrayDeque;

    invoke-virtual {v1, v0}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 810
    and-int/lit8 v1, p2, 0x10

    if-eqz v1, :cond_2

    const/16 v1, 0x10

    iput v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastServiecTypeFromClient:I

    .line 815
    :cond_0
    :goto_0
    const-string v1, "SmartFaceService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "send LastServiecTypeFromClient to scroll pause worker: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastServiecTypeFromClient:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 817
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mScrollPauseWorker:Lcom/samsung/android/smartface/ScrollPauseWorker;

    iget v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastServiecTypeFromClient:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/smartface/ScrollPauseWorker;->setLastServiecTypeFromClient(I)V

    .line 819
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClientList:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/samsung/android/smartface/ISmartFaceClient;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    invoke-direct {p0}, Lcom/samsung/android/smartface/SmartFaceService;->updateClientStatusLocked()V

    .line 822
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/smartface/SmartFaceService$Client;->notifyRegisterStatus(Z)V

    .line 828
    :goto_1
    invoke-virtual {v0}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v1

    iget v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mActiveServiceType:I

    and-int/2addr v1, v2

    if-eqz v1, :cond_6

    const/4 v7, 0x1

    .line 829
    :goto_2
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 830
    invoke-static {v8, v9}, Lcom/samsung/android/smartface/SmartFaceService;->restoreCallingIdentity(J)V

    .line 832
    return v7

    .line 811
    :cond_2
    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :try_start_1
    iput v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastServiecTypeFromClient:I

    goto :goto_0

    .line 829
    .end local v0    # "new_client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    :catchall_0
    move-exception v1

    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 812
    .restart local v0    # "new_client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    :cond_3
    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_4

    const/4 v1, 0x2

    :try_start_2
    iput v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastServiecTypeFromClient:I

    goto :goto_0

    .line 813
    :cond_4
    and-int/lit8 v1, p2, 0x20

    if-eqz v1, :cond_0

    const/16 v1, 0x20

    iput v1, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastServiecTypeFromClient:I

    goto :goto_0

    .line 825
    :cond_5
    const-string v1, "SmartFaceService"

    const-string v2, "Duplicated connection from same client. Ignore"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 828
    :cond_6
    const/4 v7, 0x0

    goto :goto_2
.end method

.method public registerAsync(Lcom/samsung/android/smartface/ISmartFaceClient;I)V
    .locals 4
    .param p1, "client"    # Lcom/samsung/android/smartface/ISmartFaceClient;
    .param p2, "serviceType"    # I

    .prologue
    .line 785
    const-string v0, "SmartFaceService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerAsync:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 786
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/smartface/SmartFaceService;->register(Lcom/samsung/android/smartface/ISmartFaceClient;I)Z

    .line 787
    return-void
.end method

.method public setValue(Lcom/samsung/android/smartface/ISmartFaceClient;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "client"    # Lcom/samsung/android/smartface/ISmartFaceClient;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 884
    const-string v2, "SmartFaceService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setValue, key: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " value: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v5}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 888
    const-string v2, "smart-rotation-ui-orientation"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 890
    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 898
    .local v1, "ui_orientation":I
    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mStayRotationWorker:Lcom/samsung/android/smartface/StayRotationWorker;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mStayRotationWorker:Lcom/samsung/android/smartface/StayRotationWorker;

    invoke-virtual {v2, v1}, Lcom/samsung/android/smartface/StayRotationWorker;->setSmartRotationUIOrientation(I)V

    .line 931
    .end local v1    # "ui_orientation":I
    :cond_0
    :goto_0
    return-void

    .line 901
    :cond_1
    const-string v2, "smart-stay-framecount-reset"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 903
    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mStayRotationWorker:Lcom/samsung/android/smartface/StayRotationWorker;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mStayRotationWorker:Lcom/samsung/android/smartface/StayRotationWorker;

    invoke-virtual {v2}, Lcom/samsung/android/smartface/StayRotationWorker;->frameCountReset()V

    goto :goto_0

    .line 905
    :cond_2
    const-string v2, "smart-scroll-page-status"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 907
    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mScrollPauseWorker:Lcom/samsung/android/smartface/ScrollPauseWorker;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mScrollPauseWorker:Lcom/samsung/android/smartface/ScrollPauseWorker;

    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/smartface/ScrollPauseWorker;->setPageStatus(I)V

    goto :goto_0

    .line 909
    :cond_3
    const-string v2, "smart-screen-dump"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 911
    const-string v2, "true"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    sput-boolean v2, Lcom/samsung/android/smartface/SmartFaceService;->mDumpPreview:Z

    goto :goto_0

    .line 915
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 916
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClientList:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/samsung/android/smartface/ISmartFaceClient;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/smartface/SmartFaceService$Client;

    .line 918
    .local v0, "service_client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    if-eqz v0, :cond_6

    .line 919
    invoke-virtual {v0, p2, p3}, Lcom/samsung/android/smartface/SmartFaceService$Client;->setValue(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 922
    invoke-direct {p0}, Lcom/samsung/android/smartface/SmartFaceService;->updateClientStatusLocked()V

    .line 929
    :cond_5
    :goto_1
    monitor-exit v3

    goto :goto_0

    .end local v0    # "service_client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 927
    .restart local v0    # "service_client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    :cond_6
    :try_start_1
    const-string v2, "SmartFaceService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Client nod found. for command: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v2, v4, v5}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public unregister(Lcom/samsung/android/smartface/ISmartFaceClient;)V
    .locals 12
    .param p1, "client"    # Lcom/samsung/android/smartface/ISmartFaceClient;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 843
    const-string v5, "SmartFaceService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unregister:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v11}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 844
    const-string v5, "SmartFaceService"

    const-string v6, "unregister"

    invoke-static {v5, v6, v10}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 846
    invoke-static {}, Lcom/samsung/android/smartface/SmartFaceService;->clearCallingIdentity()J

    move-result-wide v2

    .line 848
    .local v2, "identityToken":J
    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;

    monitor-enter v6

    .line 849
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mClientList:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/samsung/android/smartface/ISmartFaceClient;->asBinder()Landroid/os/IBinder;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/smartface/SmartFaceService$Client;

    .line 851
    .local v0, "current":Lcom/samsung/android/smartface/SmartFaceService$Client;
    if-eqz v0, :cond_8

    .line 852
    invoke-virtual {v0}, Lcom/samsung/android/smartface/SmartFaceService$Client;->unlink()V

    .line 853
    const-string v5, "SmartFaceService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "unregister client found. pid "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getCallingPid()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " servicetype: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " AppName: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getAppName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v5, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 855
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastClient:Ljava/util/ArrayDeque;

    invoke-virtual {v5, v0}, Ljava/util/ArrayDeque;->remove(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 857
    iget-object v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastClient:Ljava/util/ArrayDeque;

    invoke-virtual {v5}, Ljava/util/ArrayDeque;->peekLast()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/smartface/SmartFaceService$Client;

    .line 859
    .local v1, "last":Lcom/samsung/android/smartface/SmartFaceService$Client;
    const/4 v4, 0x0

    .line 861
    .local v4, "serviceType":I
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v4

    .line 863
    :cond_1
    and-int/lit8 v5, v4, 0x10

    if-eqz v5, :cond_5

    const/16 v5, 0x10

    iput v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastServiecTypeFromClient:I

    .line 868
    :cond_2
    :goto_0
    const-string v5, "SmartFaceService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "send LastServiecTypeFromClient to scroll pause worker: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastServiecTypeFromClient:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v5, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 869
    iget-object v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mScrollPauseWorker:Lcom/samsung/android/smartface/ScrollPauseWorker;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mScrollPauseWorker:Lcom/samsung/android/smartface/ScrollPauseWorker;

    iget v7, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastServiecTypeFromClient:I

    invoke-virtual {v5, v7}, Lcom/samsung/android/smartface/ScrollPauseWorker;->setLastServiecTypeFromClient(I)V

    .line 874
    .end local v1    # "last":Lcom/samsung/android/smartface/SmartFaceService$Client;
    .end local v4    # "serviceType":I
    :cond_3
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/smartface/SmartFaceService;->updateClientStatusLocked()V

    .line 875
    if-eqz v0, :cond_4

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/samsung/android/smartface/SmartFaceService$Client;->notifyRegisterStatus(Z)V

    .line 876
    :cond_4
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 878
    invoke-static {v2, v3}, Lcom/samsung/android/smartface/SmartFaceService;->restoreCallingIdentity(J)V

    .line 879
    return-void

    .line 864
    .restart local v1    # "last":Lcom/samsung/android/smartface/SmartFaceService$Client;
    .restart local v4    # "serviceType":I
    :cond_5
    and-int/lit8 v5, v4, 0x1

    if-eqz v5, :cond_6

    const/4 v5, 0x1

    :try_start_1
    iput v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastServiecTypeFromClient:I

    goto :goto_0

    .line 876
    .end local v0    # "current":Lcom/samsung/android/smartface/SmartFaceService$Client;
    .end local v1    # "last":Lcom/samsung/android/smartface/SmartFaceService$Client;
    .end local v4    # "serviceType":I
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 865
    .restart local v0    # "current":Lcom/samsung/android/smartface/SmartFaceService$Client;
    .restart local v1    # "last":Lcom/samsung/android/smartface/SmartFaceService$Client;
    .restart local v4    # "serviceType":I
    :cond_6
    and-int/lit8 v5, v4, 0x2

    if-eqz v5, :cond_7

    const/4 v5, 0x2

    :try_start_2
    iput v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastServiecTypeFromClient:I

    goto :goto_0

    .line 866
    :cond_7
    and-int/lit8 v5, v4, 0x20

    if-eqz v5, :cond_2

    const/16 v5, 0x20

    iput v5, p0, Lcom/samsung/android/smartface/SmartFaceService;->mLastServiecTypeFromClient:I

    goto :goto_0

    .line 871
    .end local v1    # "last":Lcom/samsung/android/smartface/SmartFaceService$Client;
    .end local v4    # "serviceType":I
    :cond_8
    const-string v5, "SmartFaceService"

    const-string v7, "unregister - not an registered client"

    const/4 v8, 0x1

    invoke-static {v5, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public unregisterAsync(Lcom/samsung/android/smartface/ISmartFaceClient;)V
    .locals 4
    .param p1, "client"    # Lcom/samsung/android/smartface/ISmartFaceClient;

    .prologue
    .line 837
    const-string v0, "SmartFaceService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unregisterAsync:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 838
    invoke-virtual {p0, p1}, Lcom/samsung/android/smartface/SmartFaceService;->unregister(Lcom/samsung/android/smartface/ISmartFaceClient;)V

    .line 839
    return-void
.end method
