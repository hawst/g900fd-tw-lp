.class Lcom/samsung/android/smartface/CameraController$EventHandler;
.super Landroid/os/Handler;
.source "CameraController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartface/CameraController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field private mOwner:Lcom/samsung/android/smartface/CameraController;

.field final synthetic this$0:Lcom/samsung/android/smartface/CameraController;


# direct methods
.method public constructor <init>(Lcom/samsung/android/smartface/CameraController;Lcom/samsung/android/smartface/CameraController;Landroid/os/Looper;)V
    .locals 0
    .param p2, "f"    # Lcom/samsung/android/smartface/CameraController;
    .param p3, "l"    # Landroid/os/Looper;

    .prologue
    .line 405
    iput-object p1, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    .line 406
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 407
    iput-object p2, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->mOwner:Lcom/samsung/android/smartface/CameraController;

    .line 408
    return-void
.end method

.method private setCameraParameter(II)V
    .locals 11
    .param p1, "camera_mode"    # I
    .param p2, "service_type"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v5, 0x2

    const/4 v9, 0x1

    const/16 v8, 0x140

    const/4 v7, 0x0

    .line 289
    const-string v2, "CameraController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setCamreaParameter with mode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " service_type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v7}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 290
    iget-object v2, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v2}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v0

    .line 292
    .local v0, "mParam":Lcom/sec/android/seccamera/SecCamera$Parameters;
    if-nez p1, :cond_7

    .line 294
    const-string v2, "qualcomm: intelligent-mode and no-display"

    const-string v3, "qualcomm: intelligent-mode"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "qualcomm: intelligent-mode and no-display"

    const-string v3, "lsi: intelligent-mode"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "qualcomm: intelligent-mode and no-display"

    const-string v3, "qualcomm: intelligent-mode and no-display"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 300
    :cond_0
    and-int/lit8 v2, p2, 0x10

    if-eqz v2, :cond_2

    .line 302
    const-string v2, "CameraController"

    const-string v3, "Set 5fps."

    invoke-static {v2, v3, v7}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 303
    const/16 v2, 0x1388

    const/16 v3, 0x1388

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setPreviewFpsRange(II)V

    .line 304
    const-string v2, "intelligent-mode"

    invoke-virtual {v0, v2, v10}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;I)V

    .line 324
    :goto_0
    const-string v2, "320x180"

    invoke-static {v2}, Lcom/samsung/android/smartface/CameraController;->strToSize(Ljava/lang/String;)Lcom/samsung/android/smartface/CameraController$Size;

    move-result-object v1

    .line 325
    .local v1, "size":Lcom/samsung/android/smartface/CameraController$Size;
    if-eqz v1, :cond_6

    .line 327
    const-string v2, "CameraController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Pre-Configured vision preview size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/samsung/android/smartface/CameraController$Size;->width:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/samsung/android/smartface/CameraController$Size;->height:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v7}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 328
    iget-object v2, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    iget v3, v1, Lcom/samsung/android/smartface/CameraController$Size;->width:I

    iget v4, v1, Lcom/samsung/android/smartface/CameraController$Size;->height:I

    mul-int/2addr v3, v4

    new-array v3, v3, [B

    # setter for: Lcom/samsung/android/smartface/CameraController;->mPreviewData:[B
    invoke-static {v2, v3}, Lcom/samsung/android/smartface/CameraController;->access$102(Lcom/samsung/android/smartface/CameraController;[B)[B

    .line 329
    new-instance v2, Lcom/samsung/android/smartface/CameraController$Size;

    iget v3, v1, Lcom/samsung/android/smartface/CameraController$Size;->width:I

    iget v4, v1, Lcom/samsung/android/smartface/CameraController$Size;->height:I

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/smartface/CameraController$Size;-><init>(II)V

    sput-object v2, Lcom/samsung/android/smartface/CameraController;->PreviewSize:Lcom/samsung/android/smartface/CameraController$Size;

    .line 397
    :cond_1
    :goto_1
    const-string v2, "vtmode"

    invoke-virtual {v0, v2, v10}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;I)V

    .line 398
    iget-object v2, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v2}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 400
    iget-object v2, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v2}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->mOwner:Lcom/samsung/android/smartface/CameraController;

    invoke-virtual {v2, v3}, Lcom/sec/android/seccamera/SecCamera;->setErrorCallback(Lcom/sec/android/seccamera/SecCamera$ErrorCallback;)V

    .line 401
    iget-object v2, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v2}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->mOwner:Lcom/samsung/android/smartface/CameraController;

    invoke-virtual {v2, v3}, Lcom/sec/android/seccamera/SecCamera;->setPreviewCallbackWithBuffer(Lcom/sec/android/seccamera/SecCamera$PreviewCallback;)V

    .line 402
    iget-object v2, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v2}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mPreviewData:[B
    invoke-static {v3}, Lcom/samsung/android/smartface/CameraController;->access$100(Lcom/samsung/android/smartface/CameraController;)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/seccamera/SecCamera;->addCallbackBuffer([B)V

    .line 403
    return-void

    .line 306
    .end local v1    # "size":Lcom/samsung/android/smartface/CameraController$Size;
    :cond_2
    and-int/lit8 v2, p2, 0x2

    if-nez v2, :cond_3

    and-int/lit8 v2, p2, 0x1

    if-nez v2, :cond_3

    and-int/lit8 v2, p2, 0x20

    if-eqz v2, :cond_4

    .line 308
    :cond_3
    const-string v2, "CameraController"

    const-string v3, "Set 10fps."

    invoke-static {v2, v3, v7}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 309
    const/16 v2, 0x2710

    const/16 v3, 0x2710

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setPreviewFpsRange(II)V

    .line 310
    const-string v2, "intelligent-mode"

    invoke-virtual {v0, v2, v5}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 314
    :cond_4
    const-string v2, "CameraController"

    const-string v3, "WRONG SERVICE TYPE FOR VISION CAMERA. use default value."

    invoke-static {v2, v3, v7}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 315
    const-string v2, "intelligent-mode"

    invoke-virtual {v0, v2, v5}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 320
    :cond_5
    const-string v2, "CameraController"

    const-string v3, "I don\'t know how to enable the vision camera."

    invoke-static {v2, v3, v9}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 333
    .restart local v1    # "size":Lcom/samsung/android/smartface/CameraController$Size;
    :cond_6
    const-string v2, "CameraController"

    const-string v3, "Auto preview size for vision camera ?!?!"

    invoke-static {v2, v3, v7}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 334
    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewSize()Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 335
    iget-object v2, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewSize()Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v3

    iget v3, v3, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewSize()Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v4

    iget v4, v4, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    mul-int/2addr v3, v4

    new-array v3, v3, [B

    # setter for: Lcom/samsung/android/smartface/CameraController;->mPreviewData:[B
    invoke-static {v2, v3}, Lcom/samsung/android/smartface/CameraController;->access$102(Lcom/samsung/android/smartface/CameraController;[B)[B

    .line 336
    new-instance v2, Lcom/samsung/android/smartface/CameraController$Size;

    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewSize()Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v3

    iget v3, v3, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewSize()Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v4

    iget v4, v4, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/smartface/CameraController$Size;-><init>(II)V

    sput-object v2, Lcom/samsung/android/smartface/CameraController;->PreviewSize:Lcom/samsung/android/smartface/CameraController$Size;

    goto/16 :goto_1

    .line 342
    .end local v1    # "size":Lcom/samsung/android/smartface/CameraController$Size;
    :cond_7
    const-string v2, "480x272"

    invoke-static {v2}, Lcom/samsung/android/smartface/CameraController;->strToSize(Ljava/lang/String;)Lcom/samsung/android/smartface/CameraController$Size;

    move-result-object v1

    .line 343
    .restart local v1    # "size":Lcom/samsung/android/smartface/CameraController$Size;
    if-eqz v1, :cond_b

    .line 345
    iget v2, v1, Lcom/samsung/android/smartface/CameraController$Size;->width:I

    iget v3, v1, Lcom/samsung/android/smartface/CameraController$Size;->height:I

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setPreviewSize(II)V

    .line 346
    const-string v2, "CameraController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Pre-Configured preview size: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/samsung/android/smartface/CameraController$Size;->width:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Lcom/samsung/android/smartface/CameraController$Size;->height:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v7}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 367
    :cond_8
    :goto_2
    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewSize()Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 368
    iget-object v2, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewSize()Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v3

    iget v3, v3, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewSize()Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v4

    iget v4, v4, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    mul-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x3

    div-int/lit8 v3, v3, 0x2

    new-array v3, v3, [B

    # setter for: Lcom/samsung/android/smartface/CameraController;->mPreviewData:[B
    invoke-static {v2, v3}, Lcom/samsung/android/smartface/CameraController;->access$102(Lcom/samsung/android/smartface/CameraController;[B)[B

    .line 369
    new-instance v2, Lcom/samsung/android/smartface/CameraController$Size;

    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewSize()Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v3

    iget v3, v3, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewSize()Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v4

    iget v4, v4, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/smartface/CameraController$Size;-><init>(II)V

    sput-object v2, Lcom/samsung/android/smartface/CameraController;->PreviewSize:Lcom/samsung/android/smartface/CameraController$Size;

    .line 372
    :cond_9
    const-string v2, "qualcomm: intelligent-mode and no-display"

    const-string v3, "qualcomm: intelligent-mode"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 374
    const-string v2, "intelligent-mode"

    invoke-virtual {v0, v2, v9}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;I)V

    .line 391
    :cond_a
    :goto_3
    const/16 v2, 0x3a98

    const/16 v3, 0x7530

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setPreviewFpsRange(II)V

    goto/16 :goto_1

    .line 350
    :cond_b
    const-string v2, "CameraController"

    const-string v3, "Preview Size Is Not Supported (maybe auto-mode). Auto-Select"

    invoke-static {v2, v3, v7}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 352
    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/sec/android/seccamera/SecCamera$Size;

    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v5, 0x1e0

    const/16 v6, 0x10e

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/seccamera/SecCamera$Size;-><init>(Lcom/sec/android/seccamera/SecCamera;II)V

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 354
    const-string v2, "CameraController"

    const-string v3, "Front Camera Support 480, 270 preview"

    invoke-static {v2, v3, v7}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 355
    const/16 v2, 0x1e0

    const/16 v3, 0x10e

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setPreviewSize(II)V

    goto :goto_2

    .line 356
    :cond_c
    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/sec/android/seccamera/SecCamera$Size;

    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v3, v4, v8, v8}, Lcom/sec/android/seccamera/SecCamera$Size;-><init>(Lcom/sec/android/seccamera/SecCamera;II)V

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 358
    const-string v2, "CameraController"

    const-string v3, "Front Camera Support 320, 320 preview"

    invoke-static {v2, v3, v7}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 359
    invoke-virtual {v0, v8, v8}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setPreviewSize(II)V

    goto/16 :goto_2

    .line 360
    :cond_d
    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v2

    new-instance v3, Lcom/sec/android/seccamera/SecCamera$Size;

    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v5, 0xf0

    invoke-direct {v3, v4, v8, v5}, Lcom/sec/android/seccamera/SecCamera$Size;-><init>(Lcom/sec/android/seccamera/SecCamera;II)V

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 362
    const-string v2, "CameraController"

    const-string v3, "Front Camera Support 320, 240 preview"

    invoke-static {v2, v3, v7}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 363
    const/16 v2, 0xf0

    invoke-virtual {v0, v8, v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->setPreviewSize(II)V

    goto/16 :goto_2

    .line 376
    :cond_e
    const-string v2, "qualcomm: intelligent-mode and no-display"

    const-string v3, "qualcomm: intelligent-mode and no-display"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 378
    const-string v2, "intelligent-mode"

    invoke-virtual {v0, v2, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;I)V

    .line 379
    const-string v2, "no-display-mode"

    invoke-virtual {v0, v2, v9}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;I)V

    goto/16 :goto_3

    .line 381
    :cond_f
    const-string v2, "qualcomm: intelligent-mode and no-display"

    const-string v3, "lsi: intelligent-mode"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 383
    const-string v2, "intelligent-mode"

    invoke-virtual {v0, v2, v9}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;I)V

    goto/16 :goto_3
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 412
    const-string v4, "CameraController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleMessage("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") before lock"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7, v6}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 413
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    iget-object v4, v4, Lcom/samsung/android/smartface/CameraController;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 414
    const-string v4, "CameraController"

    const-string v7, "handleMessage, lock.lock after"

    invoke-static {v4, v7, v6}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 417
    :try_start_0
    const-string v4, "CameraController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleMessage("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") after lock"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v4, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 418
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 537
    const-string v4, "CameraController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleMessage("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") default!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v4, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 541
    :goto_0
    const-string v4, "CameraController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleMessage signal: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7, v5}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 543
    const-string v4, "CameraController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "handleMessage("

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, p1, Landroid/os/Message;->what:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ") before let go!"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v6}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 544
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # invokes: Lcom/samsung/android/smartface/CameraController;->letGo()V
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$800(Lcom/samsung/android/smartface/CameraController;)V

    .line 545
    const-string v4, "CameraController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "handleMessage("

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, p1, Landroid/os/Message;->what:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ") after let go!"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v6}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 547
    const-string v4, "CameraController"

    const-string v5, "handleMessage, lock.unlock before"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 548
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    iget-object v4, v4, Lcom/samsung/android/smartface/CameraController;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v4}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 549
    const-string v4, "CameraController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "handleMessage("

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v7, p1, Landroid/os/Message;->what:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ") after unlock"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v6}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 551
    return-void

    .line 421
    :pswitch_0
    :try_start_1
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->mOwner:Lcom/samsung/android/smartface/CameraController;

    invoke-virtual {v4}, Lcom/samsung/android/smartface/CameraController;->isCameraAllowed()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 423
    iget v3, p1, Landroid/os/Message;->arg1:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 425
    .local v3, "service_type":I
    :try_start_2
    const-string v4, "CameraController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "open() as "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v3}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v4, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 427
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    const/16 v7, 0xb

    const/16 v8, 0x1e

    iget-object v9, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCallbackThread:Landroid/os/HandlerThread;
    invoke-static {v9}, Lcom/samsung/android/smartface/CameraController;->access$200(Lcom/samsung/android/smartface/CameraController;)Landroid/os/HandlerThread;

    move-result-object v9

    invoke-virtual {v9}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v7, v8, v9, v10}, Lcom/sec/android/seccamera/SecCamera;->open(IILandroid/os/Looper;Z)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v7

    # setter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4, v7}, Lcom/samsung/android/smartface/CameraController;->access$002(Lcom/samsung/android/smartface/CameraController;Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera;

    .line 431
    const-string v4, "CameraController"

    const-string v7, "open() x"

    const/4 v8, 0x0

    invoke-static {v4, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 436
    :goto_1
    :try_start_3
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 437
    new-instance v0, Lcom/sec/android/seccamera/SecCamera$CameraInfo;

    invoke-direct {v0}, Lcom/sec/android/seccamera/SecCamera$CameraInfo;-><init>()V

    .line 438
    .local v0, "cameraInfo":Lcom/sec/android/seccamera/SecCamera$CameraInfo;
    const/4 v4, 0x1

    invoke-static {v4, v0}, Lcom/sec/android/seccamera/SecCamera;->getCameraInfo(ILcom/sec/android/seccamera/SecCamera$CameraInfo;)V

    .line 440
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    iget v7, v0, Lcom/sec/android/seccamera/SecCamera$CameraInfo;->orientation:I

    rsub-int v7, v7, 0x168

    rem-int/lit16 v7, v7, 0x168

    # setter for: Lcom/samsung/android/smartface/CameraController;->mDegreeOffset:I
    invoke-static {v4, v7}, Lcom/samsung/android/smartface/CameraController;->access$302(Lcom/samsung/android/smartface/CameraController;I)I

    .line 441
    const-string v4, "CameraController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Preview need to be rotated counter clock-wise "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mDegreeOffset:I
    invoke-static {v8}, Lcom/samsung/android/smartface/CameraController;->access$300(Lcom/samsung/android/smartface/CameraController;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " degree."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v4, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 443
    iget-object v7, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # invokes: Lcom/samsung/android/smartface/CameraController;->needVisionCamera(I)Z
    invoke-static {v4, v3}, Lcom/samsung/android/smartface/CameraController;->access$500(Lcom/samsung/android/smartface/CameraController;I)Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v5

    :goto_2
    # setter for: Lcom/samsung/android/smartface/CameraController;->mCameraMode:I
    invoke-static {v7, v4}, Lcom/samsung/android/smartface/CameraController;->access$402(Lcom/samsung/android/smartface/CameraController;I)I

    .line 445
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraMode:I
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$400(Lcom/samsung/android/smartface/CameraController;)I

    move-result v4

    invoke-direct {p0, v4, v3}, Lcom/samsung/android/smartface/CameraController$EventHandler;->setCameraParameter(II)V

    .line 447
    const-string v4, "CameraController"

    const-string v7, "StartPreview()"

    const/4 v8, 0x0

    invoke-static {v4, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 448
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/seccamera/SecCamera;->startPreview()V

    .line 449
    const-string v4, "CameraController"

    const-string v7, "StartPreview() X"

    const/4 v8, 0x0

    invoke-static {v4, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 460
    .end local v0    # "cameraInfo":Lcom/sec/android/seccamera/SecCamera$CameraInfo;
    .end local v3    # "service_type":I
    :catch_0
    move-exception v1

    .line 461
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v4, "CameraController"

    const-string v7, "Something goes wrong!!! Retry!"

    const/4 v8, 0x1

    invoke-static {v4, v7, v1, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Z)V

    .line 462
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mMainHandler:Lcom/samsung/android/smartface/CameraController$EventHandler;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$600(Lcom/samsung/android/smartface/CameraController;)Lcom/samsung/android/smartface/CameraController$EventHandler;

    move-result-object v4

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Lcom/samsung/android/smartface/CameraController$EventHandler;->removeMessages(I)V

    .line 463
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mMainHandler:Lcom/samsung/android/smartface/CameraController$EventHandler;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$600(Lcom/samsung/android/smartface/CameraController;)Lcom/samsung/android/smartface/CameraController$EventHandler;

    move-result-object v4

    const/4 v7, 0x1

    const-wide/16 v8, 0xbb8

    invoke-virtual {v4, v7, v8, v9}, Lcom/samsung/android/smartface/CameraController$EventHandler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 541
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    const-string v7, "CameraController"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "handleMessage signal: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p1, Landroid/os/Message;->what:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v5}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 543
    const-string v5, "CameraController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleMessage("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") before let go!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7, v6}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 544
    iget-object v5, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # invokes: Lcom/samsung/android/smartface/CameraController;->letGo()V
    invoke-static {v5}, Lcom/samsung/android/smartface/CameraController;->access$800(Lcom/samsung/android/smartface/CameraController;)V

    .line 545
    const-string v5, "CameraController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleMessage("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") after let go!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7, v6}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 547
    const-string v5, "CameraController"

    const-string v7, "handleMessage, lock.unlock before"

    invoke-static {v5, v7, v6}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 548
    iget-object v5, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    iget-object v5, v5, Lcom/samsung/android/smartface/CameraController;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v5}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 549
    const-string v5, "CameraController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleMessage("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->what:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") after unlock"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7, v6}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    throw v4

    .line 432
    .restart local v3    # "service_type":I
    :catch_1
    move-exception v1

    .line 433
    .local v1, "e":Ljava/lang/RuntimeException;
    :try_start_5
    const-string v4, "CameraController"

    const-string v7, "Unable to open Camera"

    const/4 v8, 0x1

    invoke-static {v4, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 434
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    const/4 v7, 0x0

    # setter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4, v7}, Lcom/samsung/android/smartface/CameraController;->access$002(Lcom/samsung/android/smartface/CameraController;Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera;

    goto/16 :goto_1

    .end local v1    # "e":Ljava/lang/RuntimeException;
    .restart local v0    # "cameraInfo":Lcom/sec/android/seccamera/SecCamera$CameraInfo;
    :cond_0
    move v4, v6

    .line 443
    goto/16 :goto_2

    .line 451
    .end local v0    # "cameraInfo":Lcom/sec/android/seccamera/SecCamera$CameraInfo;
    :cond_1
    const-string v4, "CameraController"

    const-string v7, "Retry! START_CAMERA!!"

    const/4 v8, 0x0

    invoke-static {v4, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 452
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mMainHandler:Lcom/samsung/android/smartface/CameraController$EventHandler;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$600(Lcom/samsung/android/smartface/CameraController;)Lcom/samsung/android/smartface/CameraController$EventHandler;

    move-result-object v4

    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Lcom/samsung/android/smartface/CameraController$EventHandler;->removeMessages(I)V

    .line 453
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mMainHandler:Lcom/samsung/android/smartface/CameraController$EventHandler;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$600(Lcom/samsung/android/smartface/CameraController;)Lcom/samsung/android/smartface/CameraController$EventHandler;

    move-result-object v4

    const/4 v7, 0x1

    const-wide/16 v8, 0xbb8

    invoke-virtual {v4, v7, v8, v9}, Lcom/samsung/android/smartface/CameraController$EventHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 458
    .end local v3    # "service_type":I
    :cond_2
    const-string v4, "CameraController"

    const-string v7, "Camera blocked."

    const/4 v8, 0x0

    invoke-static {v4, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 467
    :pswitch_1
    :try_start_6
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v4

    if-eqz v4, :cond_4

    .line 469
    :try_start_7
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Lcom/sec/android/seccamera/SecCamera;->setPreviewCallback(Lcom/sec/android/seccamera/SecCamera$PreviewCallback;)V

    .line 470
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/seccamera/SecCamera;->stopPreview()V

    .line 472
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mListener:Lcom/samsung/android/smartface/CameraController$CameraListener;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$700(Lcom/samsung/android/smartface/CameraController;)Lcom/samsung/android/smartface/CameraController$CameraListener;

    move-result-object v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mListener:Lcom/samsung/android/smartface/CameraController$CameraListener;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$700(Lcom/samsung/android/smartface/CameraController;)Lcom/samsung/android/smartface/CameraController$CameraListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/samsung/android/smartface/CameraController$CameraListener;->onCallbackReset()V

    .line 474
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/seccamera/SecCamera;->release()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 479
    :cond_4
    :goto_3
    :try_start_8
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    const/4 v7, 0x0

    # setter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4, v7}, Lcom/samsung/android/smartface/CameraController;->access$002(Lcom/samsung/android/smartface/CameraController;Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera;

    goto/16 :goto_0

    .line 475
    :catch_2
    move-exception v1

    .line 476
    .local v1, "e":Ljava/lang/Exception;
    const-string v4, "CameraController"

    const-string v7, "Something goes wrong!!! Ignore."

    const/4 v8, 0x1

    invoke-static {v4, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    .line 483
    .end local v1    # "e":Ljava/lang/Exception;
    :pswitch_2
    :try_start_9
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 484
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v2

    .line 485
    .local v2, "mParam":Lcom/sec/android/seccamera/SecCamera$Parameters;
    const-string v4, "smart-screen-exposure"

    iget v7, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v4, v7}, Lcom/sec/android/seccamera/SecCamera$Parameters;->set(Ljava/lang/String;I)V

    .line 486
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/sec/android/seccamera/SecCamera;->setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_0

    .line 492
    .end local v2    # "mParam":Lcom/sec/android/seccamera/SecCamera$Parameters;
    :catch_3
    move-exception v1

    .line 493
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_a
    const-string v4, "CameraController"

    const-string v7, "Mode Exposure Mode Failed."

    const/4 v8, 0x1

    invoke-static {v4, v7, v1, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Z)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_0

    .line 490
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_5
    :try_start_b
    const-string v4, "CameraController"

    const-string v7, "No Camera Device!"

    const/4 v8, 0x0

    invoke-static {v4, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_0

    .line 500
    :pswitch_3
    :try_start_c
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 501
    const-string v4, "CameraController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Camera Mode Changed: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraMode:I
    invoke-static {v8}, Lcom/samsung/android/smartface/CameraController;->access$400(Lcom/samsung/android/smartface/CameraController;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "->"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v4, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 503
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Lcom/sec/android/seccamera/SecCamera;->setPreviewCallback(Lcom/sec/android/seccamera/SecCamera$PreviewCallback;)V

    .line 504
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/seccamera/SecCamera;->stopPreview()V

    .line 506
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mListener:Lcom/samsung/android/smartface/CameraController$CameraListener;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$700(Lcom/samsung/android/smartface/CameraController;)Lcom/samsung/android/smartface/CameraController$CameraListener;

    move-result-object v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mListener:Lcom/samsung/android/smartface/CameraController$CameraListener;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$700(Lcom/samsung/android/smartface/CameraController;)Lcom/samsung/android/smartface/CameraController$CameraListener;

    move-result-object v4

    invoke-interface {v4}, Lcom/samsung/android/smartface/CameraController$CameraListener;->onCallbackReset()V

    .line 508
    :cond_6
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/seccamera/SecCamera;->release()V

    .line 511
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    const/16 v7, 0xb

    const/16 v8, 0x1e

    iget-object v9, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCallbackThread:Landroid/os/HandlerThread;
    invoke-static {v9}, Lcom/samsung/android/smartface/CameraController;->access$200(Lcom/samsung/android/smartface/CameraController;)Landroid/os/HandlerThread;

    move-result-object v9

    invoke-virtual {v9}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v7, v8, v9, v10}, Lcom/sec/android/seccamera/SecCamera;->open(IILandroid/os/Looper;Z)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v7

    # setter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4, v7}, Lcom/samsung/android/smartface/CameraController;->access$002(Lcom/samsung/android/smartface/CameraController;Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera;

    .line 515
    iget v4, p1, Landroid/os/Message;->arg1:I

    iget v7, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v4, v7}, Lcom/samsung/android/smartface/CameraController$EventHandler;->setCameraParameter(II)V

    .line 517
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    # getter for: Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;
    invoke-static {v4}, Lcom/samsung/android/smartface/CameraController;->access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/seccamera/SecCamera;->startPreview()V

    .line 519
    iget-object v4, p0, Lcom/samsung/android/smartface/CameraController$EventHandler;->this$0:Lcom/samsung/android/smartface/CameraController;

    iget v7, p1, Landroid/os/Message;->arg1:I

    # setter for: Lcom/samsung/android/smartface/CameraController;->mCameraMode:I
    invoke-static {v4, v7}, Lcom/samsung/android/smartface/CameraController;->access$402(Lcom/samsung/android/smartface/CameraController;I)I
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0

    .line 526
    :catch_4
    move-exception v1

    .line 528
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_d
    const-string v4, "CameraController"

    const-string v7, "Mode Change Failed."

    const/4 v8, 0x1

    invoke-static {v4, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_0

    .line 523
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_7
    :try_start_e
    const-string v4, "CameraController"

    const-string v7, "No Camera Device!. Camera mode cannot be changed. (maybe block by edm. open failed. etc.)"

    const/4 v8, 0x0

    invoke-static {v4, v7, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_4
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_0

    .line 418
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
