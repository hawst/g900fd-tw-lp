.class public Lcom/samsung/android/smartface/SmartFaceService$Logger;
.super Ljava/lang/Object;
.source "SmartFaceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartface/SmartFaceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Logger"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Logger"

.field private static mInstance:Lcom/samsung/android/smartface/SmartFaceService$Logger;


# instance fields
.field private mQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1034
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/smartface/SmartFaceService$Logger;->mInstance:Lcom/samsung/android/smartface/SmartFaceService$Logger;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 1051
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1035
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$Logger;->mQueue:Ljava/util/Queue;

    .line 1052
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$Logger;->mQueue:Ljava/util/Queue;

    .line 1053
    const-string v0, "Logger"

    const-string v1, "New Logger Instance"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1054
    return-void
.end method

.method private declared-synchronized addLogInternal(Ljava/lang/String;J)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "time"    # J

    .prologue
    .line 1094
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$Logger;->mQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    const/16 v1, 0x2710

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$Logger;->mQueue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1095
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$Logger;->mQueue:Ljava/util/Queue;

    new-instance v1, Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;-><init>(Lcom/samsung/android/smartface/SmartFaceService$Logger;Ljava/lang/String;J)V

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1096
    monitor-exit p0

    return-void
.end method

.method public static dumpLog(Ljava/io/PrintWriter;)V
    .locals 1
    .param p0, "pw"    # Ljava/io/PrintWriter;

    .prologue
    .line 1102
    invoke-static {}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->getInstance()Lcom/samsung/android/smartface/SmartFaceService$Logger;

    move-result-object v0

    invoke-direct {v0, p0}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->dumpLogInternal(Ljava/io/PrintWriter;)V

    .line 1103
    return-void
.end method

.method private declared-synchronized dumpLogInternal(Ljava/io/PrintWriter;)V
    .locals 8
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .prologue
    .line 1107
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService$Logger;->mQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1108
    .local v2, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;>;"
    const/4 v1, 0x0

    .line 1110
    .local v1, "item":Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;
    const-string v3, "Dump of SmartFaceService Activity"

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1112
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v3, "MM-dd HH:mm:ss.SSS"

    invoke-direct {v0, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 1113
    .local v0, "dataFormat":Ljava/text/SimpleDateFormat;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " Time ref. : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    new-instance v4, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1115
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1116
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "item":Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;
    check-cast v1, Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;

    .line 1117
    .restart local v1    # "item":Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "\t"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;->mTimeStamp:J
    invoke-static {v1}, Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;->access$1900(Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;->mString:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;->access$2000(Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1107
    .end local v0    # "dataFormat":Ljava/text/SimpleDateFormat;
    .end local v1    # "item":Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;
    .end local v2    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;>;"
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 1120
    .restart local v0    # "dataFormat":Ljava/text/SimpleDateFormat;
    .restart local v1    # "item":Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;
    .restart local v2    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;>;"
    :cond_0
    :try_start_1
    const-string v3, "--------------------------------------\n"

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1121
    monitor-exit p0

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/smartface/SmartFaceService$Logger;
    .locals 2

    .prologue
    .line 1058
    const-class v1, Lcom/samsung/android/smartface/SmartFaceService$Logger;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/smartface/SmartFaceService$Logger;->mInstance:Lcom/samsung/android/smartface/SmartFaceService$Logger;

    if-nez v0, :cond_0

    new-instance v0, Lcom/samsung/android/smartface/SmartFaceService$Logger;

    invoke-direct {v0}, Lcom/samsung/android/smartface/SmartFaceService$Logger;-><init>()V

    sput-object v0, Lcom/samsung/android/smartface/SmartFaceService$Logger;->mInstance:Lcom/samsung/android/smartface/SmartFaceService$Logger;

    .line 1059
    :cond_0
    sget-object v0, Lcom/samsung/android/smartface/SmartFaceService$Logger;->mInstance:Lcom/samsung/android/smartface/SmartFaceService$Logger;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1058
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Z)V
    .locals 5
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;
    .param p3, "addToLogger"    # Z

    .prologue
    .line 1064
    invoke-static {p0, p1, p2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1066
    if-eqz p3, :cond_0

    .line 1070
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1071
    .local v0, "time":J
    invoke-static {}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->getInstance()Lcom/samsung/android/smartface/SmartFaceService$Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0, v1}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->addLogInternal(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1075
    .end local v0    # "time":J
    :cond_0
    :goto_0
    return-void

    .line 1072
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static log(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "addToLogger"    # Z

    .prologue
    .line 1079
    invoke-static {p0, p1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1081
    if-eqz p2, :cond_0

    .line 1085
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1086
    .local v0, "time":J
    invoke-static {}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->getInstance()Lcom/samsung/android/smartface/SmartFaceService$Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0, v1}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->addLogInternal(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1090
    .end local v0    # "time":J
    :cond_0
    :goto_0
    return-void

    .line 1087
    :catch_0
    move-exception v2

    goto :goto_0
.end method
