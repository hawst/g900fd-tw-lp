.class public Lcom/samsung/android/smartface/SmartFaceIndicator;
.super Ljava/lang/Object;
.source "SmartFaceIndicator.java"


# static fields
.field private static final INDICATOR_DIM:I = 0x1

.field private static final INDICATOR_NONE:I = 0x2

.field private static final INDICATOR_NOR:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SmartFaceIndicator"


# instance fields
.field private mActiveService:I

.field private mContext:Landroid/content/Context;

.field private mIsCameraOpened:Z

.field private mNormalIconId:I

.field private mPm:Landroid/os/PowerManager;

.field private mService:Landroid/app/StatusBarManager;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mContext:Landroid/content/Context;

    .line 58
    const v0, 0x10807c9

    iput v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mNormalIconId:I

    .line 59
    const-string v0, "statusbar"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    iput-object v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mService:Landroid/app/StatusBarManager;

    .line 61
    iput v1, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mActiveService:I

    .line 62
    iput-boolean v1, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mIsCameraOpened:Z

    .line 64
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mPm:Landroid/os/PowerManager;

    .line 65
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mPm:Landroid/os/PowerManager;

    const/16 v1, 0xa

    const-string v2, "SmartFaceIndicator"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 66
    return-void
.end method

.method private setIndicator(I)V
    .locals 5
    .param p1, "dimmed"    # I

    .prologue
    const/4 v4, 0x0

    .line 98
    if-nez p1, :cond_0

    .line 99
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mService:Landroid/app/StatusBarManager;

    const-string v1, "smart_scroll"

    iget v2, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mNormalIconId:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/app/StatusBarManager;->setIcon(Ljava/lang/String;IILjava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mService:Landroid/app/StatusBarManager;

    const-string v1, "smart_scroll"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/StatusBarManager;->setIconVisibility(Ljava/lang/String;Z)V

    .line 101
    const-string v0, "SmartFaceIndicator"

    const-string v1, "normal icon"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mService:Landroid/app/StatusBarManager;

    const-string v1, "smart_scroll"

    invoke-virtual {v0, v1, v4}, Landroid/app/StatusBarManager;->setIconVisibility(Ljava/lang/String;Z)V

    .line 104
    const-string v0, "SmartFaceIndicator"

    const-string v1, "no icon"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private update()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 82
    iget v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mActiveService:I

    if-nez v0, :cond_0

    .line 84
    invoke-direct {p0, v1}, Lcom/samsung/android/smartface/SmartFaceIndicator;->setIndicator(I)V

    .line 95
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mIsCameraOpened:Z

    if-eqz v0, :cond_1

    .line 88
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/smartface/SmartFaceIndicator;->setIndicator(I)V

    goto :goto_0

    .line 92
    :cond_1
    invoke-direct {p0, v1}, Lcom/samsung/android/smartface/SmartFaceIndicator;->setIndicator(I)V

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized onCameraStatusChanged(Z)V
    .locals 1
    .param p1, "is_opened"    # Z

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mIsCameraOpened:Z

    .line 77
    invoke-direct {p0}, Lcom/samsung/android/smartface/SmartFaceIndicator;->update()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    monitor-exit p0

    return-void

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onSettingChanged(I)V
    .locals 1
    .param p1, "active_service"    # I

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mActiveService:I

    .line 71
    invoke-direct {p0}, Lcom/samsung/android/smartface/SmartFaceIndicator;->update()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    monitor-exit p0

    return-void

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setWakeLock(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    const/4 v1, 0x1

    .line 110
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mIsCameraOpened:Z

    if-nez v0, :cond_0

    const/4 p1, 0x0

    .line 112
    :cond_0
    if-ne p1, v1, :cond_2

    .line 113
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 115
    const-string v0, "SmartFaceIndicator"

    const-string v1, "mWakeLock.acquired (SCREEN_BRIGHT_WAKE_LOCK)"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 124
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 118
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mPm:Landroid/os/PowerManager;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/os/PowerManager;->userActivity(JZ)V

    .line 120
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceIndicator;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 121
    const-string v0, "SmartFaceIndicator"

    const-string v1, "mWakeLock.released (SCREEN_BRIGHT_WAKE_LOCK)"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
