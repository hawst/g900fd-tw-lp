.class Lcom/samsung/android/smartface/Worker$1;
.super Landroid/os/Handler;
.source "Worker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/smartface/Worker;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/samsung/android/smartface/SmartFaceIndicator;Lcom/samsung/android/smartface/CameraController;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/smartface/Worker;


# direct methods
.method constructor <init>(Lcom/samsung/android/smartface/Worker;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/samsung/android/smartface/Worker$1;->this$0:Lcom/samsung/android/smartface/Worker;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker$1;->this$0:Lcom/samsung/android/smartface/Worker;

    iget-object v0, v0, Lcom/samsung/android/smartface/Worker;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 150
    :goto_0
    return-void

    .line 143
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker$1;->this$0:Lcom/samsung/android/smartface/Worker;

    iget-object v1, v0, Lcom/samsung/android/smartface/Worker;->mPreviewDataLock:Ljava/lang/Object;

    monitor-enter v1

    .line 145
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker$1;->this$0:Lcom/samsung/android/smartface/Worker;

    iget-object v0, v0, Lcom/samsung/android/smartface/Worker;->mTempData:Lcom/samsung/android/smartface/Worker$PreviewData;

    iget-object v2, p0, Lcom/samsung/android/smartface/Worker$1;->this$0:Lcom/samsung/android/smartface/Worker;

    iget-object v2, v2, Lcom/samsung/android/smartface/Worker;->mData:Lcom/samsung/android/smartface/Worker$PreviewData;

    invoke-virtual {v0, v2}, Lcom/samsung/android/smartface/Worker$PreviewData;->copy(Lcom/samsung/android/smartface/Worker$PreviewData;)V

    .line 146
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker$1;->this$0:Lcom/samsung/android/smartface/Worker;

    iget-object v1, p0, Lcom/samsung/android/smartface/Worker$1;->this$0:Lcom/samsung/android/smartface/Worker;

    iget-object v2, p0, Lcom/samsung/android/smartface/Worker$1;->this$0:Lcom/samsung/android/smartface/Worker;

    iget-object v2, v2, Lcom/samsung/android/smartface/Worker;->mTempData:Lcom/samsung/android/smartface/Worker$PreviewData;

    invoke-virtual {v1, v2}, Lcom/samsung/android/smartface/Worker;->Process(Lcom/samsung/android/smartface/Worker$PreviewData;)Lcom/samsung/android/smartface/Worker$Response;

    move-result-object v1

    # invokes: Lcom/samsung/android/smartface/Worker;->sendToClients(Lcom/samsung/android/smartface/Worker$Response;)V
    invoke-static {v0, v1}, Lcom/samsung/android/smartface/Worker;->access$000(Lcom/samsung/android/smartface/Worker;Lcom/samsung/android/smartface/Worker$Response;)V

    goto :goto_0

    .line 146
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 139
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
