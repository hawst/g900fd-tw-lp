.class Lcom/samsung/android/smartface/SmartFaceService$SensorListener;
.super Ljava/lang/Object;
.source "SmartFaceService.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartface/SmartFaceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/smartface/SmartFaceService;


# direct methods
.method private constructor <init>(Lcom/samsung/android/smartface/SmartFaceService;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/samsung/android/smartface/SmartFaceService$SensorListener;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/smartface/SmartFaceService;Lcom/samsung/android/smartface/SmartFaceService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/smartface/SmartFaceService;
    .param p2, "x1"    # Lcom/samsung/android/smartface/SmartFaceService$1;

    .prologue
    .line 167
    invoke-direct {p0, p1}, Lcom/samsung/android/smartface/SmartFaceService$SensorListener;-><init>(Lcom/samsung/android/smartface/SmartFaceService;)V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 169
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 172
    iget-object v3, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v3}, Landroid/hardware/Sensor;->getType()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_1

    .line 174
    const/4 v1, 0x1

    .line 176
    .local v1, "light":Z
    const v0, 0x3f4ccccd    # 0.8f

    .line 177
    .local v0, "alpha":F
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x0

    aget v2, v3, v4

    .line 179
    .local v2, "lux":F
    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService$SensorListener;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mLux:F
    invoke-static {v3}, Lcom/samsung/android/smartface/SmartFaceService;->access$000(Lcom/samsung/android/smartface/SmartFaceService;)F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$SensorListener;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mLux:F
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$000(Lcom/samsung/android/smartface/SmartFaceService;)F

    move-result v4

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService$SensorListener;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mLux:F
    invoke-static {v3, v2}, Lcom/samsung/android/smartface/SmartFaceService;->access$002(Lcom/samsung/android/smartface/SmartFaceService;F)F

    .line 182
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService$SensorListener;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mLux:F
    invoke-static {v3}, Lcom/samsung/android/smartface/SmartFaceService;->access$000(Lcom/samsung/android/smartface/SmartFaceService;)F

    move-result v3

    const/high16 v4, 0x41200000    # 10.0f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_3

    const/4 v1, 0x0

    .line 185
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService$SensorListener;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/samsung/android/smartface/SmartFaceService;->access$100(Lcom/samsung/android/smartface/SmartFaceService;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 187
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService$SensorListener;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mLightIntensityEnough:Z
    invoke-static {v3}, Lcom/samsung/android/smartface/SmartFaceService;->access$200(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v3

    if-eq v1, v3, :cond_0

    .line 189
    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService$SensorListener;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mLightIntensityEnough:Z
    invoke-static {v3, v1}, Lcom/samsung/android/smartface/SmartFaceService;->access$202(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    .line 190
    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService$SensorListener;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # invokes: Lcom/samsung/android/smartface/SmartFaceService;->updateClientStatusLocked()V
    invoke-static {v3}, Lcom/samsung/android/smartface/SmartFaceService;->access$300(Lcom/samsung/android/smartface/SmartFaceService;)V

    .line 192
    :cond_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    .end local v0    # "alpha":F
    .end local v1    # "light":Z
    .end local v2    # "lux":F
    :cond_1
    return-void

    .line 180
    .restart local v0    # "alpha":F
    .restart local v1    # "light":Z
    .restart local v2    # "lux":F
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService$SensorListener;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$SensorListener;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mLux:F
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$000(Lcom/samsung/android/smartface/SmartFaceService;)F

    move-result v4

    mul-float/2addr v4, v0

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v5, v0

    mul-float/2addr v5, v2

    add-float/2addr v4, v5

    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mLux:F
    invoke-static {v3, v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$002(Lcom/samsung/android/smartface/SmartFaceService;F)F

    goto :goto_0

    .line 183
    :cond_3
    const/4 v1, 0x1

    goto :goto_1

    .line 192
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method
