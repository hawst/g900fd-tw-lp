.class public Lcom/samsung/android/smartface/StayRotationWorker;
.super Lcom/samsung/android/smartface/Worker;
.source "StayRotationWorker.java"


# instance fields
.field private final FRAME_COUNT_LOCK:Ljava/lang/Object;

.field private final ROTATION_LOCK:Ljava/lang/Object;

.field private mFixed:Z

.field private mFrameCount:I

.field private mRotationUI:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/samsung/android/smartface/SmartFaceIndicator;Lcom/samsung/android/smartface/CameraController;Landroid/os/Handler;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "service_type"    # I
    .param p4, "indicator"    # Lcom/samsung/android/smartface/SmartFaceIndicator;
    .param p5, "controller"    # Lcom/samsung/android/smartface/CameraController;
    .param p6, "listenerhandler"    # Landroid/os/Handler;

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct/range {p0 .. p6}, Lcom/samsung/android/smartface/Worker;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/samsung/android/smartface/SmartFaceIndicator;Lcom/samsung/android/smartface/CameraController;Landroid/os/Handler;)V

    .line 40
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/smartface/StayRotationWorker;->mRotationUI:I

    .line 41
    iput v1, p0, Lcom/samsung/android/smartface/StayRotationWorker;->mFrameCount:I

    .line 42
    iput-boolean v1, p0, Lcom/samsung/android/smartface/StayRotationWorker;->mFixed:Z

    .line 43
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/smartface/StayRotationWorker;->ROTATION_LOCK:Ljava/lang/Object;

    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/smartface/StayRotationWorker;->FRAME_COUNT_LOCK:Ljava/lang/Object;

    .line 48
    return-void
.end method


# virtual methods
.method protected Process(Lcom/samsung/android/smartface/Worker$PreviewData;)Lcom/samsung/android/smartface/Worker$Response;
    .locals 29
    .param p1, "data"    # Lcom/samsung/android/smartface/Worker$PreviewData;

    .prologue
    .line 71
    sget-boolean v4, Lcom/samsung/android/smartface/SmartFaceService;->mDumpPreview:Z

    if-eqz v4, :cond_0

    .line 73
    const/16 v20, 0x0

    .line 75
    .local v20, "out":Ljava/io/FileOutputStream;
    new-instance v16, Ljava/text/SimpleDateFormat;

    const-string v4, "MMddHHmmssSSS"

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 78
    .local v16, "dataFormat":Ljava/text/SimpleDateFormat;
    :try_start_0
    new-instance v21, Ljava/io/FileOutputStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/data/log/SR"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".yuv"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 81
    .end local v20    # "out":Ljava/io/FileOutputStream;
    .local v21, "out":Ljava/io/FileOutputStream;
    :try_start_1
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    array-length v6, v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5, v6}, Ljava/io/FileOutputStream;->write([BII)V

    .line 82
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 96
    .end local v16    # "dataFormat":Ljava/text/SimpleDateFormat;
    .end local v21    # "out":Ljava/io/FileOutputStream;
    :cond_0
    :goto_0
    const/4 v14, 0x0

    .local v14, "bStay":Z
    const/4 v13, 0x0

    .line 98
    .local v13, "bRotation":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/StayRotationWorker;->mClientList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/android/smartface/SmartFaceService$Client;

    .line 100
    .local v15, "client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    invoke-virtual {v15}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v4

    and-int/lit8 v4, v4, 0x4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/smartface/StayRotationWorker;->mActiveServiceType:I

    and-int/2addr v4, v5

    if-eqz v4, :cond_2

    .line 101
    const/4 v14, 0x1

    .line 102
    :cond_2
    invoke-virtual {v15}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v4

    and-int/lit8 v4, v4, 0x8

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/smartface/StayRotationWorker;->mActiveServiceType:I

    and-int/2addr v4, v5

    if-eqz v4, :cond_1

    .line 103
    const/4 v13, 0x1

    goto :goto_1

    .line 84
    .end local v13    # "bRotation":Z
    .end local v14    # "bStay":Z
    .end local v15    # "client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    .end local v18    # "i$":Ljava/util/Iterator;
    .restart local v16    # "dataFormat":Ljava/text/SimpleDateFormat;
    .restart local v21    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v17

    .line 85
    .local v17, "e":Ljava/io/IOException;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/StayRotationWorker;->TAG:Ljava/lang/String;

    const-string v5, "Error IOException"

    move-object/from16 v0, v17

    invoke-static {v4, v5, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 86
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 89
    .end local v17    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v17

    move-object/from16 v20, v21

    .line 90
    .end local v21    # "out":Ljava/io/FileOutputStream;
    .local v17, "e":Ljava/lang/Exception;
    .restart local v20    # "out":Ljava/io/FileOutputStream;
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/StayRotationWorker;->TAG:Ljava/lang/String;

    const-string v5, "Error Exception"

    move-object/from16 v0, v17

    invoke-static {v4, v5, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 106
    .end local v16    # "dataFormat":Ljava/text/SimpleDateFormat;
    .end local v17    # "e":Ljava/lang/Exception;
    .end local v20    # "out":Ljava/io/FileOutputStream;
    .restart local v13    # "bRotation":Z
    .restart local v14    # "bStay":Z
    .restart local v18    # "i$":Ljava/util/Iterator;
    :cond_3
    new-instance v19, Lcom/samsung/android/smartface/FaceInfo;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/android/smartface/FaceInfo;-><init>()V

    .line 107
    .local v19, "info":Lcom/samsung/android/smartface/FaceInfo;
    const/4 v4, 0x0

    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->needToRotate:I

    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->needToStay:I

    .line 109
    if-eqz v14, :cond_6

    .line 111
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/smartface/StayRotationWorker;->FRAME_COUNT_LOCK:Ljava/lang/Object;

    move-object/from16 v28, v0

    monitor-enter v28

    .line 113
    const/16 v22, 0x0

    .line 114
    .local v22, "ret":I
    const/4 v12, 0x0

    .line 116
    .local v12, "cam_land":I
    :try_start_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v24

    .line 117
    .local v24, "time1":J
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    move-object/from16 v0, p1

    iget v5, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mWidth:I

    move-object/from16 v0, p1

    iget v6, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mHeight:I

    move-object/from16 v0, p1

    iget v7, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mUIOrientation:I

    const-string v8, "/data/smart_stay_hash.dmc"

    const/high16 v9, 0x40a00000    # 5.0f

    move-object/from16 v0, p1

    iget-boolean v10, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->bVisionMode:Z

    if-eqz v10, :cond_b

    const/4 v10, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget v11, v0, Lcom/samsung/android/smartface/StayRotationWorker;->mFrameCount:I

    invoke-static/range {v4 .. v12}, Lcom/sec/android/smartface/SmartScreen;->processSmartStay([BIIILjava/lang/String;FIII)I

    move-result v22

    const/4 v4, -0x1

    move/from16 v0, v22

    if-le v0, v4, :cond_c

    const/4 v4, 0x1

    :goto_4
    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->needToStay:I

    .line 120
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v26

    .line 122
    .local v26, "time2":J
    const/16 v4, -0x64

    move/from16 v0, v22

    if-ne v0, v4, :cond_4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/smartface/StayRotationWorker;->mFrameCount:I

    if-nez v4, :cond_4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/android/smartface/StayRotationWorker;->mFixed:Z

    .line 123
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/smartface/StayRotationWorker;->mFixed:Z

    if-eqz v4, :cond_5

    const/4 v4, 0x0

    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->needToStay:I

    .line 125
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/StayRotationWorker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "processSmartStay["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v6, v26, v24

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ms], ret: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " needToStay: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    iget v6, v0, Lcom/samsung/android/smartface/FaceInfo;->needToStay:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mFrameCount: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/smartface/StayRotationWorker;->mFrameCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mFixed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/samsung/android/smartface/StayRotationWorker;->mFixed:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/smartface/StayRotationWorker;->mFrameCount:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/android/smartface/StayRotationWorker;->mFrameCount:I

    .line 127
    monitor-exit v28
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 129
    .end local v12    # "cam_land":I
    .end local v22    # "ret":I
    .end local v24    # "time1":J
    .end local v26    # "time2":J
    :cond_6
    if-eqz v13, :cond_8

    .line 131
    const/16 v22, 0x0

    .restart local v22    # "ret":I
    move-object/from16 v0, p1

    iget v0, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mUIOrientation:I

    move/from16 v23, v0

    .line 133
    .local v23, "ui_orientation":I
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartface/StayRotationWorker;->ROTATION_LOCK:Ljava/lang/Object;

    monitor-enter v5

    .line 135
    :try_start_4
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/smartface/StayRotationWorker;->mRotationUI:I

    const/4 v6, -0x1

    if-eq v4, v6, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/smartface/StayRotationWorker;->mRotationUI:I

    move/from16 v23, v0

    .line 136
    :cond_7
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 138
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/StayRotationWorker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "UI Orientation for SmartRotation: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v24

    .line 141
    .restart local v24    # "time1":J
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    move-object/from16 v0, p1

    iget v6, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mWidth:I

    move-object/from16 v0, p1

    iget v7, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mHeight:I

    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->bVisionMode:Z

    if-eqz v4, :cond_d

    const/4 v4, 0x0

    :goto_5
    move/from16 v0, v23

    invoke-static {v5, v6, v7, v0, v4}, Lcom/sec/android/smartface/SmartScreen;->processSmartRotation([BIIII)I

    move-result v22

    const/4 v4, -0x1

    move/from16 v0, v22

    if-le v0, v4, :cond_e

    const/4 v4, 0x1

    :goto_6
    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->needToRotate:I

    .line 144
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v26

    .line 146
    .restart local v26    # "time2":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/StayRotationWorker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "processSmartRotation["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v6, v26, v24

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ms], ret: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " needToRotate: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    iget v6, v0, Lcom/samsung/android/smartface/FaceInfo;->needToRotate:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    .end local v22    # "ret":I
    .end local v23    # "ui_orientation":I
    .end local v24    # "time1":J
    .end local v26    # "time2":J
    :cond_8
    const/16 v22, 0x0

    .line 151
    .local v22, "ret":Lcom/samsung/android/smartface/Worker$Response;
    if-nez v14, :cond_9

    if-eqz v13, :cond_a

    .line 153
    :cond_9
    new-instance v22, Lcom/samsung/android/smartface/Worker$Response;

    .end local v22    # "ret":Lcom/samsung/android/smartface/Worker$Response;
    if-eqz v14, :cond_f

    const/4 v4, 0x4

    move v5, v4

    :goto_7
    if-eqz v13, :cond_10

    const/16 v4, 0x8

    :goto_8
    or-int/2addr v4, v5

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v4}, Lcom/samsung/android/smartface/Worker$Response;-><init>(Lcom/samsung/android/smartface/Worker;Lcom/samsung/android/smartface/FaceInfo;I)V

    .line 156
    .restart local v22    # "ret":Lcom/samsung/android/smartface/Worker$Response;
    :cond_a
    return-object v22

    .line 117
    .restart local v12    # "cam_land":I
    .local v22, "ret":I
    .restart local v24    # "time1":J
    :cond_b
    const/4 v10, 0x1

    goto/16 :goto_3

    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 127
    .end local v24    # "time1":J
    :catchall_0
    move-exception v4

    :try_start_5
    monitor-exit v28
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v4

    .line 136
    .end local v12    # "cam_land":I
    .restart local v23    # "ui_orientation":I
    :catchall_1
    move-exception v4

    :try_start_6
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v4

    .line 141
    .restart local v24    # "time1":J
    :cond_d
    const/4 v4, 0x1

    goto :goto_5

    :cond_e
    const/4 v4, 0x0

    goto :goto_6

    .line 153
    .end local v22    # "ret":I
    .end local v23    # "ui_orientation":I
    .end local v24    # "time1":J
    :cond_f
    const/4 v4, 0x0

    move v5, v4

    goto :goto_7

    :cond_10
    const/4 v4, 0x0

    goto :goto_8

    .line 89
    .end local v13    # "bRotation":Z
    .end local v14    # "bStay":Z
    .end local v18    # "i$":Ljava/util/Iterator;
    .end local v19    # "info":Lcom/samsung/android/smartface/FaceInfo;
    .restart local v16    # "dataFormat":Ljava/text/SimpleDateFormat;
    .restart local v20    # "out":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v17

    goto/16 :goto_2
.end method

.method public frameCountReset()V
    .locals 2

    .prologue
    .line 52
    iget-object v1, p0, Lcom/samsung/android/smartface/StayRotationWorker;->FRAME_COUNT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 54
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/samsung/android/smartface/StayRotationWorker;->mFrameCount:I

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/smartface/StayRotationWorker;->mFixed:Z

    .line 56
    monitor-exit v1

    .line 57
    return-void

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setSmartRotationUIOrientation(I)V
    .locals 4
    .param p1, "ui_orientation"    # I

    .prologue
    .line 61
    iget-object v1, p0, Lcom/samsung/android/smartface/StayRotationWorker;->ROTATION_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 63
    :try_start_0
    iput p1, p0, Lcom/samsung/android/smartface/StayRotationWorker;->mRotationUI:I

    .line 64
    iget-object v0, p0, Lcom/samsung/android/smartface/StayRotationWorker;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Set mRotationUI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/smartface/StayRotationWorker;->mRotationUI:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    monitor-exit v1

    .line 66
    return-void

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
