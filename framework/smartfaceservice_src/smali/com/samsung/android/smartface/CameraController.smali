.class public Lcom/samsung/android/smartface/CameraController;
.super Ljava/lang/Object;
.source "CameraController.java"

# interfaces
.implements Lcom/sec/android/seccamera/SecCamera$ErrorCallback;
.implements Lcom/sec/android/seccamera/SecCamera$PreviewCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/smartface/CameraController$Size;,
        Lcom/samsung/android/smartface/CameraController$EventHandler;,
        Lcom/samsung/android/smartface/CameraController$CameraListener;
    }
.end annotation


# static fields
.field public static final BUFFER_SIZE:I = 0x300000

.field public static final CHANGE_CAMERAMODE:I = 0x3

.field public static final CHANGE_EXPOSURE:I = 0x2

.field public static final NORMAL_CAMERA:I = 0x1

.field public static PreviewSize:Lcom/samsung/android/smartface/CameraController$Size; = null

.field public static final START_CAMERA:I = 0x1

.field public static final STOP_CAMERA:I = 0x0

.field private static final TAG:Ljava/lang/String; = "CameraController"

.field public static final VISON_CAMERA:I


# instance fields
.field final callback:Ljava/util/concurrent/locks/Condition;

.field callbackDone:Z

.field final complete:Ljava/util/concurrent/locks/Condition;

.field done:Z

.field final lock:Ljava/util/concurrent/locks/Lock;

.field final lock2:Ljava/util/concurrent/locks/Lock;

.field private mCallbackThread:Landroid/os/HandlerThread;

.field private mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

.field private mCameraMode:I

.field private mContext:Landroid/content/Context;

.field private mDPM:Landroid/app/admin/DevicePolicyManager;

.field private mDegreeOffset:I

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mListener:Lcom/samsung/android/smartface/CameraController$CameraListener;

.field private mMainHandler:Lcom/samsung/android/smartface/CameraController$EventHandler;

.field private mPreviewData:[B

.field private mRP:Landroid/app/enterprise/RestrictionPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 554
    new-instance v0, Lcom/samsung/android/smartface/CameraController$Size;

    invoke-direct {v0, v1, v1}, Lcom/samsung/android/smartface/CameraController$Size;-><init>(II)V

    sput-object v0, Lcom/samsung/android/smartface/CameraController;->PreviewSize:Lcom/samsung/android/smartface/CameraController$Size;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/smartface/CameraController$CameraListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/android/smartface/CameraController$CameraListener;

    .prologue
    const/4 v2, 0x0

    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/smartface/CameraController;->lock:Ljava/util/concurrent/locks/Lock;

    .line 72
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/smartface/CameraController;->complete:Ljava/util/concurrent/locks/Condition;

    .line 73
    iput-boolean v2, p0, Lcom/samsung/android/smartface/CameraController;->done:Z

    .line 75
    new-instance v1, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/smartface/CameraController;->lock2:Ljava/util/concurrent/locks/Lock;

    .line 76
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->lock2:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/smartface/CameraController;->callback:Ljava/util/concurrent/locks/Condition;

    .line 77
    iput-boolean v2, p0, Lcom/samsung/android/smartface/CameraController;->callbackDone:Z

    .line 144
    iput-object p1, p0, Lcom/samsung/android/smartface/CameraController;->mContext:Landroid/content/Context;

    .line 145
    iput-object p2, p0, Lcom/samsung/android/smartface/CameraController;->mListener:Lcom/samsung/android/smartface/CameraController$CameraListener;

    .line 148
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mContext:Landroid/content/Context;

    const-string v2, "device_policy"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/admin/DevicePolicyManager;

    iput-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mDPM:Landroid/app/admin/DevicePolicyManager;

    .line 150
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mContext:Landroid/content/Context;

    const-string v2, "enterprise_policy"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 151
    .local v0, "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mRP:Landroid/app/enterprise/RestrictionPolicy;

    .line 154
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mPreviewData:[B

    .line 156
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "CameraController Main Handler"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mHandlerThread:Landroid/os/HandlerThread;

    .line 157
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 159
    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "CameraController Callback Handler"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mCallbackThread:Landroid/os/HandlerThread;

    .line 160
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mCallbackThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    .line 162
    new-instance v1, Lcom/samsung/android/smartface/CameraController$EventHandler;

    iget-object v2, p0, Lcom/samsung/android/smartface/CameraController;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, p0, v2}, Lcom/samsung/android/smartface/CameraController$EventHandler;-><init>(Lcom/samsung/android/smartface/CameraController;Lcom/samsung/android/smartface/CameraController;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mMainHandler:Lcom/samsung/android/smartface/CameraController$EventHandler;

    .line 164
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/smartface/CameraController;)Lcom/sec/android/seccamera/SecCamera;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/CameraController;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/smartface/CameraController;Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/CameraController;
    .param p1, "x1"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/android/smartface/CameraController;)[B
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/CameraController;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->mPreviewData:[B

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/smartface/CameraController;[B)[B
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/CameraController;
    .param p1, "x1"    # [B

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/android/smartface/CameraController;->mPreviewData:[B

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/android/smartface/CameraController;)Landroid/os/HandlerThread;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/CameraController;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->mCallbackThread:Landroid/os/HandlerThread;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/smartface/CameraController;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/CameraController;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/smartface/CameraController;->mDegreeOffset:I

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/android/smartface/CameraController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/CameraController;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/samsung/android/smartface/CameraController;->mDegreeOffset:I

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/android/smartface/CameraController;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/CameraController;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/smartface/CameraController;->mCameraMode:I

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/android/smartface/CameraController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/CameraController;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/samsung/android/smartface/CameraController;->mCameraMode:I

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/android/smartface/CameraController;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/CameraController;
    .param p1, "x1"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/android/smartface/CameraController;->needVisionCamera(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/smartface/CameraController;)Lcom/samsung/android/smartface/CameraController$EventHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/CameraController;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->mMainHandler:Lcom/samsung/android/smartface/CameraController$EventHandler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/smartface/CameraController;)Lcom/samsung/android/smartface/CameraController$CameraListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/CameraController;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->mListener:Lcom/samsung/android/smartface/CameraController$CameraListener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/smartface/CameraController;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/CameraController;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/android/smartface/CameraController;->letGo()V

    return-void
.end method

.method private callbackArrived()V
    .locals 2

    .prologue
    .line 97
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->lock2:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/smartface/CameraController;->callbackDone:Z

    .line 100
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->callback:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->lock2:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 104
    return-void

    .line 102
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->lock2:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private letGo()V
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/smartface/CameraController;->done:Z

    .line 139
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->complete:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V

    .line 140
    return-void
.end method

.method private needVisionCamera(I)Z
    .locals 1
    .param p1, "service_type"    # I

    .prologue
    .line 214
    and-int/lit8 v0, p1, 0x33

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized sendMessageAndWait(IIILjava/lang/Object;)V
    .locals 5
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 234
    monitor-enter p0

    :try_start_0
    const-string v1, "CameraController"

    const-string v2, "sendMessageAndWait, lock.lock before"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 235
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 236
    const-string v1, "CameraController"

    const-string v2, "sendMessageAndWait, lock.lock after"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 239
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mMainHandler:Lcom/samsung/android/smartface/CameraController$EventHandler;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/samsung/android/smartface/CameraController$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 240
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mMainHandler:Lcom/samsung/android/smartface/CameraController$EventHandler;

    invoke-virtual {v1, v0}, Lcom/samsung/android/smartface/CameraController$EventHandler;->sendMessage(Landroid/os/Message;)Z

    .line 242
    const-string v1, "CameraController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wait for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 243
    invoke-direct {p0}, Lcom/samsung/android/smartface/CameraController;->waitHere()V

    .line 244
    const-string v1, "CameraController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wait for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " DONE!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246
    :try_start_2
    const-string v1, "CameraController"

    const-string v2, "sendMessageAndWait, lock.unlock before"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 247
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 248
    const-string v1, "CameraController"

    const-string v2, "sendMessageAndWait, lock.unlock after"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 250
    monitor-exit p0

    return-void

    .line 246
    .end local v0    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v1

    :try_start_3
    const-string v2, "CameraController"

    const-string v3, "sendMessageAndWait, lock.unlock before"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 247
    iget-object v2, p0, Lcom/samsung/android/smartface/CameraController;->lock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 248
    const-string v2, "CameraController"

    const-string v3, "sendMessageAndWait, lock.unlock after"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 234
    :catchall_1
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public static strToSize(Ljava/lang/String;)Lcom/samsung/android/smartface/CameraController$Size;
    .locals 6
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 568
    if-nez p0, :cond_1

    .line 578
    :cond_0
    :goto_0
    return-object v3

    .line 569
    :cond_1
    const-string v4, "auto"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 571
    const/16 v4, 0x78

    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 572
    .local v1, "pos":I
    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    .line 573
    const/4 v3, 0x0

    invoke-virtual {p0, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 574
    .local v2, "width":Ljava/lang/String;
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 575
    .local v0, "height":Ljava/lang/String;
    new-instance v3, Lcom/samsung/android/smartface/CameraController$Size;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-direct {v3, v4, v5}, Lcom/samsung/android/smartface/CameraController$Size;-><init>(II)V

    goto :goto_0
.end method

.method private waitForCallback()V
    .locals 4

    .prologue
    .line 84
    const-wide/32 v0, 0x11e1a300

    .line 85
    .local v0, "callback_timeout":J
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/android/smartface/CameraController;->callbackDone:Z

    .line 86
    :goto_0
    iget-boolean v2, p0, Lcom/samsung/android/smartface/CameraController;->callbackDone:Z

    if-nez v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 89
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/smartface/CameraController;->callback:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2, v0, v1}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 92
    :cond_0
    return-void

    .line 90
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private waitForMillis(I)V
    .locals 6
    .param p1, "millis"    # I

    .prologue
    .line 108
    mul-int/lit16 v2, p1, 0x3e8

    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    mul-long v0, v2, v4

    .line 111
    .local v0, "callback_timeout":J
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/smartface/CameraController;->complete:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2, v0, v1}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 113
    const-string v2, "CameraController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "waitForMillis, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms time out."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 116
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private waitHere()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 123
    const-string v4, "CameraController"

    const-string v5, "waitHere, condition var. will be used. related lock release and re-acquired after await done."

    invoke-static {v4, v5, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 124
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/smartface/CameraController;->done:Z

    .line 126
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 128
    .local v0, "time1":J
    :goto_0
    iget-boolean v4, p0, Lcom/samsung/android/smartface/CameraController;->done:Z

    if-nez v4, :cond_0

    .line 129
    const/16 v4, 0x3e8

    invoke-direct {p0, v4}, Lcom/samsung/android/smartface/CameraController;->waitForMillis(I)V

    goto :goto_0

    .line 131
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 133
    .local v2, "time2":J
    const-string v4, "CameraController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "waitHere, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long v6, v2, v0

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v8}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 134
    return-void
.end method


# virtual methods
.method public closeCamera()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 195
    const-string v0, "CameraController"

    const-string v1, "closeCamera S"

    invoke-static {v0, v1, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 196
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->mMainHandler:Lcom/samsung/android/smartface/CameraController$EventHandler;

    invoke-virtual {v0, v3}, Lcom/samsung/android/smartface/CameraController$EventHandler;->removeMessages(I)V

    .line 197
    const/4 v0, 0x0

    invoke-direct {p0, v2, v2, v2, v0}, Lcom/samsung/android/smartface/CameraController;->sendMessageAndWait(IIILjava/lang/Object;)V

    .line 198
    const-string v0, "CameraController"

    const-string v1, "closeCamera E"

    invoke-static {v0, v1, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 199
    return-void
.end method

.method public isCameraAllowed()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 168
    const/4 v0, 0x0

    .line 169
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mRP:Landroid/app/enterprise/RestrictionPolicy;

    invoke-virtual {v1, v4}, Landroid/app/enterprise/RestrictionPolicy;->isCameraEnabled(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 171
    const/4 v0, 0x1

    .line 175
    :cond_0
    const-string v1, "CameraController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isCameraAllowed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 176
    return v0
.end method

.method public isOpened()Z
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onError(ILcom/sec/android/seccamera/SecCamera;)V
    .locals 3
    .param p1, "error"    # I
    .param p2, "camera"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 268
    const-string v0, "CameraController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onError: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 270
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    .line 272
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->mListener:Lcom/samsung/android/smartface/CameraController$CameraListener;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->mListener:Lcom/samsung/android/smartface/CameraController$CameraListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/smartface/CameraController$CameraListener;->onError(I)V

    .line 276
    :cond_0
    return-void
.end method

.method public onPreviewFrame([BLcom/sec/android/seccamera/SecCamera;)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "camera"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 254
    const/4 v0, 0x0

    .line 256
    .local v0, "ret":Z
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mListener:Lcom/samsung/android/smartface/CameraController$CameraListener;

    if-eqz v1, :cond_0

    .line 258
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mListener:Lcom/samsung/android/smartface/CameraController$CameraListener;

    invoke-interface {v1, p1}, Lcom/samsung/android/smartface/CameraController$CameraListener;->onPreviewFrame([B)Z

    move-result v0

    .line 261
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    .line 262
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mCameraDevice:Lcom/sec/android/seccamera/SecCamera;

    iget-object v2, p0, Lcom/samsung/android/smartface/CameraController;->mPreviewData:[B

    invoke-virtual {v1, v2}, Lcom/sec/android/seccamera/SecCamera;->addCallbackBuffer([B)V

    .line 264
    :cond_1
    return-void
.end method

.method public openCamera(I)V
    .locals 4
    .param p1, "service_type"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 187
    const-string v0, "CameraController"

    const-string v1, "openCamera S"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 188
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->mMainHandler:Lcom/samsung/android/smartface/CameraController$EventHandler;

    invoke-virtual {v0, v3}, Lcom/samsung/android/smartface/CameraController$EventHandler;->removeMessages(I)V

    .line 189
    const/4 v0, 0x0

    invoke-direct {p0, v2, p1, v3, v0}, Lcom/samsung/android/smartface/CameraController;->sendMessageAndWait(IIILjava/lang/Object;)V

    .line 190
    const-string v0, "CameraController"

    const-string v1, "openCamera E"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 191
    return-void
.end method

.method public openCameraAsync()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 181
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->mMainHandler:Lcom/samsung/android/smartface/CameraController$EventHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/android/smartface/CameraController$EventHandler;->removeMessages(I)V

    .line 182
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->mMainHandler:Lcom/samsung/android/smartface/CameraController$EventHandler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/smartface/CameraController$EventHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 183
    return-void
.end method

.method public setSmartScreenExposueMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    const/4 v2, 0x2

    .line 208
    iget-object v0, p0, Lcom/samsung/android/smartface/CameraController;->mMainHandler:Lcom/samsung/android/smartface/CameraController$EventHandler;

    invoke-virtual {v0, v2}, Lcom/samsung/android/smartface/CameraController$EventHandler;->removeMessages(I)V

    .line 209
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v2, p1, v0, v1}, Lcom/samsung/android/smartface/CameraController;->sendMessageAndWait(IIILjava/lang/Object;)V

    .line 210
    return-void
.end method

.method public updateCameraMode(I)V
    .locals 3
    .param p1, "service_type"    # I

    .prologue
    const/4 v2, 0x3

    .line 221
    invoke-direct {p0, p1}, Lcom/samsung/android/smartface/CameraController;->needVisionCamera(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 222
    const/4 v0, 0x0

    .line 226
    .local v0, "mode":I
    :goto_0
    iget v1, p0, Lcom/samsung/android/smartface/CameraController;->mCameraMode:I

    if-eq v1, v0, :cond_0

    .line 228
    iget-object v1, p0, Lcom/samsung/android/smartface/CameraController;->mMainHandler:Lcom/samsung/android/smartface/CameraController$EventHandler;

    invoke-virtual {v1, v2}, Lcom/samsung/android/smartface/CameraController$EventHandler;->removeMessages(I)V

    .line 229
    const/4 v1, 0x0

    invoke-direct {p0, v2, v0, p1, v1}, Lcom/samsung/android/smartface/CameraController;->sendMessageAndWait(IIILjava/lang/Object;)V

    .line 231
    :cond_0
    return-void

    .line 224
    .end local v0    # "mode":I
    :cond_1
    const/4 v0, 0x1

    .restart local v0    # "mode":I
    goto :goto_0
.end method
