.class public Lcom/samsung/android/smartface/ScrollPauseWorker;
.super Lcom/samsung/android/smartface/Worker;
.source "ScrollPauseWorker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/smartface/ScrollPauseWorker$4;,
        Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;,
        Lcom/samsung/android/smartface/ScrollPauseWorker$SensorContextListener;,
        Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;
    }
.end annotation


# static fields
.field private static final DB_SMART_SCROLL:Ljava/lang/String; = "smart_scroll"

.field private static final INVALIDE_VALUE:I = -0x1

.field private static final NEED_TO_PAUSE:I = 0x1

.field private static final NEED_TO_PLAY:I

.field private static mAccSensor:Landroid/hardware/Sensor;

.field private static mSensorManager:Landroid/hardware/SensorManager;


# instance fields
.field private TrackerLock:Ljava/lang/Object;

.field private intent:Landroid/content/Intent;

.field private mAccSensorListener:Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;

.field private mAccelerometerRegistered:Z

.field private mAnimated:I

.field mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mCurrentMoveState:Z

.field private mCurrnetFlatState:Z

.field private mDimmedIconId:I

.field private mLastServiecTypeFromClient:I

.field private mLowLightDialg:Landroid/app/AlertDialog;

.field private mNormalIconId:I

.field private mPageStatus:I

.field private mPaused:I

.field private mPedometerRegistered:Z

.field private mSContextManager:Landroid/hardware/scontext/SContextManager;

.field private mSensorListener:Lcom/samsung/android/smartface/ScrollPauseWorker$SensorContextListener;

.field private mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

.field private mStepStatus:I

.field private mTracker:Lcom/sec/dmc/smartux/Tracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/samsung/android/smartface/SmartFaceIndicator;Lcom/samsung/android/smartface/CameraController;Landroid/os/Handler;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "service_type"    # I
    .param p4, "indicator"    # Lcom/samsung/android/smartface/SmartFaceIndicator;
    .param p5, "controller"    # Lcom/samsung/android/smartface/CameraController;
    .param p6, "listenerhandler"    # Landroid/os/Handler;

    .prologue
    const/4 v4, -0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 285
    invoke-direct/range {p0 .. p6}, Lcom/samsung/android/smartface/Worker;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/samsung/android/smartface/SmartFaceIndicator;Lcom/samsung/android/smartface/CameraController;Landroid/os/Handler;)V

    .line 69
    iput-object v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mTracker:Lcom/sec/dmc/smartux/Tracker;

    .line 70
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TrackerLock:Ljava/lang/Object;

    .line 75
    iput v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mPaused:I

    .line 76
    iput v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mAnimated:I

    .line 87
    iput v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mLastServiecTypeFromClient:I

    .line 89
    iput v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mPageStatus:I

    .line 95
    iput-boolean v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mPedometerRegistered:Z

    .line 103
    iput-boolean v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mAccelerometerRegistered:Z

    .line 272
    new-instance v2, Lcom/samsung/android/smartface/ScrollPauseWorker$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/smartface/ScrollPauseWorker$1;-><init>(Lcom/samsung/android/smartface/ScrollPauseWorker;)V

    iput-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 288
    sget-object v2, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->NOT_INIT:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    iput-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    .line 291
    iput-boolean v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrnetFlatState:Z

    .line 292
    iput-boolean v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrentMoveState:Z

    .line 295
    iput v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mStepStatus:I

    .line 297
    new-instance v2, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorContextListener;

    invoke-direct {v2, p0, v5}, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorContextListener;-><init>(Lcom/samsung/android/smartface/ScrollPauseWorker;Lcom/samsung/android/smartface/ScrollPauseWorker$1;)V

    iput-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSensorListener:Lcom/samsung/android/smartface/ScrollPauseWorker$SensorContextListener;

    .line 298
    new-instance v2, Landroid/hardware/scontext/SContextManager;

    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mListenerHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/hardware/scontext/SContextManager;-><init>(Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 300
    iget-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mContext:Landroid/content/Context;

    const-string v3, "sensor"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    sput-object v2, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSensorManager:Landroid/hardware/SensorManager;

    .line 301
    sget-object v2, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    sput-object v2, Lcom/samsung/android/smartface/ScrollPauseWorker;->mAccSensor:Landroid/hardware/Sensor;

    .line 302
    new-instance v2, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;

    invoke-direct {v2, p0, v5}, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;-><init>(Lcom/samsung/android/smartface/ScrollPauseWorker;Lcom/samsung/android/smartface/ScrollPauseWorker$1;)V

    iput-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mAccSensorListener:Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;

    .line 305
    iget-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mContentResolver:Landroid/content/ContentResolver;

    .line 307
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 308
    .local v0, "dialog_builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x10407e3

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 309
    const v2, 0x10407e4

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 310
    const v2, 0x10407e6

    new-instance v3, Lcom/samsung/android/smartface/ScrollPauseWorker$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/smartface/ScrollPauseWorker$2;-><init>(Lcom/samsung/android/smartface/ScrollPauseWorker;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 316
    const v2, 0x10407e5

    new-instance v3, Lcom/samsung/android/smartface/ScrollPauseWorker$3;

    invoke-direct {v3, p0}, Lcom/samsung/android/smartface/ScrollPauseWorker$3;-><init>(Lcom/samsung/android/smartface/ScrollPauseWorker;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 321
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mLowLightDialg:Landroid/app/AlertDialog;

    .line 322
    iget-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mLowLightDialg:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7d8

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    .line 324
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 325
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 327
    iget-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mListenerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v3, v1, v5, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 329
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/smartface/ScrollPauseWorker;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/ScrollPauseWorker;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TrackerLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/smartface/ScrollPauseWorker;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/ScrollPauseWorker;
    .param p1, "x1"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mStepStatus:I

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/smartface/ScrollPauseWorker;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/ScrollPauseWorker;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrnetFlatState:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/android/smartface/ScrollPauseWorker;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/ScrollPauseWorker;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrnetFlatState:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/smartface/ScrollPauseWorker;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/ScrollPauseWorker;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrentMoveState:Z

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/android/smartface/ScrollPauseWorker;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/ScrollPauseWorker;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrentMoveState:Z

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/android/smartface/ScrollPauseWorker;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/ScrollPauseWorker;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mLowLightDialg:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/smartface/ScrollPauseWorker;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/ScrollPauseWorker;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method private getResourceOrientation(I)I
    .locals 1
    .param p1, "UI_orientation"    # I

    .prologue
    .line 387
    packed-switch p1, :pswitch_data_0

    .line 395
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 390
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 393
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 387
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private isUserStop(I)Z
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 372
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    if-nez p1, :cond_1

    .line 375
    :cond_0
    const/4 v0, 0x1

    .line 378
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private registerAccelerometer()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 350
    iget-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v1, "regsiterAccelerometer"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    iput-boolean v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrnetFlatState:Z

    .line 353
    iput-boolean v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrentMoveState:Z

    .line 354
    iget-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mAccSensorListener:Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;

    invoke-virtual {v0}, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->resetInternalVariables()V

    .line 356
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mAccelerometerRegistered:Z

    .line 357
    sget-object v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mAccSensorListener:Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;

    sget-object v2, Lcom/samsung/android/smartface/ScrollPauseWorker;->mAccSensor:Landroid/hardware/Sensor;

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mListenerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 358
    return-void
.end method

.method private registerPedometer()V
    .locals 3

    .prologue
    .line 333
    iget-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v1, "regsiterPedometer"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mPedometerRegistered:Z

    .line 335
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mStepStatus:I

    .line 336
    iget-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSensorListener:Lcom/samsung/android/smartface/ScrollPauseWorker$SensorContextListener;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    .line 337
    return-void
.end method

.method private unregisterAccelerometer()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 362
    iget-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v1, "unregsiterAccelerometer"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    iput-boolean v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mAccelerometerRegistered:Z

    .line 364
    sget-object v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mAccSensorListener:Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 366
    iput-boolean v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrnetFlatState:Z

    .line 367
    iput-boolean v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrentMoveState:Z

    .line 368
    return-void
.end method

.method private unregisterPedometer()V
    .locals 2

    .prologue
    .line 341
    iget-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v1, "unregisterPedometer"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mPedometerRegistered:Z

    .line 343
    iget-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSensorListener:Lcom/samsung/android/smartface/ScrollPauseWorker$SensorContextListener;

    invoke-virtual {v0, v1}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;)V

    .line 345
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mStepStatus:I

    .line 346
    return-void
.end method


# virtual methods
.method protected Process(Lcom/samsung/android/smartface/Worker$PreviewData;)Lcom/samsung/android/smartface/Worker$Response;
    .locals 27
    .param p1, "data"    # Lcom/samsung/android/smartface/Worker$PreviewData;

    .prologue
    .line 448
    sget-boolean v4, Lcom/samsung/android/smartface/SmartFaceService;->mDumpPreview:Z

    if-eqz v4, :cond_0

    .line 450
    const/16 v20, 0x0

    .line 452
    .local v20, "out":Ljava/io/FileOutputStream;
    new-instance v16, Ljava/text/SimpleDateFormat;

    const-string v4, "MMddHHmmssSSS"

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 455
    .local v16, "dataFormat":Ljava/text/SimpleDateFormat;
    :try_start_0
    new-instance v21, Ljava/io/FileOutputStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/data/log/SP"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".yuv"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 458
    .end local v20    # "out":Ljava/io/FileOutputStream;
    .local v21, "out":Ljava/io/FileOutputStream;
    :try_start_1
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    const/4 v5, 0x0

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    array-length v6, v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v4, v5, v6}, Ljava/io/FileOutputStream;->write([BII)V

    .line 459
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 473
    .end local v16    # "dataFormat":Ljava/text/SimpleDateFormat;
    .end local v21    # "out":Ljava/io/FileOutputStream;
    :cond_0
    :goto_0
    const/16 v25, 0x0

    .line 476
    .local v25, "wakelock":Z
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TrackerLock:Ljava/lang/Object;

    move-object/from16 v26, v0

    monitor-enter v26
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 481
    :try_start_3
    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->bVisionMode:Z

    if-nez v4, :cond_1

    const/16 v23, 0x0

    monitor-exit v26
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 678
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    move/from16 v0, v25

    invoke-virtual {v4, v0}, Lcom/samsung/android/smartface/SmartFaceIndicator;->setWakeLock(Z)V

    :goto_1
    return-object v23

    .line 461
    .end local v25    # "wakelock":Z
    .restart local v16    # "dataFormat":Ljava/text/SimpleDateFormat;
    .restart local v21    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v17

    .line 462
    .local v17, "e":Ljava/io/IOException;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "Error IOException"

    move-object/from16 v0, v17

    invoke-static {v4, v5, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 463
    invoke-virtual/range {v21 .. v21}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 466
    .end local v17    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v17

    move-object/from16 v20, v21

    .line 467
    .end local v21    # "out":Ljava/io/FileOutputStream;
    .local v17, "e":Ljava/lang/Exception;
    .restart local v20    # "out":Ljava/io/FileOutputStream;
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "Error Exception"

    move-object/from16 v0, v17

    invoke-static {v4, v5, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 483
    .end local v16    # "dataFormat":Ljava/text/SimpleDateFormat;
    .end local v17    # "e":Ljava/lang/Exception;
    .end local v20    # "out":Ljava/io/FileOutputStream;
    .restart local v25    # "wakelock":Z
    :cond_1
    :try_start_5
    sget-object v4, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->NOT_INIT:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    if-ne v4, v5, :cond_2

    .line 485
    new-instance v4, Lcom/sec/dmc/smartux/Tracker;

    move-object/from16 v0, p1

    iget v5, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mWidth:I

    move-object/from16 v0, p1

    iget v6, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mHeight:I

    invoke-direct {v4, v5, v6}, Lcom/sec/dmc/smartux/Tracker;-><init>(II)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mTracker:Lcom/sec/dmc/smartux/Tracker;

    .line 486
    sget-object v4, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->INIT:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    .line 489
    :cond_2
    const/4 v13, 0x0

    .local v13, "bScroll":Z
    const/4 v12, 0x0

    .local v12, "bPause":Z
    const/4 v11, 0x0

    .local v11, "bMotion":Z
    const/4 v10, 0x0

    .line 490
    .local v10, "bHybrid":Z
    const/4 v15, 0x0

    .local v15, "client_count":I
    const/16 v22, 0x0

    .line 492
    .local v22, "paused_client_count":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mClientList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_3
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/smartface/SmartFaceService$Client;

    .line 494
    .local v14, "client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    add-int/lit8 v15, v15, 0x1

    .line 495
    invoke-virtual {v14}, Lcom/samsung/android/smartface/SmartFaceService$Client;->isPaused()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 497
    add-int/lit8 v22, v22, 0x1

    .line 498
    goto :goto_3

    .line 501
    :cond_4
    invoke-virtual {v14}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v4

    and-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mActiveServiceType:I

    and-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mLastServiecTypeFromClient:I

    and-int/2addr v4, v5

    if-eqz v4, :cond_5

    .line 502
    const/4 v13, 0x1

    .line 503
    :cond_5
    invoke-virtual {v14}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v4

    and-int/lit8 v4, v4, 0x2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mActiveServiceType:I

    and-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mLastServiecTypeFromClient:I

    and-int/2addr v4, v5

    if-eqz v4, :cond_6

    .line 504
    const/4 v12, 0x1

    .line 505
    :cond_6
    invoke-virtual {v14}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v4

    and-int/lit8 v4, v4, 0x10

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mActiveServiceType:I

    and-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mLastServiecTypeFromClient:I

    and-int/2addr v4, v5

    if-eqz v4, :cond_7

    .line 506
    const/4 v11, 0x1

    .line 507
    :cond_7
    invoke-virtual {v14}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v4

    and-int/lit8 v4, v4, 0x20

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mActiveServiceType:I

    and-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mLastServiecTypeFromClient:I

    and-int/2addr v4, v5

    if-eqz v4, :cond_3

    .line 508
    const/4 v10, 0x1

    goto :goto_3

    .line 511
    .end local v14    # "client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Before: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    invoke-virtual {v6}, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Client: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Paused: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 513
    sget-object v4, Lcom/samsung/android/smartface/ScrollPauseWorker$4;->$SwitchMap$com$samsung$android$smartface$ScrollPauseWorker$WORKER_STATE:[I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    invoke-virtual {v5}, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 542
    :cond_9
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "After: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    invoke-virtual {v6}, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    if-eqz v13, :cond_e

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mStepStatus:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/android/smartface/ScrollPauseWorker;->isUserStop(I)Z

    move-result v4

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrnetFlatState:Z

    if-nez v4, :cond_a

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrentMoveState:Z

    if-eqz v4, :cond_e

    .line 546
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "user not in stop position! or device flat or device in move. send null data"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    const/16 v23, 0x0

    monitor-exit v26
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 678
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    move/from16 v0, v25

    invoke-virtual {v4, v0}, Lcom/samsung/android/smartface/SmartFaceIndicator;->setWakeLock(Z)V

    goto/16 :goto_1

    .line 516
    :pswitch_0
    if-eqz v15, :cond_b

    .line 518
    :try_start_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "Start!"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mTracker:Lcom/sec/dmc/smartux/Tracker;

    move-object/from16 v0, p1

    iget v5, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mWidth:I

    move-object/from16 v0, p1

    iget v6, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mHeight:I

    invoke-virtual {v4, v5, v6}, Lcom/sec/dmc/smartux/Tracker;->startTracker(II)V

    .line 520
    sget-object v4, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->START:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    .line 524
    :cond_b
    :pswitch_1
    if-eqz v15, :cond_c

    move/from16 v0, v22

    if-eq v15, v0, :cond_d

    :cond_c
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mStepStatus:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/android/smartface/ScrollPauseWorker;->isUserStop(I)Z

    move-result v4

    if-eqz v4, :cond_d

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrnetFlatState:Z

    if-nez v4, :cond_d

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrentMoveState:Z

    if-eqz v4, :cond_9

    .line 526
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "Pause!"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mTracker:Lcom/sec/dmc/smartux/Tracker;

    invoke-virtual {v4}, Lcom/sec/dmc/smartux/Tracker;->pauseTracker()V

    .line 528
    sget-object v4, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->PAUSED:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    goto/16 :goto_4

    .line 676
    .end local v10    # "bHybrid":Z
    .end local v11    # "bMotion":Z
    .end local v12    # "bPause":Z
    .end local v13    # "bScroll":Z
    .end local v15    # "client_count":I
    .end local v18    # "i$":Ljava/util/Iterator;
    .end local v22    # "paused_client_count":I
    :catchall_0
    move-exception v4

    monitor-exit v26
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    throw v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 678
    :catchall_1
    move-exception v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Lcom/samsung/android/smartface/SmartFaceIndicator;->setWakeLock(Z)V

    throw v4

    .line 532
    .restart local v10    # "bHybrid":Z
    .restart local v11    # "bMotion":Z
    .restart local v12    # "bPause":Z
    .restart local v13    # "bScroll":Z
    .restart local v15    # "client_count":I
    .restart local v18    # "i$":Ljava/util/Iterator;
    .restart local v22    # "paused_client_count":I
    :pswitch_2
    if-eqz v15, :cond_9

    sub-int v4, v15, v22

    if-lez v4, :cond_9

    :try_start_8
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mStepStatus:I

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/samsung/android/smartface/ScrollPauseWorker;->isUserStop(I)Z

    move-result v4

    if-eqz v4, :cond_9

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrnetFlatState:Z

    if-nez v4, :cond_9

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrentMoveState:Z

    if-nez v4, :cond_9

    .line 534
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "Resume!"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mTracker:Lcom/sec/dmc/smartux/Tracker;

    invoke-virtual {v4}, Lcom/sec/dmc/smartux/Tracker;->resumeTracker()V

    .line 536
    sget-object v4, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->START:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    goto/16 :goto_4

    .line 550
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    sget-object v5, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->START:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    if-eq v4, v5, :cond_f

    .line 552
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "not in start state. no need to process."

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    const/16 v23, 0x0

    monitor-exit v26
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 678
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    move/from16 v0, v25

    invoke-virtual {v4, v0}, Lcom/samsung/android/smartface/SmartFaceIndicator;->setWakeLock(Z)V

    goto/16 :goto_1

    .line 557
    :cond_f
    :try_start_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mLowLightDialg:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 559
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "Low light dlg. is showing."

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    const/16 v23, 0x0

    monitor-exit v26
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 678
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    move/from16 v0, v25

    invoke-virtual {v4, v0}, Lcom/samsung/android/smartface/SmartFaceIndicator;->setWakeLock(Z)V

    goto/16 :goto_1

    .line 563
    :cond_10
    const/4 v9, 0x0

    .line 565
    .local v9, "mode":I
    if-eqz v13, :cond_11

    or-int/lit8 v9, v9, 0x1

    .line 566
    :cond_11
    if-eqz v12, :cond_12

    or-int/lit8 v9, v9, 0x10

    .line 567
    :cond_12
    if-eqz v11, :cond_13

    or-int/lit16 v9, v9, 0x100

    .line 568
    :cond_13
    if-eqz v10, :cond_14

    or-int/lit16 v9, v9, 0x1000

    .line 570
    :cond_14
    :try_start_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Running frame update with (0x"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v9}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    const/16 v24, 0x0

    .line 574
    .local v24, "um":Lcom/sec/dmc/smartux/DMCresponse;
    const/4 v8, 0x0

    .line 575
    .local v8, "cam_land":I
    if-eqz v9, :cond_15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mTracker:Lcom/sec/dmc/smartux/Tracker;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    move-object/from16 v0, p1

    iget v6, v0, Lcom/samsung/android/smartface/Worker$PreviewData;->mUIOrientation:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mPageStatus:I

    invoke-virtual/range {v4 .. v9}, Lcom/sec/dmc/smartux/Tracker;->frameUpdate([BIIII)Lcom/sec/dmc/smartux/DMCresponse;

    move-result-object v24

    .line 577
    :cond_15
    const/16 v23, 0x0

    .line 579
    .local v23, "ret":Lcom/samsung/android/smartface/Worker$Response;
    new-instance v19, Lcom/samsung/android/smartface/FaceInfo;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/android/smartface/FaceInfo;-><init>()V

    .line 581
    .local v19, "info":Lcom/samsung/android/smartface/FaceInfo;
    if-eqz v24, :cond_2a

    .line 582
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getState()Lcom/sec/dmc/smartux/DMCresponse$States;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/dmc/smartux/DMCresponse$States;->ordinal()I

    move-result v4

    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->processStatus:I

    .line 584
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ProcessStatus: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getState()Lcom/sec/dmc/smartux/DMCresponse$States;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/dmc/smartux/DMCresponse$States;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    iget v6, v0, Lcom/samsung/android/smartface/FaceInfo;->processStatus:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AE target: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getChangeSensorStatus()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getChangeSensorStatus()I

    move-result v4

    if-lez v4, :cond_18

    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getChangeSensorStatus()I

    move-result v4

    const/4 v5, 0x3

    if-ge v4, v5, :cond_18

    .line 588
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "Change AE target!"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCameraController:Lcom/samsung/android/smartface/CameraController;

    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getChangeSensorStatus()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/smartface/CameraController;->setSmartScreenExposueMode(I)V

    .line 592
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mTracker:Lcom/sec/dmc/smartux/Tracker;

    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getChangeSensorStatus()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/dmc/smartux/Tracker;->setSensorChanged(I)V

    .line 600
    :cond_16
    :goto_5
    if-nez v13, :cond_17

    if-nez v11, :cond_17

    if-eqz v10, :cond_19

    .line 602
    :cond_17
    move-object/from16 v0, v19

    iget-boolean v4, v0, Lcom/samsung/android/smartface/FaceInfo;->bLowLightBackLighting:Z

    if-eqz v4, :cond_19

    .line 603
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mLowLightDialg:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 604
    const/16 v23, 0x0

    monitor-exit v26
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 678
    .end local v23    # "ret":Lcom/samsung/android/smartface/Worker$Response;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    move/from16 v0, v25

    invoke-virtual {v4, v0}, Lcom/samsung/android/smartface/SmartFaceIndicator;->setWakeLock(Z)V

    goto/16 :goto_1

    .line 594
    .restart local v23    # "ret":Lcom/samsung/android/smartface/Worker$Response;
    :cond_18
    :try_start_b
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getChangeSensorStatus()I

    move-result v4

    const/16 v5, 0xa

    if-ne v4, v5, :cond_16

    .line 596
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "Low-light / backlinghting env."

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    const/4 v4, 0x1

    move-object/from16 v0, v19

    iput-boolean v4, v0, Lcom/samsung/android/smartface/FaceInfo;->bLowLightBackLighting:Z

    goto :goto_5

    .line 609
    :cond_19
    if-eqz v13, :cond_1b

    .line 611
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getResponseY()I

    move-result v4

    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->verticalMovement:I

    .line 612
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getResponseX()I

    move-result v4

    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->horizontalMovement:I

    .line 613
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getGuide()Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    move-result-object v4

    if-eqz v4, :cond_1a

    .line 614
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getGuide()Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;->ordinal()I

    move-result v4

    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->guideDir:I

    .line 615
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "info.guideDirections = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getGuide()Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    iget v6, v0, Lcom/samsung/android/smartface/FaceInfo;->guideDir:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "info. v: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    iget v6, v0, Lcom/samsung/android/smartface/FaceInfo;->verticalMovement:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " h: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    iget v6, v0, Lcom/samsung/android/smartface/FaceInfo;->horizontalMovement:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 621
    :cond_1b
    if-eqz v12, :cond_1c

    .line 623
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getPlayState()I

    move-result v4

    if-nez v4, :cond_22

    .line 624
    const/4 v4, 0x1

    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->needToPause:I

    .line 630
    :goto_6
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getFaceDistance()I

    move-result v4

    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->faceDistance:I

    .line 632
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "info.needToPause: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    iget v6, v0, Lcom/samsung/android/smartface/FaceInfo;->needToPause:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " faceDistance: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    iget v6, v0, Lcom/samsung/android/smartface/FaceInfo;->faceDistance:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    :cond_1c
    if-eqz v11, :cond_1d

    .line 638
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getState()Lcom/sec/dmc/smartux/DMCresponse$States;

    move-result-object v4

    sget-object v5, Lcom/sec/dmc/smartux/DMCresponse$States;->TRACKING:Lcom/sec/dmc/smartux/DMCresponse$States;

    if-ne v4, v5, :cond_24

    const/4 v4, 0x1

    :goto_7
    move-object/from16 v0, v19

    iput-boolean v4, v0, Lcom/samsung/android/smartface/FaceInfo;->bFaceDetected:Z

    .line 639
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "info.bFaceDetected: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    iget-boolean v6, v0, Lcom/samsung/android/smartface/FaceInfo;->bFaceDetected:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    :cond_1d
    if-eqz v10, :cond_1f

    .line 644
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "Hybrid"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getResponseY()I

    move-result v4

    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->verticalMovement:I

    .line 647
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getResponseX()I

    move-result v4

    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->horizontalMovement:I

    .line 648
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getGuide()Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    move-result-object v4

    if-eqz v4, :cond_1e

    .line 649
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getGuide()Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;->ordinal()I

    move-result v4

    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->guideDir:I

    .line 650
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "info.guideDirections = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getGuide()Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/dmc/smartux/DMCresponse$GuideDirections;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    iget v6, v0, Lcom/samsung/android/smartface/FaceInfo;->guideDir:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 652
    :cond_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "info. v: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    iget v6, v0, Lcom/samsung/android/smartface/FaceInfo;->verticalMovement:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " h: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    iget v6, v0, Lcom/samsung/android/smartface/FaceInfo;->horizontalMovement:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 654
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getState()Lcom/sec/dmc/smartux/DMCresponse$States;

    move-result-object v4

    sget-object v5, Lcom/sec/dmc/smartux/DMCresponse$States;->TRACKING:Lcom/sec/dmc/smartux/DMCresponse$States;

    if-ne v4, v5, :cond_25

    const/4 v4, 0x1

    :goto_8
    move-object/from16 v0, v19

    iput-boolean v4, v0, Lcom/samsung/android/smartface/FaceInfo;->bFaceDetected:Z

    .line 655
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "info.bFaceDetected: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v19

    iget-boolean v6, v0, Lcom/samsung/android/smartface/FaceInfo;->bFaceDetected:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    :cond_1f
    new-instance v23, Lcom/samsung/android/smartface/Worker$Response;

    .end local v23    # "ret":Lcom/samsung/android/smartface/Worker$Response;
    if-eqz v13, :cond_26

    const/4 v4, 0x1

    move v5, v4

    :goto_9
    if-eqz v12, :cond_27

    const/4 v4, 0x2

    :goto_a
    or-int/2addr v5, v4

    if-eqz v11, :cond_28

    const/16 v4, 0x10

    :goto_b
    or-int/2addr v5, v4

    if-eqz v10, :cond_29

    const/16 v4, 0x20

    :goto_c
    or-int/2addr v4, v5

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2, v4}, Lcom/samsung/android/smartface/Worker$Response;-><init>(Lcom/samsung/android/smartface/Worker;Lcom/samsung/android/smartface/FaceInfo;I)V

    .line 667
    .restart local v23    # "ret":Lcom/samsung/android/smartface/Worker$Response;
    :goto_d
    if-eqz v24, :cond_21

    if-nez v13, :cond_20

    if-nez v11, :cond_20

    if-eqz v10, :cond_21

    .line 669
    :cond_20
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getState()Lcom/sec/dmc/smartux/DMCresponse$States;

    move-result-object v4

    sget-object v5, Lcom/sec/dmc/smartux/DMCresponse$States;->NO_DETECTION:Lcom/sec/dmc/smartux/DMCresponse$States;

    if-eq v4, v5, :cond_21

    .line 670
    const/16 v25, 0x1

    .line 674
    :cond_21
    monitor-exit v26
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 678
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    move/from16 v0, v25

    invoke-virtual {v4, v0}, Lcom/samsung/android/smartface/SmartFaceIndicator;->setWakeLock(Z)V

    goto/16 :goto_1

    .line 625
    :cond_22
    const/4 v4, 0x1

    :try_start_c
    invoke-virtual/range {v24 .. v24}, Lcom/sec/dmc/smartux/DMCresponse;->getPlayState()I

    move-result v5

    if-ne v4, v5, :cond_23

    .line 626
    const/4 v4, 0x0

    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->needToPause:I

    goto/16 :goto_6

    .line 628
    :cond_23
    const/4 v4, -0x1

    move-object/from16 v0, v19

    iput v4, v0, Lcom/samsung/android/smartface/FaceInfo;->needToPause:I

    goto/16 :goto_6

    .line 638
    :cond_24
    const/4 v4, 0x0

    goto/16 :goto_7

    .line 654
    :cond_25
    const/4 v4, 0x0

    goto :goto_8

    .line 658
    .end local v23    # "ret":Lcom/samsung/android/smartface/Worker$Response;
    :cond_26
    const/4 v4, 0x0

    move v5, v4

    goto :goto_9

    :cond_27
    const/4 v4, 0x0

    goto :goto_a

    :cond_28
    const/4 v4, 0x0

    goto :goto_b

    :cond_29
    const/4 v4, 0x0

    goto :goto_c

    .line 663
    .restart local v23    # "ret":Lcom/samsung/android/smartface/Worker$Response;
    :cond_2a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "null or not in tracing state. skip."

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_d

    .line 466
    .end local v8    # "cam_land":I
    .end local v9    # "mode":I
    .end local v10    # "bHybrid":Z
    .end local v11    # "bMotion":Z
    .end local v12    # "bPause":Z
    .end local v13    # "bScroll":Z
    .end local v15    # "client_count":I
    .end local v18    # "i$":Ljava/util/Iterator;
    .end local v19    # "info":Lcom/samsung/android/smartface/FaceInfo;
    .end local v22    # "paused_client_count":I
    .end local v23    # "ret":Lcom/samsung/android/smartface/Worker$Response;
    .end local v24    # "um":Lcom/sec/dmc/smartux/DMCresponse;
    .end local v25    # "wakelock":Z
    .restart local v16    # "dataFormat":Ljava/text/SimpleDateFormat;
    .restart local v20    # "out":Ljava/io/FileOutputStream;
    :catch_2
    move-exception v17

    goto/16 :goto_2

    .line 513
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .prologue
    .line 685
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " mCurrnetFlatState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrnetFlatState:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 686
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " mCurrentMoveState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrentMoveState:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 687
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " mAccelerometerRegistered: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mAccelerometerRegistered:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 688
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " mPageStatus: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mPageStatus:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 689
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " mState: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    invoke-virtual {v1}, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 690
    return-void
.end method

.method public setLastServiecTypeFromClient(I)V
    .locals 0
    .param p1, "lastServiecTypeFromClient"    # I

    .prologue
    .line 383
    iput p1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mLastServiecTypeFromClient:I

    .line 384
    return-void
.end method

.method public setPageStatus(I)V
    .locals 0
    .param p1, "page_status"    # I

    .prologue
    .line 442
    iput p1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mPageStatus:I

    .line 443
    return-void
.end method

.method protected updateClientsDone()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 402
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v4, "updateClientsDone."

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    sget-object v4, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->START:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    sget-object v4, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->PAUSED:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    if-ne v3, v4, :cond_2

    .line 405
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mClientList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 407
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Client is empty. but the Traker in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    invoke-virtual {v5}, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". stop it."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TrackerLock:Ljava/lang/Object;

    monitor-enter v4

    .line 410
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mTracker:Lcom/sec/dmc/smartux/Tracker;

    if-eqz v3, :cond_1

    .line 412
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mTracker:Lcom/sec/dmc/smartux/Tracker;

    invoke-virtual {v3}, Lcom/sec/dmc/smartux/Tracker;->stopTracker()V

    .line 413
    sget-object v3, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->INIT:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    iput-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mState:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    .line 415
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 419
    :cond_2
    const/4 v0, 0x0

    .line 420
    .local v0, "bScroll":Z
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mClientList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/smartface/SmartFaceService$Client;

    .line 422
    .local v1, "client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    invoke-virtual {v1}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v3

    and-int/lit8 v3, v3, 0x1

    iget v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mActiveServiceType:I

    and-int/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mLastServiecTypeFromClient:I

    and-int/2addr v3, v4

    if-eqz v3, :cond_3

    .line 424
    const/4 v0, 0x1

    .line 433
    .end local v1    # "client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    :cond_4
    iget-boolean v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mAccelerometerRegistered:Z

    if-nez v3, :cond_8

    if-eqz v0, :cond_8

    invoke-direct {p0}, Lcom/samsung/android/smartface/ScrollPauseWorker;->registerAccelerometer()V

    .line 436
    :cond_5
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mClientList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    invoke-virtual {v3, v6}, Lcom/samsung/android/smartface/SmartFaceIndicator;->setWakeLock(Z)V

    .line 437
    :cond_6
    if-nez v0, :cond_7

    iput v6, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mPageStatus:I

    .line 438
    :cond_7
    return-void

    .line 415
    .end local v0    # "bScroll":Z
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 434
    .restart local v0    # "bScroll":Z
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_8
    iget-boolean v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker;->mAccelerometerRegistered:Z

    if-eqz v3, :cond_5

    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/samsung/android/smartface/ScrollPauseWorker;->unregisterAccelerometer()V

    goto :goto_0
.end method
