.class public abstract Lcom/samsung/android/smartface/Worker;
.super Landroid/os/HandlerThread;
.source "Worker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/smartface/Worker$Response;,
        Lcom/samsung/android/smartface/Worker$PreviewData;
    }
.end annotation


# static fields
.field protected static final PROCESS_DATA:I = 0x1


# instance fields
.field protected TAG:Ljava/lang/String;

.field protected mActiveServiceType:I

.field protected mCameraController:Lcom/samsung/android/smartface/CameraController;

.field protected mClientList:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/samsung/android/smartface/SmartFaceService$Client;",
            ">;"
        }
    .end annotation
.end field

.field protected mContext:Landroid/content/Context;

.field protected mData:Lcom/samsung/android/smartface/Worker$PreviewData;

.field protected mHandler:Landroid/os/Handler;

.field protected mListenerHandler:Landroid/os/Handler;

.field protected mPreviewDataLock:Ljava/lang/Object;

.field protected mServiceType:I

.field protected mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

.field protected mTempData:Lcom/samsung/android/smartface/Worker$PreviewData;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/samsung/android/smartface/SmartFaceIndicator;Lcom/samsung/android/smartface/CameraController;Landroid/os/Handler;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "service_type"    # I
    .param p4, "indicator"    # Lcom/samsung/android/smartface/SmartFaceIndicator;
    .param p5, "controller"    # Lcom/samsung/android/smartface/CameraController;
    .param p6, "listenerhandler"    # Landroid/os/Handler;

    .prologue
    const/4 v1, 0x0

    .line 115
    invoke-direct {p0, p2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 25
    const-string v0, "SmartFaceService.Worker"

    iput-object v0, p0, Lcom/samsung/android/smartface/Worker;->TAG:Ljava/lang/String;

    .line 40
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/smartface/Worker;->mPreviewDataLock:Ljava/lang/Object;

    .line 42
    iput-object v1, p0, Lcom/samsung/android/smartface/Worker;->mHandler:Landroid/os/Handler;

    .line 43
    iput-object v1, p0, Lcom/samsung/android/smartface/Worker;->mListenerHandler:Landroid/os/Handler;

    .line 117
    iput-object p2, p0, Lcom/samsung/android/smartface/Worker;->TAG:Ljava/lang/String;

    .line 119
    iput-object p1, p0, Lcom/samsung/android/smartface/Worker;->mContext:Landroid/content/Context;

    .line 120
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/smartface/Worker;->mClientList:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 121
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker;->mClientList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 123
    new-instance v0, Lcom/samsung/android/smartface/Worker$PreviewData;

    invoke-direct {v0, p0}, Lcom/samsung/android/smartface/Worker$PreviewData;-><init>(Lcom/samsung/android/smartface/Worker;)V

    iput-object v0, p0, Lcom/samsung/android/smartface/Worker;->mData:Lcom/samsung/android/smartface/Worker$PreviewData;

    .line 124
    new-instance v0, Lcom/samsung/android/smartface/Worker$PreviewData;

    invoke-direct {v0, p0}, Lcom/samsung/android/smartface/Worker$PreviewData;-><init>(Lcom/samsung/android/smartface/Worker;)V

    iput-object v0, p0, Lcom/samsung/android/smartface/Worker;->mTempData:Lcom/samsung/android/smartface/Worker$PreviewData;

    .line 125
    iput p3, p0, Lcom/samsung/android/smartface/Worker;->mServiceType:I

    .line 126
    iput-object p4, p0, Lcom/samsung/android/smartface/Worker;->mSmartFaceIndicator:Lcom/samsung/android/smartface/SmartFaceIndicator;

    .line 127
    iput-object p5, p0, Lcom/samsung/android/smartface/Worker;->mCameraController:Lcom/samsung/android/smartface/CameraController;

    .line 129
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/smartface/Worker;->mActiveServiceType:I

    .line 131
    invoke-virtual {p0}, Lcom/samsung/android/smartface/Worker;->start()V

    .line 133
    new-instance v0, Lcom/samsung/android/smartface/Worker$1;

    invoke-virtual {p0}, Lcom/samsung/android/smartface/Worker;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/smartface/Worker$1;-><init>(Lcom/samsung/android/smartface/Worker;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/smartface/Worker;->mHandler:Landroid/os/Handler;

    .line 153
    iput-object p6, p0, Lcom/samsung/android/smartface/Worker;->mListenerHandler:Landroid/os/Handler;

    .line 154
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/smartface/Worker;Lcom/samsung/android/smartface/Worker$Response;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartface/Worker;
    .param p1, "x1"    # Lcom/samsung/android/smartface/Worker$Response;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/samsung/android/smartface/Worker;->sendToClients(Lcom/samsung/android/smartface/Worker$Response;)V

    return-void
.end method

.method private sendToClients(Lcom/samsung/android/smartface/Worker$Response;)V
    .locals 5
    .param p1, "info"    # Lcom/samsung/android/smartface/Worker$Response;

    .prologue
    .line 203
    if-nez p1, :cond_1

    .line 205
    iget-object v2, p0, Lcom/samsung/android/smartface/Worker;->TAG:Ljava/lang/String;

    const-string v3, "Cannot send null data to client"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :cond_0
    return-void

    .line 210
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/smartface/Worker;->mClientList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/smartface/SmartFaceService$Client;

    .line 213
    .local v0, "client":Lcom/samsung/android/smartface/SmartFaceService$Client;
    invoke-virtual {v0}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v2

    iget v3, p1, Lcom/samsung/android/smartface/Worker$Response;->mServiceType:I

    and-int/2addr v2, v3

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/samsung/android/smartface/SmartFaceService$Client;->isPaused()Z

    move-result v2

    if-nez v2, :cond_2

    .line 214
    iget-object v2, p1, Lcom/samsung/android/smartface/Worker$Response;->mInfo:Lcom/samsung/android/smartface/FaceInfo;

    invoke-virtual {v0}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v3

    iget v4, p1, Lcom/samsung/android/smartface/Worker$Response;->mServiceType:I

    and-int/2addr v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Client;->sendDataToClient(Lcom/samsung/android/smartface/FaceInfo;I)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract Process(Lcom/samsung/android/smartface/Worker$PreviewData;)Lcom/samsung/android/smartface/Worker$Response;
.end method

.method public addClient(Lcom/samsung/android/smartface/SmartFaceService$Client;)V
    .locals 2
    .param p1, "client"    # Lcom/samsung/android/smartface/SmartFaceService$Client;

    .prologue
    .line 158
    iget v0, p0, Lcom/samsung/android/smartface/Worker;->mServiceType:I

    invoke-virtual {p1}, Lcom/samsung/android/smartface/SmartFaceService$Client;->getRequestedServiceType()I

    move-result v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker;->mClientList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker;->TAG:Ljava/lang/String;

    const-string v1, "client added"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    :cond_0
    return-void
.end method

.method public clearClients()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker;->mClientList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 174
    return-void
.end method

.method public clearMessages()V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/smartface/Worker;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 169
    :goto_0
    return-void

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker;->TAG:Ljava/lang/String;

    const-string v1, "mHandler is NULL."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 0
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .prologue
    .line 230
    return-void
.end method

.method public getSupportedServiceType()I
    .locals 1

    .prologue
    .line 225
    iget v0, p0, Lcom/samsung/android/smartface/Worker;->mServiceType:I

    return v0
.end method

.method public onPreviewFrame([BI)V
    .locals 3
    .param p1, "data"    # [B
    .param p2, "UI_orientation"    # I

    .prologue
    const/4 v2, 0x1

    .line 178
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker;->mClientList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    iget-object v1, p0, Lcom/samsung/android/smartface/Worker;->mPreviewDataLock:Ljava/lang/Object;

    monitor-enter v1

    .line 182
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker;->mData:Lcom/samsung/android/smartface/Worker$PreviewData;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/smartface/Worker$PreviewData;->assign([BI)Z

    move-result v0

    if-nez v0, :cond_1

    .line 184
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker;->TAG:Ljava/lang/String;

    const-string v2, "Preview size had been changed. Discard preview data."

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    monitor-exit v1

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 187
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 189
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 187
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setActiveServiceType(I)V
    .locals 0
    .param p1, "service_type"    # I

    .prologue
    .line 220
    iput p1, p0, Lcom/samsung/android/smartface/Worker;->mActiveServiceType:I

    .line 221
    return-void
.end method

.method protected updateClientsDone()V
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker;->TAG:Ljava/lang/String;

    const-string v1, "updateClientsDone. def. do nothing."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    return-void
.end method
