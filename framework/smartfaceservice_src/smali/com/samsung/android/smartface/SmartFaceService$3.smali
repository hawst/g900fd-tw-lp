.class Lcom/samsung/android/smartface/SmartFaceService$3;
.super Lcom/samsung/android/cover/CoverManager$StateListener;
.source "SmartFaceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/smartface/SmartFaceService;->initialize()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/smartface/SmartFaceService;


# direct methods
.method constructor <init>(Lcom/samsung/android/smartface/SmartFaceService;)V
    .locals 0

    .prologue
    .line 553
    iput-object p1, p0, Lcom/samsung/android/smartface/SmartFaceService$3;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    invoke-direct {p0}, Lcom/samsung/android/cover/CoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/cover/CoverState;)V
    .locals 5
    .param p1, "state"    # Lcom/samsung/android/cover/CoverState;

    .prologue
    const/4 v4, 0x1

    .line 555
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$3;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/smartface/SmartFaceService;->access$100(Lcom/samsung/android/smartface/SmartFaceService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 557
    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/android/cover/CoverState;->getSwitchState()Z

    move-result v0

    if-ne v0, v4, :cond_0

    .line 558
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$3;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mClearCoverOpened:Z
    invoke-static {v0, v2}, Lcom/samsung/android/smartface/SmartFaceService;->access$1302(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    .line 563
    :goto_0
    const-string v0, "SmartFaceService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "StateListener mClearCoverOpened: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/smartface/SmartFaceService$3;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mClearCoverOpened:Z
    invoke-static {v3}, Lcom/samsung/android/smartface/SmartFaceService;->access$1300(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 564
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$3;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mListenerHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/smartface/SmartFaceService;->access$400(Lcom/samsung/android/smartface/SmartFaceService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 566
    return-void

    .line 561
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$3;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mClearCoverOpened:Z
    invoke-static {v0, v2}, Lcom/samsung/android/smartface/SmartFaceService;->access$1302(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    goto :goto_0

    .line 564
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
