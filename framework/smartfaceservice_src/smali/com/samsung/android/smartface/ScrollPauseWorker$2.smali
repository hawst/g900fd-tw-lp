.class Lcom/samsung/android/smartface/ScrollPauseWorker$2;
.super Ljava/lang/Object;
.source "ScrollPauseWorker.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/smartface/ScrollPauseWorker;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/samsung/android/smartface/SmartFaceIndicator;Lcom/samsung/android/smartface/CameraController;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;


# direct methods
.method constructor <init>(Lcom/samsung/android/smartface/ScrollPauseWorker;)V
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$2;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 312
    iget-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$2;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    iget-object v0, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v1, "Turn off smart scroll."

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    iget-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$2;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    # getter for: Lcom/samsung/android/smartface/ScrollPauseWorker;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v0}, Lcom/samsung/android/smartface/ScrollPauseWorker;->access$700(Lcom/samsung/android/smartface/ScrollPauseWorker;)Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "smart_scroll"

    const/4 v2, 0x0

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 314
    return-void
.end method
