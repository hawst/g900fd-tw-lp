.class Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;
.super Ljava/lang/Object;
.source "SmartFaceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartface/SmartFaceService$Logger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LogItem"
.end annotation


# instance fields
.field private mString:Ljava/lang/String;

.field private mTimeStamp:J

.field final synthetic this$0:Lcom/samsung/android/smartface/SmartFaceService$Logger;


# direct methods
.method public constructor <init>(Lcom/samsung/android/smartface/SmartFaceService$Logger;Ljava/lang/String;J)V
    .locals 3
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "time"    # J

    .prologue
    .line 1042
    iput-object p1, p0, Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;->this$0:Lcom/samsung/android/smartface/SmartFaceService$Logger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1038
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;->mString:Ljava/lang/String;

    .line 1039
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;->mTimeStamp:J

    .line 1043
    iput-object p2, p0, Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;->mString:Ljava/lang/String;

    .line 1044
    iput-wide p3, p0, Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;->mTimeStamp:J

    .line 1045
    return-void
.end method

.method static synthetic access$1900(Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;

    .prologue
    .line 1037
    iget-wide v0, p0, Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;->mTimeStamp:J

    return-wide v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;

    .prologue
    .line 1037
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$Logger$LogItem;->mString:Ljava/lang/String;

    return-object v0
.end method
