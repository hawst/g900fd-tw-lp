.class Lcom/samsung/android/smartface/ScrollPauseWorker$1;
.super Landroid/content/BroadcastReceiver;
.source "ScrollPauseWorker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartface/ScrollPauseWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;


# direct methods
.method constructor <init>(Lcom/samsung/android/smartface/ScrollPauseWorker;)V
    .locals 0

    .prologue
    .line 272
    iput-object p1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$1;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 275
    iget-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$1;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    iget-object v0, v0, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceive: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    const-string v0, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$1;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    # getter for: Lcom/samsung/android/smartface/ScrollPauseWorker;->mLowLightDialg:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/android/smartface/ScrollPauseWorker;->access$400(Lcom/samsung/android/smartface/ScrollPauseWorker;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$1;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    # getter for: Lcom/samsung/android/smartface/ScrollPauseWorker;->mLowLightDialg:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/android/smartface/ScrollPauseWorker;->access$400(Lcom/samsung/android/smartface/ScrollPauseWorker;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 281
    :cond_0
    return-void
.end method
