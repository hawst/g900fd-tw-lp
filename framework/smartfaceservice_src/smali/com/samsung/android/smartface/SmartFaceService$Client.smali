.class public Lcom/samsung/android/smartface/SmartFaceService$Client;
.super Ljava/lang/Object;
.source "SmartFaceService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartface/SmartFaceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Client"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SmartFaceService.Client"


# instance fields
.field private mAppName:Ljava/lang/String;

.field private mCallingPid:I

.field private mClient:Lcom/samsung/android/smartface/ISmartFaceClient;

.field private mOwner:Lcom/samsung/android/smartface/SmartFaceService;

.field private mPaused:Z

.field private mRequestServiceType:I

.field final synthetic this$0:Lcom/samsung/android/smartface/SmartFaceService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/smartface/SmartFaceService;Lcom/samsung/android/smartface/ISmartFaceClient;Lcom/samsung/android/smartface/SmartFaceService;IILjava/lang/String;)V
    .locals 5
    .param p2, "client"    # Lcom/samsung/android/smartface/ISmartFaceClient;
    .param p3, "owner"    # Lcom/samsung/android/smartface/SmartFaceService;
    .param p4, "service_type"    # I
    .param p5, "pid"    # I
    .param p6, "appName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 250
    iput-object p1, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    iput-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mClient:Lcom/samsung/android/smartface/ISmartFaceClient;

    .line 242
    iput-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mOwner:Lcom/samsung/android/smartface/SmartFaceService;

    .line 243
    iput v4, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mRequestServiceType:I

    .line 251
    const-string v1, "SmartFaceService.Client"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "New SmartFaceService.Client, service type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p4}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " pid: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " appName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 253
    iput-object p2, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mClient:Lcom/samsung/android/smartface/ISmartFaceClient;

    .line 254
    iput-object p3, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mOwner:Lcom/samsung/android/smartface/SmartFaceService;

    .line 255
    iput p4, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mRequestServiceType:I

    .line 256
    iput p5, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mCallingPid:I

    .line 257
    iput-object p6, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mAppName:Ljava/lang/String;

    .line 258
    iput-boolean v4, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mPaused:Z

    .line 261
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mClient:Lcom/samsung/android/smartface/ISmartFaceClient;

    invoke-interface {v1}, Lcom/samsung/android/smartface/ISmartFaceClient;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    :goto_0
    return-void

    .line 262
    :catch_0
    move-exception v0

    .line 263
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SmartFaceService.Client"

    const-string v2, "linkToDeath failed."

    invoke-static {v1, v2, v0, v4}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Z)V

    goto :goto_0
.end method


# virtual methods
.method public binderDied()V
    .locals 3

    .prologue
    .line 343
    const-string v0, "SmartFaceService.Client"

    const-string v1, "SmartFaceService.Client died"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 345
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mOwner:Lcom/samsung/android/smartface/SmartFaceService;

    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mClient:Lcom/samsung/android/smartface/ISmartFaceClient;

    invoke-virtual {v0, v1}, Lcom/samsung/android/smartface/SmartFaceService;->unregister(Lcom/samsung/android/smartface/ISmartFaceClient;)V

    .line 346
    return-void
.end method

.method public dump(Ljava/io/PrintWriter;)V
    .locals 2
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .prologue
    .line 350
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "    [Client ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mClient:Lcom/samsung/android/smartface/ISmartFaceClient;

    invoke-interface {v1}, Lcom/samsung/android/smartface/ISmartFaceClient;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") ] Pid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mCallingPid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AppName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mAppName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 351
    return-void
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mAppName:Ljava/lang/String;

    return-object v0
.end method

.method public getCallingPid()I
    .locals 1

    .prologue
    .line 338
    iget v0, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mCallingPid:I

    return v0
.end method

.method public getRequestedServiceType()I
    .locals 1

    .prologue
    .line 330
    iget v0, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mRequestServiceType:I

    return v0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mPaused:Z

    return v0
.end method

.method public notifyRegisterStatus(Z)V
    .locals 5
    .param p1, "isRegister"    # Z

    .prologue
    .line 273
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    .line 276
    .local v1, "msg_type":I
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mClient:Lcom/samsung/android/smartface/ISmartFaceClient;

    const/4 v3, 0x0

    iget v4, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mRequestServiceType:I

    invoke-interface {v2, v1, v3, v4}, Lcom/samsung/android/smartface/ISmartFaceClient;->onInfo(ILcom/samsung/android/smartface/FaceInfo;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    :goto_1
    return-void

    .line 273
    .end local v1    # "msg_type":I
    :cond_0
    const/4 v1, 0x2

    goto :goto_0

    .line 277
    .restart local v1    # "msg_type":I
    :catch_0
    move-exception v0

    .line 278
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "SmartFaceService.Client"

    const-string v3, "notifyRegisterStatus failed."

    const/4 v4, 0x0

    invoke-static {v2, v3, v0, v4}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Z)V

    goto :goto_1
.end method

.method public sendDataToClient(Lcom/samsung/android/smartface/FaceInfo;I)V
    .locals 5
    .param p1, "data"    # Lcom/samsung/android/smartface/FaceInfo;
    .param p2, "service_type"    # I

    .prologue
    const/4 v4, 0x0

    .line 284
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mPauseEnabled:Z
    invoke-static {v1}, Lcom/samsung/android/smartface/SmartFaceService;->access$700(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mScrollEnabled:Z
    invoke-static {v1}, Lcom/samsung/android/smartface/SmartFaceService;->access$500(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mStayEnabled:Z
    invoke-static {v1}, Lcom/samsung/android/smartface/SmartFaceService;->access$800(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mRotationEnabled:Z
    invoke-static {v1}, Lcom/samsung/android/smartface/SmartFaceService;->access$900(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 285
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mClient:Lcom/samsung/android/smartface/ISmartFaceClient;

    const/4 v2, 0x0

    invoke-interface {v1, v2, p1, p2}, Lcom/samsung/android/smartface/ISmartFaceClient;->onInfo(ILcom/samsung/android/smartface/FaceInfo;I)V

    .line 292
    :goto_0
    return-void

    .line 287
    :cond_1
    const-string v1, "SmartFaceService.Client"

    const-string v2, "smart face service is disabled. Ignore FaceInfo."

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 289
    :catch_0
    move-exception v0

    .line 290
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SmartFaceService.Client"

    const-string v2, "sendDataToClient failed."

    invoke-static {v1, v2, v0, v4}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Z)V

    goto :goto_0
.end method

.method public setValue(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 295
    const-string v2, "paused-state"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 297
    const-string v2, "true"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 299
    iput-boolean v1, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mPaused:Z

    .line 310
    :goto_0
    const-string v2, "SmartFaceService.Client"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mPaused: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mPaused:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    move v0, v1

    .line 318
    :goto_1
    return v0

    .line 301
    :cond_0
    const-string v2, "false"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 303
    iput-boolean v0, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mPaused:Z

    goto :goto_0

    .line 307
    :cond_1
    const-string v1, "SmartFaceService.Client"

    const-string v2, "Wrong Command value from client. Ignore."

    invoke-static {v1, v2, v0}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1

    .line 314
    :cond_2
    const-string v1, "SmartFaceService.Client"

    const-string v2, "Wrong Command key from client. Ignore."

    invoke-static {v1, v2, v0}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method public unlink()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 323
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/smartface/SmartFaceService$Client;->mClient:Lcom/samsung/android/smartface/ISmartFaceClient;

    invoke-interface {v1}, Lcom/samsung/android/smartface/ISmartFaceClient;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    .line 327
    :goto_0
    return-void

    .line 324
    :catch_0
    move-exception v0

    .line 325
    .local v0, "e":Ljava/util/NoSuchElementException;
    const-string v1, "SmartFaceService.Client"

    const-string v2, "unlinkToDeath failed."

    invoke-static {v1, v2, v0, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Z)V

    goto :goto_0
.end method
