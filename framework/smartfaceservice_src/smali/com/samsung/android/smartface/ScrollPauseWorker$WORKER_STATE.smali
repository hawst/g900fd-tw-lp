.class final enum Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;
.super Ljava/lang/Enum;
.source "ScrollPauseWorker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartface/ScrollPauseWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "WORKER_STATE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

.field public static final enum INIT:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

.field public static final enum NOT_INIT:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

.field public static final enum PAUSED:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

.field public static final enum START:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 79
    new-instance v0, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    const-string v1, "NOT_INIT"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->NOT_INIT:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    new-instance v0, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    const-string v1, "INIT"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->INIT:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    new-instance v0, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    const-string v1, "START"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->START:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    new-instance v0, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->PAUSED:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    .line 78
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    sget-object v1, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->NOT_INIT:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->INIT:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->START:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->PAUSED:Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->$VALUES:[Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 78
    const-class v0, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->$VALUES:[Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    invoke-virtual {v0}, [Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/smartface/ScrollPauseWorker$WORKER_STATE;

    return-object v0
.end method
