.class Lcom/samsung/android/smartface/ScrollPauseWorker$SensorContextListener;
.super Ljava/lang/Object;
.source "ScrollPauseWorker.java"

# interfaces
.implements Landroid/hardware/scontext/SContextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartface/ScrollPauseWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorContextListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;


# direct methods
.method private constructor <init>(Lcom/samsung/android/smartface/ScrollPauseWorker;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorContextListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/smartface/ScrollPauseWorker;Lcom/samsung/android/smartface/ScrollPauseWorker$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/smartface/ScrollPauseWorker;
    .param p2, "x1"    # Lcom/samsung/android/smartface/ScrollPauseWorker$1;

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorContextListener;-><init>(Lcom/samsung/android/smartface/ScrollPauseWorker;)V

    return-void
.end method


# virtual methods
.method public onSContextChanged(Landroid/hardware/scontext/SContextEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/hardware/scontext/SContextEvent;

    .prologue
    .line 113
    iget-object v1, p1, Landroid/hardware/scontext/SContextEvent;->scontext:Landroid/hardware/scontext/SContext;

    .line 115
    .local v1, "scontext":Landroid/hardware/scontext/SContext;
    invoke-virtual {v1}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 117
    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getPedometerContext()Landroid/hardware/scontext/SContextPedometer;

    move-result-object v0

    .line 118
    .local v0, "pedometerContext":Landroid/hardware/scontext/SContextPedometer;
    iget-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorContextListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    iget-object v2, v2, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onSContextChanged: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/hardware/scontext/SContextPedometer;->getStepStatus()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iget-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorContextListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    # getter for: Lcom/samsung/android/smartface/ScrollPauseWorker;->TrackerLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/smartface/ScrollPauseWorker;->access$000(Lcom/samsung/android/smartface/ScrollPauseWorker;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 122
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorContextListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    invoke-virtual {v0}, Landroid/hardware/scontext/SContextPedometer;->getStepStatus()I

    move-result v4

    # setter for: Lcom/samsung/android/smartface/ScrollPauseWorker;->mStepStatus:I
    invoke-static {v2, v4}, Lcom/samsung/android/smartface/ScrollPauseWorker;->access$102(Lcom/samsung/android/smartface/ScrollPauseWorker;I)I

    .line 123
    monitor-exit v3

    .line 125
    .end local v0    # "pedometerContext":Landroid/hardware/scontext/SContextPedometer;
    :cond_0
    return-void

    .line 123
    .restart local v0    # "pedometerContext":Landroid/hardware/scontext/SContextPedometer;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
