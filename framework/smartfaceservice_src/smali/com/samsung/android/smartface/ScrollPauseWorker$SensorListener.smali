.class Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;
.super Ljava/lang/Object;
.source "ScrollPauseWorker.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartface/ScrollPauseWorker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensorListener"
.end annotation


# static fields
.field private static final MAX_DIFFERENCE:F = 0.2f

.field private static final MAX_TILTANGLE:I = 0x5

.field private static final MIN_ANGLEAVERAGE:I = 0x58

.field private static final RADIANS_TO_DEGREES:F = 57.29578f


# instance fields
.field private Accel:[F

.field private Angles:[I

.field private CurrentFlatState:Z

.field private CurrentMoveState_Accel:Z

.field private FlatCount:I

.field private MAX_MOVE_FRAME:I

.field private MOVE_THRESHOLD:F

.field private Speeds:[F

.field private isFirstFrame_Accel:Z

.field private prevGravity:[F

.field final synthetic this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;


# direct methods
.method private constructor <init>(Lcom/samsung/android/smartface/ScrollPauseWorker;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 128
    iput-object p1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Angles:[I

    .line 131
    iput-boolean v1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentFlatState:Z

    .line 132
    iput v1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->FlatCount:I

    .line 139
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->MAX_MOVE_FRAME:I

    .line 140
    new-array v0, v2, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->prevGravity:[F

    .line 141
    new-array v0, v2, [F

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Accel:[F

    .line 142
    iget v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->MAX_MOVE_FRAME:I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Speeds:[F

    .line 143
    const v0, 0x3f19999a    # 0.6f

    iput v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->MOVE_THRESHOLD:F

    .line 144
    iput-boolean v1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentMoveState_Accel:Z

    .line 145
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->isFirstFrame_Accel:Z

    return-void

    .line 130
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 140
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 141
    :array_2
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method synthetic constructor <init>(Lcom/samsung/android/smartface/ScrollPauseWorker;Lcom/samsung/android/smartface/ScrollPauseWorker$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/smartface/ScrollPauseWorker;
    .param p2, "x1"    # Lcom/samsung/android/smartface/ScrollPauseWorker$1;

    .prologue
    .line 128
    invoke-direct {p0, p1}, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;-><init>(Lcom/samsung/android/smartface/ScrollPauseWorker;)V

    return-void
.end method

.method private isFlat(FFF)V
    .locals 10
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 156
    mul-float v5, p1, p1

    mul-float v6, p2, p2

    add-float/2addr v5, v6

    mul-float v6, p3, p3

    add-float/2addr v5, v6

    invoke-static {v5}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v3

    .line 157
    .local v3, "magnitude":F
    div-float v5, p3, v3

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->asin(D)D

    move-result-wide v6

    const-wide v8, 0x404ca5dc20000000L    # 57.295780181884766

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v4, v6

    .line 158
    .local v4, "tiltAngle":I
    iget-object v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Angles:[I

    const/16 v6, 0x9

    aget v5, v5, v6

    sub-int v5, v4, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-float v1, v5

    .line 159
    .local v1, "differAverage":F
    iget-object v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Angles:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    int-to-float v0, v5

    .line 161
    .local v0, "angleAverage":F
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_0
    const/16 v5, 0xa

    if-ge v2, v5, :cond_0

    .line 163
    iget-object v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Angles:[I

    aget v5, v5, v2

    iget-object v6, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Angles:[I

    add-int/lit8 v7, v2, -0x1

    aget v6, v6, v7

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v1, v5

    .line 164
    iget-object v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Angles:[I

    aget v5, v5, v2

    int-to-float v5, v5

    add-float/2addr v0, v5

    .line 161
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 166
    :cond_0
    const/high16 v5, 0x41200000    # 10.0f

    div-float/2addr v1, v5

    .line 167
    const/high16 v5, 0x41200000    # 10.0f

    div-float/2addr v0, v5

    .line 169
    const/16 v5, 0x55

    if-le v5, v4, :cond_1

    const/16 v5, -0x55

    if-gt v4, v5, :cond_4

    .line 170
    :cond_1
    iget v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->FlatCount:I

    const/16 v6, 0xa

    if-lt v5, v6, :cond_3

    const/16 v5, 0xa

    :goto_1
    iput v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->FlatCount:I

    .line 174
    :goto_2
    iget-boolean v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentFlatState:Z

    if-nez v5, :cond_7

    iget v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->FlatCount:I

    const/16 v6, 0xa

    if-ne v5, v6, :cond_7

    .line 176
    const/high16 v5, 0x42b00000    # 88.0f

    cmpl-float v5, v0, v5

    if-ltz v5, :cond_6

    .line 177
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentFlatState:Z

    .line 189
    :cond_2
    :goto_3
    const/4 v2, 0x1

    :goto_4
    const/16 v5, 0xa

    if-ge v2, v5, :cond_9

    .line 190
    iget-object v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Angles:[I

    add-int/lit8 v6, v2, -0x1

    iget-object v7, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Angles:[I

    aget v7, v7, v2

    aput v7, v5, v6

    .line 189
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 170
    :cond_3
    iget v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->FlatCount:I

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 172
    :cond_4
    iget v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->FlatCount:I

    if-gtz v5, :cond_5

    const/4 v5, 0x0

    :goto_5
    iput v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->FlatCount:I

    goto :goto_2

    :cond_5
    iget v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->FlatCount:I

    add-int/lit8 v5, v5, -0x1

    goto :goto_5

    .line 178
    :cond_6
    const v5, 0x3e4ccccd    # 0.2f

    cmpg-float v5, v1, v5

    if-gez v5, :cond_2

    .line 179
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentFlatState:Z

    goto :goto_3

    .line 181
    :cond_7
    iget-boolean v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentFlatState:Z

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 183
    iget v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->FlatCount:I

    const/4 v6, 0x6

    if-gt v5, v6, :cond_8

    .line 184
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentFlatState:Z

    goto :goto_3

    .line 185
    :cond_8
    const/high16 v5, 0x42b00000    # 88.0f

    cmpg-float v5, v0, v5

    if-gez v5, :cond_2

    const v5, 0x3e4ccccd    # 0.2f

    cmpl-float v5, v1, v5

    if-ltz v5, :cond_2

    .line 186
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentFlatState:Z

    goto :goto_3

    .line 192
    :cond_9
    iget-object v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Angles:[I

    const/16 v6, 0x9

    aput v4, v5, v6

    .line 193
    return-void
.end method

.method private isMove(FFF)V
    .locals 12
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    const v8, 0x3f4ccccd    # 0.8f

    const v7, 0x3e4ccccc    # 0.19999999f

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 197
    const v1, 0x3f4ccccd    # 0.8f

    .line 198
    .local v1, "alpha":F
    iget-boolean v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->isFirstFrame_Accel:Z

    if-eqz v4, :cond_0

    .line 200
    mul-float v4, p1, p1

    mul-float v5, p2, p2

    add-float/2addr v4, v5

    mul-float v5, p3, p3

    add-float/2addr v4, v5

    invoke-static {v4}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v3

    .line 201
    .local v3, "magnitude":F
    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->prevGravity:[F

    aput p1, v4, v9

    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->prevGravity:[F

    aput p2, v4, v10

    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->prevGravity:[F

    aput p3, v4, v11

    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->prevGravity:[F

    const/4 v5, 0x3

    aput v3, v4, v5

    .line 202
    iput-boolean v9, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->isFirstFrame_Accel:Z

    .line 228
    .end local v3    # "magnitude":F
    :goto_0
    return-void

    .line 206
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->prevGravity:[F

    iget-object v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->prevGravity:[F

    aget v5, v5, v9

    mul-float/2addr v5, v8

    mul-float v6, v7, p1

    add-float/2addr v5, v6

    aput v5, v4, v9

    .line 207
    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->prevGravity:[F

    iget-object v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->prevGravity:[F

    aget v5, v5, v10

    mul-float/2addr v5, v8

    mul-float v6, v7, p2

    add-float/2addr v5, v6

    aput v5, v4, v10

    .line 208
    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->prevGravity:[F

    iget-object v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->prevGravity:[F

    aget v5, v5, v11

    mul-float/2addr v5, v8

    mul-float v6, v7, p3

    add-float/2addr v5, v6

    aput v5, v4, v11

    .line 209
    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Accel:[F

    iget-object v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->prevGravity:[F

    aget v5, v5, v9

    sub-float v5, p1, v5

    aput v5, v4, v9

    .line 210
    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Accel:[F

    iget-object v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->prevGravity:[F

    aget v5, v5, v10

    sub-float v5, p2, v5

    aput v5, v4, v10

    .line 211
    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Accel:[F

    iget-object v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->prevGravity:[F

    aget v5, v5, v11

    sub-float v5, p3, v5

    aput v5, v4, v11

    .line 212
    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Accel:[F

    const/4 v5, 0x3

    iget-object v6, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Accel:[F

    aget v6, v6, v9

    iget-object v7, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Accel:[F

    aget v7, v7, v9

    mul-float/2addr v6, v7

    iget-object v7, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Accel:[F

    aget v7, v7, v10

    iget-object v8, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Accel:[F

    aget v8, v8, v10

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Accel:[F

    aget v7, v7, v11

    iget-object v8, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Accel:[F

    aget v8, v8, v11

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    invoke-static {v6}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v6

    invoke-static {v6}, Landroid/util/FloatMath;->floor(F)F

    move-result v6

    aput v6, v4, v5

    .line 214
    const/4 v0, 0x0

    .line 215
    .local v0, "AverageSpeed":F
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_1
    iget v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->MAX_MOVE_FRAME:I

    if-ge v2, v4, :cond_1

    .line 217
    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Speeds:[F

    add-int/lit8 v5, v2, -0x1

    iget-object v6, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Speeds:[F

    aget v6, v6, v2

    aput v6, v4, v5

    .line 218
    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Speeds:[F

    add-int/lit8 v5, v2, -0x1

    aget v4, v4, v5

    add-float/2addr v0, v4

    .line 215
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 220
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Speeds:[F

    iget v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->MAX_MOVE_FRAME:I

    add-int/lit8 v5, v5, -0x1

    iget-object v6, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Accel:[F

    const/4 v7, 0x3

    aget v6, v6, v7

    aput v6, v4, v5

    .line 221
    iget-object v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->Speeds:[F

    iget v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->MAX_MOVE_FRAME:I

    add-int/lit8 v5, v5, -0x1

    aget v4, v4, v5

    add-float/2addr v4, v0

    iget v5, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->MAX_MOVE_FRAME:I

    int-to-float v5, v5

    div-float v0, v4, v5

    .line 223
    iget v4, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->MOVE_THRESHOLD:F

    cmpl-float v4, v0, v4

    if-lez v4, :cond_2

    .line 224
    iput-boolean v10, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentMoveState_Accel:Z

    goto/16 :goto_0

    .line 226
    :cond_2
    iput-boolean v9, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentMoveState_Accel:Z

    goto/16 :goto_0
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 231
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 234
    iget-object v3, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v3}, Landroid/hardware/Sensor;->getType()I

    move-result v3

    if-ne v3, v4, :cond_2

    .line 235
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v0, v3, v5

    .line 236
    .local v0, "x":F
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v3, v4

    .line 237
    .local v1, "y":F
    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x2

    aget v2, v3, v4

    .line 239
    .local v2, "z":F
    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->isFlat(FFF)V

    .line 240
    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->isMove(FFF)V

    .line 242
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    # getter for: Lcom/samsung/android/smartface/ScrollPauseWorker;->TrackerLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/samsung/android/smartface/ScrollPauseWorker;->access$000(Lcom/samsung/android/smartface/ScrollPauseWorker;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 245
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    # getter for: Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrnetFlatState:Z
    invoke-static {v3}, Lcom/samsung/android/smartface/ScrollPauseWorker;->access$200(Lcom/samsung/android/smartface/ScrollPauseWorker;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-boolean v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentFlatState:Z

    if-eqz v3, :cond_3

    .line 247
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    const/4 v5, 0x1

    # setter for: Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrnetFlatState:Z
    invoke-static {v3, v5}, Lcom/samsung/android/smartface/ScrollPauseWorker;->access$202(Lcom/samsung/android/smartface/ScrollPauseWorker;Z)Z

    .line 248
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    iget-object v3, v3, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "Changed to flat!"

    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    # getter for: Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrentMoveState:Z
    invoke-static {v3}, Lcom/samsung/android/smartface/ScrollPauseWorker;->access$300(Lcom/samsung/android/smartface/ScrollPauseWorker;)Z

    move-result v3

    if-nez v3, :cond_4

    iget-boolean v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentMoveState_Accel:Z

    if-eqz v3, :cond_4

    .line 259
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    const/4 v5, 0x1

    # setter for: Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrentMoveState:Z
    invoke-static {v3, v5}, Lcom/samsung/android/smartface/ScrollPauseWorker;->access$302(Lcom/samsung/android/smartface/ScrollPauseWorker;Z)Z

    .line 260
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    iget-object v3, v3, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "Changed to move!"

    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    :cond_1
    :goto_1
    monitor-exit v4

    .line 269
    .end local v0    # "x":F
    .end local v1    # "y":F
    .end local v2    # "z":F
    :cond_2
    return-void

    .line 250
    .restart local v0    # "x":F
    .restart local v1    # "y":F
    .restart local v2    # "z":F
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    # getter for: Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrnetFlatState:Z
    invoke-static {v3}, Lcom/samsung/android/smartface/ScrollPauseWorker;->access$200(Lcom/samsung/android/smartface/ScrollPauseWorker;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentFlatState:Z

    if-nez v3, :cond_0

    .line 252
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    const/4 v5, 0x0

    # setter for: Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrnetFlatState:Z
    invoke-static {v3, v5}, Lcom/samsung/android/smartface/ScrollPauseWorker;->access$202(Lcom/samsung/android/smartface/ScrollPauseWorker;Z)Z

    .line 253
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    iget-object v3, v3, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "Changed to non-flat!"

    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 267
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 262
    :cond_4
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    # getter for: Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrentMoveState:Z
    invoke-static {v3}, Lcom/samsung/android/smartface/ScrollPauseWorker;->access$300(Lcom/samsung/android/smartface/ScrollPauseWorker;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentMoveState_Accel:Z

    if-nez v3, :cond_1

    .line 264
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    const/4 v5, 0x0

    # setter for: Lcom/samsung/android/smartface/ScrollPauseWorker;->mCurrentMoveState:Z
    invoke-static {v3, v5}, Lcom/samsung/android/smartface/ScrollPauseWorker;->access$302(Lcom/samsung/android/smartface/ScrollPauseWorker;Z)Z

    .line 265
    iget-object v3, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->this$0:Lcom/samsung/android/smartface/ScrollPauseWorker;

    iget-object v3, v3, Lcom/samsung/android/smartface/ScrollPauseWorker;->TAG:Ljava/lang/String;

    const-string v5, "Changed to non-move!"

    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public resetInternalVariables()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 149
    iput v1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->FlatCount:I

    .line 150
    iput-boolean v1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentFlatState:Z

    .line 151
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->isFirstFrame_Accel:Z

    .line 152
    iput-boolean v1, p0, Lcom/samsung/android/smartface/ScrollPauseWorker$SensorListener;->CurrentMoveState_Accel:Z

    .line 153
    return-void
.end method
