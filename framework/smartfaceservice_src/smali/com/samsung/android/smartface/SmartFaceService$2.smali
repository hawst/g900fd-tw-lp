.class Lcom/samsung/android/smartface/SmartFaceService$2;
.super Landroid/os/Handler;
.source "SmartFaceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/smartface/SmartFaceService;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/smartface/SmartFaceService;


# direct methods
.method constructor <init>(Lcom/samsung/android/smartface/SmartFaceService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 477
    iput-object p1, p0, Lcom/samsung/android/smartface/SmartFaceService$2;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 480
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 492
    :goto_0
    return-void

    .line 482
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$2;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/smartface/SmartFaceService;->access$100(Lcom/samsung/android/smartface/SmartFaceService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 484
    :try_start_0
    const-string v0, "SmartFaceService"

    const-string v2, "Update service state according to cover state"

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 485
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$2;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # invokes: Lcom/samsung/android/smartface/SmartFaceService;->updateActiveServiceTypeLocked()V
    invoke-static {v0}, Lcom/samsung/android/smartface/SmartFaceService;->access$1000(Lcom/samsung/android/smartface/SmartFaceService;)V

    .line 486
    iget-object v0, p0, Lcom/samsung/android/smartface/SmartFaceService$2;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # invokes: Lcom/samsung/android/smartface/SmartFaceService;->updateClientStatusLocked()V
    invoke-static {v0}, Lcom/samsung/android/smartface/SmartFaceService;->access$300(Lcom/samsung/android/smartface/SmartFaceService;)V

    .line 487
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 480
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
