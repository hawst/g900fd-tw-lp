.class public Lcom/samsung/android/smartface/SampleWorker;
.super Lcom/samsung/android/smartface/Worker;
.source "SampleWorker.java"


# instance fields
.field private mValue:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/samsung/android/smartface/SmartFaceIndicator;Lcom/samsung/android/smartface/CameraController;Landroid/os/Handler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "service_type"    # I
    .param p4, "indicator"    # Lcom/samsung/android/smartface/SmartFaceIndicator;
    .param p5, "controller"    # Lcom/samsung/android/smartface/CameraController;
    .param p6, "listenerhandler"    # Landroid/os/Handler;

    .prologue
    .line 21
    invoke-direct/range {p0 .. p6}, Lcom/samsung/android/smartface/Worker;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/samsung/android/smartface/SmartFaceIndicator;Lcom/samsung/android/smartface/CameraController;Landroid/os/Handler;)V

    .line 17
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/smartface/SampleWorker;->mValue:I

    .line 22
    return-void
.end method


# virtual methods
.method protected Process(Lcom/samsung/android/smartface/Worker$PreviewData;)Lcom/samsung/android/smartface/Worker$Response;
    .locals 3
    .param p1, "data"    # Lcom/samsung/android/smartface/Worker$PreviewData;

    .prologue
    .line 27
    new-instance v0, Lcom/samsung/android/smartface/FaceInfo;

    invoke-direct {v0}, Lcom/samsung/android/smartface/FaceInfo;-><init>()V

    .line 29
    .local v0, "ret":Lcom/samsung/android/smartface/FaceInfo;
    iget v1, p0, Lcom/samsung/android/smartface/SampleWorker;->mValue:I

    iput v1, v0, Lcom/samsung/android/smartface/FaceInfo;->horizontalMovement:I

    .line 30
    iget v1, p0, Lcom/samsung/android/smartface/SampleWorker;->mValue:I

    iput v1, v0, Lcom/samsung/android/smartface/FaceInfo;->verticalMovement:I

    .line 31
    iget v1, p0, Lcom/samsung/android/smartface/SampleWorker;->mValue:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/android/smartface/SampleWorker;->mValue:I

    .line 33
    new-instance v1, Lcom/samsung/android/smartface/Worker$Response;

    invoke-virtual {p0}, Lcom/samsung/android/smartface/SampleWorker;->getSupportedServiceType()I

    move-result v2

    invoke-direct {v1, p0, v0, v2}, Lcom/samsung/android/smartface/Worker$Response;-><init>(Lcom/samsung/android/smartface/Worker;Lcom/samsung/android/smartface/FaceInfo;I)V

    return-object v1
.end method
