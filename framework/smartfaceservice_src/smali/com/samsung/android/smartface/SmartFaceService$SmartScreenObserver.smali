.class Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;
.super Landroid/database/ContentObserver;
.source "SmartFaceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartface/SmartFaceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SmartScreenObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/smartface/SmartFaceService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/smartface/SmartFaceService;)V
    .locals 1

    .prologue
    .line 198
    iput-object p1, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    .line 199
    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mListenerHandler:Landroid/os/Handler;
    invoke-static {p1}, Lcom/samsung/android/smartface/SmartFaceService;->access$400(Lcom/samsung/android/smartface/SmartFaceService;)Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 200
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 12
    .param p1, "selfChange"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 203
    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;
    invoke-static {v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$100(Lcom/samsung/android/smartface/SmartFaceService;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 206
    :try_start_0
    iget-object v8, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$600(Lcom/samsung/android/smartface/SmartFaceService;)Landroid/content/ContentResolver;

    move-result-object v6

    const-string v9, "smart_scroll"

    const/4 v10, 0x0

    const/4 v11, -0x2

    invoke-static {v6, v9, v10, v11}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    if-ne v6, v4, :cond_0

    move v6, v4

    :goto_0
    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mScrollEnabled:Z
    invoke-static {v8, v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$502(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    .line 208
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v6

    const-string v8, "SEC_FLOATING_FEATURE_SETTINGS_MOTION_CONCEPT_2014"

    invoke-virtual {v6, v8}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 210
    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$600(Lcom/samsung/android/smartface/SmartFaceService;)Landroid/content/ContentResolver;

    move-result-object v6

    const-string v8, "motion_merged_mute_pause"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v6, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    if-lez v6, :cond_1

    move v0, v4

    .line 211
    .local v0, "mute_pause":Z
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$600(Lcom/samsung/android/smartface/SmartFaceService;)Landroid/content/ContentResolver;

    move-result-object v6

    const-string v8, "smart_pause"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v6, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    if-ne v6, v4, :cond_2

    move v2, v4

    .line 212
    .local v2, "smart_pause":Z
    :goto_2
    iget-object v8, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    move v6, v4

    :goto_3
    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mPauseEnabled:Z
    invoke-static {v8, v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$702(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    .line 219
    .end local v0    # "mute_pause":Z
    .end local v2    # "smart_pause":Z
    :goto_4
    iget-object v8, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$600(Lcom/samsung/android/smartface/SmartFaceService;)Landroid/content/ContentResolver;

    move-result-object v6

    const-string v9, "intelligent_sleep_mode"

    const/4 v10, 0x0

    const/4 v11, -0x2

    invoke-static {v6, v9, v10, v11}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    if-ne v6, v4, :cond_6

    move v6, v4

    :goto_5
    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mStayEnabled:Z
    invoke-static {v8, v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$802(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    .line 221
    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$600(Lcom/samsung/android/smartface/SmartFaceService;)Landroid/content/ContentResolver;

    move-result-object v6

    const-string v8, "intelligent_rotation_mode"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v6, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    if-ne v6, v4, :cond_7

    move v3, v4

    .line 222
    .local v3, "smart_rotation":Z
    :goto_6
    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$600(Lcom/samsung/android/smartface/SmartFaceService;)Landroid/content/ContentResolver;

    move-result-object v6

    const-string v8, "accelerometer_rotation"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v6, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    if-ne v6, v4, :cond_8

    move v1, v4

    .line 224
    .local v1, "screen_rotation":Z
    :goto_7
    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    if-eqz v3, :cond_9

    if-eqz v1, :cond_9

    :goto_8
    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mRotationEnabled:Z
    invoke-static {v6, v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$902(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    .line 226
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # invokes: Lcom/samsung/android/smartface/SmartFaceService;->updateActiveServiceTypeLocked()V
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$1000(Lcom/samsung/android/smartface/SmartFaceService;)V

    .line 228
    const-string v4, "SmartFaceService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mScrollEnabled:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mScrollEnabled:Z
    invoke-static {v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$500(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mPauseEnabled:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mPauseEnabled:Z
    invoke-static {v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$700(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mStayEnabled:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mStayEnabled:Z
    invoke-static {v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$800(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mRotationEnabled:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mRotationEnabled:Z
    invoke-static {v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$900(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 233
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # invokes: Lcom/samsung/android/smartface/SmartFaceService;->updateClientStatusLocked()V
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$300(Lcom/samsung/android/smartface/SmartFaceService;)V

    .line 234
    monitor-exit v7

    .line 235
    return-void

    .end local v1    # "screen_rotation":Z
    .end local v3    # "smart_rotation":Z
    :cond_0
    move v6, v5

    .line 206
    goto/16 :goto_0

    :cond_1
    move v0, v5

    .line 210
    goto/16 :goto_1

    .restart local v0    # "mute_pause":Z
    :cond_2
    move v2, v5

    .line 211
    goto/16 :goto_2

    .restart local v2    # "smart_pause":Z
    :cond_3
    move v6, v5

    .line 212
    goto/16 :goto_3

    .line 216
    .end local v0    # "mute_pause":Z
    .end local v2    # "smart_pause":Z
    :cond_4
    iget-object v8, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    iget-object v6, p0, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$600(Lcom/samsung/android/smartface/SmartFaceService;)Landroid/content/ContentResolver;

    move-result-object v6

    const-string v9, "smart_pause"

    const/4 v10, 0x0

    const/4 v11, -0x2

    invoke-static {v6, v9, v10, v11}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v6

    if-ne v6, v4, :cond_5

    move v6, v4

    :goto_9
    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mPauseEnabled:Z
    invoke-static {v8, v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$702(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    goto/16 :goto_4

    .line 234
    :catchall_0
    move-exception v4

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_5
    move v6, v5

    .line 216
    goto :goto_9

    :cond_6
    move v6, v5

    .line 219
    goto/16 :goto_5

    :cond_7
    move v3, v5

    .line 221
    goto/16 :goto_6

    .restart local v3    # "smart_rotation":Z
    :cond_8
    move v1, v5

    .line 222
    goto/16 :goto_7

    .restart local v1    # "screen_rotation":Z
    :cond_9
    move v4, v5

    .line 224
    goto/16 :goto_8
.end method
