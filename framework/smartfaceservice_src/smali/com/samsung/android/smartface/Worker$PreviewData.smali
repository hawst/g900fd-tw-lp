.class public Lcom/samsung/android/smartface/Worker$PreviewData;
.super Ljava/lang/Object;
.source "Worker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartface/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "PreviewData"
.end annotation


# instance fields
.field public bVisionMode:Z

.field public mData:[B

.field public mHeight:I

.field public mUIOrientation:I

.field public mWidth:I

.field final synthetic this$0:Lcom/samsung/android/smartface/Worker;


# direct methods
.method protected constructor <init>(Lcom/samsung/android/smartface/Worker;)V
    .locals 1

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->this$0:Lcom/samsung/android/smartface/Worker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    return-void
.end method


# virtual methods
.method public assign([BI)Z
    .locals 5
    .param p1, "preview"    # [B
    .param p2, "ui_orientation"    # I

    .prologue
    const/4 v4, 0x0

    .line 56
    const/4 v1, 0x0

    .line 58
    .local v1, "ret":Z
    iget-object v2, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    array-length v2, v2

    array-length v3, p1

    if-eq v2, v3, :cond_0

    .line 60
    array-length v2, p1

    new-array v2, v2, [B

    iput-object v2, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    .line 62
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    array-length v3, p1

    invoke-static {p1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 64
    iput p2, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->mUIOrientation:I

    .line 67
    iput-boolean v4, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->bVisionMode:Z

    .line 69
    sget-object v0, Lcom/samsung/android/smartface/CameraController;->PreviewSize:Lcom/samsung/android/smartface/CameraController$Size;

    .line 70
    .local v0, "previewsize":Lcom/samsung/android/smartface/CameraController$Size;
    array-length v2, p1

    iget v3, v0, Lcom/samsung/android/smartface/CameraController$Size;->width:I

    iget v4, v0, Lcom/samsung/android/smartface/CameraController$Size;->height:I

    mul-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x3

    div-int/lit8 v3, v3, 0x2

    if-ne v2, v3, :cond_2

    .line 72
    iget v2, v0, Lcom/samsung/android/smartface/CameraController$Size;->width:I

    iput v2, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->mWidth:I

    .line 73
    iget v2, v0, Lcom/samsung/android/smartface/CameraController$Size;->height:I

    iput v2, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->mHeight:I

    .line 74
    const/4 v1, 0x1

    .line 83
    :cond_1
    :goto_0
    return v1

    .line 75
    :cond_2
    array-length v2, p1

    iget v3, v0, Lcom/samsung/android/smartface/CameraController$Size;->width:I

    iget v4, v0, Lcom/samsung/android/smartface/CameraController$Size;->height:I

    mul-int/2addr v3, v4

    if-ne v2, v3, :cond_1

    .line 77
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->bVisionMode:Z

    .line 78
    iget v2, v0, Lcom/samsung/android/smartface/CameraController$Size;->width:I

    iput v2, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->mWidth:I

    .line 79
    iget v2, v0, Lcom/samsung/android/smartface/CameraController$Size;->height:I

    iput v2, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->mHeight:I

    .line 80
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public copy(Lcom/samsung/android/smartface/Worker$PreviewData;)V
    .locals 4
    .param p1, "input"    # Lcom/samsung/android/smartface/Worker$PreviewData;

    .prologue
    const/4 v3, 0x0

    .line 89
    iget-object v0, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    array-length v0, v0

    iget-object v1, p1, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    array-length v1, v1

    if-eq v0, v1, :cond_0

    .line 91
    iget-object v0, p1, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    array-length v0, v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    .line 93
    :cond_0
    iget-object v0, p1, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    iget-object v1, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    iget-object v2, p1, Lcom/samsung/android/smartface/Worker$PreviewData;->mData:[B

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 95
    iget v0, p1, Lcom/samsung/android/smartface/Worker$PreviewData;->mUIOrientation:I

    iput v0, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->mUIOrientation:I

    .line 96
    iget-boolean v0, p1, Lcom/samsung/android/smartface/Worker$PreviewData;->bVisionMode:Z

    iput-boolean v0, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->bVisionMode:Z

    .line 97
    iget v0, p1, Lcom/samsung/android/smartface/Worker$PreviewData;->mWidth:I

    iput v0, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->mWidth:I

    .line 98
    iget v0, p1, Lcom/samsung/android/smartface/Worker$PreviewData;->mHeight:I

    iput v0, p0, Lcom/samsung/android/smartface/Worker$PreviewData;->mHeight:I

    .line 99
    return-void
.end method
