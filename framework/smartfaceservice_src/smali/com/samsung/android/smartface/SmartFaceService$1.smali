.class Lcom/samsung/android/smartface/SmartFaceService$1;
.super Landroid/content/BroadcastReceiver;
.source "SmartFaceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartface/SmartFaceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private oldHardKeyboardHidden:Z

.field final synthetic this$0:Lcom/samsung/android/smartface/SmartFaceService;


# direct methods
.method constructor <init>(Lcom/samsung/android/smartface/SmartFaceService;)V
    .locals 1

    .prologue
    .line 354
    iput-object p1, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 356
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->oldHardKeyboardHidden:Z

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v10, -0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 360
    const-string v4, "SmartFaceService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onReceive: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7, v6}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 362
    const-string v4, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 364
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$100(Lcom/samsung/android/smartface/SmartFaceService;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 366
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    const/4 v8, 0x1

    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mSystemReady:Z
    invoke-static {v4, v8}, Lcom/samsung/android/smartface/SmartFaceService;->access$1102(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    .line 367
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # invokes: Lcom/samsung/android/smartface/SmartFaceService;->updateActiveServiceTypeLocked()V
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$1000(Lcom/samsung/android/smartface/SmartFaceService;)V

    .line 368
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # invokes: Lcom/samsung/android/smartface/SmartFaceService;->updateClientStatusLocked()V
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$300(Lcom/samsung/android/smartface/SmartFaceService;)V

    .line 369
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371
    :cond_0
    const-string v4, "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 373
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$100(Lcom/samsung/android/smartface/SmartFaceService;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 375
    :try_start_1
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    const-string v8, "com.sec.android.extra.MULTIWINDOW_RUNNING"

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mMultiWindowEnabled:Z
    invoke-static {v4, v8}, Lcom/samsung/android/smartface/SmartFaceService;->access$1202(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    .line 376
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # invokes: Lcom/samsung/android/smartface/SmartFaceService;->updateActiveServiceTypeLocked()V
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$1000(Lcom/samsung/android/smartface/SmartFaceService;)V

    .line 377
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # invokes: Lcom/samsung/android/smartface/SmartFaceService;->updateClientStatusLocked()V
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$300(Lcom/samsung/android/smartface/SmartFaceService;)V

    .line 378
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 380
    :cond_1
    const-string v4, "com.samsung.cover.OPEN"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 382
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$100(Lcom/samsung/android/smartface/SmartFaceService;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 384
    :try_start_2
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    const-string v8, "coverOpen"

    const/4 v9, 0x0

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mClearCoverOpened:Z
    invoke-static {v4, v8}, Lcom/samsung/android/smartface/SmartFaceService;->access$1302(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    .line 385
    const-string v4, "SmartFaceService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mClearCoverOpened: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mClearCoverOpened:Z
    invoke-static {v9}, Lcom/samsung/android/smartface/SmartFaceService;->access$1300(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x1

    invoke-static {v4, v8, v9}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 386
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # invokes: Lcom/samsung/android/smartface/SmartFaceService;->updateActiveServiceTypeLocked()V
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$1000(Lcom/samsung/android/smartface/SmartFaceService;)V

    .line 387
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # invokes: Lcom/samsung/android/smartface/SmartFaceService;->updateClientStatusLocked()V
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$300(Lcom/samsung/android/smartface/SmartFaceService;)V

    .line 388
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 391
    :cond_2
    const-string v4, "android.intent.action.USER_SWITCHED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 393
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mSmartScreenObserver:Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$1400(Lcom/samsung/android/smartface/SmartFaceService;)Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/samsung/android/smartface/SmartFaceService$SmartScreenObserver;->onChange(Z)V

    .line 396
    :cond_3
    const-string v4, "android.intent.action.DOCK_EVENT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 398
    const-string v4, "android.intent.extra.DOCK_STATE"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 399
    .local v1, "dockState":I
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mScrollEnabled:Z
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$500(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 400
    iget-object v7, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$600(Lcom/samsung/android/smartface/SmartFaceService;)Landroid/content/ContentResolver;

    move-result-object v4

    const-string v8, "face_smart_scroll"

    invoke-static {v4, v8, v6, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v4

    if-ne v4, v5, :cond_8

    move v4, v5

    :goto_0
    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mFaceScrollState:Z
    invoke-static {v7, v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$1502(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    .line 402
    if-eqz v1, :cond_a

    .line 404
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mFaceScrollState:Z
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$1500(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 405
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mChangedByDock:Z
    invoke-static {v4, v5}, Lcom/samsung/android/smartface/SmartFaceService;->access$1602(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    .line 406
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$600(Lcom/samsung/android/smartface/SmartFaceService;)Landroid/content/ContentResolver;

    move-result-object v4

    const-string v6, "face_smart_scroll"

    invoke-static {v4, v6, v5, v10}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 407
    const v4, 0x10407e7

    invoke-static {p1, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 421
    :cond_4
    :goto_1
    const-string v4, "SmartFaceService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mChangedByDock: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mChangedByDock:Z
    invoke-static {v7}, Lcom/samsung/android/smartface/SmartFaceService;->access$1600(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", mFaceScrollState: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mFaceScrollState:Z
    invoke-static {v7}, Lcom/samsung/android/smartface/SmartFaceService;->access$1500(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v5}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 425
    .end local v1    # "dockState":I
    :cond_5
    const-string v4, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 427
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$100(Lcom/samsung/android/smartface/SmartFaceService;)Ljava/lang/Object;

    move-result-object v6

    monitor-enter v6

    .line 429
    const/4 v2, 0x0

    .line 430
    .local v2, "hardKeyboardHidden":Z
    :try_start_3
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mFolderCoverOpened:Z
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$1700(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v3

    .line 432
    .local v3, "oldFolderCoverOpened":Z
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 434
    .local v0, "config":Landroid/content/res/Configuration;
    iget v4, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    const/4 v7, 0x2

    if-ne v4, v7, :cond_c

    .line 435
    const/4 v2, 0x1

    .line 445
    :goto_2
    iget-boolean v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->oldHardKeyboardHidden:Z

    if-nez v4, :cond_d

    if-ne v2, v5, :cond_d

    .line 446
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    const/4 v5, 0x0

    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mFolderCoverOpened:Z
    invoke-static {v4, v5}, Lcom/samsung/android/smartface/SmartFaceService;->access$1702(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    .line 451
    :goto_3
    const-string v4, "SmartFaceService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mFolderCoverOpened: ("

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v7, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->oldHardKeyboardHidden:Z

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ") -> "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v7, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mFolderCoverOpened:Z
    invoke-static {v7}, Lcom/samsung/android/smartface/SmartFaceService;->access$1700(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x1

    invoke-static {v4, v5, v7}, Lcom/samsung/android/smartface/SmartFaceService$Logger;->log(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 453
    iput-boolean v2, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->oldHardKeyboardHidden:Z

    .line 455
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mFolderCoverOpened:Z
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$1700(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v4

    if-eq v3, v4, :cond_6

    .line 456
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # invokes: Lcom/samsung/android/smartface/SmartFaceService;->updateActiveServiceTypeLocked()V
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$1000(Lcom/samsung/android/smartface/SmartFaceService;)V

    .line 457
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # invokes: Lcom/samsung/android/smartface/SmartFaceService;->updateClientStatusLocked()V
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$300(Lcom/samsung/android/smartface/SmartFaceService;)V

    .line 459
    :cond_6
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 462
    .end local v0    # "config":Landroid/content/res/Configuration;
    .end local v2    # "hardKeyboardHidden":Z
    .end local v3    # "oldFolderCoverOpened":Z
    :cond_7
    return-void

    .line 369
    :catchall_0
    move-exception v4

    :try_start_4
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4

    .line 378
    :catchall_1
    move-exception v4

    :try_start_5
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v4

    .line 388
    :catchall_2
    move-exception v4

    :try_start_6
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v4

    .restart local v1    # "dockState":I
    :cond_8
    move v4, v6

    .line 400
    goto/16 :goto_0

    .line 409
    :cond_9
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mChangedByDock:Z
    invoke-static {v4, v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$1602(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    goto/16 :goto_1

    .line 411
    :cond_a
    if-nez v1, :cond_4

    .line 413
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mFaceScrollState:Z
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$1500(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v4

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mChangedByDock:Z
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$1600(Lcom/samsung/android/smartface/SmartFaceService;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 414
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # getter for: Lcom/samsung/android/smartface/SmartFaceService;->mContentResolver:Landroid/content/ContentResolver;
    invoke-static {v4}, Lcom/samsung/android/smartface/SmartFaceService;->access$600(Lcom/samsung/android/smartface/SmartFaceService;)Landroid/content/ContentResolver;

    move-result-object v4

    const-string v7, "face_smart_scroll"

    invoke-static {v4, v7, v6, v10}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 415
    const v4, 0x10407e8

    invoke-static {p1, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 417
    :cond_b
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mChangedByDock:Z
    invoke-static {v4, v6}, Lcom/samsung/android/smartface/SmartFaceService;->access$1602(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    goto/16 :goto_1

    .line 437
    .end local v1    # "dockState":I
    .restart local v0    # "config":Landroid/content/res/Configuration;
    .restart local v2    # "hardKeyboardHidden":Z
    .restart local v3    # "oldFolderCoverOpened":Z
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 448
    :cond_d
    :try_start_7
    iget-object v4, p0, Lcom/samsung/android/smartface/SmartFaceService$1;->this$0:Lcom/samsung/android/smartface/SmartFaceService;

    const/4 v5, 0x1

    # setter for: Lcom/samsung/android/smartface/SmartFaceService;->mFolderCoverOpened:Z
    invoke-static {v4, v5}, Lcom/samsung/android/smartface/SmartFaceService;->access$1702(Lcom/samsung/android/smartface/SmartFaceService;Z)Z

    goto/16 :goto_3

    .line 459
    .end local v0    # "config":Landroid/content/res/Configuration;
    .end local v3    # "oldFolderCoverOpened":Z
    :catchall_3
    move-exception v4

    monitor-exit v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v4
.end method
