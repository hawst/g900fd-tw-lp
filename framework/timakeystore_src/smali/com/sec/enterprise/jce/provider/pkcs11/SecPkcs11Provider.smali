.class public final Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;
.super Ljava/security/AuthProvider;
.source "SecPkcs11Provider.java"

# interfaces
.implements Lcom/sec/smartcard/pkcs11/PKCS11Constants;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;
    }
.end annotation


# static fields
.field private static final CERTIFICATEFACTORY:Ljava/lang/String; = "CertificateFactory"

.field private static final CERTSTORE:Ljava/lang/String; = "CertStore"

.field private static final CIPHER:Ljava/lang/String; = "Cipher"

.field protected static final DESCRIPTION:Ljava/lang/String; = "SEC PKCS11 Crypto Provider"

.field private static final KEYPAIRGENERATOR:Ljava/lang/String; = "KeyPairGenerator"

.field private static final KEYSTORE:Ljava/lang/String; = "KeyStore"

.field private static final MESSAGEDIGEST:Ljava/lang/String; = "MessageDigest"

.field public static final PROVIDER_NAME:Ljava/lang/String; = "SECPkcs11"

.field private static final SECURERANDOM:Ljava/lang/String; = "SecureRandom"

.field private static final SIGNATURE:Ljava/lang/String; = "Signature"

.field private static final TAG:Ljava/lang/String; = "SecPkcs11Provider"

.field private static final VERSION:D = 1.0

.field public static pHandler:Ljavax/security/auth/callback/CallbackHandler; = null

.field private static final serialVersionUID:J = -0x1ff5ecf0661a5904L


# instance fields
.field final config:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

.field private mProvider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

.field final opensslHelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;)V
    .locals 8
    .param p1, "cfg"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    .prologue
    .line 156
    const-string v2, "SECPkcs11"

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-string v3, "SEC PKCS11 Crypto Provider"

    invoke-direct {p0, v2, v4, v5, v3}, Ljava/security/AuthProvider;-><init>(Ljava/lang/String;DLjava/lang/String;)V

    .line 157
    iput-object p0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->mProvider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    .line 160
    if-nez p1, :cond_2

    .line 161
    new-instance v2, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    invoke-direct {v2}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;-><init>()V

    iput-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->config:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    .line 166
    :goto_0
    iget-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->config:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    iget-boolean v2, v2, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->debug:Z

    if-eqz v2, :cond_0

    .line 167
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11DefaultLogger;

    const-string v2, "Default"

    invoke-direct {v0, v2}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11DefaultLogger;-><init>(Ljava/lang/String;)V

    .line 168
    .local v0, "defaultLogger":Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11DefaultLogger;
    invoke-static {v0}, Lcom/sec/generic/util/log/Log;->addLogger(Lcom/sec/generic/util/log/Logger;)Z

    .line 171
    .end local v0    # "defaultLogger":Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11DefaultLogger;
    :cond_0
    iget-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->config:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    iget-wide v2, v2, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->slotId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 172
    const-string v2, "SecPkcs11Provider"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "slotId user set: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->config:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    iget-wide v4, v4, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->slotId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->mProvider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    invoke-direct {p0, v2}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->registerEngines(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;)V

    .line 178
    new-instance v2, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    iget-object v3, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->config:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    iget-object v3, v3, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->libraryName:Ljava/lang/String;

    iget-object v4, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->config:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    iget-object v4, v4, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->functionListName:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->config:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    iget-wide v6, v5, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->slotId:J

    invoke-direct {v2, v3, v4, v6, v7}, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    iput-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->opensslHelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    .line 179
    iget-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->opensslHelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    invoke-virtual {v2}, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->registerSSLEngine()Z

    move-result v1

    .line 181
    .local v1, "ret":Z
    if-nez v1, :cond_3

    .line 182
    new-instance v2, Ljava/security/ProviderException;

    const-string v3, "Register SSL Engine Failed"

    invoke-direct {v2, v3}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 163
    .end local v1    # "ret":Z
    :cond_2
    iput-object p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->config:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    goto :goto_0

    .line 184
    .restart local v1    # "ret":Z
    :cond_3
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "libName"    # Ljava/lang/String;

    .prologue
    .line 142
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    invoke-direct {v0, p1}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;)V

    .line 143
    return-void
.end method

.method private getCallbackHandler()Ljavax/security/auth/callback/CallbackHandler;
    .locals 1

    .prologue
    .line 319
    sget-object v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->pHandler:Ljavax/security/auth/callback/CallbackHandler;

    return-object v0
.end method

.method private registerEngines(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;)V
    .locals 8
    .param p1, "provider"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    .prologue
    .line 281
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "MessageDigest"

    const-string v3, "MD2"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x200

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 282
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "MessageDigest"

    const-string v3, "MD5"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x210

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 283
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "MessageDigest"

    const-string v3, "SHA1"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v6, "SHA"

    aput-object v6, v5, v1

    const/4 v1, 0x1

    const-string v6, "SHA-1"

    aput-object v6, v5, v1

    const-wide/16 v6, 0x220

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 286
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "MessageDigest"

    const-string v3, "SHA256"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v6, "SHA-256"

    aput-object v6, v5, v1

    const-wide/16 v6, 0x250

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 288
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "MessageDigest"

    const-string v3, "SHA384"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v6, "SHA-384"

    aput-object v6, v5, v1

    const-wide/16 v6, 0x260

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 290
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "MessageDigest"

    const-string v3, "SHA512"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v6, "SHA-512"

    aput-object v6, v5, v1

    const-wide/16 v6, 0x270

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 293
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "SecureRandom"

    const-string v3, "PKCS11"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11SecureRandom;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 295
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "Cipher"

    const-string v3, "RSA/None/PKCS1Padding"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x1

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 296
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "Cipher"

    const-string v3, "RSA/ECB/PKCS1Padding"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x1

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 298
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "Signature"

    const-string v3, "NONEwithRSA"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x1

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 299
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "Signature"

    const-string v3, "SHA1withRSA"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x1

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 300
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "Signature"

    const-string v3, "SHA224withRSA"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x1

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 301
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "Signature"

    const-string v3, "SHA256withRSA"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x1

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 302
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "Signature"

    const-string v3, "SHA384withRSA"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x1

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 303
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "Signature"

    const-string v3, "SHA512withRSA"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x1

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 304
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "Signature"

    const-string v3, "MD5withRSA"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x1

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 306
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "KeyStore"

    const-string v3, "PKCS11"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 308
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "KeyPairGenerator"

    const-string v3, "RSA"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyPairGenerator;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 310
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "CertStore"

    const-string v3, "Collection"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11CertStore;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 312
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;

    const-string v2, "CertificateFactory"

    const-string v3, "X.509"

    const-class v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11CertificateFactory;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V

    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->putService(Ljava/security/Provider$Service;)V

    .line 313
    return-void
.end method


# virtual methods
.method protected getConfig()Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->config:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    return-object v0
.end method

.method protected getPin()[C
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/security/auth/login/LoginException;
        }
    .end annotation

    .prologue
    .line 222
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getToken()Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Token;
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    return-object v0
.end method

.method protected login(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;)V
    .locals 0
    .param p1, "existingSession"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/security/auth/login/LoginException;
        }
    .end annotation

    .prologue
    .line 232
    return-void
.end method

.method protected login(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;[C)V
    .locals 0
    .param p1, "existingSession"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;
    .param p2, "pin"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/security/auth/login/LoginException;
        }
    .end annotation

    .prologue
    .line 242
    return-void
.end method

.method public login(Ljavax/security/auth/Subject;Ljavax/security/auth/callback/CallbackHandler;)V
    .locals 0
    .param p1, "arg0"    # Ljavax/security/auth/Subject;
    .param p2, "arg1"    # Ljavax/security/auth/callback/CallbackHandler;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/security/auth/login/LoginException;
        }
    .end annotation

    .prologue
    .line 198
    return-void
.end method

.method public logout()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/security/auth/login/LoginException;
        }
    .end annotation

    .prologue
    .line 213
    return-void
.end method

.method protected logout(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;)V
    .locals 0
    .param p1, "currentSession"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/security/auth/login/LoginException;
        }
    .end annotation

    .prologue
    .line 251
    return-void
.end method

.method public setCallbackHandler(Ljavax/security/auth/callback/CallbackHandler;)V
    .locals 0
    .param p1, "arg0"    # Ljavax/security/auth/callback/CallbackHandler;

    .prologue
    .line 272
    sput-object p1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->pHandler:Ljavax/security/auth/callback/CallbackHandler;

    .line 273
    return-void
.end method
