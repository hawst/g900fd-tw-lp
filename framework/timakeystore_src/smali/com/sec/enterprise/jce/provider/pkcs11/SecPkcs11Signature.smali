.class final Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;
.super Ljava/security/SignatureSpi;
.source "SecPkcs11Signature.java"


# static fields
.field private static final ANDROID_OPENSSL_PROVIDER:Ljava/lang/String; = "AndroidOpenSSL"

.field private static final KEY_ALGORITHM_RSA:I = 0x1

.field private static final PKCS11_SIGNATURE_SIGN:I = 0x1

.field private static final PKCS11_SIGNATURE_VERIFY:I = 0x2

.field private static final RSA_PRIVATE_KEY_SIGN_DATA_LENGTH:I = 0x80

.field private static final TAG:Ljava/lang/String; = "SecPkcs11Signature"


# instance fields
.field private final algorithm:Ljava/lang/String;

.field private bytesStored:I

.field private dataBuffer:[B

.field private externalVerify:Ljava/security/Signature;

.field private initComplete:Z

.field private final keyAlgorithm:I

.field private opensslSig:Ljava/security/Signature;

.field private operationMode:I

.field private final pkcs11Mechanism:J

.field private final pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

.field private publicKey:Ljava/security/PublicKey;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;J)V
    .locals 7
    .param p1, "provider"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;
    .param p2, "algorithm"    # Ljava/lang/String;
    .param p3, "pkcs11Mechanism"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/ProviderException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 113
    invoke-direct {p0}, Ljava/security/SignatureSpi;-><init>()V

    .line 115
    iput-object p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    .line 116
    iput-object p2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->algorithm:Ljava/lang/String;

    .line 117
    iput-wide p3, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->pkcs11Mechanism:J

    .line 118
    iput-boolean v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->initComplete:Z

    .line 119
    iput v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->bytesStored:I

    .line 128
    long-to-int v1, p3

    packed-switch v1, :pswitch_data_0

    .line 138
    new-instance v1, Ljava/security/ProviderException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Mechanism NOT supported - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 130
    :pswitch_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->keyAlgorithm:I

    .line 131
    const-string v1, "RSA"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    const/16 v1, 0x80

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->dataBuffer:[B

    .line 142
    :cond_0
    :try_start_0
    const-string v1, "AndroidOpenSSL"

    invoke-static {p2, v1}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->opensslSig:Ljava/security/Signature;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_1

    .line 148
    const-string v1, "SecPkcs11Signature"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SecPkcs11Signature - Algorithm: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->algorithm:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v1, "SecPkcs11Signature"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SecPkcs11Signature - key Algorithm: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->keyAlgorithm:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v1, "SecPkcs11Signature"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SecPkcs11Signature - Mechanism: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->pkcs11Mechanism:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    return-void

    .line 143
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v1, Ljava/security/ProviderException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No such algorithm"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 145
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 146
    .local v0, "e":Ljava/security/NoSuchProviderException;
    new-instance v1, Ljava/security/ProviderException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No such provider"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 128
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected engineGetParameter(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 158
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "No support for getParameter()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected engineInitSign(Ljava/security/PrivateKey;)V
    .locals 3
    .param p1, "privateKey"    # Ljava/security/PrivateKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .prologue
    .line 166
    const-string v0, "SecPkcs11Signature"

    const-string v1, "engineInitSign"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v0, "SecPkcs11Signature"

    const-string v1, "engineInitSign .. test"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    if-nez p1, :cond_0

    .line 170
    new-instance v0, Ljava/security/InvalidKeyException;

    const-string v1, "Private Key is NULL"

    invoke-direct {v0, v1}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_0
    invoke-interface {p1}, Ljava/security/PrivateKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    const-string v1, "RSA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 176
    new-instance v0, Ljava/security/ProviderException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported Key algorithm"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/security/PrivateKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->opensslSig:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;)V

    .line 183
    return-void
.end method

.method protected engineInitVerify(Ljava/security/PublicKey;)V
    .locals 2
    .param p1, "publicKey"    # Ljava/security/PublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .prologue
    .line 188
    const-string v0, "SecPkcs11Signature"

    const-string v1, "engineInitVerify"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    if-nez p1, :cond_0

    .line 191
    new-instance v0, Ljava/security/InvalidKeyException;

    const-string v1, "Public Key is NULL"

    invoke-direct {v0, v1}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->opensslSig:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    .line 195
    return-void
.end method

.method protected engineSetParameter(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 200
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "No support for setParameter()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected engineSign()[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 206
    const-string v1, "SecPkcs11Signature"

    const-string v2, "engineSign"

    invoke-static {v1, v2}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    iget v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->keyAlgorithm:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 210
    new-instance v1, Ljava/security/SignatureException;

    const-string v2, "Unsupported algorithm"

    invoke-direct {v1, v2}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 216
    :cond_0
    iget-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->opensslSig:Ljava/security/Signature;

    invoke-virtual {v1}, Ljava/security/Signature;->sign()[B

    move-result-object v0

    .line 218
    .local v0, "digitalSignature":[B
    return-object v0
.end method

.method protected engineUpdate(B)V
    .locals 2
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 224
    const-string v0, "SecPkcs11Signature"

    const-string v1, "engineUpdate: byte value"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->opensslSig:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->update(B)V

    .line 228
    return-void
.end method

.method protected engineUpdate(Ljava/nio/ByteBuffer;)V
    .locals 4
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 241
    const-string v1, "SecPkcs11Signature"

    const-string v2, "engineUpdate: byte buffer "

    invoke-static {v1, v2}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->opensslSig:Ljava/security/Signature;

    invoke-virtual {v1, p1}, Ljava/security/Signature;->update(Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    return-void

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/security/SignatureException;
    new-instance v1, Ljava/security/ProviderException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Signature"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected engineUpdate([BII)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 233
    const-string v0, "SecPkcs11Signature"

    const-string v1, "engineUpdate: data, data off, len "

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->opensslSig:Ljava/security/Signature;

    invoke-virtual {v0, p1, p2, p3}, Ljava/security/Signature;->update([BII)V

    .line 237
    return-void
.end method

.method protected engineVerify([B)Z
    .locals 3
    .param p1, "inSignature"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 253
    const-string v1, "SecPkcs11Signature"

    const-string v2, "engineVerify"

    invoke-static {v1, v2}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    iget-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;->opensslSig:Ljava/security/Signature;

    invoke-virtual {v1, p1}, Ljava/security/Signature;->verify([B)Z

    move-result v0

    .line 258
    .local v0, "isValid":Z
    return v0
.end method
