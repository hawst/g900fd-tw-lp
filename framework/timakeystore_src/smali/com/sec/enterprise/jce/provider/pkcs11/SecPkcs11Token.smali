.class public final Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Token;
.super Ljava/lang/Object;
.source "SecPkcs11Token.java"

# interfaces
.implements Lcom/sec/smartcard/pkcs11/PKCS11Constants;


# static fields
.field private static final TAG:Ljava/lang/String; = "SecPkcs11Token"


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;)V
    .locals 0
    .param p1, "provider"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    return-void
.end method


# virtual methods
.method protected destroy()V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method protected ensureLoggedIn(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;)V
    .locals 0
    .param p1, "session"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;,
            Ljavax/security/auth/login/LoginException;
        }
    .end annotation

    .prologue
    .line 110
    return-void
.end method

.method protected ensureLoggedIn(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;[C)V
    .locals 0
    .param p1, "session"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;
    .param p2, "pin"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;,
            Ljavax/security/auth/login/LoginException;
        }
    .end annotation

    .prologue
    .line 116
    return-void
.end method

.method protected ensureValid()V
    .locals 0

    .prologue
    .line 87
    return-void
.end method

.method protected getOpSession()Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation

    .prologue
    .line 72
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getTokenInfo()Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return-object v0
.end method

.method protected isLoggedIn(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;)Z
    .locals 1
    .param p1, "session"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method protected isPresent()Z
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    .line 94
    .local v0, "ok":Z
    return v0
.end method

.method protected releaseSession(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;)Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;
    .locals 1
    .param p1, "session"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;

    .prologue
    .line 80
    const/4 v0, 0x0

    return-object v0
.end method
