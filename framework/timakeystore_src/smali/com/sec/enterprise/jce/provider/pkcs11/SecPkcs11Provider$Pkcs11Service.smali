.class final Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;
.super Ljava/security/Provider$Service;
.source "SecPkcs11Provider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Pkcs11Service"
.end annotation


# instance fields
.field private final mechanism:J

.field private final provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)V
    .locals 8
    .param p1, "provider"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "algorithm"    # Ljava/lang/String;
    .param p4, "className"    # Ljava/lang/String;
    .param p5, "aliases"    # [Ljava/lang/String;
    .param p6, "mechanism"    # J

    .prologue
    .line 361
    invoke-static {p5}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;->toList([Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Ljava/security/Provider$Service;-><init>(Ljava/security/Provider;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/Map;)V

    .line 362
    iput-object p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;->provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    .line 363
    iput-wide p6, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;->mechanism:J

    .line 364
    return-void
.end method

.method private static toList([Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p0, "aliases"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public newInstance(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1, "param"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 372
    invoke-virtual {p0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    .line 373
    .local v0, "algorithm":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;->getType()Ljava/lang/String;

    move-result-object v2

    .line 374
    .local v2, "type":Ljava/lang/String;
    const-string v3, "MessageDigest"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 375
    new-instance v3, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;

    iget-object v4, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;->provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    iget-wide v6, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;->mechanism:J

    invoke-direct {v3, v4, v0, v6, v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;J)V

    .line 395
    .end local p1    # "param":Ljava/lang/Object;
    :goto_0
    return-object v3

    .line 376
    .restart local p1    # "param":Ljava/lang/Object;
    :cond_0
    const-string v3, "Cipher"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 377
    new-instance v3, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;

    iget-object v4, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;->provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    iget-wide v6, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;->mechanism:J

    invoke-direct {v3, v4, v0, v6, v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;J)V

    goto :goto_0

    .line 378
    :cond_1
    const-string v3, "Signature"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 379
    new-instance v3, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;

    iget-object v4, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;->provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    iget-wide v6, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;->mechanism:J

    invoke-direct {v3, v4, v0, v6, v7}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Signature;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;J)V

    goto :goto_0

    .line 380
    :cond_2
    const-string v3, "KeyPairGenerator"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 381
    new-instance v3, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyPairGenerator;

    invoke-direct {v3, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyPairGenerator;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 383
    :cond_3
    const-string v3, "SecureRandom"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 385
    new-instance v3, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11SecureRandom;

    iget-object v4, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;->provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    invoke-direct {v3, v4, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11SecureRandom;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;)V

    goto :goto_0

    .line 386
    :cond_4
    const-string v3, "KeyStore"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 387
    new-instance v3, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;

    iget-object v4, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;->provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    invoke-direct {v3, v4}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;)V

    goto :goto_0

    .line 388
    :cond_5
    const-string v3, "CertStore"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 390
    :try_start_0
    new-instance v3, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11CertStore;

    check-cast p1, Ljava/security/cert/CertStoreParameters;

    .end local p1    # "param":Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;->provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    invoke-direct {v3, p1, v4}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11CertStore;-><init>(Ljava/security/cert/CertStoreParameters;Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;)V
    :try_end_0
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 391
    :catch_0
    move-exception v1

    .line 392
    .local v1, "e":Ljava/security/InvalidAlgorithmParameterException;
    new-instance v3, Ljava/security/ProviderException;

    invoke-virtual {v1}, Ljava/security/InvalidAlgorithmParameterException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 394
    .end local v1    # "e":Ljava/security/InvalidAlgorithmParameterException;
    .restart local p1    # "param":Ljava/lang/Object;
    :cond_6
    const-string v3, "CertificateFactory"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 395
    new-instance v3, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11CertificateFactory;

    iget-object v4, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider$Pkcs11Service;->provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    invoke-direct {v3, v4}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11CertificateFactory;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;)V

    goto :goto_0

    .line 397
    :cond_7
    new-instance v3, Ljava/security/NoSuchAlgorithmException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/security/NoSuchAlgorithmException;-><init>(Ljava/lang/String;)V

    throw v3
.end method
