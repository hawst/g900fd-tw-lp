.class public Lcom/sec/tima/TimaKeyStoreProvider;
.super Ljava/security/Provider;
.source "TimaKeyStoreProvider.java"


# static fields
.field public static final PROVIDER_NAME:Ljava/lang/String; = "TimaKeyStore"

.field public static final TAG:Ljava/lang/String; = "TimaKeyStoreProvider"


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 34
    const-string v0, "TimaKeyStore"

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-string v1, "TIMA KeyStore security provider"

    invoke-direct {p0, v0, v2, v3, v1}, Ljava/security/Provider;-><init>(Ljava/lang/String;DLjava/lang/String;)V

    .line 37
    const-string v0, "KeyStore.TimaKeyStore"

    const-class v1, Lcom/sec/tima/TimaKeyStore;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/tima/TimaKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    invoke-direct {p0}, Lcom/sec/tima/TimaKeyStoreProvider;->addTimaSignatureService()V

    .line 39
    return-void
.end method

.method private addTimaSignatureService()V
    .locals 9

    .prologue
    .line 42
    const-class v7, Lcom/sec/tima/TimaSignatureSHA1RSA;

    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "SIGN_NAME_SHA1":Ljava/lang/String;
    const-string v0, "SHA1withRSA"

    .line 45
    .local v0, "SIGN_ALIAS_SHA1":Ljava/lang/String;
    const/4 v3, 0x0

    .line 51
    .local v3, "isTimaKeystoreEnabled":Z
    :try_start_0
    const-string v7, "com.sec.enterprise.knox.keystore.TimaKeystore"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 53
    .local v4, "keyStoreClass":Ljava/lang/Class;
    const-string v7, "getInstance"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v4, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 54
    .local v6, "m":Ljava/lang/reflect/Method;
    const/4 v7, 0x0

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v6, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 55
    .local v5, "keyStorePolicyObj":Ljava/lang/Object;
    const-string v7, "isTimaKeystoreEnabledInDB"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v4, v7, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 56
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v6, v5, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 58
    if-eqz v3, :cond_0

    .line 59
    const-string v7, "Signature.SHA1withRSA"

    invoke-virtual {p0, v7, v1}, Lcom/sec/tima/TimaKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const-string v7, "Alg.Alias.Signature.RSA"

    const-string v8, "SHA1withRSA"

    invoke-virtual {p0, v7, v8}, Lcom/sec/tima/TimaKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    const-string v7, "Alg.Alias.Signature.SHA/RSA"

    const-string v8, "SHA1withRSA"

    invoke-virtual {p0, v7, v8}, Lcom/sec/tima/TimaKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    const-string v7, "Alg.Alias.Signature.SHA1/RSA"

    const-string v8, "SHA1withRSA"

    invoke-virtual {p0, v7, v8}, Lcom/sec/tima/TimaKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    const-string v7, "Alg.Alias.Signature.SHA-1/RSA"

    const-string v8, "SHA1withRSA"

    invoke-virtual {p0, v7, v8}, Lcom/sec/tima/TimaKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    const-string v7, "Alg.Alias.Signature.SHAwithRSA"

    const-string v8, "SHA1withRSA"

    invoke-virtual {p0, v7, v8}, Lcom/sec/tima/TimaKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    const-string v7, "Alg.Alias.Signature.SHA1withRSA"

    const-string v8, "SHA1withRSA"

    invoke-virtual {p0, v7, v8}, Lcom/sec/tima/TimaKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    const-string v7, "Alg.Alias.Signature.RSAwithSHA1"

    const-string v8, "SHA1withRSA"

    invoke-virtual {p0, v7, v8}, Lcom/sec/tima/TimaKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    const-string v7, "Alg.Alias.Signature.1.3.14.3.2.29"

    const-string v8, "SHA1withRSA"

    invoke-virtual {p0, v7, v8}, Lcom/sec/tima/TimaKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    const-string v7, "Alg.Alias.Signature.OID.1.3.14.3.2.29"

    const-string v8, "SHA1withRSA"

    invoke-virtual {p0, v7, v8}, Lcom/sec/tima/TimaKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    const-string v7, "Alg.Alias.Signature.1.2.840.113549.1.1.5"

    const-string v8, "SHA1withRSA"

    invoke-virtual {p0, v7, v8}, Lcom/sec/tima/TimaKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    const-string v7, "Alg.Alias.Signature.OID.1.2.840.113549.1.1.5"

    const-string v8, "SHA1withRSA"

    invoke-virtual {p0, v7, v8}, Lcom/sec/tima/TimaKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    const-string v7, "Alg.Alias.Signature.1.3.14.3.2.26with1.2.840.113549.1.1.1"

    const-string v8, "SHA1withRSA"

    invoke-virtual {p0, v7, v8}, Lcom/sec/tima/TimaKeyStoreProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    .end local v4    # "keyStoreClass":Ljava/lang/Class;
    .end local v5    # "keyStorePolicyObj":Ljava/lang/Object;
    .end local v6    # "m":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 74
    .restart local v4    # "keyStoreClass":Ljava/lang/Class;
    .restart local v5    # "keyStorePolicyObj":Ljava/lang/Object;
    .restart local v6    # "m":Ljava/lang/reflect/Method;
    :cond_0
    const-string v7, "TimaKeyStoreProvider"

    const-string v8, "TimaSignature is unavailable"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 77
    .end local v4    # "keyStoreClass":Ljava/lang/Class;
    .end local v5    # "keyStorePolicyObj":Ljava/lang/Object;
    .end local v6    # "m":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v2

    .line 78
    .local v2, "e":Ljava/lang/Exception;
    const-string v7, "TimaKeyStoreProvider"

    const-string v8, "Cannot add TimaSignature Service"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
