.class public final Lcom/sec/tima/TimaSignatureSHA1RSA;
.super Ljava/security/SignatureSpi;
.source "TimaSignatureSHA1RSA.java"


# static fields
.field public static final NAME:Ljava/lang/String; = "TimaSignatureSHA1RSA"


# instance fields
.field private mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

.field mCxtInfo:Landroid/app/enterprise/ContextInfo;

.field mSignature:Ljava/security/Signature;


# direct methods
.method public constructor <init>()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/security/SignatureSpi;-><init>()V

    .line 153
    const-string v5, "TimaSignatureSHA1RSA"

    const-string v6, "creating new TIMASignature"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    const/4 v1, 0x0

    .line 161
    .local v1, "isTimaKeystoreEnabled":Ljava/lang/Object;
    :try_start_0
    const-string v5, "com.sec.enterprise.knox.keystore.TimaKeystore"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 163
    .local v2, "keyStoreClass":Ljava/lang/Class;
    const-string v5, "getInstance"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v2, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 165
    .local v4, "m":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 167
    .local v3, "keyStorePolicyObj":Ljava/lang/Object;
    const-string v5, "isTimaKeystoreEnabled"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v2, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 169
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 176
    check-cast v1, Ljava/lang/Boolean;

    .end local v1    # "isTimaKeystoreEnabled":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_0

    .line 177
    new-instance v5, Ljava/security/SignatureException;

    const-string v6, "TIMA Keystore is not enabled"

    invoke-direct {v5, v6}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 171
    .end local v2    # "keyStoreClass":Ljava/lang/Class;
    .end local v3    # "keyStorePolicyObj":Ljava/lang/Object;
    .end local v4    # "m":Ljava/lang/reflect/Method;
    .restart local v1    # "isTimaKeystoreEnabled":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 172
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 173
    new-instance v5, Ljava/security/SignatureException;

    invoke-direct {v5, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 180
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "isTimaKeystoreEnabled":Ljava/lang/Object;
    .restart local v2    # "keyStoreClass":Ljava/lang/Class;
    .restart local v3    # "keyStorePolicyObj":Ljava/lang/Object;
    .restart local v4    # "m":Ljava/lang/reflect/Method;
    :cond_0
    invoke-direct {p0}, Lcom/sec/tima/TimaSignatureSHA1RSA;->getPkcs11SignatureInstance()Ljava/security/Signature;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mSignature:Ljava/security/Signature;

    .line 181
    iget-object v5, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mSignature:Ljava/security/Signature;

    if-nez v5, :cond_1

    .line 182
    new-instance v5, Ljava/security/SignatureException;

    const-string v6, "Error creating Signature instance"

    invoke-direct {v5, v6}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 184
    :cond_1
    return-void
.end method

.method private getPkcs11SignatureInstance()Ljava/security/Signature;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 90
    const-wide/16 v4, -0x1

    .line 92
    .local v4, "slotid":J
    const-string v3, "knox_ccm_policy"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    .line 95
    iget-object v3, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    if-nez v3, :cond_0

    .line 96
    const-string v3, "TimaSignatureSHA1RSA"

    const-string v6, "Unable to check access to ClientCertificateManager"

    invoke-static {v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    new-instance v3, Ljava/security/SignatureException;

    const-string v6, "Error creating Signature instance"

    invoke-direct {v3, v6}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 100
    :cond_0
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    invoke-direct {v3, v6}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    iput-object v3, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mCxtInfo:Landroid/app/enterprise/ContextInfo;

    .line 103
    :try_start_0
    iget-object v3, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v6, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mCxtInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v6}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->isCCMPolicyEnabledForCaller(Landroid/app/enterprise/ContextInfo;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 104
    const-string v3, "TimaSignatureSHA1RSA"

    const-string v6, "CCM is not enabled for the caller"

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    new-instance v3, Ljava/security/SignatureException;

    const-string v6, "Error creating Signature instance"

    invoke-direct {v3, v6}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "TimaSignatureSHA1RSA"

    const-string v6, "Failed at ClientCertificateManager API isCCMPolicyEnabledForPackage-Exception "

    invoke-static {v3, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 109
    new-instance v3, Ljava/security/SignatureException;

    const-string v6, "Error creating Signature instance"

    invoke-direct {v3, v6}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 112
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    const-string v3, "TimaSignatureSHA1RSA"

    const-string v6, "creating new SecPkcs11Configuration"

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    new-instance v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    invoke-direct {v1}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;-><init>()V

    .line 116
    .local v1, "pkcs11Config":Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;
    const-string v3, "CCM"

    invoke-virtual {v1, v3}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->setModuleName(Ljava/lang/String;)V

    .line 117
    const-string v3, "libtlc_tz_ccm.so"

    invoke-virtual {v1, v3}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->setLibraryName(Ljava/lang/String;)V

    .line 118
    const-string v3, "TZ_CCM_C_GetFunctionList"

    invoke-virtual {v1, v3}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->setFunctionListName(Ljava/lang/String;)V

    .line 120
    :try_start_1
    iget-object v3, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v6, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mCxtInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v7, 0x0

    invoke-interface {v3, v6, v7}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->getSlotIdForCaller(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J

    move-result-wide v4

    .line 121
    const-string v3, "TimaSignatureSHA1RSA"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getSlotIdForCaller returned : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 127
    :goto_0
    const-wide/16 v6, 0x0

    cmp-long v3, v6, v4

    if-lez v3, :cond_2

    .line 128
    const-string v3, "TimaSignatureSHA1RSA"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getSlotIdForCaller returned invalid slotid = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    new-instance v3, Ljava/security/SignatureException;

    const-string v6, "Error creating Signature instance"

    invoke-direct {v3, v6}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 122
    :catch_1
    move-exception v0

    .line 123
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-wide/16 v4, -0x1

    .line 124
    const-string v3, "TimaSignatureSHA1RSA"

    const-string v6, "getSlotIdForCaller failed - Exception "

    invoke-static {v3, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 132
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    invoke-virtual {v1, v4, v5}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->setSlotId(J)V

    .line 134
    const-string v3, "TimaSignatureSHA1RSA"

    const-string v6, "successfully created new SecPkcs11Configuration"

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    new-instance v2, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    invoke-direct {v2, v1}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;)V

    .line 137
    .local v2, "pkcs11Provider":Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;
    const-string v3, "TimaSignatureSHA1RSA"

    const-string v6, "successfully created new pkccs11 provider"

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-static {v2}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    .line 140
    const-string v3, "TimaSignatureSHA1RSA"

    const-string v6, "successfully added new pkccs11 provider to the provider list"

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :try_start_2
    const-string v3, "SHA1withRSA"

    const-string v6, "SECPkcs11"

    invoke-static {v3, v6}, Ljava/security/Signature;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Signature;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v3

    return-object v3

    .line 144
    :catch_2
    move-exception v0

    .line 145
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 146
    new-instance v3, Ljava/security/SignatureException;

    const-string v6, "Error creating Signature instance"

    invoke-direct {v3, v6}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v3
.end method


# virtual methods
.method protected engineGetParameter(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mSignature:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->getParameter(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected engineInitSign(Ljava/security/PrivateKey;)V
    .locals 1
    .param p1, "privateKey"    # Ljava/security/PrivateKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mSignature:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;)V

    .line 195
    return-void
.end method

.method protected engineInitVerify(Ljava/security/PublicKey;)V
    .locals 1
    .param p1, "publicKey"    # Ljava/security/PublicKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mSignature:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->initVerify(Ljava/security/PublicKey;)V

    .line 202
    return-void
.end method

.method protected engineSetParameter(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mSignature:Ljava/security/Signature;

    invoke-virtual {v0, p1, p2}, Ljava/security/Signature;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    .line 208
    return-void
.end method

.method protected engineSign()[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 214
    iget-object v0, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mSignature:Ljava/security/Signature;

    invoke-virtual {v0}, Ljava/security/Signature;->sign()[B

    move-result-object v0

    return-object v0
.end method

.method protected engineUpdate(B)V
    .locals 1
    .param p1, "b"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mSignature:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->update(B)V

    .line 222
    return-void
.end method

.method protected engineUpdate(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1, "byteBuffer"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 236
    :try_start_0
    iget-object v1, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mSignature:Ljava/security/Signature;

    invoke-virtual {v1, p1}, Ljava/security/Signature;->update(Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 240
    :goto_0
    return-void

    .line 237
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected engineUpdate([BII)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "off"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 228
    iget-object v0, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mSignature:Ljava/security/Signature;

    invoke-virtual {v0, p1, p2, p3}, Ljava/security/Signature;->update([BII)V

    .line 230
    return-void
.end method

.method protected engineVerify([B)Z
    .locals 1
    .param p1, "inSignature"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/SignatureException;
        }
    .end annotation

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/tima/TimaSignatureSHA1RSA;->mSignature:Ljava/security/Signature;

    invoke-virtual {v0, p1}, Ljava/security/Signature;->verify([B)Z

    move-result v0

    return v0
.end method
