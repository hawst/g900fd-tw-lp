.class public Lcom/sec/tima/TimaKeyStore;
.super Ljava/security/KeyStoreSpi;
.source "TimaKeyStore.java"


# static fields
.field private static final CCM_FN_LIST_NAME:Ljava/lang/String; = "TZ_CCM_C_GetFunctionList"

.field private static final CCM_LIB_NAME:Ljava/lang/String; = "libtlc_tz_ccm.so"

.field private static final CCM_MODULE_NAME:Ljava/lang/String; = "CCM"

.field private static final CCM_VERSION:Ljava/lang/String; = "3.0"

.field public static final NAME:Ljava/lang/String; = "TimaKeyStore"

.field private static final ZERO_LENGTH_ALIAS:Ljava/lang/String; = "comSecTima0LenAlias"


# instance fields
.field private cxtInfo:Landroid/app/enterprise/ContextInfo;

.field private defaultpassword:[C

.field private mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

.field private mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

.field private mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

.field private myUID:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/security/KeyStoreSpi;-><init>()V

    .line 126
    const-string v0, "timakeystore"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lcom/sec/tima/TimaKeyStore;->defaultpassword:[C

    .line 127
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/tima/TimaKeyStore;->myUID:I

    return-void
.end method

.method private checkDeleteEntry(Ljava/lang/String;)V
    .locals 4
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 194
    :try_start_0
    iget-object v1, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/tima/TimaKeyStoreAdapter;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 195
    iget-object v1, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/tima/TimaKeyStoreAdapter;->delete(Ljava/lang/String;)Z

    .line 198
    :cond_0
    iget-object v1, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-virtual {v1, p1}, Ljava/security/KeyStoreSpi;->engineContainsAlias(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 199
    iget-object v1, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    instance-of v1, v1, Landroid/security/AndroidKeyStore;

    if-eqz v1, :cond_2

    .line 200
    iget-object v1, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-virtual {v1, p1}, Ljava/security/KeyStoreSpi;->engineDeleteEntry(Ljava/lang/String;)V

    .line 209
    :cond_1
    :goto_0
    return-void

    .line 202
    :cond_2
    iget-object v1, p0, Lcom/sec/tima/TimaKeyStore;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/tima/TimaKeyStore;->cxtInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->deleteCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 205
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "TimaKeyStore"

    const-string v2, "Error removing duplicate key"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private static deserialize([B)Ljavax/crypto/SecretKey;
    .locals 10
    .param p0, "seal_key"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/ClassNotFoundException;
        }
    .end annotation

    .prologue
    .line 256
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 257
    .local v1, "bIn":Ljava/io/ByteArrayInputStream;
    new-instance v2, Ljava/io/DataInputStream;

    invoke-direct {v2, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 259
    .local v2, "dIn":Ljava/io/DataInputStream;
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v5

    .line 260
    .local v5, "format":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 261
    .local v0, "algorithm":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readInt()I

    move-result v7

    new-array v4, v7, [B

    .line 264
    .local v4, "enc":[B
    invoke-virtual {v2, v4}, Ljava/io/DataInputStream;->readFully([B)V

    .line 266
    const-string v7, "PKCS#8"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "PKCS8"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 267
    :cond_0
    new-instance v6, Ljava/security/spec/PKCS8EncodedKeySpec;

    invoke-direct {v6, v4}, Ljava/security/spec/PKCS8EncodedKeySpec;-><init>([B)V

    .line 277
    .local v6, "spec":Ljava/security/spec/KeySpec;
    :goto_0
    :try_start_0
    const-string v7, "BC"

    invoke-static {v0, v7}, Ljavax/crypto/SecretKeyFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/SecretKeyFactory;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljavax/crypto/SecretKeyFactory;->generateSecret(Ljava/security/spec/KeySpec;)Ljavax/crypto/SecretKey;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .end local v6    # "spec":Ljava/security/spec/KeySpec;
    :goto_1
    return-object v7

    .line 268
    :cond_1
    const-string v7, "X.509"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "X509"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 269
    :cond_2
    new-instance v6, Ljava/security/spec/X509EncodedKeySpec;

    invoke-direct {v6, v4}, Ljava/security/spec/X509EncodedKeySpec;-><init>([B)V

    .restart local v6    # "spec":Ljava/security/spec/KeySpec;
    goto :goto_0

    .line 270
    .end local v6    # "spec":Ljava/security/spec/KeySpec;
    :cond_3
    const-string v7, "RAW"

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 271
    new-instance v7, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v7, v4, v0}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    goto :goto_1

    .line 273
    :cond_4
    new-instance v7, Ljava/io/IOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Key format "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " not recognised!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 279
    .restart local v6    # "spec":Ljava/security/spec/KeySpec;
    :catch_0
    move-exception v3

    .line 280
    .local v3, "e":Ljava/lang/Exception;
    new-instance v7, Ljava/io/IOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception deserialize key: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7
.end method

.method private getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    instance-of v0, v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/sec/tima/TimaKeyStore;->myUID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .end local p1    # "alias":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method private getAliasWithoutUID(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 170
    iget-object v2, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    instance-of v2, v2, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;

    if-eqz v2, :cond_0

    .line 171
    if-nez p1, :cond_1

    .line 172
    const-string v2, "TimaKeyStore"

    const-string v3, "null alias received"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object p1, v1

    .line 188
    .end local p1    # "alias":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 176
    .restart local p1    # "alias":Ljava/lang/String;
    :cond_1
    const-string v2, "_"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 177
    .local v0, "index":I
    if-gez v0, :cond_2

    move-object p1, v1

    .line 178
    goto :goto_0

    .line 181
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/sec/tima/TimaKeyStore;->myUID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move-object p1, v1

    .line 182
    goto :goto_0

    .line 185
    :cond_3
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private getFormattedPrivateKey(Ljava/security/interfaces/RSAPrivateKey;Ljava/security/interfaces/RSAPublicKey;)[B
    .locals 13
    .param p1, "rsaPrivateKey"    # Ljava/security/interfaces/RSAPrivateKey;
    .param p2, "rsaPublicKey"    # Ljava/security/interfaces/RSAPublicKey;

    .prologue
    .line 333
    const/4 v0, 0x3

    .line 334
    .local v0, "NO_OF_TYPES":I
    const/4 v1, 0x4

    .line 335
    .local v1, "TYPE_SIZE_IN_BYTES":I
    const/4 v2, 0x4

    .line 336
    .local v2, "VALUE_LEN_SIZE_IN_BYTES":I
    const/4 v5, 0x0

    .line 338
    .local v5, "output":[B
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 339
    :cond_0
    const-string v11, "TimaKeyStore"

    const-string v12, "Failed to import privatekey"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v5

    .line 373
    .end local v5    # "output":[B
    .local v6, "output":[B
    :goto_0
    return-object v6

    .line 344
    .end local v6    # "output":[B
    .restart local v5    # "output":[B
    :cond_1
    :try_start_0
    invoke-interface {p1}, Ljava/security/interfaces/RSAPrivateKey;->getModulus()Ljava/math/BigInteger;

    move-result-object v11

    invoke-virtual {v11}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/sec/tima/TimaKeyStore;->trimByteArray([B)[B

    move-result-object v4

    .line 345
    .local v4, "mod":[B
    invoke-interface {p1}, Ljava/security/interfaces/RSAPrivateKey;->getPrivateExponent()Ljava/math/BigInteger;

    move-result-object v11

    invoke-virtual {v11}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/sec/tima/TimaKeyStore;->trimByteArray([B)[B

    move-result-object v9

    .line 347
    .local v9, "pExp":[B
    invoke-interface {p2}, Ljava/security/interfaces/RSAPublicKey;->getPublicExponent()Ljava/math/BigInteger;

    move-result-object v11

    invoke-virtual {v11}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/sec/tima/TimaKeyStore;->trimByteArray([B)[B

    move-result-object v10

    .line 350
    .local v10, "pubExp":[B
    array-length v11, v4

    array-length v12, v9

    add-int/2addr v11, v12

    array-length v12, v10

    add-int/2addr v11, v12

    add-int/lit8 v8, v11, 0x18

    .line 352
    .local v8, "outputLength":I
    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 353
    .local v7, "outputBuffer":Ljava/nio/ByteBuffer;
    sget-object v11, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v7, v11}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 355
    const/16 v11, 0x120

    invoke-virtual {v7, v11}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 356
    array-length v11, v4

    invoke-virtual {v7, v11}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 357
    invoke-virtual {v7, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 359
    const/16 v11, 0x122

    invoke-virtual {v7, v11}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 360
    array-length v11, v10

    invoke-virtual {v7, v11}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 361
    invoke-virtual {v7, v10}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 363
    const/16 v11, 0x123

    invoke-virtual {v7, v11}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 364
    array-length v11, v9

    invoke-virtual {v7, v11}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 365
    invoke-virtual {v7, v9}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 367
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .end local v4    # "mod":[B
    .end local v7    # "outputBuffer":Ljava/nio/ByteBuffer;
    .end local v8    # "outputLength":I
    .end local v9    # "pExp":[B
    .end local v10    # "pubExp":[B
    :goto_1
    move-object v6, v5

    .line 373
    .end local v5    # "output":[B
    .restart local v6    # "output":[B
    goto :goto_0

    .line 368
    .end local v6    # "output":[B
    .restart local v5    # "output":[B
    :catch_0
    move-exception v3

    .line 369
    .local v3, "e":Ljava/lang/Exception;
    const-string v11, "TimaKeyStore"

    const-string v12, "Unable to format private key for ccm"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 371
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private getPkcs11KeyStoreInstance()Ljava/security/KeyStoreSpi;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 131
    const-wide/16 v4, -0x1

    .line 132
    .local v4, "slotid":J
    const-string v6, "TimaKeyStore"

    const-string v7, "in getPkcs11KeyStoreInstance"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    new-instance v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    invoke-direct {v1}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;-><init>()V

    .line 136
    .local v1, "pkcs11Config":Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;
    const-string v6, "CCM"

    invoke-virtual {v1, v6}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->setModuleName(Ljava/lang/String;)V

    .line 137
    const-string v6, "libtlc_tz_ccm.so"

    invoke-virtual {v1, v6}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->setLibraryName(Ljava/lang/String;)V

    .line 138
    const-string v6, "TZ_CCM_C_GetFunctionList"

    invoke-virtual {v1, v6}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->setFunctionListName(Ljava/lang/String;)V

    .line 141
    :try_start_0
    iget-object v6, p0, Lcom/sec/tima/TimaKeyStore;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v7, p0, Lcom/sec/tima/TimaKeyStore;->cxtInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->getSlotIdForCaller(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J

    move-result-wide v4

    .line 142
    const-string v6, "TimaKeyStore"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSlotIdForCaller returned : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :goto_0
    const-wide/16 v6, 0x0

    cmp-long v6, v6, v4

    if-lez v6, :cond_0

    .line 150
    const-string v6, "TimaKeyStore"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSlotIdForCaller returned invalid slotid = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :goto_1
    return-object v3

    .line 143
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Landroid/os/RemoteException;
    const-wide/16 v4, -0x1

    .line 145
    const-string v6, "TimaKeyStore"

    const-string v7, "getSlotIdForCaller failed - Exception "

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 155
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    invoke-virtual {v1, v4, v5}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->setSlotId(J)V

    .line 156
    new-instance v2, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    invoke-direct {v2, v1}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;)V

    .line 157
    .local v2, "pkcs11Provider":Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;
    invoke-static {v2}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    .line 159
    const-string v3, "TimaKeyStore"

    const-string v6, "successfully added pkcs11 provider to the provider list"

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    new-instance v3, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;

    invoke-direct {v3, v2}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;)V

    goto :goto_1
.end method

.method private getUniqueAliases()Ljava/util/Set;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v12, 0x5f

    .line 285
    iget-object v9, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-virtual {v9}, Ljava/security/KeyStoreSpi;->engineAliases()Ljava/util/Enumeration;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/sec/tima/TimaKeyStore;->getValidAliasList(Ljava/util/Enumeration;)Ljava/util/Enumeration;

    move-result-object v6

    .line 287
    .local v6, "rawAliasesRSAKeyStore":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    iget-object v9, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    const-string v10, ""

    invoke-virtual {v9, v10}, Lcom/sec/tima/TimaKeyStoreAdapter;->saw(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 289
    .local v7, "rawAliasesTimaKeyStore":[Ljava/lang/String;
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 291
    .local v1, "aliases":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v6, :cond_2

    .line 292
    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 293
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-direct {p0, v9}, Lcom/sec/tima/TimaKeyStore;->getAliasWithoutUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 294
    .local v8, "tempString":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 295
    const-string v9, "comSecTima0LenAlias"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 296
    const-string v9, ""

    invoke-interface {v1, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 298
    :cond_1
    invoke-interface {v1, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 304
    .end local v8    # "tempString":Ljava/lang/String;
    :cond_2
    if-eqz v7, :cond_6

    .line 305
    move-object v2, v7

    .local v2, "arr$":[Ljava/lang/String;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v5, :cond_6

    aget-object v0, v2, v3

    .line 306
    .local v0, "alias":Ljava/lang/String;
    invoke-virtual {v0, v12}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 307
    .local v4, "idx":I
    add-int/lit8 v9, v4, 0x1

    invoke-virtual {v0, v12, v9}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 309
    add-int/lit8 v9, v4, 0x1

    invoke-virtual {v0, v12, v9}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 310
    const/4 v9, -0x1

    if-eq v4, v9, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    if-gt v9, v4, :cond_4

    .line 311
    :cond_3
    const-string v9, "TimaKeyStore"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "invalid alias: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 314
    :cond_4
    new-instance v8, Ljava/lang/String;

    add-int/lit8 v9, v4, 0x1

    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 315
    .restart local v8    # "tempString":Ljava/lang/String;
    const-string v9, "comSecTima0LenAlias"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 316
    const-string v9, ""

    invoke-interface {v1, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 318
    :cond_5
    invoke-interface {v1, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 323
    .end local v0    # "alias":Ljava/lang/String;
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "idx":I
    .end local v5    # "len$":I
    .end local v8    # "tempString":Ljava/lang/String;
    :cond_6
    return-object v1
.end method

.method private getValidAliasList(Ljava/util/Enumeration;)Ljava/util/Enumeration;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 875
    .local p1, "aliasList":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :try_start_0
    iget-object v3, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    instance-of v3, v3, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;

    if-eqz v3, :cond_0

    .line 876
    iget-object v3, p0, Lcom/sec/tima/TimaKeyStore;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v4, p0, Lcom/sec/tima/TimaKeyStore;->cxtInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->getAliasesForCaller(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v2

    .line 878
    .local v2, "validAliasList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v0

    .line 881
    .local v0, "aliasesInSlot":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    invoke-interface {v0, v2}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 883
    invoke-static {v2}, Ljava/util/Collections;->enumeration(Ljava/util/Collection;)Ljava/util/Enumeration;

    move-result-object p1

    .line 895
    .end local v0    # "aliasesInSlot":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "validAliasList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local p1    # "aliasList":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-object p1

    .line 885
    .restart local v0    # "aliasesInSlot":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "validAliasList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local p1    # "aliasList":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :cond_1
    const-string v3, "TimaKeyStore"

    const-string v4, "getValidAliasList: returning empty alias list"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 886
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Collections;->enumeration(Ljava/util/Collection;)Ljava/util/Enumeration;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto :goto_0

    .line 892
    .end local v0    # "aliasesInSlot":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "validAliasList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 893
    .local v1, "re":Landroid/os/RemoteException;
    const-string v3, "TimaKeyStore"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to list valid aliases, Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 894
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    .line 895
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Collections;->enumeration(Ljava/util/Collection;)Ljava/util/Enumeration;

    move-result-object p1

    goto :goto_0
.end method

.method private isAliasAcessPermitted(Ljava/lang/String;)Z
    .locals 7
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 904
    :try_start_0
    iget-object v4, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    instance-of v4, v4, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;

    if-eqz v4, :cond_0

    .line 905
    iget-object v4, p0, Lcom/sec/tima/TimaKeyStore;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    if-eqz v4, :cond_0

    .line 906
    const-wide/16 v2, -0x1

    .line 907
    .local v2, "slotid":J
    iget-object v4, p0, Lcom/sec/tima/TimaKeyStore;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v5, p0, Lcom/sec/tima/TimaKeyStore;->cxtInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v4, v5, p1}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->getSlotIdForCaller(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J

    move-result-wide v2

    .line 908
    const-string v4, "TimaKeyStore"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isAliasAcessPermitted - getSlotIdForCaller returned : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 911
    const-wide/16 v4, 0x0

    cmp-long v4, v4, v2

    if-lez v4, :cond_0

    .line 922
    .end local v2    # "slotid":J
    :goto_0
    return v1

    .line 916
    :catch_0
    move-exception v0

    .line 917
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "TimaKeyStore"

    const-string v5, "isAliasAcessPermitted - getSlotIdForCaller failed - Exception "

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 919
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 922
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private isKeyEntry(Ljava/lang/String;)Z
    .locals 2
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 327
    iget-object v0, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/tima/TimaKeyStoreAdapter;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/KeyStoreSpi;->engineIsKeyEntry(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isKeyTypeSupported(Ljava/security/PrivateKey;)Z
    .locals 2
    .param p1, "pKey"    # Ljava/security/PrivateKey;

    .prologue
    .line 927
    iget-object v1, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    instance-of v1, v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;

    if-eqz v1, :cond_0

    .line 928
    instance-of v1, p1, Ljava/security/interfaces/RSAPrivateKey;

    .line 933
    :goto_0
    return v1

    .line 932
    :cond_0
    invoke-interface {p1}, Ljava/security/PrivateKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    .line 933
    .local v0, "keyType":Ljava/lang/String;
    const-string v1, "RSA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "DSA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "EC"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static serialize(Ljavax/crypto/SecretKey;)[B
    .locals 4
    .param p0, "key"    # Ljavax/crypto/SecretKey;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 241
    invoke-interface {p0}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v2

    .line 242
    .local v2, "enc":[B
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 243
    .local v0, "bOut":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 245
    .local v1, "dOut":Ljava/io/DataOutputStream;
    invoke-interface {p0}, Ljavax/crypto/SecretKey;->getFormat()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 246
    invoke-interface {p0}, Ljavax/crypto/SecretKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 247
    array-length v3, v2

    invoke-virtual {v1, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    .line 248
    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->write([B)V

    .line 249
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 251
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    return-object v3
.end method

.method private setSecretKeyEntry(Ljava/lang/String;Ljavax/crypto/SecretKey;[C)V
    .locals 6
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "key"    # Ljavax/crypto/SecretKey;
    .param p3, "password"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 213
    const-string v3, "TimaKeyStore"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "alias: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    iget-object v3, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/security/KeyStoreSpi;->engineContainsAlias(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 216
    new-instance v3, Ljava/security/KeyStoreException;

    const-string v4, "Entry exists and is not a Secret key"

    invoke-direct {v3, v4}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 219
    :cond_0
    invoke-interface {p2}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v1

    .line 220
    .local v1, "keyBytes":[B
    if-nez v1, :cond_1

    .line 221
    new-instance v3, Ljava/security/KeyStoreException;

    const-string v4, "SecretKey has no encoding"

    invoke-direct {v3, v4}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 224
    :cond_1
    const/4 v2, 0x0

    .line 226
    .local v2, "serialized_key":[B
    :try_start_0
    invoke-static {p2}, Lcom/sec/tima/TimaKeyStore;->serialize(Ljavax/crypto/SecretKey;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 232
    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->checkDeleteEntry(Ljava/lang/String;)V

    .line 233
    iget-object v3, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    const/4 v4, -0x1

    invoke-virtual {v3, p1, v2, v4, p3}, Lcom/sec/tima/TimaKeyStoreAdapter;->put(Ljava/lang/String;[BI[C)I

    move-result v3

    if-eqz v3, :cond_2

    .line 235
    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->checkDeleteEntry(Ljava/lang/String;)V

    .line 236
    new-instance v3, Ljava/security/KeyStoreException;

    const-string v4, "put failed for SecretKey"

    invoke-direct {v3, v4}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 227
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 229
    new-instance v3, Ljava/security/KeyStoreException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "serialization error: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 238
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    return-void
.end method

.method private trimByteArray([B)[B
    .locals 4
    .param p1, "inBytes"    # [B

    .prologue
    const/4 v3, 0x0

    .line 377
    if-nez p1, :cond_0

    .line 378
    const/4 v0, 0x0

    .line 386
    :goto_0
    return-object v0

    .line 381
    :cond_0
    aget-byte v1, p1, v3

    if-nez v1, :cond_1

    .line 382
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    new-array v0, v1, [B

    .line 383
    .local v0, "outBytes":[B
    const/4 v1, 0x1

    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    invoke-static {p1, v1, v0, v3, v2}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_0

    .end local v0    # "outBytes":[B
    :cond_1
    move-object v0, p1

    .line 386
    goto :goto_0
.end method


# virtual methods
.method public engineAliases()Ljava/util/Enumeration;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 745
    invoke-direct {p0}, Lcom/sec/tima/TimaKeyStore;->getUniqueAliases()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->enumeration(Ljava/util/Collection;)Ljava/util/Enumeration;

    move-result-object v0

    return-object v0
.end method

.method public engineContainsAlias(Ljava/lang/String;)Z
    .locals 2
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 750
    if-nez p1, :cond_0

    .line 751
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "alias == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 754
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p1, "comSecTima0LenAlias"

    .line 756
    :cond_1
    iget-object v0, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/tima/TimaKeyStoreAdapter;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/KeyStoreSpi;->engineContainsAlias(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public engineDeleteEntry(Ljava/lang/String;)V
    .locals 4
    .param p1, "alias"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 715
    if-nez p1, :cond_0

    .line 716
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "alias == null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 719
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string p1, "comSecTima0LenAlias"

    .line 721
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->isKeyEntry(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 722
    iget-object v1, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/tima/TimaKeyStoreAdapter;->delete(Ljava/lang/String;)Z

    .line 725
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->isKeyEntry(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/security/KeyStoreSpi;->engineIsCertificateEntry(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 727
    :cond_3
    iget-object v1, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    instance-of v1, v1, Landroid/security/AndroidKeyStore;

    if-eqz v1, :cond_5

    .line 728
    iget-object v1, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-virtual {v1, p1}, Ljava/security/KeyStoreSpi;->engineDeleteEntry(Ljava/lang/String;)V

    .line 741
    :cond_4
    return-void

    .line 731
    :cond_5
    :try_start_0
    iget-object v1, p0, Lcom/sec/tima/TimaKeyStore;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/tima/TimaKeyStore;->cxtInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->deleteCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 732
    new-instance v1, Ljava/security/KeyStoreException;

    invoke-direct {v1}, Ljava/security/KeyStoreException;-><init>()V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 734
    :catch_0
    move-exception v0

    .line 735
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 736
    new-instance v1, Ljava/security/KeyStoreException;

    const-string v2, "Error deleting key/cert from keystore"

    invoke-direct {v1, v2, v0}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public engineGetCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;
    .locals 2
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 537
    if-nez p1, :cond_0

    .line 538
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "alias == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 541
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p1, "comSecTima0LenAlias"

    .line 543
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/tima/TimaKeyStore;->isAliasAcessPermitted(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 544
    const/4 v0, 0x0

    .line 547
    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/KeyStoreSpi;->engineGetCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;

    move-result-object v0

    goto :goto_0
.end method

.method public engineGetCertificateAlias(Ljava/security/cert/Certificate;)Ljava/lang/String;
    .locals 1
    .param p1, "cert"    # Ljava/security/cert/Certificate;

    .prologue
    .line 793
    if-nez p1, :cond_0

    .line 794
    const/4 v0, 0x0

    .line 797
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-virtual {v0, p1}, Ljava/security/KeyStoreSpi;->engineGetCertificateAlias(Ljava/security/cert/Certificate;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public engineGetCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;
    .locals 2
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 522
    if-nez p1, :cond_0

    .line 523
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "alias == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 526
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p1, "comSecTima0LenAlias"

    .line 528
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/tima/TimaKeyStore;->isAliasAcessPermitted(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 529
    const/4 v0, 0x0

    .line 532
    :goto_0
    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/KeyStoreSpi;->engineGetCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;

    move-result-object v0

    goto :goto_0
.end method

.method public engineGetCreationDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 4
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 552
    if-nez p1, :cond_0

    .line 553
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "alias == null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 556
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string p1, "comSecTima0LenAlias"

    .line 558
    :cond_1
    iget-object v2, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    invoke-virtual {v2, p1}, Lcom/sec/tima/TimaKeyStoreAdapter;->getmtime(Ljava/lang/String;)J

    move-result-wide v0

    .line 559
    .local v0, "epochMillis":J
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    .line 560
    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/tima/TimaKeyStore;->isAliasAcessPermitted(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 561
    const/4 v2, 0x0

    .line 565
    :goto_0
    return-object v2

    .line 563
    :cond_2
    iget-object v2, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/security/KeyStoreSpi;->engineGetCreationDate(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    goto :goto_0

    .line 565
    :cond_3
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    goto :goto_0
.end method

.method public engineGetKey(Ljava/lang/String;[C)Ljava/security/Key;
    .locals 5
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "password"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/UnrecoverableKeyException;
        }
    .end annotation

    .prologue
    .line 485
    if-nez p1, :cond_0

    .line 486
    new-instance v3, Ljava/security/UnrecoverableKeyException;

    const-string v4, "alias == null"

    invoke-direct {v3, v4}, Ljava/security/UnrecoverableKeyException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 489
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string p1, "comSecTima0LenAlias"

    .line 491
    :cond_1
    iget-object v3, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    invoke-virtual {v3, p1}, Lcom/sec/tima/TimaKeyStoreAdapter;->contains(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 492
    if-nez p2, :cond_2

    .line 493
    const-string v3, "TimaKeyStore"

    const-string v4, "password is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    iget-object p2, p0, Lcom/sec/tima/TimaKeyStore;->defaultpassword:[C

    .line 498
    :cond_2
    iget-object v3, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    invoke-virtual {v3, p1, p2}, Lcom/sec/tima/TimaKeyStoreAdapter;->get(Ljava/lang/String;[C)[B

    move-result-object v2

    .line 499
    .local v2, "serialized_key":[B
    if-nez v2, :cond_3

    .line 500
    new-instance v3, Ljava/security/UnrecoverableKeyException;

    const-string v4, "Unable to retreive key"

    invoke-direct {v3, v4}, Ljava/security/UnrecoverableKeyException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 503
    :cond_3
    const/4 v1, 0x0

    .line 505
    .local v1, "mSecretKey":Ljavax/crypto/SecretKey;
    :try_start_0
    invoke-static {v2}, Lcom/sec/tima/TimaKeyStore;->deserialize([B)Ljavax/crypto/SecretKey;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 517
    .end local v1    # "mSecretKey":Ljavax/crypto/SecretKey;
    .end local v2    # "serialized_key":[B
    :goto_0
    return-object v1

    .line 506
    .restart local v1    # "mSecretKey":Ljavax/crypto/SecretKey;
    .restart local v2    # "serialized_key":[B
    :catch_0
    move-exception v0

    .line 507
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/security/UnrecoverableKeyException;

    const-string v4, "deserialization problem"

    invoke-direct {v3, v4}, Ljava/security/UnrecoverableKeyException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 512
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "mSecretKey":Ljavax/crypto/SecretKey;
    .end local v2    # "serialized_key":[B
    :cond_4
    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/tima/TimaKeyStore;->isAliasAcessPermitted(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 513
    new-instance v3, Ljava/security/UnrecoverableKeyException;

    const-string v4, "Access to Key is not permitted"

    invoke-direct {v3, v4}, Ljava/security/UnrecoverableKeyException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 517
    :cond_5
    iget-object v3, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, p2}, Ljava/security/KeyStoreSpi;->engineGetKey(Ljava/lang/String;[C)Ljava/security/Key;

    move-result-object v1

    goto :goto_0
.end method

.method public engineIsCertificateEntry(Ljava/lang/String;)Z
    .locals 2
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 778
    if-nez p1, :cond_0

    .line 779
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "alias == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 782
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p1, "comSecTima0LenAlias"

    .line 784
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/tima/TimaKeyStore;->isAliasAcessPermitted(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 785
    const/4 v0, 0x0

    .line 788
    :goto_0
    return v0

    :cond_2
    iget-object v0, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/security/KeyStoreSpi;->engineIsCertificateEntry(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public engineIsKeyEntry(Ljava/lang/String;)Z
    .locals 2
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 767
    if-nez p1, :cond_0

    .line 768
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "alias == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 771
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p1, "comSecTima0LenAlias"

    .line 773
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->isKeyEntry(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public engineLoad(Ljava/io/InputStream;[C)V
    .locals 10
    .param p1, "stream"    # Ljava/io/InputStream;
    .param p2, "password"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 393
    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    .line 394
    :cond_0
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v8, "InputStream and password are not supported"

    invoke-direct {v7, v8}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 398
    :cond_1
    const/4 v0, 0x0

    .line 399
    .local v0, "ccmVersion":Ljava/lang/String;
    const/4 v2, 0x0

    .line 400
    .local v2, "isCCMEnabled":Z
    const/4 v3, 0x0

    .line 406
    .local v3, "isTimaKeystoreEnabled":Z
    :try_start_0
    const-string v7, "com.sec.enterprise.knox.keystore.TimaKeystore"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    .line 408
    .local v4, "keyStoreClass":Ljava/lang/Class;
    const-string v7, "getInstance"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Class;

    invoke-virtual {v4, v7, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 409
    .local v6, "m":Ljava/lang/reflect/Method;
    const/4 v7, 0x0

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v6, v7, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 410
    .local v5, "keyStorePolicyObj":Ljava/lang/Object;
    const-string v7, "isTimaKeystoreEnabled"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Class;

    invoke-virtual {v4, v7, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 411
    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v6, v5, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 417
    if-nez v3, :cond_2

    .line 418
    iput-object v8, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    .line 419
    iput-object v8, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    .line 420
    new-instance v7, Ljava/security/cert/CertificateException;

    const-string v8, "TIMA Keystore is not enabled"

    invoke-direct {v7, v8}, Ljava/security/cert/CertificateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 412
    .end local v4    # "keyStoreClass":Ljava/lang/Class;
    .end local v5    # "keyStorePolicyObj":Ljava/lang/Object;
    .end local v6    # "m":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v1

    .line 413
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 414
    new-instance v7, Ljava/security/cert/CertificateException;

    invoke-direct {v7, v1}, Ljava/security/cert/CertificateException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 423
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v4    # "keyStoreClass":Ljava/lang/Class;
    .restart local v5    # "keyStorePolicyObj":Ljava/lang/Object;
    .restart local v6    # "m":Ljava/lang/reflect/Method;
    :cond_2
    invoke-static {}, Lcom/sec/tima/TimaKeyStoreAdapter;->getInstance()Lcom/sec/tima/TimaKeyStoreAdapter;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    .line 424
    iget-object v7, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    if-eqz v7, :cond_3

    .line 425
    iget-object v7, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    invoke-virtual {v7}, Lcom/sec/tima/TimaKeyStoreAdapter;->init()I

    move-result v7

    if-eqz v7, :cond_3

    .line 426
    iput-object v8, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    .line 427
    iput-object v8, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    .line 428
    new-instance v7, Ljava/security/cert/CertificateException;

    const-string v8, "TimaKeystore initialization error"

    invoke-direct {v7, v8}, Ljava/security/cert/CertificateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 434
    :cond_3
    :try_start_1
    const-string v7, "knox_ccm_policy"

    invoke-static {v7}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/tima/TimaKeyStore;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    .line 437
    iget-object v7, p0, Lcom/sec/tima/TimaKeyStore;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/sec/tima/TimaKeyStore;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    invoke-interface {v7}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->getCCMVersion()Ljava/lang/String;

    move-result-object v0

    .line 440
    :goto_0
    const-string v7, "3.0"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 441
    const-string v7, "TimaKeyStore"

    const-string v9, "CCM is available on this device"

    invoke-static {v7, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    new-instance v7, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v9

    invoke-direct {v7, v9}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    iput-object v7, p0, Lcom/sec/tima/TimaKeyStore;->cxtInfo:Landroid/app/enterprise/ContextInfo;

    .line 443
    iget-object v7, p0, Lcom/sec/tima/TimaKeyStore;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v9, p0, Lcom/sec/tima/TimaKeyStore;->cxtInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v7, v9}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->isCCMPolicyEnabledForCaller(Landroid/app/enterprise/ContextInfo;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 444
    iget-object v7, p0, Lcom/sec/tima/TimaKeyStore;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v9, p0, Lcom/sec/tima/TimaKeyStore;->cxtInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v7, v9}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->isAccessControlMethodPassword(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v7

    if-nez v7, :cond_7

    .line 445
    const/4 v2, 0x1

    .line 462
    :cond_4
    :goto_1
    if-eqz v0, :cond_5

    if-eqz v2, :cond_5

    .line 463
    const-string v7, "TimaKeyStore"

    const-string v9, "CCM is enabled for this user"

    invoke-static {v7, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    invoke-direct {p0}, Lcom/sec/tima/TimaKeyStore;->getPkcs11KeyStoreInstance()Ljava/security/KeyStoreSpi;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    .line 467
    :cond_5
    iget-object v7, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    if-nez v7, :cond_a

    .line 468
    if-eqz v2, :cond_9

    .line 469
    const-string v7, "TimaKeyStore"

    const-string v9, "Error Creating CCM backed KeyStore, Using AndoirdKeyStore"

    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    :goto_2
    new-instance v7, Landroid/security/AndroidKeyStore;

    invoke-direct {v7}, Landroid/security/AndroidKeyStore;-><init>()V

    iput-object v7, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    .line 479
    :goto_3
    iget-object v7, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-virtual {v7, v8, v8}, Ljava/security/KeyStoreSpi;->engineLoad(Ljava/io/InputStream;[C)V

    .line 480
    return-void

    :cond_6
    move-object v0, v8

    .line 437
    goto :goto_0

    .line 447
    :cond_7
    :try_start_2
    const-string v7, "TimaKeyStore"

    const-string v9, "CCM cannot be used, required password protection is missing"

    invoke-static {v7, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 454
    :catch_1
    move-exception v1

    .line 455
    .local v1, "e":Landroid/os/RemoteException;
    const/4 v0, 0x0

    .line 456
    const/4 v2, 0x0

    .line 457
    const-string v7, "TimaKeyStore"

    const-string v9, "Failed at ClientCertificateManager API isCCMPolicyEnabledForPackage-Exception "

    invoke-static {v7, v9, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 452
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_8
    :try_start_3
    const-string v7, "TimaKeyStore"

    const-string v9, "CCM is NOT available on this device"

    invoke-static {v7, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    .line 472
    :cond_9
    const-string v7, "TimaKeyStore"

    const-string v9, "CCM is NOT enabled for this user"

    invoke-static {v7, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 477
    :cond_a
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v7

    iput v7, p0, Lcom/sec/tima/TimaKeyStore;->myUID:I

    goto :goto_3
.end method

.method public engineSetCertificateEntry(Ljava/lang/String;Ljava/security/cert/Certificate;)V
    .locals 8
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "cert"    # Ljava/security/cert/Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 668
    if-nez p1, :cond_0

    .line 669
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "alias == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 672
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p1, "comSecTima0LenAlias"

    .line 674
    :cond_1
    if-nez p2, :cond_2

    .line 675
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "cert == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 678
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->isKeyEntry(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 679
    new-instance v0, Ljava/security/KeyStoreException;

    const-string v1, "Entry exists and is not a trusted certificate"

    invoke-direct {v0, v1}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 683
    :cond_3
    iget-object v0, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    instance-of v0, v0, Landroid/security/AndroidKeyStore;

    if-eqz v0, :cond_5

    .line 684
    iget-object v0, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-virtual {v0, p1, p2}, Ljava/security/KeyStoreSpi;->engineSetCertificateEntry(Ljava/lang/String;Ljava/security/cert/Certificate;)V

    .line 711
    :cond_4
    return-void

    .line 687
    :cond_5
    :try_start_0
    invoke-virtual {p2}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v7

    .line 688
    .local v7, "pubkey":Ljava/security/PublicKey;
    if-eqz v7, :cond_6

    if-eqz v7, :cond_7

    instance-of v0, v7, Ljava/security/interfaces/RSAPublicKey;

    if-nez v0, :cond_7

    .line 689
    :cond_6
    new-instance v0, Ljava/security/KeyStoreException;

    const-string v1, "Unsupported certificate type"

    invoke-direct {v0, v1}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 705
    .end local v7    # "pubkey":Ljava/security/PublicKey;
    :catch_0
    move-exception v6

    .line 706
    .local v6, "e":Ljava/lang/Exception;
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    .line 707
    new-instance v0, Ljava/security/KeyStoreException;

    const-string v1, "Error installing cert to keystore"

    invoke-direct {v0, v1, v6}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 692
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v7    # "pubkey":Ljava/security/PublicKey;
    :cond_7
    :try_start_1
    new-instance v2, Lcom/sec/enterprise/knox/ccm/CertificateProfile;

    invoke-direct {v2}, Lcom/sec/enterprise/knox/ccm/CertificateProfile;-><init>()V

    .line 693
    .local v2, "profile":Lcom/sec/enterprise/knox/ccm/CertificateProfile;
    const/4 v0, 0x0

    iput-boolean v0, v2, Lcom/sec/enterprise/knox/ccm/CertificateProfile;->isCSRResponse:Z

    .line 694
    const/4 v0, 0x0

    iput-boolean v0, v2, Lcom/sec/enterprise/knox/ccm/CertificateProfile;->allowWiFi:Z

    .line 695
    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/sec/enterprise/knox/ccm/CertificateProfile;->alias:Ljava/lang/String;

    .line 696
    const/4 v0, 0x0

    iput-object v0, v2, Lcom/sec/enterprise/knox/ccm/CertificateProfile;->packageList:Ljava/util/List;

    .line 697
    const/4 v0, 0x0

    iput-boolean v0, v2, Lcom/sec/enterprise/knox/ccm/CertificateProfile;->allowAllPackages:Z

    .line 699
    iget-object v0, p0, Lcom/sec/tima/TimaKeyStore;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v1, p0, Lcom/sec/tima/TimaKeyStore;->cxtInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x1

    invoke-virtual {p2}, Ljava/security/cert/Certificate;->getEncoded()[B

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->installObjectWithProfile(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/ccm/CertificateProfile;I[BLjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 702
    new-instance v0, Ljava/security/KeyStoreException;

    const-string v1, "Error installing cert to keystore"

    invoke-direct {v0, v1}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method public engineSetEntry(Ljava/lang/String;Ljava/security/KeyStore$Entry;Ljava/security/KeyStore$ProtectionParameter;)V
    .locals 7
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "entry"    # Ljava/security/KeyStore$Entry;
    .param p3, "param"    # Ljava/security/KeyStore$ProtectionParameter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 810
    if-nez p2, :cond_0

    .line 811
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "entry == null"

    invoke-direct {v4, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 814
    :cond_0
    if-nez p1, :cond_1

    .line 815
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "alias == null"

    invoke-direct {v4, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 818
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    const-string p1, "comSecTima0LenAlias"

    .line 820
    :cond_2
    instance-of v4, p2, Ljava/security/KeyStore$SecretKeyEntry;

    if-eqz v4, :cond_6

    .line 821
    const/4 v0, 0x0

    .line 822
    .local v0, "password":[C
    if-nez p3, :cond_4

    .line 823
    iget-object v0, p0, Lcom/sec/tima/TimaKeyStore;->defaultpassword:[C

    .end local p3    # "param":Ljava/security/KeyStore$ProtectionParameter;
    :goto_0
    move-object v2, p2

    .line 836
    check-cast v2, Ljava/security/KeyStore$SecretKeyEntry;

    .line 837
    .local v2, "seE":Ljava/security/KeyStore$SecretKeyEntry;
    invoke-virtual {v2}, Ljava/security/KeyStore$SecretKeyEntry;->getSecretKey()Ljavax/crypto/SecretKey;

    move-result-object v4

    invoke-direct {p0, p1, v4, v0}, Lcom/sec/tima/TimaKeyStore;->setSecretKeyEntry(Ljava/lang/String;Ljavax/crypto/SecretKey;[C)V

    .line 871
    .end local v0    # "password":[C
    .end local v2    # "seE":Ljava/security/KeyStore$SecretKeyEntry;
    :cond_3
    :goto_1
    return-void

    .restart local v0    # "password":[C
    .restart local p3    # "param":Ljava/security/KeyStore$ProtectionParameter;
    :cond_4
    move-object v4, p3

    .line 825
    check-cast v4, Ljava/security/KeyStore$PasswordProtection;

    invoke-virtual {v4}, Ljava/security/KeyStore$PasswordProtection;->getPassword()[C

    move-result-object v4

    if-nez v4, :cond_5

    .line 826
    const-string v4, "TimaKeyStore"

    const-string v5, "password is null"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    iget-object v0, p0, Lcom/sec/tima/TimaKeyStore;->defaultpassword:[C

    goto :goto_0

    .line 830
    :cond_5
    check-cast p3, Ljava/security/KeyStore$PasswordProtection;

    .end local p3    # "param":Ljava/security/KeyStore$ProtectionParameter;
    invoke-virtual {p3}, Ljava/security/KeyStore$PasswordProtection;->getPassword()[C

    move-result-object v0

    goto :goto_0

    .line 841
    .end local v0    # "password":[C
    .restart local p3    # "param":Ljava/security/KeyStore$ProtectionParameter;
    :cond_6
    if-eqz p3, :cond_7

    .line 842
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "ProtectionParameter is not supported"

    invoke-direct {v4, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 845
    :cond_7
    iget-object v4, p0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    invoke-virtual {v4, p1}, Lcom/sec/tima/TimaKeyStoreAdapter;->contains(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 846
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "Entry exists and is not an RSA Key"

    invoke-direct {v4, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 849
    :cond_8
    instance-of v4, p2, Ljava/security/KeyStore$PrivateKeyEntry;

    if-eqz v4, :cond_9

    move-object v4, p2

    check-cast v4, Ljava/security/KeyStore$PrivateKeyEntry;

    invoke-virtual {v4}, Ljava/security/KeyStore$PrivateKeyEntry;->getPrivateKey()Ljava/security/PrivateKey;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/tima/TimaKeyStore;->isKeyTypeSupported(Ljava/security/PrivateKey;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 850
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "Unsupported keytype"

    invoke-direct {v4, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 853
    :cond_9
    instance-of v4, p2, Ljava/security/KeyStore$TrustedCertificateEntry;

    if-eqz v4, :cond_a

    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->isKeyEntry(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 854
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "Entry exists and is not a trusted certificate"

    invoke-direct {v4, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 858
    :cond_a
    invoke-direct {p0, p1}, Lcom/sec/tima/TimaKeyStore;->checkDeleteEntry(Ljava/lang/String;)V

    .line 859
    iget-object v4, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    instance-of v4, v4, Landroid/security/AndroidKeyStore;

    if-eqz v4, :cond_b

    .line 860
    iget-object v4, p0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    invoke-virtual {v4, p1, p2, p3}, Ljava/security/KeyStoreSpi;->engineSetEntry(Ljava/lang/String;Ljava/security/KeyStore$Entry;Ljava/security/KeyStore$ProtectionParameter;)V

    goto :goto_1

    .line 862
    :cond_b
    instance-of v4, p2, Ljava/security/KeyStore$TrustedCertificateEntry;

    if-eqz v4, :cond_c

    move-object v3, p2

    .line 863
    check-cast v3, Ljava/security/KeyStore$TrustedCertificateEntry;

    .line 864
    .local v3, "trE":Ljava/security/KeyStore$TrustedCertificateEntry;
    invoke-virtual {v3}, Ljava/security/KeyStore$TrustedCertificateEntry;->getTrustedCertificate()Ljava/security/cert/Certificate;

    move-result-object v4

    invoke-virtual {p0, p1, v4}, Lcom/sec/tima/TimaKeyStore;->engineSetCertificateEntry(Ljava/lang/String;Ljava/security/cert/Certificate;)V

    goto :goto_1

    .line 865
    .end local v3    # "trE":Ljava/security/KeyStore$TrustedCertificateEntry;
    :cond_c
    instance-of v4, p2, Ljava/security/KeyStore$PrivateKeyEntry;

    if-eqz v4, :cond_3

    move-object v1, p2

    .line 866
    check-cast v1, Ljava/security/KeyStore$PrivateKeyEntry;

    .line 867
    .local v1, "prE":Ljava/security/KeyStore$PrivateKeyEntry;
    invoke-virtual {v1}, Ljava/security/KeyStore$PrivateKeyEntry;->getPrivateKey()Ljava/security/PrivateKey;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v1}, Ljava/security/KeyStore$PrivateKeyEntry;->getCertificateChain()[Ljava/security/cert/Certificate;

    move-result-object v6

    invoke-virtual {p0, p1, v4, v5, v6}, Lcom/sec/tima/TimaKeyStore;->engineSetKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V

    goto/16 :goto_1
.end method

.method public engineSetKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V
    .locals 17
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/security/Key;
    .param p3, "password"    # [C
    .param p4, "chain"    # [Ljava/security/cert/Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 571
    if-nez p1, :cond_0

    .line 572
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "alias == null"

    invoke-direct {v4, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 575
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string p1, "comSecTima0LenAlias"

    .line 577
    :cond_1
    if-nez p2, :cond_2

    .line 578
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "key data is NULL"

    invoke-direct {v4, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 581
    :cond_2
    move-object/from16 v0, p2

    instance-of v4, v0, Ljavax/crypto/SecretKey;

    if-eqz v4, :cond_5

    .line 582
    if-nez p3, :cond_3

    .line 583
    const-string v4, "TimaKeyStore"

    const-string v5, "password is null"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/tima/TimaKeyStore;->defaultpassword:[C

    move-object/from16 p3, v0

    .line 587
    :cond_3
    check-cast p2, Ljavax/crypto/SecretKey;

    .end local p2    # "key":Ljava/security/Key;
    invoke-direct/range {p0 .. p3}, Lcom/sec/tima/TimaKeyStore;->setSecretKeyEntry(Ljava/lang/String;Ljavax/crypto/SecretKey;[C)V

    .line 656
    :cond_4
    :goto_0
    return-void

    .line 588
    .restart local p2    # "key":Ljava/security/Key;
    :cond_5
    move-object/from16 v0, p2

    instance-of v4, v0, Ljava/security/PrivateKey;

    if-eqz v4, :cond_d

    .line 589
    if-nez p4, :cond_6

    .line 590
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "Cannot install key without certificate, certificate chain is null"

    invoke-direct {v4, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 594
    :cond_6
    if-eqz p3, :cond_7

    .line 595
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "TimaKeystore does not support password protected RSA keys"

    invoke-direct {v4, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 599
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/tima/TimaKeyStore;->mTimaKeyStore:Lcom/sec/tima/TimaKeyStoreAdapter;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/sec/tima/TimaKeyStoreAdapter;->contains(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 600
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "Entry exists and is not an RSA Key"

    invoke-direct {v4, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_8
    move-object/from16 v4, p2

    .line 604
    check-cast v4, Ljava/security/PrivateKey;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/tima/TimaKeyStore;->isKeyTypeSupported(Ljava/security/PrivateKey;)Z

    move-result v4

    if-nez v4, :cond_9

    .line 605
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "Unsupported keytype"

    invoke-direct {v4, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 608
    :cond_9
    invoke-direct/range {p0 .. p1}, Lcom/sec/tima/TimaKeyStore;->checkDeleteEntry(Ljava/lang/String;)V

    .line 610
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    instance-of v4, v4, Landroid/security/AndroidKeyStore;

    if-eqz v4, :cond_a

    .line 611
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/tima/TimaKeyStore;->mAsymmetricKeystore:Ljava/security/KeyStoreSpi;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v4, v0, v1, v2, v3}, Ljava/security/KeyStoreSpi;->engineSetKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V

    goto :goto_0

    .line 615
    :cond_a
    :try_start_0
    move-object/from16 v0, p2

    check-cast v0, Ljava/security/PrivateKey;

    move-object/from16 v16, v0

    .line 616
    .local v16, "pKey":Ljava/security/PrivateKey;
    new-instance v15, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v15}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 619
    .local v15, "outputStream":Ljava/io/ByteArrayOutputStream;
    new-instance v6, Lcom/sec/enterprise/knox/ccm/CertificateProfile;

    invoke-direct {v6}, Lcom/sec/enterprise/knox/ccm/CertificateProfile;-><init>()V

    .line 620
    .local v6, "profile":Lcom/sec/enterprise/knox/ccm/CertificateProfile;
    const/4 v4, 0x0

    iput-boolean v4, v6, Lcom/sec/enterprise/knox/ccm/CertificateProfile;->isCSRResponse:Z

    .line 621
    const/4 v4, 0x0

    iput-boolean v4, v6, Lcom/sec/enterprise/knox/ccm/CertificateProfile;->allowWiFi:Z

    .line 622
    invoke-direct/range {p0 .. p1}, Lcom/sec/tima/TimaKeyStore;->getAliasWithUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v6, Lcom/sec/enterprise/knox/ccm/CertificateProfile;->alias:Ljava/lang/String;

    .line 623
    const/4 v4, 0x0

    iput-object v4, v6, Lcom/sec/enterprise/knox/ccm/CertificateProfile;->packageList:Ljava/util/List;

    .line 624
    const/4 v4, 0x0

    iput-boolean v4, v6, Lcom/sec/enterprise/knox/ccm/CertificateProfile;->allowAllPackages:Z

    .line 626
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/tima/TimaKeyStore;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/tima/TimaKeyStore;->cxtInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v7, 0x3

    check-cast v16, Ljava/security/interfaces/RSAPrivateKey;

    .end local v16    # "pKey":Ljava/security/PrivateKey;
    const/4 v8, 0x0

    aget-object v8, p4, v8

    invoke-virtual {v8}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v8

    check-cast v8, Ljava/security/interfaces/RSAPublicKey;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v8}, Lcom/sec/tima/TimaKeyStore;->getFormattedPrivateKey(Ljava/security/interfaces/RSAPrivateKey;Ljava/security/interfaces/RSAPublicKey;)[B

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface/range {v4 .. v9}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->installObjectWithProfile(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/ccm/CertificateProfile;I[BLjava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 633
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "Error installing cert to keystore"

    invoke-direct {v4, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 645
    .end local v6    # "profile":Lcom/sec/enterprise/knox/ccm/CertificateProfile;
    .end local v15    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :catch_0
    move-exception v12

    .line 646
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    .line 647
    invoke-virtual/range {p0 .. p1}, Lcom/sec/tima/TimaKeyStore;->engineDeleteEntry(Ljava/lang/String;)V

    .line 648
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "Error installing cert to keystore"

    invoke-direct {v4, v5, v12}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4

    .line 636
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v6    # "profile":Lcom/sec/enterprise/knox/ccm/CertificateProfile;
    .restart local v15    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :cond_b
    move-object/from16 v10, p4

    .local v10, "arr$":[Ljava/security/cert/Certificate;
    :try_start_1
    array-length v14, v10

    .local v14, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_1
    if-ge v13, v14, :cond_c

    aget-object v11, v10, v13

    .line 637
    .local v11, "cert":Ljava/security/cert/Certificate;
    invoke-virtual {v11}, Ljava/security/cert/Certificate;->getEncoded()[B

    move-result-object v4

    invoke-virtual {v15, v4}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 636
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 639
    .end local v11    # "cert":Ljava/security/cert/Certificate;
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/tima/TimaKeyStore;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/tima/TimaKeyStore;->cxtInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v7, 0x1

    invoke-virtual {v15}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface/range {v4 .. v9}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->installObjectWithProfile(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/ccm/CertificateProfile;I[BLjava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 642
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "Error installing cert to keystore"

    invoke-direct {v4, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 653
    .end local v6    # "profile":Lcom/sec/enterprise/knox/ccm/CertificateProfile;
    .end local v10    # "arr$":[Ljava/security/cert/Certificate;
    .end local v13    # "i$":I
    .end local v14    # "len$":I
    .end local v15    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :cond_d
    new-instance v4, Ljava/security/KeyStoreException;

    const-string v5, "Only SecretKeys and PrivateKeys are supported"

    invoke-direct {v4, v5}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public engineSetKeyEntry(Ljava/lang/String;[B[Ljava/security/cert/Certificate;)V
    .locals 2
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "userKey"    # [B
    .param p3, "chain"    # [Ljava/security/cert/Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 661
    new-instance v0, Ljava/security/KeyStoreException;

    const-string v1, "Operation not supported because key encoding is unknown"

    invoke-direct {v0, v1}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public engineSize()I
    .locals 1

    .prologue
    .line 762
    invoke-direct {p0}, Lcom/sec/tima/TimaKeyStore;->getUniqueAliases()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    return v0
.end method

.method public engineStore(Ljava/io/OutputStream;[C)V
    .locals 2
    .param p1, "stream"    # Ljava/io/OutputStream;
    .param p2, "password"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 803
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Can not serialize TIMAKeyStore to OutputStream"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
