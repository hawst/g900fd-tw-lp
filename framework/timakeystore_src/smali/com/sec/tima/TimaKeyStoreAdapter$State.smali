.class public final enum Lcom/sec/tima/TimaKeyStoreAdapter$State;
.super Ljava/lang/Enum;
.source "TimaKeyStoreAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/tima/TimaKeyStoreAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/tima/TimaKeyStoreAdapter$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/tima/TimaKeyStoreAdapter$State;

.field public static final enum LOCKED:Lcom/sec/tima/TimaKeyStoreAdapter$State;

.field public static final enum UNINITIALIZED:Lcom/sec/tima/TimaKeyStoreAdapter$State;

.field public static final enum UNLOCKED:Lcom/sec/tima/TimaKeyStoreAdapter$State;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 61
    new-instance v0, Lcom/sec/tima/TimaKeyStoreAdapter$State;

    const-string v1, "UNLOCKED"

    invoke-direct {v0, v1, v2}, Lcom/sec/tima/TimaKeyStoreAdapter$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/tima/TimaKeyStoreAdapter$State;->UNLOCKED:Lcom/sec/tima/TimaKeyStoreAdapter$State;

    new-instance v0, Lcom/sec/tima/TimaKeyStoreAdapter$State;

    const-string v1, "LOCKED"

    invoke-direct {v0, v1, v3}, Lcom/sec/tima/TimaKeyStoreAdapter$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/tima/TimaKeyStoreAdapter$State;->LOCKED:Lcom/sec/tima/TimaKeyStoreAdapter$State;

    new-instance v0, Lcom/sec/tima/TimaKeyStoreAdapter$State;

    const-string v1, "UNINITIALIZED"

    invoke-direct {v0, v1, v4}, Lcom/sec/tima/TimaKeyStoreAdapter$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/tima/TimaKeyStoreAdapter$State;->UNINITIALIZED:Lcom/sec/tima/TimaKeyStoreAdapter$State;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/tima/TimaKeyStoreAdapter$State;

    sget-object v1, Lcom/sec/tima/TimaKeyStoreAdapter$State;->UNLOCKED:Lcom/sec/tima/TimaKeyStoreAdapter$State;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/tima/TimaKeyStoreAdapter$State;->LOCKED:Lcom/sec/tima/TimaKeyStoreAdapter$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/tima/TimaKeyStoreAdapter$State;->UNINITIALIZED:Lcom/sec/tima/TimaKeyStoreAdapter$State;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/tima/TimaKeyStoreAdapter$State;->$VALUES:[Lcom/sec/tima/TimaKeyStoreAdapter$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/tima/TimaKeyStoreAdapter$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 61
    const-class v0, Lcom/sec/tima/TimaKeyStoreAdapter$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/tima/TimaKeyStoreAdapter$State;

    return-object v0
.end method

.method public static values()[Lcom/sec/tima/TimaKeyStoreAdapter$State;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/tima/TimaKeyStoreAdapter$State;->$VALUES:[Lcom/sec/tima/TimaKeyStoreAdapter$State;

    invoke-virtual {v0}, [Lcom/sec/tima/TimaKeyStoreAdapter$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/tima/TimaKeyStoreAdapter$State;

    return-object v0
.end method
