.class public Lcom/sec/smartcard/pkcs11/PKCS11Exception;
.super Lcom/sec/smartcard/pkcs11/TokenException;
.source "PKCS11Exception.java"


# static fields
.field protected static final ERROR_CODE_PROPERTIES:Ljava/lang/String; = "/com/sec/smartcard/pkcs11/ExceptionMessages.properties"

.field protected static errorCodeNamesAvailable_:Z

.field protected static errorCodeNames_:Ljava/util/Properties;


# instance fields
.field protected errorCode_:J


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .param p1, "errorCode"    # J

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/sec/smartcard/pkcs11/TokenException;-><init>()V

    .line 105
    iput-wide p1, p0, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->errorCode_:J

    .line 106
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "errorCode"    # Ljava/lang/String;

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/sec/smartcard/pkcs11/TokenException;-><init>()V

    .line 118
    invoke-static {p1}, Lcom/sec/smartcard/pkcs11/Functions;->HexStringToLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->errorCode_:J

    .line 119
    return-void
.end method


# virtual methods
.method public getErrorCode()J
    .locals 2

    .prologue
    .line 176
    iget-wide v0, p0, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->errorCode_:J

    return-wide v0
.end method

.method public getErrorCodeString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->errorCode_:J

    long-to-int v2, v2

    invoke-static {v2}, Lcom/sec/smartcard/pkcs11/Functions;->toFullHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 188
    .local v0, "errorCodeHexString":Ljava/lang/String;
    return-object v0
.end method

.method public getMessage()Ljava/lang/String;
    .locals 11

    .prologue
    .line 132
    sget-object v7, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->errorCodeNames_:Ljava/util/Properties;

    if-nez v7, :cond_1

    .line 133
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    monitor-enter v8

    .line 134
    :try_start_0
    sget-object v7, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->errorCodeNames_:Ljava/util/Properties;

    if-nez v7, :cond_0

    .line 135
    new-instance v3, Ljava/util/Properties;

    invoke-direct {v3}, Ljava/util/Properties;-><init>()V

    .line 136
    .local v3, "errorCodeNames":Ljava/util/Properties;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    const-string v9, "/com/sec/smartcard/pkcs11/ExceptionMessages.properties"

    invoke-virtual {v7, v9}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 139
    .local v5, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v3, v5}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V

    .line 140
    sput-object v3, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->errorCodeNames_:Ljava/util/Properties;

    .line 141
    const/4 v7, 0x1

    sput-boolean v7, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->errorCodeNamesAvailable_:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 147
    if-eqz v5, :cond_0

    .line 149
    :try_start_2
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 156
    .end local v3    # "errorCodeNames":Ljava/util/Properties;
    .end local v5    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_0
    :try_start_3
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 161
    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "0x"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p0, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->errorCode_:J

    long-to-int v8, v8

    invoke-static {v8}, Lcom/sec/smartcard/pkcs11/Functions;->toFullHexString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 162
    .local v1, "errorCodeHexString":Ljava/lang/String;
    sget-boolean v7, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->errorCodeNamesAvailable_:Z

    if-eqz v7, :cond_3

    sget-object v7, Lcom/sec/smartcard/pkcs11/PKCS11Exception;->errorCodeNames_:Ljava/util/Properties;

    invoke-virtual {v7, v1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 163
    .local v2, "errorCodeName":Ljava/lang/String;
    :goto_1
    if-eqz v2, :cond_4

    move-object v6, v2

    .line 165
    .local v6, "message":Ljava/lang/String;
    :goto_2
    return-object v6

    .line 150
    .end local v1    # "errorCodeHexString":Ljava/lang/String;
    .end local v2    # "errorCodeName":Ljava/lang/String;
    .end local v6    # "message":Ljava/lang/String;
    .restart local v3    # "errorCodeNames":Ljava/util/Properties;
    .restart local v5    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 156
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "errorCodeNames":Ljava/util/Properties;
    .end local v5    # "in":Ljava/io/InputStream;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v7

    .line 142
    .restart local v3    # "errorCodeNames":Ljava/util/Properties;
    .restart local v5    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v4

    .line 143
    .local v4, "exception":Ljava/lang/Exception;
    :try_start_5
    sget-object v7, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v9, "unable to load property from /com/sec/smartcard/pkcs11/ExceptionMessages.properties"

    invoke-virtual {v7, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 144
    sget-object v7, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Could not read properties for error code names: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 147
    if-eqz v5, :cond_0

    .line 149
    :try_start_6
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 150
    :catch_2
    move-exception v0

    .line 151
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_7
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    .line 147
    .end local v0    # "e":Ljava/io/IOException;
    .end local v4    # "exception":Ljava/lang/Exception;
    :catchall_1
    move-exception v7

    if-eqz v5, :cond_2

    .line 149
    :try_start_8
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 152
    :cond_2
    :goto_3
    :try_start_9
    throw v7

    .line 150
    :catch_3
    move-exception v0

    .line 151
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_3

    .line 162
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "errorCodeNames":Ljava/util/Properties;
    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v1    # "errorCodeHexString":Ljava/lang/String;
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .restart local v2    # "errorCodeName":Ljava/lang/String;
    :cond_4
    move-object v6, v1

    .line 163
    goto :goto_2
.end method
