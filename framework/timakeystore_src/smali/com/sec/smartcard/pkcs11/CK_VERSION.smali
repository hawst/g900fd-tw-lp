.class public Lcom/sec/smartcard/pkcs11/CK_VERSION;
.super Ljava/lang/Object;
.source "CK_VERSION.java"


# instance fields
.field public major:B

.field public minor:B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 100
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 102
    .local v0, "buffer":Ljava/lang/StringBuffer;
    iget-byte v1, p0, Lcom/sec/smartcard/pkcs11/CK_VERSION;->major:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 103
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 104
    iget-byte v1, p0, Lcom/sec/smartcard/pkcs11/CK_VERSION;->minor:B

    const/16 v2, 0xa

    if-ge v1, v2, :cond_0

    .line 105
    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 107
    :cond_0
    iget-byte v1, p0, Lcom/sec/smartcard/pkcs11/CK_VERSION;->minor:B

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 109
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
