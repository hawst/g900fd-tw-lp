.class public Lcom/sec/smartcard/pkcs11/CK_DATE;
.super Ljava/lang/Object;
.source "CK_DATE.java"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field public day:[C

.field public month:[C

.field public year:[C


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 109
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_DATE;

    invoke-direct {v0}, Lcom/sec/smartcard/pkcs11/CK_DATE;-><init>()V

    .line 110
    .local v0, "clone":Lcom/sec/smartcard/pkcs11/CK_DATE;
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->year:[C

    invoke-virtual {v1}, [C->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    iput-object v1, v0, Lcom/sec/smartcard/pkcs11/CK_DATE;->year:[C

    .line 111
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->month:[C

    invoke-virtual {v1}, [C->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    iput-object v1, v0, Lcom/sec/smartcard/pkcs11/CK_DATE;->month:[C

    .line 112
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->day:[C

    invoke-virtual {v1}, [C->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [C

    iput-object v1, v0, Lcom/sec/smartcard/pkcs11/CK_DATE;->day:[C

    .line 114
    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0x2e

    .line 126
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 128
    .local v0, "buffer":Ljava/lang/StringBuffer;
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->day:[C

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 129
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 130
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->month:[C

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 131
    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 132
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->year:[C

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 133
    const-string v1, " (DD.MM.YYYY)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 135
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
