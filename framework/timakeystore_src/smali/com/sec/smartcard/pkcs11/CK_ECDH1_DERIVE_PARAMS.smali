.class public Lcom/sec/smartcard/pkcs11/CK_ECDH1_DERIVE_PARAMS;
.super Ljava/lang/Object;
.source "CK_ECDH1_DERIVE_PARAMS.java"


# instance fields
.field public kdf:J

.field public pPublicData:[B

.field public pSharedData:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 116
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 117
    .local v0, "buffer":Ljava/lang/StringBuffer;
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_ECDH1_DERIVE_PARAMS;->pSharedData:[B

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_ECDH1_DERIVE_PARAMS;->pPublicData:[B

    if-nez v1, :cond_1

    .line 118
    :cond_0
    const/4 v1, 0x0

    .line 144
    :goto_0
    return-object v1

    .line 119
    :cond_1
    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 120
    const-string v1, "kdf: 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 121
    iget-wide v2, p0, Lcom/sec/smartcard/pkcs11/CK_ECDH1_DERIVE_PARAMS;->kdf:J

    invoke-static {v2, v3}, Lcom/sec/smartcard/pkcs11/Functions;->toFullHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 122
    sget-object v1, Lcom/sec/smartcard/pkcs11/Constants;->NEWLINE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 124
    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 125
    const-string v1, "pSharedDataLen: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 126
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_ECDH1_DERIVE_PARAMS;->pSharedData:[B

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 127
    sget-object v1, Lcom/sec/smartcard/pkcs11/Constants;->NEWLINE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 129
    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 130
    const-string v1, "pSharedData: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 131
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_ECDH1_DERIVE_PARAMS;->pSharedData:[B

    invoke-static {v1}, Lcom/sec/smartcard/pkcs11/Functions;->toHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 132
    sget-object v1, Lcom/sec/smartcard/pkcs11/Constants;->NEWLINE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 134
    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 135
    const-string v1, "pPublicDataLen: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 136
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_ECDH1_DERIVE_PARAMS;->pPublicData:[B

    array-length v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 137
    sget-object v1, Lcom/sec/smartcard/pkcs11/Constants;->NEWLINE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 139
    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 140
    const-string v1, "pPublicData: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 141
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_ECDH1_DERIVE_PARAMS;->pPublicData:[B

    invoke-static {v1}, Lcom/sec/smartcard/pkcs11/Functions;->toHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 144
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
