.class public final Lc/j;
.super Lc/a;


# instance fields
.field vm:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lc/a;-><init>()V

    invoke-virtual {p0}, Lc/j;->dY()V

    return-void
.end method


# virtual methods
.method protected a(Lc/f;Lc/g;)I
    .locals 5

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v3, p0, Lc/j;->vm:Z

    if-nez v3, :cond_1

    invoke-virtual {p0}, Lc/j;->dY()V

    iget-boolean v3, p0, Lc/j;->vm:Z

    if-nez v3, :cond_1

    sget-object v1, Lc/j;->TAG:Ljava/lang/String;

    const-string v2, "Setup wizard is foreground yet."

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lc/f;->dJ()Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lc/f;->dI()Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lc/f;->dz()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p2}, Lc/g;->dU()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Lc/f;->dB()Z

    move-result v3

    if-eqz v3, :cond_4

    move v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Lc/g;->dT()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {p1}, Lc/f;->dF()Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {p1}, Lc/f;->dG()Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_5
    invoke-virtual {p2}, Lc/g;->dP()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p2}, Lc/g;->dQ()I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x3

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_0

    :cond_7
    invoke-virtual {p2}, Lc/g;->dW()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {p1}, Lc/f;->dH()Z

    move-result v3

    if-eqz v3, :cond_8

    move v0, v2

    goto :goto_0

    :cond_8
    iget-object v3, p0, Lc/j;->oN:Ljava/lang/String;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lc/j;->oN:Ljava/lang/String;

    const-string v4, "com.sec.android.app.popupcalculator"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {p2}, Lc/g;->dV()Z

    move-result v3

    if-eqz v3, :cond_9

    move v0, v2

    goto :goto_0

    :cond_9
    invoke-virtual {p2}, Lc/g;->dX()Z

    move-result v2

    if-eqz v2, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    invoke-virtual {p2}, Lc/g;->dP()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p2}, Lc/g;->dQ()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lc/f;->isMultiWindow()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lc/f;->isStatusBarVisible()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lc/f;->dD()Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "com.sec.android.app.sbrowser"

    iget-object v3, p0, Lc/j;->oN:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {p2}, Lc/g;->dR()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_b
    iget-boolean v2, p0, Lc/j;->uv:Z

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lc/f;->dB()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public dY()V
    .locals 3

    const-string v0, "FINISH"

    const-string v1, "persist.sys.setupwizard"

    const-string v2, "FINISH"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lc/j;->vm:Z

    iget-boolean v0, p0, Lc/j;->vm:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lc/j;->uG:Lc/f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lc/f;->W(Z)V

    :cond_0
    return-void
.end method
