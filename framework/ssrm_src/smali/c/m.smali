.class public Lc/m;
.super Lb/p;

# interfaces
.implements Lb/B;
.implements Lb/t;


# instance fields
.field final TAG:Ljava/lang/String;

.field mHandler:Landroid/os/Handler;

.field nr:I

.field tH:Z

.field final vq:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    const-class v0, Lc/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc/m;->TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lc/m;->tH:Z

    const/4 v0, 0x1

    iput v0, p0, Lc/m;->vq:I

    iput v1, p0, Lc/m;->nr:I

    iget-object v0, p0, Lc/m;->TAG:Ljava/lang/String;

    const-string v1, "/sys/class/sec/tsp/cmd_list"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->e(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "stylus_enable"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "com.samsung.android.snote"

    invoke-virtual {p0, v0}, Lc/m;->addPackage(Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lc/n;

    invoke-static {}, Lcom/android/server/ssrm/ag;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lc/n;-><init>(Lc/m;Landroid/os/Looper;)V

    iput-object v0, p0, Lc/m;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method private cc()V
    .locals 4

    iget-boolean v0, p0, Lc/m;->tH:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget v1, p0, Lc/m;->nr:I

    if-eq v1, v0, :cond_0

    iput v0, p0, Lc/m;->nr:I

    iget-object v1, p0, Lc/m;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stylus_enable,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cb()V
    .locals 0

    return-void
.end method

.method public onScreenOn()V
    .locals 4

    iget-object v0, p0, Lc/m;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lc/m;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x320

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public z(Z)V
    .locals 0

    iput-boolean p1, p0, Lc/m;->tH:Z

    invoke-direct {p0}, Lc/m;->cc()V

    return-void
.end method
