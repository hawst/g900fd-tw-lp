.class public final Lc/h;
.super Lc/a;


# instance fields
.field vl:Z

.field vm:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lc/a;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lc/h;->vl:Z

    sget-boolean v0, Lcom/android/server/ssrm/N;->dk:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lc/h;->vl:Z

    :cond_0
    invoke-virtual {p0}, Lc/h;->dY()V

    return-void
.end method


# virtual methods
.method protected a(Lc/f;Lc/g;)I
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lc/h;->vm:Z

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lc/h;->dY()V

    iget-boolean v2, p0, Lc/h;->vm:Z

    if-nez v2, :cond_1

    sget-object v1, Lc/h;->TAG:Ljava/lang/String;

    const-string v2, "Setup wizard is foreground yet."

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lc/f;->dJ()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lc/f;->dI()Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lc/f;->dz()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p2}, Lc/g;->dX()Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p2}, Lc/g;->dO()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lc/h;->vl:Z

    if-eqz v2, :cond_5

    invoke-virtual {p1}, Lc/f;->dC()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_5
    invoke-virtual {p1}, Lc/f;->isMultiWindow()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lc/f;->isStatusBarVisible()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lc/f;->dD()Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "com.sec.android.app.sbrowser"

    iget-object v3, p0, Lc/h;->oN:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p2}, Lc/g;->dR()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_6
    iget-boolean v2, p0, Lc/h;->uv:Z

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lc/f;->dB()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public dY()V
    .locals 3

    const-string v0, "FINISH"

    const-string v1, "persist.sys.setupwizard"

    const-string v2, "FINISH"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lc/h;->vm:Z

    iget-boolean v0, p0, Lc/h;->vm:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lc/h;->uG:Lc/f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lc/f;->W(Z)V

    :cond_0
    return-void
.end method
