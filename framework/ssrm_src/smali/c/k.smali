.class public Lc/k;
.super Lb/p;

# interfaces
.implements Lb/r;


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field mResolver:Landroid/content/ContentResolver;

.field vn:Lc/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lc/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc/k;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lb/p;-><init>()V

    return-void
.end method


# virtual methods
.method public ag(Z)V
    .locals 3

    sget-object v1, Lc/k;->TAG:Ljava/lang/String;

    const-string v2, "/sys/class/sec/tsp/cmd"

    if-eqz p1, :cond_0

    const-string v0, "glove_mode,1"

    :goto_0
    invoke-static {v1, v2, v0}, Le/v;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "glove_mode,0"

    goto :goto_0
.end method

.method public ci()V
    .locals 5

    new-instance v0, Lc/l;

    invoke-direct {v0, p0}, Lc/l;-><init>(Lc/k;)V

    iput-object v0, p0, Lc/k;->vn:Lc/l;

    sget-object v0, Lc/k;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lc/k;->mResolver:Landroid/content/ContentResolver;

    iget-object v0, p0, Lc/k;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "auto_adjust_touch"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lc/k;->vn:Lc/l;

    const/4 v4, -0x2

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lc/k;->vn:Lc/l;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lc/l;->onChange(Z)V

    return-void
.end method
