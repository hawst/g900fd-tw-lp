.class Lc/l;
.super Landroid/database/ContentObserver;


# instance fields
.field vo:Z

.field final synthetic vp:Lc/k;


# direct methods
.method constructor <init>(Lc/k;)V
    .locals 2

    iput-object p1, p0, Lc/l;->vp:Lc/k;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/ssrm/ag;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lc/l;->vo:Z

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lc/l;->vp:Lc/k;

    iget-object v2, v2, Lc/k;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "auto_adjust_touch"

    const/4 v4, -0x2

    invoke-static {v2, v3, v1, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    iget-boolean v1, p0, Lc/l;->vo:Z

    if-ne v1, v0, :cond_1

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iput-boolean v0, p0, Lc/l;->vo:Z

    iget-object v1, p0, Lc/l;->vp:Lc/k;

    invoke-virtual {v1, v0}, Lc/k;->ag(Z)V

    sget-boolean v1, Lcom/android/server/ssrm/N;->cW:Z

    if-nez v1, :cond_2

    sget-boolean v1, Lcom/android/server/ssrm/N;->df:Z

    if-eqz v1, :cond_3

    :cond_2
    sget-object v2, Lc/k;->TAG:Ljava/lang/String;

    const-string v3, "/sys/class/sec/sec_touchkey/glove_mode"

    if-eqz v0, :cond_4

    const-string v1, "1"

    :goto_2
    invoke-static {v2, v3, v1}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    sget-object v1, Lc/k;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enableGloveMode:: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    const-string v1, "0"

    goto :goto_2
.end method
