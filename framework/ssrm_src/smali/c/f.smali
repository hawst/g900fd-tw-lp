.class Lc/f;
.super Ljava/lang/Object;


# instance fields
.field private oO:Z

.field private uN:Z

.field private uO:Z

.field private uP:Z

.field private uQ:Z

.field private uR:Z

.field private uS:Z

.field private uT:Z

.field private uU:Z

.field private uV:Z

.field private uW:Z

.field private uX:Z

.field private uY:Z


# direct methods
.method constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lc/f;->uN:Z

    iput-boolean v0, p0, Lc/f;->oO:Z

    iput-boolean v0, p0, Lc/f;->uO:Z

    iput-boolean v0, p0, Lc/f;->uP:Z

    iput-boolean v0, p0, Lc/f;->uR:Z

    iput-boolean v0, p0, Lc/f;->uS:Z

    iput-boolean v0, p0, Lc/f;->uT:Z

    iput-boolean v0, p0, Lc/f;->uU:Z

    iput-boolean v0, p0, Lc/f;->uV:Z

    iput-boolean v0, p0, Lc/f;->uW:Z

    iput-boolean v0, p0, Lc/f;->uX:Z

    iput-boolean v0, p0, Lc/f;->uY:Z

    return-void
.end method


# virtual methods
.method T(Z)V
    .locals 0

    iput-boolean p1, p0, Lc/f;->uN:Z

    return-void
.end method

.method U(Z)V
    .locals 0

    iput-boolean p1, p0, Lc/f;->oO:Z

    return-void
.end method

.method V(Z)V
    .locals 0

    iput-boolean p1, p0, Lc/f;->uO:Z

    return-void
.end method

.method W(Z)V
    .locals 0

    iput-boolean p1, p0, Lc/f;->uP:Z

    return-void
.end method

.method X(Z)V
    .locals 0

    iput-boolean p1, p0, Lc/f;->uQ:Z

    return-void
.end method

.method Y(Z)V
    .locals 0

    iput-boolean p1, p0, Lc/f;->uR:Z

    return-void
.end method

.method Z(Z)V
    .locals 0

    iput-boolean p1, p0, Lc/f;->uS:Z

    return-void
.end method

.method aa(Z)V
    .locals 0

    iput-boolean p1, p0, Lc/f;->uT:Z

    return-void
.end method

.method ab(Z)V
    .locals 0

    iput-boolean p1, p0, Lc/f;->uU:Z

    return-void
.end method

.method ac(Z)V
    .locals 0

    iput-boolean p1, p0, Lc/f;->uV:Z

    return-void
.end method

.method ad(Z)V
    .locals 0

    iput-boolean p1, p0, Lc/f;->uW:Z

    return-void
.end method

.method ae(Z)V
    .locals 0

    iput-boolean p1, p0, Lc/f;->uX:Z

    return-void
.end method

.method af(Z)V
    .locals 0

    iput-boolean p1, p0, Lc/f;->uY:Z

    return-void
.end method

.method dA()Z
    .locals 1

    iget-boolean v0, p0, Lc/f;->uP:Z

    return v0
.end method

.method dB()Z
    .locals 1

    iget-boolean v0, p0, Lc/f;->uQ:Z

    return v0
.end method

.method dC()Z
    .locals 1

    iget-boolean v0, p0, Lc/f;->uR:Z

    return v0
.end method

.method dD()Z
    .locals 1

    iget-boolean v0, p0, Lc/f;->uS:Z

    return v0
.end method

.method dE()Z
    .locals 1

    iget-boolean v0, p0, Lc/f;->uT:Z

    return v0
.end method

.method dF()Z
    .locals 1

    iget-boolean v0, p0, Lc/f;->uU:Z

    return v0
.end method

.method dG()Z
    .locals 1

    iget-boolean v0, p0, Lc/f;->uV:Z

    return v0
.end method

.method dH()Z
    .locals 1

    iget-boolean v0, p0, Lc/f;->uW:Z

    return v0
.end method

.method dI()Z
    .locals 1

    iget-boolean v0, p0, Lc/f;->uX:Z

    return v0
.end method

.method dJ()Z
    .locals 1

    iget-boolean v0, p0, Lc/f;->uY:Z

    return v0
.end method

.method dK()V
    .locals 3

    sget-object v1, Lc/a;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DeviceInfo:: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/f;->uN:Z

    if-eqz v0, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/f;->oO:Z

    if-eqz v0, :cond_1

    const-string v0, "1"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/f;->uO:Z

    if-eqz v0, :cond_2

    const-string v0, "1"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/f;->uP:Z

    if-eqz v0, :cond_3

    const-string v0, "1"

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/f;->uR:Z

    if-eqz v0, :cond_4

    const-string v0, "1"

    :goto_4
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/f;->uS:Z

    if-eqz v0, :cond_5

    const-string v0, "1"

    :goto_5
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/f;->uT:Z

    if-eqz v0, :cond_6

    const-string v0, "1"

    :goto_6
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/f;->uU:Z

    if-eqz v0, :cond_7

    const-string v0, "1"

    :goto_7
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/f;->uV:Z

    if-eqz v0, :cond_8

    const-string v0, "1"

    :goto_8
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/f;->uW:Z

    if-eqz v0, :cond_9

    const-string v0, "1"

    :goto_9
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/f;->uX:Z

    if-eqz v0, :cond_a

    const-string v0, "1"

    :goto_a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/f;->uY:Z

    if-eqz v0, :cond_b

    const-string v0, "1"

    :goto_b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "0"

    goto :goto_0

    :cond_1
    const-string v0, "0"

    goto :goto_1

    :cond_2
    const-string v0, "0"

    goto :goto_2

    :cond_3
    const-string v0, "0"

    goto :goto_3

    :cond_4
    const-string v0, "0"

    goto :goto_4

    :cond_5
    const-string v0, "0"

    goto :goto_5

    :cond_6
    const-string v0, "0"

    goto :goto_6

    :cond_7
    const-string v0, "0"

    goto :goto_7

    :cond_8
    const-string v0, "0"

    goto :goto_8

    :cond_9
    const-string v0, "0"

    goto :goto_9

    :cond_a
    const-string v0, "0"

    goto :goto_a

    :cond_b
    const-string v0, "0"

    goto :goto_b
.end method

.method dz()Z
    .locals 1

    iget-boolean v0, p0, Lc/f;->uN:Z

    return v0
.end method

.method isMultiWindow()Z
    .locals 1

    iget-boolean v0, p0, Lc/f;->oO:Z

    return v0
.end method

.method isStatusBarVisible()Z
    .locals 1

    iget-boolean v0, p0, Lc/f;->uO:Z

    return v0
.end method
