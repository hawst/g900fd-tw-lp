.class Lc/g;
.super Ljava/lang/Object;


# instance fields
.field mResolver:Landroid/content/ContentResolver;

.field final synthetic uI:Lc/a;

.field private uZ:Z

.field private va:Z

.field private vb:I

.field private vc:Z

.field private vd:Z

.field private ve:Z

.field private vf:Z

.field private vg:Z

.field private vh:Z

.field private vi:Z

.field vj:Lc/e;

.field vk:Landroid/content/Context;


# direct methods
.method constructor <init>(Lc/a;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lc/g;->uI:Lc/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p1, Lc/a;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    invoke-virtual {p0}, Lc/g;->dL()V

    invoke-virtual {p0}, Lc/g;->dM()V

    return-void
.end method


# virtual methods
.method dK()V
    .locals 3

    sget-object v1, Lc/a;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SettingsAirViewInfo:: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/g;->uZ:Z

    if-eqz v0, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/g;->va:Z

    if-eqz v0, :cond_1

    const-string v0, "1"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lc/g;->vb:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/g;->vc:Z

    if-eqz v0, :cond_2

    const-string v0, "1"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/g;->vd:Z

    if-eqz v0, :cond_3

    const-string v0, "1"

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/g;->ve:Z

    if-eqz v0, :cond_4

    const-string v0, "1"

    :goto_4
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/g;->vf:Z

    if-eqz v0, :cond_5

    const-string v0, "1"

    :goto_5
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/g;->vg:Z

    if-eqz v0, :cond_6

    const-string v0, "1"

    :goto_6
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v0, p0, Lc/g;->vh:Z

    if-eqz v0, :cond_7

    const-string v0, "1"

    :goto_7
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const-string v0, "0"

    goto :goto_0

    :cond_1
    const-string v0, "0"

    goto :goto_1

    :cond_2
    const-string v0, "0"

    goto :goto_2

    :cond_3
    const-string v0, "0"

    goto :goto_3

    :cond_4
    const-string v0, "0"

    goto :goto_4

    :cond_5
    const-string v0, "0"

    goto :goto_5

    :cond_6
    const-string v0, "0"

    goto :goto_6

    :cond_7
    const-string v0, "0"

    goto :goto_7
.end method

.method dL()V
    .locals 5

    const/4 v4, -0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "finger_air_view"

    invoke-static {v0, v3, v2, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lc/g;->uZ:Z

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "air_view_master_onoff"

    invoke-static {v0, v3, v2, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lc/g;->va:Z

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "air_view_mode"

    invoke-static {v0, v3, v2, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lc/g;->vb:I

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "finger_air_view_magnifier"

    invoke-static {v0, v3, v2, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lc/g;->vc:Z

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "any_screen_enabled"

    invoke-static {v0, v3, v2, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lc/g;->vd:Z

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "onehand_dialer_enabled"

    invoke-static {v0, v3, v2, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lc/g;->ve:Z

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "onehand_samsungkeypad_enabled"

    invoke-static {v0, v3, v2, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lc/g;->vf:Z

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "onehand_calculator_enabled"

    invoke-static {v0, v3, v2, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lc/g;->vg:Z

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "onehand_pattern_enabled"

    invoke-static {v0, v3, v2, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lc/g;->vh:Z

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v3, "accessibility_magnifier"

    invoke-static {v0, v3, v2, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-ne v0, v1, :cond_8

    :goto_8
    iput-boolean v1, p0, Lc/g;->vi:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v0, v2

    goto :goto_5

    :cond_6
    move v0, v2

    goto :goto_6

    :cond_7
    move v0, v2

    goto :goto_7

    :cond_8
    move v1, v2

    goto :goto_8
.end method

.method dM()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, -0x2

    new-instance v0, Lc/e;

    iget-object v1, p0, Lc/g;->uI:Lc/a;

    invoke-direct {v0, v1}, Lc/e;-><init>(Lc/a;)V

    iput-object v0, p0, Lc/g;->vj:Lc/e;

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "finger_air_view"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lc/g;->vj:Lc/e;

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "air_view_master_onoff"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lc/g;->vj:Lc/e;

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "air_view_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lc/g;->vj:Lc/e;

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "finger_air_view_magnifier"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lc/g;->vj:Lc/e;

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "any_screen_enabled"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lc/g;->vj:Lc/e;

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "onehand_dialer_enabled"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lc/g;->vj:Lc/e;

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "onehand_samsungkeypad_enabled"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lc/g;->vj:Lc/e;

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "onehand_calculator_enabled"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lc/g;->vj:Lc/e;

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "onehand_pattern_enabled"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lc/g;->vj:Lc/e;

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "accessibility_magnifier"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lc/g;->vj:Lc/e;

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    return-void
.end method

.method dN()V
    .locals 2

    iget-object v0, p0, Lc/g;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lc/g;->vj:Lc/e;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public dO()Z
    .locals 1

    iget-boolean v0, p0, Lc/g;->uZ:Z

    return v0
.end method

.method public dP()Z
    .locals 1

    iget-boolean v0, p0, Lc/g;->va:Z

    return v0
.end method

.method public dQ()I
    .locals 1

    iget v0, p0, Lc/g;->vb:I

    return v0
.end method

.method public dR()Z
    .locals 1

    iget-boolean v0, p0, Lc/g;->vc:Z

    return v0
.end method

.method public dS()Z
    .locals 1

    iget-boolean v0, p0, Lc/g;->vd:Z

    return v0
.end method

.method public dT()Z
    .locals 1

    iget-boolean v0, p0, Lc/g;->ve:Z

    return v0
.end method

.method public dU()Z
    .locals 1

    iget-boolean v0, p0, Lc/g;->vf:Z

    return v0
.end method

.method public dV()Z
    .locals 1

    iget-boolean v0, p0, Lc/g;->vg:Z

    return v0
.end method

.method public dW()Z
    .locals 1

    iget-boolean v0, p0, Lc/g;->vh:Z

    return v0
.end method

.method public dX()Z
    .locals 1

    iget-boolean v0, p0, Lc/g;->vi:Z

    return v0
.end method
