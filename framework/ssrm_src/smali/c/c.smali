.class Lc/c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic uI:Lc/a;

.field final synthetic uJ:Ljava/lang/String;

.field final synthetic uK:I


# direct methods
.method constructor <init>(Lc/a;Ljava/lang/String;I)V
    .locals 0

    iput-object p1, p0, Lc/c;->uI:Lc/a;

    iput-object p2, p0, Lc/c;->uJ:Ljava/lang/String;

    iput p3, p0, Lc/c;->uK:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lc/c;->uI:Lc/a;

    iget-boolean v0, v0, Lc/a;->uB:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lc/c;->uI:Lc/a;

    iput-boolean v5, v0, Lc/a;->ur:Z

    iget-object v0, p0, Lc/c;->uI:Lc/a;

    invoke-virtual {v0}, Lc/a;->dp()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0xa

    :goto_1
    if-lez v0, :cond_0

    const-wide/16 v2, 0x32

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    sget-object v1, Lc/a;->TAG:Ljava/lang/String;

    iget-object v2, p0, Lc/c;->uJ:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lc/a;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFrameRateUpdate:: Current FPS trim = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lc/a;->q(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lc/c;->uI:Lc/a;

    iget v3, p0, Lc/c;->uK:I

    invoke-virtual {v2, v1, v3}, Lc/a;->k(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lc/c;->uI:Lc/a;

    iput-boolean v5, v0, Lc/a;->ur:Z

    iget-object v0, p0, Lc/c;->uI:Lc/a;

    iget v1, p0, Lc/c;->uK:I

    invoke-virtual {v0, v1}, Lc/a;->af(I)V

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lc/a;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFrameRateUpdate:: exception msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v1}, Lc/a;->access$000(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method
