.class public abstract Lc/a;
.super Lb/p;

# interfaces
.implements Lb/t;


# static fields
.field static final DEBUG:Z

.field static final TAG:Ljava/lang/String;

.field private static up:Lc/a; = null

.field public static final uw:I = 0x0

.field public static final ux:I = 0x1

.field public static final uy:I = 0x2


# instance fields
.field mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field mUserId:I

.field oN:Ljava/lang/String;

.field uA:Ljava/util/HashMap;

.field uB:Z

.field uC:Ljava/util/HashMap;

.field final uD:Ljava/lang/String;

.field final uE:Ljava/lang/String;

.field uF:Z

.field uG:Lc/f;

.field uH:Lc/g;

.field final uk:I

.field final ul:I

.field final um:I

.field final un:I

.field final uo:I

.field uq:I

.field ur:Z

.field us:I

.field ut:I

.field uu:I

.field uv:Z

.field final uz:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lc/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lc/a;->DEBUG:Z

    const/4 v0, 0x0

    sput-object v0, Lc/a;->up:Lc/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    iput v0, p0, Lc/a;->uk:I

    iput v1, p0, Lc/a;->ul:I

    iput v0, p0, Lc/a;->um:I

    const/4 v0, 0x2

    iput v0, p0, Lc/a;->un:I

    const/4 v0, 0x3

    iput v0, p0, Lc/a;->uo:I

    iput v1, p0, Lc/a;->mUserId:I

    iput-object v2, p0, Lc/a;->oN:Ljava/lang/String;

    iput-object v2, p0, Lc/a;->mContext:Landroid/content/Context;

    iput v1, p0, Lc/a;->uq:I

    iput-boolean v1, p0, Lc/a;->ur:Z

    iput v1, p0, Lc/a;->us:I

    iput v1, p0, Lc/a;->ut:I

    iput v1, p0, Lc/a;->uu:I

    iput-boolean v1, p0, Lc/a;->uv:Z

    const-string v0, "com.sec.android.airview.HOVER"

    iput-object v0, p0, Lc/a;->uz:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    iput-boolean v1, p0, Lc/a;->uB:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lc/a;->uC:Ljava/util/HashMap;

    const-string v0, "/sys/class/sec/tsp/cmd"

    iput-object v0, p0, Lc/a;->uD:Ljava/lang/String;

    const-string v0, "/sys/class/sec/tsp/cmd_result"

    iput-object v0, p0, Lc/a;->uE:Ljava/lang/String;

    iput-boolean v1, p0, Lc/a;->uF:Z

    new-instance v0, Lc/f;

    invoke-direct {v0}, Lc/f;-><init>()V

    iput-object v0, p0, Lc/a;->uG:Lc/f;

    iput-object v2, p0, Lc/a;->uH:Lc/g;

    invoke-virtual {p0}, Lc/a;->dw()V

    invoke-virtual {p0}, Lc/a;->du()V

    invoke-virtual {p0}, Lc/a;->dv()V

    new-instance v0, Lc/b;

    invoke-static {}, Lcom/android/server/ssrm/ag;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lc/b;-><init>(Lc/a;Landroid/os/Looper;)V

    iput-object v0, p0, Lc/a;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static declared-synchronized do()Lc/a;
    .locals 3

    const-class v1, Lc/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lc/a;->up:Lc/a;

    if-nez v0, :cond_0

    invoke-static {}, Lc/a;->dx()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lc/a;->dy()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v2, "getInstance:: AirViewOnOffForDual"

    invoke-static {v0, v2}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lc/j;

    invoke-direct {v0}, Lc/j;-><init>()V

    sput-object v0, Lc/a;->up:Lc/a;

    :cond_0
    :goto_0
    sget-object v0, Lc/a;->up:Lc/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :cond_1
    :try_start_1
    invoke-static {}, Lc/a;->dy()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v2, "getInstance:: AirViewOnOff"

    invoke-static {v0, v2}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lc/h;

    invoke-direct {v0}, Lc/h;-><init>()V

    sput-object v0, Lc/a;->up:Lc/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_2
    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v2, "getInstance:: AirViewOnOffDummy"

    invoke-static {v0, v2}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lc/i;

    invoke-direct {v0}, Lc/i;-><init>()V

    sput-object v0, Lc/a;->up:Lc/a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static dx()Z
    .locals 2

    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/class/sec/sec_epen"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method public static dy()Z
    .locals 1

    const-string v0, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_FINGER_AIR_VIEW"

    invoke-static {v0}, Lcom/android/server/ssrm/aT;->O(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static p(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    invoke-static {v0, p1, p0}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic q(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public R(Z)V
    .locals 1

    iget-object v0, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v0, p1}, Lc/f;->ae(Z)V

    invoke-virtual {p0}, Lc/a;->dp()V

    return-void
.end method

.method public S(Z)V
    .locals 1

    iget-object v0, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v0, p1}, Lc/f;->af(Z)V

    invoke-virtual {p0}, Lc/a;->dp()V

    return-void
.end method

.method protected abstract a(Lc/f;Lc/g;)I
.end method

.method public a(Landroid/content/Context;Landroid/content/Intent;Lb/p;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-super {p0, p1, p2, p3}, Lb/p;->a(Landroid/content/Context;Landroid/content/Intent;Lb/p;)V

    iget-boolean v2, p0, Lc/a;->uF:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "ResponseAxT9Info"

    const-string v2, "AxT9IME.isVisibleWindow"

    iput-object p1, p0, Lc/a;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.android.systemui.statusbar.EXPANDED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v0, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v0, v1}, Lc/f;->V(Z)V

    invoke-virtual {p0}, Lc/a;->dp()V

    goto :goto_0

    :cond_2
    const-string v3, "com.android.systemui.statusbar.COLLAPSED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v1, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v1, v0}, Lc/f;->V(Z)V

    invoke-virtual {p0}, Lc/a;->dp()V

    goto :goto_0

    :cond_3
    const-string v3, "com.samsung.cover.OPEN"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v2, p0, Lc/a;->uG:Lc/f;

    const-string v3, "coverOpen"

    invoke-virtual {p2, v3, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_4

    move v0, v1

    :cond_4
    invoke-virtual {v2, v0}, Lc/f;->T(Z)V

    invoke-virtual {p0}, Lc/a;->dp()V

    goto :goto_0

    :cond_5
    const-string v3, "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v1, p0, Lc/a;->uG:Lc/f;

    const-string v2, "com.sec.android.extra.MULTIWINDOW_RUNNING"

    invoke-virtual {p2, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v1, v0}, Lc/f;->U(Z)V

    invoke-virtual {p0}, Lc/a;->dp()V

    goto :goto_0

    :cond_6
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    new-instance v0, Lc/g;

    invoke-direct {v0, p0, p1}, Lc/g;-><init>(Lc/a;Landroid/content/Context;)V

    iput-object v0, p0, Lc/a;->uH:Lc/g;

    const-string v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v0, v1}, Lc/f;->W(Z)V

    invoke-virtual {p0}, Lc/a;->dp()V

    :cond_7
    invoke-virtual {p0, p1}, Lc/a;->f(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_8
    const-string v3, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {p0, p1, p2}, Lc/a;->c(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_9
    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    iput v0, p0, Lc/a;->ut:I

    iput v0, p0, Lc/a;->uu:I

    goto/16 :goto_0

    :cond_a
    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v2, "onReceive:: Intent.ACTION_SCREEN_ON"

    invoke-static {v0, v2}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    if-eqz v0, :cond_c

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v0, v1}, Lc/f;->W(Z)V

    iget v0, p0, Lc/a;->uq:I

    if-ne v0, v1, :cond_b

    iput-boolean v1, p0, Lc/a;->ur:Z

    :cond_b
    invoke-virtual {p0}, Lc/a;->dp()V

    goto/16 :goto_0

    :cond_c
    iget-object v0, p0, Lc/a;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lc/a;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lc/a;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x320

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    :cond_d
    const-string v3, "android.intent.action.USER_PRESENT"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    sget-object v1, Lc/a;->TAG:Ljava/lang/String;

    const-string v2, "onReceive:: Intent.ACTION_USER_PRESENT"

    invoke-static {v1, v2}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v1, v0}, Lc/f;->W(Z)V

    iget-object v1, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v1, v0}, Lc/f;->ad(Z)V

    invoke-virtual {p0}, Lc/a;->dp()V

    goto/16 :goto_0

    :cond_e
    const-string v3, "ResponseAxT9Info"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    iget-object v1, p0, Lc/a;->uG:Lc/f;

    const-string v2, "AxT9IME.isVisibleWindow"

    invoke-virtual {p2, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v1, v0}, Lc/f;->X(Z)V

    invoke-virtual {p0}, Lc/a;->dp()V

    goto/16 :goto_0

    :cond_f
    sget-object v3, Lc/a;->iu:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    sget-object v3, Lc/a;->iw:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    :cond_10
    sget-object v1, Lc/a;->ix:Ljava/lang/String;

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lc/a;->iy:Ljava/lang/String;

    invoke-virtual {p2, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v2, "PackageName"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "Browser_showMain"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    iget-object v1, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v1, v0}, Lc/f;->Z(Z)V

    :cond_11
    :goto_1
    invoke-virtual {p0}, Lc/a;->dp()V

    goto/16 :goto_0

    :cond_12
    const-string v3, "Phone_dialer"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    sget-object v1, Lc/a;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Phone_dialer = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", packageName = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "com.android.contacts"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    iget-object v1, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v1, v0}, Lc/f;->ab(Z)V

    goto :goto_1

    :cond_13
    iget-object v1, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v1, v0}, Lc/f;->ac(Z)V

    goto :goto_1

    :cond_14
    const-string v2, "Lockscreen_patternLock"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    sget-object v1, Lc/a;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Lockscreen_patternLock = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v1, v0}, Lc/f;->ad(Z)V

    goto :goto_1

    :cond_15
    const-string v3, "com.samsung.pen.INSERT"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    const-string v1, "penInsert"

    invoke-virtual {p2, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v1, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v1, v0}, Lc/f;->aa(Z)V

    invoke-virtual {p0}, Lc/a;->dp()V

    goto/16 :goto_0

    :cond_16
    const-string v3, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lc/a;->TAG:Ljava/lang/String;

    const-string v3, "ACTION_USER_SWITCHED"

    invoke-static {v2, v3}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "android.intent.extra.user_handle"

    invoke-virtual {p2, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lc/a;->mUserId:I

    iget-object v0, p0, Lc/a;->uH:Lc/g;

    invoke-virtual {v0}, Lc/g;->dN()V

    new-instance v0, Lc/g;

    iget-object v2, p0, Lc/a;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v2}, Lc/g;-><init>(Lc/a;Landroid/content/Context;)V

    iput-object v0, p0, Lc/a;->uH:Lc/g;

    iput-boolean v1, p0, Lc/a;->ur:Z

    invoke-virtual {p0}, Lc/a;->dp()V

    goto/16 :goto_0
.end method

.method af(I)V
    .locals 0

    iput p1, p0, Lc/a;->us:I

    invoke-virtual {p0}, Lc/a;->dp()V

    return-void
.end method

.method declared-synchronized ag(I)V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lc/a;->dq()V

    invoke-virtual {p0}, Lc/a;->ds()V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lc/a;->dr()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    :try_start_1
    invoke-virtual {p0}, Lc/a;->dt()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    :try_start_2
    invoke-virtual {p0}, Lc/a;->dr()V

    invoke-virtual {p0}, Lc/a;->dt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public ah(I)V
    .locals 2

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    iget-object v0, p0, Lc/a;->uG:Lc/f;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lc/f;->Y(Z)V

    invoke-virtual {p0}, Lc/a;->dp()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v0, v1}, Lc/f;->Y(Z)V

    invoke-virtual {p0}, Lc/a;->dp()V

    goto :goto_0
.end method

.method protected c(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    const/16 v7, 0x200

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v1, v0, v1

    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACTION_PACKAGE_ADDED :: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_4

    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v2, "AirView Tag Scan Start (Install)"

    invoke-static {v0, v2}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.sec.android.airview.HOVER"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    sget-object v3, Lc/a;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AirView added ris count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    sget-object v4, Lc/a;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AirView added package intent for :: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, Lc/a;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AirView added HashMap size Before Adding : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lc/a;->uA:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lc/a;->uA:Ljava/util/HashMap;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AirView added HashMap size after Adding : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lc/a;->uA:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1
    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v3, "AirView Tag Scan End (Install)"

    invoke-static {v0, v3}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v3, "AirView Provider Scan Start (Install)"

    invoke-static {v0, v3}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/N;->cz:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_3

    new-instance v0, Landroid/content/Intent;

    sget-object v3, Lcom/android/server/ssrm/N;->cz:Ljava/lang/String;

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0, v7}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    sget-object v3, Lc/a;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AirView added provider package intent for :: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v3, Lc/a;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AirView added provider HashMap size Before Adding : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lc/a;->uA:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lc/a;->uA:Ljava/util/HashMap;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AirView added provider HashMap size after Adding : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lc/a;->uA:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_3
    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v1, "AirView Provider Scan End (Install)"

    invoke-static {v0, v1}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    return-void
.end method

.method public cb()V
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lc/a;->bH()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lc/a;->oN:Ljava/lang/String;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    iget-object v2, p0, Lc/a;->oN:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-boolean v2, p0, Lc/a;->uv:Z

    if-eq v0, v2, :cond_0

    iput-boolean v0, p0, Lc/a;->uv:Z

    invoke-virtual {p0}, Lc/a;->dp()V

    :cond_0
    const-string v0, "com.sec.android.app.launcher"

    iget-object v2, p0, Lc/a;->oN:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v0, v1}, Lc/f;->ac(Z)V

    iget-object v0, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v0, v1}, Lc/f;->ab(Z)V

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public d(ILjava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lc/c;

    invoke-direct {v1, p0, p2, p1}, Lc/c;-><init>(Lc/a;Ljava/lang/String;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method dp()V
    .locals 2

    iget-object v0, p0, Lc/a;->uH:Lc/g;

    if-nez v0, :cond_1

    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v1, "updateAirViewStatus:: mSettingsAirView or mSettingsAirView is null."

    invoke-static {v0, v1}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lc/a;->uG:Lc/f;

    invoke-virtual {v0}, Lc/f;->dK()V

    iget-object v0, p0, Lc/a;->uH:Lc/g;

    invoke-virtual {v0}, Lc/g;->dK()V

    iget-object v0, p0, Lc/a;->uG:Lc/f;

    iget-object v1, p0, Lc/a;->uH:Lc/g;

    invoke-virtual {p0, v0, v1}, Lc/a;->a(Lc/f;Lc/g;)I

    move-result v0

    iget v1, p0, Lc/a;->uq:I

    if-ne v1, v0, :cond_2

    iget-boolean v1, p0, Lc/a;->ur:Z

    if-eqz v1, :cond_0

    :cond_2
    invoke-virtual {p0, v0}, Lc/a;->ag(I)V

    iput v0, p0, Lc/a;->uq:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lc/a;->ur:Z

    goto :goto_0
.end method

.method dq()V
    .locals 2

    iget v0, p0, Lc/a;->ut:I

    if-eqz v0, :cond_0

    const-string v0, "hover_enable,0"

    const-string v1, "/sys/class/sec/tsp/cmd"

    invoke-static {v0, v1}, Lc/a;->p(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v1, "/sys/class/sec/tsp/cmd_result"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lc/a;->ut:I

    :cond_0
    return-void
.end method

.method dr()V
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Lc/a;->ut:I

    if-eq v0, v2, :cond_0

    const-string v0, "hover_enable,1"

    const-string v1, "/sys/class/sec/tsp/cmd"

    invoke-static {v0, v1}, Lc/a;->p(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v1, "/sys/class/sec/tsp/cmd_result"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    iput v2, p0, Lc/a;->ut:I

    :cond_0
    return-void
.end method

.method ds()V
    .locals 2

    iget v0, p0, Lc/a;->uu:I

    if-eqz v0, :cond_0

    const-string v0, "handgrip_enable,0"

    const-string v1, "/sys/class/sec/tsp/cmd"

    invoke-static {v0, v1}, Lc/a;->p(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v1, "/sys/class/sec/tsp/cmd_result"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lc/a;->uu:I

    :cond_0
    return-void
.end method

.method dt()V
    .locals 3

    const/4 v2, 0x1

    iget v0, p0, Lc/a;->uu:I

    if-eq v0, v2, :cond_0

    const-string v0, "handgrip_enable,1"

    const-string v1, "/sys/class/sec/tsp/cmd"

    invoke-static {v0, v1}, Lc/a;->p(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v1, "/sys/class/sec/tsp/cmd_result"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    iput v2, p0, Lc/a;->uu:I

    :cond_0
    return-void
.end method

.method protected du()V
    .locals 3

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Contacts"

    const-string v2, "com.android.contacts"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Email"

    const-string v2, "com.android.email"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Gallery"

    const-string v2, "com.sec.android.gallery3d"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Messaging"

    const-string v2, "com.android.mms"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Music"

    const-string v2, "com.samsung.music"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Music Player"

    const-string v2, "com.sec.android.app.music"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "My files"

    const-string v2, "com.sec.android.app.myfiles"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "New Internet"

    const-string v2, "com.sec.android.app.sbrowser"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "S Memo"

    const-string v2, "com.sec.android.widgetapp.diotek.smemo"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "S-Planner"

    const-string v2, "com.android.calendar"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Story ablum"

    const-string v2, "com.samsung.android.app.episodes"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Video"

    const-string v2, "com.samsung.everglades.video"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Video (hidden)"

    const-string v2, "com.sec.android.app.videoplayer"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Allshare play"

    const-string v2, "com.sec.pcw"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Clock Package"

    const-string v2, "com.sec.android.app.clockpackage"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Group play"

    const-string v2, "com.samsung.groupcast"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Kids mode"

    const-string v2, "com.samsung.kidsmode"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Kies cast"

    const-string v2, "com.sec.android.app.podcast"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Prism store"

    const-string v2, "com.sec.everglades"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "S health"

    const-string v2, "com.sec.android.app.shealth"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Video editor"

    const-string v2, "com.sec.android.app.ve"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Voice"

    const-string v2, "com.vlingo.midas"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Voice recorder"

    const-string v2, "com.sec.android.app.voicerecorder"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "color blind"

    const-string v2, "com.samsung.android.app.colorblind"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Remote view finder"

    const-string v2, "com.sec.android.app.remoteviewfinder"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "SamsungApps"

    const-string v2, "com.sec.android.app.samsungapps"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "sbrowsertry"

    const-string v2, "com.sec.android.app.sbrowsertry"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Music Live Share"

    const-string v2, "com.sec.android.app.mediasync"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Books"

    const-string v2, "com.sec.readershub"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Learning"

    const-string v2, "com.sec.msc.learninghub"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Music 3.1"

    const-string v2, "com.samsung.musichub"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "Games"

    const-string v2, "com.sec.android.app.gamehub"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uA:Ljava/util/HashMap;

    const-string v1, "WatchOn"

    const-string v2, "com.sec.watchon.phone"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method dv()V
    .locals 5

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v1, "updating lookup hash"

    invoke-static {v0, v1}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v0, Lcom/android/server/ssrm/N;->dk:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lb/i;->nN:Lb/j;

    if-eqz v0, :cond_0

    const-string v0, "/sys/class/lcd/panel/fps_change"

    const-string v0, "/sys/class/graphics/fb0/lcdfreq/level"

    sget-object v0, Lb/i;->nN:Lb/j;

    invoke-virtual {v0}, Lb/j;->co()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/sys/class/graphics/fb0/lcdfreq/level"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lc/a;->uC:Ljava/util/HashMap;

    const-string v1, "60hz, div=3"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uC:Ljava/util/HashMap;

    const-string v1, "40hz, div=5"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uC:Ljava/util/HashMap;

    const-string v1, "48hz, div=8"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-boolean v3, p0, Lc/a;->uB:Z

    goto :goto_0

    :cond_2
    const-string v1, "/sys/class/lcd/panel/fps_change"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lc/a;->uC:Ljava/util/HashMap;

    const-string v1, "60"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uC:Ljava/util/HashMap;

    const-string v1, "42"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lc/a;->uC:Ljava/util/HashMap;

    const-string v1, "51"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-boolean v3, p0, Lc/a;->uB:Z

    goto :goto_0
.end method

.method dw()V
    .locals 2

    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/class/sec/tsp/cmd"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lc/a;->uF:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected f(Landroid/content/Context;)V
    .locals 4

    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lc/d;

    invoke-direct {v3, p0, v1, v0}, Lc/d;-><init>(Lc/a;Landroid/content/pm/PackageManager;Landroid/app/ActivityManager;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method k(Ljava/lang/String;I)Z
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lc/a;->uC:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lc/a;->TAG:Ljava/lang/String;

    const-string v2, "FPS found in look up"

    invoke-static {v0, v2}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lc/a;->uC:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v2, Lc/a;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FPS found in look up sysfps="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lc/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
