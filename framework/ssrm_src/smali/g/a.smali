.class public Lg/a;
.super Ljava/lang/Object;


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field final packageName:Ljava/lang/String;

.field private final zx:Ljava/lang/String;

.field zy:Lg/b;

.field zz:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lg/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lg/a;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "com.sec.android.app.wluc"

    iput-object v0, p0, Lg/a;->packageName:Ljava/lang/String;

    const-string v0, "/data/system/wluc.db"

    iput-object v0, p0, Lg/a;->zx:Ljava/lang/String;

    iput-object p1, p0, Lg/a;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lg/a;->mHandler:Landroid/os/Handler;

    invoke-virtual {p0}, Lg/a;->fc()V

    return-void
.end method

.method protected static logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Message;)V
    .locals 5

    const/16 v3, 0x63

    const/4 v2, 0x5

    if-nez p1, :cond_0

    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    const-string v1, "onMsgReceivedFromWlucThread : received msg is null!!"

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, 0x5a

    if-ne v0, v1, :cond_2

    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    const-string v1, "NO_MATCHING_APPLICATION"

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lg/a;->aL(I)V

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lg/a;->fe()V

    goto :goto_0

    :cond_2
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, 0x5b

    if-ne v0, v1, :cond_3

    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    const-string v1, "MATCHING_BUT_NO_UPDATE"

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lg/a;->aL(I)V

    goto :goto_1

    :cond_3
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v3, :cond_4

    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    const-string v1, "CHECK_UPDATE_AVAIBLE"

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lg/c;

    iget-object v1, p0, Lg/a;->mContext:Landroid/content/Context;

    const/16 v2, 0x60

    const-string v3, "com.sec.android.app.wluc"

    iget-object v4, p0, Lg/a;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3, v4}, Lg/c;-><init>(Landroid/content/Context;ILjava/lang/String;Landroid/os/Handler;)V

    invoke-virtual {v0}, Lg/c;->start()V

    goto :goto_1

    :cond_4
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, 0x5c

    if-eq v0, v1, :cond_5

    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, 0x5d

    if-ne v0, v1, :cond_6

    :cond_5
    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    const-string v1, "MATCHING_UPDATABLE_NORMAL/MATCHING_UPDATABLE_CRITICAL"

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    new-instance v1, Lg/c;

    iget-object v2, p0, Lg/a;->mContext:Landroid/content/Context;

    const/16 v3, 0x61

    iget-object v4, p0, Lg/a;->mHandler:Landroid/os/Handler;

    invoke-direct {v1, v2, v3, v0, v4}, Lg/c;-><init>(Landroid/content/Context;ILjava/lang/String;Landroid/os/Handler;)V

    invoke-virtual {v1}, Lg/c;->start()V

    goto :goto_1

    :cond_6
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, 0x5e

    if-ne v0, v1, :cond_7

    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    const-string v1, "DOWNLOAD_FAILED"

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, 0x5f

    if-ne v0, v1, :cond_8

    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    const-string v1, "INSTALL_FAILED"

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_8
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, 0x59

    if-ne v0, v1, :cond_9

    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    const-string v1, "INSTALL_SUCCESS"

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lg/a;->aL(I)V

    goto/16 :goto_1

    :cond_9
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, 0x62

    if-ne v0, v1, :cond_b

    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    const-string v1, "CHECK_UPDATE_SCHEDULE"

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lg/a;->ff()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lg/a;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lg/a;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x6

    iput v1, v0, Landroid/os/Message;->what:I

    iput v3, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lg/a;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    :cond_a
    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    const-string v1, "CHECK_UPDATE_SCHEDULE : not yet!!!"

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_b
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, 0x58

    if-ne v0, v1, :cond_1

    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    const-string v1, "UPDATE_CHECK_FAIL"

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public aL(I)V
    .locals 8

    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateWlucDatabase : after "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", updateCheck will be available"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const v3, 0x5265c00

    mul-int/2addr v3, p1

    int-to-long v6, v3

    add-long/2addr v4, v6

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lg/a;->zy:Lg/b;

    invoke-virtual {v1, v0}, Lg/b;->e(Ljava/util/ArrayList;)V

    return-void
.end method

.method public fc()V
    .locals 2

    iget-object v0, p0, Lg/a;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lg/b;->g(Landroid/content/Context;)Lg/b;

    move-result-object v0

    iput-object v0, p0, Lg/a;->zy:Lg/b;

    new-instance v0, Ljava/io/File;

    const-string v1, "/data/system/wluc.db"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lg/a;->fd()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lg/a;->fe()V

    goto :goto_0
.end method

.method public fd()V
    .locals 6

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x19bfcc00

    add-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a;->zz:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lg/a;->zz:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lg/a;->zy:Lg/b;

    invoke-virtual {v1, v0}, Lg/b;->e(Ljava/util/ArrayList;)V

    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "createWlucDatabase : wlucNextUpdateCheck : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lg/a;->zz:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public fe()V
    .locals 5

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lg/a;->zy:Lg/b;

    invoke-virtual {v0}, Lg/b;->fg()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v2, Lg/a;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "readWlucDatabase : array - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lg/a;->zz:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/File;

    const-string v2, "/data/system/wluc.db"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_0
    invoke-virtual {p0}, Lg/a;->fd()V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :goto_1
    return-void

    :cond_1
    :try_start_1
    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "readWlucDatabase : wlucNextUpdateCheck : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lg/a;->zz:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public ff()Z
    .locals 4

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lg/a;->zz:Ljava/lang/String;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lg/a;->zz:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    sget-object v1, Lg/a;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isNeedToUpdateCheck :e = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    const-string v1, "isNeedToUpdateCheck : wrong wlucNextUpdateCheck"

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m(Landroid/content/Intent;)V
    .locals 5

    :try_start_0
    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    const-string v1, "download check for package : com.sec.android.app.wluc"

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lg/c;

    iget-object v1, p0, Lg/a;->mContext:Landroid/content/Context;

    const/16 v2, 0x60

    const-string v3, "com.sec.android.app.wluc"

    iget-object v4, p0, Lg/a;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3, v4}, Lg/c;-><init>(Landroid/content/Context;ILjava/lang/String;Landroid/os/Handler;)V

    invoke-virtual {v0}, Lg/c;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public n(Landroid/content/Intent;)V
    .locals 5

    :try_start_0
    sget-object v0, Lg/a;->TAG:Ljava/lang/String;

    const-string v1, "check update schedule for package : com.sec.android.app.wluc"

    invoke-static {v0, v1}, Lg/a;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lg/c;

    iget-object v1, p0, Lg/a;->mContext:Landroid/content/Context;

    const/16 v2, 0x62

    const-string v3, "com.sec.android.app.wluc"

    iget-object v4, p0, Lg/a;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2, v3, v4}, Lg/c;-><init>(Landroid/content/Context;ILjava/lang/String;Landroid/os/Handler;)V

    invoke-virtual {v0}, Lg/c;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
