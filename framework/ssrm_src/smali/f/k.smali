.class Lf/k;
.super Ljava/lang/Object;


# static fields
.field static final TAG:Ljava/lang/String;

.field private static final xS:I = 0x12c


# instance fields
.field private xF:I

.field private xT:I

.field private xU:I

.field private xV:[I

.field private xW:I

.field private xX:I

.field private xY:I

.field private xZ:I

.field private ya:Z

.field private yb:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lf/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lf/k;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x1ea

    iput v0, p0, Lf/k;->xT:I

    const/16 v0, 0x1e

    iput v0, p0, Lf/k;->xU:I

    const/16 v0, 0xa

    iput v0, p0, Lf/k;->xF:I

    iget v0, p0, Lf/k;->xF:I

    new-array v0, v0, [I

    iput-object v0, p0, Lf/k;->xV:[I

    iput v1, p0, Lf/k;->xW:I

    iget v0, p0, Lf/k;->xF:I

    iput v0, p0, Lf/k;->xX:I

    iput v1, p0, Lf/k;->xY:I

    iput v1, p0, Lf/k;->xZ:I

    iput-boolean v1, p0, Lf/k;->ya:Z

    iput-boolean v1, p0, Lf/k;->yb:Z

    new-instance v0, Ljava/io/File;

    const-string v1, "/data/system/ssrm_v2.pst_init"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lf/k;->eG()V

    :cond_0
    return-void
.end method


# virtual methods
.method public aw(I)I
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x0

    if-lez p1, :cond_0

    const/16 v1, 0x3e8

    if-lt p1, v1, :cond_1

    :cond_0
    const/16 v0, 0x12c

    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lf/k;->ya:Z

    if-nez v1, :cond_5

    iget-boolean v1, p0, Lf/k;->yb:Z

    if-nez v1, :cond_3

    iget v1, p0, Lf/k;->xF:I

    new-array v1, v1, [I

    iput-object v1, p0, Lf/k;->xV:[I

    :goto_1
    iget v1, p0, Lf/k;->xF:I

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lf/k;->xV:[I

    aput p1, v1, v0

    iget v1, p0, Lf/k;->xY:I

    iget-object v2, p0, Lf/k;->xV:[I

    aget v2, v2, v0

    add-int/2addr v1, v2

    iput v1, p0, Lf/k;->xY:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget v0, p0, Lf/k;->xF:I

    iput v0, p0, Lf/k;->xX:I

    iput-boolean v3, p0, Lf/k;->yb:Z

    :cond_3
    iget v0, p0, Lf/k;->xY:I

    iget-object v1, p0, Lf/k;->xV:[I

    iget v2, p0, Lf/k;->xW:I

    aget v1, v1, v2

    sub-int/2addr v0, v1

    iput v0, p0, Lf/k;->xY:I

    iget-object v0, p0, Lf/k;->xV:[I

    iget v1, p0, Lf/k;->xW:I

    aput p1, v0, v1

    iget v0, p0, Lf/k;->xY:I

    add-int/2addr v0, p1

    iput v0, p0, Lf/k;->xY:I

    iget v0, p0, Lf/k;->xW:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lf/k;->xW:I

    iget v0, p0, Lf/k;->xW:I

    iget v1, p0, Lf/k;->xX:I

    if-ne v0, v1, :cond_4

    iput-boolean v3, p0, Lf/k;->ya:Z

    :cond_4
    iget v0, p0, Lf/k;->xY:I

    iget v1, p0, Lf/k;->xX:I

    div-int/2addr v0, v1

    goto :goto_0

    :cond_5
    iget v1, p0, Lf/k;->xW:I

    iget v2, p0, Lf/k;->xX:I

    if-ne v1, v2, :cond_6

    iput v0, p0, Lf/k;->xW:I

    :cond_6
    iget-object v0, p0, Lf/k;->xV:[I

    iget v1, p0, Lf/k;->xW:I

    aget v0, v0, v1

    iget-object v1, p0, Lf/k;->xV:[I

    iget v2, p0, Lf/k;->xW:I

    aput p1, v1, v2

    iget v1, p0, Lf/k;->xY:I

    sub-int v0, v1, v0

    iput v0, p0, Lf/k;->xY:I

    iget v0, p0, Lf/k;->xY:I

    add-int/2addr v0, p1

    iput v0, p0, Lf/k;->xY:I

    iget v0, p0, Lf/k;->xW:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lf/k;->xW:I

    iget v0, p0, Lf/k;->xY:I

    iget v1, p0, Lf/k;->xX:I

    div-int/2addr v0, v1

    iput v0, p0, Lf/k;->xZ:I

    iget v0, p0, Lf/k;->xZ:I

    invoke-virtual {p0, v0}, Lf/k;->ax(I)V

    iget v0, p0, Lf/k;->xZ:I

    goto/16 :goto_0
.end method

.method public ax(I)V
    .locals 7

    const/4 v2, 0x0

    iget v0, p0, Lf/k;->xT:I

    if-lt p1, v0, :cond_3

    iget v0, p0, Lf/k;->xX:I

    iget v1, p0, Lf/k;->xU:I

    if-eq v0, v1, :cond_2

    iget v0, p0, Lf/k;->xU:I

    iput v0, p0, Lf/k;->xX:I

    iget v0, p0, Lf/k;->xF:I

    new-array v0, v0, [I

    iget-object v0, p0, Lf/k;->xV:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput v2, p0, Lf/k;->xY:I

    iget v1, p0, Lf/k;->xU:I

    new-array v1, v1, [I

    iput-object v1, p0, Lf/k;->xV:[I

    move v1, v2

    :goto_0
    iget v3, p0, Lf/k;->xU:I

    if-ge v1, v3, :cond_1

    iget v3, p0, Lf/k;->xF:I

    if-ge v1, v3, :cond_0

    iget-object v3, p0, Lf/k;->xV:[I

    aget v4, v0, v1

    aput v4, v3, v1

    :goto_1
    iget v3, p0, Lf/k;->xY:I

    iget-object v4, p0, Lf/k;->xV:[I

    aget v4, v4, v1

    add-int/2addr v3, v4

    iput v3, p0, Lf/k;->xY:I

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lf/k;->xV:[I

    aput p1, v3, v1

    goto :goto_1

    :cond_1
    iput v2, p0, Lf/k;->xW:I

    :cond_2
    :goto_2
    return-void

    :cond_3
    iget v0, p0, Lf/k;->xX:I

    iget v1, p0, Lf/k;->xF:I

    if-eq v0, v1, :cond_2

    iget v0, p0, Lf/k;->xF:I

    iput v0, p0, Lf/k;->xX:I

    iget v0, p0, Lf/k;->xU:I

    new-array v0, v0, [I

    iget-object v0, p0, Lf/k;->xV:[I

    invoke-virtual {v0}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput v2, p0, Lf/k;->xY:I

    iget v1, p0, Lf/k;->xF:I

    new-array v1, v1, [I

    iput-object v1, p0, Lf/k;->xV:[I

    iget v1, p0, Lf/k;->xW:I

    iget v3, p0, Lf/k;->xF:I

    sub-int v3, v1, v3

    move v1, v2

    :goto_3
    iget v4, p0, Lf/k;->xF:I

    if-ge v1, v4, :cond_4

    iget-object v4, p0, Lf/k;->xV:[I

    iget v5, p0, Lf/k;->xU:I

    add-int/2addr v5, v3

    add-int/2addr v5, v1

    iget v6, p0, Lf/k;->xU:I

    rem-int/2addr v5, v6

    aget v5, v0, v5

    aput v5, v4, v1

    iget v4, p0, Lf/k;->xY:I

    iget-object v5, p0, Lf/k;->xV:[I

    aget v5, v5, v1

    add-int/2addr v4, v5

    iput v4, p0, Lf/k;->xY:I

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    iput v2, p0, Lf/k;->xW:I

    goto :goto_2
.end method

.method eG()V
    .locals 4

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    const-string v3, "/data/system/ssrm_v2.pst_init"

    invoke-direct {v0, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v2, "long_range_temp"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lf/k;->xT:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_1

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_1
    :goto_2
    sget-object v0, Lf/k;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INIT Value change :: LONG_RANGE_TEMP = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/k;->xT:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , LONG_WINDOW_SIZE = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/k;->xU:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " , DEFAULT_WINDOW_SIZE = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/k;->xF:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_2
    :try_start_4
    const-string v2, "long_window_size"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lf/k;->xU:I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_3

    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_3
    :goto_4
    throw v0

    :cond_4
    :try_start_6
    const-string v2, "default_window_size"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lf/k;->xF:I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    :cond_5
    if-eqz v1, :cond_1

    :try_start_7
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catch_4
    move-exception v0

    move-object v1, v2

    goto/16 :goto_1
.end method

.method reset()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/k;->ya:Z

    iput-boolean v0, p0, Lf/k;->yb:Z

    iput v0, p0, Lf/k;->xY:I

    iput v0, p0, Lf/k;->xW:I

    return-void
.end method
