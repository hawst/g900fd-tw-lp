.class public abstract Lf/c;
.super Ljava/lang/Object;


# instance fields
.field mName:Ljava/lang/String;

.field xi:Ljava/lang/String;

.field xj:Z

.field xk:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lf/c;->xj:Z

    iput-boolean v0, p0, Lf/c;->xk:Z

    return-void
.end method


# virtual methods
.method abstract a(Ljava/util/HashMap;)V
.end method

.method ai(Z)V
    .locals 0

    iput-boolean p1, p0, Lf/c;->xj:Z

    return-void
.end method

.method aj(Z)V
    .locals 0

    iput-boolean p1, p0, Lf/c;->xk:Z

    return-void
.end method

.method abstract ao(I)V
.end method

.method ao(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/c;->mName:Ljava/lang/String;

    return-void
.end method

.method ap(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lf/c;->xi:Ljava/lang/String;

    return-void
.end method

.method eA()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/c;->mName:Ljava/lang/String;

    return-object v0
.end method

.method eB()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lf/c;->xi:Ljava/lang/String;

    return-object v0
.end method

.method eC()Z
    .locals 1

    iget-boolean v0, p0, Lf/c;->xj:Z

    return v0
.end method

.method abstract ex()V
.end method

.method abstract ey()V
.end method

.method abstract ez()I
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lf/c;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/c;->xi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
