.class public Lf/d;
.super Lf/c;


# instance fields
.field final TAG:Ljava/lang/String;

.field xl:Ljava/util/HashMap;

.field xm:Z

.field xn:Z

.field xo:I

.field xp:I

.field xq:Lf/q;


# direct methods
.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lf/c;-><init>()V

    const-class v0, Lf/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/d;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lf/d;->xl:Ljava/util/HashMap;

    iput-boolean v1, p0, Lf/d;->xm:Z

    iput-boolean v1, p0, Lf/d;->xn:Z

    const/4 v0, -0x1

    iput v0, p0, Lf/d;->xo:I

    const/16 v0, -0x3e7

    iput v0, p0, Lf/d;->xp:I

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lf/c;-><init>()V

    const-class v0, Lf/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/d;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lf/d;->xl:Ljava/util/HashMap;

    iput-boolean v1, p0, Lf/d;->xm:Z

    iput-boolean v1, p0, Lf/d;->xn:Z

    const/4 v0, -0x1

    iput v0, p0, Lf/d;->xo:I

    const/16 v0, -0x3e7

    iput v0, p0, Lf/d;->xp:I

    invoke-virtual {p0, p1}, Lf/d;->ao(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lf/d;->ap(Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lf/c;-><init>()V

    const-class v0, Lf/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/d;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lf/d;->xl:Ljava/util/HashMap;

    iput-boolean v1, p0, Lf/d;->xm:Z

    iput-boolean v1, p0, Lf/d;->xn:Z

    const/4 v0, -0x1

    iput v0, p0, Lf/d;->xo:I

    const/16 v0, -0x3e7

    iput v0, p0, Lf/d;->xp:I

    invoke-virtual {p0, p1}, Lf/d;->ao(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lf/d;->ap(Ljava/lang/String;)V

    invoke-virtual {p0, p3}, Lf/d;->a(Ljava/util/HashMap;)V

    return-void
.end method


# virtual methods
.method a(Ljava/util/HashMap;)V
    .locals 2

    new-instance v0, Lf/q;

    invoke-direct {v0}, Lf/q;-><init>()V

    iput-object v0, p0, Lf/d;->xq:Lf/q;

    const-string v0, "temp"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lf/d;->xo:I

    const-string v0, "0"

    const-string v1, "value"

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/d;->xm:Z

    :goto_0
    const-string v0, "low_temp_attr"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "low_temp_attr"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lf/d;->xp:I

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/d;->xm:Z

    goto :goto_0
.end method

.method ao(I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lf/d;->xj:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lf/d;->xp:I

    if-gt p1, v0, :cond_2

    iget-boolean v0, p0, Lf/d;->xm:Z

    if-eqz v0, :cond_1

    iput-boolean v2, p0, Lf/d;->xn:Z

    :goto_0
    invoke-virtual {p0}, Lf/d;->ex()V

    :cond_0
    return-void

    :cond_1
    iput-boolean v1, p0, Lf/d;->xn:Z

    goto :goto_0

    :cond_2
    iget v0, p0, Lf/d;->xo:I

    if-ge p1, v0, :cond_3

    invoke-virtual {p0, p1}, Lf/d;->ap(I)V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lf/d;->xm:Z

    if-eqz v0, :cond_4

    iput-boolean v2, p0, Lf/d;->xn:Z

    goto :goto_0

    :cond_4
    iput-boolean v1, p0, Lf/d;->xn:Z

    goto :goto_0
.end method

.method ap(I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lf/d;->xm:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lf/d;->xn:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lf/d;->xo:I

    add-int/lit8 v0, v0, -0x14

    if-ge p1, v0, :cond_1

    iput-boolean v1, p0, Lf/d;->xn:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lf/d;->xn:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lf/d;->xn:Z

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lf/d;->xn:Z

    if-nez v0, :cond_3

    iget v0, p0, Lf/d;->xo:I

    add-int/lit8 v0, v0, -0x14

    if-ge p1, v0, :cond_3

    iput-boolean v2, p0, Lf/d;->xn:Z

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lf/d;->xn:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lf/d;->xn:Z

    goto :goto_0
.end method

.method ex()V
    .locals 3

    iget-boolean v0, p0, Lf/d;->xj:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/d;->xq:Lf/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/d;->xq:Lf/q;

    iget-object v1, p0, Lf/d;->mName:Ljava/lang/String;

    iget-boolean v2, p0, Lf/d;->xn:Z

    invoke-virtual {v0, v1, v2}, Lf/q;->c(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method ey()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/d;->xj:Z

    return-void
.end method

.method ez()I
    .locals 1

    iget-boolean v0, p0, Lf/d;->xj:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lf/d;->xn:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/16 v0, -0x3e7

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lf/d;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/d;->xi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "temp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lf/d;->xo:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lf/d;->xm:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
