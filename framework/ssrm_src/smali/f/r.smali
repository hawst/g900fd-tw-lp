.class Lf/r;
.super Ljava/lang/Object;


# instance fields
.field DEBUG:Z

.field TAG:Ljava/lang/String;

.field ig:Lcom/android/server/ssrm/X;

.field mContext:Landroid/content/Context;

.field mDuration:I

.field mName:Ljava/lang/String;

.field final yR:I

.field yS:I

.field yT:I

.field yU:I

.field yV:I

.field yW:I

.field yX:I

.field yY:I

.field yZ:I

.field za:I

.field zb:Ljava/lang/String;

.field zc:Landroid/os/DVFSHelper;

.field zd:[I

.field ze:I

.field zf:I

.field zg:I

.field zh:I

.field zi:I

.field zj:[I

.field zk:Z

.field zl:Z

.field zm:Z

.field zn:I

.field private zo:J

.field private zp:J

.field zq:Z

.field zr:I

.field zs:I

.field zt:Z

.field zu:Z


# direct methods
.method constructor <init>(Ljava/lang/String;[I)V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/16 v4, -0x3e7

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lf/r;->DEBUG:Z

    const-class v0, Lf/r;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/r;->TAG:Ljava/lang/String;

    const/16 v0, 0xa

    iput v0, p0, Lf/r;->yR:I

    const/16 v0, 0x46

    iput v0, p0, Lf/r;->yS:I

    iput v3, p0, Lf/r;->yT:I

    iput v4, p0, Lf/r;->yU:I

    iput v4, p0, Lf/r;->yV:I

    iput v3, p0, Lf/r;->yW:I

    iput v3, p0, Lf/r;->yX:I

    iput v3, p0, Lf/r;->yY:I

    iput v3, p0, Lf/r;->mDuration:I

    iput v3, p0, Lf/r;->yZ:I

    iput v3, p0, Lf/r;->za:I

    const/4 v0, 0x0

    iput-object v0, p0, Lf/r;->zb:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lf/r;->zc:Landroid/os/DVFSHelper;

    iput v4, p0, Lf/r;->ze:I

    iput v4, p0, Lf/r;->zf:I

    iput v4, p0, Lf/r;->zg:I

    iput v3, p0, Lf/r;->zh:I

    iput v5, p0, Lf/r;->zi:I

    const/4 v0, 0x7

    new-array v0, v0, [I

    iput-object v0, p0, Lf/r;->zj:[I

    iput-boolean v2, p0, Lf/r;->zk:Z

    iput-boolean v2, p0, Lf/r;->zl:Z

    iput-boolean v2, p0, Lf/r;->zm:Z

    iput v2, p0, Lf/r;->zn:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lf/r;->zo:J

    iget-wide v0, p0, Lf/r;->zo:J

    iput-wide v0, p0, Lf/r;->zp:J

    iput-boolean v2, p0, Lf/r;->zq:Z

    iput v2, p0, Lf/r;->zr:I

    iput v3, p0, Lf/r;->zs:I

    iput-boolean v2, p0, Lf/r;->zt:Z

    iput-boolean v2, p0, Lf/r;->zu:Z

    invoke-static {}, Lf/m;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lf/r;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lf/r;->mName:Ljava/lang/String;

    const/4 v0, 0x2

    aget v0, p2, v0

    iput v0, p0, Lf/r;->yX:I

    aget v0, p2, v6

    iput v0, p0, Lf/r;->yY:I

    aget v0, p2, v2

    if-ne v0, v4, :cond_2

    iget v0, p0, Lf/r;->yY:I

    add-int/lit8 v0, v0, 0xa

    iput v0, p0, Lf/r;->yW:I

    iput-boolean v2, p0, Lf/r;->zk:Z

    :goto_0
    aget v0, p2, v5

    if-eq v0, v4, :cond_0

    aget v0, p2, v5

    iput v0, p0, Lf/r;->yS:I

    :cond_0
    const/4 v0, 0x4

    aget v0, p2, v0

    if-ne v0, v4, :cond_5

    const-string v0, "CPUFreqMax"

    iget-object v1, p0, Lf/r;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x2328

    iput v0, p0, Lf/r;->mDuration:I

    :cond_1
    :goto_1
    const/4 v0, 0x5

    aget v0, p2, v0

    if-ne v0, v4, :cond_6

    iput v3, p0, Lf/r;->yZ:I

    :goto_2
    const/4 v0, 0x6

    aget v0, p2, v0

    iput v0, p0, Lf/r;->za:I

    invoke-static {}, Lcom/android/server/ssrm/X;->aU()Lcom/android/server/ssrm/X;

    move-result-object v0

    iput-object v0, p0, Lf/r;->ig:Lcom/android/server/ssrm/X;

    return-void

    :cond_2
    aget v0, p2, v2

    const/16 v1, -0x270f

    if-ne v0, v1, :cond_3

    iput-boolean v5, p0, Lf/r;->zk:Z

    goto :goto_0

    :cond_3
    aget v0, p2, v2

    iput v0, p0, Lf/r;->yW:I

    iput-boolean v2, p0, Lf/r;->zk:Z

    goto :goto_0

    :cond_4
    const-string v0, "GPUFreqMax"

    iget-object v1, p0, Lf/r;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x7148

    iput v0, p0, Lf/r;->mDuration:I

    goto :goto_1

    :cond_5
    aget v0, p2, v6

    add-int/lit16 v0, v0, -0x3e8

    iput v0, p0, Lf/r;->mDuration:I

    goto :goto_1

    :cond_6
    const/4 v0, 0x5

    aget v0, p2, v0

    iput v0, p0, Lf/r;->yZ:I

    goto :goto_2
.end method


# virtual methods
.method aC(I)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v3, -0x3e7

    iget-object v0, p0, Lf/r;->zd:[I

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lf/r;->eU()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lf/r;->ze:I

    if-ne v0, v3, :cond_2

    iput p1, p0, Lf/r;->ze:I

    iput v4, p0, Lf/r;->zn:I

    goto :goto_0

    :cond_2
    iget-wide v0, p0, Lf/r;->zp:J

    iput-wide v0, p0, Lf/r;->zo:J

    iget v0, p0, Lf/r;->zn:I

    iget v1, p0, Lf/r;->mDuration:I

    if-ge v0, v1, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lf/r;->zp:J

    iget v0, p0, Lf/r;->zn:I

    int-to-long v0, v0

    iget-wide v2, p0, Lf/r;->zp:J

    iget-wide v4, p0, Lf/r;->zo:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lf/r;->zn:I

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lf/r;->zu:Z

    if-nez v0, :cond_4

    iget v0, p0, Lf/r;->zn:I

    iget v1, p0, Lf/r;->mDuration:I

    if-ne v0, v1, :cond_5

    :cond_4
    iput v4, p0, Lf/r;->zn:I

    iget v0, p0, Lf/r;->zs:I

    invoke-virtual {p0, v0}, Lf/r;->aD(I)V

    goto :goto_0

    :cond_5
    iput v4, p0, Lf/r;->zn:I

    iget v0, p0, Lf/r;->ze:I

    sub-int v0, p1, v0

    iput v0, p0, Lf/r;->zf:I

    iput p1, p0, Lf/r;->ze:I

    iget-boolean v0, p0, Lf/r;->zt:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lf/r;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startSts:: Diff = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->zf:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lf/r;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] maxFreq = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->zs:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    iget v0, p0, Lf/r;->yX:I

    if-lt p1, v0, :cond_c

    iget-boolean v0, p0, Lf/r;->zl:Z

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lf/r;->eY()V

    :cond_7
    iput-boolean v5, p0, Lf/r;->zl:Z

    iput-boolean v4, p0, Lf/r;->zm:Z

    iget v0, p0, Lf/r;->za:I

    iput v0, p0, Lf/r;->yV:I

    invoke-virtual {p0}, Lf/r;->eW()V

    :cond_8
    :goto_1
    iget-boolean v0, p0, Lf/r;->zk:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lf/r;->zl:Z

    if-nez v0, :cond_a

    iget v0, p0, Lf/r;->yW:I

    if-lt p1, v0, :cond_f

    iget-boolean v0, p0, Lf/r;->zm:Z

    if-nez v0, :cond_9

    invoke-virtual {p0}, Lf/r;->eY()V

    :cond_9
    iput-boolean v5, p0, Lf/r;->zm:Z

    iget v0, p0, Lf/r;->yT:I

    iput v0, p0, Lf/r;->yV:I

    invoke-virtual {p0}, Lf/r;->eW()V

    :cond_a
    :goto_2
    iget v0, p0, Lf/r;->zf:I

    iput v0, p0, Lf/r;->zg:I

    iget-boolean v0, p0, Lf/r;->DEBUG:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lf/r;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setStsStatus:: mLowBound = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->yV:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mHighBound = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->yU:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    iget-boolean v0, p0, Lf/r;->DEBUG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/r;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setStsStatus:: mPreMonitorTemperature = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->yW:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mTriggerTemperature = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->yX:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mReleaseTemperature = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->yY:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mMaxLimit = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->yZ:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mMinLimit = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->za:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    iget-boolean v0, p0, Lf/r;->zl:Z

    if-eqz v0, :cond_8

    iget v0, p0, Lf/r;->yY:I

    if-ge p1, v0, :cond_d

    iput v3, p0, Lf/r;->yV:I

    invoke-virtual {p0}, Lf/r;->eX()V

    goto/16 :goto_1

    :cond_d
    iget-boolean v0, p0, Lf/r;->zk:Z

    if-nez v0, :cond_8

    iget v0, p0, Lf/r;->yW:I

    if-lt p1, v0, :cond_e

    iget v0, p0, Lf/r;->za:I

    iput v0, p0, Lf/r;->yV:I

    invoke-virtual {p0}, Lf/r;->eW()V

    goto/16 :goto_1

    :cond_e
    iput v3, p0, Lf/r;->yV:I

    invoke-virtual {p0}, Lf/r;->eX()V

    goto/16 :goto_1

    :cond_f
    iget-boolean v0, p0, Lf/r;->zm:Z

    if-eqz v0, :cond_a

    iput v3, p0, Lf/r;->yV:I

    invoke-virtual {p0}, Lf/r;->eX()V

    goto/16 :goto_2
.end method

.method aD(I)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, -0x1

    iget-boolean v0, p0, Lf/r;->zu:Z

    if-nez v0, :cond_0

    iget v0, p0, Lf/r;->zs:I

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lf/r;->zt:Z

    if-eqz v0, :cond_1

    iput p1, p0, Lf/r;->zs:I

    goto :goto_0

    :cond_1
    iget v0, p0, Lf/r;->zh:I

    invoke-virtual {p0}, Lf/r;->fa()I

    move-result v1

    if-le v0, v1, :cond_2

    invoke-virtual {p0}, Lf/r;->fa()I

    move-result v0

    if-eq v0, v2, :cond_2

    invoke-virtual {p0}, Lf/r;->fa()I

    move-result v0

    iput v0, p0, Lf/r;->zh:I

    iget-object v0, p0, Lf/r;->zd:[I

    iget v1, p0, Lf/r;->zh:I

    aget p1, v0, v1

    :cond_2
    const-string v0, "CPUFreqMax"

    iget-object v1, p0, Lf/r;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lf/r;->zd:[I

    aget v0, v0, v3

    if-ne v0, p1, :cond_4

    iget-object v0, p0, Lf/r;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, v2}, Lcom/android/server/ssrm/X;->y(I)V

    :cond_3
    :goto_1
    iput p1, p0, Lf/r;->zs:I

    iget-object v0, p0, Lf/r;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SSFrequencyControl:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lf/r;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lf/r;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/X;->y(I)V

    goto :goto_1

    :cond_5
    const-string v0, "GPUFreqMax"

    iget-object v1, p0, Lf/r;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lf/r;->zd:[I

    aget v0, v0, v3

    if-ne v0, p1, :cond_6

    iget-object v0, p0, Lf/r;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, v2}, Lcom/android/server/ssrm/X;->F(I)V

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lf/r;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/X;->F(I)V

    goto :goto_1
.end method

.method al(Z)V
    .locals 1

    iget-boolean v0, p0, Lf/r;->zt:Z

    if-eq v0, p1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/r;->zu:Z

    iget v0, p0, Lf/r;->mDuration:I

    iput v0, p0, Lf/r;->zn:I

    :goto_0
    iput-boolean p1, p0, Lf/r;->zt:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/r;->zu:Z

    goto :goto_0
.end method

.method c([I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    aget v0, p1, v2

    iput v0, p0, Lf/r;->yW:I

    iget v0, p0, Lf/r;->yW:I

    const/16 v1, -0x270f

    if-ne v0, v1, :cond_0

    iput-boolean v3, p0, Lf/r;->zk:Z

    :goto_0
    aget v0, p1, v3

    iput v0, p0, Lf/r;->yX:I

    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Lf/r;->yY:I

    const/4 v0, 0x3

    aget v0, p1, v0

    iput v0, p0, Lf/r;->mDuration:I

    const/4 v0, 0x4

    aget v0, p1, v0

    iput v0, p0, Lf/r;->yZ:I

    const/4 v0, 0x5

    aget v0, p1, v0

    iput v0, p0, Lf/r;->za:I

    const/4 v0, 0x6

    aget v0, p1, v0

    iput v0, p0, Lf/r;->yT:I

    iget v0, p0, Lf/r;->yZ:I

    iput v0, p0, Lf/r;->yU:I

    return-void

    :cond_0
    iput-boolean v2, p0, Lf/r;->zk:Z

    goto :goto_0
.end method

.method eU()V
    .locals 8

    const-wide/16 v4, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x0

    const-string v0, "CPUFreqMax"

    iget-object v1, p0, Lf/r;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/os/DVFSHelper;

    iget-object v1, p0, Lf/r;->mContext:Landroid/content/Context;

    const-string v2, "STS_CPU_MAX"

    const/16 v3, 0xd

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lf/r;->zc:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lf/r;->zc:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUFrequencyForSSRM()[I

    move-result-object v0

    iput-object v0, p0, Lf/r;->zd:[I

    const-string v0, "/sys/power/cpufreq_max_limit"

    iput-object v0, p0, Lf/r;->zb:Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v0, p0, Lf/r;->zd:[I

    if-nez v0, :cond_4

    :goto_1
    return-void

    :cond_1
    const-string v0, "GPUFreqMax"

    iget-object v1, p0, Lf/r;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/DVFSHelper;

    iget-object v1, p0, Lf/r;->mContext:Landroid/content/Context;

    const-string v2, "STS_GPU_MAX"

    const/16 v3, 0x11

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lf/r;->zc:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lf/r;->zc:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedGPUFrequencyForSSRM()[I

    move-result-object v0

    iput-object v0, p0, Lf/r;->zd:[I

    sget-boolean v0, Lcom/android/server/ssrm/N;->ep:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/server/ssrm/QcFalImpl;->av()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/r;->zb:Ljava/lang/String;

    goto :goto_0

    :cond_2
    sget-boolean v0, Lcom/android/server/ssrm/N;->dV:Z

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/android/server/ssrm/MarvellSysfsFALImpl;->av()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/r;->zb:Ljava/lang/String;

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/android/server/ssrm/GenericFalImpl;->av()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/r;->zb:Ljava/lang/String;

    goto :goto_0

    :cond_4
    iget v0, p0, Lf/r;->yZ:I

    if-ne v0, v7, :cond_5

    iget-object v0, p0, Lf/r;->zd:[I

    aget v0, v0, v6

    iput v0, p0, Lf/r;->yZ:I

    :cond_5
    iget v0, p0, Lf/r;->za:I

    if-ne v0, v7, :cond_6

    iget-object v0, p0, Lf/r;->zd:[I

    iget-object v1, p0, Lf/r;->zd:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    iput v0, p0, Lf/r;->za:I

    :cond_6
    iget v0, p0, Lf/r;->yZ:I

    iput v0, p0, Lf/r;->yU:I

    iget-object v0, p0, Lf/r;->zd:[I

    aget v0, v0, v6

    div-int/lit8 v0, v0, 0x64

    iget v1, p0, Lf/r;->yS:I

    mul-int/2addr v1, v0

    move v0, v6

    :goto_2
    iget-object v2, p0, Lf/r;->zd:[I

    array-length v2, v2

    if-ge v0, v2, :cond_9

    iget-object v2, p0, Lf/r;->zd:[I

    aget v2, v2, v0

    if-gt v2, v1, :cond_8

    add-int/lit8 v1, v0, -0x1

    if-gez v1, :cond_7

    iget-object v0, p0, Lf/r;->zd:[I

    aget v0, v0, v6

    :goto_3
    iput v0, p0, Lf/r;->yT:I

    iget-object v0, p0, Lf/r;->zj:[I

    iget v1, p0, Lf/r;->yW:I

    aput v1, v0, v6

    iget-object v0, p0, Lf/r;->zj:[I

    const/4 v1, 0x1

    iget v2, p0, Lf/r;->yX:I

    aput v2, v0, v1

    iget-object v0, p0, Lf/r;->zj:[I

    const/4 v1, 0x2

    iget v2, p0, Lf/r;->yY:I

    aput v2, v0, v1

    iget-object v0, p0, Lf/r;->zj:[I

    const/4 v1, 0x3

    iget v2, p0, Lf/r;->mDuration:I

    aput v2, v0, v1

    iget-object v0, p0, Lf/r;->zj:[I

    const/4 v1, 0x4

    iget v2, p0, Lf/r;->yZ:I

    aput v2, v0, v1

    iget-object v0, p0, Lf/r;->zj:[I

    const/4 v1, 0x5

    iget v2, p0, Lf/r;->za:I

    aput v2, v0, v1

    iget-object v0, p0, Lf/r;->zj:[I

    const/4 v1, 0x6

    iget v2, p0, Lf/r;->yT:I

    aput v2, v0, v1

    goto/16 :goto_1

    :cond_7
    iget-object v1, p0, Lf/r;->zd:[I

    add-int/lit8 v0, v0, -0x1

    aget v0, v1, v0

    goto :goto_3

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_9
    move v0, v1

    goto :goto_3
.end method

.method eV()[I
    .locals 1

    iget-object v0, p0, Lf/r;->zj:[I

    return-object v0
.end method

.method eW()V
    .locals 4

    iget-object v0, p0, Lf/r;->zd:[I

    if-eqz v0, :cond_0

    iget v0, p0, Lf/r;->zh:I

    if-ltz v0, :cond_0

    iget-object v0, p0, Lf/r;->zd:[I

    iget v1, p0, Lf/r;->zh:I

    aget v0, v0, v1

    iget v1, p0, Lf/r;->yV:I

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/r;->zq:Z

    invoke-virtual {p0}, Lf/r;->eZ()V

    iget v0, p0, Lf/r;->zh:I

    iget v1, p0, Lf/r;->zi:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lf/r;->zd:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_3

    iget-object v0, p0, Lf/r;->zd:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lf/r;->zh:I

    :goto_1
    iget-boolean v0, p0, Lf/r;->DEBUG:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lf/r;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Act]["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lf/r;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] freq = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lf/r;->zd:[I

    iget v3, p0, Lf/r;->zh:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Min = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->yV:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Loc = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->zh:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Tune = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->zi:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", dur = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->mDuration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lf/r;->zd:[I

    iget v1, p0, Lf/r;->zh:I

    aget v0, v0, v1

    iget v1, p0, Lf/r;->yV:I

    if-le v0, v1, :cond_4

    iget-object v0, p0, Lf/r;->zd:[I

    iget v1, p0, Lf/r;->zh:I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lf/r;->aD(I)V

    goto/16 :goto_0

    :cond_3
    iget v0, p0, Lf/r;->zh:I

    iget v1, p0, Lf/r;->zi:I

    add-int/2addr v0, v1

    iput v0, p0, Lf/r;->zh:I

    goto :goto_1

    :cond_4
    iget v0, p0, Lf/r;->yV:I

    invoke-virtual {p0, v0}, Lf/r;->aD(I)V

    goto/16 :goto_0
.end method

.method eX()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lf/r;->zd:[I

    if-eqz v0, :cond_0

    iget v0, p0, Lf/r;->zh:I

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lf/r;->zd:[I

    iget v1, p0, Lf/r;->zh:I

    aget v0, v0, v1

    iget v1, p0, Lf/r;->yU:I

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lf/r;->zd:[I

    iget v1, p0, Lf/r;->zh:I

    aget v0, v0, v1

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    :cond_2
    iput-boolean v4, p0, Lf/r;->zl:Z

    :cond_3
    iput-boolean v4, p0, Lf/r;->zq:Z

    invoke-virtual {p0}, Lf/r;->eZ()V

    iget v0, p0, Lf/r;->zh:I

    iget v1, p0, Lf/r;->zi:I

    sub-int/2addr v0, v1

    if-gez v0, :cond_5

    iput v4, p0, Lf/r;->zh:I

    :goto_1
    iget-boolean v0, p0, Lf/r;->DEBUG:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lf/r;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Deact]["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lf/r;->mName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] freq = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lf/r;->zd:[I

    iget v3, p0, Lf/r;->zh:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Max = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->yU:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Loc = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->zh:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Tune = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->zi:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", dur = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->mDuration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v0, p0, Lf/r;->zd:[I

    iget v1, p0, Lf/r;->zh:I

    aget v0, v0, v1

    iget v1, p0, Lf/r;->yU:I

    if-ge v0, v1, :cond_6

    iget-object v0, p0, Lf/r;->zd:[I

    iget v1, p0, Lf/r;->zh:I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lf/r;->aD(I)V

    goto/16 :goto_0

    :cond_5
    iget v0, p0, Lf/r;->zh:I

    iget v1, p0, Lf/r;->zi:I

    sub-int/2addr v0, v1

    iput v0, p0, Lf/r;->zh:I

    goto :goto_1

    :cond_6
    iput-boolean v4, p0, Lf/r;->zl:Z

    iget v0, p0, Lf/r;->yU:I

    invoke-virtual {p0, v0}, Lf/r;->aD(I)V

    goto/16 :goto_0
.end method

.method eY()V
    .locals 4

    const/4 v3, -0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lf/r;->zb:Ljava/lang/String;

    if-nez v1, :cond_1

    iget-object v1, p0, Lf/r;->TAG:Ljava/lang/String;

    const-string v2, "getInitialValue: There is no SYSFS_PATH."

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iput v0, p0, Lf/r;->zh:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lf/r;->TAG:Ljava/lang/String;

    iget-object v2, p0, Lf/r;->zb:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lf/r;->zd:[I

    if-nez v2, :cond_2

    iget-object v0, p0, Lf/r;->TAG:Ljava/lang/String;

    const-string v1, "getInitialValue: There is no supported table."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    if-eq v1, v3, :cond_3

    div-int/lit8 v2, v1, 0xa

    if-eqz v2, :cond_7

    :cond_3
    if-ne v1, v3, :cond_6

    iput v0, p0, Lf/r;->zh:I

    :cond_4
    :goto_1
    iget-boolean v0, p0, Lf/r;->DEBUG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/r;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getInitialValue:: freq["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->zh:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lf/r;->zd:[I

    iget v3, p0, Lf/r;->zh:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    add-int/lit8 v0, v0, 0x1

    :cond_6
    iget-object v2, p0, Lf/r;->zd:[I

    array-length v2, v2

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lf/r;->zd:[I

    aget v2, v2, v0

    if-ne v1, v2, :cond_5

    iput v0, p0, Lf/r;->zh:I

    goto :goto_1

    :cond_7
    iput v1, p0, Lf/r;->zh:I

    goto :goto_1
.end method

.method eZ()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/16 v4, -0x12

    const/4 v3, 0x0

    iget-boolean v0, p0, Lf/r;->DEBUG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/r;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "controlOverallSts: mDeltaPstTemperature = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/r;->zf:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lf/r;->zg:I

    const/16 v1, -0x3e7

    if-ne v0, v1, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lf/r;->zq:Z

    if-eqz v0, :cond_5

    iput v5, p0, Lf/r;->zi:I

    iget v0, p0, Lf/r;->zf:I

    const/4 v1, 0x6

    if-le v0, v1, :cond_2

    iput v6, p0, Lf/r;->zi:I

    :cond_2
    iget v0, p0, Lf/r;->zf:I

    const/4 v1, -0x2

    if-ge v0, v1, :cond_3

    iput v3, p0, Lf/r;->zi:I

    :cond_3
    iget v0, p0, Lf/r;->zg:I

    if-gez v0, :cond_4

    iget v0, p0, Lf/r;->zf:I

    iget v1, p0, Lf/r;->zg:I

    if-ge v0, v1, :cond_4

    iput v3, p0, Lf/r;->zi:I

    :cond_4
    :goto_1
    iget v0, p0, Lf/r;->zf:I

    iput v0, p0, Lf/r;->zg:I

    goto :goto_0

    :cond_5
    iput v5, p0, Lf/r;->zi:I

    iget v0, p0, Lf/r;->zf:I

    if-gez v0, :cond_8

    iget v0, p0, Lf/r;->zg:I

    if-gez v0, :cond_8

    iget v0, p0, Lf/r;->zf:I

    iget v1, p0, Lf/r;->zg:I

    if-lt v0, v1, :cond_6

    iget v0, p0, Lf/r;->zg:I

    const/16 v1, -0xc

    if-ge v0, v1, :cond_7

    :cond_6
    iput v6, p0, Lf/r;->zi:I

    goto :goto_1

    :cond_7
    iget v0, p0, Lf/r;->zf:I

    if-ge v0, v4, :cond_4

    iget v0, p0, Lf/r;->zg:I

    if-ge v0, v4, :cond_4

    iput v7, p0, Lf/r;->zi:I

    goto :goto_1

    :cond_8
    iget v0, p0, Lf/r;->zg:I

    if-le v0, v7, :cond_4

    iget v0, p0, Lf/r;->zf:I

    iget v1, p0, Lf/r;->zg:I

    if-le v0, v1, :cond_4

    iput v3, p0, Lf/r;->zi:I

    goto :goto_1
.end method

.method ez()I
    .locals 4

    const/4 v1, -0x1

    iget v0, p0, Lf/r;->zs:I

    if-ne v0, v1, :cond_1

    :cond_0
    return v1

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lf/r;->zd:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lf/r;->zd:[I

    aget v2, v2, v0

    iget v3, p0, Lf/r;->zs:I

    if-ne v2, v3, :cond_2

    move v1, v0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method fa()I
    .locals 4

    const/4 v1, -0x1

    iget v0, p0, Lf/r;->za:I

    if-ne v0, v1, :cond_1

    :cond_0
    return v1

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lf/r;->zd:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lf/r;->zd:[I

    aget v2, v2, v0

    iget v3, p0, Lf/r;->za:I

    if-ne v2, v3, :cond_2

    move v1, v0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
