.class Lf/q;
.super Ljava/lang/Object;


# instance fields
.field DEBUG:Z

.field TAG:Ljava/lang/String;

.field ig:Lcom/android/server/ssrm/X;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/q;->DEBUG:Z

    const-class v0, Lf/q;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/q;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/android/server/ssrm/X;->aU()Lcom/android/server/ssrm/X;

    move-result-object v0

    iput-object v0, p0, Lf/q;->ig:Lcom/android/server/ssrm/X;

    return-void
.end method


# virtual methods
.method c(Ljava/lang/String;Z)V
    .locals 1

    const-string v0, "Flash"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p2}, Lf/q;->l(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "Recording"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p2}, Lf/q;->m(Z)V

    goto :goto_0

    :cond_2
    const-string v0, "SmartBonding"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, p2}, Lf/q;->n(Z)V

    goto :goto_0

    :cond_3
    const-string v0, "CameraDisable"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, p2}, Lf/q;->o(Z)V

    goto :goto_0

    :cond_4
    const-string v0, "CameraRecordingLowFps"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lf/q;->p(Z)V

    goto :goto_0
.end method

.method public l(Z)V
    .locals 1

    iget-object v0, p0, Lf/q;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/X;->l(Z)V

    iget-object v0, p0, Lf/q;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0}, Lcom/android/server/ssrm/X;->aW()V

    return-void
.end method

.method public m(Z)V
    .locals 1

    iget-object v0, p0, Lf/q;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/X;->m(Z)V

    iget-object v0, p0, Lf/q;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0}, Lcom/android/server/ssrm/X;->aW()V

    return-void
.end method

.method public n(Z)V
    .locals 1

    iget-object v0, p0, Lf/q;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/X;->n(Z)V

    iget-object v0, p0, Lf/q;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0}, Lcom/android/server/ssrm/X;->aW()V

    return-void
.end method

.method public o(Z)V
    .locals 1

    iget-object v0, p0, Lf/q;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/X;->o(Z)V

    iget-object v0, p0, Lf/q;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0}, Lcom/android/server/ssrm/X;->aW()V

    return-void
.end method

.method public p(Z)V
    .locals 1

    iget-object v0, p0, Lf/q;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/X;->p(Z)V

    iget-object v0, p0, Lf/q;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0}, Lcom/android/server/ssrm/X;->aW()V

    return-void
.end method
