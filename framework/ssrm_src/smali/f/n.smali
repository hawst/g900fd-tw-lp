.class Lf/n;
.super Landroid/os/Handler;


# instance fields
.field final synthetic yO:Lf/m;


# direct methods
.method constructor <init>(Lf/m;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lf/n;->yO:Lf/m;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    sget-boolean v0, Lcom/android/server/ssrm/ag;->jv:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lf/m;->yK:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    iget v1, p1, Landroid/os/Message;->arg1:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    iput v1, v0, Landroid/os/Message;->arg2:I

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    new-instance v1, Lf/p;

    invoke-direct {v1, v0}, Lf/p;-><init>(Landroid/os/Message;)V

    invoke-virtual {v1}, Lf/p;->start()V

    goto :goto_0
.end method
