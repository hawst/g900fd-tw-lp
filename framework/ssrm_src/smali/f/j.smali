.class Lf/j;
.super Ljava/lang/Object;


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field DEBUG:Z

.field final xF:I

.field final xG:I

.field final xH:I

.field final xI:I

.field xJ:I

.field xK:I

.field xL:Ljava/util/Queue;

.field xM:I

.field xN:I

.field xO:I

.field xP:I

.field xQ:I

.field xR:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lf/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lf/j;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 3

    const/16 v2, -0x3e7

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lf/j;->DEBUG:Z

    const/16 v0, 0xc

    iput v0, p0, Lf/j;->xF:I

    const/16 v0, 0xa

    iput v0, p0, Lf/j;->xG:I

    const/16 v0, 0x1e

    iput v0, p0, Lf/j;->xH:I

    const/4 v0, 0x2

    iput v0, p0, Lf/j;->xI:I

    iput v2, p0, Lf/j;->xJ:I

    iput v2, p0, Lf/j;->xK:I

    iput v2, p0, Lf/j;->xO:I

    iput v1, p0, Lf/j;->xP:I

    iput v1, p0, Lf/j;->xQ:I

    iput-boolean v1, p0, Lf/j;->xR:Z

    iput v1, p0, Lf/j;->xM:I

    iput v1, p0, Lf/j;->xN:I

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lf/j;->xL:Ljava/util/Queue;

    iput v1, p0, Lf/j;->xQ:I

    return-void
.end method


# virtual methods
.method aq(I)I
    .locals 3

    const/16 v0, -0xc8

    if-lt p1, v0, :cond_0

    const/16 v0, 0x3e8

    if-le p1, v0, :cond_1

    :cond_0
    iget v0, p0, Lf/j;->xO:I

    :goto_0
    return v0

    :cond_1
    iget-boolean v0, p0, Lf/j;->xR:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lf/j;->xL:Ljava/util/Queue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    iget v0, p0, Lf/j;->xQ:I

    add-int/2addr v0, p1

    iput v0, p0, Lf/j;->xQ:I

    iget v0, p0, Lf/j;->xM:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lf/j;->xM:I

    iget v0, p0, Lf/j;->xM:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lf/j;->xR:Z

    :cond_2
    iget v0, p0, Lf/j;->xQ:I

    iget v1, p0, Lf/j;->xM:I

    div-int/2addr v0, v1

    iput v0, p0, Lf/j;->xO:I

    iget v0, p0, Lf/j;->xO:I

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1}, Lf/j;->ar(I)V

    iput p1, p0, Lf/j;->xJ:I

    invoke-virtual {p0, p1}, Lf/j;->as(I)V

    iget-boolean v0, p0, Lf/j;->DEBUG:Z

    if-eqz v0, :cond_4

    sget-object v0, Lf/j;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPST: Delta = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/j;->xK:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/j;->xM:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", AP = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Pst = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lf/j;->xO:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget v0, p0, Lf/j;->xM:I

    invoke-virtual {p0, v0}, Lf/j;->at(I)V

    iget v0, p0, Lf/j;->xO:I

    goto :goto_0
.end method

.method ar(I)V
    .locals 2

    const/16 v1, -0x3e7

    if-eq p1, v1, :cond_0

    iget v0, p0, Lf/j;->xJ:I

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lf/j;->xJ:I

    sub-int v0, p1, v0

    iput v0, p0, Lf/j;->xK:I

    goto :goto_0
.end method

.method as(I)V
    .locals 3

    iget-object v0, p0, Lf/j;->xL:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-gtz v0, :cond_1

    sget-object v0, Lf/j;->TAG:Ljava/lang/String;

    const-string v1, "calculatePst: queue is empty!"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/j;->xL:Ljava/util/Queue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lf/j;->xL:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    iget-object v1, p0, Lf/j;->xL:Ljava/util/Queue;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    iget v1, p0, Lf/j;->xQ:I

    sub-int v0, v1, v0

    iput v0, p0, Lf/j;->xQ:I

    iget v0, p0, Lf/j;->xQ:I

    add-int/2addr v0, p1

    iput v0, p0, Lf/j;->xQ:I

    iget v0, p0, Lf/j;->xQ:I

    iget v1, p0, Lf/j;->xM:I

    div-int/2addr v0, v1

    iput v0, p0, Lf/j;->xO:I

    iget-boolean v0, p0, Lf/j;->DEBUG:Z

    if-eqz v0, :cond_0

    sget-object v0, Lf/j;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "calculatePst: queue = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lf/j;->xL:Ljava/util/Queue;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method at(I)V
    .locals 5

    const/16 v4, 0x1e

    const/16 v3, 0xa

    const/4 v2, 0x0

    iget v0, p0, Lf/j;->xK:I

    const/16 v1, -0x3e7

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lf/j;->xK:I

    div-int/lit8 v0, v0, 0xa

    iput p1, p0, Lf/j;->xN:I

    const/4 v1, 0x5

    if-gt v0, v1, :cond_2

    const/4 v1, -0x5

    if-ge v0, v1, :cond_5

    :cond_2
    iput v3, p0, Lf/j;->xM:I

    iput v2, p0, Lf/j;->xP:I

    :goto_1
    iget v0, p0, Lf/j;->xP:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_3

    iget v0, p0, Lf/j;->xM:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lf/j;->xM:I

    iput v2, p0, Lf/j;->xP:I

    :cond_3
    iget v0, p0, Lf/j;->xM:I

    if-le v0, v4, :cond_8

    iput v4, p0, Lf/j;->xM:I

    :cond_4
    :goto_2
    iget v0, p0, Lf/j;->xM:I

    iget v1, p0, Lf/j;->xN:I

    if-le v0, v1, :cond_9

    iget v0, p0, Lf/j;->xM:I

    invoke-virtual {p0, v0}, Lf/j;->au(I)V

    goto :goto_0

    :cond_5
    if-gtz v0, :cond_6

    const/4 v1, -0x1

    if-ge v0, v1, :cond_7

    :cond_6
    iget v1, p0, Lf/j;->xM:I

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v1, v0

    iput v0, p0, Lf/j;->xM:I

    iput v2, p0, Lf/j;->xP:I

    goto :goto_1

    :cond_7
    iget v0, p0, Lf/j;->xP:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lf/j;->xP:I

    goto :goto_1

    :cond_8
    iget v0, p0, Lf/j;->xM:I

    if-ge v0, v3, :cond_4

    iput v3, p0, Lf/j;->xM:I

    goto :goto_2

    :cond_9
    iget v0, p0, Lf/j;->xM:I

    iget v1, p0, Lf/j;->xN:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lf/j;->xM:I

    invoke-virtual {p0, v0}, Lf/j;->av(I)V

    goto :goto_0
.end method

.method au(I)V
    .locals 5

    const/4 v2, 0x0

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput v2, p0, Lf/j;->xQ:I

    move v1, v2

    :goto_0
    iget v0, p0, Lf/j;->xM:I

    if-ge v1, v0, :cond_3

    iget v0, p0, Lf/j;->xN:I

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lf/j;->xL:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lf/j;->xL:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    iget v4, p0, Lf/j;->xQ:I

    add-int/2addr v4, v0

    iput v4, p0, Lf/j;->xQ:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    iget v0, p0, Lf/j;->xJ:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    iget v0, p0, Lf/j;->xQ:I

    iget v4, p0, Lf/j;->xJ:I

    add-int/2addr v0, v4

    iput v0, p0, Lf/j;->xQ:I

    goto :goto_2

    :cond_3
    iput-object v3, p0, Lf/j;->xL:Ljava/util/Queue;

    return-void
.end method

.method av(I)V
    .locals 5

    const/4 v2, 0x0

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput v2, p0, Lf/j;->xQ:I

    move v1, v2

    :goto_0
    iget v0, p0, Lf/j;->xN:I

    if-ge v1, v0, :cond_3

    iget v0, p0, Lf/j;->xN:I

    iget v4, p0, Lf/j;->xM:I

    sub-int/2addr v0, v4

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lf/j;->xL:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lf/j;->xL:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lf/j;->xL:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lf/j;->xL:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_2
    iget v4, p0, Lf/j;->xQ:I

    add-int/2addr v4, v0

    iput v4, p0, Lf/j;->xQ:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    iput-object v3, p0, Lf/j;->xL:Ljava/util/Queue;

    return-void
.end method
