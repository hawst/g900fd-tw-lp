.class public Lf/h;
.super Lf/c;


# instance fields
.field final TAG:Ljava/lang/String;

.field xl:Ljava/util/HashMap;

.field xw:I

.field xx:[I

.field xy:Lf/t;

.field xz:I


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/c;-><init>()V

    const-class v0, Lf/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/h;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lf/h;->xl:Ljava/util/HashMap;

    const/4 v0, -0x1

    iput v0, p0, Lf/h;->xw:I

    const/16 v0, -0x3e7

    iput v0, p0, Lf/h;->xz:I

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lf/c;-><init>()V

    const-class v0, Lf/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/h;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lf/h;->xl:Ljava/util/HashMap;

    const/4 v0, -0x1

    iput v0, p0, Lf/h;->xw:I

    const/16 v0, -0x3e7

    iput v0, p0, Lf/h;->xz:I

    invoke-virtual {p0, p1}, Lf/h;->ao(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lf/h;->ap(Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 1

    invoke-direct {p0}, Lf/c;-><init>()V

    const-class v0, Lf/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/h;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lf/h;->xl:Ljava/util/HashMap;

    const/4 v0, -0x1

    iput v0, p0, Lf/h;->xw:I

    const/16 v0, -0x3e7

    iput v0, p0, Lf/h;->xz:I

    invoke-virtual {p0, p1}, Lf/h;->ao(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lf/h;->ap(Ljava/lang/String;)V

    invoke-virtual {p0, p3}, Lf/h;->a(Ljava/util/HashMap;)V

    return-void
.end method


# virtual methods
.method a(Ljava/util/HashMap;)V
    .locals 5

    const/4 v0, 0x0

    new-instance v1, Lf/t;

    invoke-direct {v1}, Lf/t;-><init>()V

    iput-object v1, p0, Lf/h;->xy:Lf/t;

    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result v1

    new-array v1, v1, [I

    iput-object v1, p0, Lf/h;->xx:[I

    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v4, p0, Lf/h;->xx:[I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v4, v2

    iget-object v0, p0, Lf/h;->xl:Ljava/util/HashMap;

    iget-object v4, p0, Lf/h;->xx:[I

    aget v4, v4, v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf/h;->xx:[I

    invoke-static {v0}, Ljava/util/Arrays;->sort([I)V

    return-void
.end method

.method ao(I)V
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, Lf/h;->xj:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h;->xx:[I

    aget v0, v0, v1

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lf/h;->xx:[I

    aget v0, v0, v1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lf/h;->xl:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lf/h;->xw:I

    invoke-virtual {p0}, Lf/h;->ex()V

    iput v1, p0, Lf/h;->xz:I

    :cond_0
    return-void

    :cond_1
    iget v0, p0, Lf/h;->xz:I

    iget v1, p0, Lf/h;->xz:I

    if-ge p1, v1, :cond_5

    iget-object v1, p0, Lf/h;->xx:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_4

    iget-object v2, p0, Lf/h;->xx:[I

    aget v2, v2, v1

    iget v3, p0, Lf/h;->xz:I

    if-lt v2, v3, :cond_3

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lf/h;->xx:[I

    aget v2, v2, v1

    if-ge p1, v2, :cond_2

    iget-object v0, p0, Lf/h;->xx:[I

    aget v0, v0, v1

    :cond_4
    move v1, v0

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lf/h;->xx:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    :goto_2
    if-ltz v1, :cond_7

    iget-object v2, p0, Lf/h;->xx:[I

    aget v2, v2, v1

    if-lt p1, v2, :cond_6

    iget-object v0, p0, Lf/h;->xx:[I

    aget v0, v0, v1

    move v1, v0

    goto :goto_0

    :cond_6
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_7
    move v1, v0

    goto :goto_0
.end method

.method ex()V
    .locals 3

    iget-boolean v0, p0, Lf/h;->xj:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h;->xy:Lf/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/h;->xy:Lf/t;

    iget-object v1, p0, Lf/h;->mName:Ljava/lang/String;

    iget v2, p0, Lf/h;->xw:I

    invoke-virtual {v0, v1, v2}, Lf/t;->l(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method ey()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/h;->xj:Z

    return-void
.end method

.method ez()I
    .locals 1

    iget-boolean v0, p0, Lf/h;->xj:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lf/h;->xw:I

    :goto_0
    return v0

    :cond_0
    const/16 v0, -0x3e7

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lf/h;->mName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lf/h;->xi:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "] "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lf/h;->xl:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "temp="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lf/h;->xx:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",value="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lf/h;->xl:Ljava/util/HashMap;

    iget-object v4, p0, Lf/h;->xx:[I

    aget v4, v4, v0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
