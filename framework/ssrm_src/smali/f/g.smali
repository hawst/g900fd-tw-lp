.class public Lf/g;
.super Lf/c;


# instance fields
.field final TAG:Ljava/lang/String;

.field xr:[I

.field xt:I

.field xv:Lf/s;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lf/c;-><init>()V

    const-class v0, Lf/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/g;->TAG:Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [I

    iput-object v0, p0, Lf/g;->xr:[I

    const/16 v0, -0x3e7

    iput v0, p0, Lf/g;->xt:I

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lf/c;-><init>()V

    const-class v0, Lf/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/g;->TAG:Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [I

    iput-object v0, p0, Lf/g;->xr:[I

    const/16 v0, -0x3e7

    iput v0, p0, Lf/g;->xt:I

    invoke-virtual {p0, p1}, Lf/g;->ao(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lf/g;->ap(Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 1

    invoke-direct {p0}, Lf/c;-><init>()V

    const-class v0, Lf/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/g;->TAG:Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [I

    iput-object v0, p0, Lf/g;->xr:[I

    const/16 v0, -0x3e7

    iput v0, p0, Lf/g;->xt:I

    invoke-virtual {p0, p1}, Lf/g;->ao(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lf/g;->ap(Ljava/lang/String;)V

    invoke-virtual {p0, p3}, Lf/g;->a(Ljava/util/HashMap;)V

    return-void
.end method


# virtual methods
.method a(Ljava/util/HashMap;)V
    .locals 4

    const/4 v1, -0x1

    iget-object v2, p0, Lf/g;->xr:[I

    const/4 v3, 0x0

    const-string v0, "pre"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v0, "pre"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    aput v0, v2, v3

    iget-object v2, p0, Lf/g;->xr:[I

    const/4 v3, 0x1

    const-string v0, "trigger"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "trigger"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    aput v0, v2, v3

    iget-object v2, p0, Lf/g;->xr:[I

    const/4 v3, 0x2

    const-string v0, "release"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v0, "release"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_2
    aput v0, v2, v3

    iget-object v2, p0, Lf/g;->xr:[I

    const/4 v3, 0x3

    const-string v0, "duration"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v0, "duration"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_3
    aput v0, v2, v3

    iget-object v2, p0, Lf/g;->xr:[I

    const/4 v3, 0x4

    const-string v0, "max"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v0, "max"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_4
    aput v0, v2, v3

    iget-object v2, p0, Lf/g;->xr:[I

    const/4 v3, 0x5

    const-string v0, "min"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "min"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :cond_0
    aput v1, v2, v3

    new-instance v0, Lf/s;

    iget-object v1, p0, Lf/g;->mName:Ljava/lang/String;

    iget-object v2, p0, Lf/g;->xr:[I

    invoke-direct {v0, v1, v2}, Lf/s;-><init>(Ljava/lang/String;[I)V

    iput-object v0, p0, Lf/g;->xv:Lf/s;

    return-void

    :cond_1
    move v0, v1

    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto/16 :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4
.end method

.method ao(I)V
    .locals 1

    iget-boolean v0, p0, Lf/g;->xj:Z

    if-eqz v0, :cond_0

    iput p1, p0, Lf/g;->xt:I

    invoke-virtual {p0}, Lf/g;->ex()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lf/g;->ey()V

    goto :goto_0
.end method

.method ex()V
    .locals 2

    iget-boolean v0, p0, Lf/g;->xj:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lf/g;->xv:Lf/s;

    iget v1, p0, Lf/g;->xt:I

    invoke-virtual {v0, v1}, Lf/s;->aC(I)V

    :cond_0
    return-void
.end method

.method ey()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/g;->xj:Z

    iget-object v0, p0, Lf/g;->xv:Lf/s;

    invoke-virtual {v0}, Lf/s;->fb()V

    return-void
.end method

.method ez()I
    .locals 1

    const/16 v0, -0x3e7

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lf/g;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/g;->xi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "pre="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/g;->xr:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",trigger="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/g;->xr:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",release="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/g;->xr:[I

    const/4 v2, 0x2

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/g;->xr:[I

    const/4 v2, 0x3

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",max="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/g;->xr:[I

    const/4 v2, 0x4

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",min="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lf/g;->xr:[I

    const/4 v2, 0x5

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
