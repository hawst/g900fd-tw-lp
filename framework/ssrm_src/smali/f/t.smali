.class Lf/t;
.super Ljava/lang/Object;


# instance fields
.field DEBUG:Z

.field TAG:Ljava/lang/String;

.field ig:Lcom/android/server/ssrm/X;


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf/t;->DEBUG:Z

    const-class v0, Lf/t;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/t;->TAG:Ljava/lang/String;

    invoke-static {}, Lcom/android/server/ssrm/X;->aU()Lcom/android/server/ssrm/X;

    move-result-object v0

    iput-object v0, p0, Lf/t;->ig:Lcom/android/server/ssrm/X;

    return-void
.end method


# virtual methods
.method public A(I)V
    .locals 0

    invoke-static {p1}, Lcom/android/server/ssrm/X;->A(I)V

    return-void
.end method

.method public E(I)V
    .locals 1

    iget-object v0, p0, Lf/t;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/X;->E(I)V

    return-void
.end method

.method public final F(I)V
    .locals 1

    iget-object v0, p0, Lf/t;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/X;->F(I)V

    return-void
.end method

.method public P(I)V
    .locals 1

    invoke-static {}, Lcom/android/server/ssrm/ag;->bj()Lcom/android/server/ssrm/ag;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/ag;->J(I)V

    return-void
.end method

.method public aE(I)V
    .locals 0

    invoke-static {p1}, Lcom/android/server/ssrm/X;->B(I)V

    return-void
.end method

.method public aF(I)V
    .locals 1

    iget-object v0, p0, Lf/t;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/X;->w(I)V

    return-void
.end method

.method public aG(I)V
    .locals 1

    iget-object v0, p0, Lf/t;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/X;->x(I)V

    return-void
.end method

.method public aH(I)V
    .locals 1

    invoke-static {}, Lf/m;->eN()Lf/m;

    move-result-object v0

    invoke-virtual {v0, p1}, Lf/m;->aB(I)V

    return-void
.end method

.method public aI(I)V
    .locals 1

    invoke-static {}, Lf/m;->eN()Lf/m;

    move-result-object v0

    invoke-virtual {v0, p1}, Lf/m;->aA(I)V

    return-void
.end method

.method public aJ(I)V
    .locals 1

    iget-object v0, p0, Lf/t;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/X;->C(I)V

    return-void
.end method

.method public aK(I)V
    .locals 1

    iget-object v0, p0, Lf/t;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/X;->D(I)V

    return-void
.end method

.method l(Ljava/lang/String;I)V
    .locals 1

    const-string v0, "CPUCoreMax"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p2}, Lf/t;->z(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "CPUFreqMax"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p2}, Lf/t;->y(I)V

    goto :goto_0

    :cond_2
    const-string v0, "GPUFreqMax"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, p2}, Lf/t;->F(I)V

    goto :goto_0

    :cond_3
    const-string v0, "SIOPLevel"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, p2}, Lf/t;->E(I)V

    goto :goto_0

    :cond_4
    const-string v0, "Charging"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0, p2}, Lf/t;->A(I)V

    goto :goto_0

    :cond_5
    const-string v0, "LCDBrightness"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0, p2}, Lf/t;->P(I)V

    goto :goto_0

    :cond_6
    const-string v0, "DynamicFpsLevel"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0, p2}, Lf/t;->aE(I)V

    goto :goto_0

    :cond_7
    const-string v0, "IPAControlTemp"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0, p2}, Lf/t;->aF(I)V

    goto :goto_0

    :cond_8
    const-string v0, "IPAHotPlugOut"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0, p2}, Lf/t;->aG(I)V

    goto :goto_0

    :cond_9
    const-string v0, "TempShift"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p0, p2}, Lf/t;->aH(I)V

    goto :goto_0

    :cond_a
    const-string v0, "PST"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p0, p2}, Lf/t;->aI(I)V

    goto :goto_0

    :cond_b
    const-string v0, "MaxVolume"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0, p2}, Lf/t;->aJ(I)V

    goto/16 :goto_0

    :cond_c
    const-string v0, "HmtLevel"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lf/t;->aK(I)V

    goto/16 :goto_0
.end method

.method public final y(I)V
    .locals 1

    iget-object v0, p0, Lf/t;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/X;->y(I)V

    return-void
.end method

.method public final z(I)V
    .locals 1

    iget-object v0, p0, Lf/t;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/X;->z(I)V

    return-void
.end method
