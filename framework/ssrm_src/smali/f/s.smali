.class Lf/s;
.super Ljava/lang/Object;


# instance fields
.field DEBUG:Z

.field TAG:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mDuration:I

.field mName:Ljava/lang/String;

.field final yR:I

.field final yS:I

.field yT:I

.field yW:I

.field yX:I

.field yY:I

.field yZ:I

.field za:I

.field zc:Landroid/os/DVFSHelper;

.field zd:[I

.field zj:[I

.field zv:Lf/e;

.field zw:Z


# direct methods
.method constructor <init>(Ljava/lang/String;[I)V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v2, -0x3e7

    const/4 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v4, p0, Lf/s;->DEBUG:Z

    const-class v0, Lf/s;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf/s;->TAG:Ljava/lang/String;

    const/16 v0, 0xa

    iput v0, p0, Lf/s;->yR:I

    const/16 v0, 0x46

    iput v0, p0, Lf/s;->yS:I

    iput v1, p0, Lf/s;->yT:I

    iput v1, p0, Lf/s;->yW:I

    iput v1, p0, Lf/s;->yX:I

    iput v1, p0, Lf/s;->yY:I

    iput v1, p0, Lf/s;->mDuration:I

    iput v1, p0, Lf/s;->yZ:I

    iput v1, p0, Lf/s;->za:I

    const/4 v0, 0x0

    iput-object v0, p0, Lf/s;->zc:Landroid/os/DVFSHelper;

    const/4 v0, 0x7

    new-array v0, v0, [I

    iput-object v0, p0, Lf/s;->zj:[I

    iput-boolean v3, p0, Lf/s;->zw:Z

    invoke-static {}, Lf/m;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lf/s;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lf/s;->zj:[I

    aget v0, p2, v4

    iput v0, p0, Lf/s;->yX:I

    const/4 v0, 0x2

    aget v0, p2, v0

    iput v0, p0, Lf/s;->yY:I

    iput-object p1, p0, Lf/s;->mName:Ljava/lang/String;

    aget v0, p2, v3

    if-ne v0, v2, :cond_1

    iget v0, p0, Lf/s;->yY:I

    add-int/lit8 v0, v0, 0xa

    iput v0, p0, Lf/s;->yW:I

    :goto_0
    aget v0, p2, v5

    if-ne v0, v2, :cond_3

    const-string v0, "CPUFreqMax"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x2710

    iput v0, p0, Lf/s;->mDuration:I

    :cond_0
    :goto_1
    const/4 v0, 0x4

    aget v0, p2, v0

    if-ne v0, v2, :cond_4

    iput v1, p0, Lf/s;->yZ:I

    :goto_2
    const/4 v0, 0x5

    aget v0, p2, v0

    iput v0, p0, Lf/s;->za:I

    return-void

    :cond_1
    aget v0, p2, v3

    iput v0, p0, Lf/s;->yW:I

    goto :goto_0

    :cond_2
    const-string v0, "GPUFreqMax"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x7530

    iput v0, p0, Lf/s;->mDuration:I

    goto :goto_1

    :cond_3
    aget v0, p2, v5

    iput v0, p0, Lf/s;->mDuration:I

    goto :goto_1

    :cond_4
    const/4 v0, 0x4

    aget v0, p2, v0

    iput v0, p0, Lf/s;->yZ:I

    goto :goto_2
.end method


# virtual methods
.method aC(I)V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lf/s;->zd:[I

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lf/s;->eU()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lf/s;->zv:Lf/e;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lf/s;->zw:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lf/s;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startSts:: mChangeParameters = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lf/s;->zw:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf/s;->zv:Lf/e;

    iget-object v1, p0, Lf/s;->zj:[I

    invoke-virtual {v0, v1}, Lf/e;->b([I)V

    iget-object v0, p0, Lf/s;->zv:Lf/e;

    invoke-virtual {v0}, Lf/e;->eF()V

    iget-object v0, p0, Lf/s;->zv:Lf/e;

    invoke-virtual {v0, v3}, Lf/e;->aj(Z)V

    iput-boolean v3, p0, Lf/s;->zw:Z

    goto :goto_0
.end method

.method eU()V
    .locals 8

    const-wide/16 v4, 0x0

    const/4 v7, -0x1

    const/4 v6, 0x0

    const-string v0, "CPUFreqMax"

    iget-object v1, p0, Lf/s;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/os/DVFSHelper;

    iget-object v1, p0, Lf/s;->mContext:Landroid/content/Context;

    const-string v2, "STSH_CPU_MAX"

    const/16 v3, 0xd

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lf/s;->zc:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lf/s;->zc:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUFrequencyForSSRM()[I

    move-result-object v0

    iput-object v0, p0, Lf/s;->zd:[I

    :cond_0
    :goto_0
    iget-object v0, p0, Lf/s;->zd:[I

    if-nez v0, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string v0, "GPUFreqMax"

    iget-object v1, p0, Lf/s;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/DVFSHelper;

    iget-object v1, p0, Lf/s;->mContext:Landroid/content/Context;

    const-string v2, "STSH_GPU_MAX"

    const/16 v3, 0x11

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lf/s;->zc:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lf/s;->zc:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedGPUFrequency()[I

    move-result-object v0

    iput-object v0, p0, Lf/s;->zd:[I

    goto :goto_0

    :cond_3
    iget v0, p0, Lf/s;->yZ:I

    if-ne v0, v7, :cond_4

    iget-object v0, p0, Lf/s;->zd:[I

    aget v0, v0, v6

    iput v0, p0, Lf/s;->yZ:I

    :cond_4
    iget v0, p0, Lf/s;->za:I

    if-ne v0, v7, :cond_5

    iget-object v0, p0, Lf/s;->zd:[I

    iget-object v1, p0, Lf/s;->zd:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    iput v0, p0, Lf/s;->za:I

    :cond_5
    iget-object v0, p0, Lf/s;->zd:[I

    aget v0, v0, v6

    div-int/lit8 v0, v0, 0x64

    mul-int/lit8 v1, v0, 0x46

    move v0, v6

    :goto_2
    iget-object v2, p0, Lf/s;->zd:[I

    array-length v2, v2

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lf/s;->zd:[I

    aget v2, v2, v0

    if-gt v2, v1, :cond_7

    add-int/lit8 v1, v0, -0x1

    if-gez v1, :cond_6

    iget-object v0, p0, Lf/s;->zd:[I

    aget v0, v0, v6

    :goto_3
    iput v0, p0, Lf/s;->yT:I

    iget-object v0, p0, Lf/s;->zj:[I

    iget v1, p0, Lf/s;->yW:I

    aput v1, v0, v6

    iget-object v0, p0, Lf/s;->zj:[I

    const/4 v1, 0x1

    iget v2, p0, Lf/s;->yX:I

    aput v2, v0, v1

    iget-object v0, p0, Lf/s;->zj:[I

    const/4 v1, 0x2

    iget v2, p0, Lf/s;->yY:I

    aput v2, v0, v1

    iget-object v0, p0, Lf/s;->zj:[I

    const/4 v1, 0x3

    iget v2, p0, Lf/s;->mDuration:I

    aput v2, v0, v1

    iget-object v0, p0, Lf/s;->zj:[I

    const/4 v1, 0x4

    iget v2, p0, Lf/s;->yZ:I

    aput v2, v0, v1

    iget-object v0, p0, Lf/s;->zj:[I

    const/4 v1, 0x5

    iget v2, p0, Lf/s;->za:I

    aput v2, v0, v1

    iget-object v0, p0, Lf/s;->zj:[I

    const/4 v1, 0x6

    iget v2, p0, Lf/s;->yT:I

    aput v2, v0, v1

    sget-object v0, Lf/m;->yk:Lf/l;

    if-eqz v0, :cond_1

    sget-object v0, Lf/m;->yk:Lf/l;

    iget-object v1, p0, Lf/s;->mName:Ljava/lang/String;

    const-string v2, "STS"

    invoke-virtual {v0, v1, v2}, Lf/l;->s(Ljava/lang/String;Ljava/lang/String;)Lf/c;

    move-result-object v0

    check-cast v0, Lf/e;

    iput-object v0, p0, Lf/s;->zv:Lf/e;

    goto/16 :goto_1

    :cond_6
    iget-object v1, p0, Lf/s;->zd:[I

    add-int/lit8 v0, v0, -0x1

    aget v0, v1, v0

    goto :goto_3

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_8
    move v0, v1

    goto :goto_3
.end method

.method ez()I
    .locals 1

    iget-object v0, p0, Lf/s;->zv:Lf/e;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lf/s;->zv:Lf/e;

    invoke-virtual {v0}, Lf/e;->ez()I

    move-result v0

    goto :goto_0
.end method

.method fb()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lf/s;->zv:Lf/e;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lf/s;->zv:Lf/e;

    invoke-virtual {v0}, Lf/e;->eE()V

    iput-boolean v1, p0, Lf/s;->zw:Z

    iget-object v0, p0, Lf/s;->zv:Lf/e;

    invoke-virtual {v0, v1}, Lf/e;->aj(Z)V

    goto :goto_0
.end method
