.class public Ld/a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/server/ssrm/aE;


# static fields
.field private static TAG:Ljava/lang/String;

.field private static vu:Ljava/util/ArrayList;


# instance fields
.field cr:Lcom/android/server/ssrm/d;

.field vA:I

.field vv:Lcom/android/server/ssrm/d;

.field vw:Lcom/android/server/ssrm/d;

.field vx:Lcom/android/server/ssrm/d;

.field vy:I

.field vz:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Ld/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ld/a;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Ld/a;->vu:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ld/a;->cr:Lcom/android/server/ssrm/d;

    iput-object v0, p0, Ld/a;->vv:Lcom/android/server/ssrm/d;

    iput-object v0, p0, Ld/a;->vw:Lcom/android/server/ssrm/d;

    iput-object v0, p0, Ld/a;->vx:Lcom/android/server/ssrm/d;

    iput v1, p0, Ld/a;->vy:I

    iput-boolean v1, p0, Ld/a;->vz:Z

    iput v1, p0, Ld/a;->vA:I

    return-void
.end method

.method public static b(Ljava/util/ArrayList;)V
    .locals 2

    sget-object v0, Ld/a;->vu:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    sget-object v0, Ld/a;->TAG:Ljava/lang/String;

    const-string v1, "Policy is already updated."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sput-object p0, Ld/a;->vu:Ljava/util/ArrayList;

    goto :goto_0
.end method


# virtual methods
.method public O(I)V
    .locals 3

    sget-object v0, Ld/a;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onChanged:: t = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mPstTriggerTemp = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Ld/a;->vy:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Ld/a;->vy:I

    if-lt p1, v0, :cond_4

    iget-boolean v0, p0, Ld/a;->vz:Z

    if-nez v0, :cond_4

    const/4 v0, 0x1

    iput-boolean v0, p0, Ld/a;->vz:Z

    iget-object v0, p0, Ld/a;->vv:Lcom/android/server/ssrm/d;

    if-eqz v0, :cond_0

    sget-object v0, Ld/a;->TAG:Ljava/lang/String;

    const-string v1, "CPU freq min lock is acquired."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ld/a;->vv:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->acquire()V

    :cond_0
    iget-object v0, p0, Ld/a;->cr:Lcom/android/server/ssrm/d;

    if-eqz v0, :cond_1

    sget-object v0, Ld/a;->TAG:Ljava/lang/String;

    const-string v1, "CPU freq max lock is acquired."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ld/a;->cr:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->acquire()V

    :cond_1
    iget-object v0, p0, Ld/a;->vw:Lcom/android/server/ssrm/d;

    if-eqz v0, :cond_2

    sget-object v0, Ld/a;->TAG:Ljava/lang/String;

    const-string v1, "GPU freq max lock is acquired."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ld/a;->vw:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->acquire()V

    :cond_2
    iget-object v0, p0, Ld/a;->vx:Lcom/android/server/ssrm/d;

    if-eqz v0, :cond_3

    sget-object v0, Ld/a;->TAG:Ljava/lang/String;

    const-string v1, "GPU freq min lock is acquired."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ld/a;->vx:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->acquire()V

    :cond_3
    :goto_0
    iput p1, p0, Ld/a;->vA:I

    return-void

    :cond_4
    iget-boolean v0, p0, Ld/a;->vz:Z

    if-eqz v0, :cond_3

    iget v0, p0, Ld/a;->vy:I

    if-ge p1, v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Ld/a;->vz:Z

    iget-object v0, p0, Ld/a;->vv:Lcom/android/server/ssrm/d;

    if-eqz v0, :cond_5

    sget-object v0, Ld/a;->TAG:Ljava/lang/String;

    const-string v1, "CPU freq min lock is released."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ld/a;->vv:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->release()V

    :cond_5
    iget-object v0, p0, Ld/a;->cr:Lcom/android/server/ssrm/d;

    if-eqz v0, :cond_6

    sget-object v0, Ld/a;->TAG:Ljava/lang/String;

    const-string v1, "CPU freq max lock is released."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ld/a;->cr:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->release()V

    :cond_6
    iget-object v0, p0, Ld/a;->vw:Lcom/android/server/ssrm/d;

    if-eqz v0, :cond_7

    sget-object v0, Ld/a;->TAG:Ljava/lang/String;

    const-string v1, "GPU freq max lock is released."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ld/a;->vw:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->release()V

    :cond_7
    iget-object v0, p0, Ld/a;->vx:Lcom/android/server/ssrm/d;

    if-eqz v0, :cond_3

    sget-object v0, Ld/a;->TAG:Ljava/lang/String;

    const-string v1, "GPU freq min lock is released."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ld/a;->vx:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->release()V

    goto :goto_0
.end method

.method public aj(Ljava/lang/String;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v5, -0x1

    iget-boolean v0, p0, Ld/a;->vz:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Ld/a;->vz:Z

    iget-object v0, p0, Ld/a;->vv:Lcom/android/server/ssrm/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ld/a;->vv:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->release()V

    iput-object v2, p0, Ld/a;->vv:Lcom/android/server/ssrm/d;

    sget-object v0, Ld/a;->TAG:Ljava/lang/String;

    const-string v1, "CPU freq min lock is released."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Ld/a;->cr:Lcom/android/server/ssrm/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ld/a;->cr:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->release()V

    iput-object v2, p0, Ld/a;->cr:Lcom/android/server/ssrm/d;

    sget-object v0, Ld/a;->TAG:Ljava/lang/String;

    const-string v1, "CPU freq max lock is released."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Ld/a;->vw:Lcom/android/server/ssrm/d;

    if-eqz v0, :cond_2

    iget-object v0, p0, Ld/a;->vw:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->release()V

    iput-object v2, p0, Ld/a;->vw:Lcom/android/server/ssrm/d;

    sget-object v0, Ld/a;->TAG:Ljava/lang/String;

    const-string v1, "GPU freq max lock is released."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Ld/a;->vx:Lcom/android/server/ssrm/d;

    if-eqz v0, :cond_3

    iget-object v0, p0, Ld/a;->vx:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->release()V

    iput-object v2, p0, Ld/a;->vx:Lcom/android/server/ssrm/d;

    sget-object v0, Ld/a;->TAG:Ljava/lang/String;

    const-string v1, "GPU freq min lock is released."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    sget-object v0, Ld/a;->vu:Ljava/util/ArrayList;

    if-nez v0, :cond_4

    :goto_0
    return-void

    :cond_4
    sget-object v0, Ld/a;->vu:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ld/b;

    invoke-virtual {v0, p1}, Ld/b;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object v1, Ld/a;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Package "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is in Group "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ld/b;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ld/b;->ea()I

    move-result v1

    iput v1, p0, Ld/a;->vy:I

    const-string v1, "cpu_min"

    invoke-virtual {v0, v1}, Ld/b;->ak(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v5, :cond_6

    sget-object v2, Ld/a;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Target CPU min freq is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "HGL_CPU_MIN"

    invoke-static {v2, v1}, Lcom/android/server/ssrm/d;->a(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v1

    iput-object v1, p0, Ld/a;->vv:Lcom/android/server/ssrm/d;

    :cond_6
    const-string v1, "cpu_max"

    invoke-virtual {v0, v1}, Ld/b;->ak(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v5, :cond_7

    sget-object v2, Ld/a;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Target CPU max freq is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "HGL_CPU_MAX"

    invoke-static {v2, v1}, Lcom/android/server/ssrm/d;->b(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v1

    iput-object v1, p0, Ld/a;->cr:Lcom/android/server/ssrm/d;

    :cond_7
    const-string v1, "gpu_max"

    invoke-virtual {v0, v1}, Ld/b;->ak(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v5, :cond_8

    sget-object v2, Ld/a;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Target GPU max freq is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "HGL_GPU_MAX"

    invoke-static {v2, v1}, Lcom/android/server/ssrm/d;->f(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v1

    iput-object v1, p0, Ld/a;->vw:Lcom/android/server/ssrm/d;

    :cond_8
    const-string v1, "gpu_min"

    invoke-virtual {v0, v1}, Ld/b;->ak(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v5, :cond_9

    sget-object v1, Ld/a;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Target GPU min freq is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "HGL_GPU_MIN"

    invoke-static {v1, v0}, Lcom/android/server/ssrm/d;->e(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v0

    iput-object v0, p0, Ld/a;->vx:Lcom/android/server/ssrm/d;

    :cond_9
    iget v0, p0, Ld/a;->vA:I

    invoke-virtual {p0, v0}, Ld/a;->O(I)V

    goto/16 :goto_0
.end method
