.class public Lcom/android/server/ssrm/k;
.super Lcom/android/server/ssrm/e;


# instance fields
.field J:I

.field N:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/server/ssrm/e;-><init>(Ljava/lang/String;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/ssrm/k;->J:I

    iput p2, p0, Lcom/android/server/ssrm/k;->N:I

    return-void
.end method


# virtual methods
.method public acquire(I)V
    .locals 8

    iget-object v0, p0, Lcom/android/server/ssrm/k;->H:Landroid/os/CustomFrequencyManager$FrequencyRequest;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/ssrm/k;->J:I

    if-eq v0, p1, :cond_1

    :cond_0
    sget-object v1, Lcom/android/server/ssrm/d;->D:Landroid/os/CustomFrequencyManager;

    const/16 v2, 0x9

    iget v3, p0, Lcom/android/server/ssrm/k;->N:I

    int-to-long v4, p1

    iget-object v6, p0, Lcom/android/server/ssrm/k;->mTag:Ljava/lang/String;

    sget-object v7, Lcom/android/server/ssrm/d;->C:Landroid/content/Context;

    invoke-virtual/range {v1 .. v7}, Landroid/os/CustomFrequencyManager;->newFrequencyRequest(IIJLjava/lang/String;Landroid/content/Context;)Landroid/os/CustomFrequencyManager$FrequencyRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/k;->H:Landroid/os/CustomFrequencyManager$FrequencyRequest;

    iput p1, p0, Lcom/android/server/ssrm/k;->J:I

    :cond_1
    invoke-virtual {p0}, Lcom/android/server/ssrm/k;->b()V

    return-void
.end method

.method release()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/server/ssrm/k;->c()V

    return-void
.end method
