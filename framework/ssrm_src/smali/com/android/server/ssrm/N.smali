.class public Lcom/android/server/ssrm/N;
.super Ljava/lang/Object;


# static fields
.field static final TAG:Ljava/lang/String;

.field public static cA:Z

.field private static cB:Ljava/lang/String;

.field public static cC:Z

.field public static cD:Z

.field public static cE:Z

.field public static cF:Z

.field public static cG:Z

.field public static cH:Ljava/lang/String;

.field public static cI:Z

.field public static cJ:Z

.field public static cK:Z

.field public static cL:Z

.field public static cM:Z

.field public static cN:Z

.field public static cO:Z

.field public static cP:Z

.field public static cQ:Z

.field public static cR:Z

.field public static cS:Z

.field public static cT:Z

.field public static cU:Z

.field public static cV:Z

.field public static cW:Z

.field public static cX:Z

.field public static cY:Z

.field public static cZ:Z

.field public static cw:Ljava/lang/String;

.field public static cx:Ljava/lang/String;

.field public static cy:Ljava/lang/String;

.field public static cz:Ljava/lang/String;

.field public static dA:Z

.field public static dB:Z

.field public static dC:Z

.field public static dD:Z

.field public static dE:Z

.field public static dF:Z

.field public static dG:Z

.field public static dH:Z

.field public static dI:Z

.field public static dJ:Z

.field public static dK:Z

.field public static dL:Z

.field public static dM:Z

.field public static dN:Z

.field public static dO:Z

.field public static dP:Z

.field public static dQ:Z

.field public static dR:Z

.field public static dS:Z

.field public static dT:Z

.field public static dU:Z

.field public static dV:Z

.field public static dW:Z

.field public static dX:Z

.field public static dY:Z

.field public static dZ:Z

.field public static da:Z

.field public static db:Z

.field public static dc:Z

.field public static dd:Z

.field public static de:Z

.field public static df:Z

.field public static dg:Z

.field public static dh:Z

.field public static di:Z

.field public static dj:Z

.field public static dk:Z

.field public static dl:Z

.field public static dm:Z

.field public static dn:Z

.field public static do:Z

.field public static dp:Z

.field public static dq:Z

.field public static dr:Z

.field public static ds:Z

.field public static dt:Z

.field public static du:Z

.field public static dv:Z

.field public static dw:Z

.field public static dx:Z

.field public static dy:Z

.field public static dz:Z

.field public static eA:Z

.field public static eB:Z

.field public static eC:Z

.field public static eD:Z

.field public static eE:Z

.field public static eF:Z

.field public static eG:Z

.field public static ea:Z

.field public static eb:Z

.field public static ec:Z

.field public static ed:Z

.field public static ee:Z

.field public static ef:Z

.field public static eg:Z

.field public static eh:Ljava/lang/String;

.field public static ei:Z

.field public static ej:Z

.field public static ek:Z

.field public static el:Z

.field public static em:Z

.field public static en:Z

.field public static eo:Z

.field public static ep:Z

.field public static eq:Z

.field public static er:Z

.field public static es:Z

.field public static et:Z

.field public static eu:Z

.field public static ev:Z

.field public static ew:Z

.field public static ex:Z

.field public static ey:Z

.field public static ez:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-class v0, Lcom/android/server/ssrm/N;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/N;->TAG:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/android/server/ssrm/N;->cy:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/android/server/ssrm/N;->cz:Ljava/lang/String;

    sput-boolean v1, Lcom/android/server/ssrm/N;->cA:Z

    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/N;->cB:Ljava/lang/String;

    const-string v0, "korea"

    sget-object v3, Lcom/android/server/ssrm/N;->cB:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/N;->cC:Z

    const-string v0, "china"

    sget-object v3, Lcom/android/server/ssrm/N;->cB:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/N;->cD:Z

    const-string v0, "jp"

    sget-object v3, Lcom/android/server/ssrm/N;->cB:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/N;->cE:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cF:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cG:Z

    const-string v0, ""

    sput-object v0, Lcom/android/server/ssrm/N;->cH:Ljava/lang/String;

    sput-boolean v1, Lcom/android/server/ssrm/N;->cI:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cJ:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cK:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cL:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cM:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cN:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cO:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cP:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cQ:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cR:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cS:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cT:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cU:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cV:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cW:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cX:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cY:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->cZ:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->da:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->db:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dc:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dd:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->de:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->df:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dg:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dh:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->di:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dj:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dk:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dl:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dm:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dn:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->do:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dp:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dq:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dr:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->ds:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dt:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->du:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dv:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dw:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dx:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dy:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dz:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dA:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dB:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dC:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dD:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dE:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dF:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dG:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dH:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dI:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dJ:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dK:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dL:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dM:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dN:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dO:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dP:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dQ:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dR:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dS:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dT:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dU:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dV:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dW:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dX:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dY:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->dZ:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->ea:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->eb:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->ec:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->ed:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->ee:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->ef:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->eg:Z

    const-string v0, ""

    sput-object v0, Lcom/android/server/ssrm/N;->eh:Ljava/lang/String;

    sput-boolean v1, Lcom/android/server/ssrm/N;->ei:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->ej:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->ek:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->el:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->em:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->en:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->eo:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->ep:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->eq:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->er:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->es:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->et:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->eu:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->ev:Z

    :try_start_0
    new-instance v0, Ldalvik/system/PathClassLoader;

    const-string v3, "/system/framework/services.jar"

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    const-string v3, "com.android.server.SsrmService"

    invoke-virtual {v0, v3}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    const-string v0, "BASE_MODEL"

    invoke-virtual {v3, v0}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    const-string v0, "SSRM_FILENAME"

    invoke-virtual {v3, v0}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v0, "dvfs_hint"

    sput-object v0, Lcom/android/server/ssrm/N;->cy:Ljava/lang/String;

    const-string v0, "AIR_VIEW_PROVIDER_NAME"

    invoke-virtual {v3, v0}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/android/server/ssrm/N;->cz:Ljava/lang/String;

    const-string v0, "COMMON_GESTURE_WITH_FINGERHOVER"

    invoke-virtual {v3, v0}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/N;->cA:Z

    const-string v0, "GMS_BUNDLING"

    invoke-virtual {v3, v0}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/N;->et:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    const-string v0, "SUPPORT_WEARABLE_HMT"

    invoke-virtual {v3, v0}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/N;->eu:Z
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    :try_start_2
    const-string v0, "scx15"

    const-string v3, "ro.board.platform"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/N;->cF:Z

    const-string v0, "ro.board.platform"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const-string v0, "msm"

    invoke-virtual {v3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "apq"

    invoke-virtual {v3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_68

    :cond_0
    move v0, v2

    :goto_1
    sput-boolean v0, Lcom/android/server/ssrm/N;->ep:Z

    const-string v0, "exynos"

    invoke-virtual {v3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/N;->eq:Z

    const-string v0, "mrvl"

    invoke-virtual {v3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/N;->er:Z

    const-string v0, "sc"

    invoke-virtual {v3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/N;->es:Z

    :cond_1
    const-string v0, "msm8226"

    const-string v3, "ro.board.platform"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/N;->dp:Z

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v3, "CscFeature_Common_ConfigSiop"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v3, "CscFeature_Common_ConfigSiop"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/N;->cH:Ljava/lang/String;

    const-string v0, "SIOPS4ITALY"

    sget-object v3, Lcom/android/server/ssrm/N;->cH:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "ssrm_jf_italy"

    sput-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cG:Z

    :cond_2
    const-string v0, "ha"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cM:Z

    :cond_3
    const-string v0, "hf"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cN:Z

    :cond_4
    sget-boolean v0, Lcom/android/server/ssrm/N;->cM:Z

    if-nez v0, :cond_5

    sget-boolean v0, Lcom/android/server/ssrm/N;->cN:Z

    if-eqz v0, :cond_6

    :cond_5
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cO:Z

    :cond_6
    const-string v0, "kam"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cP:Z

    :cond_7
    const-string v0, "ka"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cT:Z

    :cond_8
    const-string v0, "kf"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cV:Z

    :cond_9
    const-string v0, "kq"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cU:Z

    :cond_a
    sget-boolean v0, Lcom/android/server/ssrm/N;->cT:Z

    if-nez v0, :cond_b

    sget-boolean v0, Lcom/android/server/ssrm/N;->cU:Z

    if-nez v0, :cond_b

    sget-boolean v0, Lcom/android/server/ssrm/N;->cV:Z

    if-eqz v0, :cond_c

    :cond_b
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cW:Z

    :cond_c
    const-string v0, "tf"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cY:Z

    :cond_d
    const-string v0, "ta"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cX:Z

    :cond_e
    sget-boolean v0, Lcom/android/server/ssrm/N;->cY:Z

    if-nez v0, :cond_f

    sget-boolean v0, Lcom/android/server/ssrm/N;->cX:Z

    if-eqz v0, :cond_10

    :cond_f
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cZ:Z

    :cond_10
    const-string v0, "zq"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->db:Z

    :cond_11
    const-string v0, "zl"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->da:Z

    :cond_12
    sget-boolean v0, Lcom/android/server/ssrm/N;->db:Z

    if-nez v0, :cond_13

    sget-boolean v0, Lcom/android/server/ssrm/N;->da:Z

    if-eqz v0, :cond_14

    :cond_13
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dc:Z

    :cond_14
    const-string v0, "sa"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dd:Z

    :cond_15
    const-string v0, "sf"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->de:Z

    :cond_16
    sget-boolean v0, Lcom/android/server/ssrm/N;->dd:Z

    if-nez v0, :cond_17

    sget-boolean v0, Lcom/android/server/ssrm/N;->de:Z

    if-eqz v0, :cond_18

    :cond_17
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->df:Z

    :cond_18
    const-string v0, "ja"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cI:Z

    :cond_19
    const-string v0, "jf"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cJ:Z

    :cond_1a
    sget-boolean v0, Lcom/android/server/ssrm/N;->cI:Z

    if-nez v0, :cond_1b

    sget-boolean v0, Lcom/android/server/ssrm/N;->cJ:Z

    if-eqz v0, :cond_1c

    :cond_1b
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cK:Z

    :cond_1c
    const-string v0, "heat3g"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dS:Z

    :cond_1d
    const-string v0, "garda"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dY:Z

    :cond_1e
    const-string v0, "heatlte"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dT:Z

    :cond_1f
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    if-eqz v0, :cond_5c

    sget-boolean v0, Lcom/android/server/ssrm/N;->cM:Z

    if-eqz v0, :cond_20

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "lt03"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_20

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->di:Z

    :cond_20
    sget-boolean v0, Lcom/android/server/ssrm/N;->cN:Z

    if-eqz v0, :cond_21

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "lt03"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_21

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dj:Z

    :cond_21
    sget-boolean v0, Lcom/android/server/ssrm/N;->di:Z

    if-nez v0, :cond_22

    sget-boolean v0, Lcom/android/server/ssrm/N;->dj:Z

    if-eqz v0, :cond_69

    :cond_22
    move v0, v2

    :goto_2
    sput-boolean v0, Lcom/android/server/ssrm/N;->dh:Z

    sget-boolean v0, Lcom/android/server/ssrm/N;->cM:Z

    if-eqz v0, :cond_23

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "vienna"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_23

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->ds:Z

    :cond_23
    sget-boolean v0, Lcom/android/server/ssrm/N;->cN:Z

    if-eqz v0, :cond_24

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "vienna"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_24

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dt:Z

    :cond_24
    sget-boolean v0, Lcom/android/server/ssrm/N;->ds:Z

    if-nez v0, :cond_25

    sget-boolean v0, Lcom/android/server/ssrm/N;->dt:Z

    if-eqz v0, :cond_6a

    :cond_25
    move v0, v2

    :goto_3
    sput-boolean v0, Lcom/android/server/ssrm/N;->dr:Z

    sget-boolean v0, Lcom/android/server/ssrm/N;->cM:Z

    if-eqz v0, :cond_26

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "v2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_26

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dv:Z

    :cond_26
    sget-boolean v0, Lcom/android/server/ssrm/N;->cN:Z

    if-eqz v0, :cond_27

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "v2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_27

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dw:Z

    :cond_27
    sget-boolean v0, Lcom/android/server/ssrm/N;->dv:Z

    if-nez v0, :cond_28

    sget-boolean v0, Lcom/android/server/ssrm/N;->dw:Z

    if-eqz v0, :cond_6b

    :cond_28
    move v0, v2

    :goto_4
    sput-boolean v0, Lcom/android/server/ssrm/N;->du:Z

    sget-boolean v0, Lcom/android/server/ssrm/N;->cM:Z

    if-eqz v0, :cond_29

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "picasso"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_29

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dE:Z

    :cond_29
    sget-boolean v0, Lcom/android/server/ssrm/N;->cN:Z

    if-eqz v0, :cond_2a

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "picasso"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2a

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dF:Z

    :cond_2a
    sget-boolean v0, Lcom/android/server/ssrm/N;->dE:Z

    if-nez v0, :cond_2b

    sget-boolean v0, Lcom/android/server/ssrm/N;->dF:Z

    if-eqz v0, :cond_6c

    :cond_2b
    move v0, v2

    :goto_5
    sput-boolean v0, Lcom/android/server/ssrm/N;->dD:Z

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "millet"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2c

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dG:Z

    :cond_2c
    sget-boolean v0, Lcom/android/server/ssrm/N;->dG:Z

    if-eqz v0, :cond_2d

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "att"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2d

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dH:Z

    :cond_2d
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "matisse"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2e

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dI:Z

    :cond_2e
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "berluti"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2f

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dJ:Z

    :cond_2f
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "mega23g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_30

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dK:Z

    :cond_30
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "mega2lte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_31

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dL:Z

    :cond_31
    sget-boolean v0, Lcom/android/server/ssrm/N;->cM:Z

    if-eqz v0, :cond_32

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "chagall"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_32

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dx:Z

    :cond_32
    sget-boolean v0, Lcom/android/server/ssrm/N;->cN:Z

    if-eqz v0, :cond_33

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "chagall"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_33

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dy:Z

    :cond_33
    sget-boolean v0, Lcom/android/server/ssrm/N;->cX:Z

    if-eqz v0, :cond_34

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "chagallh"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_34

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dz:Z

    :cond_34
    sget-boolean v0, Lcom/android/server/ssrm/N;->cM:Z

    if-eqz v0, :cond_35

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "klimt"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_35

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dA:Z

    :cond_35
    sget-boolean v0, Lcom/android/server/ssrm/N;->cN:Z

    if-eqz v0, :cond_36

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "klimt"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_36

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dB:Z

    :cond_36
    sget-boolean v0, Lcom/android/server/ssrm/N;->cN:Z

    if-eqz v0, :cond_37

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "mondrian"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_37

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dC:Z

    :cond_37
    sget-boolean v0, Lcom/android/server/ssrm/N;->cN:Z

    if-eqz v0, :cond_38

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "flte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_38

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dN:Z

    :cond_38
    sget-boolean v0, Lcom/android/server/ssrm/N;->cN:Z

    if-eqz v0, :cond_39

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "ks01"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_39

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dR:Z

    :cond_39
    sget-boolean v0, Lcom/android/server/ssrm/N;->cJ:Z

    if-eqz v0, :cond_3a

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "jfve"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3a

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cL:Z

    :cond_3a
    const-string v0, "vastalte"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3b

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->ev:Z

    :cond_3b
    sget-boolean v0, Lcom/android/server/ssrm/N;->eq:Z

    if-eqz v0, :cond_3c

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "_km_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3c

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cQ:Z

    :cond_3c
    sget-boolean v0, Lcom/android/server/ssrm/N;->ep:Z

    if-eqz v0, :cond_3d

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "_km3g_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3d

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cR:Z

    :cond_3d
    sget-boolean v0, Lcom/android/server/ssrm/N;->cQ:Z

    if-nez v0, :cond_3e

    sget-boolean v0, Lcom/android/server/ssrm/N;->cR:Z

    if-eqz v0, :cond_3f

    :cond_3e
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->cS:Z

    :cond_3f
    const-string v0, "VZW"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_40

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->ee:Z

    :cond_40
    const-string v0, "CHU"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_41

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->ef:Z

    :cond_41
    const-string v0, "CHM"

    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_42

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->eg:Z

    :cond_42
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "js01"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_43

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dq:Z

    :cond_43
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "ms013g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_44

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dO:Z

    :cond_44
    sget-boolean v0, Lcom/android/server/ssrm/N;->cJ:Z

    if-eqz v0, :cond_45

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "refresh"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_45

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dP:Z

    :cond_45
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "d2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_46

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dQ:Z

    :cond_46
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "melius"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_47

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dk:Z

    :cond_47
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "_jm_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_48

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dl:Z

    :cond_48
    sget-boolean v0, Lcom/android/server/ssrm/N;->ep:Z

    if-eqz v0, :cond_49

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "lt02"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_49

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dm:Z

    :cond_49
    sget-boolean v0, Lcom/android/server/ssrm/N;->ep:Z

    if-eqz v0, :cond_4a

    const-string v0, "golden"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4a

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dn:Z

    :cond_4a
    sget-boolean v0, Lcom/android/server/ssrm/N;->es:Z

    if-eqz v0, :cond_4b

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "_kanas_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4b

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->do:Z

    :cond_4b
    sget-boolean v0, Lcom/android/server/ssrm/N;->cP:Z

    if-eqz v0, :cond_4c

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "m2"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4c

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dZ:Z

    :cond_4c
    const-string v0, "t0"

    sget-object v3, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4d

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "_t0_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4d

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dU:Z

    :cond_4d
    sget-boolean v0, Lcom/android/server/ssrm/N;->er:Z

    if-eqz v0, :cond_4e

    new-instance v0, Ljava/io/File;

    const-string v3, "/sys/power/cpufreq_table"

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/N;->dV:Z

    :cond_4e
    sget-boolean v0, Lcom/android/server/ssrm/N;->er:Z

    if-eqz v0, :cond_4f

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "degas"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4f

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dW:Z

    :cond_4f
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "agera"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_50

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dX:Z

    :cond_50
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "kcat6"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_51

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->dg:Z

    :cond_51
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "ja"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_52

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "jf"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_53

    :cond_52
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->el:Z

    :cond_53
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "_ha_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_54

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "_hf_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_55

    :cond_54
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->em:Z

    :cond_55
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "ta"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_56

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "tf"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_57

    :cond_56
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->en:Z

    :cond_57
    sget-boolean v0, Lcom/android/server/ssrm/N;->cY:Z

    if-eqz v0, :cond_58

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "lentis"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_58

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->ea:Z

    :cond_58
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "_a3_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_59

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->eb:Z

    :cond_59
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "_a5_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5a

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->ec:Z

    :cond_5a
    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "_a7_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5b

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->ed:Z

    :cond_5b
    const-string v0, "tablet"

    const-string v3, "ro.build.characteristics"

    const-string v4, "default"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/N;->dM:Z

    :cond_5c
    new-instance v0, Ljava/io/File;

    const-string v3, "/sys/class/sec/sec-thermistor/temperature"

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/io/File;

    const-string v4, "/sys/devices/platform/sec-thermistor/temperature"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_6d

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v4

    if-eqz v4, :cond_6d

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/N;->eh:Ljava/lang/String;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->ei:Z

    :goto_6
    sget-boolean v0, Lcom/android/server/ssrm/N;->cW:Z

    if-nez v0, :cond_5d

    sget-boolean v0, Lcom/android/server/ssrm/N;->cZ:Z

    if-nez v0, :cond_5d

    sget-boolean v0, Lcom/android/server/ssrm/N;->df:Z

    if-nez v0, :cond_5d

    sget-boolean v0, Lcom/android/server/ssrm/N;->dc:Z

    if-eqz v0, :cond_6f

    :cond_5d
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->ek:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :goto_7
    sget-boolean v0, Lcom/android/server/ssrm/N;->cO:Z

    if-nez v0, :cond_5e

    sget-boolean v0, Lcom/android/server/ssrm/N;->cW:Z

    if-nez v0, :cond_5e

    sget-boolean v0, Lcom/android/server/ssrm/N;->df:Z

    if-nez v0, :cond_5e

    sget-boolean v0, Lcom/android/server/ssrm/N;->cZ:Z

    if-nez v0, :cond_5e

    sget-boolean v0, Lcom/android/server/ssrm/N;->cP:Z

    if-nez v0, :cond_5e

    sget-boolean v0, Lcom/android/server/ssrm/N;->eb:Z

    if-nez v0, :cond_5e

    sget-boolean v0, Lcom/android/server/ssrm/N;->ec:Z

    if-nez v0, :cond_5e

    sget-boolean v0, Lcom/android/server/ssrm/N;->ed:Z

    if-eqz v0, :cond_70

    :cond_5e
    move v0, v2

    :goto_8
    sput-boolean v0, Lcom/android/server/ssrm/N;->ew:Z

    sget-boolean v0, Lcom/android/server/ssrm/N;->cO:Z

    if-nez v0, :cond_5f

    sget-boolean v0, Lcom/android/server/ssrm/N;->cK:Z

    if-nez v0, :cond_5f

    sget-boolean v0, Lcom/android/server/ssrm/N;->dk:Z

    if-eqz v0, :cond_71

    :cond_5f
    move v0, v2

    :goto_9
    sput-boolean v0, Lcom/android/server/ssrm/N;->ex:Z

    sget-boolean v0, Lcom/android/server/ssrm/N;->cJ:Z

    if-nez v0, :cond_60

    sget-boolean v0, Lcom/android/server/ssrm/N;->dk:Z

    if-eqz v0, :cond_72

    :cond_60
    move v0, v2

    :goto_a
    sput-boolean v0, Lcom/android/server/ssrm/N;->ey:Z

    sget-boolean v0, Lcom/android/server/ssrm/N;->cO:Z

    sput-boolean v0, Lcom/android/server/ssrm/N;->ez:Z

    sget-boolean v0, Lcom/android/server/ssrm/N;->cW:Z

    if-nez v0, :cond_61

    sget-boolean v0, Lcom/android/server/ssrm/N;->cZ:Z

    if-nez v0, :cond_61

    sget-boolean v0, Lcom/android/server/ssrm/N;->df:Z

    if-eqz v0, :cond_73

    :cond_61
    move v0, v2

    :goto_b
    sput-boolean v0, Lcom/android/server/ssrm/N;->eA:Z

    sget-boolean v0, Lcom/android/server/ssrm/N;->cO:Z

    if-nez v0, :cond_62

    sget-boolean v0, Lcom/android/server/ssrm/N;->cK:Z

    if-nez v0, :cond_62

    sget-boolean v0, Lcom/android/server/ssrm/N;->dO:Z

    if-eqz v0, :cond_74

    :cond_62
    move v0, v2

    :goto_c
    sput-boolean v0, Lcom/android/server/ssrm/N;->eB:Z

    sput-boolean v2, Lcom/android/server/ssrm/N;->eC:Z

    sget-boolean v0, Lcom/android/server/ssrm/N;->ee:Z

    if-nez v0, :cond_75

    move v0, v2

    :goto_d
    sput-boolean v0, Lcom/android/server/ssrm/N;->eD:Z

    sget-boolean v0, Lcom/android/server/ssrm/N;->ep:Z

    if-nez v0, :cond_63

    sget-boolean v0, Lcom/android/server/ssrm/N;->cT:Z

    if-nez v0, :cond_63

    sget-boolean v0, Lcom/android/server/ssrm/N;->cU:Z

    if-nez v0, :cond_63

    sget-boolean v0, Lcom/android/server/ssrm/N;->dd:Z

    if-nez v0, :cond_63

    sget-boolean v0, Lcom/android/server/ssrm/N;->cS:Z

    if-nez v0, :cond_63

    sget-boolean v0, Lcom/android/server/ssrm/N;->dL:Z

    if-eqz v0, :cond_76

    :cond_63
    move v0, v2

    :goto_e
    sput-boolean v0, Lcom/android/server/ssrm/N;->eE:Z

    sput-boolean v1, Lcom/android/server/ssrm/N;->eF:Z

    sget-boolean v0, Lcom/android/server/ssrm/N;->cW:Z

    if-eqz v0, :cond_64

    sget-boolean v0, Lcom/android/server/ssrm/N;->cE:Z

    if-nez v0, :cond_66

    :cond_64
    sget-boolean v0, Lcom/android/server/ssrm/N;->df:Z

    if-nez v0, :cond_66

    sget-boolean v0, Lcom/android/server/ssrm/N;->cZ:Z

    if-eqz v0, :cond_65

    sget-boolean v0, Lcom/android/server/ssrm/N;->cC:Z

    if-nez v0, :cond_66

    sget-boolean v0, Lcom/android/server/ssrm/N;->cE:Z

    if-nez v0, :cond_66

    :cond_65
    sget-boolean v0, Lcom/android/server/ssrm/N;->ea:Z

    if-eqz v0, :cond_67

    :cond_66
    move v1, v2

    :cond_67
    sput-boolean v1, Lcom/android/server/ssrm/N;->eG:Z

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    sget-object v3, Lcom/android/server/ssrm/N;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "e = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto/16 :goto_7

    :cond_68
    move v0, v1

    goto/16 :goto_1

    :cond_69
    move v0, v1

    goto/16 :goto_2

    :cond_6a
    move v0, v1

    goto/16 :goto_3

    :cond_6b
    move v0, v1

    goto/16 :goto_4

    :cond_6c
    move v0, v1

    goto/16 :goto_5

    :cond_6d
    :try_start_4
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_6e

    invoke-virtual {v3}, Ljava/io/File;->canRead()Z

    move-result v0

    if-eqz v0, :cond_6e

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/N;->eh:Ljava/lang/String;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->ei:Z

    goto/16 :goto_6

    :cond_6e
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/ssrm/N;->ei:Z

    goto/16 :goto_6

    :cond_6f
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/N;->ej:Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_7

    :cond_70
    move v0, v1

    goto/16 :goto_8

    :cond_71
    move v0, v1

    goto/16 :goto_9

    :cond_72
    move v0, v1

    goto/16 :goto_a

    :cond_73
    move v0, v1

    goto/16 :goto_b

    :cond_74
    move v0, v1

    goto/16 :goto_c

    :cond_75
    move v0, v1

    goto/16 :goto_d

    :cond_76
    move v0, v1

    goto/16 :goto_e
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
