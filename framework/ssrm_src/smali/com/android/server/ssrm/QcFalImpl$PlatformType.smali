.class final enum Lcom/android/server/ssrm/QcFalImpl$PlatformType;
.super Ljava/lang/Enum;


# static fields
.field public static final enum kL:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

.field public static final enum kM:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

.field public static final enum kN:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

.field public static final enum kO:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

.field public static final enum kP:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

.field public static final enum kQ:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

.field public static final enum kR:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

.field private static final synthetic kS:[Lcom/android/server/ssrm/QcFalImpl$PlatformType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/android/server/ssrm/QcFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kL:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    const-string v1, "MSM8064"

    invoke-direct {v0, v1, v4}, Lcom/android/server/ssrm/QcFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kM:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    const-string v1, "MSM8226"

    invoke-direct {v0, v1, v5}, Lcom/android/server/ssrm/QcFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kN:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    const-string v1, "MSM8974"

    invoke-direct {v0, v1, v6}, Lcom/android/server/ssrm/QcFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kO:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    const-string v1, "MSM8610"

    invoke-direct {v0, v1, v7}, Lcom/android/server/ssrm/QcFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kP:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    const-string v1, "MSM8084"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/server/ssrm/QcFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kQ:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    const-string v1, "MSM8916"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/server/ssrm/QcFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kR:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kL:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kM:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kN:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kO:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kP:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kQ:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kR:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kS:[Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/ssrm/QcFalImpl$PlatformType;
    .locals 1

    const-class v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    return-object v0
.end method

.method public static values()[Lcom/android/server/ssrm/QcFalImpl$PlatformType;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kS:[Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    invoke-virtual {v0}, [Lcom/android/server/ssrm/QcFalImpl$PlatformType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    return-object v0
.end method
