.class Lcom/android/server/ssrm/p;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/CustomFrequencyManagerService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    iput-boolean v3, v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->aK:Z

    invoke-static {}, Lcom/android/server/ssrm/W;->init()V

    invoke-static {}, Lcom/android/server/ssrm/aL;->bC()V

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->a(Lcom/android/server/ssrm/CustomFrequencyManagerService;)V

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    sget-object v1, Lcom/android/server/ssrm/CustomFrequencyManagerService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    iget-object v2, v2, Lcom/android/server/ssrm/CustomFrequencyManagerService;->at:Landroid/app/IActivityManager;

    invoke-static {v1, v2}, Lcom/android/server/ssrm/ag;->a(Landroid/content/Context;Landroid/app/IActivityManager;)Lcom/android/server/ssrm/ag;

    move-result-object v1

    iput-object v1, v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->aH:Lcom/android/server/ssrm/ag;

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    iget-object v0, v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->aH:Lcom/android/server/ssrm/ag;

    invoke-virtual {v0}, Lcom/android/server/ssrm/ag;->bg()V

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    iget-object v0, v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->aH:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v1, Lcom/android/server/ssrm/CustomFrequencyManagerService;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    iget-boolean v0, v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->mScreenOn:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    iget-object v0, v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->aH:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v1, Lcom/android/server/ssrm/CustomFrequencyManagerService;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    iget-boolean v0, v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->aJ:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    iget-object v0, v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->aH:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v1, Lcom/android/server/ssrm/CustomFrequencyManagerService;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.intent.action.USER_PRESENT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    iget-object v0, v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->aH:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v1, Lcom/android/server/ssrm/CustomFrequencyManagerService;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    iput-boolean v3, v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->mScreenOn:Z

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->b(Lcom/android/server/ssrm/CustomFrequencyManagerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->c(Lcom/android/server/ssrm/CustomFrequencyManagerService;)Landroid/app/KeyguardManager;

    move-result-object v0

    if-nez v0, :cond_3

    iget-object v1, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    sget-object v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->mContext:Landroid/content/Context;

    const-string v2, "keyguard"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-static {v1, v0}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->a(Lcom/android/server/ssrm/CustomFrequencyManagerService;Landroid/app/KeyguardManager;)Landroid/app/KeyguardManager;

    :cond_3
    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->c(Lcom/android/server/ssrm/CustomFrequencyManagerService;)Landroid/app/KeyguardManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    if-nez v0, :cond_0

    sput-boolean v3, Lcom/android/internal/os/BatteryStatsDumper;->sScreenOn:Z

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0, v3}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->a(Lcom/android/server/ssrm/CustomFrequencyManagerService;Z)Z

    goto :goto_0

    :cond_4
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    iput-boolean v2, v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->mScreenOn:Z

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    iput-boolean v2, v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->aJ:Z

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->b(Lcom/android/server/ssrm/CustomFrequencyManagerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    sput-boolean v2, Lcom/android/internal/os/BatteryStatsDumper;->sScreenOn:Z

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->d(Lcom/android/server/ssrm/CustomFrequencyManagerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0, v3}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->b(Lcom/android/server/ssrm/CustomFrequencyManagerService;Z)V

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0, v2}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->a(Lcom/android/server/ssrm/CustomFrequencyManagerService;Z)Z

    goto/16 :goto_0

    :cond_5
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    iput-boolean v3, v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->aJ:Z

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->b(Lcom/android/server/ssrm/CustomFrequencyManagerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    sput-boolean v3, Lcom/android/internal/os/BatteryStatsDumper;->sScreenOn:Z

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0, v3}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->b(Lcom/android/server/ssrm/CustomFrequencyManagerService;Z)V

    iget-object v0, p0, Lcom/android/server/ssrm/p;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0, v3}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->a(Lcom/android/server/ssrm/CustomFrequencyManagerService;Z)Z

    goto/16 :goto_0
.end method
