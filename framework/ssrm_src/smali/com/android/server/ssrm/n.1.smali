.class public Lcom/android/server/ssrm/n;
.super Ljava/lang/Object;


# instance fields
.field private final S:[Ljava/lang/Object;

.field private T:I

.field private U:I

.field private pointer:I

.field private size:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/android/server/ssrm/n;->size:I

    iput v0, p0, Lcom/android/server/ssrm/n;->T:I

    iput v0, p0, Lcom/android/server/ssrm/n;->U:I

    iput v0, p0, Lcom/android/server/ssrm/n;->pointer:I

    iput p1, p0, Lcom/android/server/ssrm/n;->T:I

    new-array v0, p1, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/server/ssrm/n;->S:[Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public declared-synchronized f()[Ljava/lang/String;
    .locals 5

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/server/ssrm/n;->size:I

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v0, p0, Lcom/android/server/ssrm/n;->size:I

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/n;->S:[Ljava/lang/Object;

    iget v3, p0, Lcom/android/server/ssrm/n;->U:I

    add-int/2addr v3, v1

    iget v4, p0, Lcom/android/server/ssrm/n;->T:I

    rem-int/2addr v3, v4

    aget-object v0, v0, v3

    check-cast v0, Ljava/lang/String;

    aput-object v0, v2, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    monitor-exit p0

    return-object v2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized put(Ljava/lang/Object;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/server/ssrm/n;->S:[Ljava/lang/Object;

    iget v1, p0, Lcom/android/server/ssrm/n;->pointer:I

    aput-object p1, v0, v1

    iget v0, p0, Lcom/android/server/ssrm/n;->pointer:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/android/server/ssrm/n;->T:I

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/ssrm/n;->pointer:I

    iget v0, p0, Lcom/android/server/ssrm/n;->size:I

    iget v1, p0, Lcom/android/server/ssrm/n;->T:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/server/ssrm/n;->U:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/android/server/ssrm/n;->T:I

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/ssrm/n;->U:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/android/server/ssrm/n;->size:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/ssrm/n;->size:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
