.class Lcom/android/server/ssrm/M;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic cv:Lcom/android/server/ssrm/J;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/J;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/M;->cv:Lcom/android/server/ssrm/J;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    sget-boolean v0, Lcom/android/server/ssrm/G;->bQ:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/server/ssrm/J;->TAG:Ljava/lang/String;

    const-string v1, "report_rate,1"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v0, Lcom/android/server/ssrm/aT;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "dev.ssrm.report_rate"

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/server/ssrm/M;->cv:Lcom/android/server/ssrm/J;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/server/ssrm/J;->ct:Z

    return-void

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/J;->TAG:Ljava/lang/String;

    const-string v1, "report_rate,0"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v0, Lcom/android/server/ssrm/aT;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "dev.ssrm.report_rate"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
