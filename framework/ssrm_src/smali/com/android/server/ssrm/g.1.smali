.class public Lcom/android/server/ssrm/g;
.super Lcom/android/server/ssrm/e;


# instance fields
.field I:I

.field J:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/android/server/ssrm/e;-><init>(Ljava/lang/String;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/ssrm/g;->J:I

    iput p2, p0, Lcom/android/server/ssrm/g;->I:I

    return-void
.end method


# virtual methods
.method public acquire(I)V
    .locals 8

    sget-object v0, Lcom/android/server/ssrm/d;->z:[I

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/d;->TAG:Ljava/lang/String;

    const-string v1, "CfmsCpuCoreNumMinLock.acquire:: core table does not exist."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/server/ssrm/g;->H:Landroid/os/CustomFrequencyManager$FrequencyRequest;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/android/server/ssrm/g;->J:I

    if-eq v0, p1, :cond_2

    :cond_1
    sget-object v1, Lcom/android/server/ssrm/d;->D:Landroid/os/CustomFrequencyManager;

    const/4 v2, 0x4

    iget v3, p0, Lcom/android/server/ssrm/g;->I:I

    int-to-long v4, p1

    iget-object v6, p0, Lcom/android/server/ssrm/g;->mTag:Ljava/lang/String;

    sget-object v7, Lcom/android/server/ssrm/d;->C:Landroid/content/Context;

    invoke-virtual/range {v1 .. v7}, Landroid/os/CustomFrequencyManager;->newFrequencyRequest(IIJLjava/lang/String;Landroid/content/Context;)Landroid/os/CustomFrequencyManager$FrequencyRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/g;->H:Landroid/os/CustomFrequencyManager$FrequencyRequest;

    iput p1, p0, Lcom/android/server/ssrm/g;->J:I

    :cond_2
    invoke-virtual {p0}, Lcom/android/server/ssrm/g;->b()V

    goto :goto_0
.end method

.method release()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/server/ssrm/g;->c()V

    return-void
.end method
