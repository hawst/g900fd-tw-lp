.class public Lcom/android/server/ssrm/GenericFalImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/server/ssrm/bd;


# static fields
.field private static final DEBUG:Z

.field private static final TAG:Ljava/lang/String;

.field private static final UTF8:Ljava/lang/String; = "UTF-8"

.field public static eH:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

.field public static eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field private static eJ:Lcom/android/server/ssrm/P;

.field public static eK:Z

.field public static eL:Z

.field private static final eM:[I

.field private static final eN:[I

.field private static final eO:[I

.field private static final eP:[I

.field private static final eQ:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x6

    const/4 v3, 0x4

    const/4 v2, 0x0

    const-class v0, Lcom/android/server/ssrm/GenericFalImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/GenericFalImpl;->DEBUG:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    sput-boolean v2, Lcom/android/server/ssrm/GenericFalImpl;->eK:Z

    sput-boolean v2, Lcom/android/server/ssrm/GenericFalImpl;->eL:Z

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eM:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eN:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eO:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eP:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eQ:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x214
        0x1e0
        0x15e
        0x10a
        0xb1
    .end array-data

    :array_1
    .array-data 4
        0x3
        0x2
        0x1
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x6b79c
        0x47964
        0x47938
        0x2b030
        0x23ecb
        0x1ae1e
    .end array-data

    :array_3
    .array-data 4
        0x61b48
        0x413c0
        0x41398
        0x271a0
        0x2080d
        0x18704
    .end array-data

    :array_4
    .array-data 4
        0x4c2c0
        0x3e800
        0x1f400
        0x14d55
    .end array-data
.end method

.method constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-boolean v0, Lcom/android/server/ssrm/N;->ep:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;->eT:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eH:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    :goto_0
    const-string v0, "ro.board.platform"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "msm8960"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->eY:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    :goto_1
    new-instance v0, Lcom/android/server/ssrm/P;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eH:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    invoke-direct {v0, v1, v2}, Lcom/android/server/ssrm/P;-><init>(Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;Lcom/android/server/ssrm/GenericFalImpl$PlatformType;)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    return-void

    :cond_0
    sget-boolean v0, Lcom/android/server/ssrm/N;->eq:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;->eU:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eH:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    goto :goto_0

    :cond_1
    sget-boolean v0, Lcom/android/server/ssrm/N;->er:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;->eV:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eH:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;->eS:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eH:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    goto :goto_0

    :cond_3
    const-string v1, "msm8226"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->eZ:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    goto :goto_1

    :cond_4
    const-string v1, "msm8974"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fa:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    goto :goto_1

    :cond_5
    const-string v1, "ja"

    sget-object v2, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fb:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    goto :goto_1

    :cond_6
    const-string v1, "ha"

    sget-object v2, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fd:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    goto :goto_1

    :cond_7
    const-string v1, "ka"

    sget-object v2, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fe:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    goto :goto_1

    :cond_8
    const-string v1, "kq"

    sget-object v2, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "sa"

    sget-object v2, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_9
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->ff:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    goto/16 :goto_1

    :cond_a
    const-string v1, "ta"

    sget-object v2, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fk:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    goto/16 :goto_1

    :cond_b
    const-string v1, "zl"

    sget-object v2, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fl:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    goto/16 :goto_1

    :cond_c
    const-string v1, "kam"

    sget-object v2, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fg:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    goto/16 :goto_1

    :cond_d
    const-string v1, "exynos4"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v0, "pp"

    sget-object v1, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fj:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    goto/16 :goto_1

    :cond_e
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fc:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    goto/16 :goto_1

    :cond_f
    const-string v1, "exynos3"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fh:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    goto/16 :goto_1

    :cond_10
    const-string v1, "sc8830"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fi:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    goto/16 :goto_1

    :cond_11
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->eX:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    goto/16 :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private static aq()Z
    .locals 7

    const/4 v1, 0x0

    const-string v2, "/sys/devices/system/cpu/busfreq/curr_freq"

    const/4 v4, 0x0

    const-string v0, ""

    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, v2}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    if-eqz v3, :cond_0

    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    :goto_0
    sget-object v3, Lcom/android/server/ssrm/GenericFalImpl;->eO:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget v5, v3, v2

    if-eqz v0, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v0, 0x1

    :goto_2
    return v0

    :catch_0
    move-exception v2

    sget-object v3, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isPegaPrime:: e = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/android/server/ssrm/GenericFalImpl;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v2

    move-object v3, v4

    :goto_3
    :try_start_3
    sget-object v4, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "isPegaPrime:: e = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/android/server/ssrm/GenericFalImpl;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v3, :cond_0

    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    :catch_2
    move-exception v2

    sget-object v3, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isPegaPrime:: e = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/android/server/ssrm/GenericFalImpl;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    move-object v3, v4

    :goto_4
    if-eqz v3, :cond_1

    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_1
    :goto_5
    throw v0

    :catch_3
    move-exception v1

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isPegaPrime:: e = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/android/server/ssrm/GenericFalImpl;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    :cond_3
    move v0, v1

    goto/16 :goto_2

    :catchall_1
    move-exception v0

    goto :goto_4

    :catch_4
    move-exception v2

    goto :goto_3
.end method

.method private static ar()[I
    .locals 8

    const/4 v1, 0x0

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    const-string v2, "parseSystemBusTable"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/GenericFalImpl;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v0, v0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/ssrm/GenericFalImpl;->k(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v2, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    sget-object v3, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v3, v3, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    if-nez v2, :cond_1

    if-eqz v2, :cond_0

    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_1
    return-object v1

    :catch_0
    move-exception v0

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseSystemBusTable:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    if-nez v0, :cond_2

    if-eqz v2, :cond_0

    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseSystemBusTable:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    :try_start_4
    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v0, v4

    new-array v0, v0, [I

    const/4 v3, 0x0

    :goto_2
    array-length v5, v4

    if-ge v3, v5, :cond_8

    sget-object v5, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v6, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fe:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v5, v6, :cond_3

    sget-object v5, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v6, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->ff:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v5, v6, :cond_3

    sget-object v5, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v6, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fk:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v5, v6, :cond_3

    sget-object v5, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v6, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fl:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v5, v6, :cond_3

    sget-object v5, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v6, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fg:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v5, v6, :cond_3

    sget-object v5, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v6, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fh:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v5, v6, :cond_3

    sget-object v5, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v6, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fi:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-ne v5, v6, :cond_4

    :cond_3
    array-length v5, v4

    sub-int/2addr v5, v3

    add-int/lit8 v5, v5, -0x1

    aget-object v5, v4, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v0, v3

    :goto_3
    sget-object v5, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "parseSystemBusTable:: supportedFrequency = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v0, v3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/server/ssrm/GenericFalImpl;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    aget-object v5, v4, v3

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v0, v3
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_3

    :catch_2
    move-exception v0

    :goto_4
    :try_start_5
    sget-boolean v3, Lcom/android/server/ssrm/GenericFalImpl;->DEBUG:Z

    if-eqz v3, :cond_5

    sget-object v3, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseSystemBusTable:: failed by Exception, msg = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :cond_5
    if-eqz v2, :cond_6

    :try_start_6
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    :cond_6
    move-object v0, v1

    :cond_7
    :goto_5
    move-object v1, v0

    goto/16 :goto_1

    :cond_8
    if-eqz v2, :cond_7

    :try_start_7
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_5

    :catch_3
    move-exception v1

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseSystemBusTable:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :catch_4
    move-exception v0

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseSystemBusTable:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_5

    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_6
    if-eqz v2, :cond_9

    :try_start_8
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    :cond_9
    :goto_7
    throw v0

    :catch_5
    move-exception v1

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseSystemBusTable:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    :catchall_1
    move-exception v0

    goto :goto_6

    :catch_6
    move-exception v0

    move-object v2, v1

    goto/16 :goto_4

    :cond_a
    move-object v2, v1

    goto/16 :goto_0
.end method

.method private static as()[I
    .locals 8

    const/4 v1, 0x0

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    const-string v2, "parseCpuCoreTable"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/GenericFalImpl;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v0, v0, Lcom/android/server/ssrm/P;->fw:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/ssrm/GenericFalImpl;->k(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance v2, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    sget-object v3, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v3, v3, Lcom/android/server/ssrm/P;->fw:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    if-nez v2, :cond_1

    if-eqz v2, :cond_0

    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_1
    return-object v1

    :catch_0
    move-exception v0

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseCpuCoreTable:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    if-nez v0, :cond_2

    if-eqz v2, :cond_0

    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseCpuCoreTable:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    :try_start_4
    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v0, v4

    new-array v0, v0, [I

    const/4 v3, 0x0

    :goto_2
    array-length v5, v4

    if-ge v3, v5, :cond_3

    aget-object v5, v4, v3

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v0, v3

    sget-object v5, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "parseCpuCoreTable:: supportedCpuCore = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v0, v3

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/server/ssrm/GenericFalImpl;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    if-eqz v2, :cond_4

    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :cond_4
    :goto_3
    move-object v1, v0

    goto/16 :goto_1

    :catch_2
    move-exception v1

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseCpuCoreTable:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :catch_3
    move-exception v0

    move-object v2, v1

    :goto_4
    :try_start_6
    sget-boolean v3, Lcom/android/server/ssrm/GenericFalImpl;->DEBUG:Z

    if-eqz v3, :cond_5

    sget-object v3, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseCpuCoreTable:: failed by Exception, msg = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :cond_5
    if-eqz v2, :cond_6

    :try_start_7
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :cond_6
    move-object v0, v1

    goto :goto_3

    :catch_4
    move-exception v0

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseCpuCoreTable:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_3

    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_5
    if-eqz v2, :cond_7

    :try_start_8
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    :cond_7
    :goto_6
    throw v0

    :catch_5
    move-exception v1

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseCpuCoreTable:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    :catchall_1
    move-exception v0

    goto :goto_5

    :catch_6
    move-exception v0

    goto :goto_4

    :cond_8
    move-object v2, v1

    goto/16 :goto_0
.end method

.method private static at()[I
    .locals 6

    const/4 v0, 0x0

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v2, v2, Lcom/android/server/ssrm/P;->fw:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v2, v1, 0x1

    const/16 v1, 0x20

    if-gt v2, v1, :cond_0

    new-array v1, v2, [I

    const/4 v0, 0x0

    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_2

    sub-int v3, v2, v0

    aput v3, v1, v0

    sget-object v3, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "makeCpuCoreTable:: table["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, v1, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private static au()[I
    .locals 9

    const/4 v2, 0x0

    const/4 v4, 0x0

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    const-string v1, "parseGpuFreqTable"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/GenericFalImpl;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v0, v0, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/ssrm/GenericFalImpl;->k(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    new-instance v3, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v1, v1, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    if-nez v3, :cond_1

    if-eqz v3, :cond_0

    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_0
    :goto_1
    return-object v2

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseGpuFreqTable:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    if-nez v0, :cond_2

    if-eqz v3, :cond_0

    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseGpuFreqTable:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    :try_start_4
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    move v1, v4

    move v0, v4

    :goto_2
    array-length v6, v5

    if-ge v1, v6, :cond_4

    aget-object v6, v5, v1

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    add-int/lit8 v0, v0, 0x1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    array-length v1, v5

    if-eq v0, v1, :cond_6

    move v1, v4

    :goto_3
    array-length v6, v5

    if-ge v1, v6, :cond_6

    aget-object v6, v5, v1

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_5

    add-int/lit8 v6, v1, 0x1

    array-length v7, v5

    if-ge v6, v7, :cond_5

    add-int/lit8 v6, v1, 0x1

    aget-object v6, v5, v6

    aput-object v6, v5, v1

    add-int/lit8 v6, v1, 0x1

    const-string v7, ""

    aput-object v7, v5, v6

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_6
    new-array v1, v0, [I

    const/4 v6, 0x0

    aget-object v6, v5, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v7, v0, -0x1

    aget-object v7, v5, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    if-ge v6, v7, :cond_7

    :goto_4
    if-ge v4, v0, :cond_8

    sub-int v6, v0, v4

    add-int/lit8 v6, v6, -0x1

    aget-object v6, v5, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v1, v4

    sget-object v6, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "parseGpuFreqTable:: supportedFrequency = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget v8, v1, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/server/ssrm/GenericFalImpl;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_7
    :goto_5
    if-ge v4, v0, :cond_8

    aget-object v6, v5, v4

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v1, v4

    sget-object v6, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "parseGpuFreqTable:: supportedFrequency = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    aget v8, v1, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/android/server/ssrm/GenericFalImpl;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_8
    if-eqz v3, :cond_9

    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :cond_9
    move-object v0, v1

    :goto_6
    move-object v2, v0

    goto/16 :goto_1

    :catch_2
    move-exception v0

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseGpuFreqTable:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_6

    :catch_3
    move-exception v0

    move-object v1, v2

    :goto_7
    :try_start_6
    sget-boolean v3, Lcom/android/server/ssrm/GenericFalImpl;->DEBUG:Z

    if-eqz v3, :cond_a

    sget-object v3, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseGpuFreqTable:: failed by Exception, msg = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :cond_a
    if-eqz v1, :cond_b

    :try_start_7
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :cond_b
    move-object v0, v2

    goto :goto_6

    :catch_4
    move-exception v0

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseGpuFreqTable:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    goto :goto_6

    :catchall_0
    move-exception v0

    move-object v3, v2

    :goto_8
    if-eqz v3, :cond_c

    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    :cond_c
    :goto_9
    throw v0

    :catch_5
    move-exception v1

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "parseGpuFreqTable:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    :catchall_1
    move-exception v0

    goto :goto_8

    :catchall_2
    move-exception v0

    move-object v3, v1

    goto :goto_8

    :catch_6
    move-exception v0

    move-object v1, v3

    goto :goto_7

    :cond_d
    move-object v3, v2

    goto/16 :goto_0
.end method

.method public static av()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v0, v0, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    return-object v0
.end method

.method private static d(II)I
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fb:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-ne v1, v2, :cond_1

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x4

    if-ne p0, v1, :cond_1

    :cond_0
    sparse-switch p1, :sswitch_data_0

    :cond_1
    :goto_0
    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fi:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-ne v1, v2, :cond_2

    const/16 v1, 0x8

    if-ne p0, v1, :cond_2

    packed-switch p1, :pswitch_data_0

    move p1, v0

    :cond_2
    :goto_1
    return p1

    :sswitch_0
    move p1, v0

    goto :goto_0

    :pswitch_0
    const/4 p1, 0x1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x214 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method private static k(Ljava/lang/String;)Z
    .locals 2

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    sget-boolean v0, Lcom/android/server/ssrm/GenericFalImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method


# virtual methods
.method public c(II)Z
    .locals 8

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v4, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fc:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v2, v4, :cond_1

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v4, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fi:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-ne v2, v4, :cond_9

    :cond_1
    new-instance v2, Ljava/io/File;

    sget-object v4, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v4, v4, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :cond_2
    :goto_1
    invoke-static {p1, p2}, Lcom/android/server/ssrm/GenericFalImpl;->d(II)I

    move-result v5

    if-eqz v2, :cond_3

    sget-object v4, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "modifyToValues:: path = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", value = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/android/server/ssrm/GenericFalImpl;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    if-eqz v2, :cond_0

    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v4, :cond_4

    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_4
    move v0, v1

    goto :goto_0

    :pswitch_2
    new-instance v2, Ljava/io/File;

    sget-object v4, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v4, v4, Lcom/android/server/ssrm/P;->fv:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_3
    new-instance v2, Ljava/io/File;

    sget-object v4, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v4, v4, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_4
    new-instance v2, Ljava/io/File;

    sget-object v4, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v4, v4, Lcom/android/server/ssrm/P;->fn:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_5
    new-instance v2, Ljava/io/File;

    sget-object v4, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v4, v4, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_6
    new-instance v2, Ljava/io/File;

    sget-object v4, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v4, v4, Lcom/android/server/ssrm/P;->fz:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_7
    new-instance v2, Ljava/io/File;

    sget-object v4, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v4, v4, Lcom/android/server/ssrm/P;->fA:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_8
    new-instance v2, Ljava/io/File;

    sget-object v4, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v4, v4, Lcom/android/server/ssrm/P;->fs:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v4, 0x28

    if-eq p2, v4, :cond_5

    const/16 v4, 0x33

    if-eq p2, v4, :cond_5

    const/16 v4, 0x32

    if-ne p2, v4, :cond_6

    :cond_5
    move p2, v1

    goto/16 :goto_1

    :cond_6
    const/16 v4, 0x3c

    if-ne p2, v4, :cond_2

    move p2, v0

    goto/16 :goto_1

    :pswitch_9
    new-instance v2, Ljava/io/File;

    sget-object v4, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v4, v4, Lcom/android/server/ssrm/P;->fF:Ljava/lang/String;

    invoke-direct {v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move p2, v1

    goto/16 :goto_1

    :catch_0
    move-exception v0

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "modifyToValues:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto/16 :goto_0

    :catch_1
    move-exception v1

    :goto_2
    :try_start_3
    sget-boolean v2, Lcom/android/server/ssrm/GenericFalImpl;->DEBUG:Z

    if-eqz v2, :cond_7

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "modifyToValues:: failed by IOException, msg = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_7
    if-eqz v3, :cond_0

    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v1

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "modifyToValues:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v3, :cond_8

    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_8
    :goto_4
    throw v0

    :catch_3
    move-exception v1

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "modifyToValues:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :catchall_1
    move-exception v0

    move-object v3, v4

    goto :goto_3

    :catch_4
    move-exception v1

    move-object v3, v4

    goto :goto_2

    :cond_9
    move-object v2, v3

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_1
        :pswitch_2
        :pswitch_9
    .end packed-switch
.end method

.method public p(I)Z
    .locals 7

    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v1, -0x1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v4, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fc:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v2, v4, :cond_1

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v4, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fi:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-ne v2, v4, :cond_5

    :cond_1
    new-instance v2, Ljava/io/File;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v1, v1, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget v1, v1, Lcom/android/server/ssrm/P;->fx:I

    move-object v4, v2

    :goto_1
    if-eqz v4, :cond_2

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "revertToDefault:: path = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", revertValue = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/android/server/ssrm/GenericFalImpl;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    if-eqz v4, :cond_0

    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "UTF-8"

    invoke-virtual {v1, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v0, 0x1

    if-eqz v2, :cond_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "revertToDefault:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_2
    new-instance v2, Ljava/io/File;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v1, v1, Lcom/android/server/ssrm/P;->fv:Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget v1, v1, Lcom/android/server/ssrm/P;->fy:I

    move-object v4, v2

    goto :goto_1

    :pswitch_3
    new-instance v2, Ljava/io/File;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v1, v1, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget v1, v1, Lcom/android/server/ssrm/P;->fx:I

    move-object v4, v2

    goto :goto_1

    :pswitch_4
    new-instance v2, Ljava/io/File;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v1, v1, Lcom/android/server/ssrm/P;->fn:Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget v1, v1, Lcom/android/server/ssrm/P;->fq:I

    move-object v4, v2

    goto/16 :goto_1

    :pswitch_5
    new-instance v2, Ljava/io/File;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v1, v1, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget v1, v1, Lcom/android/server/ssrm/P;->fr:I

    move-object v4, v2

    goto/16 :goto_1

    :pswitch_6
    new-instance v2, Ljava/io/File;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v1, v1, Lcom/android/server/ssrm/P;->fz:Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget v1, v1, Lcom/android/server/ssrm/P;->fC:I

    move-object v4, v2

    goto/16 :goto_1

    :pswitch_7
    new-instance v2, Ljava/io/File;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v1, v1, Lcom/android/server/ssrm/P;->fA:Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget v1, v1, Lcom/android/server/ssrm/P;->fD:I

    move-object v4, v2

    goto/16 :goto_1

    :pswitch_8
    new-instance v2, Ljava/io/File;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v1, v1, Lcom/android/server/ssrm/P;->fs:Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget v1, v1, Lcom/android/server/ssrm/P;->ft:I

    move-object v4, v2

    goto/16 :goto_1

    :pswitch_9
    new-instance v2, Ljava/io/File;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v1, v1, Lcom/android/server/ssrm/P;->fF:Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget v1, v1, Lcom/android/server/ssrm/P;->fE:I

    move-object v4, v2

    goto/16 :goto_1

    :catch_1
    move-exception v1

    move-object v2, v3

    :goto_2
    :try_start_3
    sget-boolean v3, Lcom/android/server/ssrm/GenericFalImpl;->DEBUG:Z

    if-eqz v3, :cond_3

    sget-object v3, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "revertToDefault:: failed by IOException, msg = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :cond_3
    if-eqz v2, :cond_0

    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v1

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "revertToDefault:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    move-object v2, v3

    :goto_3
    if-eqz v2, :cond_4

    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_4
    :goto_4
    throw v0

    :catch_3
    move-exception v1

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "revertToDefault:: failed by IOException, msg = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :cond_5
    move-object v4, v3

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_1
        :pswitch_2
        :pswitch_9
    .end packed-switch
.end method

.method public q(I)Z
    .locals 2

    packed-switch p1, :pswitch_data_0

    :cond_0
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fc:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_1

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fi:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-ne v0, v1, :cond_0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v0, v0, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/ssrm/GenericFalImpl;->k(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v0, v0, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/ssrm/GenericFalImpl;->k(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v0, v0, Lcom/android/server/ssrm/P;->fn:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/ssrm/GenericFalImpl;->k(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v0, v0, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/ssrm/GenericFalImpl;->k(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v0, v0, Lcom/android/server/ssrm/P;->fz:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/ssrm/GenericFalImpl;->k(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v0, v0, Lcom/android/server/ssrm/P;->fA:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/ssrm/GenericFalImpl;->k(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :pswitch_7
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eJ:Lcom/android/server/ssrm/P;

    iget-object v0, v0, Lcom/android/server/ssrm/P;->fs:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/ssrm/GenericFalImpl;->k(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public r(I)[I
    .locals 2

    packed-switch p1, :pswitch_data_0

    :cond_0
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fb:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eM:[I

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fc:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eN:[I

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fd:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_3

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fe:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_3

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fh:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_3

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->ff:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_3

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fk:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_3

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fl:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_3

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fj:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_3

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fg:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-ne v0, v1, :cond_4

    :cond_3
    invoke-static {}, Lcom/android/server/ssrm/GenericFalImpl;->au()[I

    move-result-object v0

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fi:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eQ:[I

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fc:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-ne v0, v1, :cond_5

    invoke-static {}, Lcom/android/server/ssrm/GenericFalImpl;->as()[I

    move-result-object v0

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fi:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/GenericFalImpl;->at()[I

    move-result-object v0

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fb:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fd:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fe:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->ff:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fk:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fl:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fg:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fh:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fj:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-eq v0, v1, :cond_6

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fi:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-ne v0, v1, :cond_7

    :cond_6
    invoke-static {}, Lcom/android/server/ssrm/GenericFalImpl;->ar()[I

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eI:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fc:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/GenericFalImpl;->aq()Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eO:[I

    goto/16 :goto_0

    :cond_8
    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl;->eP:[I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
