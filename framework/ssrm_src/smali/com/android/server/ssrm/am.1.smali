.class Lcom/android/server/ssrm/am;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic jG:Lcom/android/server/ssrm/al;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/al;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/am;->jG:Lcom/android/server/ssrm/al;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "sendCommandToSSRM:: create thread"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-wide/32 v0, 0x927c0

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v0, p0, Lcom/android/server/ssrm/am;->jG:Lcom/android/server/ssrm/al;

    iget v0, v0, Lcom/android/server/ssrm/al;->jF:I

    if-le v0, v4, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/am;->jG:Lcom/android/server/ssrm/al;

    iget v1, v0, Lcom/android/server/ssrm/al;->jF:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v0, Lcom/android/server/ssrm/al;->jF:I

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendCommandToSSRM:: count = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/ssrm/am;->jG:Lcom/android/server/ssrm/al;

    iget v2, v2, Lcom/android/server/ssrm/al;->jF:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/android/server/ssrm/am;->jG:Lcom/android/server/ssrm/al;

    iget v0, v0, Lcom/android/server/ssrm/al;->jF:I

    if-ne v0, v4, :cond_2

    invoke-static {v3}, Lb/p;->D(Z)V

    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_1

    const-string v0, "HeavyUserScenario"

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/server/ssrm/am;->jG:Lcom/android/server/ssrm/al;

    iput v3, v0, Lcom/android/server/ssrm/al;->jF:I

    :cond_2
    return-void
.end method
