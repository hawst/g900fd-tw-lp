.class public Lcom/android/server/ssrm/QcFalImpl;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/server/ssrm/bd;


# static fields
.field static final TAG:Ljava/lang/String;

.field private static final kG:[I

.field private static final kH:[I

.field private static final kI:[I

.field private static final kJ:[I

.field private static ky:Lcom/android/server/ssrm/aR;


# instance fields
.field gN:[I

.field kA:I

.field kB:Lorg/codeaurora/Performance;

.field kC:Z

.field kD:Lorg/codeaurora/Performance;

.field kE:Z

.field kF:Lorg/codeaurora/Performance;

.field kx:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

.field kz:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/server/ssrm/QcFalImpl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/server/ssrm/QcFalImpl;->kG:[I

    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/server/ssrm/QcFalImpl;->kH:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/server/ssrm/QcFalImpl;->kI:[I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/server/ssrm/QcFalImpl;->kJ:[I

    return-void

    :array_0
    .array-data 4
        0x3a3
        0x320
        0x266
        0x1cc
        0x133
        0xc8
        0x96
        0x4b
    .end array-data

    :array_1
    .array-data 4
        0x429
        0x31c
        0x296
        0x210
        0x1cc
        0x189
        0x133
        0x103
        0xc8
        0x96
        0x64
        0x4b
        0x32
    .end array-data

    :array_2
    .array-data 4
        0xfe2
        0xbeb
        0x5f5
        0x2fa
    .end array-data

    :array_3
    .array-data 4
        0x2
        0x1
    .end array-data
.end method

.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/android/server/ssrm/QcFalImpl;->kz:I

    iput v1, p0, Lcom/android/server/ssrm/QcFalImpl;->kA:I

    new-instance v0, Lorg/codeaurora/Performance;

    invoke-direct {v0}, Lorg/codeaurora/Performance;-><init>()V

    iput-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kB:Lorg/codeaurora/Performance;

    iput-boolean v1, p0, Lcom/android/server/ssrm/QcFalImpl;->kC:Z

    new-instance v0, Lorg/codeaurora/Performance;

    invoke-direct {v0}, Lorg/codeaurora/Performance;-><init>()V

    iput-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kD:Lorg/codeaurora/Performance;

    iput-boolean v1, p0, Lcom/android/server/ssrm/QcFalImpl;->kE:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kF:Lorg/codeaurora/Performance;

    const-string v0, "ro.board.platform"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "msm8960"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kM:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    iput-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kx:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    :goto_0
    new-instance v0, Lcom/android/server/ssrm/aR;

    iget-object v1, p0, Lcom/android/server/ssrm/QcFalImpl;->kx:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    invoke-direct {v0, v1}, Lcom/android/server/ssrm/aR;-><init>(Lcom/android/server/ssrm/QcFalImpl$PlatformType;)V

    sput-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    return-void

    :cond_0
    const-string v1, "msm8226"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kN:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    iput-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kx:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    goto :goto_0

    :cond_1
    const-string v1, "msm8974"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kO:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    iput-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kx:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    goto :goto_0

    :cond_2
    const-string v1, "msm8610"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kP:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    iput-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kx:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    goto :goto_0

    :cond_3
    const-string v1, "apq8084"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kQ:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    iput-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kx:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    goto :goto_0

    :cond_4
    const-string v1, "msm8916"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kR:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    iput-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kx:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kL:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    iput-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kx:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    goto :goto_0
.end method

.method private static as()[I
    .locals 6

    const/4 v0, 0x0

    sget-object v1, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    sget-object v2, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v2, v2, Lcom/android/server/ssrm/aR;->fw:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v2, v1, 0x1

    const/16 v1, 0x20

    if-gt v2, v1, :cond_0

    new-array v1, v2, [I

    const/4 v0, 0x0

    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_2

    sub-int v3, v2, v0

    aput v3, v1, v0

    sget-object v3, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "parseCpuCoreTable:: table["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget v5, v1, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private au()[I
    .locals 8

    const/4 v0, 0x0

    const/4 v2, 0x0

    sget-object v1, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    sget-object v3, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v3, v3, Lcom/android/server/ssrm/aR;->fp:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    array-length v4, v3

    new-array v0, v4, [I

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v0, v1

    sget-object v5, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "parseGpuFreqTable:: table["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, v0, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    sget-object v1, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iput v4, v1, Lcom/android/server/ssrm/aR;->fq:I

    sget-object v1, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iput v2, v1, Lcom/android/server/ssrm/aR;->fr:I

    sget-object v1, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget v1, v1, Lcom/android/server/ssrm/aR;->fq:I

    iput v1, p0, Lcom/android/server/ssrm/QcFalImpl;->kz:I

    sget-object v1, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget v1, v1, Lcom/android/server/ssrm/aR;->fr:I

    iput v1, p0, Lcom/android/server/ssrm/QcFalImpl;->kA:I

    goto :goto_0
.end method

.method public static av()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v0, v0, Lcom/android/server/ssrm/aR;->fo:Ljava/lang/String;

    return-object v0
.end method

.method private static k(Ljava/lang/String;)Z
    .locals 2

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method R(I)V
    .locals 6

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    const/4 v0, 0x4

    if-le p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/server/ssrm/QcFalImpl;->kB:Lorg/codeaurora/Performance;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kC:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    const-string v2, "perfLockRelease(),  type = CORE_MIN"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kB:Lorg/codeaurora/Performance;

    invoke-virtual {v0}, Lorg/codeaurora/Performance;->perfLockRelease()I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kC:Z

    :cond_2
    packed-switch p1, :pswitch_data_0

    :goto_1
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "perfLockAcquire("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "), type = CORE_MIN"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kB:Lorg/codeaurora/Performance;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/16 v5, 0x704

    aput v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lorg/codeaurora/Performance;->perfLockAcquire(I[I)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kC:Z

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kB:Lorg/codeaurora/Performance;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/16 v5, 0x703

    aput v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lorg/codeaurora/Performance;->perfLockAcquire(I[I)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kC:Z

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kB:Lorg/codeaurora/Performance;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/16 v5, 0x702

    aput v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lorg/codeaurora/Performance;->perfLockAcquire(I[I)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kC:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method S(I)V
    .locals 6

    const/4 v0, 0x1

    if-lt p1, v0, :cond_0

    const/4 v0, 0x4

    if-le p1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/android/server/ssrm/QcFalImpl;->kD:Lorg/codeaurora/Performance;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kE:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    const-string v2, "perfLockRelease(),  type = CORE_MAX"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kD:Lorg/codeaurora/Performance;

    invoke-virtual {v0}, Lorg/codeaurora/Performance;->perfLockRelease()I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kE:Z

    :cond_2
    packed-switch p1, :pswitch_data_0

    :goto_1
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "perfLockAcquire("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "), type = CORE_MAX"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kD:Lorg/codeaurora/Performance;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/16 v5, 0x8fc

    aput v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lorg/codeaurora/Performance;->perfLockAcquire(I[I)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kE:Z

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kD:Lorg/codeaurora/Performance;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/16 v5, 0x8fd

    aput v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lorg/codeaurora/Performance;->perfLockAcquire(I[I)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kE:Z

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kD:Lorg/codeaurora/Performance;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/16 v5, 0x8fe

    aput v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lorg/codeaurora/Performance;->perfLockAcquire(I[I)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kE:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method bE()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kF:Lorg/codeaurora/Performance;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    const-string v1, "acquirePerfPwrClpsDisLock:: create new Performance instance"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kF:Lorg/codeaurora/Performance;

    invoke-virtual {v0}, Lorg/codeaurora/Performance;->perfLockRelease()I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kF:Lorg/codeaurora/Performance;

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    const-string v1, "acquirePerfPwrClpsDisLock:: perfLockAcquire(ALL_CPUS_PWR_CLPS_DIS)"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lorg/codeaurora/Performance;

    invoke-direct {v0}, Lorg/codeaurora/Performance;-><init>()V

    iput-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kF:Lorg/codeaurora/Performance;

    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kF:Lorg/codeaurora/Performance;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/16 v2, 0x100

    aput v2, v1, v3

    invoke-virtual {v0, v3, v1}, Lorg/codeaurora/Performance;->perfLockAcquire(I[I)I

    return-void
.end method

.method public c(II)Z
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v1, 0x1

    const-string v0, ""

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    invoke-virtual {p0, p2}, Lcom/android/server/ssrm/QcFalImpl;->R(I)V

    move v0, v1

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p2}, Lcom/android/server/ssrm/QcFalImpl;->S(I)V

    move v0, v1

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v0, v0, Lcom/android/server/ssrm/aR;->fn:Ljava/lang/String;

    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/android/server/ssrm/QcFalImpl;->f(II)I

    move-result v2

    if-eq p1, v3, :cond_0

    if-ne p1, v4, :cond_4

    :cond_0
    if-ne p1, v3, :cond_2

    iput v2, p0, Lcom/android/server/ssrm/QcFalImpl;->kz:I

    :cond_1
    :goto_2
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mCurrentGpuMinValue = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/ssrm/QcFalImpl;->kz:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mCurrentGpuMaxValue = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/ssrm/QcFalImpl;->kA:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    sget-object v2, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v2, v2, Lcom/android/server/ssrm/aR;->fo:Ljava/lang/String;

    sget-object v3, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget v3, v3, Lcom/android/server/ssrm/aR;->fr:I

    invoke-static {v0, v2, v3}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    sget-object v2, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v2, v2, Lcom/android/server/ssrm/aR;->fn:Ljava/lang/String;

    sget-object v3, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget v3, v3, Lcom/android/server/ssrm/aR;->fq:I

    invoke-static {v0, v2, v3}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    sget-object v2, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v2, v2, Lcom/android/server/ssrm/aR;->fo:Ljava/lang/String;

    iget v3, p0, Lcom/android/server/ssrm/QcFalImpl;->kA:I

    invoke-static {v0, v2, v3}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v2, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v3, v0, Lcom/android/server/ssrm/aR;->fn:Ljava/lang/String;

    iget v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kz:I

    iget v4, p0, Lcom/android/server/ssrm/QcFalImpl;->kA:I

    if-ge v0, v4, :cond_3

    iget v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kA:I

    :goto_3
    invoke-static {v2, v3, v0}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :goto_4
    move v0, v1

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v0, v0, Lcom/android/server/ssrm/aR;->fo:Ljava/lang/String;

    goto :goto_1

    :pswitch_5
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v0, v0, Lcom/android/server/ssrm/aR;->fz:Ljava/lang/String;

    goto :goto_1

    :pswitch_6
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v0, v0, Lcom/android/server/ssrm/aR;->fA:Ljava/lang/String;

    goto :goto_1

    :pswitch_7
    invoke-virtual {p0}, Lcom/android/server/ssrm/QcFalImpl;->bE()V

    move v0, v1

    goto/16 :goto_0

    :cond_2
    if-ne p1, v4, :cond_1

    iput v2, p0, Lcom/android/server/ssrm/QcFalImpl;->kA:I

    goto :goto_2

    :cond_3
    iget v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kz:I

    goto :goto_3

    :cond_4
    sget-object v3, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v0, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_7
    .end packed-switch
.end method

.method f(II)I
    .locals 5

    const/16 v0, 0x124c

    const/16 v2, 0x5f5

    const/16 v3, 0x478

    const/4 v1, 0x0

    const/4 v4, 0x3

    if-eq p1, v4, :cond_0

    const/4 v4, 0x4

    if-ne p1, v4, :cond_5

    :cond_0
    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->gN:[I

    if-nez v0, :cond_3

    move p2, v1

    :cond_1
    :goto_0
    move v0, p2

    :cond_2
    return v0

    :cond_3
    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/android/server/ssrm/QcFalImpl;->gN:[I

    array-length v2, v2

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/android/server/ssrm/QcFalImpl;->gN:[I

    aget v2, v2, v0

    if-eq p2, v2, :cond_2

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move p2, v1

    goto :goto_0

    :cond_5
    const/4 v1, 0x5

    if-eq p1, v1, :cond_6

    const/4 v1, 0x6

    if-ne p1, v1, :cond_1

    :cond_6
    sget-boolean v1, Lcom/android/server/ssrm/N;->cV:Z

    if-nez v1, :cond_7

    sget-boolean v1, Lcom/android/server/ssrm/N;->de:Z

    if-eqz v1, :cond_8

    :cond_7
    sparse-switch p2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const/16 p2, 0x23c

    goto :goto_0

    :sswitch_1
    const/16 p2, 0x1bbe

    goto :goto_0

    :sswitch_2
    const/16 p2, 0x17d7

    goto :goto_0

    :sswitch_3
    move p2, v0

    goto :goto_0

    :sswitch_4
    const/16 p2, 0xdb5

    goto :goto_0

    :sswitch_5
    const/16 p2, 0x926

    goto :goto_0

    :sswitch_6
    move p2, v2

    goto :goto_0

    :sswitch_7
    move p2, v3

    goto :goto_0

    :cond_8
    sget-boolean v1, Lcom/android/server/ssrm/N;->cY:Z

    if-eqz v1, :cond_1

    sparse-switch p2, :sswitch_data_1

    goto :goto_0

    :sswitch_8
    const/16 p2, 0x2fa

    goto :goto_0

    :sswitch_9
    const/16 p2, 0x3f7a

    goto :goto_0

    :sswitch_a
    const/16 p2, 0x2f71

    goto :goto_0

    :sswitch_b
    const/16 p2, 0x2775

    goto :goto_0

    :sswitch_c
    const/16 p2, 0x1f78

    goto :goto_0

    :sswitch_d
    const/16 p2, 0x1b6b

    goto :goto_0

    :sswitch_e
    const/16 p2, 0x176c

    goto :goto_0

    :sswitch_f
    move p2, v0

    goto :goto_0

    :sswitch_10
    const/16 p2, 0xf70

    goto :goto_0

    :sswitch_11
    const/16 p2, 0xbeb

    goto :goto_0

    :sswitch_12
    const/16 p2, 0x8f0

    goto :goto_0

    :sswitch_13
    move p2, v2

    goto :goto_0

    :sswitch_14
    move p2, v3

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4b -> :sswitch_0
        0x96 -> :sswitch_7
        0xc8 -> :sswitch_6
        0x133 -> :sswitch_5
        0x1cc -> :sswitch_4
        0x266 -> :sswitch_3
        0x320 -> :sswitch_2
        0x3a3 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x32 -> :sswitch_8
        0x4b -> :sswitch_14
        0x64 -> :sswitch_13
        0x96 -> :sswitch_12
        0xc8 -> :sswitch_11
        0x103 -> :sswitch_10
        0x133 -> :sswitch_f
        0x189 -> :sswitch_e
        0x1cc -> :sswitch_d
        0x210 -> :sswitch_c
        0x296 -> :sswitch_b
        0x31c -> :sswitch_a
        0x429 -> :sswitch_9
    .end sparse-switch
.end method

.method public p(I)Z
    .locals 7

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string v2, ""

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    iget-object v2, p0, Lcom/android/server/ssrm/QcFalImpl;->kB:Lorg/codeaurora/Performance;

    monitor-enter v2

    :try_start_0
    iget-boolean v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kC:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    const-string v3, "perfLockRelease(),  type = CORE_MIN"

    invoke-static {v0, v3}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kB:Lorg/codeaurora/Performance;

    invoke-virtual {v0}, Lorg/codeaurora/Performance;->perfLockRelease()I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kC:Z

    :cond_0
    monitor-exit v2

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :pswitch_2
    iget-object v2, p0, Lcom/android/server/ssrm/QcFalImpl;->kD:Lorg/codeaurora/Performance;

    monitor-enter v2

    :try_start_1
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    const-string v3, "perfLockRelease(),  type = CORE_MAX"

    invoke-static {v0, v3}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kE:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kD:Lorg/codeaurora/Performance;

    invoke-virtual {v0}, Lorg/codeaurora/Performance;->perfLockRelease()I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kE:Z

    :cond_1
    monitor-exit v2

    move v0, v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    :pswitch_3
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v2, v0, Lcom/android/server/ssrm/aR;->fn:Ljava/lang/String;

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget v0, v0, Lcom/android/server/ssrm/aR;->fq:I

    move-object v3, v2

    move v2, v0

    :goto_1
    if-eq p1, v4, :cond_2

    if-ne p1, v5, :cond_6

    :cond_2
    if-ne p1, v4, :cond_4

    iput v2, p0, Lcom/android/server/ssrm/QcFalImpl;->kz:I

    :cond_3
    :goto_2
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mCurrentGpuMinValue = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/server/ssrm/QcFalImpl;->kz:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mCurrentGpuMaxValue = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/server/ssrm/QcFalImpl;->kA:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    sget-object v4, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v4, v4, Lcom/android/server/ssrm/aR;->fo:Ljava/lang/String;

    sget-object v5, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget v5, v5, Lcom/android/server/ssrm/aR;->fr:I

    invoke-static {v0, v4, v5}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    sget-object v4, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v4, v4, Lcom/android/server/ssrm/aR;->fn:Ljava/lang/String;

    sget-object v5, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget v5, v5, Lcom/android/server/ssrm/aR;->fq:I

    invoke-static {v0, v4, v5}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    sget-object v4, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v4, v4, Lcom/android/server/ssrm/aR;->fo:Ljava/lang/String;

    iget v5, p0, Lcom/android/server/ssrm/QcFalImpl;->kA:I

    invoke-static {v0, v4, v5}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;I)V

    sget-object v4, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v5, v0, Lcom/android/server/ssrm/aR;->fn:Ljava/lang/String;

    iget v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kz:I

    iget v6, p0, Lcom/android/server/ssrm/QcFalImpl;->kA:I

    if-ge v0, v6, :cond_5

    iget v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kA:I

    :goto_3
    invoke-static {v4, v5, v0}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :goto_4
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v3, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto/16 :goto_0

    :pswitch_4
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v2, v0, Lcom/android/server/ssrm/aR;->fo:Ljava/lang/String;

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget v0, v0, Lcom/android/server/ssrm/aR;->fr:I

    move-object v3, v2

    move v2, v0

    goto/16 :goto_1

    :pswitch_5
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v2, v0, Lcom/android/server/ssrm/aR;->fz:Ljava/lang/String;

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget v0, v0, Lcom/android/server/ssrm/aR;->fC:I

    move-object v3, v2

    move v2, v0

    goto/16 :goto_1

    :pswitch_6
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v2, v0, Lcom/android/server/ssrm/aR;->fA:Ljava/lang/String;

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget v0, v0, Lcom/android/server/ssrm/aR;->fD:I

    move-object v3, v2

    move v2, v0

    goto/16 :goto_1

    :pswitch_7
    iget-object v3, p0, Lcom/android/server/ssrm/QcFalImpl;->kF:Lorg/codeaurora/Performance;

    if-eqz v3, :cond_7

    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kF:Lorg/codeaurora/Performance;

    invoke-virtual {v0}, Lorg/codeaurora/Performance;->perfLockRelease()I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kF:Lorg/codeaurora/Performance;

    move v0, v1

    goto/16 :goto_0

    :cond_4
    if-ne p1, v5, :cond_3

    iput v2, p0, Lcom/android/server/ssrm/QcFalImpl;->kA:I

    goto/16 :goto_2

    :cond_5
    iget v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kz:I

    goto :goto_3

    :cond_6
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_7
    move-object v3, v2

    move v2, v0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_7
    .end packed-switch
.end method

.method public q(I)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v0, v0, Lcom/android/server/ssrm/aR;->fn:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/ssrm/QcFalImpl;->k(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v0, v0, Lcom/android/server/ssrm/aR;->fo:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/ssrm/QcFalImpl;->k(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v0, v0, Lcom/android/server/ssrm/aR;->fz:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/ssrm/QcFalImpl;->k(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->ky:Lcom/android/server/ssrm/aR;

    iget-object v0, v0, Lcom/android/server/ssrm/aR;->fA:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/server/ssrm/QcFalImpl;->k(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public r(I)[I
    .locals 2

    packed-switch p1, :pswitch_data_0

    :cond_0
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_1
    invoke-direct {p0}, Lcom/android/server/ssrm/QcFalImpl;->au()[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->gN:[I

    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->gN:[I

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/android/server/ssrm/QcFalImpl;->kx:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->kP:Lcom/android/server/ssrm/QcFalImpl$PlatformType;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->kJ:[I

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/android/server/ssrm/QcFalImpl;->as()[I

    move-result-object v0

    goto :goto_0

    :pswitch_3
    sget-boolean v0, Lcom/android/server/ssrm/N;->cV:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/android/server/ssrm/N;->de:Z

    if-eqz v0, :cond_3

    :cond_2
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->kG:[I

    goto :goto_0

    :cond_3
    sget-boolean v0, Lcom/android/server/ssrm/N;->cY:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->kH:[I

    goto :goto_0

    :cond_4
    sget-boolean v0, Lcom/android/server/ssrm/N;->ec:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->kI:[I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
