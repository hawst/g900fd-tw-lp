.class Lcom/android/server/ssrm/J;
.super Ljava/lang/Object;


# static fields
.field static TAG:Ljava/lang/String;


# instance fields
.field cr:Lcom/android/server/ssrm/d;

.field cs:Ljava/lang/Runnable;

.field ct:Z

.field cu:Ljava/lang/Runnable;

.field mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field mIntentReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/server/ssrm/J;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/J;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/server/ssrm/K;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/K;-><init>(Lcom/android/server/ssrm/J;)V

    iput-object v0, p0, Lcom/android/server/ssrm/J;->cs:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/server/ssrm/L;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/L;-><init>(Lcom/android/server/ssrm/J;)V

    iput-object v0, p0, Lcom/android/server/ssrm/J;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/ssrm/J;->ct:Z

    new-instance v0, Lcom/android/server/ssrm/M;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/M;-><init>(Lcom/android/server/ssrm/J;)V

    iput-object v0, p0, Lcom/android/server/ssrm/J;->cu:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/android/server/ssrm/J;->mContext:Landroid/content/Context;

    sget-boolean v0, Lcom/android/server/ssrm/N;->cT:Z

    if-eqz v0, :cond_0

    const-string v0, "EMERGENCY_MODE"

    const v1, 0x16e360

    invoke-static {v0, v1}, Lcom/android/server/ssrm/d;->b(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/J;->cr:Lcom/android/server/ssrm/d;

    :goto_0
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/J;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/ssrm/J;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v0, Landroid/os/Handler;

    sget-object v1, Lcom/android/server/ssrm/ag;->jd:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/ssrm/J;->mHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/android/server/ssrm/J;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/ssrm/J;->cs:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    :cond_0
    const-string v0, "EMERGENCY_MODE"

    const-wide v2, 0x3fe3333333333333L    # 0.6

    invoke-static {v2, v3}, Lcom/android/server/ssrm/d;->a(D)I

    move-result v1

    const v2, 0xf4240

    invoke-static {v2}, Lcom/android/server/ssrm/d;->c(I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/d;->b(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/J;->cr:Lcom/android/server/ssrm/d;

    goto :goto_0
.end method

.method private static j(Ljava/lang/String;)V
    .locals 5

    sget-object v0, Lcom/android/server/ssrm/J;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "executeCommand:: cmd = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0, p0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v0

    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-virtual {v0}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v2, Lcom/android/server/ssrm/J;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "str = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    :cond_0
    return-void
.end method


# virtual methods
.method j(Z)V
    .locals 4

    sput-boolean p1, Lcom/android/server/ssrm/G;->bQ:Z

    if-eqz p1, :cond_4

    sget-boolean v0, Lcom/android/server/ssrm/N;->cV:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->cY:Z

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "stop mpdecision"

    invoke-static {v0}, Lcom/android/server/ssrm/J;->j(Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/J;->TAG:Ljava/lang/String;

    const-string v1, "/sys/devices/system/cpu/cpu0/online"

    const-string v2, "1"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/J;->TAG:Ljava/lang/String;

    const-string v1, "/sys/devices/system/cpu/cpu1/online"

    const-string v2, "0"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/J;->TAG:Ljava/lang/String;

    const-string v1, "/sys/devices/system/cpu/cpu2/online"

    const-string v2, "0"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/J;->TAG:Ljava/lang/String;

    const-string v1, "/sys/devices/system/cpu/cpu3/online"

    const-string v2, "1"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/android/server/ssrm/J;->cr:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->acquire()V

    :goto_0
    iget-object v0, p0, Lcom/android/server/ssrm/J;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.intent.action.EMERGENCY_MODE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    sget-boolean v0, Lcom/android/server/ssrm/N;->eA:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/android/server/ssrm/J;->ct:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/J;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/ssrm/J;->cu:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/ssrm/J;->ct:Z

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ssrm/J;->ct:Z

    iget-object v0, p0, Lcom/android/server/ssrm/J;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/ssrm/J;->cu:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_3
    return-void

    :cond_4
    sget-boolean v0, Lcom/android/server/ssrm/N;->cV:Z

    if-nez v0, :cond_5

    sget-boolean v0, Lcom/android/server/ssrm/N;->cY:Z

    if-eqz v0, :cond_6

    :cond_5
    const-string v0, "start mpdecision"

    invoke-static {v0}, Lcom/android/server/ssrm/J;->j(Ljava/lang/String;)V

    :cond_6
    iget-object v0, p0, Lcom/android/server/ssrm/J;->cr:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->release()V

    goto :goto_0
.end method
