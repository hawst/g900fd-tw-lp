.class Lcom/android/server/ssrm/L;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic cv:Lcom/android/server/ssrm/J;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/J;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/L;->cv:Lcom/android/server/ssrm/J;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x3

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v2, "reason"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v4, :cond_0

    const/4 v3, 0x4

    if-ne v2, v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/android/server/ssrm/L;->cv:Lcom/android/server/ssrm/J;

    if-ne v2, v4, :cond_2

    :goto_0
    invoke-virtual {v3, v0}, Lcom/android/server/ssrm/J;->j(Z)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    sget-boolean v3, Lcom/android/server/ssrm/N;->eA:Z

    if-nez v3, :cond_1

    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/server/ssrm/L;->cv:Lcom/android/server/ssrm/J;

    iget-boolean v2, v2, Lcom/android/server/ssrm/J;->ct:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/server/ssrm/L;->cv:Lcom/android/server/ssrm/J;

    iget-object v2, v2, Lcom/android/server/ssrm/J;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/server/ssrm/L;->cv:Lcom/android/server/ssrm/J;

    iget-object v3, v3, Lcom/android/server/ssrm/J;->cu:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v2, p0, Lcom/android/server/ssrm/L;->cv:Lcom/android/server/ssrm/J;

    iput-boolean v1, v2, Lcom/android/server/ssrm/J;->ct:Z

    :cond_4
    iget-object v1, p0, Lcom/android/server/ssrm/L;->cv:Lcom/android/server/ssrm/J;

    iput-boolean v0, v1, Lcom/android/server/ssrm/J;->ct:Z

    iget-object v0, p0, Lcom/android/server/ssrm/L;->cv:Lcom/android/server/ssrm/J;

    iget-object v0, v0, Lcom/android/server/ssrm/J;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/ssrm/L;->cv:Lcom/android/server/ssrm/J;

    iget-object v1, v1, Lcom/android/server/ssrm/J;->cu:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method
