.class public Lcom/android/server/ssrm/z;
.super Ljava/lang/Thread;


# static fields
.field static final SOCKET_TIMEOUT:I = 0x36ee80

.field private static final TAG:Ljava/lang/String;

.field private static final bn:I = 0x7595

.field private static bo:Lcom/android/server/ssrm/z;

.field private static final bp:Z

.field private static final bq:Ljava/util/Hashtable;

.field private static br:Lcom/android/server/ssrm/d;

.field private static bs:Lcom/android/server/ssrm/d;

.field private static bt:Lcom/android/server/ssrm/d;

.field private static bu:Lcom/android/server/ssrm/d;


# instance fields
.field mPort:I

.field private serverSocket:Ljava/net/ServerSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/server/ssrm/z;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/z;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/android/server/ssrm/z;->bq:Ljava/util/Hashtable;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput p1, p0, Lcom/android/server/ssrm/z;->mPort:I

    new-instance v0, Ljava/net/ServerSocket;

    invoke-direct {v0, p1}, Ljava/net/ServerSocket;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/ssrm/z;->serverSocket:Ljava/net/ServerSocket;

    iget-object v0, p0, Lcom/android/server/ssrm/z;->serverSocket:Ljava/net/ServerSocket;

    const v1, 0x36ee80

    invoke-virtual {v0, v1}, Ljava/net/ServerSocket;->setSoTimeout(I)V

    return-void
.end method

.method static synthetic P()Lcom/android/server/ssrm/d;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/z;->br:Lcom/android/server/ssrm/d;

    return-object v0
.end method

.method static synthetic Q()Lcom/android/server/ssrm/d;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/z;->bs:Lcom/android/server/ssrm/d;

    return-object v0
.end method

.method static synthetic R()Lcom/android/server/ssrm/d;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/z;->bt:Lcom/android/server/ssrm/d;

    return-object v0
.end method

.method static synthetic S()Lcom/android/server/ssrm/d;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/z;->bu:Lcom/android/server/ssrm/d;

    return-object v0
.end method

.method static synthetic a(Lcom/android/server/ssrm/d;)Lcom/android/server/ssrm/d;
    .locals 0

    sput-object p0, Lcom/android/server/ssrm/z;->br:Lcom/android/server/ssrm/d;

    return-object p0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/z;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/android/server/ssrm/d;)Lcom/android/server/ssrm/d;
    .locals 0

    sput-object p0, Lcom/android/server/ssrm/z;->bs:Lcom/android/server/ssrm/d;

    return-object p0
.end method

.method static synthetic c(Lcom/android/server/ssrm/d;)Lcom/android/server/ssrm/d;
    .locals 0

    sput-object p0, Lcom/android/server/ssrm/z;->bt:Lcom/android/server/ssrm/d;

    return-object p0
.end method

.method public static create()V
    .locals 4

    sget-object v0, Lcom/android/server/ssrm/z;->bo:Lcom/android/server/ssrm/z;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/z;->TAG:Ljava/lang/String;

    const-string v1, "Debug server already exists"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/android/server/ssrm/z;->init()V

    :try_start_0
    new-instance v0, Lcom/android/server/ssrm/z;

    const/16 v1, 0x7595

    invoke-direct {v0, v1}, Lcom/android/server/ssrm/z;-><init>(I)V

    sput-object v0, Lcom/android/server/ssrm/z;->bo:Lcom/android/server/ssrm/z;

    sget-object v0, Lcom/android/server/ssrm/z;->bo:Lcom/android/server/ssrm/z;

    invoke-virtual {v0}, Lcom/android/server/ssrm/z;->start()V

    sget-object v0, Lcom/android/server/ssrm/z;->TAG:Ljava/lang/String;

    const-string v1, "Start new server with port 30101"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/server/ssrm/z;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "e = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic d(Lcom/android/server/ssrm/d;)Lcom/android/server/ssrm/d;
    .locals 0

    sput-object p0, Lcom/android/server/ssrm/z;->bu:Lcom/android/server/ssrm/d;

    return-object p0
.end method

.method private static init()V
    .locals 3

    sget-object v0, Lcom/android/server/ssrm/z;->bq:Ljava/util/Hashtable;

    const-string v1, "show"

    new-instance v2, Lcom/android/server/ssrm/A;

    invoke-direct {v2}, Lcom/android/server/ssrm/A;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/server/ssrm/z;->bq:Ljava/util/Hashtable;

    const-string v1, "set_dss"

    new-instance v2, Lcom/android/server/ssrm/B;

    invoke-direct {v2}, Lcom/android/server/ssrm/B;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/server/ssrm/z;->bq:Ljava/util/Hashtable;

    const-string v1, "acquire"

    new-instance v2, Lcom/android/server/ssrm/C;

    invoke-direct {v2}, Lcom/android/server/ssrm/C;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/server/ssrm/z;->bq:Ljava/util/Hashtable;

    const-string v1, "release"

    new-instance v2, Lcom/android/server/ssrm/D;

    invoke-direct {v2}, Lcom/android/server/ssrm/D;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    :goto_0
    :try_start_0
    sget-object v0, Lcom/android/server/ssrm/z;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Waiting for client on port "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/ssrm/z;->serverSocket:Ljava/net/ServerSocket;

    invoke-virtual {v2}, Ljava/net/ServerSocket;->getLocalPort()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/z;->serverSocket:Ljava/net/ServerSocket;

    invoke-virtual {v0}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v1

    sget-object v0, Lcom/android/server/ssrm/z;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Just connected to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/net/Socket;->getRemoteSocketAddress()Ljava/net/SocketAddress;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-virtual {v1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    new-instance v3, Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const-string v0, "\r\nWelcome to the ssrm world!"

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->write([B)V

    :cond_0
    const-string v0, "\r\nssrm> "

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->write([B)V

    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "[^\\x20-\\x7e]"

    const-string v5, ""

    invoke-virtual {v4, v0, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v5, " "

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    sget-object v0, Lcom/android/server/ssrm/z;->bq:Ljava/util/Hashtable;

    const/4 v6, 0x0

    aget-object v6, v5, v6

    invoke-virtual {v0, v6}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/server/ssrm/z;->bq:Ljava/util/Hashtable;

    const/4 v6, 0x0

    aget-object v6, v5, v6

    invoke-virtual {v0, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/ssrm/E;

    invoke-interface {v0, v5}, Lcom/android/server/ssrm/E;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->write([B)V

    :goto_1
    const-string v0, "bye"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/z;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Thank you for connecting to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/net/Socket;->getLocalSocketAddress()Ljava/net/SocketAddress;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\nGoodbye!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_0

    :cond_1
    const-string v0, "Error: syntax error."

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_1
    iget-object v0, p0, Lcom/android/server/ssrm/z;->serverSocket:Ljava/net/ServerSocket;

    invoke-virtual {v0}, Ljava/net/ServerSocket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/z;->serverSocket:Ljava/net/ServerSocket;

    invoke-virtual {v0}, Ljava/net/ServerSocket;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    :try_start_2
    new-instance v0, Ljava/net/ServerSocket;

    iget v1, p0, Lcom/android/server/ssrm/z;->mPort:I

    invoke-direct {v0, v1}, Ljava/net/ServerSocket;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/ssrm/z;->serverSocket:Ljava/net/ServerSocket;

    iget-object v0, p0, Lcom/android/server/ssrm/z;->serverSocket:Ljava/net/ServerSocket;

    const v1, 0x36ee80

    invoke-virtual {v0, v1}, Ljava/net/ServerSocket;->setSoTimeout(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    goto/16 :goto_0

    :catch_3
    move-exception v0

    :try_start_3
    new-instance v0, Ljava/net/ServerSocket;

    iget v1, p0, Lcom/android/server/ssrm/z;->mPort:I

    invoke-direct {v0, v1}, Ljava/net/ServerSocket;-><init>(I)V

    iput-object v0, p0, Lcom/android/server/ssrm/z;->serverSocket:Ljava/net/ServerSocket;

    iget-object v0, p0, Lcom/android/server/ssrm/z;->serverSocket:Ljava/net/ServerSocket;

    const v1, 0x36ee80

    invoke-virtual {v0, v1}, Ljava/net/ServerSocket;->setSoTimeout(I)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    new-instance v1, Ljava/net/ServerSocket;

    iget v2, p0, Lcom/android/server/ssrm/z;->mPort:I

    invoke-direct {v1, v2}, Ljava/net/ServerSocket;-><init>(I)V

    iput-object v1, p0, Lcom/android/server/ssrm/z;->serverSocket:Ljava/net/ServerSocket;

    iget-object v1, p0, Lcom/android/server/ssrm/z;->serverSocket:Ljava/net/ServerSocket;

    const v2, 0x36ee80

    invoke-virtual {v1, v2}, Ljava/net/ServerSocket;->setSoTimeout(I)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5

    :goto_2
    throw v0

    :catch_5
    move-exception v1

    goto :goto_2
.end method
