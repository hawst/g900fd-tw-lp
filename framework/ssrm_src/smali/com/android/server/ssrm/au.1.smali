.class Lcom/android/server/ssrm/au;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/server/ssrm/ay;


# static fields
.field static final jJ:Ljava/lang/String; = "/sys/class/sec/sec_touchkey/rotation"


# instance fields
.field final synthetic jE:Lcom/android/server/ssrm/ag;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/ag;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/au;->jE:Lcom/android/server/ssrm/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 3

    const-string v0, "PORTRAIT"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "setWindowOrientation:: WINDOW_ORIENTATION_PORTRAIT"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/au;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iT:Lc/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/au;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iT:Lc/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lc/a;->ah(I)V

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "/sys/class/sec/sec_touchkey/rotation"

    const-string v2, "1"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "LANDSCAPE"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "setWindowOrientation:: WINDOW_ORIENTATION_LANDSCAPE"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/au;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iT:Lc/a;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/ssrm/au;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iT:Lc/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lc/a;->ah(I)V

    :cond_3
    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "/sys/class/sec/sec_touchkey/rotation"

    const-string v2, "0"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
