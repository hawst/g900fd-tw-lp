.class public Lcom/android/server/ssrm/X;
.super Ljava/lang/Object;


# static fields
.field private static TAG:Ljava/lang/String; = null

.field public static gH:[I = null

.field public static gN:[I = null

.field private static final gP:Ljava/lang/String; = "/sys/class/power_supply/battery/siop_level"

.field private static gQ:I = 0x0

.field private static gR:I = 0x0

.field private static final gU:Ljava/lang/String; = "/sys/power/ipa/hotplug_out_threshold"

.field public static hc:I

.field static he:Z

.field public static ht:I

.field public static hu:I

.field static mContext:Landroid/content/Context;


# instance fields
.field private final gI:Landroid/os/DVFSHelper;

.field private final gJ:[I

.field private final gK:Landroid/os/DVFSHelper;

.field gL:Landroid/os/DVFSHelper;

.field gM:Landroid/os/DVFSHelper;

.field gO:Landroid/os/DVFSHelper;

.field private final gS:Ljava/lang/String;

.field private gT:I

.field private gV:I

.field private gW:Z

.field private gX:Z

.field private gY:Z

.field private gZ:Z

.field private ha:Z

.field private hb:Z

.field hd:I

.field private hf:I

.field private hg:I

.field hh:I

.field final hi:Landroid/content/Intent;

.field private hj:Z

.field private hk:Z

.field private hl:Z

.field private hm:Z

.field private hn:Z

.field private ho:Z

.field private final hp:Ljava/lang/String;

.field private final hq:Landroid/content/Intent;

.field hr:I

.field final hs:Landroid/content/Intent;

.field mAudioManager:Landroid/media/AudioManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, -0x1

    const-class v0, Lcom/android/server/ssrm/X;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/ssrm/X;->gN:[I

    const/16 v0, 0x64

    sput v0, Lcom/android/server/ssrm/X;->gQ:I

    const/16 v0, 0x63

    sput v0, Lcom/android/server/ssrm/X;->gR:I

    sput v1, Lcom/android/server/ssrm/X;->hc:I

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/ssrm/X;->he:Z

    sput v1, Lcom/android/server/ssrm/X;->ht:I

    sput v1, Lcom/android/server/ssrm/X;->hu:I

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 8

    const/16 v7, 0x11

    const/4 v0, 0x0

    const/4 v2, -0x1

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/server/ssrm/X;->gL:Landroid/os/DVFSHelper;

    iput-object v0, p0, Lcom/android/server/ssrm/X;->gM:Landroid/os/DVFSHelper;

    iput-object v0, p0, Lcom/android/server/ssrm/X;->gO:Landroid/os/DVFSHelper;

    const-string v0, "/sys/power/ipa/control_temp"

    iput-object v0, p0, Lcom/android/server/ssrm/X;->gS:Ljava/lang/String;

    const/16 v0, 0x34

    iput v0, p0, Lcom/android/server/ssrm/X;->gT:I

    iput-boolean v6, p0, Lcom/android/server/ssrm/X;->gW:Z

    iput-boolean v6, p0, Lcom/android/server/ssrm/X;->gX:Z

    iput-boolean v6, p0, Lcom/android/server/ssrm/X;->gY:Z

    iput-boolean v6, p0, Lcom/android/server/ssrm/X;->gZ:Z

    iput-boolean v6, p0, Lcom/android/server/ssrm/X;->ha:Z

    iput-boolean v6, p0, Lcom/android/server/ssrm/X;->hb:Z

    const/16 v0, -0x3e7

    iput v0, p0, Lcom/android/server/ssrm/X;->hd:I

    iput v2, p0, Lcom/android/server/ssrm/X;->hf:I

    iput v2, p0, Lcom/android/server/ssrm/X;->hg:I

    iput v2, p0, Lcom/android/server/ssrm/X;->hh:I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CHECK_HMT_LEVEL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/ssrm/X;->hi:Landroid/content/Intent;

    iput-boolean v6, p0, Lcom/android/server/ssrm/X;->hj:Z

    iput-boolean v6, p0, Lcom/android/server/ssrm/X;->hk:Z

    iput-boolean v6, p0, Lcom/android/server/ssrm/X;->hl:Z

    iput-boolean v6, p0, Lcom/android/server/ssrm/X;->hm:Z

    iput-boolean v6, p0, Lcom/android/server/ssrm/X;->hn:Z

    iput-boolean v6, p0, Lcom/android/server/ssrm/X;->ho:Z

    const-string v0, "android.intent.action.SIOP_LEVEL_CHANGED"

    iput-object v0, p0, Lcom/android/server/ssrm/X;->hp:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SIOP_LEVEL_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/ssrm/X;->hq:Landroid/content/Intent;

    iput v2, p0, Lcom/android/server/ssrm/X;->hr:I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CHECK_SIOP_LEVEL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/ssrm/X;->hs:Landroid/content/Intent;

    sput-object p1, Lcom/android/server/ssrm/X;->mContext:Landroid/content/Context;

    sget-object v0, Lcom/android/server/ssrm/X;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/android/server/ssrm/X;->mAudioManager:Landroid/media/AudioManager;

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lcom/android/server/ssrm/X;->mContext:Landroid/content/Context;

    const-string v2, "SIOP_ARM_MAX"

    const/16 v3, 0xd

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/android/server/ssrm/X;->gI:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lcom/android/server/ssrm/X;->gI:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUFrequencyForSSRM()[I

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/X;->gH:[I

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lcom/android/server/ssrm/X;->mContext:Landroid/content/Context;

    const-string v2, "SIOP_CORE_NUM_MAX"

    const/16 v3, 0xf

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/android/server/ssrm/X;->gK:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lcom/android/server/ssrm/X;->gK:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUCoreNum()[I

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/X;->gJ:[I

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lcom/android/server/ssrm/X;->mContext:Landroid/content/Context;

    const-string v2, "SIOP_GPU_FREQ_MAX"

    move v3, v7

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/android/server/ssrm/X;->gL:Landroid/os/DVFSHelper;

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lcom/android/server/ssrm/X;->mContext:Landroid/content/Context;

    const-string v2, "SIOP_GPU_FREQ_MAX"

    move v3, v7

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/android/server/ssrm/X;->gM:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lcom/android/server/ssrm/X;->gL:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedGPUFrequencyForSSRM()[I

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/X;->gN:[I

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lcom/android/server/ssrm/X;->mContext:Landroid/content/Context;

    const-string v2, "SIOP_DDR_FREQ_MIN"

    const/16 v3, 0x13

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/android/server/ssrm/X;->gO:Landroid/os/DVFSHelper;

    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/power/ipa/hotplug_out_threshold"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    const-string v1, "/sys/power/ipa/hotplug_out_threshold"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/server/ssrm/X;->gV:I

    :goto_0
    return-void

    :cond_0
    iput v6, p0, Lcom/android/server/ssrm/X;->gV:I

    goto :goto_0
.end method

.method public static A(I)V
    .locals 4

    const/16 v0, 0x64

    if-ltz p0, :cond_0

    if-le p0, v0, :cond_1

    :cond_0
    move p0, v0

    :cond_1
    sget v0, Lcom/android/server/ssrm/X;->gQ:I

    if-eq p0, v0, :cond_2

    sget-boolean v0, Lcom/android/server/ssrm/X;->he:Z

    if-eqz v0, :cond_3

    :cond_2
    :goto_0
    return-void

    :cond_3
    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "limitChargingCurrent:: percent = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sput p0, Lcom/android/server/ssrm/X;->gQ:I

    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    const-string v1, "/sys/class/power_supply/battery/siop_level"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static B(I)V
    .locals 7

    const/4 v0, 0x0

    const/4 v6, -0x1

    const/16 v1, 0x63

    sget v2, Lcom/android/server/ssrm/X;->gR:I

    if-eq v2, p0, :cond_0

    sget v2, Lcom/android/server/ssrm/X;->gR:I

    if-ne v2, v1, :cond_1

    if-ne p0, v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    const-string v2, "SurfaceFlinger"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v3, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setDynamicFpsLevel:: level = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eq p0, v6, :cond_2

    if-le p0, v1, :cond_4

    :cond_2
    move p0, v1

    :cond_3
    :goto_1
    sput p0, Lcom/android/server/ssrm/X;->gR:I

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    const-string v1, "android.ui.ISurfaceComposer"

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Landroid/os/Parcel;->writeInt(I)V

    const/16 v1, 0x3fe

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v2, v1, v0, v3, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    const-string v1, "setDynamicFpsLevel:: failed: SurfaceFlinger is dead!"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    if-gez p0, :cond_3

    move p0, v0

    goto :goto_1
.end method

.method public static G(I)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    if-ne p0, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v3, Lcom/android/server/ssrm/X;->gN:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget v5, v3, v2

    if-eq v5, p0, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static a(IZ)V
    .locals 4

    const/16 v0, 0x64

    if-ltz p0, :cond_0

    if-le p0, v0, :cond_1

    :cond_0
    move p0, v0

    :cond_1
    sput-boolean p1, Lcom/android/server/ssrm/X;->he:Z

    sget-boolean v0, Lcom/android/server/ssrm/X;->he:Z

    if-eqz v0, :cond_2

    sget v0, Lcom/android/server/ssrm/X;->gQ:I

    if-ne p0, v0, :cond_3

    :cond_2
    :goto_0
    return-void

    :cond_3
    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "limitChargingCurrent:: percent = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sput p0, Lcom/android/server/ssrm/X;->gQ:I

    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    const-string v1, "/sys/class/power_supply/battery/siop_level"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static aU()Lcom/android/server/ssrm/X;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/Y;->hv:Lcom/android/server/ssrm/X;

    return-object v0
.end method

.method public static aV()V
    .locals 4

    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    const-string v1, "/sys/class/power_supply/battery/siop_level"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget v3, Lcom/android/server/ssrm/X;->gQ:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static r(Z)V
    .locals 4

    const-string v0, "/sys/class/power_supply/battery/batt_slate_mode"

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    const-string v2, "/sys/class/power_supply/battery/batt_slate_mode"

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p0, :cond_1

    :try_start_1
    sget-object v1, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    const-string v2, "Going to disable battery charging"

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x31

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    :try_start_3
    sget-object v1, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    const-string v2, "Going to enable battery charging"

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write(I)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v1

    :goto_2
    :try_start_4
    sget-object v1, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    const-string v2, "Exception in opening the file /sys/class/power_supply/battery/batt_slate_mode"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v0, :cond_0

    :try_start_5
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v0

    move-object v0, v1

    :goto_3
    :try_start_6
    sget-object v1, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    const-string v2, "Exception in creating the FileOutputStream"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v0, :cond_0

    :try_start_7
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_1

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v1, :cond_2

    :try_start_8
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    :cond_2
    :goto_5
    throw v0

    :catch_5
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    :catchall_1
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :catch_7
    move-exception v0

    move-object v0, v1

    goto :goto_2
.end method


# virtual methods
.method public C(I)V
    .locals 5

    const/4 v3, -0x1

    const/4 v4, 0x3

    const/4 v1, 0x0

    iget v0, p0, Lcom/android/server/ssrm/X;->hg:I

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p1, v3, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/X;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iget v2, p0, Lcom/android/server/ssrm/X;->hg:I

    if-ne v0, v2, :cond_3

    move v0, v1

    :goto_1
    iget v2, p0, Lcom/android/server/ssrm/X;->hf:I

    iget v3, p0, Lcom/android/server/ssrm/X;->hg:I

    sub-int/2addr v2, v3

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lcom/android/server/ssrm/X;->mAudioManager:Landroid/media/AudioManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v4, v3, v1}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/android/server/ssrm/X;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, v4}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-le v0, p1, :cond_0

    iput v0, p0, Lcom/android/server/ssrm/X;->hf:I

    move v0, v1

    :goto_2
    iget v2, p0, Lcom/android/server/ssrm/X;->hf:I

    sub-int/2addr v2, p1

    if-ge v0, v2, :cond_3

    iget-object v2, p0, Lcom/android/server/ssrm/X;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v4, v3, v1}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iput p1, p0, Lcom/android/server/ssrm/X;->hg:I

    goto :goto_0
.end method

.method public D(I)V
    .locals 3

    iget v0, p0, Lcom/android/server/ssrm/X;->hh:I

    if-eq v0, p1, :cond_0

    const-string v0, "dev.ssrm.hmt_level"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/X;->hi:Landroid/content/Intent;

    const-string v1, "hmt_level_broadcast"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    sget-object v0, Lcom/android/server/ssrm/X;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/ssrm/X;->hi:Landroid/content/Intent;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    :cond_0
    iput p1, p0, Lcom/android/server/ssrm/X;->hh:I

    return-void
.end method

.method public E(I)V
    .locals 3

    iget v0, p0, Lcom/android/server/ssrm/X;->hr:I

    if-eq v0, p1, :cond_0

    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "broadcastSiopLevelIntent:: currentSiopLevel = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "sys.siop.level"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/X;->hs:Landroid/content/Intent;

    const-string v1, "siop_level_broadcast"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    sget-object v0, Lcom/android/server/ssrm/X;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/ssrm/X;->hs:Landroid/content/Intent;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    :cond_0
    iput p1, p0, Lcom/android/server/ssrm/X;->hr:I

    return-void
.end method

.method public F(I)V
    .locals 4

    sget-object v0, Lcom/android/server/ssrm/X;->gN:[I

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/android/server/ssrm/X;->G(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/android/server/ssrm/X;->ht:I

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "limitGPUFreq:: freq = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/X;->gL:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    :goto_1
    sput p1, Lcom/android/server/ssrm/X;->ht:I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/server/ssrm/X;->gL:Landroid/os/DVFSHelper;

    const-string v1, "GPU"

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/android/server/ssrm/X;->gL:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    goto :goto_1
.end method

.method public H(I)V
    .locals 4

    sget-object v0, Lcom/android/server/ssrm/X;->gN:[I

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/android/server/ssrm/X;->G(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "limitGPUFreq:: freq = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/android/server/ssrm/X;->gN:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    if-eq v0, p1, :cond_2

    const/4 v0, -0x1

    if-ne p1, v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/server/ssrm/X;->gM:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/server/ssrm/X;->gM:Landroid/os/DVFSHelper;

    const-string v1, "GPU"

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/android/server/ssrm/X;->gM:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    goto :goto_0
.end method

.method public I(I)V
    .locals 4

    sget v0, Lcom/android/server/ssrm/X;->hu:I

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "limitDDRFreq:: freq = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/android/server/ssrm/X;->gO:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    :goto_1
    sput p1, Lcom/android/server/ssrm/X;->hu:I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/server/ssrm/X;->gO:Landroid/os/DVFSHelper;

    const-string v1, "BUS"

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/android/server/ssrm/X;->gO:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    goto :goto_1
.end method

.method public aW()V
    .locals 3

    const-string v0, "flash_led_disable"

    const-string v0, "camera_start_disable"

    const-string v0, "camera_recording_disable"

    const-string v0, "camera_recording_low_fps_enable"

    const-string v0, "smart_bonding_disable"

    const-string v0, "live_thumbnail_disable"

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->hj:Z

    iget-boolean v1, p0, Lcom/android/server/ssrm/X;->gW:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->hl:Z

    iget-boolean v1, p0, Lcom/android/server/ssrm/X;->gY:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->hn:Z

    iget-boolean v1, p0, Lcom/android/server/ssrm/X;->ha:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->hk:Z

    iget-boolean v1, p0, Lcom/android/server/ssrm/X;->gX:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->hm:Z

    iget-boolean v1, p0, Lcom/android/server/ssrm/X;->gZ:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->ho:Z

    iget-boolean v1, p0, Lcom/android/server/ssrm/X;->hb:Z

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->gW:Z

    iput-boolean v0, p0, Lcom/android/server/ssrm/X;->hj:Z

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->gX:Z

    iput-boolean v0, p0, Lcom/android/server/ssrm/X;->hk:Z

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->gY:Z

    iput-boolean v0, p0, Lcom/android/server/ssrm/X;->hl:Z

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->ha:Z

    iput-boolean v0, p0, Lcom/android/server/ssrm/X;->hn:Z

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->gZ:Z

    iput-boolean v0, p0, Lcom/android/server/ssrm/X;->hm:Z

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->hb:Z

    iput-boolean v0, p0, Lcom/android/server/ssrm/X;->ho:Z

    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "broadcastSiopChangedIntent:: FLASH = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/ssrm/X;->gW:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", CAMERA = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/ssrm/X;->gX:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", RECORDING = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/ssrm/X;->gY:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", RECORDING_FPS = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/ssrm/X;->gZ:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", SMARTBONDING = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/ssrm/X;->ha:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", LIVETHUMBNAIL = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/ssrm/X;->hb:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/ssrm/X;->hq:Landroid/content/Intent;

    const-string v1, "flash_led_disable"

    iget-boolean v2, p0, Lcom/android/server/ssrm/X;->gW:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/server/ssrm/X;->hq:Landroid/content/Intent;

    const-string v1, "camera_start_disable"

    iget-boolean v2, p0, Lcom/android/server/ssrm/X;->gX:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/server/ssrm/X;->hq:Landroid/content/Intent;

    const-string v1, "camera_recording_disable"

    iget-boolean v2, p0, Lcom/android/server/ssrm/X;->gY:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/server/ssrm/X;->hq:Landroid/content/Intent;

    const-string v1, "camera_recording_low_fps_enable"

    iget-boolean v2, p0, Lcom/android/server/ssrm/X;->gZ:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/server/ssrm/X;->hq:Landroid/content/Intent;

    const-string v1, "smart_bonding_disable"

    iget-boolean v2, p0, Lcom/android/server/ssrm/X;->ha:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/server/ssrm/X;->hq:Landroid/content/Intent;

    const-string v1, "live_thumbnail_disable"

    iget-boolean v2, p0, Lcom/android/server/ssrm/X;->hb:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-object v0, Lcom/android/server/ssrm/X;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/ssrm/X;->hq:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public l(Z)V
    .locals 3

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->gW:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "limitFlashLed:: bLimit = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/android/server/ssrm/X;->gW:Z

    goto :goto_0
.end method

.method public m(Z)V
    .locals 3

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->gY:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "limitRecording:: recordingStop = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/android/server/ssrm/X;->gY:Z

    goto :goto_0
.end method

.method public n(Z)V
    .locals 3

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->ha:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "limitSmartBonding:: limit = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/android/server/ssrm/X;->ha:Z

    goto :goto_0
.end method

.method public o(Z)V
    .locals 3

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->gX:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "limitCameraStart:: limit = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/android/server/ssrm/X;->gX:Z

    goto :goto_0
.end method

.method public p(Z)V
    .locals 3

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->gZ:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "limitCameraFps:: limit = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/android/server/ssrm/X;->gZ:Z

    goto :goto_0
.end method

.method public q(Z)Z
    .locals 3

    iget-boolean v0, p0, Lcom/android/server/ssrm/X;->hb:Z

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "limitLiveThumbnail:: limit = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/android/server/ssrm/X;->hb:Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public w(I)V
    .locals 4

    iget v0, p0, Lcom/android/server/ssrm/X;->gT:I

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ControlIPATemperature:: controlTemp = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iput p1, p0, Lcom/android/server/ssrm/X;->gT:I

    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    const-string v1, "/sys/power/ipa/control_temp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public x(I)V
    .locals 4

    iget v0, p0, Lcom/android/server/ssrm/X;->gV:I

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setIPAHotPlugOutThreshold:: threshold = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iput p1, p0, Lcom/android/server/ssrm/X;->gV:I

    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    const-string v1, "/sys/power/ipa/hotplug_out_threshold"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public y(I)V
    .locals 4

    sget-object v0, Lcom/android/server/ssrm/X;->gH:[I

    if-eqz v0, :cond_0

    sget v0, Lcom/android/server/ssrm/X;->hc:I

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/X;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "limitCPUFreq:: freq = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/X;->gI:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    :goto_1
    sput p1, Lcom/android/server/ssrm/X;->hc:I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/server/ssrm/X;->gI:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->cancelExtraOptions()V

    iget-object v0, p0, Lcom/android/server/ssrm/X;->gI:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/android/server/ssrm/X;->gI:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    goto :goto_1
.end method

.method public final z(I)V
    .locals 4

    iget-object v0, p0, Lcom/android/server/ssrm/X;->gJ:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/X;->gJ:[I

    array-length v0, v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/android/server/ssrm/X;->hd:I

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/server/ssrm/X;->gJ:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    if-eq v0, p1, :cond_2

    const/4 v0, -0x1

    if-ne p1, v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/server/ssrm/X;->gK:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    :goto_1
    iput p1, p0, Lcom/android/server/ssrm/X;->hd:I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/server/ssrm/X;->gK:Landroid/os/DVFSHelper;

    const-string v1, "CORE_NUM"

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/android/server/ssrm/X;->gK:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    goto :goto_1
.end method
