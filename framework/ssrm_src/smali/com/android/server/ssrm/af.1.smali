.class public Lcom/android/server/ssrm/af;
.super Ljava/lang/Object;


# instance fields
.field public fA:Ljava/lang/String;

.field public fB:Ljava/lang/String;

.field public fC:I

.field public fD:I

.field public fn:Ljava/lang/String;

.field public fo:Ljava/lang/String;

.field public fp:Ljava/lang/String;

.field public fq:I

.field public fr:I

.field public fs:Ljava/lang/String;

.field public ft:I

.field public fu:Ljava/lang/String;

.field public fv:Ljava/lang/String;

.field public fw:Ljava/lang/String;

.field public fz:Ljava/lang/String;

.field public hY:I


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/MarvellSysfsFALImpl$PlatformType;)V
    .locals 3

    const/4 v2, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fn:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fo:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fp:Ljava/lang/String;

    iput v1, p0, Lcom/android/server/ssrm/af;->fq:I

    iput v1, p0, Lcom/android/server/ssrm/af;->fr:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fs:Ljava/lang/String;

    iput v1, p0, Lcom/android/server/ssrm/af;->ft:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fu:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fv:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fw:Ljava/lang/String;

    iput v1, p0, Lcom/android/server/ssrm/af;->hY:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fz:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fA:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fB:Ljava/lang/String;

    iput v1, p0, Lcom/android/server/ssrm/af;->fC:I

    iput v1, p0, Lcom/android/server/ssrm/af;->fD:I

    sget-object v0, Lcom/android/server/ssrm/ae;->ia:[I

    invoke-virtual {p1}, Lcom/android/server/ssrm/MarvellSysfsFALImpl$PlatformType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string v0, "/sys/devices/platform/galcore/gpu/gpu0/gpufreq/scaling_min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fn:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/galcore/gpu/gpu0/gpufreq/scaling_max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fo:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/galcore/gpu/gpu0/gpufreq/scaling_available_frequencies"

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fp:Ljava/lang/String;

    const v0, 0x26160

    iput v0, p0, Lcom/android/server/ssrm/af;->fq:I

    const v0, 0x98580

    iput v0, p0, Lcom/android/server/ssrm/af;->fr:I

    const-string v0, "/sys/devices/platform/soc.2/devfreq-ddr/ddr_min"

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fz:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/soc.2/devfreq-ddr/ddr_max"

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fA:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/devfreq-ddr/available_frequencies"

    iput-object v0, p0, Lcom/android/server/ssrm/af;->fB:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/af;->fC:I

    iput v2, p0, Lcom/android/server/ssrm/af;->fD:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
