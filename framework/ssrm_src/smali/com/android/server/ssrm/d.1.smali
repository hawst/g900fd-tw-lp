.class public Lcom/android/server/ssrm/d;
.super Ljava/lang/Object;


# static fields
.field static A:[I

.field static B:[I

.field static C:Landroid/content/Context;

.field static D:Landroid/os/CustomFrequencyManager;

.field private static E:Lcom/android/server/ssrm/d;

.field static final TAG:Ljava/lang/String;

.field private static x:Ljava/util/Hashtable;

.field static y:[I

.field static z:[I


# instance fields
.field F:Lcom/android/server/ssrm/e;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/d;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    new-instance v0, Lcom/android/server/ssrm/d;

    const-string v1, "DUMMY"

    invoke-direct {v0, v1}, Lcom/android/server/ssrm/d;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/ssrm/d;->E:Lcom/android/server/ssrm/d;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/android/server/ssrm/d;->init()Z

    sget-object v0, Lcom/android/server/ssrm/d;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CfmsHelper for tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is created."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static a(D)I
    .locals 6

    const/4 v3, 0x0

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    if-nez v0, :cond_1

    const/4 v0, -0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    aget v0, v0, v3

    int-to-double v0, v0

    mul-double/2addr v0, p0

    double-to-int v2, v0

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    array-length v1, v0

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    aget v0, v0, v3

    :goto_1
    if-lez v1, :cond_0

    sget-object v3, Lcom/android/server/ssrm/d;->y:[I

    add-int/lit8 v4, v1, -0x1

    aget v3, v3, v4

    if-lt v3, v2, :cond_2

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Lcom/android/server/ssrm/d;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/ssrm/d;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/d;->E:Lcom/android/server/ssrm/d;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;D)Lcom/android/server/ssrm/d;
    .locals 5

    const/4 v3, 0x0

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/d;->E:Lcom/android/server/ssrm/d;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    aget v0, v0, v3

    int-to-double v0, v0

    mul-double/2addr v0, p1

    double-to-int v2, v0

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    array-length v1, v0

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    aget v0, v0, v3

    :goto_1
    if-lez v1, :cond_2

    sget-object v3, Lcom/android/server/ssrm/d;->y:[I

    add-int/lit8 v4, v1, -0x1

    aget v3, v3, v4

    if-lt v3, v2, :cond_3

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    :cond_2
    invoke-static {p0, v0}, Lcom/android/server/ssrm/d;->a(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v0

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;I)Lcom/android/server/ssrm/d;
    .locals 2

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    if-eqz v0, :cond_0

    if-gtz p1, :cond_1

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/d;->E:Lcom/android/server/ssrm/d;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/ssrm/d;

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/android/server/ssrm/d;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/d;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/android/server/ssrm/i;

    invoke-direct {v1, p0, p1}, Lcom/android/server/ssrm/i;-><init>(Ljava/lang/String;I)V

    iput-object v1, v0, Lcom/android/server/ssrm/d;->F:Lcom/android/server/ssrm/e;

    sget-object v1, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v1, p0, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;D)Lcom/android/server/ssrm/d;
    .locals 5

    const/4 v3, 0x0

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/d;->E:Lcom/android/server/ssrm/d;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    aget v0, v0, v3

    int-to-double v0, v0

    mul-double/2addr v0, p1

    double-to-int v2, v0

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    array-length v1, v0

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    aget v0, v0, v3

    :goto_1
    if-lez v1, :cond_2

    sget-object v3, Lcom/android/server/ssrm/d;->y:[I

    add-int/lit8 v4, v1, -0x1

    aget v3, v3, v4

    if-lt v3, v2, :cond_3

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    :cond_2
    invoke-static {p0, v0}, Lcom/android/server/ssrm/d;->b(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v0

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method public static b(Ljava/lang/String;I)Lcom/android/server/ssrm/d;
    .locals 2

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    if-eqz v0, :cond_0

    if-gtz p1, :cond_1

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/d;->E:Lcom/android/server/ssrm/d;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/ssrm/d;

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/android/server/ssrm/d;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/d;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/android/server/ssrm/h;

    invoke-direct {v1, p0, p1}, Lcom/android/server/ssrm/h;-><init>(Ljava/lang/String;I)V

    iput-object v1, v0, Lcom/android/server/ssrm/d;->F:Lcom/android/server/ssrm/e;

    sget-object v1, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v1, p0, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static c(I)I
    .locals 4

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    if-nez v0, :cond_1

    const/4 v0, -0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    array-length v1, v0

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    const/4 v2, 0x0

    aget v0, v0, v2

    :goto_1
    if-lez v1, :cond_0

    sget-object v2, Lcom/android/server/ssrm/d;->y:[I

    add-int/lit8 v3, v1, -0x1

    aget v2, v2, v3

    if-lt v2, p0, :cond_2

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method public static c(Ljava/lang/String;D)Lcom/android/server/ssrm/d;
    .locals 5

    const/4 v3, 0x0

    sget-object v0, Lcom/android/server/ssrm/d;->A:[I

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/d;->E:Lcom/android/server/ssrm/d;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/d;->A:[I

    aget v0, v0, v3

    int-to-double v0, v0

    mul-double/2addr v0, p1

    double-to-int v2, v0

    sget-object v0, Lcom/android/server/ssrm/d;->A:[I

    array-length v1, v0

    sget-object v0, Lcom/android/server/ssrm/d;->A:[I

    aget v0, v0, v3

    :goto_1
    if-lez v1, :cond_2

    sget-object v3, Lcom/android/server/ssrm/d;->A:[I

    add-int/lit8 v4, v1, -0x1

    aget v3, v3, v4

    if-lt v3, v2, :cond_3

    sget-object v0, Lcom/android/server/ssrm/d;->A:[I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    :cond_2
    invoke-static {p0, v0}, Lcom/android/server/ssrm/d;->e(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v0

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method public static c(Ljava/lang/String;I)Lcom/android/server/ssrm/d;
    .locals 2

    sget-object v0, Lcom/android/server/ssrm/d;->z:[I

    if-eqz v0, :cond_0

    if-gtz p1, :cond_1

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/d;->E:Lcom/android/server/ssrm/d;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/ssrm/d;

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/android/server/ssrm/d;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/d;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/android/server/ssrm/g;

    invoke-direct {v1, p0, p1}, Lcom/android/server/ssrm/g;-><init>(Ljava/lang/String;I)V

    iput-object v1, v0, Lcom/android/server/ssrm/d;->F:Lcom/android/server/ssrm/e;

    sget-object v1, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v1, p0, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;D)Lcom/android/server/ssrm/d;
    .locals 5

    const/4 v3, 0x0

    sget-object v0, Lcom/android/server/ssrm/d;->A:[I

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-gtz v0, :cond_1

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/d;->E:Lcom/android/server/ssrm/d;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/d;->A:[I

    aget v0, v0, v3

    int-to-double v0, v0

    mul-double/2addr v0, p1

    double-to-int v2, v0

    sget-object v0, Lcom/android/server/ssrm/d;->A:[I

    array-length v1, v0

    sget-object v0, Lcom/android/server/ssrm/d;->A:[I

    aget v0, v0, v3

    :goto_1
    if-lez v1, :cond_2

    sget-object v3, Lcom/android/server/ssrm/d;->A:[I

    add-int/lit8 v4, v1, -0x1

    aget v3, v3, v4

    if-lt v3, v2, :cond_3

    sget-object v0, Lcom/android/server/ssrm/d;->A:[I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    :cond_2
    invoke-static {p0, v0}, Lcom/android/server/ssrm/d;->f(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v0

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method public static d(Ljava/lang/String;I)Lcom/android/server/ssrm/d;
    .locals 2

    sget-object v0, Lcom/android/server/ssrm/d;->z:[I

    if-eqz v0, :cond_0

    if-gtz p1, :cond_1

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/d;->E:Lcom/android/server/ssrm/d;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/ssrm/d;

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/android/server/ssrm/d;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/d;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/android/server/ssrm/f;

    invoke-direct {v1, p0, p1}, Lcom/android/server/ssrm/f;-><init>(Ljava/lang/String;I)V

    iput-object v1, v0, Lcom/android/server/ssrm/d;->F:Lcom/android/server/ssrm/e;

    sget-object v1, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v1, p0, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;I)Lcom/android/server/ssrm/d;
    .locals 2

    sget-object v0, Lcom/android/server/ssrm/d;->A:[I

    if-eqz v0, :cond_0

    if-gtz p1, :cond_1

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/d;->E:Lcom/android/server/ssrm/d;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/ssrm/d;

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/android/server/ssrm/d;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/d;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/android/server/ssrm/l;

    invoke-direct {v1, p0, p1}, Lcom/android/server/ssrm/l;-><init>(Ljava/lang/String;I)V

    iput-object v1, v0, Lcom/android/server/ssrm/d;->F:Lcom/android/server/ssrm/e;

    sget-object v1, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v1, p0, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static f(Ljava/lang/String;I)Lcom/android/server/ssrm/d;
    .locals 2

    sget-object v0, Lcom/android/server/ssrm/d;->A:[I

    if-eqz v0, :cond_0

    if-gtz p1, :cond_1

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/d;->E:Lcom/android/server/ssrm/d;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/ssrm/d;

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/android/server/ssrm/d;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/d;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/android/server/ssrm/k;

    invoke-direct {v1, p0, p1}, Lcom/android/server/ssrm/k;-><init>(Ljava/lang/String;I)V

    iput-object v1, v0, Lcom/android/server/ssrm/d;->F:Lcom/android/server/ssrm/e;

    sget-object v1, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v1, p0, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static g(Ljava/lang/String;I)Lcom/android/server/ssrm/d;
    .locals 3

    if-ltz p1, :cond_0

    const/16 v0, 0x63

    if-le p1, v0, :cond_1

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/d;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getInstanceForFpsMaxLock:: invalid value = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/d;->E:Lcom/android/server/ssrm/d;

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/ssrm/d;

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/android/server/ssrm/d;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/d;-><init>(Ljava/lang/String;)V

    new-instance v1, Lcom/android/server/ssrm/j;

    invoke-direct {v1, p0, p1}, Lcom/android/server/ssrm/j;-><init>(Ljava/lang/String;I)V

    iput-object v1, v0, Lcom/android/server/ssrm/d;->F:Lcom/android/server/ssrm/e;

    sget-object v1, Lcom/android/server/ssrm/d;->x:Ljava/util/Hashtable;

    invoke-virtual {v1, p0, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static declared-synchronized init()Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-class v3, Lcom/android/server/ssrm/d;

    monitor-enter v3

    :try_start_0
    sget-object v0, Lcom/android/server/ssrm/d;->C:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    monitor-exit v3

    return v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    sget-object v0, Lcom/android/server/ssrm/d;->TAG:Ljava/lang/String;

    const-string v1, "cfms context is null."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->mContext:Landroid/content/Context;

    sput-object v0, Lcom/android/server/ssrm/d;->C:Landroid/content/Context;

    sget-object v0, Lcom/android/server/ssrm/d;->C:Landroid/content/Context;

    const-string v4, "CustomFrequencyManagerService"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/CustomFrequencyManager;

    sput-object v0, Lcom/android/server/ssrm/d;->D:Landroid/os/CustomFrequencyManager;

    sget-object v0, Lcom/android/server/ssrm/d;->D:Landroid/os/CustomFrequencyManager;

    if-nez v0, :cond_2

    sget-object v0, Lcom/android/server/ssrm/d;->TAG:Ljava/lang/String;

    const-string v1, "failed to get cfms service."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/android/server/ssrm/d;->D:Landroid/os/CustomFrequencyManager;

    invoke-virtual {v0}, Landroid/os/CustomFrequencyManager;->getSupportedCPUFrequency()[I

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/d;->y:[I

    sget-object v0, Lcom/android/server/ssrm/d;->y:[I

    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    sget-object v4, Lcom/android/server/ssrm/d;->y:[I

    array-length v4, v4

    if-ge v0, v4, :cond_3

    sget-object v4, Lcom/android/server/ssrm/d;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sCpuFreqTable["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/android/server/ssrm/d;->y:[I

    aget v6, v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/android/server/ssrm/d;->D:Landroid/os/CustomFrequencyManager;

    invoke-virtual {v0}, Landroid/os/CustomFrequencyManager;->getSupportedCPUCoreNum()[I

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/d;->z:[I

    sget-object v0, Lcom/android/server/ssrm/d;->z:[I

    if-eqz v0, :cond_4

    move v0, v2

    :goto_2
    sget-object v4, Lcom/android/server/ssrm/d;->z:[I

    array-length v4, v4

    if-ge v0, v4, :cond_4

    sget-object v4, Lcom/android/server/ssrm/d;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sCoreNumTable["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/android/server/ssrm/d;->z:[I

    aget v6, v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    sget-object v0, Lcom/android/server/ssrm/d;->D:Landroid/os/CustomFrequencyManager;

    invoke-virtual {v0}, Landroid/os/CustomFrequencyManager;->getSupportedGPUFrequency()[I

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/d;->A:[I

    sget-object v0, Lcom/android/server/ssrm/d;->A:[I

    if-eqz v0, :cond_5

    move v0, v2

    :goto_3
    sget-object v4, Lcom/android/server/ssrm/d;->A:[I

    array-length v4, v4

    if-ge v0, v4, :cond_5

    sget-object v4, Lcom/android/server/ssrm/d;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sGpuFreqTable["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/android/server/ssrm/d;->A:[I

    aget v6, v6, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    sget-object v0, Lcom/android/server/ssrm/d;->D:Landroid/os/CustomFrequencyManager;

    invoke-virtual {v0}, Landroid/os/CustomFrequencyManager;->getSupportedSysBusFrequency()[I

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/d;->B:[I

    sget-object v0, Lcom/android/server/ssrm/d;->B:[I

    if-eqz v0, :cond_6

    :goto_4
    sget-object v0, Lcom/android/server/ssrm/d;->B:[I

    array-length v0, v0

    if-ge v2, v0, :cond_6

    sget-object v0, Lcom/android/server/ssrm/d;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sBusFreqTable["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/android/server/ssrm/d;->B:[I

    aget v5, v5, v2

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_6
    move v0, v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method


# virtual methods
.method public acquire()V
    .locals 1

    iget-object v0, p0, Lcom/android/server/ssrm/d;->F:Lcom/android/server/ssrm/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/d;->F:Lcom/android/server/ssrm/e;

    invoke-virtual {v0}, Lcom/android/server/ssrm/e;->acquire()V

    :cond_0
    return-void
.end method

.method public acquire(I)V
    .locals 1

    iget-object v0, p0, Lcom/android/server/ssrm/d;->F:Lcom/android/server/ssrm/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/d;->F:Lcom/android/server/ssrm/e;

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/e;->acquire(I)V

    :cond_0
    return-void
.end method

.method public release()V
    .locals 1

    iget-object v0, p0, Lcom/android/server/ssrm/d;->F:Lcom/android/server/ssrm/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/d;->F:Lcom/android/server/ssrm/e;

    invoke-virtual {v0}, Lcom/android/server/ssrm/e;->release()V

    :cond_0
    return-void
.end method
