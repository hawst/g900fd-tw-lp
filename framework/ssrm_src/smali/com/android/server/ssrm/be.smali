.class public Lcom/android/server/ssrm/be;
.super Ljava/lang/Object;


# static fields
.field static final mL:I

.field static final mM:I

.field static final mN:I

.field static final mO:Ljava/lang/String;

.field static final mP:Ljava/lang/String;

.field static final mQ:Ljava/lang/String;

.field private static mY:Lcom/android/server/ssrm/be;


# instance fields
.field final TAG:Ljava/lang/String;

.field private final handler:Landroid/os/Handler;

.field final iI:Ljava/lang/String;

.field final iJ:Ljava/lang/String;

.field final mJ:Ljava/lang/String;

.field final mK:Ljava/lang/String;

.field mR:I

.field mS:I

.field mT:I

.field mU:Z

.field mV:Z

.field mW:Ljava/util/HashMap;

.field mX:Ljava/util/HashMap;

.field mZ:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v5, 0x4b

    const/16 v4, 0x14

    const/16 v1, 0x12

    const/4 v3, -0x1

    const/16 v2, 0x13

    sget-boolean v0, Lcom/android/server/ssrm/N;->dh:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->dr:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->du:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x3

    sput v0, Lcom/android/server/ssrm/be;->mL:I

    const/4 v0, 0x4

    sput v0, Lcom/android/server/ssrm/be;->mM:I

    const/4 v0, 0x4

    sput v0, Lcom/android/server/ssrm/be;->mN:I

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/be;->mO:Ljava/lang/String;

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/be;->mP:Ljava/lang/String;

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/be;->mQ:Ljava/lang/String;

    :goto_0
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/ssrm/be;->mY:Lcom/android/server/ssrm/be;

    return-void

    :cond_1
    sget-boolean v0, Lcom/android/server/ssrm/N;->cO:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v1, "_ha_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v1, "_hf_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    sput v5, Lcom/android/server/ssrm/be;->mL:I

    sput v4, Lcom/android/server/ssrm/be;->mM:I

    sput v4, Lcom/android/server/ssrm/be;->mN:I

    new-array v0, v2, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/be;->mO:Ljava/lang/String;

    new-array v0, v2, [I

    fill-array-data v0, :array_4

    invoke-static {v0}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/be;->mP:Ljava/lang/String;

    new-array v0, v2, [I

    fill-array-data v0, :array_5

    invoke-static {v0}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/be;->mQ:Ljava/lang/String;

    goto :goto_0

    :cond_3
    sget-boolean v0, Lcom/android/server/ssrm/N;->dx:Z

    if-nez v0, :cond_4

    sget-boolean v0, Lcom/android/server/ssrm/N;->dy:Z

    if-nez v0, :cond_4

    sget-boolean v0, Lcom/android/server/ssrm/N;->dz:Z

    if-eqz v0, :cond_5

    :cond_4
    const/16 v0, 0x64

    sput v0, Lcom/android/server/ssrm/be;->mL:I

    sput v5, Lcom/android/server/ssrm/be;->mM:I

    sput v5, Lcom/android/server/ssrm/be;->mN:I

    new-array v0, v4, [I

    fill-array-data v0, :array_6

    invoke-static {v0}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/be;->mO:Ljava/lang/String;

    new-array v0, v2, [I

    fill-array-data v0, :array_7

    invoke-static {v0}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/be;->mP:Ljava/lang/String;

    new-array v0, v2, [I

    fill-array-data v0, :array_8

    invoke-static {v0}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/be;->mQ:Ljava/lang/String;

    goto :goto_0

    :cond_5
    sput v3, Lcom/android/server/ssrm/be;->mL:I

    sput v3, Lcom/android/server/ssrm/be;->mM:I

    sput v3, Lcom/android/server/ssrm/be;->mN:I

    const-string v0, ""

    sput-object v0, Lcom/android/server/ssrm/be;->mO:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/android/server/ssrm/be;->mP:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/android/server/ssrm/be;->mQ:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :array_0
    .array-data 4
        0x9
        0x1f
        0xe
        0x25
        0x10
        0x13
        0xe
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x1f
        0xc
        0x1f
        0x16
        0x56
        0x49
    .end array-data

    :array_1
    .array-data 4
        0x9
        0x1f
        0xe
        0x25
        0x10
        0x13
        0xe
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x1f
        0xc
        0x1f
        0x16
        0x56
        0x4e
    .end array-data

    :array_2
    .array-data 4
        0x9
        0x1f
        0xe
        0x25
        0x10
        0x13
        0xe
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x1f
        0xc
        0x1f
        0x16
        0x56
        0x4e
    .end array-data

    :array_3
    .array-data 4
        0x9
        0x1f
        0xe
        0x25
        0x10
        0x13
        0xe
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x1f
        0xc
        0x1f
        0x16
        0x56
        0x4d
        0x4f
    .end array-data

    :array_4
    .array-data 4
        0x9
        0x1f
        0xe
        0x25
        0x10
        0x13
        0xe
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x1f
        0xc
        0x1f
        0x16
        0x56
        0x48
        0x4a
    .end array-data

    :array_5
    .array-data 4
        0x9
        0x1f
        0xe
        0x25
        0x10
        0x13
        0xe
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x1f
        0xc
        0x1f
        0x16
        0x56
        0x48
        0x4a
    .end array-data

    :array_6
    .array-data 4
        0x9
        0x1f
        0xe
        0x25
        0x10
        0x13
        0xe
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x1f
        0xc
        0x1f
        0x16
        0x56
        0x4b
        0x4a
        0x4a
    .end array-data

    :array_7
    .array-data 4
        0x9
        0x1f
        0xe
        0x25
        0x10
        0x13
        0xe
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x1f
        0xc
        0x1f
        0x16
        0x56
        0x4d
        0x4f
    .end array-data

    :array_8
    .array-data 4
        0x9
        0x1f
        0xe
        0x25
        0x10
        0x13
        0xe
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x1f
        0xc
        0x1f
        0x16
        0x56
        0x4d
        0x4f
    .end array-data
.end method

.method constructor <init>()V
    .locals 4

    const/16 v2, 0x1c

    const/16 v3, 0x19

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-class v0, Lcom/android/server/ssrm/be;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/be;->TAG:Ljava/lang/String;

    const/16 v0, 0x2a

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/be;->mJ:Ljava/lang/String;

    const/16 v0, 0x3a

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/be;->mK:Ljava/lang/String;

    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/be;->iI:Ljava/lang/String;

    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/be;->iJ:Ljava/lang/String;

    sget v0, Lcom/android/server/ssrm/be;->mL:I

    iput v0, p0, Lcom/android/server/ssrm/be;->mR:I

    sget v0, Lcom/android/server/ssrm/be;->mL:I

    iput v0, p0, Lcom/android/server/ssrm/be;->mS:I

    sget v0, Lcom/android/server/ssrm/be;->mL:I

    iput v0, p0, Lcom/android/server/ssrm/be;->mT:I

    iput-boolean v1, p0, Lcom/android/server/ssrm/be;->mU:Z

    iput-boolean v1, p0, Lcom/android/server/ssrm/be;->mV:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/ssrm/be;->mW:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/ssrm/be;->mX:Ljava/util/HashMap;

    new-instance v0, Lcom/android/server/ssrm/bf;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/bf;-><init>(Lcom/android/server/ssrm/be;)V

    iput-object v0, p0, Lcom/android/server/ssrm/be;->handler:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/android/server/ssrm/be;->mZ:Z

    sget-boolean v0, Lcom/android/server/ssrm/N;->dh:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->dr:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->du:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/server/ssrm/be;->mW:Ljava/util/HashMap;

    const-string v1, "Launcher"

    new-array v2, v2, [I

    fill-array-data v2, :array_4

    invoke-static {v2}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_1
    sget-boolean v0, Lcom/android/server/ssrm/N;->dx:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/android/server/ssrm/N;->dy:Z

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/android/server/ssrm/be;->mW:Ljava/util/HashMap;

    const-string v1, "Snote"

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    invoke-static {v2}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/server/ssrm/be;->mW:Ljava/util/HashMap;

    const-string v1, "Launcher"

    new-array v2, v2, [I

    fill-array-data v2, :array_6

    invoke-static {v2}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/server/ssrm/be;->mW:Ljava/util/HashMap;

    const-string v1, "Gallery"

    new-array v2, v3, [I

    fill-array-data v2, :array_7

    invoke-static {v2}, Lcom/android/server/ssrm/be;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x13
        0x14
        0xe
        0x1f
        0x14
        0xe
        0x54
        0x1b
        0x19
        0xe
        0x13
        0x15
        0x14
        0x54
        0x29
        0x29
        0x28
        0x37
        0x25
        0x28
        0x3f
        0x2b
        0x2f
        0x3f
        0x29
        0x2e
    .end array-data

    :array_1
    .array-data 4
        0x9
        0x1f
        0x19
        0x25
        0x19
        0x15
        0x14
        0xe
        0x1b
        0x13
        0x14
        0x1f
        0x8
        0x25
        0x4b
        0x54
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x13
        0x14
        0xe
        0x1f
        0x14
        0xe
        0x54
        0x1b
        0x19
        0xe
        0x13
        0x15
        0x14
        0x54
        0x29
        0x29
        0x28
        0x37
        0x25
        0x28
        0x3f
        0x2b
        0x2f
        0x3f
        0x29
        0x2e
    .end array-data

    :array_2
    .array-data 4
        0x28
        0x1f
        0x9
        0xa
        0x15
        0x14
        0x9
        0x1f
        0x3b
        0x2
        0x2e
        0x43
        0x33
        0x14
        0x1c
        0x15
    .end array-data

    :array_3
    .array-data 4
        0x3b
        0x2
        0x2e
        0x43
        0x33
        0x37
        0x3f
        0x54
        0x13
        0x9
        0x2c
        0x13
        0x9
        0x13
        0x18
        0x16
        0x1f
        0x2d
        0x13
        0x14
        0x1e
        0x15
        0xd
    .end array-data

    :array_4
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0xa
        0xa
        0x54
        0x16
        0x1b
        0xf
        0x14
        0x19
        0x12
        0x1f
        0x8
    .end array-data

    :array_5
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1b
        0x17
        0x9
        0xf
        0x14
        0x1d
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x9
        0x14
        0x15
        0xe
        0x1f
    .end array-data

    :array_6
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0xa
        0xa
        0x54
        0x16
        0x1b
        0xf
        0x14
        0x19
        0x12
        0x1f
        0x8
    .end array-data

    :array_7
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1d
        0x1b
        0x16
        0x16
        0x1f
        0x8
        0x3
        0x49
        0x1e
    .end array-data
.end method

.method static a([I)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/android/server/ssrm/aT;->a([I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1

    sget-boolean v0, Lcom/android/server/ssrm/N;->ez:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/be;->bW()Lcom/android/server/ssrm/be;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {v0, p0, p1, p2}, Lcom/android/server/ssrm/be;->b(Landroid/content/Context;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method private b(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1

    invoke-virtual {p0, p2}, Lcom/android/server/ssrm/be;->X(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/server/ssrm/be;->mS:I

    invoke-virtual {p0}, Lcom/android/server/ssrm/be;->bX()V

    return-void
.end method

.method public static bW()Lcom/android/server/ssrm/be;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/be;->mY:Lcom/android/server/ssrm/be;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/ssrm/be;

    invoke-direct {v0}, Lcom/android/server/ssrm/be;-><init>()V

    sput-object v0, Lcom/android/server/ssrm/be;->mY:Lcom/android/server/ssrm/be;

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/be;->mY:Lcom/android/server/ssrm/be;

    return-object v0
.end method


# virtual methods
.method X(Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/android/server/ssrm/be;->mW:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/android/server/ssrm/be;->mN:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/ssrm/be;->mX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/android/server/ssrm/be;->mM:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/android/server/ssrm/be;->mL:I

    goto :goto_0
.end method

.method protected X(I)V
    .locals 3

    iget v0, p0, Lcom/android/server/ssrm/be;->mR:I

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/android/server/ssrm/be;->mV:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/server/ssrm/be;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateTouchModeSysFS :: new value = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/android/server/ssrm/be;->mN:I

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/be;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/android/server/ssrm/be;->mQ:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    iput p1, p0, Lcom/android/server/ssrm/be;->mR:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/ssrm/be;->mV:Z

    :cond_1
    return-void

    :cond_2
    sget v0, Lcom/android/server/ssrm/be;->mM:I

    if-ne p1, v0, :cond_3

    iget-object v0, p0, Lcom/android/server/ssrm/be;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/android/server/ssrm/be;->mP:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/server/ssrm/be;->TAG:Ljava/lang/String;

    sget-object v1, Lcom/android/server/ssrm/be;->mO:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bX()V
    .locals 1

    invoke-static {}, Lcom/android/server/ssrm/G;->isEmergencyMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/G;->ac()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-boolean v0, p0, Lcom/android/server/ssrm/be;->mZ:Z

    if-nez v0, :cond_1

    sget v0, Lcom/android/server/ssrm/be;->mL:I

    invoke-virtual {p0, v0}, Lcom/android/server/ssrm/be;->X(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ssrm/be;->mZ:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/ssrm/be;->mZ:Z

    invoke-virtual {p0}, Lcom/android/server/ssrm/be;->bY()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/server/ssrm/be;->X(I)V

    goto :goto_0
.end method

.method protected bY()I
    .locals 2

    iget v0, p0, Lcom/android/server/ssrm/be;->mT:I

    sget v1, Lcom/android/server/ssrm/be;->mL:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/android/server/ssrm/be;->mT:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/server/ssrm/be;->mS:I

    sget v1, Lcom/android/server/ssrm/be;->mL:I

    if-eq v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/android/server/ssrm/be;->mU:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    sget v0, Lcom/android/server/ssrm/be;->mL:I

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/android/server/ssrm/be;->mS:I

    goto :goto_0

    :cond_2
    sget v0, Lcom/android/server/ssrm/be;->mL:I

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/android/server/ssrm/be;->mJ:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/be;->mK:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const-string v0, "SSRM_STATUS_NAME"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "TouchSeparation"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "SSRM_STATUS_VALUE"

    sget v1, Lcom/android/server/ssrm/be;->mL:I

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/server/ssrm/be;->mT:I

    invoke-virtual {p0}, Lcom/android/server/ssrm/be;->bX()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/server/ssrm/be;->iI:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/ssrm/be;->iJ:Ljava/lang/String;

    invoke-virtual {p2, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/ssrm/be;->mU:Z

    invoke-virtual {p0}, Lcom/android/server/ssrm/be;->bX()V

    goto :goto_0

    :cond_3
    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iput-boolean v4, p0, Lcom/android/server/ssrm/be;->mV:Z

    iget-object v0, p0, Lcom/android/server/ssrm/be;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    :cond_4
    const-string v0, "android.intent.action.USER_PRESENT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ssrm/be;->mV:Z

    iget-object v0, p0, Lcom/android/server/ssrm/be;->handler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
