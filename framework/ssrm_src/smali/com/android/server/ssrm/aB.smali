.class Lcom/android/server/ssrm/aB;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic jO:Lcom/android/server/ssrm/aA;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/aA;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-boolean v0, Lcom/android/server/ssrm/N;->eC:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-boolean v0, v0, Lcom/android/server/ssrm/aA;->jK:Z

    if-eqz v0, :cond_1

    const-string v0, "1"

    const-string v1, "service.camera.running"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_0

    const-string v0, "HangoutsVTCall"

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iX:Lb/N;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iX:Lb/N;

    invoke-virtual {v0, v3}, Lb/N;->M(Z)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iY:Lb/P;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iY:Lb/P;

    invoke-virtual {v0, v3}, Lb/P;->M(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iZ:Lb/T;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iZ:Lb/T;

    invoke-virtual {v0, v3}, Lb/T;->M(Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->ja:Lb/R;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->ja:Lb/R;

    invoke-virtual {v0, v3}, Lb/R;->M(Z)V

    goto :goto_0

    :cond_5
    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_6

    const-string v0, "HangoutsVTCall"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_6
    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iX:Lb/N;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iX:Lb/N;

    invoke-virtual {v0, v2}, Lb/N;->M(Z)V

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iY:Lb/P;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iY:Lb/P;

    invoke-virtual {v0, v2}, Lb/P;->M(Z)V

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iZ:Lb/T;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iZ:Lb/T;

    invoke-virtual {v0, v2}, Lb/T;->M(Z)V

    goto :goto_0

    :cond_9
    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->ja:Lb/R;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/ssrm/aB;->jO:Lcom/android/server/ssrm/aA;

    iget-object v0, v0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->ja:Lb/R;

    invoke-virtual {v0, v2}, Lb/R;->M(Z)V

    goto/16 :goto_0
.end method
