.class Lcom/android/server/ssrm/S;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic gb:Lcom/android/server/ssrm/Q;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/Q;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/S;->gb:Lcom/android/server/ssrm/Q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/ssrm/S;->gb:Lcom/android/server/ssrm/Q;

    iget v1, v1, Lcom/android/server/ssrm/Q;->fU:I

    const/16 v2, 0xb

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/android/server/ssrm/S;->gb:Lcom/android/server/ssrm/Q;

    iget-object v1, v1, Lcom/android/server/ssrm/Q;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "vrmode_developer_mode"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-nez v0, :cond_1

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v0

    if-nez v0, :cond_1

    # getter for: Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/Q;->access$000()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/ssrm/S;->gb:Lcom/android/server/ssrm/Q;

    iget-object v1, v1, Lcom/android/server/ssrm/Q;->fR:Ljava/lang/String;

    const-string v2, "0"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    # getter for: Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/Q;->access$000()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/ssrm/S;->gb:Lcom/android/server/ssrm/Q;

    iget-object v1, v1, Lcom/android/server/ssrm/Q;->fS:Ljava/lang/String;

    const-string v2, "0"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    # getter for: Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/Q;->access$000()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/ssrm/S;->gb:Lcom/android/server/ssrm/Q;

    iget-object v1, v1, Lcom/android/server/ssrm/Q;->fT:Ljava/lang/String;

    const-string v2, "0"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    # getter for: Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/Q;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mHmtMountedRunnable:: e = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
