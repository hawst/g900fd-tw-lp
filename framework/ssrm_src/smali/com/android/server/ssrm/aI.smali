.class Lcom/android/server/ssrm/aI;
.super Ljava/lang/Object;


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field kd:Lcom/android/server/ssrm/aJ;

.field ke:Lcom/android/server/ssrm/aK;

.field final kf:Ljava/lang/String;

.field final kg:Ljava/lang/String;

.field mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/server/ssrm/aI;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/aI;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "PSM_CPU_MAX"

    iput-object v0, p0, Lcom/android/server/ssrm/aI;->kf:Ljava/lang/String;

    const-string v0, "PSM_GPU_MAX"

    iput-object v0, p0, Lcom/android/server/ssrm/aI;->kg:Ljava/lang/String;

    iput-object p1, p0, Lcom/android/server/ssrm/aI;->mContext:Landroid/content/Context;

    new-instance v0, Lcom/android/server/ssrm/aJ;

    invoke-direct {v0, p0, p1}, Lcom/android/server/ssrm/aJ;-><init>(Lcom/android/server/ssrm/aI;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/ssrm/aI;->kd:Lcom/android/server/ssrm/aJ;

    iget-object v0, p0, Lcom/android/server/ssrm/aI;->kd:Lcom/android/server/ssrm/aJ;

    const-string v1, "powersaving_switch"

    invoke-virtual {v0, v1}, Lcom/android/server/ssrm/aJ;->K(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/aI;->kd:Lcom/android/server/ssrm/aJ;

    const-string v1, "psm_switch"

    invoke-virtual {v0, v1}, Lcom/android/server/ssrm/aJ;->K(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/aI;->kd:Lcom/android/server/ssrm/aJ;

    const-string v1, "psm_cpu_clock"

    invoke-virtual {v0, v1}, Lcom/android/server/ssrm/aJ;->K(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/aI;->kd:Lcom/android/server/ssrm/aJ;

    const-string v1, "psm_display"

    invoke-virtual {v0, v1}, Lcom/android/server/ssrm/aJ;->K(Ljava/lang/String;)V

    const-string v0, "PSM_CPU_MAX"

    const-wide v2, 0x3fe6666666666666L    # 0.7

    invoke-static {v2, v3}, Lcom/android/server/ssrm/d;->a(D)I

    move-result v1

    const v2, 0xf4240

    invoke-static {v2}, Lcom/android/server/ssrm/d;->c(I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/d;->b(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    const/4 v0, 0x0

    sget-boolean v1, Lcom/android/server/ssrm/N;->cY:Z

    if-eqz v1, :cond_1

    const v0, 0x172fab40

    :cond_0
    :goto_0
    const-string v1, "PSM_GPU_MAX"

    invoke-static {v1, v0}, Lcom/android/server/ssrm/d;->f(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    new-instance v0, Lcom/android/server/ssrm/aK;

    invoke-direct {v0, p1}, Lcom/android/server/ssrm/aK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/ssrm/aI;->ke:Lcom/android/server/ssrm/aK;

    iget-object v0, p0, Lcom/android/server/ssrm/aI;->ke:Lcom/android/server/ssrm/aK;

    const-string v1, "ultra_powersaving_mode"

    invoke-virtual {v0, v1}, Lcom/android/server/ssrm/aK;->K(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/server/ssrm/aI;->bx()V

    return-void

    :cond_1
    sget-boolean v1, Lcom/android/server/ssrm/N;->cV:Z

    if-nez v1, :cond_2

    sget-boolean v1, Lcom/android/server/ssrm/N;->ep:Z

    if-eqz v1, :cond_3

    :cond_2
    const v0, 0x1312d000

    goto :goto_0

    :cond_3
    sget-boolean v1, Lcom/android/server/ssrm/N;->cT:Z

    if-nez v1, :cond_4

    sget-boolean v1, Lcom/android/server/ssrm/N;->cU:Z

    if-nez v1, :cond_4

    sget-boolean v1, Lcom/android/server/ssrm/N;->eq:Z

    if-eqz v1, :cond_0

    :cond_4
    const/16 v0, 0x15e

    goto :goto_0
.end method

.method static by()V
    .locals 3

    sget-boolean v0, Lcom/android/server/ssrm/aT;->DEBUG:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/android/server/ssrm/aI;->TAG:Ljava/lang/String;

    const-string v2, "/sys/class/power_supply/battery/test_charge_current"

    invoke-static {}, Lcom/android/server/ssrm/G;->isPowerSavingMode()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "530"

    :goto_1
    invoke-static {v1, v2, v0}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "460"

    goto :goto_1
.end method


# virtual methods
.method bx()V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/server/ssrm/aI;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "powersaving_switch"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iget-object v3, p0, Lcom/android/server/ssrm/aI;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "psm_switch"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v0, v1, :cond_0

    if-ne v3, v1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/android/server/ssrm/G;->e(Z)V

    iget-object v0, p0, Lcom/android/server/ssrm/aI;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "psm_cpu_clock"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    sput-boolean v0, Lcom/android/server/ssrm/G;->bN:Z

    iget-object v0, p0, Lcom/android/server/ssrm/aI;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "psm_display"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    sput-boolean v1, Lcom/android/server/ssrm/G;->bO:Z

    sget-object v0, Lcom/android/server/ssrm/aI;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PSM = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/android/server/ssrm/G;->isPowerSavingMode()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", PSM_CPU = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/server/ssrm/G;->bN:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", PSM_DISP = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/server/ssrm/G;->bO:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/server/ssrm/G;->aa()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "PSM_CPU_MAX"

    invoke-static {v0}, Lcom/android/server/ssrm/d;->a(Ljava/lang/String;)Lcom/android/server/ssrm/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->acquire()V

    const-string v0, "PSM_GPU_MAX"

    invoke-static {v0}, Lcom/android/server/ssrm/d;->a(Ljava/lang/String;)Lcom/android/server/ssrm/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->acquire()V

    :goto_3
    invoke-static {}, Lcom/android/server/ssrm/aI;->by()V

    iget-object v0, p0, Lcom/android/server/ssrm/aI;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.intent.action.POWER_SAVING_MODE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    const-string v0, "PSM_CPU_MAX"

    invoke-static {v0}, Lcom/android/server/ssrm/d;->a(Ljava/lang/String;)Lcom/android/server/ssrm/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->release()V

    const-string v0, "PSM_GPU_MAX"

    invoke-static {v0}, Lcom/android/server/ssrm/d;->a(Ljava/lang/String;)Lcom/android/server/ssrm/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->release()V

    goto :goto_3
.end method
