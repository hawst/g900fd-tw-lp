.class public Lcom/android/server/ssrm/Q;
.super Ljava/lang/Object;


# static fields
.field static final DEBUG:Z

.field private static TAG:Ljava/lang/String; = null

.field private static fG:Lcom/android/server/ssrm/Q; = null

.field static final fY:I = -0x3e7


# instance fields
.field fH:Landroid/os/DVFSHelper;

.field fI:Landroid/os/DVFSHelper;

.field fJ:Landroid/os/DVFSHelper;

.field fK:Landroid/os/DVFSHelper;

.field fL:Landroid/os/DVFSHelper;

.field fM:I

.field fN:I

.field fO:I

.field fP:I

.field fQ:I

.field final fR:Ljava/lang/String;

.field final fS:Ljava/lang/String;

.field final fT:Ljava/lang/String;

.field fU:I

.field final fV:I

.field final fW:I

.field fX:I

.field fZ:Ljava/lang/Runnable;

.field ga:Ljava/lang/Runnable;

.field mContext:Landroid/content/Context;

.field t:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/android/server/ssrm/Q;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;

    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/Q;->DEBUG:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/ssrm/Q;->fG:Lcom/android/server/ssrm/Q;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 7

    const/16 v1, 0x18b

    const-wide/16 v4, 0x0

    const/4 v6, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v6, p0, Lcom/android/server/ssrm/Q;->fM:I

    iput v6, p0, Lcom/android/server/ssrm/Q;->fN:I

    iput v6, p0, Lcom/android/server/ssrm/Q;->fO:I

    iput v6, p0, Lcom/android/server/ssrm/Q;->fP:I

    iput v6, p0, Lcom/android/server/ssrm/Q;->fQ:I

    const/16 v0, 0x20

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/android/server/ssrm/aT;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/Q;->fR:Ljava/lang/String;

    const/16 v0, 0x29

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lcom/android/server/ssrm/aT;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/Q;->fS:Ljava/lang/String;

    const/16 v0, 0x25

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lcom/android/server/ssrm/aT;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/Q;->fT:Ljava/lang/String;

    iput v6, p0, Lcom/android/server/ssrm/Q;->fU:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/ssrm/Q;->t:Z

    const v0, 0xea60

    iput v0, p0, Lcom/android/server/ssrm/Q;->fV:I

    iput v1, p0, Lcom/android/server/ssrm/Q;->fW:I

    iput v1, p0, Lcom/android/server/ssrm/Q;->fX:I

    new-instance v0, Lcom/android/server/ssrm/R;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/R;-><init>(Lcom/android/server/ssrm/Q;)V

    iput-object v0, p0, Lcom/android/server/ssrm/Q;->fZ:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/server/ssrm/S;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/S;-><init>(Lcom/android/server/ssrm/Q;)V

    iput-object v0, p0, Lcom/android/server/ssrm/Q;->ga:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/android/server/ssrm/Q;->mContext:Landroid/content/Context;

    sget-boolean v0, Lcom/android/server/ssrm/N;->cV:Z

    if-eqz v0, :cond_0

    :cond_0
    iget v0, p0, Lcom/android/server/ssrm/Q;->fM:I

    if-eq v0, v6, :cond_1

    new-instance v0, Landroid/os/DVFSHelper;

    const-string v2, "HMT_ARM_MIN"

    const/16 v3, 0xc

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/android/server/ssrm/Q;->fH:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lcom/android/server/ssrm/Q;->fH:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/android/server/ssrm/Q;->fM:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget v0, p0, Lcom/android/server/ssrm/Q;->fN:I

    if-eq v0, v6, :cond_2

    new-instance v0, Landroid/os/DVFSHelper;

    const-string v2, "HMT_ARM_MAX"

    const/16 v3, 0xd

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/android/server/ssrm/Q;->fI:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lcom/android/server/ssrm/Q;->fI:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/android/server/ssrm/Q;->fN:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget v0, p0, Lcom/android/server/ssrm/Q;->fO:I

    if-eq v0, v6, :cond_3

    new-instance v0, Landroid/os/DVFSHelper;

    const-string v2, "HMT_GPU_MIN"

    const/16 v3, 0x10

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/android/server/ssrm/Q;->fJ:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lcom/android/server/ssrm/Q;->fJ:Landroid/os/DVFSHelper;

    const-string v1, "GPU"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/android/server/ssrm/Q;->fO:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget v0, p0, Lcom/android/server/ssrm/Q;->fP:I

    if-eq v0, v6, :cond_4

    new-instance v0, Landroid/os/DVFSHelper;

    const-string v2, "HMT_GPU_MAX"

    const/16 v3, 0x11

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/android/server/ssrm/Q;->fK:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lcom/android/server/ssrm/Q;->fK:Landroid/os/DVFSHelper;

    const-string v1, "GPU"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/android/server/ssrm/Q;->fP:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget v0, p0, Lcom/android/server/ssrm/Q;->fQ:I

    if-eq v0, v6, :cond_5

    new-instance v0, Landroid/os/DVFSHelper;

    const-string v2, "HMT_CORE_NUM_MAX"

    const/16 v3, 0xf

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/android/server/ssrm/Q;->fL:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lcom/android/server/ssrm/Q;->fL:Landroid/os/DVFSHelper;

    const-string v1, "CORE_NUM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/android/server/ssrm/Q;->fQ:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return-void

    :array_0
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x19
        0x16
        0x1b
        0x9
        0x9
        0x55
        0x9
        0x1f
        0x19
        0x55
        0xe
        0x9
        0xa
        0x55
        0x13
        0x14
        0xa
        0xf
        0xe
        0x55
        0x1f
        0x14
        0x1b
        0x18
        0x16
        0x1f
        0x1e
    .end array-data

    :array_1
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x19
        0x16
        0x1b
        0x9
        0x9
        0x55
        0x9
        0x1f
        0x19
        0x55
        0x9
        0x1f
        0x19
        0x25
        0xe
        0x15
        0xf
        0x19
        0x12
        0x11
        0x1f
        0x3
        0x55
        0x13
        0x14
        0xa
        0xf
        0xe
        0x55
        0x1f
        0x14
        0x1b
        0x18
        0x16
        0x1f
        0x1e
    .end array-data

    :array_2
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x19
        0x16
        0x1b
        0x9
        0x9
        0x55
        0x9
        0x1f
        0x19
        0x55
        0x9
        0x1f
        0x19
        0x25
        0x1f
        0xa
        0x1f
        0x14
        0x55
        0x13
        0x14
        0xa
        0xf
        0xe
        0x55
        0x1f
        0x14
        0x1b
        0x18
        0x16
        0x1f
        0x1e
    .end array-data
.end method

.method public static a(Landroid/content/Context;)Lcom/android/server/ssrm/Q;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/Q;->fG:Lcom/android/server/ssrm/Q;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/ssrm/Q;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/Q;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/server/ssrm/Q;->fG:Lcom/android/server/ssrm/Q;

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/Q;->fG:Lcom/android/server/ssrm/Q;

    return-object v0
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method a(Landroid/content/Intent;)V
    .locals 6

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "android.intent.extra.DOCK_STATE"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/server/ssrm/Q;->fU:I

    iget v0, p0, Lcom/android/server/ssrm/Q;->fU:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/android/server/ssrm/Q;->onConnected()V

    sget-boolean v0, Lcom/android/server/ssrm/N;->eF:Z

    if-eqz v0, :cond_1

    const-string v0, "HeadMountedTheaterDemo"

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "HeadMountedTheater"

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    iput-boolean v3, p0, Lcom/android/server/ssrm/Q;->t:Z

    invoke-virtual {p0}, Lcom/android/server/ssrm/Q;->ax()I

    move-result v0

    const/16 v1, -0x3e7

    if-eq v0, v1, :cond_2

    :goto_1
    sget-object v1, Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "battery temp will be check in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/server/ssrm/ag;->bj()Lcom/android/server/ssrm/ag;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/server/ssrm/Q;->fZ:Ljava/lang/Runnable;

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_2
    const v0, 0xea60

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/android/server/ssrm/Q;->fU:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/server/ssrm/Q;->onDisconnected()V

    sget-boolean v0, Lcom/android/server/ssrm/N;->eF:Z

    if-eqz v0, :cond_4

    const-string v0, "HeadMountedTheaterDemo"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_4
    const-string v0, "HeadMountedTheater"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    const-string v0, "HeadMountedTheater_2"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    iget-boolean v0, p0, Lcom/android/server/ssrm/Q;->t:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/ag;->bj()Lcom/android/server/ssrm/ag;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fZ:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-boolean v2, p0, Lcom/android/server/ssrm/Q;->t:Z

    goto :goto_0
.end method

.method aw()I
    .locals 3

    const-string v0, "dev.ssrm.htm_batttemp"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/android/server/ssrm/Q;->DEBUG:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    :cond_0
    const/16 v0, -0x3e7

    :goto_0
    return v0

    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method ax()I
    .locals 3

    const-string v0, "dev.ssrm.htm_time"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lcom/android/server/ssrm/Q;->DEBUG:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    :cond_0
    const/16 v0, -0x3e7

    :goto_0
    return v0

    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method onConnected()V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onConnected:: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/ssrm/Q;->fM:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/ssrm/Q;->fN:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/ssrm/Q;->fO:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/ssrm/Q;->fP:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean v0, Lcom/android/server/ssrm/G;->bY:Z

    iget-object v2, p0, Lcom/android/server/ssrm/Q;->fH:Landroid/os/DVFSHelper;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/server/ssrm/Q;->fH:Landroid/os/DVFSHelper;

    invoke-virtual {v2}, Landroid/os/DVFSHelper;->acquire()V

    :cond_0
    iget-object v2, p0, Lcom/android/server/ssrm/Q;->fI:Landroid/os/DVFSHelper;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/server/ssrm/Q;->fI:Landroid/os/DVFSHelper;

    invoke-virtual {v2}, Landroid/os/DVFSHelper;->acquire()V

    :cond_1
    iget-object v2, p0, Lcom/android/server/ssrm/Q;->fJ:Landroid/os/DVFSHelper;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/server/ssrm/Q;->fJ:Landroid/os/DVFSHelper;

    invoke-virtual {v2}, Landroid/os/DVFSHelper;->acquire()V

    :cond_2
    iget-object v2, p0, Lcom/android/server/ssrm/Q;->fK:Landroid/os/DVFSHelper;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/server/ssrm/Q;->fK:Landroid/os/DVFSHelper;

    invoke-virtual {v2}, Landroid/os/DVFSHelper;->acquire()V

    :cond_3
    iget-object v2, p0, Lcom/android/server/ssrm/Q;->fL:Landroid/os/DVFSHelper;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/server/ssrm/Q;->fL:Landroid/os/DVFSHelper;

    invoke-virtual {v2}, Landroid/os/DVFSHelper;->acquire()V

    :cond_4
    sget-boolean v2, Lcom/android/server/ssrm/N;->cZ:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/server/ssrm/Q;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "vrmode_developer_mode"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_6

    :goto_0
    if-nez v0, :cond_5

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fR:Ljava/lang/String;

    const-string v2, "0"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fS:Ljava/lang/String;

    const-string v2, "0"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fT:Ljava/lang/String;

    const-string v2, "0"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return-void

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method onDisconnected()V
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onConnected:: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/ssrm/Q;->fM:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/ssrm/Q;->fN:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/ssrm/Q;->fO:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/ssrm/Q;->fP:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean v0, Lcom/android/server/ssrm/G;->bY:Z

    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fH:Landroid/os/DVFSHelper;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fH:Landroid/os/DVFSHelper;

    invoke-virtual {v1}, Landroid/os/DVFSHelper;->release()V

    :cond_0
    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fI:Landroid/os/DVFSHelper;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fI:Landroid/os/DVFSHelper;

    invoke-virtual {v1}, Landroid/os/DVFSHelper;->release()V

    :cond_1
    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fJ:Landroid/os/DVFSHelper;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fJ:Landroid/os/DVFSHelper;

    invoke-virtual {v1}, Landroid/os/DVFSHelper;->release()V

    :cond_2
    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fK:Landroid/os/DVFSHelper;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fK:Landroid/os/DVFSHelper;

    invoke-virtual {v1}, Landroid/os/DVFSHelper;->release()V

    :cond_3
    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fL:Landroid/os/DVFSHelper;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fL:Landroid/os/DVFSHelper;

    invoke-virtual {v1}, Landroid/os/DVFSHelper;->release()V

    :cond_4
    sget-boolean v1, Lcom/android/server/ssrm/N;->cZ:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/server/ssrm/Q;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "vrmode_developer_mode"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_5

    const/4 v0, 0x1

    :cond_5
    if-nez v0, :cond_6

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fR:Ljava/lang/String;

    const-string v2, "1"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fS:Ljava/lang/String;

    const-string v2, "1"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/ssrm/Q;->fT:Ljava/lang/String;

    const-string v2, "1"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    return-void
.end method

.method onScreenOn()V
    .locals 4

    sget-boolean v0, Lcom/android/server/ssrm/N;->cZ:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/server/ssrm/Q;->fU:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/ag;->bj()Lcom/android/server/ssrm/ag;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/ssrm/Q;->ga:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method
