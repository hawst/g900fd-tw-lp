.class Lcom/android/server/ssrm/Z;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic hC:Lcom/android/server/ssrm/LoadDetectMonitor;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/LoadDetectMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/Z;->hC:Lcom/android/server/ssrm/LoadDetectMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/android/server/ssrm/Z;->hC:Lcom/android/server/ssrm/LoadDetectMonitor;

    invoke-virtual {v0}, Lcom/android/server/ssrm/LoadDetectMonitor;->nativeLoadDetectTask()V

    iget-object v0, p0, Lcom/android/server/ssrm/Z;->hC:Lcom/android/server/ssrm/LoadDetectMonitor;

    iget-object v0, v0, Lcom/android/server/ssrm/LoadDetectMonitor;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
