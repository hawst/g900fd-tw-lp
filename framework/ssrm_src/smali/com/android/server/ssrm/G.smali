.class public Lcom/android/server/ssrm/G;
.super Ljava/lang/Object;


# static fields
.field private static bM:Z

.field public static bN:Z

.field public static bO:Z

.field public static bP:Z

.field public static bQ:Z

.field private static bR:I

.field private static bS:I

.field private static bT:I

.field private static bU:Z

.field private static bV:Z

.field private static bW:Z

.field public static bX:Z

.field public static bY:Z

.field private static bZ:I

.field private static sScreenOn:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/ssrm/G;->bM:Z

    sput-boolean v0, Lcom/android/server/ssrm/G;->bN:Z

    sput-boolean v0, Lcom/android/server/ssrm/G;->bO:Z

    sput-boolean v0, Lcom/android/server/ssrm/G;->bP:Z

    sput-boolean v0, Lcom/android/server/ssrm/G;->bQ:Z

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/G;->sScreenOn:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static U()I
    .locals 1

    sget v0, Lcom/android/server/ssrm/G;->bS:I

    return v0
.end method

.method public static aa()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/ssrm/G;->bM:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/G;->bN:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ab()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/ssrm/G;->bM:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/G;->bO:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ac()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/ssrm/G;->bP:Z

    return v0
.end method

.method public static ad()I
    .locals 1

    sget v0, Lcom/android/server/ssrm/G;->bR:I

    return v0
.end method

.method public static ae()I
    .locals 1

    sget v0, Lcom/android/server/ssrm/G;->bT:I

    return v0
.end method

.method public static af()Z
    .locals 2

    sget v0, Lcom/android/server/ssrm/G;->bT:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    sget v0, Lcom/android/server/ssrm/G;->bT:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static ag()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/ssrm/G;->bU:Z

    return v0
.end method

.method public static ah()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/ssrm/G;->bV:Z

    return v0
.end method

.method public static ai()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/ssrm/G;->bW:Z

    return v0
.end method

.method public static aj()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/ssrm/G;->bX:Z

    return v0
.end method

.method public static ak()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/ssrm/G;->bY:Z

    return v0
.end method

.method public static al()I
    .locals 1

    sget v0, Lcom/android/server/ssrm/G;->bZ:I

    return v0
.end method

.method public static e(Z)V
    .locals 0

    sput-boolean p0, Lcom/android/server/ssrm/G;->bM:Z

    return-void
.end method

.method public static f(Z)V
    .locals 0

    sput-boolean p0, Lcom/android/server/ssrm/G;->bU:Z

    return-void
.end method

.method public static g(Z)V
    .locals 0

    sput-boolean p0, Lcom/android/server/ssrm/G;->bV:Z

    return-void
.end method

.method public static h(Z)V
    .locals 0

    sput-boolean p0, Lcom/android/server/ssrm/G;->bW:Z

    return-void
.end method

.method public static i(I)V
    .locals 0

    sput p0, Lcom/android/server/ssrm/G;->bR:I

    return-void
.end method

.method public static i(Z)V
    .locals 0

    sput-boolean p0, Lcom/android/server/ssrm/G;->sScreenOn:Z

    return-void
.end method

.method public static isEmergencyMode()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/ssrm/G;->bQ:Z

    return v0
.end method

.method public static isPowerSavingMode()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/ssrm/G;->bM:Z

    return v0
.end method

.method public static isScreenOn()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/ssrm/G;->sScreenOn:Z

    return v0
.end method

.method public static j(I)V
    .locals 0

    sput p0, Lcom/android/server/ssrm/G;->bS:I

    return-void
.end method

.method public static k(I)V
    .locals 0

    sput p0, Lcom/android/server/ssrm/G;->bT:I

    return-void
.end method

.method public static l(I)V
    .locals 0

    sput p0, Lcom/android/server/ssrm/G;->bZ:I

    return-void
.end method
