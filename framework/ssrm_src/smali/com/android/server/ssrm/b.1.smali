.class Lcom/android/server/ssrm/b;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic w:Lcom/android/server/ssrm/a;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/a;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/android/server/ssrm/ag;->bj()Lcom/android/server/ssrm/ag;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    iget-boolean v2, v2, Lcom/android/server/ssrm/a;->t:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    iget v2, v2, Lcom/android/server/ssrm/a;->a:I

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    iget-object v2, v2, Lcom/android/server/ssrm/a;->q:[Ljava/lang/String;

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    iget-object v5, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    iget-object v5, v5, Lcom/android/server/ssrm/a;->p:Ljava/util/HashMap;

    sget-object v6, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v5, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    iget-object v0, v0, Lcom/android/server/ssrm/a;->j:Landroid/content/Intent;

    const-string v2, "check_cooldown_list"

    iget-object v3, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    iget-object v3, v3, Lcom/android/server/ssrm/a;->p:Ljava/util/HashMap;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    iget-object v0, v0, Lcom/android/server/ssrm/a;->j:Landroid/content/Intent;

    const-string v2, "batt_temp_level"

    iget-object v3, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    iget v3, v3, Lcom/android/server/ssrm/a;->a:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    iget-object v0, v0, Lcom/android/server/ssrm/a;->j:Landroid/content/Intent;

    const-string v2, "battery_overheat_level"

    iget-object v3, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    iget v3, v3, Lcom/android/server/ssrm/a;->a:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    iget-object v0, v0, Lcom/android/server/ssrm/a;->j:Landroid/content/Intent;

    const-string v2, "overheat_id"

    const v3, 0x10407a3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, v1, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    iget-object v1, v1, Lcom/android/server/ssrm/a;->j:Landroid/content/Intent;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    iget-object v0, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    iget-boolean v0, v0, Lcom/android/server/ssrm/a;->s:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    iget-object v1, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    iget v1, v1, Lcom/android/server/ssrm/a;->a:I

    invoke-virtual {v0, v1}, Lcom/android/server/ssrm/a;->a(I)I

    move-result v0

    if-lez v0, :cond_1

    invoke-static {v0}, Lcom/android/server/ssrm/a;->b(I)V

    :cond_1
    iget-object v0, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/server/ssrm/a;->s:Z

    :cond_2
    iget-object v0, p0, Lcom/android/server/ssrm/b;->w:Lcom/android/server/ssrm/a;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/server/ssrm/a;->t:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/server/ssrm/a;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mKillActiveApplicationsRunnable:: e = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
