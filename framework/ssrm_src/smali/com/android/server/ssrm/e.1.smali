.class public abstract Lcom/android/server/ssrm/e;
.super Ljava/lang/Object;


# instance fields
.field protected G:Z

.field protected H:Landroid/os/CustomFrequencyManager$FrequencyRequest;

.field protected mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/server/ssrm/e;->mTag:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method acquire()V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/server/ssrm/e;->acquire(I)V

    return-void
.end method

.method abstract acquire(I)V
.end method

.method protected b()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/server/ssrm/e;->G:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/server/ssrm/e;->release()V

    :cond_0
    iget-object v0, p0, Lcom/android/server/ssrm/e;->H:Landroid/os/CustomFrequencyManager$FrequencyRequest;

    invoke-virtual {v0}, Landroid/os/CustomFrequencyManager$FrequencyRequest;->doFrequencyRequest()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ssrm/e;->G:Z

    return-void
.end method

.method protected c()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/server/ssrm/e;->G:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/e;->H:Landroid/os/CustomFrequencyManager$FrequencyRequest;

    invoke-virtual {v0}, Landroid/os/CustomFrequencyManager$FrequencyRequest;->cancelFrequencyRequest()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/ssrm/e;->G:Z

    :cond_0
    return-void
.end method

.method abstract release()V
.end method
