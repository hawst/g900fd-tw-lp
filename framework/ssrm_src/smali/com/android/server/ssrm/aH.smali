.class public Lcom/android/server/ssrm/aH;
.super Landroid/telephony/PhoneStateListener;


# instance fields
.field final synthetic jE:Lcom/android/server/ssrm/ag;


# direct methods
.method public constructor <init>(Lcom/android/server/ssrm/ag;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/aH;->jE:Lcom/android/server/ssrm/ag;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_2

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "onCallStateChanged:: CALL_STATE_IDLE"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean v2, Lcom/android/server/ssrm/ag;->io:Z

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/server/ssrm/aH;->jE:Lcom/android/server/ssrm/ag;

    iget-boolean v0, v0, Lcom/android/server/ssrm/ag;->iM:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/android/server/ssrm/ag;->io:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/ssrm/aH;->jE:Lcom/android/server/ssrm/ag;

    iget-object v1, v1, Lcom/android/server/ssrm/ag;->iK:Ljava/lang/String;

    const-string v2, "1"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    if-ne p1, v3, :cond_3

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "onCallStateChanged:: CALL_STATE_RINGING"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean v2, Lcom/android/server/ssrm/ag;->io:Z

    goto :goto_0

    :cond_3
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "onCallStateChanged:: CALL_STATE_OFFHOOK"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean v3, Lcom/android/server/ssrm/ag;->io:Z

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/ssrm/aH;->jE:Lcom/android/server/ssrm/ag;

    iget-object v1, v1, Lcom/android/server/ssrm/ag;->iK:Ljava/lang/String;

    const-string v2, "0"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/telephony/PhoneStateListener;->onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getDbm()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/ssrm/T;->v(I)V

    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getDbm()I

    move-result v1

    invoke-virtual {v0, v1}, Lf/m;->v(I)V

    :cond_0
    return-void
.end method
