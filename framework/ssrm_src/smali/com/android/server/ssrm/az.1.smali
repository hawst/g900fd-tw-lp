.class Lcom/android/server/ssrm/az;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/server/ssrm/aE;


# instance fields
.field final synthetic jE:Lcom/android/server/ssrm/ag;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/ag;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/az;->jE:Lcom/android/server/ssrm/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public O(I)V
    .locals 4

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DegasBmwShutdown: t = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/android/server/ssrm/ag;->iq:I

    const/16 v1, -0x96

    if-le v0, v1, :cond_0

    sget v0, Lcom/android/server/ssrm/ag;->iq:I

    const/16 v1, 0x258

    if-lt v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/android/server/ssrm/az;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "DegasBmwShutdown: broadcast Warnning intent"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.BMW_WARNNING_SHUTDOWN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "bmw_shutdown"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/az;->jE:Lcom/android/server/ssrm/ag;

    iget-object v1, v1, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/server/ssrm/az;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "DegasBmwShutdown: device will be shutdown after 90 seconds"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/az;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0xa

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/android/server/ssrm/az;->jE:Lcom/android/server/ssrm/ag;

    iget-object v1, v1, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    const-wide/32 v2, 0x15f90

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_1
    return-void
.end method
