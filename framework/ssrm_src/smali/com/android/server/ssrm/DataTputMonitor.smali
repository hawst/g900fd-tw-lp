.class public Lcom/android/server/ssrm/DataTputMonitor;
.super Ljava/lang/Thread;


# static fields
.field static final TAG:Ljava/lang/String;

.field public static TYPE_LTE:I

.field public static TYPE_WIFI:I

.field static final bb:Z

.field static bc:Lcom/android/server/ssrm/DataTputMonitor;

.field public static bf:I

.field public static bg:I

.field static bh:I

.field static bi:J

.field static bj:J

.field static bk:J

.field public static bl:I

.field public static mListeners:Ljava/util/ArrayList;


# instance fields
.field bd:Lcom/android/server/ssrm/DataTputMonitor$NetworkChangeReceiver;

.field be:Landroid/net/ConnectivityManager;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    const-class v0, Lcom/android/server/ssrm/DataTputMonitor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/DataTputMonitor;->TAG:Ljava/lang/String;

    sput v1, Lcom/android/server/ssrm/DataTputMonitor;->TYPE_WIFI:I

    const/4 v0, 0x2

    sput v0, Lcom/android/server/ssrm/DataTputMonitor;->bf:I

    const/4 v0, 0x3

    sput v0, Lcom/android/server/ssrm/DataTputMonitor;->TYPE_LTE:I

    const/4 v0, 0x0

    sput v0, Lcom/android/server/ssrm/DataTputMonitor;->bg:I

    sget v0, Lcom/android/server/ssrm/DataTputMonitor;->bg:I

    sput v0, Lcom/android/server/ssrm/DataTputMonitor;->bh:I

    sput-wide v2, Lcom/android/server/ssrm/DataTputMonitor;->bi:J

    sput-wide v2, Lcom/android/server/ssrm/DataTputMonitor;->bj:J

    sput-wide v2, Lcom/android/server/ssrm/DataTputMonitor;->bk:J

    sput v1, Lcom/android/server/ssrm/DataTputMonitor;->bl:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/ssrm/DataTputMonitor;->mListeners:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v4, 0x0

    const-string v0, "DataTputMonitor"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/android/server/ssrm/DataTputMonitor$NetworkChangeReceiver;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/DataTputMonitor$NetworkChangeReceiver;-><init>(Lcom/android/server/ssrm/DataTputMonitor;)V

    iput-object v0, p0, Lcom/android/server/ssrm/DataTputMonitor;->bd:Lcom/android/server/ssrm/DataTputMonitor$NetworkChangeReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/android/server/ssrm/DataTputMonitor;->be:Landroid/net/ConnectivityManager;

    iget-object v1, p0, Lcom/android/server/ssrm/DataTputMonitor;->bd:Lcom/android/server/ssrm/DataTputMonitor$NetworkChangeReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v0, p1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method

.method public static N()J
    .locals 2

    sget-wide v0, Lcom/android/server/ssrm/DataTputMonitor;->bk:J

    return-wide v0
.end method

.method static O()V
    .locals 8

    sget-object v1, Lcom/android/server/ssrm/DataTputMonitor;->mListeners:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/server/ssrm/DataTputMonitor;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    sget v4, Lcom/android/server/ssrm/DataTputMonitor;->bl:I

    iput v4, v3, Landroid/os/Message;->what:I

    new-instance v4, Ljava/lang/Long;

    sget-wide v6, Lcom/android/server/ssrm/DataTputMonitor;->bk:J

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    iput-object v4, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    sget v4, Lcom/android/server/ssrm/DataTputMonitor;->bh:I

    iput v4, v3, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public static a(Landroid/os/Handler;)V
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/DataTputMonitor;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static init(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/DataTputMonitor;->bc:Lcom/android/server/ssrm/DataTputMonitor;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/ssrm/DataTputMonitor;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/DataTputMonitor;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/android/server/ssrm/DataTputMonitor;->bc:Lcom/android/server/ssrm/DataTputMonitor;

    sget-object v0, Lcom/android/server/ssrm/DataTputMonitor;->bc:Lcom/android/server/ssrm/DataTputMonitor;

    invoke-virtual {v0}, Lcom/android/server/ssrm/DataTputMonitor;->start()V

    :cond_0
    return-void
.end method


# virtual methods
.method public M()I
    .locals 3

    iget-object v0, p0, Lcom/android/server/ssrm/DataTputMonitor;->be:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-nez v0, :cond_0

    sget v0, Lcom/android/server/ssrm/DataTputMonitor;->bg:I

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    sget v0, Lcom/android/server/ssrm/DataTputMonitor;->TYPE_WIFI:I

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "LTE"

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/android/server/ssrm/DataTputMonitor;->TYPE_LTE:I

    goto :goto_0

    :cond_2
    sget v0, Lcom/android/server/ssrm/DataTputMonitor;->bf:I

    goto :goto_0

    :cond_3
    sget v0, Lcom/android/server/ssrm/DataTputMonitor;->bg:I

    goto :goto_0
.end method

.method public run()V
    .locals 10

    :goto_0
    :try_start_0
    sget v0, Lcom/android/server/ssrm/DataTputMonitor;->bh:I

    sget v1, Lcom/android/server/ssrm/DataTputMonitor;->bf:I

    if-eq v0, v1, :cond_0

    sget v0, Lcom/android/server/ssrm/DataTputMonitor;->bh:I

    sget v1, Lcom/android/server/ssrm/DataTputMonitor;->TYPE_LTE:I

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-static {}, Landroid/net/TrafficStats;->getMobileTxBytes()J

    move-result-wide v0

    invoke-static {}, Landroid/net/TrafficStats;->getMobileRxBytes()J

    move-result-wide v2

    sget-wide v4, Lcom/android/server/ssrm/DataTputMonitor;->bk:J

    sget-wide v6, Lcom/android/server/ssrm/DataTputMonitor;->bi:J

    sub-long v6, v0, v6

    sget-wide v8, Lcom/android/server/ssrm/DataTputMonitor;->bj:J

    sub-long v8, v2, v8

    add-long/2addr v6, v8

    sput-wide v6, Lcom/android/server/ssrm/DataTputMonitor;->bk:J

    sput-wide v0, Lcom/android/server/ssrm/DataTputMonitor;->bi:J

    sput-wide v2, Lcom/android/server/ssrm/DataTputMonitor;->bj:J

    sget-wide v0, Lcom/android/server/ssrm/DataTputMonitor;->bk:J

    cmp-long v0, v4, v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/android/server/ssrm/DataTputMonitor;->O()V

    :cond_1
    const-wide/16 v0, 0x3e8

    invoke-static {v0, v1}, Lcom/android/server/ssrm/DataTputMonitor;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
