.class final Lcom/android/server/ssrm/D;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/server/ssrm/E;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a([Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    array-length v1, p1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    const-string v0, "Invalid parameter."

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x1

    aget-object v1, p1, v1

    const-string v2, "cpu_min_freq"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/android/server/ssrm/z;->P()Lcom/android/server/ssrm/d;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/android/server/ssrm/z;->P()Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/ssrm/d;->release()V

    invoke-static {v3}, Lcom/android/server/ssrm/z;->a(Lcom/android/server/ssrm/d;)Lcom/android/server/ssrm/d;

    const-string v1, "Cpu min freq lock is released."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v1, "There is no existing lock."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    const-string v2, "cpu_max_freq"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/android/server/ssrm/z;->Q()Lcom/android/server/ssrm/d;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/android/server/ssrm/z;->Q()Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/ssrm/d;->release()V

    invoke-static {v3}, Lcom/android/server/ssrm/z;->b(Lcom/android/server/ssrm/d;)Lcom/android/server/ssrm/d;

    const-string v1, "Cpu max freq lock is released."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    const-string v1, "There is no existing lock."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_5
    const-string v2, "core_num_min"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {}, Lcom/android/server/ssrm/z;->R()Lcom/android/server/ssrm/d;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-static {}, Lcom/android/server/ssrm/z;->R()Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/ssrm/d;->release()V

    invoke-static {v3}, Lcom/android/server/ssrm/z;->c(Lcom/android/server/ssrm/d;)Lcom/android/server/ssrm/d;

    const-string v1, "Cpu core num min lock is released."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_6
    const-string v1, "There is no existing lock."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_7
    const-string v2, "core_num_max"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/android/server/ssrm/z;->S()Lcom/android/server/ssrm/d;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-static {}, Lcom/android/server/ssrm/z;->S()Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/ssrm/d;->release()V

    invoke-static {v3}, Lcom/android/server/ssrm/z;->d(Lcom/android/server/ssrm/d;)Lcom/android/server/ssrm/d;

    const-string v1, "Cpu core num max lock is released."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_8
    const-string v1, "There is no existing lock."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
