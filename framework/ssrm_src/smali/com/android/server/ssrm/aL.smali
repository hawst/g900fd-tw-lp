.class public final Lcom/android/server/ssrm/aL;
.super Ljava/lang/Object;


# static fields
.field static C:Landroid/content/Context;

.field private static final TAG:Ljava/lang/String;

.field static ki:Lcom/android/server/ssrm/aP;

.field static kj:Z

.field private static kk:Z

.field static kl:Landroid/os/DVFSHelper;

.field static km:Landroid/os/DVFSHelper;

.field static kn:I

.field static ko:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    const-class v0, Lcom/android/server/ssrm/aL;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/aL;->TAG:Ljava/lang/String;

    sput-object v2, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    sput-boolean v1, Lcom/android/server/ssrm/aL;->kj:Z

    sput-boolean v1, Lcom/android/server/ssrm/aL;->kk:Z

    sput-object v2, Lcom/android/server/ssrm/aL;->kl:Landroid/os/DVFSHelper;

    sput-object v2, Lcom/android/server/ssrm/aL;->km:Landroid/os/DVFSHelper;

    sput v1, Lcom/android/server/ssrm/aL;->kn:I

    sput-boolean v1, Lcom/android/server/ssrm/aL;->ko:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static U()I
    .locals 1

    sget v0, Lcom/android/server/ssrm/aL;->kn:I

    return v0
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/aL;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static bA()I
    .locals 1

    sget v0, Lcom/android/server/ssrm/aL;->kn:I

    return v0
.end method

.method static bB()V
    .locals 2

    sget-boolean v0, Lcom/android/server/ssrm/aL;->kk:Z

    if-eqz v0, :cond_0

    const/16 v0, -0xc8

    sput v0, Lcom/android/server/ssrm/aL;->kn:I

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/aL;->TAG:Ljava/lang/String;

    const-string v1, "/sys/class/power_supply/battery/temp"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/android/server/ssrm/aL;->kn:I

    goto :goto_0
.end method

.method static bC()V
    .locals 2

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/aL;->ko:Z

    sget-boolean v0, Lcom/android/server/ssrm/aL;->kj:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/android/server/ssrm/aO;->bD()V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/server/ssrm/aN;

    invoke-direct {v1}, Lcom/android/server/ssrm/aN;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public static bz()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/ssrm/aL;->kj:Z

    return v0
.end method

.method public static getDuration()I
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    if-nez v0, :cond_0

    const/16 v0, 0x7d0

    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    iget v0, v0, Lcom/android/server/ssrm/aP;->duration:I

    goto :goto_0
.end method

.method static initialize(Landroid/content/Context;)V
    .locals 7

    const v3, 0xbebc200

    const v2, 0x9eb10

    const/16 v4, 0x82

    const/16 v6, 0x3c

    const/4 v5, 0x0

    sput-object p0, Lcom/android/server/ssrm/aL;->C:Landroid/content/Context;

    sget-boolean v0, Lcom/android/server/ssrm/N;->di:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->dE:Z

    if-eqz v0, :cond_2

    :cond_0
    new-instance v0, Lcom/android/server/ssrm/aP;

    const/16 v1, -0x46

    const/16 v3, 0xb1

    invoke-direct/range {v0 .. v6}, Lcom/android/server/ssrm/aP;-><init>(IIIIZI)V

    sput-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-boolean v0, Lcom/android/server/ssrm/N;->dj:Z

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/android/server/ssrm/N;->dF:Z

    if-eqz v0, :cond_4

    :cond_3
    new-instance v0, Lcom/android/server/ssrm/aP;

    const/16 v1, -0x32

    const v2, 0x16da00

    invoke-direct/range {v0 .. v6}, Lcom/android/server/ssrm/aP;-><init>(IIIIZI)V

    sput-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    goto :goto_0

    :cond_4
    sget-boolean v0, Lcom/android/server/ssrm/N;->dx:Z

    if-nez v0, :cond_5

    sget-boolean v0, Lcom/android/server/ssrm/N;->dA:Z

    if-eqz v0, :cond_6

    :cond_5
    new-instance v0, Lcom/android/server/ssrm/aP;

    const/16 v1, -0x28

    const/16 v3, 0xb1

    invoke-direct/range {v0 .. v6}, Lcom/android/server/ssrm/aP;-><init>(IIIIZI)V

    sput-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    goto :goto_0

    :cond_6
    sget-boolean v0, Lcom/android/server/ssrm/N;->dC:Z

    if-eqz v0, :cond_7

    new-instance v0, Lcom/android/server/ssrm/aP;

    const v2, 0x16da00

    const/16 v4, 0x28

    move v1, v5

    invoke-direct/range {v0 .. v6}, Lcom/android/server/ssrm/aP;-><init>(IIIIZI)V

    sput-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    goto :goto_0

    :cond_7
    sget-boolean v0, Lcom/android/server/ssrm/N;->ds:Z

    if-nez v0, :cond_8

    sget-boolean v0, Lcom/android/server/ssrm/N;->dv:Z

    if-eqz v0, :cond_9

    :cond_8
    new-instance v0, Lcom/android/server/ssrm/aP;

    const/16 v3, 0xb1

    move v1, v5

    invoke-direct/range {v0 .. v6}, Lcom/android/server/ssrm/aP;-><init>(IIIIZI)V

    sput-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    goto :goto_0

    :cond_9
    sget-boolean v0, Lcom/android/server/ssrm/N;->dt:Z

    if-nez v0, :cond_a

    sget-boolean v0, Lcom/android/server/ssrm/N;->dw:Z

    if-eqz v0, :cond_b

    :cond_a
    new-instance v0, Lcom/android/server/ssrm/aP;

    const/16 v1, -0x28

    const v2, 0x16da00

    const/16 v4, 0x9b

    const/16 v6, 0x14

    invoke-direct/range {v0 .. v6}, Lcom/android/server/ssrm/aP;-><init>(IIIIZI)V

    sput-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    goto :goto_0

    :cond_b
    sget-boolean v0, Lcom/android/server/ssrm/N;->dH:Z

    if-eqz v0, :cond_c

    new-instance v0, Lcom/android/server/ssrm/aP;

    const/16 v1, -0x32

    const v2, 0xc0300

    const/16 v4, 0x28

    invoke-direct/range {v0 .. v6}, Lcom/android/server/ssrm/aP;-><init>(IIIIZI)V

    sput-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    goto :goto_0

    :cond_c
    sget-boolean v0, Lcom/android/server/ssrm/N;->cX:Z

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/server/ssrm/aP;

    const/16 v3, 0xa0

    const/16 v4, 0xc8

    const/16 v6, 0x1e

    move v1, v5

    invoke-direct/range {v0 .. v6}, Lcom/android/server/ssrm/aP;-><init>(IIIIZI)V

    sput-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    goto/16 :goto_0
.end method

.method public static run()V
    .locals 2

    sget-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/server/ssrm/aM;

    invoke-direct {v1}, Lcom/android/server/ssrm/aM;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
