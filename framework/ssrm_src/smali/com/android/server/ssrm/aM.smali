.class final Lcom/android/server/ssrm/aM;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v6, -0x1

    const-wide/16 v0, 0x7d0

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    invoke-static {}, Lcom/android/server/ssrm/aL;->bB()V

    invoke-static {}, Lcom/android/server/ssrm/aL;->U()I

    move-result v0

    sget-object v1, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    iget v1, v1, Lcom/android/server/ssrm/aP;->ks:I

    if-le v0, v1, :cond_1

    # getter for: Lcom/android/server/ssrm/aL;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/aL;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "run:: The PreSIOP is not activated. bat = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    sput-boolean v1, Lcom/android/server/ssrm/aL;->kj:Z

    # getter for: Lcom/android/server/ssrm/aL;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/aL;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "run:: The PreSIOP is activated. bat = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    iget v0, v0, Lcom/android/server/ssrm/aP;->kt:I

    if-eq v0, v6, :cond_2

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lcom/android/server/ssrm/aL;->C:Landroid/content/Context;

    const-string v2, "PRESIOP_ARM_MAX"

    const/16 v3, 0xd

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    sput-object v0, Lcom/android/server/ssrm/aL;->kl:Landroid/os/DVFSHelper;

    sget-object v0, Lcom/android/server/ssrm/aL;->kl:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    sget-object v2, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    iget v2, v2, Lcom/android/server/ssrm/aP;->kt:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    sget-object v0, Lcom/android/server/ssrm/aL;->kl:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    :cond_2
    sget-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    iget v0, v0, Lcom/android/server/ssrm/aP;->ku:I

    if-eq v0, v6, :cond_3

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lcom/android/server/ssrm/aL;->C:Landroid/content/Context;

    const-string v2, "PRESIOP_GPU_MAX"

    const/16 v3, 0x11

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    sput-object v0, Lcom/android/server/ssrm/aL;->km:Landroid/os/DVFSHelper;

    sget-object v0, Lcom/android/server/ssrm/aL;->km:Landroid/os/DVFSHelper;

    const-string v1, "GPU"

    sget-object v2, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    iget v2, v2, Lcom/android/server/ssrm/aP;->ku:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    sget-object v0, Lcom/android/server/ssrm/aL;->km:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    :cond_3
    sget-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    iget v0, v0, Lcom/android/server/ssrm/aP;->kv:I

    if-eq v0, v6, :cond_4

    sget-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    iget v0, v0, Lcom/android/server/ssrm/aP;->kv:I

    invoke-static {v0}, Lcom/android/server/ssrm/aO;->P(I)V

    :cond_4
    sget-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    iget-boolean v0, v0, Lcom/android/server/ssrm/aP;->kw:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/server/ssrm/aO;->Q(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method
