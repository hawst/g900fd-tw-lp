.class public Lcom/android/server/ssrm/a;
.super Ljava/lang/Object;


# static fields
.field static TAG:Ljava/lang/String;


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field final i:Ljava/lang/String;

.field final j:Landroid/content/Intent;

.field final k:Ljava/lang/String;

.field final l:Ljava/lang/String;

.field final m:Ljava/lang/String;

.field final n:Ljava/lang/String;

.field private final o:Ljava/io/File;

.field p:Ljava/util/HashMap;

.field q:[Ljava/lang/String;

.field r:Z

.field s:Z

.field t:Z

.field u:Ljava/lang/Runnable;

.field private final v:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/server/ssrm/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/a;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 8

    const/16 v7, 0x29e

    const/16 v6, 0x14

    const/4 v5, 0x1

    const/16 v4, 0x2bc

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/android/server/ssrm/a;->a:I

    iput v3, p0, Lcom/android/server/ssrm/a;->b:I

    const/16 v0, 0x320

    iput v0, p0, Lcom/android/server/ssrm/a;->c:I

    const/16 v0, 0x28a

    iput v0, p0, Lcom/android/server/ssrm/a;->d:I

    iput v4, p0, Lcom/android/server/ssrm/a;->e:I

    const/16 v0, 0x352

    iput v0, p0, Lcom/android/server/ssrm/a;->f:I

    iput v4, p0, Lcom/android/server/ssrm/a;->g:I

    const/16 v0, 0x2ee

    iput v0, p0, Lcom/android/server/ssrm/a;->h:I

    const-string v0, "android.intent.action.CHECK_COOLDOWN_LEVEL"

    iput-object v0, p0, Lcom/android/server/ssrm/a;->i:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CHECK_COOLDOWN_LEVEL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/ssrm/a;->j:Landroid/content/Intent;

    const-string v0, "battery_overheat_level"

    iput-object v0, p0, Lcom/android/server/ssrm/a;->k:Ljava/lang/String;

    const-string v0, "batt_temp_level"

    iput-object v0, p0, Lcom/android/server/ssrm/a;->l:Ljava/lang/String;

    const-string v0, "overheat_id"

    iput-object v0, p0, Lcom/android/server/ssrm/a;->m:Ljava/lang/String;

    const-string v0, "check_cooldown_list"

    iput-object v0, p0, Lcom/android/server/ssrm/a;->n:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    const/16 v1, 0x25

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Lcom/android/server/ssrm/a;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/ssrm/a;->o:Ljava/io/File;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/16 v1, 0x1b

    new-array v1, v1, [I

    fill-array-data v1, :array_1

    invoke-static {v1}, Lcom/android/server/ssrm/a;->a([I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    new-array v1, v6, [I

    fill-array-data v1, :array_2

    invoke-static {v1}, Lcom/android/server/ssrm/a;->a([I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    const/4 v1, 0x2

    const/16 v2, 0x11

    new-array v2, v2, [I

    fill-array-data v2, :array_3

    invoke-static {v2}, Lcom/android/server/ssrm/a;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-array v2, v6, [I

    fill-array-data v2, :array_4

    invoke-static {v2}, Lcom/android/server/ssrm/a;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0xf

    new-array v2, v2, [I

    fill-array-data v2, :array_5

    invoke-static {v2}, Lcom/android/server/ssrm/a;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0x13

    new-array v2, v2, [I

    fill-array-data v2, :array_6

    invoke-static {v2}, Lcom/android/server/ssrm/a;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x1c

    new-array v2, v2, [I

    fill-array-data v2, :array_7

    invoke-static {v2}, Lcom/android/server/ssrm/a;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/android/server/ssrm/a;->q:[Ljava/lang/String;

    iput-boolean v3, p0, Lcom/android/server/ssrm/a;->r:Z

    iput-boolean v3, p0, Lcom/android/server/ssrm/a;->s:Z

    iput-boolean v3, p0, Lcom/android/server/ssrm/a;->t:Z

    new-instance v0, Lcom/android/server/ssrm/b;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/b;-><init>(Lcom/android/server/ssrm/a;)V

    iput-object v0, p0, Lcom/android/server/ssrm/a;->u:Ljava/lang/Runnable;

    new-instance v0, Ljava/io/File;

    const/16 v1, 0x1a

    new-array v1, v1, [I

    fill-array-data v1, :array_8

    invoke-static {v1}, Lcom/android/server/ssrm/a;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/ssrm/a;->v:Ljava/io/File;

    iget-object v0, p0, Lcom/android/server/ssrm/a;->o:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v5, p0, Lcom/android/server/ssrm/a;->r:Z

    :cond_0
    iget-boolean v0, p0, Lcom/android/server/ssrm/a;->r:Z

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->s(Z)V

    sget-boolean v0, Lcom/android/server/ssrm/N;->dr:Z

    if-eqz v0, :cond_1

    iput v7, p0, Lcom/android/server/ssrm/a;->e:I

    const/16 v0, 0x2cb

    iput v0, p0, Lcom/android/server/ssrm/a;->c:I

    iput v4, p0, Lcom/android/server/ssrm/a;->d:I

    iput v7, p0, Lcom/android/server/ssrm/a;->h:I

    const/16 v0, 0x2cb

    iput v0, p0, Lcom/android/server/ssrm/a;->f:I

    iput v4, p0, Lcom/android/server/ssrm/a;->g:I

    :cond_1
    return-void

    :array_0
    .array-data 4
        0x55
        0x1e
        0x1b
        0xe
        0x1b
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x9
        0x9
        0x8
        0x17
        0x25
        0xc
        0x48
        0x54
        0x19
        0x15
        0x15
        0x16
        0x1e
        0x15
        0xd
        0x14
        0x25
        0x1e
        0x13
        0x9
        0x1b
        0x18
        0x16
        0x1f
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x11
        0x14
        0x15
        0x2
        0x54
        0x19
        0x15
        0x14
        0xe
        0x1b
        0x13
        0x14
        0x1f
        0x8
        0x1b
        0x1d
        0x1f
        0x14
        0xe
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x19
        0x15
        0x14
        0xe
        0x1b
        0x19
        0xe
        0x9
    .end array-data

    :array_3
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0xa
        0x12
        0x15
        0x14
        0x1f
    .end array-data

    :array_4
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x13
        0x14
        0x19
        0x1b
        0x16
        0x16
        0xf
        0x13
    .end array-data

    :array_5
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x17
        0x17
        0x9
    .end array-data

    :array_6
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x13
        0x17
        0x9
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
    .end array-data

    :array_7
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1b
        0x17
        0x9
        0xf
        0x14
        0x1d
        0x54
        0x19
        0x15
        0x17
        0x17
        0x15
        0x14
        0x13
        0x17
        0x9
        0x9
        0x1f
        0x8
        0xc
        0x13
        0x19
        0x1f
    .end array-data

    :array_8
    .array-data 4
        0x55
        0x19
        0x1b
        0x8
        0x8
        0x13
        0x1f
        0x8
        0x55
        0x19
        0x15
        0x15
        0x16
        0x1e
        0x15
        0xd
        0x14
        0x54
        0x9
        0x12
        0xf
        0xe
        0x1e
        0x15
        0xd
        0x14
    .end array-data
.end method

.method public static a(Ljava/io/File;I)I
    .locals 2

    const/4 v1, -0x1

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, v1, v1}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    move-result v0

    return v0
.end method

.method private static a([I)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/android/server/ssrm/aT;->a([I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/app/ActivityManager;)Ljava/util/ArrayList;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/16 v0, 0x3e8

    invoke-virtual {p0, v0}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    iget v3, v0, Landroid/app/ActivityManager$RunningTaskInfo;->numRunning:I

    if-lez v3, :cond_0

    iget-object v3, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v3, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method private static a()Ljava/util/HashMap;
    .locals 4

    invoke-static {}, Lcom/android/server/ssrm/ag;->bj()Lcom/android/server/ssrm/ag;

    move-result-object v0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v3, "android.intent.category.HOME"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static b(I)V
    .locals 7

    const/4 v6, 0x0

    invoke-static {}, Lcom/android/server/ssrm/ag;->bj()Lcom/android/server/ssrm/ag;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, v0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    iget-object v2, v0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    const v3, 0x10407a5

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget-object v2, v0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    const v3, 0x10407a4

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    const v2, 0x10407a6

    invoke-virtual {v0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/android/server/ssrm/c;

    invoke-direct {v2}, Lcom/android/server/ssrm/c;-><init>()V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d9

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method


# virtual methods
.method a(I)I
    .locals 12

    const/4 v2, 0x0

    invoke-static {}, Lcom/android/server/ssrm/ag;->bj()Lcom/android/server/ssrm/ag;

    move-result-object v3

    sget-object v0, Lcom/android/server/ssrm/a;->TAG:Ljava/lang/String;

    const-string v1, "killActiveApplications start"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v3, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-static {v0}, Lcom/android/server/ssrm/a;->a(Landroid/app/ActivityManager;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v5

    if-lez p1, :cond_8

    invoke-static {}, Lcom/android/server/ssrm/a;->a()Ljava/util/HashMap;

    move-result-object v6

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iget-object v8, p0, Lcom/android/server/ssrm/a;->q:[Ljava/lang/String;

    array-length v9, v8

    move v1, v2

    :goto_0
    if-ge v1, v9, :cond_0

    aget-object v10, v8, v1

    sget-object v11, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v7, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v1, v8, :cond_2

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    :goto_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v7, Lcom/android/server/ssrm/a;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " killActiveApplications : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    const-string v5, "com.sec.android.app.videoplayer"

    iget-object v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    sget-object v5, Lcom/android/server/ssrm/a;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " killActiveApplications : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v3, Lcom/android/server/ssrm/ag;->ie:Landroid/app/ActivityManager;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v5, v0}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_7
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/2addr v0, v2

    :goto_5
    return v0

    :cond_8
    const/4 v0, -0x1

    goto :goto_5
.end method

.method public a(II)V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-static {}, Lcom/android/server/ssrm/ag;->bj()Lcom/android/server/ssrm/ag;

    move-result-object v0

    iget-boolean v1, v0, Lcom/android/server/ssrm/ag;->aK:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/server/ssrm/a;->r:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Lcom/android/server/ssrm/a;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Current Battery Temperature : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", apTemp = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lcom/android/server/ssrm/a;->c:I

    if-ge p1, v1, :cond_2

    iget v1, p0, Lcom/android/server/ssrm/a;->f:I

    if-lt p2, v1, :cond_6

    :cond_2
    iput v6, p0, Lcom/android/server/ssrm/a;->a:I

    :goto_1
    iget v1, p0, Lcom/android/server/ssrm/a;->b:I

    iget v2, p0, Lcom/android/server/ssrm/a;->a:I

    if-eq v1, v2, :cond_5

    sget-object v1, Lcom/android/server/ssrm/a;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startBatteryCoolDownMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/ssrm/a;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lcom/android/server/ssrm/a;->a:I

    invoke-static {v1}, Lcom/android/server/ssrm/G;->l(I)V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/android/server/ssrm/a;->p:Ljava/util/HashMap;

    iget v1, p0, Lcom/android/server/ssrm/a;->a:I

    if-lez v1, :cond_c

    iget v0, p0, Lcom/android/server/ssrm/a;->a:I

    if-ne v0, v4, :cond_3

    iget v0, p0, Lcom/android/server/ssrm/a;->b:I

    if-eq v0, v6, :cond_3

    iget-object v0, p0, Lcom/android/server/ssrm/a;->j:Landroid/content/Intent;

    const-string v1, "battery_overheat_level"

    iget v2, p0, Lcom/android/server/ssrm/a;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-static {}, Lcom/android/server/ssrm/ag;->bj()Lcom/android/server/ssrm/ag;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/ssrm/a;->j:Landroid/content/Intent;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    :cond_3
    iget v0, p0, Lcom/android/server/ssrm/a;->a:I

    if-ne v0, v4, :cond_b

    iget v0, p0, Lcom/android/server/ssrm/a;->b:I

    if-eq v0, v6, :cond_b

    const v0, 0xea60

    :goto_2
    iget-boolean v1, p0, Lcom/android/server/ssrm/a;->t:Z

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/android/server/ssrm/ag;->bj()Lcom/android/server/ssrm/ag;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/server/ssrm/a;->u:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-boolean v5, p0, Lcom/android/server/ssrm/a;->t:Z

    :cond_4
    iput-boolean v4, p0, Lcom/android/server/ssrm/a;->t:Z

    invoke-static {}, Lcom/android/server/ssrm/ag;->bj()Lcom/android/server/ssrm/ag;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/server/ssrm/a;->u:Ljava/lang/Runnable;

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_5
    :goto_3
    iget v0, p0, Lcom/android/server/ssrm/a;->a:I

    iput v0, p0, Lcom/android/server/ssrm/a;->b:I

    goto/16 :goto_0

    :cond_6
    iget v1, p0, Lcom/android/server/ssrm/a;->e:I

    if-ge p1, v1, :cond_7

    iget v1, p0, Lcom/android/server/ssrm/a;->h:I

    if-lt p2, v1, :cond_8

    :cond_7
    iput v4, p0, Lcom/android/server/ssrm/a;->a:I

    goto/16 :goto_1

    :cond_8
    iget v1, p0, Lcom/android/server/ssrm/a;->b:I

    if-lez v1, :cond_a

    iget v1, p0, Lcom/android/server/ssrm/a;->d:I

    if-gt p1, v1, :cond_9

    iget v1, p0, Lcom/android/server/ssrm/a;->g:I

    if-le p2, v1, :cond_a

    :cond_9
    iput v4, p0, Lcom/android/server/ssrm/a;->a:I

    goto/16 :goto_1

    :cond_a
    iput v5, p0, Lcom/android/server/ssrm/a;->a:I

    goto/16 :goto_1

    :cond_b
    const/16 v0, 0x3e8

    goto :goto_2

    :cond_c
    iget-object v1, p0, Lcom/android/server/ssrm/a;->j:Landroid/content/Intent;

    const-string v2, "check_cooldown_list"

    iget-object v3, p0, Lcom/android/server/ssrm/a;->p:Ljava/util/HashMap;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/a;->j:Landroid/content/Intent;

    const-string v2, "batt_temp_level"

    iget v3, p0, Lcom/android/server/ssrm/a;->a:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/a;->j:Landroid/content/Intent;

    const-string v2, "battery_overheat_level"

    iget v3, p0, Lcom/android/server/ssrm/a;->a:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/a;->j:Landroid/content/Intent;

    const-string v2, "overheat_id"

    const v3, 0x10407a3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/ssrm/a;->j:Landroid/content/Intent;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    iput-boolean v5, p0, Lcom/android/server/ssrm/a;->s:Z

    goto :goto_3
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, "26653696"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/a;->o:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    :try_start_0
    const-string v0, "Cooldown mode : [OFF]"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/android/server/ssrm/a;->o:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ssrm/a;->r:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-boolean v0, p0, Lcom/android/server/ssrm/a;->r:Z

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->s(Z)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :cond_2
    const-string v0, "Cooldown mode : [ON]"

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/android/server/ssrm/a;->o:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    iput-boolean v2, p0, Lcom/android/server/ssrm/a;->r:Z

    goto :goto_1

    :cond_3
    const-string v1, "com.android.systemui.power.action.ACTION_REQUEST_SHUTDOWN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/android/server/ssrm/a;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_4

    :try_start_1
    sget-object v0, Lcom/android/server/ssrm/a;->TAG:Ljava/lang/String;

    const-string v1, " onReceive: create cooldown shutdown file"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/a;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    iget-object v0, p0, Lcom/android/server/ssrm/a;->v:Ljava/io/File;

    const/16 v1, 0x1a4

    invoke-static {v0, v1}, Lcom/android/server/ssrm/a;->a(Ljava/io/File;I)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_4
    :goto_3
    if-eqz p1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.ACTION_REQUEST_SHUTDOWN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.extra.KEY_CONFIRM"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget-object v1, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_2

    :catch_1
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    :cond_5
    const-string v1, "com.android.systemui.power.action.ACTION_CLEAR_SHUTDOWN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/a;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/a;->TAG:Ljava/lang/String;

    const-string v1, " onReceive: delete cooldown shutdown file."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/a;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_2
.end method
