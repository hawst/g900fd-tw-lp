.class Lcom/android/server/ssrm/at;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic jE:Lcom/android/server/ssrm/ag;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/ag;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/at;->jE:Lcom/android/server/ssrm/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    invoke-static {}, Lcom/android/server/ssrm/aG;->bw()Lcom/android/server/ssrm/aG;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-boolean v1, v0, Lcom/android/server/ssrm/aG;->kb:Z

    iget-boolean v2, v0, Lcom/android/server/ssrm/aG;->kc:Z

    if-eq v1, v2, :cond_1

    iget-boolean v1, v0, Lcom/android/server/ssrm/aG;->kc:Z

    iput-boolean v1, v0, Lcom/android/server/ssrm/aG;->kb:Z

    iget-boolean v1, v0, Lcom/android/server/ssrm/aG;->kb:Z

    invoke-static {v1}, Lb/p;->F(Z)V

    iget-boolean v1, v0, Lcom/android/server/ssrm/aG;->kb:Z

    invoke-static {v1}, Lcom/android/server/ssrm/ag;->u(Z)V

    :goto_2
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/server/ssrm/aG;->ka:Z

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v2, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onSIPIntentReceived:: InterruptedException = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v2, "onSIPIntentReceived:: Redundant SIP visibility update will be ignored."

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method
