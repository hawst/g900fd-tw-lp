.class Lcom/android/server/ssrm/aD;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic jE:Lcom/android/server/ssrm/ag;

.field jW:I

.field jX:I

.field jY:I

.field final jZ:I


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/ag;)V
    .locals 1

    const/4 v0, -0x1

    iput-object p1, p0, Lcom/android/server/ssrm/aD;->jE:Lcom/android/server/ssrm/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/android/server/ssrm/aD;->jW:I

    iput v0, p0, Lcom/android/server/ssrm/aD;->jX:I

    iput v0, p0, Lcom/android/server/ssrm/aD;->jY:I

    const/16 v0, 0x7530

    iput v0, p0, Lcom/android/server/ssrm/aD;->jZ:I

    return-void
.end method


# virtual methods
.method public bv()V
    .locals 4

    const/4 v3, -0x1

    invoke-static {}, Lcom/android/server/ssrm/aL;->bz()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lcom/android/server/ssrm/ag;->ir:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/server/ssrm/aD;->jW:I

    iput v0, p0, Lcom/android/server/ssrm/aD;->jY:I

    iput v3, p0, Lcom/android/server/ssrm/aD;->jW:I

    :cond_1
    :goto_1
    iget v0, p0, Lcom/android/server/ssrm/aD;->jX:I

    iget v1, p0, Lcom/android/server/ssrm/aD;->jW:I

    if-eq v0, v1, :cond_2

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "limitBrightness : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/ssrm/aD;->jW:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/aD;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mPowerManager:Landroid/os/PowerManager;

    iget v1, p0, Lcom/android/server/ssrm/aD;->jW:I

    invoke-virtual {v0, v3, v1}, Landroid/os/PowerManager;->setMasterBrightnessLimit(II)V

    iget-object v0, p0, Lcom/android/server/ssrm/aD;->jE:Lcom/android/server/ssrm/ag;

    iget-boolean v0, v0, Lcom/android/server/ssrm/ag;->aK:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/aD;->jE:Lcom/android/server/ssrm/ag;

    iget v1, p0, Lcom/android/server/ssrm/aD;->jW:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/android/server/ssrm/ag;->c(IZ)V

    :cond_2
    iget v0, p0, Lcom/android/server/ssrm/aD;->jW:I

    iput v0, p0, Lcom/android/server/ssrm/aD;->jX:I

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/android/server/ssrm/aD;->jY:I

    if-eq v0, v3, :cond_1

    sget-boolean v0, Lcom/android/server/ssrm/ag;->ir:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/android/server/ssrm/aD;->jY:I

    iput v0, p0, Lcom/android/server/ssrm/aD;->jW:I

    iput v3, p0, Lcom/android/server/ssrm/aD;->jY:I

    goto :goto_1
.end method

.method public run()V
    .locals 4

    invoke-virtual {p0}, Lcom/android/server/ssrm/aD;->bv()V

    iget-object v0, p0, Lcom/android/server/ssrm/aD;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
