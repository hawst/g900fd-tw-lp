.class public Lcom/android/server/ssrm/aX;
.super Landroid/database/sqlite/SQLiteOpenHelper;


# static fields
.field static final DATABASE_VERSION:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field public static final lB:Ljava/lang/String;

.field private static lC:Lcom/android/server/ssrm/aX;


# instance fields
.field final QUERY:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x19

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/android/server/ssrm/aX;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/aX;->TAG:Ljava/lang/String;

    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lcom/android/server/ssrm/aX;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/aX;->lB:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/ssrm/aX;->lC:Lcom/android/server/ssrm/aX;

    return-void

    :array_0
    .array-data 4
        0x29
        0x29
        0x28
        0x37
        0xc
        0x48
        0x40
        0x29
        0x9
        0x8
        0x17
        0x3e
        0x1b
        0xe
        0x1b
        0x18
        0x1b
        0x9
        0x1f
        0x32
        0x1f
        0x16
        0xa
        0x1f
        0x8
    .end array-data

    :array_1
    .array-data 4
        0x9
        0x9
        0x8
        0x17
        0x25
        0x15
        0x8
        0x1d
        0x54
        0x1e
        0x18
    .end array-data
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    const/16 v0, 0x34

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/android/server/ssrm/aX;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/aX;->QUERY:Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 4
        0x39
        0x28
        0x3f
        0x3b
        0x2e
        0x3f
        0x5a
        0x2e
        0x3b
        0x38
        0x36
        0x3f
        0x5a
        0xe
        0x5a
        0x52
        0x5a
        0x19
        0x1b
        0xe
        0x1f
        0x1d
        0x15
        0x8
        0x3
        0x5a
        0x2e
        0x3f
        0x22
        0x2e
        0x56
        0x5a
        0xa
        0x1b
        0x19
        0x11
        0x1b
        0x1d
        0x1f
        0x25
        0x16
        0x13
        0x9
        0xe
        0x5a
        0x2e
        0x3f
        0x22
        0x2e
        0x5a
        0x53
        0x41
    .end array-data
.end method

.method public static a([I)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/android/server/ssrm/aT;->a([I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized d(Landroid/content/Context;)Lcom/android/server/ssrm/aX;
    .locals 3

    const-class v1, Lcom/android/server/ssrm/aX;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/server/ssrm/aX;->lC:Lcom/android/server/ssrm/aX;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/ssrm/aX;

    sget-object v2, Lcom/android/server/ssrm/aX;->lB:Ljava/lang/String;

    invoke-direct {v0, p0, v2}, Lcom/android/server/ssrm/aX;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/ssrm/aX;->lC:Lcom/android/server/ssrm/aX;

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/aX;->lC:Lcom/android/server/ssrm/aX;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    sget-object v0, Lcom/android/server/ssrm/aX;->TAG:Ljava/lang/String;

    const-string v1, "onCreate:: "

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/aX;->QUERY:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    sget-object v0, Lcom/android/server/ssrm/aX;->TAG:Ljava/lang/String;

    const-string v1, "onUpgrade:: "

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
