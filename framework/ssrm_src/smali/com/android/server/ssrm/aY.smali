.class public Lcom/android/server/ssrm/aY;
.super Landroid/database/sqlite/SQLiteSecureOpenHelper;


# static fields
.field static final DATABASE_VERSION:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field static final lB:Ljava/lang/String;

.field static final lD:Ljava/lang/String;

.field static lE:[B

.field static lF:Lcom/android/server/ssrm/aY;


# instance fields
.field lG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x1f

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/android/server/ssrm/aY;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/aY;->TAG:Ljava/lang/String;

    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lcom/android/server/ssrm/aY;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/aY;->lD:Ljava/lang/String;

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lcom/android/server/ssrm/aY;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/aY;->lB:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/ssrm/aY;->lF:Lcom/android/server/ssrm/aY;

    return-void

    nop

    :array_0
    .array-data 4
        0x29
        0x29
        0x28
        0x37
        0xc
        0x48
        0x40
        0x29
        0x9
        0x8
        0x17
        0x29
        0x1f
        0x19
        0xf
        0x8
        0x1f
        0x3e
        0x1b
        0xe
        0x1b
        0x18
        0x1b
        0x9
        0x1f
        0x32
        0x1f
        0x16
        0xa
        0x1f
        0x8
    .end array-data

    :array_1
    .array-data 4
        0x9
        0x9
        0x8
        0x17
        0x25
        0x15
        0x8
        0x1d
        0x54
        0x1e
        0x18
    .end array-data

    :array_2
    .array-data 4
        0x9
        0x9
        0x8
        0x17
        0x25
        0x9
        0x1f
        0x19
        0xf
        0x8
        0x1f
        0x54
        0x1e
        0x18
    .end array-data
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteSecureOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/aY;->lG:Ljava/lang/String;

    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/android/server/ssrm/aY;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/aY;->lG:Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 4
        0x55
        0x1e
        0x1b
        0xe
        0x1b
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
    .end array-data
.end method

.method public static a([I)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/android/server/ssrm/aT;->a([I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized e(Landroid/content/Context;)Lcom/android/server/ssrm/aY;
    .locals 3

    const-class v1, Lcom/android/server/ssrm/aY;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/server/ssrm/aY;->lF:Lcom/android/server/ssrm/aY;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/ssrm/aY;

    sget-object v2, Lcom/android/server/ssrm/aY;->lB:Ljava/lang/String;

    invoke-direct {v0, p0, v2}, Lcom/android/server/ssrm/aY;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/android/server/ssrm/aY;->lF:Lcom/android/server/ssrm/aY;

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/aY;->lF:Lcom/android/server/ssrm/aY;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public V(Ljava/lang/String;)V
    .locals 4

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/server/ssrm/aY;->lG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/server/ssrm/aY;->lD:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/android/server/ssrm/aY;->lG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/android/server/ssrm/aY;->lB:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v0, Lcom/android/server/ssrm/aY;->TAG:Ljava/lang/String;

    const-string v1, "Original Plain database file does not exist"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    sget-object v2, Lcom/android/server/ssrm/aY;->TAG:Ljava/lang/String;

    const-string v3, "Delete secure database file"

    invoke-static {v2, v3}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    sput-object v2, Lcom/android/server/ssrm/aY;->lE:[B

    :try_start_0
    sget-object v2, Lcom/android/server/ssrm/aY;->lE:[B

    invoke-static {v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->convert2SecureDB(Ljava/io/File;Ljava/io/File;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    sget-object v0, Lcom/android/server/ssrm/aY;->TAG:Ljava/lang/String;

    const-string v1, "onCreate::"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2

    sget-object v0, Lcom/android/server/ssrm/aY;->TAG:Ljava/lang/String;

    const-string v1, "onUpgrade::"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
