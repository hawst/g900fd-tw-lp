.class public Lcom/android/server/ssrm/aR;
.super Ljava/lang/Object;


# instance fields
.field public fA:Ljava/lang/String;

.field public fB:Ljava/lang/String;

.field public fC:I

.field public fD:I

.field public fn:Ljava/lang/String;

.field public fo:Ljava/lang/String;

.field public fp:Ljava/lang/String;

.field public fq:I

.field public fr:I

.field public fw:Ljava/lang/String;

.field public fz:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/QcFalImpl$PlatformType;)V
    .locals 4

    const/16 v3, 0x1bbe

    const/16 v2, 0x23c

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fn:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fo:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fp:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fw:Ljava/lang/String;

    iput v1, p0, Lcom/android/server/ssrm/aR;->fq:I

    iput v1, p0, Lcom/android/server/ssrm/aR;->fr:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fz:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fA:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fB:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/aR;->fC:I

    iput v3, p0, Lcom/android/server/ssrm/aR;->fD:I

    sget-object v0, Lcom/android/server/ssrm/aQ;->kK:[I

    invoke-virtual {p1}, Lcom/android/server/ssrm/QcFalImpl$PlatformType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    sget-boolean v0, Lcom/android/server/ssrm/N;->cV:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->de:Z

    if-eqz v0, :cond_2

    :cond_0
    const-string v0, "/sys/class/devfreq/0.qcom,cpubw/min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fz:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/0.qcom,cpubw/max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fA:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/0.qcom,cpubw/available_frequencies"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fB:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/aR;->fC:I

    iput v3, p0, Lcom/android/server/ssrm/aR;->fD:I

    :cond_1
    :goto_1
    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BUS_MIN_DEFAULT_FREQUENCY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/ssrm/aR;->fC:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BUS_MAX_DEFAULT_FREQUENCY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/ssrm/aR;->fD:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_0
    const-string v0, "/sys/class/kgsl/kgsl-3d0/min_pwrlevel"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fn:Ljava/lang/String;

    const-string v0, "/sys/class/kgsl/kgsl-3d0/max_pwrlevel"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fo:Ljava/lang/String;

    const-string v0, "/sys/class/kgsl/kgsl-3d0/gpu_available_frequencies"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fp:Ljava/lang/String;

    const-string v0, "/sys/devices/system/cpu/kernel_max"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fw:Ljava/lang/String;

    goto :goto_0

    :pswitch_1
    const-string v0, "/sys/class/kgsl/kgsl-3d0/min_pwrlevel"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fn:Ljava/lang/String;

    const-string v0, "/sys/class/kgsl/kgsl-3d0/max_pwrlevel"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fo:Ljava/lang/String;

    const-string v0, "/sys/class/kgsl/kgsl-3d0/gpu_available_frequencies"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fp:Ljava/lang/String;

    const-string v0, "/sys/devices/system/cpu/kernel_max"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fw:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/0.qcom,cpubw/min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fz:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/0.qcom,cpubw/max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fA:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/0.qcom,cpubw/available_frequencies"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fB:Ljava/lang/String;

    const/16 v0, 0x2fa

    iput v0, p0, Lcom/android/server/ssrm/aR;->fC:I

    const/16 v0, 0xfe2

    iput v0, p0, Lcom/android/server/ssrm/aR;->fD:I

    goto/16 :goto_0

    :cond_2
    sget-boolean v0, Lcom/android/server/ssrm/N;->cY:Z

    if-eqz v0, :cond_1

    const-string v0, "/sys/class/devfreq/0.qcom,cpubw/min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fz:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/0.qcom,cpubw/max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fA:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/0.qcom,cpubw/available_frequencies"

    iput-object v0, p0, Lcom/android/server/ssrm/aR;->fB:Ljava/lang/String;

    sget-object v0, Lcom/android/server/ssrm/QcFalImpl;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/server/ssrm/aR;->fB:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/ssrm/bg;->Y(Ljava/lang/String;)Lcom/android/server/ssrm/bh;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/android/server/ssrm/bh;->min:I

    iput v1, p0, Lcom/android/server/ssrm/aR;->fC:I

    iget v0, v0, Lcom/android/server/ssrm/bh;->max:I

    iput v0, p0, Lcom/android/server/ssrm/aR;->fD:I

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
