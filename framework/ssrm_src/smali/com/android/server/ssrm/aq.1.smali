.class Lcom/android/server/ssrm/aq;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic jE:Lcom/android/server/ssrm/ag;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/ag;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/aq;->jE:Lcom/android/server/ssrm/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :try_start_0
    invoke-static {}, Lcom/android/server/ssrm/aL;->bz()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "PreSIOP is working now."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/server/ssrm/aq;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/server/ssrm/aq;->jE:Lcom/android/server/ssrm/ag;

    invoke-virtual {v0}, Lcom/android/server/ssrm/ag;->bk()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->eD:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/aq;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->ik:Lcom/android/server/ssrm/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/aq;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->ik:Lcom/android/server/ssrm/a;

    sget v1, Lcom/android/server/ssrm/ag;->iq:I

    sget v2, Lcom/android/server/ssrm/ag;->ip:I

    invoke-virtual {v0, v1, v2}, Lcom/android/server/ssrm/a;->a(II)V

    :cond_2
    sget-object v0, Lcom/android/server/ssrm/ag;->iO:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/ssrm/aE;

    sget v2, Lcom/android/server/ssrm/ag;->ip:I

    invoke-interface {v0, v2}, Lcom/android/server/ssrm/aE;->O(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mThermistorObeserverTask:: e = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
