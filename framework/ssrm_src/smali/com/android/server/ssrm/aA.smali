.class Lcom/android/server/ssrm/aA;
.super Ljava/lang/Object;


# instance fields
.field final synthetic jE:Lcom/android/server/ssrm/ag;

.field jK:Z

.field final jL:I

.field final jM:I

.field jN:Ljava/lang/Runnable;

.field mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/ag;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 4

    iput-object p1, p0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/ssrm/aA;->jK:Z

    const/16 v0, 0x7d0

    iput v0, p0, Lcom/android/server/ssrm/aA;->jL:I

    const/16 v0, 0x2710

    iput v0, p0, Lcom/android/server/ssrm/aA;->jM:I

    new-instance v0, Lcom/android/server/ssrm/aB;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/aB;-><init>(Lcom/android/server/ssrm/aA;)V

    iput-object v0, p0, Lcom/android/server/ssrm/aA;->jN:Ljava/lang/Runnable;

    iput-object p2, p0, Lcom/android/server/ssrm/aA;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/android/server/ssrm/aA;->mHandler:Landroid/os/Handler;

    iget-object v0, p0, Lcom/android/server/ssrm/aA;->jN:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {p3, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method public J(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lcom/android/server/ssrm/N;->eC:Z

    if-eqz v0, :cond_0

    const-string v0, "com.google.android.talk"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "sys.hangouts.fps"

    const-string v1, "15000"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->il:Lcom/android/server/ssrm/aA;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/server/ssrm/aA;->jK:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "sys.hangouts.fps"

    const-string v1, "-1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->il:Lcom/android/server/ssrm/aA;

    iput-boolean v2, v0, Lcom/android/server/ssrm/aA;->jK:Z

    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_2

    const-string v0, "HangoutsVTCall"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_2
    iget-object v0, p0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iX:Lb/N;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iX:Lb/N;

    invoke-virtual {v0, v2}, Lb/N;->M(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iY:Lb/P;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iY:Lb/P;

    invoke-virtual {v0, v2}, Lb/P;->M(Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iZ:Lb/T;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->iZ:Lb/T;

    invoke-virtual {v0, v2}, Lb/T;->M(Z)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->ja:Lb/R;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/aA;->jE:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->ja:Lb/R;

    invoke-virtual {v0, v2}, Lb/R;->M(Z)V

    goto :goto_0
.end method
