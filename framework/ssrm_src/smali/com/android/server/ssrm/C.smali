.class final Lcom/android/server/ssrm/C;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/server/ssrm/E;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a([Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    array-length v1, p1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    const-string v0, "Invalid parameter."

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x1

    aget-object v1, p1, v1

    const/4 v2, 0x2

    aget-object v2, p1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "cpu_min_freq"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/android/server/ssrm/z;->P()Lcom/android/server/ssrm/d;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/android/server/ssrm/z;->P()Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/ssrm/d;->release()V

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/android/server/ssrm/z;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/z;->access$100()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "_CPU_MIN_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/d;->a(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/ssrm/z;->a(Lcom/android/server/ssrm/d;)Lcom/android/server/ssrm/d;

    invoke-static {}, Lcom/android/server/ssrm/z;->P()Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/ssrm/d;->acquire()V

    const-string v1, "Cpu min freq lock is acquired."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v3, "cpu_max_freq"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {}, Lcom/android/server/ssrm/z;->Q()Lcom/android/server/ssrm/d;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/android/server/ssrm/z;->Q()Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/ssrm/d;->release()V

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/android/server/ssrm/z;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/z;->access$100()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "_CPU_MAX_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/d;->b(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/ssrm/z;->b(Lcom/android/server/ssrm/d;)Lcom/android/server/ssrm/d;

    invoke-static {}, Lcom/android/server/ssrm/z;->Q()Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/ssrm/d;->acquire()V

    const-string v1, "Cpu max freq lock is acquired."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    const-string v3, "core_num_min"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-static {}, Lcom/android/server/ssrm/z;->R()Lcom/android/server/ssrm/d;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/android/server/ssrm/z;->R()Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/ssrm/d;->release()V

    :cond_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/android/server/ssrm/z;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/z;->access$100()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "_CORE_MIN_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/d;->c(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/ssrm/z;->c(Lcom/android/server/ssrm/d;)Lcom/android/server/ssrm/d;

    invoke-static {}, Lcom/android/server/ssrm/z;->R()Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/ssrm/d;->acquire()V

    const-string v1, "Cpu core num min lock is acquired."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    :cond_6
    const-string v3, "core_num_max"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {}, Lcom/android/server/ssrm/z;->S()Lcom/android/server/ssrm/d;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-static {}, Lcom/android/server/ssrm/z;->S()Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/ssrm/d;->release()V

    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/android/server/ssrm/z;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/z;->access$100()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "_CORE_MAX_"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/d;->d(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-static {v1}, Lcom/android/server/ssrm/z;->d(Lcom/android/server/ssrm/d;)Lcom/android/server/ssrm/d;

    invoke-static {}, Lcom/android/server/ssrm/z;->S()Lcom/android/server/ssrm/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/server/ssrm/d;->acquire()V

    const-string v1, "Cpu core num max lock is acquired."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    :cond_8
    const-string v1, "Invalid parameter."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1
.end method
