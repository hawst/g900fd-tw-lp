.class Lcom/android/server/ssrm/K;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic cv:Lcom/android/server/ssrm/J;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/J;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/K;->cv:Lcom/android/server/ssrm/J;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/android/server/ssrm/K;->cv:Lcom/android/server/ssrm/J;

    iget-object v0, v0, Lcom/android/server/ssrm/J;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/emergencymode/EmergencyManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/EmergencyManager;

    move-result-object v1

    const-string v2, "dev.ssrm.emergencymode"

    invoke-virtual {v1}, Lcom/sec/android/emergencymode/EmergencyManager;->isEmergencyMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "true"

    :goto_0
    invoke-static {v2, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/K;->cv:Lcom/android/server/ssrm/J;

    invoke-virtual {v1}, Lcom/sec/android/emergencymode/EmergencyManager;->isEmergencyMode()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/server/ssrm/J;->j(Z)V

    return-void

    :cond_0
    const-string v0, "false"

    goto :goto_0
.end method
