.class public Lcom/android/server/ssrm/aS;
.super Ljava/lang/Object;


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field kT:I

.field kU:I

.field kV:I

.field kW:I

.field kX:I

.field kY:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/server/ssrm/aS;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/aS;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 2

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/android/server/ssrm/aS;->kT:I

    iput v0, p0, Lcom/android/server/ssrm/aS;->kU:I

    iput v0, p0, Lcom/android/server/ssrm/aS;->kV:I

    iput v0, p0, Lcom/android/server/ssrm/aS;->kW:I

    iput v0, p0, Lcom/android/server/ssrm/aS;->kX:I

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/server/ssrm/aS;->kY:J

    invoke-virtual {p0}, Lcom/android/server/ssrm/aS;->bF()V

    return-void
.end method


# virtual methods
.method bF()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string v0, "280000,933"

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v1, v0, v2

    if-eqz v1, :cond_0

    aget-object v1, v0, v2

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/android/server/ssrm/aS;->kT:I

    :goto_0
    aget-object v1, v0, v3

    if-eqz v1, :cond_1

    aget-object v0, v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/server/ssrm/aS;->kU:I

    :goto_1
    const/16 v0, 0x33e

    iput v0, p0, Lcom/android/server/ssrm/aS;->kV:I

    const/16 v0, 0x2ee

    iput v0, p0, Lcom/android/server/ssrm/aS;->kW:I

    iget v0, p0, Lcom/android/server/ssrm/aS;->kV:I

    iget v1, p0, Lcom/android/server/ssrm/aS;->kW:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/android/server/ssrm/aS;->kX:I

    return-void

    :cond_0
    const v1, 0x445c0

    iput v1, p0, Lcom/android/server/ssrm/aS;->kT:I

    goto :goto_0

    :cond_1
    const/16 v0, 0x3a5

    iput v0, p0, Lcom/android/server/ssrm/aS;->kU:I

    goto :goto_1
.end method

.method getStandbyTimeInPowerSavingMode()I
    .locals 4

    invoke-static {}, Lcom/android/server/ssrm/G;->ad()I

    move-result v0

    iget v1, p0, Lcom/android/server/ssrm/aS;->kT:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/android/server/ssrm/aS;->kX:I

    div-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x6

    div-int/lit8 v0, v0, 0xa

    sget-object v1, Lcom/android/server/ssrm/aS;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getStandbyTimeInUltraPowerSavingMode: standby time = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mBatteryCapacity = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/ssrm/aS;->kT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mBURInPowerSavingMode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/server/ssrm/aS;->kX:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    return v0
.end method

.method getStandbyTimeInUltraPowerSavingMode()I
    .locals 5

    invoke-static {}, Lcom/android/server/ssrm/G;->ad()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/aS;->TAG:Ljava/lang/String;

    const-string v1, "/sys/class/power_supply/battery/capacity"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    :cond_0
    iget v1, p0, Lcom/android/server/ssrm/aS;->kT:I

    mul-int/2addr v1, v0

    iget v2, p0, Lcom/android/server/ssrm/aS;->kU:I

    div-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x6

    div-int/lit8 v1, v1, 0xa

    sget-object v2, Lcom/android/server/ssrm/aS;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getStandbyTimeInUltraPowerSavingMode: standby time = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mBatteryCapacity = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/ssrm/aS;->kT:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mBURInUltraPowerSavingMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/server/ssrm/aS;->kU:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", level = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    return v1
.end method
