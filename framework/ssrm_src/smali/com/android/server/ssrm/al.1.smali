.class Lcom/android/server/ssrm/al;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/android/server/ssrm/ay;


# instance fields
.field final synthetic jE:Lcom/android/server/ssrm/ag;

.field jF:I


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/ag;)V
    .locals 1

    iput-object p1, p0, Lcom/android/server/ssrm/al;->jE:Lcom/android/server/ssrm/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/ssrm/al;->jF:I

    return-void
.end method


# virtual methods
.method public b(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_2

    const-string v0, "1"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v2}, Lb/p;->D(Z)V

    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_0

    const-string v0, "HeavyUserScenario"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_0
    iget v0, p0, Lcom/android/server/ssrm/al;->jF:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/ssrm/al;->jF:I

    iget v0, p0, Lcom/android/server/ssrm/al;->jF:I

    if-ne v0, v2, :cond_1

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/server/ssrm/am;

    invoke-direct {v1, p0}, Lcom/android/server/ssrm/am;-><init>(Lcom/android/server/ssrm/al;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-static {v1}, Lb/p;->D(Z)V

    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_3

    const-string v0, "HeavyUserScenario"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_3
    iput v1, p0, Lcom/android/server/ssrm/al;->jF:I

    goto :goto_0
.end method
