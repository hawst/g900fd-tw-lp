.class public final enum Lcom/android/server/ssrm/GenericFalImpl$PlatformType;
.super Ljava/lang/Enum;


# static fields
.field public static final enum eX:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field public static final enum eY:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field public static final enum eZ:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field public static final enum fa:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field public static final enum fb:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field public static final enum fc:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field public static final enum fd:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field public static final enum fe:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field public static final enum ff:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field public static final enum fg:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field public static final enum fh:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field public static final enum fi:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field public static final enum fj:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field public static final enum fk:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field public static final enum fl:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

.field private static final synthetic fm:[Lcom/android/server/ssrm/GenericFalImpl$PlatformType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->eX:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const-string v1, "MSM8064"

    invoke-direct {v0, v1, v4}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->eY:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const-string v1, "MSM8226"

    invoke-direct {v0, v1, v5}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->eZ:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const-string v1, "MSM8974"

    invoke-direct {v0, v1, v6}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fa:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const-string v1, "ADONIS"

    invoke-direct {v0, v1, v7}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fb:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const-string v1, "EXYNOS4"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fc:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const-string v1, "ADONISPRIME"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fd:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const-string v1, "ADONISPRIME2"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fe:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const-string v1, "HELSINKY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->ff:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const-string v1, "ADONISMINI"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fg:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const-string v1, "EXYNOS3"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fh:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const-string v1, "SC8830"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fi:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const-string v1, "PEGAPRO"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fj:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const-string v1, "HELSINKYPRIME"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fk:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const-string v1, "ISTOR"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fl:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    const/16 v0, 0xf

    new-array v0, v0, [Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->eX:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->eY:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->eZ:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fa:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fb:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fc:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fd:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fe:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->ff:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fg:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fh:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fi:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fj:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fk:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fl:Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fm:[Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/ssrm/GenericFalImpl$PlatformType;
    .locals 1

    const-class v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    return-object v0
.end method

.method public static values()[Lcom/android/server/ssrm/GenericFalImpl$PlatformType;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->fm:[Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    invoke-virtual {v0}, [Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/ssrm/GenericFalImpl$PlatformType;

    return-object v0
.end method
