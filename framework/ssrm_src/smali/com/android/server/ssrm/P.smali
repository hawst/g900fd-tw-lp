.class public Lcom/android/server/ssrm/P;
.super Ljava/lang/Object;


# instance fields
.field public fA:Ljava/lang/String;

.field public fB:Ljava/lang/String;

.field public fC:I

.field public fD:I

.field public fE:I

.field public fF:Ljava/lang/String;

.field public fn:Ljava/lang/String;

.field public fo:Ljava/lang/String;

.field public fp:Ljava/lang/String;

.field public fq:I

.field public fr:I

.field public fs:Ljava/lang/String;

.field public ft:I

.field public fu:Ljava/lang/String;

.field public fv:Ljava/lang/String;

.field public fw:Ljava/lang/String;

.field public fx:I

.field public fy:I

.field public fz:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;Lcom/android/server/ssrm/GenericFalImpl$PlatformType;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fn:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fq:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fr:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fs:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->ft:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fv:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fw:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fx:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fy:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fz:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fA:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fC:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fD:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fE:I

    const-string v0, "/sys/module/cpuidle/parameters/off"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fF:Ljava/lang/String;

    sget-object v0, Lcom/android/server/ssrm/O;->eR:[I

    invoke-virtual {p2}, Lcom/android/server/ssrm/GenericFalImpl$PlatformType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    # getter for: Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/GenericFalImpl;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GPU_MIN_DEFAULT_FREQUENCY = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/ssrm/P;->fq:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    # getter for: Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/GenericFalImpl;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GPU_MAX_DEFAULT_FREQUENCY = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/ssrm/P;->fr:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    # getter for: Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/GenericFalImpl;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BUS_MIN_DEFAULT_FREQUENCY = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/ssrm/P;->fC:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    # getter for: Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/GenericFalImpl;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BUS_MAX_DEFAULT_FREQUENCY = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/ssrm/P;->fD:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_0
    const-string v0, "/sys/devices/platform/pvrsrvkm.0/sgx_dvfs_min_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fn:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/pvrsrvkm.0/sgx_dvfs_max_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/pvrsrvkm.0/sgx_dvfs_table"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fq:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fr:I

    const-string v0, "/sys/class/graphics/fb0/lcdfreq/level"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fs:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->ft:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fv:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fw:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fx:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fy:I

    const-string v0, "/sys/class/devfreq/exynos5-busfreq-mif/min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fz:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/exynos5-busfreq-mif/max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fA:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/exynos5-busfreq-mif/freq_table"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_1
    const-string v0, "/sys/devices/platform/mali.0/dvfs_min_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fn:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/mali.0/dvfs_max_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/mali.0/dvfs_table"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fq:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fr:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fs:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->ft:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fv:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fw:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fx:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fy:I

    const-string v0, "/sys/class/devfreq/exynos5-busfreq-mif/min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fz:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/exynos5-busfreq-mif/max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fA:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/exynos5-busfreq-mif/freq_table"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_2
    const-string v0, "/sys/devices/11800000.mali/dvfs_min_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fn:Ljava/lang/String;

    const-string v0, "/sys/devices/11800000.mali/dvfs_max_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    const-string v0, "/sys/devices/11800000.mali/dvfs_table"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fq:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fr:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fs:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->ft:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fv:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fw:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fx:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fy:I

    const-string v0, "/sys/devices/platform/exynos5-devfreq-mif/devfreq/exynos5-devfreq-mif/min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fz:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/exynos5-devfreq-mif/devfreq/exynos5-devfreq-mif/max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fA:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/exynos5-devfreq-mif/devfreq/exynos5-devfreq-mif/available_frequencies"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_3
    const-string v0, "/sys/devices/14ac0000.mali/dvfs_min_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fn:Ljava/lang/String;

    const-string v0, "/sys/devices/14ac0000.mali/dvfs_max_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    const-string v0, "/sys/devices/14ac0000.mali/dvfs_table"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fq:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fr:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fs:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->ft:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fv:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fw:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fx:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fy:I

    const-string v0, "/sys/devices/platform/exynos5-devfreq-mif/devfreq/exynos5-devfreq-mif/min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fz:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/exynos5-devfreq-mif/devfreq/exynos5-devfreq-mif/max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fA:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/exynos5-devfreq-mif/devfreq/exynos5-devfreq-mif/available_frequencies"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_4
    const-string v0, "/sys/devices/14ac0000.mali/dvfs_min_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fn:Ljava/lang/String;

    const-string v0, "/sys/devices/14ac0000.mali/dvfs_max_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    const-string v0, "/sys/devices/14ac0000.mali/dvfs_table"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fq:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fr:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fs:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->ft:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fv:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fw:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fx:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fy:I

    const-string v0, "/sys/devices/platform/exynos5-devfreq-mif/devfreq/exynos5-devfreq-mif/min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fz:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/exynos5-devfreq-mif/devfreq/exynos5-devfreq-mif/max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fA:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/exynos5-devfreq-mif/devfreq/exynos5-devfreq-mif/available_frequencies"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    # getter for: Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/GenericFalImpl;->access$000()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/ssrm/bg;->Y(Ljava/lang/String;)Lcom/android/server/ssrm/bh;

    move-result-object v0

    iget v1, v0, Lcom/android/server/ssrm/bh;->min:I

    iput v1, p0, Lcom/android/server/ssrm/P;->fC:I

    iget v0, v0, Lcom/android/server/ssrm/bh;->max:I

    iput v0, p0, Lcom/android/server/ssrm/P;->fD:I

    goto/16 :goto_0

    :pswitch_5
    const-string v0, "/sys/devices/14ac0000.mali/dvfs_min_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fn:Ljava/lang/String;

    const-string v0, "/sys/devices/14ac0000.mali/dvfs_max_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    const-string v0, "/sys/devices/14ac0000.mali/dvfs_table"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    # getter for: Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/GenericFalImpl;->access$000()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/ssrm/bg;->Y(Ljava/lang/String;)Lcom/android/server/ssrm/bh;

    move-result-object v0

    iget v1, v0, Lcom/android/server/ssrm/bh;->min:I

    iput v1, p0, Lcom/android/server/ssrm/P;->fq:I

    iget v0, v0, Lcom/android/server/ssrm/bh;->max:I

    iput v0, p0, Lcom/android/server/ssrm/P;->fr:I

    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fs:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->ft:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fv:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fw:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fx:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fy:I

    const-string v0, "/sys/devices/platform/exynos7-devfreq-mif/devfreq/exynos7-devfreq-mif/min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fz:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/exynos7-devfreq-mif/devfreq/exynos7-devfreq-mif/max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fA:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/exynos7-devfreq-mif/devfreq/exynos7-devfreq-mif/available_frequencies"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    # getter for: Lcom/android/server/ssrm/GenericFalImpl;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/GenericFalImpl;->access$000()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/ssrm/bg;->Y(Ljava/lang/String;)Lcom/android/server/ssrm/bh;

    move-result-object v0

    iget v1, v0, Lcom/android/server/ssrm/bh;->min:I

    iput v1, p0, Lcom/android/server/ssrm/P;->fC:I

    iget v0, v0, Lcom/android/server/ssrm/bh;->max:I

    iput v0, p0, Lcom/android/server/ssrm/P;->fD:I

    goto/16 :goto_0

    :pswitch_6
    const-string v0, "/sys/devices/platform/mali.0/dvfs_min_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fn:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/mali.0/dvfs_max_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/mali.0/dvfs_table"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fq:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fr:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fs:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->ft:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fv:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fw:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fx:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fy:I

    const-string v0, "/sys/class/devfreq/exynos5-devfreq-mif/min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fz:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/exynos5-devfreq-mif/max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fA:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/exynos5-devfreq-mif/available_frequencies"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_7
    const-string v0, "/sys/module/mali/parameters/mali_min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fn:Ljava/lang/String;

    const-string v0, "/sys/module/mali/parameters/mali_max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    const-string v0, "/sys/module/mali/parameters/mali_freq_table"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fq:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fr:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fs:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->ft:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fv:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fw:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fx:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fy:I

    const-string v0, "/sys/class/devfreq/exynos4415-busfreq-mif/min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fz:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/exynos4415-busfreq-mif/max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fA:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/exynos4415-busfreq-mif/freq_table"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fC:I

    const v0, 0x84918

    iput v0, p0, Lcom/android/server/ssrm/P;->fD:I

    goto/16 :goto_0

    :pswitch_8
    const-string v0, "/sys/power/gpu_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fn:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fq:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fr:I

    const-string v0, "/sys/class/graphics/fb0/lcdfreq/level"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fs:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->ft:I

    const-string v0, "/sys/devices/system/cpu/cpufreq/pegasusq/min_cpu_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    const-string v0, "/sys/devices/system/cpu/cpufreq/pegasusq/max_cpu_lock"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fv:Ljava/lang/String;

    const-string v0, "/sys/devices/system/cpu/cpufreq/pegasusq/cpucore_table"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fw:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fx:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fy:I

    const-string v0, "/sys/devices/system/cpu/busfreq/min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fz:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fA:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_9
    const-string v0, "/sys/module/mali/parameters/mali_min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fn:Ljava/lang/String;

    const-string v0, "/sys/module/mali/parameters/mali_max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    const-string v0, "/sys/module/mali/parameters/mali_freq_table"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    const/16 v0, 0xa0

    iput v0, p0, Lcom/android/server/ssrm/P;->fq:I

    const/16 v0, 0x215

    iput v0, p0, Lcom/android/server/ssrm/P;->fr:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fs:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->ft:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fv:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fw:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fx:I

    iput v2, p0, Lcom/android/server/ssrm/P;->fy:I

    const-string v0, "/sys/class/devfreq/exynos4270-busfreq-mif/min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fz:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/exynos4270-busfreq-mif/max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fA:Ljava/lang/String;

    const-string v0, "/sys/class/devfreq/exynos4270-busfreq-mif/available_frequencies"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->fC:I

    const v0, 0x61a80

    iput v0, p0, Lcom/android/server/ssrm/P;->fD:I

    goto/16 :goto_0

    :pswitch_a
    const-string v0, "/sys/module/mali/parameters/gpufreq_min_limit"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fn:Ljava/lang/String;

    const-string v0, "/sys/module/mali/parameters/gpufreq_max_limit"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fo:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fp:Ljava/lang/String;

    const v0, 0x14d55

    iput v0, p0, Lcom/android/server/ssrm/P;->fq:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/server/ssrm/P;->fr:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fs:Ljava/lang/String;

    iput v2, p0, Lcom/android/server/ssrm/P;->ft:I

    const-string v0, "/sys/devices/system/cpu/cpufreq/sprdemand/cpu_num_min_limit"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fu:Ljava/lang/String;

    const-string v0, "/sys/devices/system/cpu/cpufreq/sprdemand/cpu_num_limit"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fv:Ljava/lang/String;

    const-string v0, "/sys/devices/system/cpu/kernel_max"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fw:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/android/server/ssrm/P;->fx:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/android/server/ssrm/P;->fy:I

    const-string v0, "/sys/devices/platform/scxx30-dmcfreq.0/devfreq/scxx30-dmcfreq.0/min_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fz:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/scxx30-dmcfreq.0/devfreq/scxx30-dmcfreq.0/max_freq"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fA:Ljava/lang/String;

    const-string v0, "/sys/devices/platform/scxx30-dmcfreq.0/devfreq/scxx30-dmcfreq.0/available_frequencies"

    iput-object v0, p0, Lcom/android/server/ssrm/P;->fB:Ljava/lang/String;

    const v0, 0x30d40

    iput v0, p0, Lcom/android/server/ssrm/P;->fC:I

    const v0, 0x81e20

    iput v0, p0, Lcom/android/server/ssrm/P;->fD:I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
