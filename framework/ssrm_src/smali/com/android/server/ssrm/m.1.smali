.class public Lcom/android/server/ssrm/m;
.super Ljava/lang/Object;


# instance fields
.field private final P:[Ljava/lang/Object;

.field private Q:I

.field private R:I

.field private size:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/android/server/ssrm/m;->Q:I

    iput v0, p0, Lcom/android/server/ssrm/m;->R:I

    iput v0, p0, Lcom/android/server/ssrm/m;->size:I

    new-array v0, p1, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/server/ssrm/m;->P:[Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/Object;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/server/ssrm/m;->P:[Ljava/lang/Object;

    iget v1, p0, Lcom/android/server/ssrm/m;->R:I

    aput-object p1, v0, v1

    iget v0, p0, Lcom/android/server/ssrm/m;->R:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/android/server/ssrm/m;->P:[Ljava/lang/Object;

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/ssrm/m;->R:I

    iget v0, p0, Lcom/android/server/ssrm/m;->size:I

    iget-object v1, p0, Lcom/android/server/ssrm/m;->P:[Ljava/lang/Object;

    array-length v1, v1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/android/server/ssrm/m;->Q:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/android/server/ssrm/m;->P:[Ljava/lang/Object;

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/android/server/ssrm/m;->Q:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/android/server/ssrm/m;->size:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/ssrm/m;->size:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()Ljava/lang/Object;
    .locals 3

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/server/ssrm/m;->size:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/server/ssrm/m;->P:[Ljava/lang/Object;

    iget v1, p0, Lcom/android/server/ssrm/m;->Q:I

    aget-object v0, v0, v1

    iget v1, p0, Lcom/android/server/ssrm/m;->Q:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/android/server/ssrm/m;->P:[Ljava/lang/Object;

    array-length v2, v2

    rem-int/2addr v1, v2

    iput v1, p0, Lcom/android/server/ssrm/m;->Q:I

    iget v1, p0, Lcom/android/server/ssrm/m;->size:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/android/server/ssrm/m;->size:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized e()Ljava/lang/Object;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/server/ssrm/m;->size:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/server/ssrm/m;->P:[Ljava/lang/Object;

    iget v1, p0, Lcom/android/server/ssrm/m;->Q:I

    aget-object v0, v0, v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized peekLast()Ljava/lang/Object;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/server/ssrm/m;->size:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget v0, p0, Lcom/android/server/ssrm/m;->R:I

    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/android/server/ssrm/m;->P:[Ljava/lang/Object;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :cond_1
    iget-object v1, p0, Lcom/android/server/ssrm/m;->P:[Ljava/lang/Object;

    aget-object v0, v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized size()I
    .locals 1

    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/android/server/ssrm/m;->size:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
