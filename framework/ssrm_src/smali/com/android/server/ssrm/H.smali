.class public Lcom/android/server/ssrm/H;
.super Ljava/lang/Object;


# static fields
.field static final co:Z


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final ca:[B

.field private cb:[Lcom/android/server/ssrm/I;

.field public final cc:Ljava/util/Hashtable;

.field private final cd:I

.field private final ce:I

.field private final cf:I

.field private final cg:I

.field private final ch:I

.field private final ci:I

.field private final cj:I

.field private final ck:I

.field private final cl:I

.field private final cm:I

.field private final cn:I

.field count:I


# direct methods
.method constructor <init>([B)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-class v0, Lcom/android/server/ssrm/H;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/H;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/android/server/ssrm/H;->cc:Ljava/util/Hashtable;

    iput v1, p0, Lcom/android/server/ssrm/H;->cd:I

    const/16 v0, 0x8

    iput v0, p0, Lcom/android/server/ssrm/H;->ce:I

    const/16 v0, 0xc

    iput v0, p0, Lcom/android/server/ssrm/H;->cf:I

    const/16 v0, 0x20

    iput v0, p0, Lcom/android/server/ssrm/H;->cg:I

    const/16 v0, 0x24

    iput v0, p0, Lcom/android/server/ssrm/H;->ch:I

    const/16 v0, 0x28

    iput v0, p0, Lcom/android/server/ssrm/H;->ci:I

    const/16 v0, 0x2c

    iput v0, p0, Lcom/android/server/ssrm/H;->cj:I

    const/16 v0, 0x30

    iput v0, p0, Lcom/android/server/ssrm/H;->ck:I

    const/16 v0, 0x34

    iput v0, p0, Lcom/android/server/ssrm/H;->cl:I

    const/16 v0, 0x38

    iput v0, p0, Lcom/android/server/ssrm/H;->cm:I

    const/16 v0, 0x3c

    iput v0, p0, Lcom/android/server/ssrm/H;->cn:I

    iput v1, p0, Lcom/android/server/ssrm/H;->count:I

    iput-object p1, p0, Lcom/android/server/ssrm/H;->ca:[B

    return-void
.end method

.method public static a([B[CI)Ljava/lang/String;
    .locals 8

    const/4 v1, 0x0

    const/16 v6, 0x80

    move v2, v1

    :goto_0
    add-int/lit8 v0, p2, 0x1

    :try_start_0
    aget-byte v1, p0, p2

    and-int/lit16 v1, v1, 0xff

    int-to-char v3, v1

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1, v2}, Ljava/lang/String;-><init>([CII)V

    :goto_1
    return-object v0

    :cond_0
    aput-char v3, p1, v2

    if-ge v3, v6, :cond_1

    add-int/lit8 v1, v2, 0x1

    :goto_2
    move p2, v0

    move v2, v1

    goto :goto_0

    :cond_1
    and-int/lit16 v1, v3, 0xe0

    const/16 v4, 0xc0

    if-ne v1, v4, :cond_3

    add-int/lit8 v1, v0, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v4, v0, 0xff

    and-int/lit16 v0, v4, 0xc0

    if-eq v0, v6, :cond_2

    const-string v0, ""

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v2, 0x1

    and-int/lit8 v3, v3, 0x1f

    shl-int/lit8 v3, v3, 0x6

    and-int/lit8 v4, v4, 0x3f

    or-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, p1, v2

    move v7, v1

    move v1, v0

    move v0, v7

    goto :goto_2

    :cond_3
    and-int/lit16 v1, v3, 0xf0

    const/16 v4, 0xe0

    if-ne v1, v4, :cond_6

    add-int/lit8 v1, v0, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v4, v0, 0xff

    add-int/lit8 v0, v1, 0x1

    aget-byte v1, p0, v1

    and-int/lit16 v5, v1, 0xff

    and-int/lit16 v1, v4, 0xc0

    if-ne v1, v6, :cond_4

    and-int/lit16 v1, v5, 0xc0

    if-eq v1, v6, :cond_5

    :cond_4
    const-string v0, ""

    goto :goto_1

    :cond_5
    add-int/lit8 v1, v2, 0x1

    and-int/lit8 v3, v3, 0xf

    shl-int/lit8 v3, v3, 0xc

    and-int/lit8 v4, v4, 0x3f

    shl-int/lit8 v4, v4, 0x6

    or-int/2addr v3, v4

    and-int/lit8 v4, v5, 0x3f

    or-int/2addr v3, v4

    int-to-char v3, v3

    aput-char v3, p1, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v0, ""

    goto :goto_1

    :cond_6
    :try_start_1
    const-string v0, ""
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public a([B)I
    .locals 8

    const/16 v7, 0x80

    const/4 v2, 0x0

    const/4 v0, -0x1

    move v1, v2

    move v3, v2

    :cond_0
    aget-byte v4, p1, v1

    and-int/lit16 v4, v4, 0xff

    and-int/lit8 v5, v4, 0x7f

    mul-int/lit8 v6, v1, 0x7

    shl-int/2addr v5, v6

    or-int/2addr v3, v5

    shl-int/lit8 v0, v0, 0x7

    add-int/lit8 v1, v1, 0x1

    and-int/lit16 v5, v4, 0x80

    if-ne v5, v7, :cond_1

    const/4 v5, 0x5

    if-lt v1, v5, :cond_0

    :cond_1
    and-int/lit16 v1, v4, 0x80

    if-ne v1, v7, :cond_2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "invalid LEB128 sequence"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_0
    return v2

    :cond_2
    shr-int/lit8 v1, v0, 0x1

    and-int/2addr v1, v3

    if-eqz v1, :cond_3

    or-int/2addr v3, v0

    :cond_3
    move v2, v3

    goto :goto_0
.end method

.method public am()I
    .locals 1

    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/android/server/ssrm/H;->m(I)I

    move-result v0

    return v0
.end method

.method public an()I
    .locals 1

    const/16 v0, 0x38

    invoke-virtual {p0, v0}, Lcom/android/server/ssrm/H;->m(I)I

    move-result v0

    return v0
.end method

.method public ao()I
    .locals 1

    const/16 v0, 0x3c

    invoke-virtual {p0, v0}, Lcom/android/server/ssrm/H;->m(I)I

    move-result v0

    return v0
.end method

.method public ap()V
    .locals 6

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {p0}, Lcom/android/server/ssrm/H;->an()I

    move-result v1

    if-gtz v1, :cond_1

    :cond_0
    return-void

    :cond_1
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "parseStringItems:: size = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/server/ssrm/H;->ao()I

    move-result v2

    new-array v0, v1, [Lcom/android/server/ssrm/I;

    iput-object v0, p0, Lcom/android/server/ssrm/H;->cb:[Lcom/android/server/ssrm/I;

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v3, p0, Lcom/android/server/ssrm/H;->cb:[Lcom/android/server/ssrm/I;

    new-instance v4, Lcom/android/server/ssrm/I;

    invoke-direct {v4}, Lcom/android/server/ssrm/I;-><init>()V

    aput-object v4, v3, v0

    iget-object v3, p0, Lcom/android/server/ssrm/H;->cb:[Lcom/android/server/ssrm/I;

    aget-object v3, v3, v0

    mul-int/lit8 v4, v0, 0x4

    add-int/2addr v4, v2

    invoke-virtual {p0, v4}, Lcom/android/server/ssrm/H;->m(I)I

    move-result v4

    iput v4, v3, Lcom/android/server/ssrm/I;->offset:I

    iget-object v3, p0, Lcom/android/server/ssrm/H;->cb:[Lcom/android/server/ssrm/I;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/android/server/ssrm/H;->cb:[Lcom/android/server/ssrm/I;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/android/server/ssrm/I;->offset:I

    invoke-virtual {p0, v4}, Lcom/android/server/ssrm/H;->n(I)I

    move-result v4

    iput v4, v3, Lcom/android/server/ssrm/I;->cp:I

    iget-object v3, p0, Lcom/android/server/ssrm/H;->cb:[Lcom/android/server/ssrm/I;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/android/server/ssrm/I;->cp:I

    const/4 v4, 0x3

    if-le v3, v4, :cond_2

    iget-object v3, p0, Lcom/android/server/ssrm/H;->cb:[Lcom/android/server/ssrm/I;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/android/server/ssrm/H;->cb:[Lcom/android/server/ssrm/I;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/android/server/ssrm/I;->offset:I

    iget v5, p0, Lcom/android/server/ssrm/H;->count:I

    add-int/2addr v4, v5

    invoke-virtual {p0, v4}, Lcom/android/server/ssrm/H;->o(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/android/server/ssrm/I;->cq:Ljava/lang/String;

    :cond_2
    iget-object v3, p0, Lcom/android/server/ssrm/H;->cb:[Lcom/android/server/ssrm/I;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/android/server/ssrm/I;->cq:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/android/server/ssrm/H;->cc:Ljava/util/Hashtable;

    iget-object v4, p0, Lcom/android/server/ssrm/H;->cb:[Lcom/android/server/ssrm/I;

    aget-object v4, v4, v0

    iget-object v4, v4, Lcom/android/server/ssrm/I;->cq:Ljava/lang/String;

    iget-object v5, p0, Lcom/android/server/ssrm/H;->cb:[Lcom/android/server/ssrm/I;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/android/server/ssrm/I;->cq:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public b([B)I
    .locals 6

    const/16 v5, 0x80

    const/4 v1, 0x0

    iput v1, p0, Lcom/android/server/ssrm/H;->count:I

    move v0, v1

    :cond_0
    iget v2, p0, Lcom/android/server/ssrm/H;->count:I

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    and-int/lit8 v3, v2, 0x7f

    iget v4, p0, Lcom/android/server/ssrm/H;->count:I

    mul-int/lit8 v4, v4, 0x7

    shl-int/2addr v3, v4

    or-int/2addr v0, v3

    iget v3, p0, Lcom/android/server/ssrm/H;->count:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/android/server/ssrm/H;->count:I

    and-int/lit16 v3, v2, 0x80

    if-ne v3, v5, :cond_1

    iget v3, p0, Lcom/android/server/ssrm/H;->count:I

    const/4 v4, 0x5

    if-lt v3, v4, :cond_0

    :cond_1
    and-int/lit16 v2, v2, 0x80

    if-ne v2, v5, :cond_2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "invalid LEB128 sequence"

    invoke-virtual {v0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_0
    return v1

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public m(I)I
    .locals 4

    const/4 v3, 0x4

    new-array v0, v3, [B

    iget-object v1, p0, Lcom/android/server/ssrm/H;->ca:[B

    const/4 v2, 0x0

    invoke-static {v1, p1, v0, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    return v0
.end method

.method public n(I)I
    .locals 4

    const/16 v3, 0xa

    new-array v0, v3, [B

    iget-object v1, p0, Lcom/android/server/ssrm/H;->ca:[B

    const/4 v2, 0x0

    invoke-static {v1, p1, v0, v2, v3}, Ljava/lang/System;->arraycopy([BI[BII)V

    invoke-virtual {p0, v0}, Lcom/android/server/ssrm/H;->b([B)I

    move-result v0

    return v0
.end method

.method public o(I)Ljava/lang/String;
    .locals 2

    const/16 v0, 0x80

    new-array v0, v0, [C

    iget-object v1, p0, Lcom/android/server/ssrm/H;->ca:[B

    invoke-static {v1, v0, p1}, Lcom/android/server/ssrm/H;->a([B[CI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
