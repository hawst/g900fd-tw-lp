.class public final enum Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;
.super Ljava/lang/Enum;


# static fields
.field public static final enum eS:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

.field public static final enum eT:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

.field public static final enum eU:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

.field public static final enum eV:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

.field private static final synthetic eW:[Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;->eS:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    const-string v1, "QUALCOMM"

    invoke-direct {v0, v1, v3}, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;->eT:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    const-string v1, "SLSI"

    invoke-direct {v0, v1, v4}, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;->eU:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    new-instance v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    const-string v1, "MARVELL"

    invoke-direct {v0, v1, v5}, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;->eV:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;->eS:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;->eT:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;->eU:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;->eV:Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    aput-object v1, v0, v5

    sput-object v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;->eW:[Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;
    .locals 1

    const-class v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    return-object v0
.end method

.method public static values()[Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;->eW:[Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    invoke-virtual {v0}, [Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/ssrm/GenericFalImpl$ChipVendor;

    return-object v0
.end method
