.class final Lcom/android/server/ssrm/aN;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v2, -0x1

    const/4 v4, 0x0

    :try_start_0
    invoke-static {}, Lcom/android/server/ssrm/aL;->getDuration()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    # getter for: Lcom/android/server/ssrm/aL;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/aL;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "run:: The PreSIOP is deactivating now."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    iget v0, v0, Lcom/android/server/ssrm/aP;->kt:I

    if-eq v0, v2, :cond_0

    sget-object v0, Lcom/android/server/ssrm/aL;->kl:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    iget v0, v0, Lcom/android/server/ssrm/aP;->ku:I

    if-eq v0, v2, :cond_1

    sget-object v0, Lcom/android/server/ssrm/aL;->km:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    :cond_1
    invoke-static {}, Lcom/android/server/ssrm/ag;->bj()Lcom/android/server/ssrm/ag;

    move-result-object v0

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->jg:Lcom/android/server/ssrm/aD;

    iget v0, v0, Lcom/android/server/ssrm/aD;->jW:I

    invoke-static {v0}, Lcom/android/server/ssrm/aO;->P(I)V

    invoke-static {}, Lcom/android/server/ssrm/aO;->bD()V

    sget-object v0, Lcom/android/server/ssrm/aL;->ki:Lcom/android/server/ssrm/aP;

    iget-boolean v0, v0, Lcom/android/server/ssrm/aP;->kw:Z

    if-nez v0, :cond_2

    const/16 v0, 0xff

    invoke-static {v0}, Lcom/android/server/ssrm/aO;->Q(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    sput-boolean v4, Lcom/android/server/ssrm/aL;->kj:Z

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    # getter for: Lcom/android/server/ssrm/aL;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/aL;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "e = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sput-boolean v4, Lcom/android/server/ssrm/aL;->kj:Z

    goto :goto_0

    :catchall_0
    move-exception v0

    sput-boolean v4, Lcom/android/server/ssrm/aL;->kj:Z

    throw v0
.end method
