.class Lcom/android/server/ssrm/r;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/CustomFrequencyManagerService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/r;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lcom/android/server/ssrm/r;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->b(Lcom/android/server/ssrm/CustomFrequencyManagerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "action_batterystats_refresh"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/android/server/ssrm/r;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->b(Lcom/android/server/ssrm/CustomFrequencyManagerService;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/r;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->g(Lcom/android/server/ssrm/CustomFrequencyManagerService;)Lcom/android/internal/os/BatteryStatsDumper;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/r;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    new-instance v1, Lcom/android/internal/os/BatteryStatsDumper;

    invoke-direct {v1, p1}, Lcom/android/internal/os/BatteryStatsDumper;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->a(Lcom/android/server/ssrm/CustomFrequencyManagerService;Lcom/android/internal/os/BatteryStatsDumper;)Lcom/android/internal/os/BatteryStatsDumper;

    :cond_2
    iget-object v0, p0, Lcom/android/server/ssrm/r;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->g(Lcom/android/server/ssrm/CustomFrequencyManagerService;)Lcom/android/internal/os/BatteryStatsDumper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/internal/os/BatteryStatsDumper;->updatePrevBatteryValue()V

    goto :goto_0
.end method
