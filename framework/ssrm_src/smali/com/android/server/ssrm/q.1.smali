.class Lcom/android/server/ssrm/q;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/CustomFrequencyManagerService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/q;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.sm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/q;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->b(Lcom/android/server/ssrm/CustomFrequencyManagerService;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/q;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->c(Lcom/android/server/ssrm/CustomFrequencyManagerService;Z)Z

    sget-object v0, Lcom/android/server/ssrm/CustomFrequencyManagerService;->TAG:Ljava/lang/String;

    const-string v1, "ACTION_PACKAGE_ADDED :: start SmartManager"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/q;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v0}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->a(Lcom/android/server/ssrm/CustomFrequencyManagerService;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.samsung.android.sm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/ssrm/q;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->c(Lcom/android/server/ssrm/CustomFrequencyManagerService;Z)Z

    iget-object v1, p0, Lcom/android/server/ssrm/q;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v1}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->e(Lcom/android/server/ssrm/CustomFrequencyManagerService;)Landroid/app/AlarmManager;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/server/ssrm/q;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v1}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->f(Lcom/android/server/ssrm/CustomFrequencyManagerService;)Landroid/app/PendingIntent;

    move-result-object v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/android/server/ssrm/CustomFrequencyManagerService;->TAG:Ljava/lang/String;

    const-string v2, "ACTION_PACKAGE_REMOVED :: Pending Alarm Cancelled"

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/server/ssrm/q;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v1}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->e(Lcom/android/server/ssrm/CustomFrequencyManagerService;)Landroid/app/AlarmManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/ssrm/q;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    invoke-static {v2}, Lcom/android/server/ssrm/CustomFrequencyManagerService;->f(Lcom/android/server/ssrm/CustomFrequencyManagerService;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    :cond_2
    iget-object v1, p0, Lcom/android/server/ssrm/q;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    iget-object v1, v1, Lcom/android/server/ssrm/CustomFrequencyManagerService;->aQ:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/server/ssrm/CustomFrequencyManagerService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/ssrm/q;->aU:Lcom/android/server/ssrm/CustomFrequencyManagerService;

    iget-object v2, v2, Lcom/android/server/ssrm/CustomFrequencyManagerService;->aQ:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    sget-object v1, Lcom/android/server/ssrm/CustomFrequencyManagerService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACTION_PACKAGE_REMOVED :: Deregistered  mBatteryStatsReceiver broadcast"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
