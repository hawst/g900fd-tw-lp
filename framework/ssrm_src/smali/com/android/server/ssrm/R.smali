.class Lcom/android/server/ssrm/R;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic gb:Lcom/android/server/ssrm/Q;


# direct methods
.method constructor <init>(Lcom/android/server/ssrm/Q;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/ssrm/R;->gb:Lcom/android/server/ssrm/Q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/16 v4, -0x3e7

    :try_start_0
    # getter for: Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/Q;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mCheck2ndHMTtableRunnable!"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/R;->gb:Lcom/android/server/ssrm/Q;

    invoke-virtual {v0}, Lcom/android/server/ssrm/Q;->aw()I

    move-result v0

    if-eq v0, v4, :cond_1

    iget-object v1, p0, Lcom/android/server/ssrm/R;->gb:Lcom/android/server/ssrm/Q;

    iput v0, v1, Lcom/android/server/ssrm/Q;->fX:I

    :goto_0
    invoke-static {}, Lcom/android/server/ssrm/G;->U()I

    move-result v0

    # getter for: Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/Q;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mBatteryTempCheckRunable:: batteryTemp = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/server/ssrm/R;->gb:Lcom/android/server/ssrm/Q;

    iget v1, v1, Lcom/android/server/ssrm/Q;->fX:I

    if-le v0, v1, :cond_0

    const-string v0, "HeadMountedTheater_2"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_0
    iget-object v0, p0, Lcom/android/server/ssrm/R;->gb:Lcom/android/server/ssrm/Q;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/server/ssrm/Q;->t:Z

    iget-object v0, p0, Lcom/android/server/ssrm/R;->gb:Lcom/android/server/ssrm/Q;

    invoke-virtual {v0}, Lcom/android/server/ssrm/Q;->ax()I

    move-result v0

    if-eq v0, v4, :cond_2

    :goto_1
    # getter for: Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/Q;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "battery temp will be check in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/server/ssrm/ag;->bj()Lcom/android/server/ssrm/ag;

    move-result-object v1

    iget-object v1, v1, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/server/ssrm/R;->gb:Lcom/android/server/ssrm/Q;

    iget-object v2, v2, Lcom/android/server/ssrm/Q;->fZ:Ljava/lang/Runnable;

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_2
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/server/ssrm/R;->gb:Lcom/android/server/ssrm/Q;

    const/16 v1, 0x18b

    iput v1, v0, Lcom/android/server/ssrm/Q;->fX:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    # getter for: Lcom/android/server/ssrm/Q;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/ssrm/Q;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mCheck2ndHMTtableRunnable:: e = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    const v0, 0xea60

    goto :goto_1
.end method
